<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_1965_Bear_nar</transcription-name>
         <referenced-file url="YIF_196X_Bear_nar.wav" />
         <referenced-file url="YIF_196X_Bear_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_Bear_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">393</ud-information>
            <ud-information attribute-name="# HIAT:w">271</ud-information>
            <ud-information attribute-name="# e">272</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">42</ud-information>
            <ud-information attribute-name="# sc">76</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.39999999999999997" type="appl" />
         <tli id="T3" time="0.7999999999999999" type="appl" />
         <tli id="T4" time="1.2" type="appl" />
         <tli id="T5" time="1.37" type="appl" />
         <tli id="T6" time="2.124" type="appl" />
         <tli id="T306" time="2.4256" type="intp" />
         <tli id="T7" time="2.878" type="appl" />
         <tli id="T8" time="3.6319999999999997" type="appl" />
         <tli id="T9" time="4.385999999999999" type="appl" />
         <tli id="T307" time="4.816857142857143" type="intp" />
         <tli id="T10" time="5.14" type="appl" />
         <tli id="T11" time="6.226631228930135" />
         <tli id="T12" time="6.80375" type="appl" />
         <tli id="T13" time="7.3925" type="appl" />
         <tli id="T14" time="7.98125" type="appl" />
         <tli id="T15" time="8.57" type="appl" />
         <tli id="T16" time="8.59" type="appl" />
         <tli id="T17" time="9.04" type="appl" />
         <tli id="T18" time="9.49" type="appl" />
         <tli id="T19" time="9.94" type="appl" />
         <tli id="T20" time="10.389999999999999" type="appl" />
         <tli id="T21" time="10.84" type="appl" />
         <tli id="T22" time="11.29" type="appl" />
         <tli id="T23" time="12.055" type="appl" />
         <tli id="T24" time="12.6625" type="appl" />
         <tli id="T25" time="13.27" type="appl" />
         <tli id="T26" time="13.685" type="appl" />
         <tli id="T27" time="14.268571428571429" type="appl" />
         <tli id="T28" time="14.852142857142857" type="appl" />
         <tli id="T29" time="15.435714285714285" type="appl" />
         <tli id="T30" time="16.019285714285715" type="appl" />
         <tli id="T31" time="16.602857142857143" type="appl" />
         <tli id="T32" time="17.18642857142857" type="appl" />
         <tli id="T33" time="17.77" type="appl" />
         <tli id="T34" time="18.045" type="appl" />
         <tli id="T35" time="18.5465" type="appl" />
         <tli id="T36" time="19.048000000000002" type="appl" />
         <tli id="T37" time="19.549500000000002" type="appl" />
         <tli id="T38" time="20.051000000000002" type="appl" />
         <tli id="T39" time="20.552500000000002" type="appl" />
         <tli id="T40" time="21.054000000000002" type="appl" />
         <tli id="T41" time="21.5555" type="appl" />
         <tli id="T308" time="21.80625" type="intp" />
         <tli id="T42" time="22.057" type="appl" />
         <tli id="T43" time="22.5585" type="appl" />
         <tli id="T44" time="23.06" type="appl" />
         <tli id="T45" time="23.5475" type="appl" />
         <tli id="T46" time="24.035" type="appl" />
         <tli id="T47" time="24.5225" type="appl" />
         <tli id="T48" time="25.01" type="appl" />
         <tli id="T49" time="25.39" type="appl" />
         <tli id="T50" time="26.023333333333333" type="appl" />
         <tli id="T51" time="26.656666666666666" type="appl" />
         <tli id="T52" time="27.29" type="appl" />
         <tli id="T53" time="27.545" type="appl" />
         <tli id="T54" time="28.240714285714287" type="appl" />
         <tli id="T55" time="28.93642857142857" type="appl" />
         <tli id="T56" time="29.63214285714286" type="appl" />
         <tli id="T309" time="29.980000000000004" type="intp" />
         <tli id="T57" time="30.327857142857145" type="appl" />
         <tli id="T310" time="30.675714285714285" type="intp" />
         <tli id="T58" time="31.02357142857143" type="appl" />
         <tli id="T59" time="31.719285714285714" type="appl" />
         <tli id="T60" time="32.415" type="appl" />
         <tli id="T61" time="32.92" type="appl" />
         <tli id="T62" time="33.288333333333334" type="appl" />
         <tli id="T63" time="33.656666666666666" type="appl" />
         <tli id="T64" time="34.025000000000006" type="appl" />
         <tli id="T65" time="34.39333333333334" type="appl" />
         <tli id="T66" time="34.76166666666667" type="appl" />
         <tli id="T67" time="35.13" type="appl" />
         <tli id="T68" time="35.498333333333335" type="appl" />
         <tli id="T69" time="35.86666666666667" type="appl" />
         <tli id="T70" time="36.235" type="appl" />
         <tli id="T71" time="36.60333333333334" type="appl" />
         <tli id="T72" time="36.97166666666667" type="appl" />
         <tli id="T73" time="37.34" type="appl" />
         <tli id="T74" time="37.895" type="appl" />
         <tli id="T75" time="38.559000000000005" type="appl" />
         <tli id="T311" time="38.891000000000005" type="intp" />
         <tli id="T76" time="39.223000000000006" type="appl" />
         <tli id="T77" time="39.887" type="appl" />
         <tli id="T78" time="40.551" type="appl" />
         <tli id="T79" time="41.215" type="appl" />
         <tli id="T80" time="42.15" type="appl" />
         <tli id="T81" time="42.55166666666666" type="appl" />
         <tli id="T83" time="43.355000000000004" type="appl" />
         <tli id="T84" time="43.75666666666667" type="appl" />
         <tli id="T85" time="44.15833333333333" type="appl" />
         <tli id="T86" time="44.56" type="appl" />
         <tli id="T87" time="45.77" type="appl" />
         <tli id="T88" time="46.184285714285714" type="appl" />
         <tli id="T89" time="46.59857142857143" type="appl" />
         <tli id="T90" time="47.01285714285714" type="appl" />
         <tli id="T91" time="47.42714285714286" type="appl" />
         <tli id="T92" time="47.84142857142857" type="appl" />
         <tli id="T93" time="48.25571428571429" type="appl" />
         <tli id="T94" time="48.67" type="appl" />
         <tli id="T95" time="48.78" type="appl" />
         <tli id="T96" time="49.6" type="appl" />
         <tli id="T97" time="50.29" type="appl" />
         <tli id="T98" time="51.376666666666665" type="appl" />
         <tli id="T99" time="52.46333333333333" type="appl" />
         <tli id="T100" time="53.55" type="appl" />
         <tli id="T101" time="54.63666666666667" type="appl" />
         <tli id="T102" time="55.723333333333336" type="appl" />
         <tli id="T103" time="56.81" type="appl" />
         <tli id="T104" time="57.414" type="appl" />
         <tli id="T105" time="58.018" type="appl" />
         <tli id="T106" time="58.622" type="appl" />
         <tli id="T107" time="59.226" type="appl" />
         <tli id="T108" time="59.83" type="appl" />
         <tli id="T109" time="60.875" type="appl" />
         <tli id="T110" time="61.595" type="appl" />
         <tli id="T111" time="62.315000000000005" type="appl" />
         <tli id="T112" time="63.035000000000004" type="appl" />
         <tli id="T113" time="63.755" type="appl" />
         <tli id="T114" time="64.47500000000001" type="appl" />
         <tli id="T115" time="65.19500000000001" type="appl" />
         <tli id="T116" time="65.915" type="appl" />
         <tli id="T117" time="66.39" type="appl" />
         <tli id="T118" time="66.96666666666667" type="appl" />
         <tli id="T119" time="67.54333333333334" type="appl" />
         <tli id="T120" time="68.12" type="appl" />
         <tli id="T121" time="68.69666666666667" type="appl" />
         <tli id="T122" time="69.27333333333333" type="appl" />
         <tli id="T123" time="69.85" type="appl" />
         <tli id="T124" time="70.42666666666666" type="appl" />
         <tli id="T125" time="71.00333333333333" type="appl" />
         <tli id="T126" time="71.58" type="appl" />
         <tli id="T127" time="71.888" type="appl" />
         <tli id="T128" time="72.43866666666668" type="appl" />
         <tli id="T129" time="72.98933333333333" type="appl" />
         <tli id="T130" time="73.54" type="appl" />
         <tli id="T131" time="73.838" type="appl" />
         <tli id="T132" time="74.346" type="appl" />
         <tli id="T133" time="74.854" type="appl" />
         <tli id="T134" time="75.362" type="appl" />
         <tli id="T135" time="75.87" type="appl" />
         <tli id="T136" time="76.135" type="appl" />
         <tli id="T137" time="76.744" type="appl" />
         <tli id="T138" time="77.35300000000001" type="appl" />
         <tli id="T139" time="77.962" type="appl" />
         <tli id="T140" time="78.571" type="appl" />
         <tli id="T141" time="79.18" type="appl" />
         <tli id="T142" time="79.789" type="appl" />
         <tli id="T143" time="80.949" type="appl" />
         <tli id="T144" time="81.47675" type="appl" />
         <tli id="T145" time="82.00450000000001" type="appl" />
         <tli id="T146" time="82.53225" type="appl" />
         <tli id="T147" time="83.06" type="appl" />
         <tli id="T148" time="83.295" type="appl" />
         <tli id="T149" time="83.816" type="appl" />
         <tli id="T150" time="84.337" type="appl" />
         <tli id="T151" time="84.858" type="appl" />
         <tli id="T152" time="85.379" type="appl" />
         <tli id="T153" time="85.9" type="appl" />
         <tli id="T154" time="86.616" type="appl" />
         <tli id="T155" time="87.332" type="appl" />
         <tli id="T156" time="87.875" type="appl" />
         <tli id="T157" time="88.29" type="appl" />
         <tli id="T158" time="88.705" type="appl" />
         <tli id="T159" time="89.12" type="appl" />
         <tli id="T160" time="89.535" type="appl" />
         <tli id="T161" time="89.95" type="appl" />
         <tli id="T162" time="90.365" type="appl" />
         <tli id="T163" time="90.78" type="appl" />
         <tli id="T164" time="91.195" type="appl" />
         <tli id="T165" time="91.61" type="appl" />
         <tli id="T166" time="92.02499999999999" type="appl" />
         <tli id="T167" time="92.44" type="appl" />
         <tli id="T168" time="92.85499999999999" type="appl" />
         <tli id="T169" time="93.27" type="appl" />
         <tli id="T170" time="94.999" type="appl" />
         <tli id="T171" time="95.380625" type="appl" />
         <tli id="T172" time="95.76225" type="appl" />
         <tli id="T173" time="96.143875" type="appl" />
         <tli id="T174" time="96.5255" type="appl" />
         <tli id="T175" time="96.90712500000001" type="appl" />
         <tli id="T176" time="97.28875000000001" type="appl" />
         <tli id="T177" time="97.670375" type="appl" />
         <tli id="T178" time="98.052" type="appl" />
         <tli id="T179" time="99.388" type="appl" />
         <tli id="T180" time="100.307" type="appl" />
         <tli id="T181" time="101.226" type="appl" />
         <tli id="T182" time="102.145" type="appl" />
         <tli id="T183" time="102.727" type="appl" />
         <tli id="T184" time="103.15023076923077" type="appl" />
         <tli id="T185" time="103.57346153846154" type="appl" />
         <tli id="T186" time="103.99669230769231" type="appl" />
         <tli id="T187" time="104.41992307692308" type="appl" />
         <tli id="T188" time="104.84315384615385" type="appl" />
         <tli id="T189" time="105.26638461538462" type="appl" />
         <tli id="T190" time="105.68961538461538" type="appl" />
         <tli id="T191" time="106.11284615384615" type="appl" />
         <tli id="T192" time="106.53607692307692" type="appl" />
         <tli id="T193" time="106.95930769230769" type="appl" />
         <tli id="T194" time="107.38253846153846" type="appl" />
         <tli id="T195" time="107.80576923076923" type="appl" />
         <tli id="T196" time="108.229" type="appl" />
         <tli id="T197" time="109.012" type="appl" />
         <tli id="T198" time="109.99533333333333" type="appl" />
         <tli id="T199" time="110.97866666666667" type="appl" />
         <tli id="T200" time="111.962" type="appl" />
         <tli id="T201" time="112.94533333333334" type="appl" />
         <tli id="T202" time="113.92866666666667" type="appl" />
         <tli id="T203" time="114.912" type="appl" />
         <tli id="T204" time="115.89533333333333" type="appl" />
         <tli id="T205" time="116.87866666666666" type="appl" />
         <tli id="T206" time="117.862" type="appl" />
         <tli id="T207" time="118.84533333333333" type="appl" />
         <tli id="T208" time="119.82866666666666" type="appl" />
         <tli id="T209" time="120.812" type="appl" />
         <tli id="T210" time="121.518" type="appl" />
         <tli id="T211" time="122.12950000000001" type="appl" />
         <tli id="T212" time="122.741" type="appl" />
         <tli id="T213" time="123.35249999999999" type="appl" />
         <tli id="T214" time="123.964" type="appl" />
         <tli id="T215" time="124.5755" type="appl" />
         <tli id="T216" time="125.187" type="appl" />
         <tli id="T217" time="125.79849999999999" type="appl" />
         <tli id="T218" time="126.41" type="appl" />
         <tli id="T219" time="127.0215" type="appl" />
         <tli id="T220" time="127.633" type="appl" />
         <tli id="T221" time="128.2445" type="appl" />
         <tli id="T222" time="128.856" type="appl" />
         <tli id="T223" time="129.4675" type="appl" />
         <tli id="T224" time="130.079" type="appl" />
         <tli id="T225" time="130.6905" type="appl" />
         <tli id="T226" time="131.302" type="appl" />
         <tli id="T227" time="131.802" type="appl" />
         <tli id="T228" time="132.61083333333332" type="appl" />
         <tli id="T229" time="133.41966666666667" type="appl" />
         <tli id="T230" time="134.2285" type="appl" />
         <tli id="T231" time="135.03733333333332" type="appl" />
         <tli id="T232" time="135.84616666666668" type="appl" />
         <tli id="T233" time="136.655" type="appl" />
         <tli id="T234" time="137.606" type="appl" />
         <tli id="T235" time="138.433" type="appl" />
         <tli id="T236" time="139.26" type="appl" />
         <tli id="T237" time="140.08700000000002" type="appl" />
         <tli id="T238" time="140.91400000000002" type="appl" />
         <tli id="T239" time="141.741" type="appl" />
         <tli id="T240" time="141.927" type="appl" />
         <tli id="T241" time="142.45116666666667" type="appl" />
         <tli id="T242" time="142.97533333333334" type="appl" />
         <tli id="T243" time="143.4995" type="appl" />
         <tli id="T244" time="144.02366666666666" type="appl" />
         <tli id="T245" time="144.54783333333333" type="appl" />
         <tli id="T246" time="145.072" type="appl" />
         <tli id="T247" time="145.692" type="appl" />
         <tli id="T248" time="146.25300000000001" type="appl" />
         <tli id="T249" time="146.81400000000002" type="appl" />
         <tli id="T250" time="147.375" type="appl" />
         <tli id="T251" time="147.936" type="appl" />
         <tli id="T252" time="148.497" type="appl" />
         <tli id="T253" time="149.05800000000002" type="appl" />
         <tli id="T254" time="149.619" type="appl" />
         <tli id="T255" time="150.18" type="appl" />
         <tli id="T256" time="150.741" type="appl" />
         <tli id="T257" time="151.235" type="appl" />
         <tli id="T258" time="151.5634" type="appl" />
         <tli id="T259" time="151.89180000000002" type="appl" />
         <tli id="T260" time="152.2202" type="appl" />
         <tli id="T261" time="152.54860000000002" type="appl" />
         <tli id="T262" time="152.877" type="appl" />
         <tli id="T263" time="153.2054" type="appl" />
         <tli id="T264" time="153.5338" type="appl" />
         <tli id="T265" time="153.8622" type="appl" />
         <tli id="T266" time="154.19060000000002" type="appl" />
         <tli id="T267" time="154.519" type="appl" />
         <tli id="T268" time="154.889" type="appl" />
         <tli id="T269" time="155.258" type="appl" />
         <tli id="T270" time="155.627" type="appl" />
         <tli id="T271" time="155.996" type="appl" />
         <tli id="T272" time="156.365" type="appl" />
         <tli id="T273" time="156.779" type="appl" />
         <tli id="T274" time="157.2975" type="appl" />
         <tli id="T275" time="157.816" type="appl" />
         <tli id="T276" time="158.27666666666667" type="appl" />
         <tli id="T277" time="158.73733333333334" type="appl" />
         <tli id="T278" time="159.198" type="appl" />
         <tli id="T279" time="159.455" type="appl" />
         <tli id="T280" time="159.96083333333334" type="appl" />
         <tli id="T281" time="160.46666666666667" type="appl" />
         <tli id="T282" time="160.97250000000003" type="appl" />
         <tli id="T283" time="161.47833333333335" type="appl" />
         <tli id="T284" time="161.98416666666668" type="appl" />
         <tli id="T285" time="162.49" type="appl" />
         <tli id="T286" time="163.006" type="appl" />
         <tli id="T287" time="163.578" type="appl" />
         <tli id="T288" time="164.15" type="appl" />
         <tli id="T289" time="164.722" type="appl" />
         <tli id="T290" time="165.215" type="appl" />
         <tli id="T291" time="165.61753333333334" type="appl" />
         <tli id="T292" time="166.02006666666668" type="appl" />
         <tli id="T293" time="166.4226" type="appl" />
         <tli id="T294" time="166.82513333333333" type="appl" />
         <tli id="T295" time="167.22766666666666" type="appl" />
         <tli id="T296" time="167.6302" type="appl" />
         <tli id="T297" time="168.03273333333334" type="appl" />
         <tli id="T298" time="168.43526666666665" type="appl" />
         <tli id="T299" time="168.8378" type="appl" />
         <tli id="T300" time="169.24033333333333" type="appl" />
         <tli id="T301" time="169.64286666666666" type="appl" />
         <tli id="T302" time="170.0454" type="appl" />
         <tli id="T303" time="170.4479333333333" type="appl" />
         <tli id="T304" time="170.85046666666665" type="appl" />
         <tli id="T305" time="171.253" type="appl" />
         <tli id="T0" time="172.045" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T4" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Kanduk</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qorɣop</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qonǯerxat</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T10" id="Seg_13" n="sc" s="T5">
               <ts e="T10" id="Seg_15" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">Mat</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_20" n="HIAT:w" s="T6">näːn</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T306">opte</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">čobortko</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">kwessaj</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_33" n="HIAT:w" s="T9">Lüdmilan</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T307">opte</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_39" n="sc" s="T11">
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">A</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">mi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">abɨʒejkat</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qwen</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_55" n="sc" s="T16">
               <ts e="T22" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">bauška</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Warwara</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">načʼan</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">wa-</ts>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">wargɨxaɣ</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_79" n="sc" s="T23">
               <ts e="T25" id="Seg_81" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">Üdenne</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">qwessaj</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_89" n="sc" s="T26">
               <ts e="T33" id="Seg_91" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">Taper</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_96" n="HIAT:w" s="T27">töwaj</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_98" n="HIAT:ip">—</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">bauška</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">Warwara</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">nʼejtuwaɣ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">na</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">astanopte</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_116" n="sc" s="T34">
               <ts e="T44" id="Seg_118" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">A</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_123" n="HIAT:w" s="T35">mat</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">Lüdkan</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_129" n="HIAT:w" s="T37">čenčak</ts>
                  <nts id="Seg_130" n="HIAT:ip">:</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_133" n="HIAT:w" s="T38">Manǯoɣa</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_135" n="HIAT:ip">—</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">tawčitə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_141" n="HIAT:w" s="T40">qwellaj</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_145" n="HIAT:w" s="T41">tawčit</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_148" n="HIAT:w" s="T308">taw</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_151" n="HIAT:w" s="T42">belekaɨt</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_154" n="HIAT:w" s="T43">qwellaj</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_158" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">Tɨndə</ts>
                  <nts id="Seg_161" n="HIAT:ip">,</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">moʒet</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">čobor</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">kolajhe</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_174" n="sc" s="T49">
               <ts e="T52" id="Seg_176" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">Taper</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_181" n="HIAT:w" s="T50">üpoǯäj</ts>
                  <nts id="Seg_182" n="HIAT:ip">,</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">čaːǯaj</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T60" id="Seg_188" n="sc" s="T53">
               <ts e="T60" id="Seg_190" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">A</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">Lüdka</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">meka</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_201" n="HIAT:w" s="T56">čenča</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T309">kawarit</ts>
                  <nts id="Seg_206" n="HIAT:ip">:</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_208" n="HIAT:ip">“</nts>
                  <ts e="T310" id="Seg_210" n="HIAT:w" s="T57">Tɨndə</ts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T310">kawarit</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">qajqoj</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">kwajakumba</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip">”</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T73" id="Seg_225" n="sc" s="T61">
               <ts e="T73" id="Seg_227" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">A</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_232" n="HIAT:w" s="T62">mat</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_235" n="HIAT:w" s="T63">taben</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">narošnak</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">čenčak</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">štobɨ</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">tab</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_251" n="HIAT:w" s="T68">ɨgɨ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_254" n="HIAT:w" s="T69">tanulend</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">što</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">urop</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">kwajakumbɨ</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_267" n="sc" s="T74">
               <ts e="T79" id="Seg_269" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">Mat</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_274" n="HIAT:w" s="T75">čenčak</ts>
                  <nts id="Seg_275" n="HIAT:ip">:</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">“</nts>
                  <ts e="T76" id="Seg_279" n="HIAT:w" s="T311">A</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">taw</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">bauškamd</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_289" n="HIAT:w" s="T78">qwajakumundaɣ</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_292" n="sc" s="T80">
               <ts e="T86" id="Seg_294" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">Čaǯaj</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_300" n="HIAT:w" s="T81">kalammut</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">elʼǯʼä</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">šɨtə</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">par</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T94" id="Seg_312" n="sc" s="T87">
               <ts e="T94" id="Seg_314" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_316" n="HIAT:w" s="T87">Taperʼ</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">man</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">üngolʼǯembap</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">mute</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_329" n="HIAT:w" s="T91">nep</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_332" n="HIAT:w" s="T92">aː</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_335" n="HIAT:w" s="T93">ügla</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T96" id="Seg_338" n="sc" s="T95">
               <ts e="T96" id="Seg_340" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_342" n="HIAT:w" s="T95">Čaǯaj</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_345" n="sc" s="T97">
               <ts e="T103" id="Seg_347" n="HIAT:u" s="T97">
                  <nts id="Seg_348" n="HIAT:ip">(</nts>
                  <nts id="Seg_349" n="HIAT:ip">(</nts>
                  <ats e="T98" id="Seg_350" n="HIAT:non-pho" s="T97">…</ats>
                  <nts id="Seg_351" n="HIAT:ip">)</nts>
                  <nts id="Seg_352" n="HIAT:ip">)</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_355" n="HIAT:w" s="T98">meka</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_358" n="HIAT:w" s="T99">nem</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_361" n="HIAT:w" s="T100">kurda</ts>
                  <nts id="Seg_362" n="HIAT:ip">:</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_364" n="HIAT:ip">“</nts>
                  <ts e="T102" id="Seg_366" n="HIAT:w" s="T101">Qorɣa</ts>
                  <nts id="Seg_367" n="HIAT:ip">,</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_370" n="HIAT:w" s="T102">tona</ts>
                  <nts id="Seg_371" n="HIAT:ip">!</nts>
                  <nts id="Seg_372" n="HIAT:ip">”</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_375" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_377" n="HIAT:w" s="T103">Mat</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_380" n="HIAT:w" s="T104">tabən</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_383" n="HIAT:w" s="T105">barʒak</ts>
                  <nts id="Seg_384" n="HIAT:ip">:</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_387" n="HIAT:w" s="T106">Kaj</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_390" n="HIAT:w" s="T107">qorɣa</ts>
                  <nts id="Seg_391" n="HIAT:ip">?</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T116" id="Seg_393" n="sc" s="T109">
               <ts e="T116" id="Seg_395" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_397" n="HIAT:w" s="T109">Enne</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_400" n="HIAT:w" s="T110">manǯeǯäk</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_402" n="HIAT:ip">—</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_405" n="HIAT:w" s="T111">sabɨlʼ</ts>
                  <nts id="Seg_406" n="HIAT:ip">,</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_409" n="HIAT:w" s="T112">qorɣ</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_412" n="HIAT:w" s="T113">nɨŋga</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_416" n="HIAT:w" s="T114">miʒe</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_419" n="HIAT:w" s="T115">manemba</ts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_422" n="sc" s="T117">
               <ts e="T126" id="Seg_424" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_426" n="HIAT:w" s="T117">Metra</ts>
                  <nts id="Seg_427" n="HIAT:ip">,</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_430" n="HIAT:w" s="T118">šed</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_433" n="HIAT:w" s="T119">metra</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_437" n="HIAT:w" s="T120">nagur</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_440" n="HIAT:w" s="T121">metra</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_443" n="HIAT:w" s="T122">čʼomb</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_446" n="HIAT:w" s="T123">nɨŋga</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_450" n="HIAT:w" s="T124">miʒe</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_453" n="HIAT:w" s="T125">manemba</ts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_456" n="sc" s="T127">
               <ts e="T130" id="Seg_458" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_460" n="HIAT:w" s="T127">Mat</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_463" n="HIAT:w" s="T128">toʒe</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_466" n="HIAT:w" s="T129">kɨčwannak</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_469" n="sc" s="T131">
               <ts e="T135" id="Seg_471" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">Tärbak</ts>
                  <nts id="Seg_474" n="HIAT:ip">:</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_477" n="HIAT:w" s="T132">Luče</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_480" n="HIAT:w" s="T133">maʒek</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_483" n="HIAT:w" s="T134">orallend</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T142" id="Seg_486" n="sc" s="T136">
               <ts e="T142" id="Seg_488" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_490" n="HIAT:w" s="T136">Mat</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_493" n="HIAT:w" s="T137">negend</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_496" n="HIAT:w" s="T138">eǯalgwak</ts>
                  <nts id="Seg_497" n="HIAT:ip">:</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_500" n="HIAT:w" s="T139">Nʼarg</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_503" n="HIAT:w" s="T140">tae</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_506" n="HIAT:w" s="T141">kuralešpeš</ts>
                  <nts id="Seg_507" n="HIAT:ip">!</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_509" n="sc" s="T143">
               <ts e="T147" id="Seg_511" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_513" n="HIAT:w" s="T143">Mat</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_516" n="HIAT:w" s="T144">nem</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_519" n="HIAT:w" s="T145">kuranna</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_522" n="HIAT:w" s="T146">kaʒete</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_525" n="sc" s="T148">
               <ts e="T153" id="Seg_527" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_529" n="HIAT:w" s="T148">A</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_532" n="HIAT:w" s="T149">man</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_535" n="HIAT:w" s="T150">eǯalgwak</ts>
                  <nts id="Seg_536" n="HIAT:ip">:</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_539" n="HIAT:w" s="T151">Ɨg</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_542" n="HIAT:w" s="T152">kaʒeš</ts>
                  <nts id="Seg_543" n="HIAT:ip">!</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_546" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_548" n="HIAT:w" s="T153">Pomalenʼku</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_551" n="HIAT:w" s="T154">čaǯeš</ts>
                  <nts id="Seg_552" n="HIAT:ip">!</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_554" n="sc" s="T156">
               <ts e="T169" id="Seg_556" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_558" n="HIAT:w" s="T156">Onek</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_561" n="HIAT:w" s="T157">nɨŋgak</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_564" n="HIAT:w" s="T158">taperʼ</ts>
                  <nts id="Seg_565" n="HIAT:ip">,</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_568" n="HIAT:w" s="T159">manembak</ts>
                  <nts id="Seg_569" n="HIAT:ip">,</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_572" n="HIAT:w" s="T160">taben</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_575" n="HIAT:w" s="T161">čenčak</ts>
                  <nts id="Seg_576" n="HIAT:ip">:</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_579" n="HIAT:w" s="T162">Tat</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_582" n="HIAT:w" s="T163">kajko</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_585" n="HIAT:w" s="T164">nɨka</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_588" n="HIAT:w" s="T165">töhand</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_592" n="HIAT:w" s="T166">man</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_595" n="HIAT:w" s="T167">wattond</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_598" n="HIAT:w" s="T168">parond</ts>
                  <nts id="Seg_599" n="HIAT:ip">?</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T178" id="Seg_601" n="sc" s="T170">
               <ts e="T178" id="Seg_603" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_605" n="HIAT:w" s="T170">Vsjo</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_608" n="HIAT:w" s="T171">nɨŋgak</ts>
                  <nts id="Seg_609" n="HIAT:ip">,</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_612" n="HIAT:w" s="T172">manembak</ts>
                  <nts id="Seg_613" n="HIAT:ip">,</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_616" n="HIAT:w" s="T173">a</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_619" n="HIAT:w" s="T174">tab</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_622" n="HIAT:w" s="T175">naj</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_625" n="HIAT:w" s="T176">mašek</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_628" n="HIAT:w" s="T177">manemba</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T182" id="Seg_631" n="sc" s="T179">
               <ts e="T182" id="Seg_633" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_635" n="HIAT:w" s="T179">Lüdka</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_638" n="HIAT:w" s="T180">kuranna</ts>
                  <nts id="Seg_639" n="HIAT:ip">,</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_642" n="HIAT:w" s="T181">merda</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_645" n="sc" s="T183">
               <ts e="T196" id="Seg_647" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_649" n="HIAT:w" s="T183">Mat</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_652" n="HIAT:w" s="T184">taperʼ</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_655" n="HIAT:w" s="T185">našaŋ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_658" n="HIAT:w" s="T186">nɨŋgak</ts>
                  <nts id="Seg_659" n="HIAT:ip">,</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_662" n="HIAT:w" s="T187">naj</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_665" n="HIAT:w" s="T188">üpoǯäk</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_668" n="HIAT:w" s="T189">aj</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_671" n="HIAT:w" s="T190">manǯekwam</ts>
                  <nts id="Seg_672" n="HIAT:ip">:</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_675" n="HIAT:w" s="T191">nɨŋga</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_678" n="HIAT:w" s="T192">aj</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_681" n="HIAT:w" s="T193">mašek</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_684" n="HIAT:w" s="T194">manemba</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_687" n="HIAT:w" s="T195">qorɣ</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_690" n="sc" s="T197">
               <ts e="T209" id="Seg_692" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_694" n="HIAT:w" s="T197">Mat</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_697" n="HIAT:w" s="T198">taperʼ</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_700" n="HIAT:w" s="T199">medrageɣ</ts>
                  <nts id="Seg_701" n="HIAT:ip">,</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_704" n="HIAT:w" s="T200">paneǯikwap</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_707" n="HIAT:w" s="T201">nɨka</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_710" n="HIAT:w" s="T202">elle</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_713" n="HIAT:w" s="T203">kwella</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_716" n="HIAT:w" s="T204">vsʼakij</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_720" n="HIAT:w" s="T205">a</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_723" n="HIAT:w" s="T206">tab</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_726" n="HIAT:w" s="T207">nödla</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_728" n="HIAT:ip">—</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_731" n="HIAT:w" s="T208">uǯegɨgu</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_734" n="sc" s="T210">
               <ts e="T226" id="Seg_736" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_738" n="HIAT:w" s="T210">Lüdka</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_741" n="HIAT:w" s="T211">naj</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_744" n="HIAT:w" s="T212">kɨbɨʒok</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_747" n="HIAT:w" s="T213">čʼem-to</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_750" n="HIAT:w" s="T214">kajda</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_753" n="HIAT:w" s="T215">katolgut</ts>
                  <nts id="Seg_754" n="HIAT:ip">,</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_757" n="HIAT:w" s="T216">nɨka</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_760" n="HIAT:w" s="T217">wes</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_763" n="HIAT:w" s="T218">tɨ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_766" n="HIAT:w" s="T219">mɨxe</ts>
                  <nts id="Seg_767" n="HIAT:ip">,</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_770" n="HIAT:w" s="T220">punurxe</ts>
                  <nts id="Seg_771" n="HIAT:ip">,</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_774" n="HIAT:w" s="T221">katolbat</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_778" n="HIAT:w" s="T222">naj</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_781" n="HIAT:w" s="T223">kuralǯekwa</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_783" n="HIAT:ip">—</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_786" n="HIAT:w" s="T224">mašek</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_789" n="HIAT:w" s="T225">manemba</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T233" id="Seg_792" n="sc" s="T227">
               <ts e="T233" id="Seg_794" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_796" n="HIAT:w" s="T227">Nʼar</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_799" n="HIAT:w" s="T228">čʼeresʼ</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_802" n="HIAT:w" s="T229">oni</ts>
                  <nts id="Seg_803" n="HIAT:ip">:</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_806" n="HIAT:w" s="T230">kanak</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_810" n="HIAT:w" s="T231">bauškan</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_813" n="HIAT:w" s="T232">parkundaj</ts>
                  <nts id="Seg_814" n="HIAT:ip">.</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T239" id="Seg_816" n="sc" s="T234">
               <ts e="T239" id="Seg_818" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_820" n="HIAT:w" s="T234">Potom</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_823" n="HIAT:w" s="T235">tae</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_826" n="HIAT:w" s="T236">pušlaj</ts>
                  <nts id="Seg_827" n="HIAT:ip">,</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_830" n="HIAT:w" s="T237">parkwaj</ts>
                  <nts id="Seg_831" n="HIAT:ip">:</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_834" n="HIAT:w" s="T238">Bauškə</ts>
                  <nts id="Seg_835" n="HIAT:ip">!</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T246" id="Seg_837" n="sc" s="T240">
               <ts e="T246" id="Seg_839" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_841" n="HIAT:w" s="T240">A</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_844" n="HIAT:w" s="T241">bauška</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_847" n="HIAT:w" s="T242">čenča</ts>
                  <nts id="Seg_848" n="HIAT:ip">:</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_851" n="HIAT:w" s="T243">Kaj</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_854" n="HIAT:w" s="T244">parkwa</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_857" n="HIAT:w" s="T245">načat</ts>
                  <nts id="Seg_858" n="HIAT:ip">?</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_860" n="sc" s="T247">
               <ts e="T256" id="Seg_862" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_864" n="HIAT:w" s="T247">Teperʼ</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_867" n="HIAT:w" s="T248">mat</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_870" n="HIAT:w" s="T249">Lüdkan</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_873" n="HIAT:w" s="T250">čenčak</ts>
                  <nts id="Seg_874" n="HIAT:ip">:</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_877" n="HIAT:w" s="T251">Wandol</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_880" n="HIAT:w" s="T252">kajko</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_883" n="HIAT:w" s="T253">nɨlʼǯik</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_886" n="HIAT:w" s="T254">čaɣ</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_889" n="HIAT:w" s="T255">čanemba</ts>
                  <nts id="Seg_890" n="HIAT:ip">?</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T267" id="Seg_892" n="sc" s="T257">
               <ts e="T267" id="Seg_894" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_896" n="HIAT:w" s="T257">A</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_899" n="HIAT:w" s="T258">man</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_902" n="HIAT:w" s="T259">nem</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_905" n="HIAT:w" s="T260">eǯalgwa</ts>
                  <nts id="Seg_906" n="HIAT:ip">:</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_909" n="HIAT:w" s="T261">A</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_912" n="HIAT:w" s="T262">tan</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_915" n="HIAT:w" s="T263">naj</ts>
                  <nts id="Seg_916" n="HIAT:ip">,</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_919" n="HIAT:w" s="T264">goworit</ts>
                  <nts id="Seg_920" n="HIAT:ip">,</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_923" n="HIAT:w" s="T265">wandol</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_926" n="HIAT:w" s="T266">čagejja</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T272" id="Seg_929" n="sc" s="T268">
               <ts e="T272" id="Seg_931" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_933" n="HIAT:w" s="T268">No</ts>
                  <nts id="Seg_934" n="HIAT:ip">,</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_937" n="HIAT:w" s="T269">mi</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_940" n="HIAT:w" s="T270">uruk</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_943" n="HIAT:w" s="T271">kɨčwannaj</ts>
                  <nts id="Seg_944" n="HIAT:ip">.</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T278" id="Seg_946" n="sc" s="T273">
               <ts e="T275" id="Seg_948" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_950" n="HIAT:w" s="T273">Bauškan</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_953" n="HIAT:w" s="T274">töaj</ts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_957" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_959" n="HIAT:w" s="T275">Bauška</ts>
                  <nts id="Seg_960" n="HIAT:ip">:</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_963" n="HIAT:w" s="T276">Kaj</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_966" n="HIAT:w" s="T277">katawand</ts>
                  <nts id="Seg_967" n="HIAT:ip">?</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T285" id="Seg_969" n="sc" s="T279">
               <ts e="T285" id="Seg_971" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_973" n="HIAT:w" s="T279">Manǯak</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_976" n="HIAT:w" s="T280">mat</ts>
                  <nts id="Seg_977" n="HIAT:ip">,</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_980" n="HIAT:w" s="T281">mi</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_983" n="HIAT:w" s="T282">qorɣop</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_986" n="HIAT:w" s="T283">ombi</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_989" n="HIAT:w" s="T284">konǯerxaj</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T289" id="Seg_992" n="sc" s="T286">
               <ts e="T289" id="Seg_994" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_996" n="HIAT:w" s="T286">Šinni</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_999" n="HIAT:w" s="T287">taw</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1002" n="HIAT:w" s="T288">kurdaj</ts>
                  <nts id="Seg_1003" n="HIAT:ip">.</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_1005" n="sc" s="T290">
               <ts e="T305" id="Seg_1007" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1009" n="HIAT:w" s="T290">A</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1012" n="HIAT:w" s="T291">bauška</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1015" n="HIAT:w" s="T292">nɨlʼǯik</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1018" n="HIAT:w" s="T293">eːǯalgwa</ts>
                  <nts id="Seg_1019" n="HIAT:ip">:</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1022" n="HIAT:w" s="T294">A</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1025" n="HIAT:w" s="T295">nau</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1028" n="HIAT:w" s="T296">kaj</ts>
                  <nts id="Seg_1029" n="HIAT:ip">,</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1032" n="HIAT:w" s="T297">goworit</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1036" n="HIAT:w" s="T298">aud</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1039" n="HIAT:w" s="T299">man</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1042" n="HIAT:w" s="T300">sat</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1045" n="HIAT:w" s="T301">nɨka</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1048" n="HIAT:w" s="T302">tadembat</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1052" n="HIAT:w" s="T303">wattət</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1055" n="HIAT:w" s="T304">xajɣet</ts>
                  <nts id="Seg_1056" n="HIAT:ip">?</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T4" id="Seg_1058" n="sc" s="T1">
               <ts e="T2" id="Seg_1060" n="e" s="T1">Kanduk </ts>
               <ts e="T3" id="Seg_1062" n="e" s="T2">qorɣop </ts>
               <ts e="T4" id="Seg_1064" n="e" s="T3">qonǯerxat. </ts>
            </ts>
            <ts e="T10" id="Seg_1065" n="sc" s="T5">
               <ts e="T6" id="Seg_1067" n="e" s="T5">Mat </ts>
               <ts e="T306" id="Seg_1069" n="e" s="T6">näːn </ts>
               <ts e="T7" id="Seg_1071" n="e" s="T306">opte </ts>
               <ts e="T8" id="Seg_1073" n="e" s="T7">čobortko </ts>
               <ts e="T9" id="Seg_1075" n="e" s="T8">kwessaj, </ts>
               <ts e="T307" id="Seg_1077" n="e" s="T9">Lüdmilan </ts>
               <ts e="T10" id="Seg_1079" n="e" s="T307">opte. </ts>
            </ts>
            <ts e="T15" id="Seg_1080" n="sc" s="T11">
               <ts e="T12" id="Seg_1082" n="e" s="T11">A </ts>
               <ts e="T13" id="Seg_1084" n="e" s="T12">mi </ts>
               <ts e="T14" id="Seg_1086" n="e" s="T13">abɨʒejkat </ts>
               <ts e="T15" id="Seg_1088" n="e" s="T14">qwen. </ts>
            </ts>
            <ts e="T22" id="Seg_1089" n="sc" s="T16">
               <ts e="T17" id="Seg_1091" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_1093" n="e" s="T17">bauška </ts>
               <ts e="T19" id="Seg_1095" n="e" s="T18">Warwara </ts>
               <ts e="T20" id="Seg_1097" n="e" s="T19">načʼan </ts>
               <ts e="T21" id="Seg_1099" n="e" s="T20">(wa-) </ts>
               <ts e="T22" id="Seg_1101" n="e" s="T21">wargɨxaɣ. </ts>
            </ts>
            <ts e="T25" id="Seg_1102" n="sc" s="T23">
               <ts e="T24" id="Seg_1104" n="e" s="T23">Üdenne </ts>
               <ts e="T25" id="Seg_1106" n="e" s="T24">qwessaj. </ts>
            </ts>
            <ts e="T33" id="Seg_1107" n="sc" s="T26">
               <ts e="T27" id="Seg_1109" n="e" s="T26">Taper </ts>
               <ts e="T28" id="Seg_1111" n="e" s="T27">töwaj — </ts>
               <ts e="T29" id="Seg_1113" n="e" s="T28">bauška </ts>
               <ts e="T30" id="Seg_1115" n="e" s="T29">Warwara </ts>
               <ts e="T31" id="Seg_1117" n="e" s="T30">nʼejtuwaɣ </ts>
               <ts e="T32" id="Seg_1119" n="e" s="T31">na </ts>
               <ts e="T33" id="Seg_1121" n="e" s="T32">astanopte. </ts>
            </ts>
            <ts e="T48" id="Seg_1122" n="sc" s="T34">
               <ts e="T35" id="Seg_1124" n="e" s="T34">A </ts>
               <ts e="T36" id="Seg_1126" n="e" s="T35">mat </ts>
               <ts e="T37" id="Seg_1128" n="e" s="T36">Lüdkan </ts>
               <ts e="T38" id="Seg_1130" n="e" s="T37">čenčak: </ts>
               <ts e="T39" id="Seg_1132" n="e" s="T38">Manǯoɣa — </ts>
               <ts e="T40" id="Seg_1134" n="e" s="T39">tawčitə </ts>
               <ts e="T41" id="Seg_1136" n="e" s="T40">qwellaj, </ts>
               <ts e="T308" id="Seg_1138" n="e" s="T41">tawčit </ts>
               <ts e="T42" id="Seg_1140" n="e" s="T308">taw </ts>
               <ts e="T43" id="Seg_1142" n="e" s="T42">belekaɨt </ts>
               <ts e="T44" id="Seg_1144" n="e" s="T43">qwellaj. </ts>
               <ts e="T45" id="Seg_1146" n="e" s="T44">Tɨndə, </ts>
               <ts e="T46" id="Seg_1148" n="e" s="T45">moʒet, </ts>
               <ts e="T47" id="Seg_1150" n="e" s="T46">čobor </ts>
               <ts e="T48" id="Seg_1152" n="e" s="T47">kolajhe. </ts>
            </ts>
            <ts e="T52" id="Seg_1153" n="sc" s="T49">
               <ts e="T50" id="Seg_1155" n="e" s="T49">Taper </ts>
               <ts e="T51" id="Seg_1157" n="e" s="T50">üpoǯäj, </ts>
               <ts e="T52" id="Seg_1159" n="e" s="T51">čaːǯaj. </ts>
            </ts>
            <ts e="T60" id="Seg_1160" n="sc" s="T53">
               <ts e="T54" id="Seg_1162" n="e" s="T53">A </ts>
               <ts e="T55" id="Seg_1164" n="e" s="T54">Lüdka </ts>
               <ts e="T56" id="Seg_1166" n="e" s="T55">meka </ts>
               <ts e="T309" id="Seg_1168" n="e" s="T56">čenča, </ts>
               <ts e="T57" id="Seg_1170" n="e" s="T309">kawarit: </ts>
               <ts e="T310" id="Seg_1172" n="e" s="T57">“Tɨndə, </ts>
               <ts e="T58" id="Seg_1174" n="e" s="T310">kawarit, </ts>
               <ts e="T59" id="Seg_1176" n="e" s="T58">qajqoj </ts>
               <ts e="T60" id="Seg_1178" n="e" s="T59">kwajakumba.” </ts>
            </ts>
            <ts e="T73" id="Seg_1179" n="sc" s="T61">
               <ts e="T62" id="Seg_1181" n="e" s="T61">A </ts>
               <ts e="T63" id="Seg_1183" n="e" s="T62">mat </ts>
               <ts e="T64" id="Seg_1185" n="e" s="T63">taben </ts>
               <ts e="T65" id="Seg_1187" n="e" s="T64">narošnak </ts>
               <ts e="T66" id="Seg_1189" n="e" s="T65">čenčak, </ts>
               <ts e="T67" id="Seg_1191" n="e" s="T66">štobɨ </ts>
               <ts e="T68" id="Seg_1193" n="e" s="T67">tab </ts>
               <ts e="T69" id="Seg_1195" n="e" s="T68">ɨgɨ </ts>
               <ts e="T70" id="Seg_1197" n="e" s="T69">tanulend, </ts>
               <ts e="T71" id="Seg_1199" n="e" s="T70">što </ts>
               <ts e="T72" id="Seg_1201" n="e" s="T71">urop </ts>
               <ts e="T73" id="Seg_1203" n="e" s="T72">kwajakumbɨ. </ts>
            </ts>
            <ts e="T79" id="Seg_1204" n="sc" s="T74">
               <ts e="T75" id="Seg_1206" n="e" s="T74">Mat </ts>
               <ts e="T311" id="Seg_1208" n="e" s="T75">čenčak: </ts>
               <ts e="T76" id="Seg_1210" n="e" s="T311">“A, </ts>
               <ts e="T77" id="Seg_1212" n="e" s="T76">taw </ts>
               <ts e="T78" id="Seg_1214" n="e" s="T77">bauškamd </ts>
               <ts e="T79" id="Seg_1216" n="e" s="T78">qwajakumundaɣ. </ts>
            </ts>
            <ts e="T86" id="Seg_1217" n="sc" s="T80">
               <ts e="T81" id="Seg_1219" n="e" s="T80">Čaǯaj, </ts>
               <ts e="T83" id="Seg_1221" n="e" s="T81">kalammut </ts>
               <ts e="T84" id="Seg_1223" n="e" s="T83">elʼǯʼä </ts>
               <ts e="T85" id="Seg_1225" n="e" s="T84">šɨtə </ts>
               <ts e="T86" id="Seg_1227" n="e" s="T85">par. </ts>
            </ts>
            <ts e="T94" id="Seg_1228" n="sc" s="T87">
               <ts e="T88" id="Seg_1230" n="e" s="T87">Taperʼ </ts>
               <ts e="T89" id="Seg_1232" n="e" s="T88">man </ts>
               <ts e="T90" id="Seg_1234" n="e" s="T89">üngolʼǯembap, </ts>
               <ts e="T91" id="Seg_1236" n="e" s="T90">mute </ts>
               <ts e="T92" id="Seg_1238" n="e" s="T91">nep </ts>
               <ts e="T93" id="Seg_1240" n="e" s="T92">aː </ts>
               <ts e="T94" id="Seg_1242" n="e" s="T93">ügla. </ts>
            </ts>
            <ts e="T96" id="Seg_1243" n="sc" s="T95">
               <ts e="T96" id="Seg_1245" n="e" s="T95">Čaǯaj. </ts>
            </ts>
            <ts e="T108" id="Seg_1246" n="sc" s="T97">
               <ts e="T98" id="Seg_1248" n="e" s="T97">((…)) </ts>
               <ts e="T99" id="Seg_1250" n="e" s="T98">meka </ts>
               <ts e="T100" id="Seg_1252" n="e" s="T99">nem </ts>
               <ts e="T101" id="Seg_1254" n="e" s="T100">kurda: </ts>
               <ts e="T102" id="Seg_1256" n="e" s="T101">“Qorɣa, </ts>
               <ts e="T103" id="Seg_1258" n="e" s="T102">tona!” </ts>
               <ts e="T104" id="Seg_1260" n="e" s="T103">Mat </ts>
               <ts e="T105" id="Seg_1262" n="e" s="T104">tabən </ts>
               <ts e="T106" id="Seg_1264" n="e" s="T105">barʒak: </ts>
               <ts e="T107" id="Seg_1266" n="e" s="T106">Kaj </ts>
               <ts e="T108" id="Seg_1268" n="e" s="T107">qorɣa? </ts>
            </ts>
            <ts e="T116" id="Seg_1269" n="sc" s="T109">
               <ts e="T110" id="Seg_1271" n="e" s="T109">Enne </ts>
               <ts e="T111" id="Seg_1273" n="e" s="T110">manǯeǯäk — </ts>
               <ts e="T112" id="Seg_1275" n="e" s="T111">sabɨlʼ, </ts>
               <ts e="T113" id="Seg_1277" n="e" s="T112">qorɣ </ts>
               <ts e="T114" id="Seg_1279" n="e" s="T113">nɨŋga, </ts>
               <ts e="T115" id="Seg_1281" n="e" s="T114">miʒe </ts>
               <ts e="T116" id="Seg_1283" n="e" s="T115">manemba. </ts>
            </ts>
            <ts e="T126" id="Seg_1284" n="sc" s="T117">
               <ts e="T118" id="Seg_1286" n="e" s="T117">Metra, </ts>
               <ts e="T119" id="Seg_1288" n="e" s="T118">šed </ts>
               <ts e="T120" id="Seg_1290" n="e" s="T119">metra, </ts>
               <ts e="T121" id="Seg_1292" n="e" s="T120">nagur </ts>
               <ts e="T122" id="Seg_1294" n="e" s="T121">metra </ts>
               <ts e="T123" id="Seg_1296" n="e" s="T122">čʼomb </ts>
               <ts e="T124" id="Seg_1298" n="e" s="T123">nɨŋga, </ts>
               <ts e="T125" id="Seg_1300" n="e" s="T124">miʒe </ts>
               <ts e="T126" id="Seg_1302" n="e" s="T125">manemba. </ts>
            </ts>
            <ts e="T130" id="Seg_1303" n="sc" s="T127">
               <ts e="T128" id="Seg_1305" n="e" s="T127">Mat </ts>
               <ts e="T129" id="Seg_1307" n="e" s="T128">toʒe </ts>
               <ts e="T130" id="Seg_1309" n="e" s="T129">kɨčwannak. </ts>
            </ts>
            <ts e="T135" id="Seg_1310" n="sc" s="T131">
               <ts e="T132" id="Seg_1312" n="e" s="T131">Tärbak: </ts>
               <ts e="T133" id="Seg_1314" n="e" s="T132">Luče </ts>
               <ts e="T134" id="Seg_1316" n="e" s="T133">maʒek </ts>
               <ts e="T135" id="Seg_1318" n="e" s="T134">orallend. </ts>
            </ts>
            <ts e="T142" id="Seg_1319" n="sc" s="T136">
               <ts e="T137" id="Seg_1321" n="e" s="T136">Mat </ts>
               <ts e="T138" id="Seg_1323" n="e" s="T137">negend </ts>
               <ts e="T139" id="Seg_1325" n="e" s="T138">eǯalgwak: </ts>
               <ts e="T140" id="Seg_1327" n="e" s="T139">Nʼarg </ts>
               <ts e="T141" id="Seg_1329" n="e" s="T140">tae </ts>
               <ts e="T142" id="Seg_1331" n="e" s="T141">kuralešpeš! </ts>
            </ts>
            <ts e="T147" id="Seg_1332" n="sc" s="T143">
               <ts e="T144" id="Seg_1334" n="e" s="T143">Mat </ts>
               <ts e="T145" id="Seg_1336" n="e" s="T144">nem </ts>
               <ts e="T146" id="Seg_1338" n="e" s="T145">kuranna </ts>
               <ts e="T147" id="Seg_1340" n="e" s="T146">kaʒete. </ts>
            </ts>
            <ts e="T155" id="Seg_1341" n="sc" s="T148">
               <ts e="T149" id="Seg_1343" n="e" s="T148">A </ts>
               <ts e="T150" id="Seg_1345" n="e" s="T149">man </ts>
               <ts e="T151" id="Seg_1347" n="e" s="T150">eǯalgwak: </ts>
               <ts e="T152" id="Seg_1349" n="e" s="T151">Ɨg </ts>
               <ts e="T153" id="Seg_1351" n="e" s="T152">kaʒeš! </ts>
               <ts e="T154" id="Seg_1353" n="e" s="T153">Pomalenʼku </ts>
               <ts e="T155" id="Seg_1355" n="e" s="T154">čaǯeš! </ts>
            </ts>
            <ts e="T169" id="Seg_1356" n="sc" s="T156">
               <ts e="T157" id="Seg_1358" n="e" s="T156">Onek </ts>
               <ts e="T158" id="Seg_1360" n="e" s="T157">nɨŋgak </ts>
               <ts e="T159" id="Seg_1362" n="e" s="T158">taperʼ, </ts>
               <ts e="T160" id="Seg_1364" n="e" s="T159">manembak, </ts>
               <ts e="T161" id="Seg_1366" n="e" s="T160">taben </ts>
               <ts e="T162" id="Seg_1368" n="e" s="T161">čenčak: </ts>
               <ts e="T163" id="Seg_1370" n="e" s="T162">Tat </ts>
               <ts e="T164" id="Seg_1372" n="e" s="T163">kajko </ts>
               <ts e="T165" id="Seg_1374" n="e" s="T164">nɨka </ts>
               <ts e="T166" id="Seg_1376" n="e" s="T165">töhand, </ts>
               <ts e="T167" id="Seg_1378" n="e" s="T166">man </ts>
               <ts e="T168" id="Seg_1380" n="e" s="T167">wattond </ts>
               <ts e="T169" id="Seg_1382" n="e" s="T168">parond? </ts>
            </ts>
            <ts e="T178" id="Seg_1383" n="sc" s="T170">
               <ts e="T171" id="Seg_1385" n="e" s="T170">Vsjo </ts>
               <ts e="T172" id="Seg_1387" n="e" s="T171">nɨŋgak, </ts>
               <ts e="T173" id="Seg_1389" n="e" s="T172">manembak, </ts>
               <ts e="T174" id="Seg_1391" n="e" s="T173">a </ts>
               <ts e="T175" id="Seg_1393" n="e" s="T174">tab </ts>
               <ts e="T176" id="Seg_1395" n="e" s="T175">naj </ts>
               <ts e="T177" id="Seg_1397" n="e" s="T176">mašek </ts>
               <ts e="T178" id="Seg_1399" n="e" s="T177">manemba. </ts>
            </ts>
            <ts e="T182" id="Seg_1400" n="sc" s="T179">
               <ts e="T180" id="Seg_1402" n="e" s="T179">Lüdka </ts>
               <ts e="T181" id="Seg_1404" n="e" s="T180">kuranna, </ts>
               <ts e="T182" id="Seg_1406" n="e" s="T181">merda. </ts>
            </ts>
            <ts e="T196" id="Seg_1407" n="sc" s="T183">
               <ts e="T184" id="Seg_1409" n="e" s="T183">Mat </ts>
               <ts e="T185" id="Seg_1411" n="e" s="T184">taperʼ </ts>
               <ts e="T186" id="Seg_1413" n="e" s="T185">našaŋ </ts>
               <ts e="T187" id="Seg_1415" n="e" s="T186">nɨŋgak, </ts>
               <ts e="T188" id="Seg_1417" n="e" s="T187">naj </ts>
               <ts e="T189" id="Seg_1419" n="e" s="T188">üpoǯäk </ts>
               <ts e="T190" id="Seg_1421" n="e" s="T189">aj </ts>
               <ts e="T191" id="Seg_1423" n="e" s="T190">manǯekwam: </ts>
               <ts e="T192" id="Seg_1425" n="e" s="T191">nɨŋga </ts>
               <ts e="T193" id="Seg_1427" n="e" s="T192">aj </ts>
               <ts e="T194" id="Seg_1429" n="e" s="T193">mašek </ts>
               <ts e="T195" id="Seg_1431" n="e" s="T194">manemba </ts>
               <ts e="T196" id="Seg_1433" n="e" s="T195">qorɣ. </ts>
            </ts>
            <ts e="T209" id="Seg_1434" n="sc" s="T197">
               <ts e="T198" id="Seg_1436" n="e" s="T197">Mat </ts>
               <ts e="T199" id="Seg_1438" n="e" s="T198">taperʼ </ts>
               <ts e="T200" id="Seg_1440" n="e" s="T199">medrageɣ, </ts>
               <ts e="T201" id="Seg_1442" n="e" s="T200">paneǯikwap </ts>
               <ts e="T202" id="Seg_1444" n="e" s="T201">nɨka </ts>
               <ts e="T203" id="Seg_1446" n="e" s="T202">elle </ts>
               <ts e="T204" id="Seg_1448" n="e" s="T203">kwella </ts>
               <ts e="T205" id="Seg_1450" n="e" s="T204">vsʼakij, </ts>
               <ts e="T206" id="Seg_1452" n="e" s="T205">a </ts>
               <ts e="T207" id="Seg_1454" n="e" s="T206">tab </ts>
               <ts e="T208" id="Seg_1456" n="e" s="T207">nödla — </ts>
               <ts e="T209" id="Seg_1458" n="e" s="T208">uǯegɨgu. </ts>
            </ts>
            <ts e="T226" id="Seg_1459" n="sc" s="T210">
               <ts e="T211" id="Seg_1461" n="e" s="T210">Lüdka </ts>
               <ts e="T212" id="Seg_1463" n="e" s="T211">naj </ts>
               <ts e="T213" id="Seg_1465" n="e" s="T212">kɨbɨʒok </ts>
               <ts e="T214" id="Seg_1467" n="e" s="T213">čʼem-to </ts>
               <ts e="T215" id="Seg_1469" n="e" s="T214">kajda </ts>
               <ts e="T216" id="Seg_1471" n="e" s="T215">katolgut, </ts>
               <ts e="T217" id="Seg_1473" n="e" s="T216">nɨka </ts>
               <ts e="T218" id="Seg_1475" n="e" s="T217">wes </ts>
               <ts e="T219" id="Seg_1477" n="e" s="T218">tɨ </ts>
               <ts e="T220" id="Seg_1479" n="e" s="T219">mɨxe, </ts>
               <ts e="T221" id="Seg_1481" n="e" s="T220">punurxe, </ts>
               <ts e="T222" id="Seg_1483" n="e" s="T221">katolbat, </ts>
               <ts e="T223" id="Seg_1485" n="e" s="T222">naj </ts>
               <ts e="T224" id="Seg_1487" n="e" s="T223">kuralǯekwa — </ts>
               <ts e="T225" id="Seg_1489" n="e" s="T224">mašek </ts>
               <ts e="T226" id="Seg_1491" n="e" s="T225">manemba. </ts>
            </ts>
            <ts e="T233" id="Seg_1492" n="sc" s="T227">
               <ts e="T228" id="Seg_1494" n="e" s="T227">Nʼar </ts>
               <ts e="T229" id="Seg_1496" n="e" s="T228">čʼeresʼ </ts>
               <ts e="T230" id="Seg_1498" n="e" s="T229">oni: </ts>
               <ts e="T231" id="Seg_1500" n="e" s="T230">kanak, </ts>
               <ts e="T232" id="Seg_1502" n="e" s="T231">bauškan </ts>
               <ts e="T233" id="Seg_1504" n="e" s="T232">parkundaj. </ts>
            </ts>
            <ts e="T239" id="Seg_1505" n="sc" s="T234">
               <ts e="T235" id="Seg_1507" n="e" s="T234">Potom </ts>
               <ts e="T236" id="Seg_1509" n="e" s="T235">tae </ts>
               <ts e="T237" id="Seg_1511" n="e" s="T236">pušlaj, </ts>
               <ts e="T238" id="Seg_1513" n="e" s="T237">parkwaj: </ts>
               <ts e="T239" id="Seg_1515" n="e" s="T238">Bauškə! </ts>
            </ts>
            <ts e="T246" id="Seg_1516" n="sc" s="T240">
               <ts e="T241" id="Seg_1518" n="e" s="T240">A </ts>
               <ts e="T242" id="Seg_1520" n="e" s="T241">bauška </ts>
               <ts e="T243" id="Seg_1522" n="e" s="T242">čenča: </ts>
               <ts e="T244" id="Seg_1524" n="e" s="T243">Kaj </ts>
               <ts e="T245" id="Seg_1526" n="e" s="T244">parkwa </ts>
               <ts e="T246" id="Seg_1528" n="e" s="T245">načat? </ts>
            </ts>
            <ts e="T256" id="Seg_1529" n="sc" s="T247">
               <ts e="T248" id="Seg_1531" n="e" s="T247">Teperʼ </ts>
               <ts e="T249" id="Seg_1533" n="e" s="T248">mat </ts>
               <ts e="T250" id="Seg_1535" n="e" s="T249">Lüdkan </ts>
               <ts e="T251" id="Seg_1537" n="e" s="T250">čenčak: </ts>
               <ts e="T252" id="Seg_1539" n="e" s="T251">Wandol </ts>
               <ts e="T253" id="Seg_1541" n="e" s="T252">kajko </ts>
               <ts e="T254" id="Seg_1543" n="e" s="T253">nɨlʼǯik </ts>
               <ts e="T255" id="Seg_1545" n="e" s="T254">čaɣ </ts>
               <ts e="T256" id="Seg_1547" n="e" s="T255">čanemba? </ts>
            </ts>
            <ts e="T267" id="Seg_1548" n="sc" s="T257">
               <ts e="T258" id="Seg_1550" n="e" s="T257">A </ts>
               <ts e="T259" id="Seg_1552" n="e" s="T258">man </ts>
               <ts e="T260" id="Seg_1554" n="e" s="T259">nem </ts>
               <ts e="T261" id="Seg_1556" n="e" s="T260">eǯalgwa: </ts>
               <ts e="T262" id="Seg_1558" n="e" s="T261">A </ts>
               <ts e="T263" id="Seg_1560" n="e" s="T262">tan </ts>
               <ts e="T264" id="Seg_1562" n="e" s="T263">naj, </ts>
               <ts e="T265" id="Seg_1564" n="e" s="T264">goworit, </ts>
               <ts e="T266" id="Seg_1566" n="e" s="T265">wandol </ts>
               <ts e="T267" id="Seg_1568" n="e" s="T266">čagejja. </ts>
            </ts>
            <ts e="T272" id="Seg_1569" n="sc" s="T268">
               <ts e="T269" id="Seg_1571" n="e" s="T268">No, </ts>
               <ts e="T270" id="Seg_1573" n="e" s="T269">mi </ts>
               <ts e="T271" id="Seg_1575" n="e" s="T270">uruk </ts>
               <ts e="T272" id="Seg_1577" n="e" s="T271">kɨčwannaj. </ts>
            </ts>
            <ts e="T278" id="Seg_1578" n="sc" s="T273">
               <ts e="T274" id="Seg_1580" n="e" s="T273">Bauškan </ts>
               <ts e="T275" id="Seg_1582" n="e" s="T274">töaj. </ts>
               <ts e="T276" id="Seg_1584" n="e" s="T275">Bauška: </ts>
               <ts e="T277" id="Seg_1586" n="e" s="T276">Kaj </ts>
               <ts e="T278" id="Seg_1588" n="e" s="T277">katawand? </ts>
            </ts>
            <ts e="T285" id="Seg_1589" n="sc" s="T279">
               <ts e="T280" id="Seg_1591" n="e" s="T279">Manǯak </ts>
               <ts e="T281" id="Seg_1593" n="e" s="T280">mat, </ts>
               <ts e="T282" id="Seg_1595" n="e" s="T281">mi </ts>
               <ts e="T283" id="Seg_1597" n="e" s="T282">qorɣop </ts>
               <ts e="T284" id="Seg_1599" n="e" s="T283">ombi </ts>
               <ts e="T285" id="Seg_1601" n="e" s="T284">konǯerxaj. </ts>
            </ts>
            <ts e="T289" id="Seg_1602" n="sc" s="T286">
               <ts e="T287" id="Seg_1604" n="e" s="T286">Šinni </ts>
               <ts e="T288" id="Seg_1606" n="e" s="T287">taw </ts>
               <ts e="T289" id="Seg_1608" n="e" s="T288">kurdaj. </ts>
            </ts>
            <ts e="T305" id="Seg_1609" n="sc" s="T290">
               <ts e="T291" id="Seg_1611" n="e" s="T290">A </ts>
               <ts e="T292" id="Seg_1613" n="e" s="T291">bauška </ts>
               <ts e="T293" id="Seg_1615" n="e" s="T292">nɨlʼǯik </ts>
               <ts e="T294" id="Seg_1617" n="e" s="T293">eːǯalgwa: </ts>
               <ts e="T295" id="Seg_1619" n="e" s="T294">A </ts>
               <ts e="T296" id="Seg_1621" n="e" s="T295">nau </ts>
               <ts e="T297" id="Seg_1623" n="e" s="T296">kaj, </ts>
               <ts e="T298" id="Seg_1625" n="e" s="T297">goworit, </ts>
               <ts e="T299" id="Seg_1627" n="e" s="T298">aud </ts>
               <ts e="T300" id="Seg_1629" n="e" s="T299">man </ts>
               <ts e="T301" id="Seg_1631" n="e" s="T300">sat </ts>
               <ts e="T302" id="Seg_1633" n="e" s="T301">nɨka </ts>
               <ts e="T303" id="Seg_1635" n="e" s="T302">tadembat, </ts>
               <ts e="T304" id="Seg_1637" n="e" s="T303">wattət </ts>
               <ts e="T305" id="Seg_1639" n="e" s="T304">xajɣet? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1640" s="T1">YIF_196X_Bear_nar.001 (001)</ta>
            <ta e="T10" id="Seg_1641" s="T5">YIF_196X_Bear_nar.002 (002)</ta>
            <ta e="T15" id="Seg_1642" s="T11">YIF_196X_Bear_nar.003 (003)</ta>
            <ta e="T22" id="Seg_1643" s="T16">YIF_196X_Bear_nar.004 (004)</ta>
            <ta e="T25" id="Seg_1644" s="T23">YIF_196X_Bear_nar.005 (005)</ta>
            <ta e="T33" id="Seg_1645" s="T26">YIF_196X_Bear_nar.006 (006)</ta>
            <ta e="T44" id="Seg_1646" s="T34">YIF_196X_Bear_nar.007 (007)</ta>
            <ta e="T48" id="Seg_1647" s="T44">YIF_196X_Bear_nar.008 (008)</ta>
            <ta e="T52" id="Seg_1648" s="T49">YIF_196X_Bear_nar.009 (009)</ta>
            <ta e="T60" id="Seg_1649" s="T53">YIF_196X_Bear_nar.010 (010)</ta>
            <ta e="T73" id="Seg_1650" s="T61">YIF_196X_Bear_nar.011 (011)</ta>
            <ta e="T79" id="Seg_1651" s="T74">YIF_196X_Bear_nar.012 (012)</ta>
            <ta e="T86" id="Seg_1652" s="T80">YIF_196X_Bear_nar.013 (013)</ta>
            <ta e="T94" id="Seg_1653" s="T87">YIF_196X_Bear_nar.014 (014)</ta>
            <ta e="T96" id="Seg_1654" s="T95">YIF_196X_Bear_nar.015 (015)</ta>
            <ta e="T103" id="Seg_1655" s="T97">YIF_196X_Bear_nar.016 (016)</ta>
            <ta e="T108" id="Seg_1656" s="T103">YIF_196X_Bear_nar.017 (017)</ta>
            <ta e="T116" id="Seg_1657" s="T109">YIF_196X_Bear_nar.018 (018)</ta>
            <ta e="T126" id="Seg_1658" s="T117">YIF_196X_Bear_nar.019 (019)</ta>
            <ta e="T130" id="Seg_1659" s="T127">YIF_196X_Bear_nar.020 (020)</ta>
            <ta e="T135" id="Seg_1660" s="T131">YIF_196X_Bear_nar.021 (021)</ta>
            <ta e="T142" id="Seg_1661" s="T136">YIF_196X_Bear_nar.022 (022)</ta>
            <ta e="T147" id="Seg_1662" s="T143">YIF_196X_Bear_nar.023 (023)</ta>
            <ta e="T153" id="Seg_1663" s="T148">YIF_196X_Bear_nar.024 (024)</ta>
            <ta e="T155" id="Seg_1664" s="T153">YIF_196X_Bear_nar.025 (025)</ta>
            <ta e="T169" id="Seg_1665" s="T156">YIF_196X_Bear_nar.026 (026)</ta>
            <ta e="T178" id="Seg_1666" s="T170">YIF_196X_Bear_nar.027 (027)</ta>
            <ta e="T182" id="Seg_1667" s="T179">YIF_196X_Bear_nar.028 (028)</ta>
            <ta e="T196" id="Seg_1668" s="T183">YIF_196X_Bear_nar.029 (029)</ta>
            <ta e="T209" id="Seg_1669" s="T197">YIF_196X_Bear_nar.030 (030)</ta>
            <ta e="T226" id="Seg_1670" s="T210">YIF_196X_Bear_nar.031 (031)</ta>
            <ta e="T233" id="Seg_1671" s="T227">YIF_196X_Bear_nar.032 (032)</ta>
            <ta e="T239" id="Seg_1672" s="T234">YIF_196X_Bear_nar.033 (033)</ta>
            <ta e="T246" id="Seg_1673" s="T240">YIF_196X_Bear_nar.034 (034)</ta>
            <ta e="T256" id="Seg_1674" s="T247">YIF_196X_Bear_nar.035 (035)</ta>
            <ta e="T267" id="Seg_1675" s="T257">YIF_196X_Bear_nar.036 (036)</ta>
            <ta e="T272" id="Seg_1676" s="T268">YIF_196X_Bear_nar.037 (037)</ta>
            <ta e="T275" id="Seg_1677" s="T273">YIF_196X_Bear_nar.038 (038)</ta>
            <ta e="T278" id="Seg_1678" s="T275">YIF_196X_Bear_nar.039 (039)</ta>
            <ta e="T285" id="Seg_1679" s="T279">YIF_196X_Bear_nar.040 (040)</ta>
            <ta e="T289" id="Seg_1680" s="T286">YIF_196X_Bear_nar.041 (041)</ta>
            <ta e="T305" id="Seg_1681" s="T290">YIF_196X_Bear_nar.042 (042)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1682" s="T1">Канду́к ӄо́рӷоп ӄонджэрҳа́т.</ta>
            <ta e="T10" id="Seg_1683" s="T5">Мат нэ́мунтэ ҷобортко́ квэ́ссай, Людми́ланоптэ.</ta>
            <ta e="T15" id="Seg_1684" s="T11">А ми абыжэ́йкатэ квэ́ннут.</ta>
            <ta e="T22" id="Seg_1685" s="T16">А баушка Варвара нача́н варгыха́ӷ.</ta>
            <ta e="T25" id="Seg_1686" s="T23">Ӱдэннэ квэ́ссай.</ta>
            <ta e="T33" id="Seg_1687" s="T26">Теперь тӧай — баушка Варвара не́туаӷ на остановке.</ta>
            <ta e="T44" id="Seg_1688" s="T34">А мат Людкан ҷэ́нҷак: «Манджо́ӷ — тавчи́д квэ́лай, тавчи́д пеле́каӷыт квэ́лай.</ta>
            <ta e="T48" id="Seg_1689" s="T44">Тынд, может, ҷо́бор кола́йҳе.</ta>
            <ta e="T52" id="Seg_1690" s="T49">Тапе́рь ӱподжӓй. Ҷа́джай.</ta>
            <ta e="T60" id="Seg_1691" s="T53">А Людка ме́ка ҷэ́нҷа: «Тынд ка́йгой-то квая́кумба».</ta>
            <ta e="T73" id="Seg_1692" s="T61">А мат та́бэн наро́шнак ҷэ́нҷак, чтобы таб ыг тӓну́лемд, что ху́руп квая́кумбы.</ta>
            <ta e="T79" id="Seg_1693" s="T74">Мат ҷэ́нҷак: «Тав ба́ушкамд квая́кумбындаӷ».</ta>
            <ta e="T86" id="Seg_1694" s="T80">Ҷа́джай, а таб ментэ́джӓ шэд пар.</ta>
            <ta e="T94" id="Seg_1695" s="T87">Теперь мат ӱнгольджемба́п, мутэ́ нэп а̄ ӱгла́.</ta>
            <ta e="T96" id="Seg_1696" s="T95">Ҷа́джай.</ta>
            <ta e="T103" id="Seg_1697" s="T97">Ме́ка нэм курда́: «Кай, ӄо́рӷа?»</ta>
            <ta e="T116" id="Seg_1698" s="T109">Эннэ́ манджэ́джӓк — са́быль, ӄорӷ ныңга́, ми́жэ манэмба́.</ta>
            <ta e="T126" id="Seg_1699" s="T117">Ме́тра, шэд ме́тра, на́гур ме́тра чомб ныңга́, ми́жэ манэмба́.</ta>
            <ta e="T130" id="Seg_1700" s="T127">Мат то́же кычва́ннак.</ta>
            <ta e="T135" id="Seg_1701" s="T131">Тӓрба́к: «Лу́че ма́жэк ора́лленд»</ta>
            <ta e="T142" id="Seg_1702" s="T136">Мат нэ́гэнд э́джалгвак: «Нярг та́э кура́лешпеш!»</ta>
            <ta e="T147" id="Seg_1703" s="T143">Мат нэм кура́нна ка́жэтэ.</ta>
            <ta e="T153" id="Seg_1704" s="T148">А ман э́джалгвак: «Ыг ка́жэш!</ta>
            <ta e="T155" id="Seg_1705" s="T153">Помале́ньку ҷа́джэш!»</ta>
            <ta e="T169" id="Seg_1706" s="T156">Онэ́к ныңга́к тапе́рь, манэмба́к, та́бэн ҷэ́нҷак: «Тат кайко́ ныка́ тӧҳанд, ман ватто́нд па́ронд?»</ta>
            <ta e="T178" id="Seg_1707" s="T170">Всё ныңга́к, манэмба́к, а таб най ма́шэк манэмба́.</ta>
            <ta e="T182" id="Seg_1708" s="T179">Кура́нна. Мерда́.</ta>
            <ta e="T196" id="Seg_1709" s="T183">Мат тапе́рь наша́ң ныңга́к, най ӱподжӓк ай манджэква́м: ныңга́ ай ма́шэк манэмба́ ӄорӷ.</ta>
            <ta e="T209" id="Seg_1710" s="T197">Мат тапе́рь медра́геӷ, панэ́джиквап ныка́ элле́ кве́лла вся́кий, а таб нӧдла — уджэ́гыгу.</ta>
            <ta e="T226" id="Seg_1711" s="T210">Людка най кыбы́жок чем-то ка́йда като́лгут, ны́ка вес ты мы́ҳе, пу́нурҳе, като́лбат. Най кура́лджэква — ма́шэк манэмба́.</ta>
            <ta e="T233" id="Seg_1712" s="T227">Няр чересь они: кана́к, ба́ушкан паркунда́й.</ta>
            <ta e="T239" id="Seg_1713" s="T234">Пото́м таэ пу́шлай, парква́й: «Баушкэ́!!!»</ta>
            <ta e="T246" id="Seg_1714" s="T240">А ба́ушка ҷэ́нҷа: «Кай парква́ нача́т?»</ta>
            <ta e="T256" id="Seg_1715" s="T247">Теперь мат Лю́дкан ҷэ́нҷак: «Вандо́л кайко́ ны́льджик чаӷ ҷанэмба́?»</ta>
            <ta e="T267" id="Seg_1716" s="T257">А ман нэм э́джалгва: «А тан най, говорит, вандо́л ча́гейя».</ta>
            <ta e="T272" id="Seg_1717" s="T268">Но, мi уру́к кычва́ннай.</ta>
            <ta e="T275" id="Seg_1718" s="T273">Бау́шкан тӧай.</ta>
            <ta e="T278" id="Seg_1719" s="T275">Ба́ушка: «Кай ката́ванд?»</ta>
            <ta e="T285" id="Seg_1720" s="T279">Манджа́к мат, мi ӄо́рӷоп омби́ конджэрха́й».</ta>
            <ta e="T289" id="Seg_1721" s="T286">Шинни́ тав курда́й.</ta>
            <ta e="T305" id="Seg_1722" s="T290">А ба́ушка ны́льджик э̄джалгва: «А нау кай, говорит, ауд ман сат ныка́ тадэмба́т, ва́ттэт ха́йӷэт?»</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_1723" s="T1">Kanduk qorɣop qonǯerxat.</ta>
            <ta e="T10" id="Seg_1724" s="T5">Mat nemunte čobortko kwessaj, Lüdmilanopte.</ta>
            <ta e="T15" id="Seg_1725" s="T11">A mi abɨʒejkate kwennut.</ta>
            <ta e="T22" id="Seg_1726" s="T16">A bauška Warwara načʼan wargɨxaɣ.</ta>
            <ta e="T25" id="Seg_1727" s="T23">Üdenne kwessaj.</ta>
            <ta e="T33" id="Seg_1728" s="T26">Teperʼ töaj — bauška Warwara netuaɣ na ostanowke.</ta>
            <ta e="T44" id="Seg_1729" s="T34">A mat Lüdkan čenčak: «Manǯoɣ — tawčʼid kwelaj, tawčʼid pelekaɣɨt kwelaj.</ta>
            <ta e="T48" id="Seg_1730" s="T44">Tɨnd, moʒet, čobor kolajxe.</ta>
            <ta e="T52" id="Seg_1731" s="T49">Taperʼ üpoǯäj. Čaǯaj.</ta>
            <ta e="T60" id="Seg_1732" s="T53">A Lüdka meka čenča: «Tɨnd kajgoj-to kwajakumba».</ta>
            <ta e="T73" id="Seg_1733" s="T61">A mat taben narošnak čenčak, čʼtobɨ tab ɨg tänulemd, čʼto xurup kwajakumbɨ.</ta>
            <ta e="T79" id="Seg_1734" s="T74">Mat čenčak: «Taw bauškamd kwajakumbɨndaɣ».</ta>
            <ta e="T86" id="Seg_1735" s="T80">Čaǯaj, a tab menteǯä šed par.</ta>
            <ta e="T94" id="Seg_1736" s="T87">Teperʼ mat üngolʼǯembap, mute nep aː ügla.</ta>
            <ta e="T96" id="Seg_1737" s="T95">Čaǯaj.</ta>
            <ta e="T103" id="Seg_1738" s="T97">Meka nem kurda: «Kaj, qorɣa?»</ta>
            <ta e="T116" id="Seg_1739" s="T109">Enne manǯeǯäk — sabɨlʼ, qorɣ nɨŋga, miʒe manemba.</ta>
            <ta e="T126" id="Seg_1740" s="T117">Metra, šed metra, nagur metra čʼomb nɨŋga, miʒe manemba.</ta>
            <ta e="T130" id="Seg_1741" s="T127">Mat toʒe kɨčwannak.</ta>
            <ta e="T135" id="Seg_1742" s="T131">Tärbak: «Lučʼe maʒek orallend»</ta>
            <ta e="T142" id="Seg_1743" s="T136">Mat negend eǯalgwak: «Nʼarg tae kuralešpeš!»</ta>
            <ta e="T147" id="Seg_1744" s="T143">Mat nem kuranna kaʒete.</ta>
            <ta e="T153" id="Seg_1745" s="T148">A man eǯalgwak: «Ɨg kaʒeš!</ta>
            <ta e="T155" id="Seg_1746" s="T153">Pomalenʼku čaǯeš!»</ta>
            <ta e="T169" id="Seg_1747" s="T156">Onek nɨŋgak taperʼ, manembak, taben čenčak: «Tat kajko nɨka töxand, man wattond parond?»</ta>
            <ta e="T178" id="Seg_1748" s="T170">Vsё nɨŋgak, manembak, a tab naj mašek manemba.</ta>
            <ta e="T182" id="Seg_1749" s="T179">Kuranna. Merda.</ta>
            <ta e="T196" id="Seg_1750" s="T183">Mat taperʼ našaŋ nɨŋgak, naj üpoǯäk aj manǯekwam: nɨŋga aj mašek manemba qorɣ.</ta>
            <ta e="T209" id="Seg_1751" s="T197">Mat taperʼ medrageɣ, paneǯikwap nɨka elle kwella vsʼakij, a tab nödla — uǯegɨgu.</ta>
            <ta e="T226" id="Seg_1752" s="T210">Lüdka naj kɨbɨʒok čʼem-to kajda katolgut, nɨka wes tɨ mɨxe, punurxe, katolbat. Naj kuralǯekwa — mašek manemba.</ta>
            <ta e="T233" id="Seg_1753" s="T227">Nʼar čʼeresʼ oni: kanak, bauškan parkundaj.</ta>
            <ta e="T239" id="Seg_1754" s="T234">Potom tae pušlaj, parkwaj: «Bauške!!!»</ta>
            <ta e="T246" id="Seg_1755" s="T240">A bauška čenča: «Kaj parkwa načʼat?»</ta>
            <ta e="T256" id="Seg_1756" s="T247">Teperʼ mat Lüdkan čenčak: «Wandol kajko nɨlʼǯik čaɣ čanemba?»</ta>
            <ta e="T267" id="Seg_1757" s="T257">A man nem eǯalgwa: «A tan naj, goworit, wandol čagejja».</ta>
            <ta e="T272" id="Seg_1758" s="T268">No, mi uruk kɨčʼwannaj.</ta>
            <ta e="T275" id="Seg_1759" s="T273">Bauškan töaj.</ta>
            <ta e="T278" id="Seg_1760" s="T275">Bauška: «Kaj katawand?»</ta>
            <ta e="T285" id="Seg_1761" s="T279">Manǯak mat, mi qorɣop ombi konǯerxaj».</ta>
            <ta e="T289" id="Seg_1762" s="T286">Šinni taw kurdaj.</ta>
            <ta e="T305" id="Seg_1763" s="T290">A bauška nɨlʼǯik eːǯalgwa: «A nau kaj, goworit, aud man sat nɨka tadembat, wattet xajɣet?»</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1764" s="T1">Kanduk qorɣop qonǯerxat. </ta>
            <ta e="T10" id="Seg_1765" s="T5">Mat näːn opte čobortko kwessaj, Lüdmilan opte. </ta>
            <ta e="T15" id="Seg_1766" s="T11">A mi abɨʒejkat qwen. </ta>
            <ta e="T22" id="Seg_1767" s="T16">A bauška Warwara načʼan (wa-) wargɨxaɣ. </ta>
            <ta e="T25" id="Seg_1768" s="T23">Üdenne qwessaj. </ta>
            <ta e="T33" id="Seg_1769" s="T26">Taper töwaj — bauška Warwara nʼejtuwaɣ na astanopte. </ta>
            <ta e="T44" id="Seg_1770" s="T34">A mat Lüdkan čenčak: Manǯoɣa — tawčitə qwellaj, tawčit taw belekaɨt qwellaj. </ta>
            <ta e="T48" id="Seg_1771" s="T44">Tɨndə, moʒet, čobor kolajhe. </ta>
            <ta e="T52" id="Seg_1772" s="T49">Taper üpoǯäj, čaːǯaj. </ta>
            <ta e="T60" id="Seg_1773" s="T53">A Lüdka meka čenča, kawarit: “Tɨndə, kawarit, qajqoj kwajakumba.” </ta>
            <ta e="T73" id="Seg_1774" s="T61">A mat taben narošnak čenčak, štobɨ tab ɨgɨ tanulend, što urop kwajakumbɨ. </ta>
            <ta e="T79" id="Seg_1775" s="T74">Mat čenčak: “A, taw bauškamd qwajakumundaɣ. </ta>
            <ta e="T86" id="Seg_1776" s="T80">Čaǯaj, kalammut elʼǯʼä šɨtə par. </ta>
            <ta e="T94" id="Seg_1777" s="T87">Taperʼ man üngolʼǯembap, mute nep aː ügla. </ta>
            <ta e="T96" id="Seg_1778" s="T95">Čaǯaj. </ta>
            <ta e="T103" id="Seg_1779" s="T97">((…)) meka nem kurda: “Qorɣa, tona!” </ta>
            <ta e="T108" id="Seg_1780" s="T103">Mat tabən barʒak: Qaj qorɣa? </ta>
            <ta e="T116" id="Seg_1781" s="T109">Enne manǯeǯäk — sabɨlʼ, qorɣ nɨŋga, miʒe manemba. </ta>
            <ta e="T126" id="Seg_1782" s="T117">Metra, šed metra, nagur metra čʼomb nɨŋga, miʒe manemba. </ta>
            <ta e="T130" id="Seg_1783" s="T127">Mat toʒe kɨčwannak. </ta>
            <ta e="T135" id="Seg_1784" s="T131">Tärbak: Luče maʒek orallend. </ta>
            <ta e="T142" id="Seg_1785" s="T136">Mat negend eǯalgwak: Nʼarg tae kuralešpeš! </ta>
            <ta e="T147" id="Seg_1786" s="T143">Mat nem kuranna kaʒete. </ta>
            <ta e="T153" id="Seg_1787" s="T148">A man eǯalgwak: Ɨg kaʒeš! </ta>
            <ta e="T155" id="Seg_1788" s="T153">Pomalenʼku čaǯeš! </ta>
            <ta e="T169" id="Seg_1789" s="T156">Onek nɨŋgak taperʼ, manembak, taben čenčak: Tat kajko nɨka töhand, man wattond parond? </ta>
            <ta e="T178" id="Seg_1790" s="T170">Vsjo nɨŋgak, manembak, a tab naj mašek manemba. </ta>
            <ta e="T182" id="Seg_1791" s="T179">Lüdka kuranna, merda. </ta>
            <ta e="T196" id="Seg_1792" s="T183">Mat taperʼ našaŋ nɨŋgak, naj üpoǯäk aj manǯekwam: nɨŋga aj mašek manemba qorɣ. </ta>
            <ta e="T209" id="Seg_1793" s="T197">Mat taperʼ medrageɣ, paneǯikwap nɨka elle kwella vsʼakij, a tab nödla — uǯegɨgu. </ta>
            <ta e="T226" id="Seg_1794" s="T210">Lüdka naj kɨbɨʒok čʼem-to kajda katolgut, nɨka wes tɨ mɨxe, punurxe, katolbat, naj kuralǯekwa — mašek manemba. </ta>
            <ta e="T233" id="Seg_1795" s="T227">Nʼar čʼeresʼ oni: kanak, bauškan parkundaj. </ta>
            <ta e="T239" id="Seg_1796" s="T234">Potom tae pušlaj, parkwaj: Bauškə! </ta>
            <ta e="T246" id="Seg_1797" s="T240">A bauška čenča: Kaj parkwa načat? </ta>
            <ta e="T256" id="Seg_1798" s="T247">Teperʼ mat Lüdkan čenčak: Wandol kajko nɨlʼǯik čaɣ čanemba? </ta>
            <ta e="T267" id="Seg_1799" s="T257">A man nem eǯalgwa: A tan naj, goworit, wandol čagejja. </ta>
            <ta e="T272" id="Seg_1800" s="T268">No, mi uruk kɨčwannaj. </ta>
            <ta e="T275" id="Seg_1801" s="T273">Bauškan töaj. </ta>
            <ta e="T278" id="Seg_1802" s="T275">Bauška: Kaj katawand? </ta>
            <ta e="T285" id="Seg_1803" s="T279">Manǯak mat, mi qorɣop ombi konǯerxaj. </ta>
            <ta e="T289" id="Seg_1804" s="T286">Šinni taw kurdaj. </ta>
            <ta e="T305" id="Seg_1805" s="T290">A bauška nɨlʼǯik eːǯalgwa: A nau kaj, goworit, aud man sat nɨka tadembat, wattət xajɣet? </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1806" s="T1">kanduk</ta>
            <ta e="T3" id="Seg_1807" s="T2">qorɣo-p</ta>
            <ta e="T4" id="Seg_1808" s="T3">qonǯer-xa-t</ta>
            <ta e="T6" id="Seg_1809" s="T5">mat</ta>
            <ta e="T306" id="Seg_1810" s="T6">näː-n</ta>
            <ta e="T7" id="Seg_1811" s="T306">opte</ta>
            <ta e="T8" id="Seg_1812" s="T7">čobor-tko</ta>
            <ta e="T9" id="Seg_1813" s="T8">kwes-sa-j</ta>
            <ta e="T307" id="Seg_1814" s="T9">Lüdmila-n</ta>
            <ta e="T10" id="Seg_1815" s="T307">opte</ta>
            <ta e="T12" id="Seg_1816" s="T11">a</ta>
            <ta e="T13" id="Seg_1817" s="T12">mi</ta>
            <ta e="T14" id="Seg_1818" s="T13">abɨʒejkat</ta>
            <ta e="T15" id="Seg_1819" s="T14">qwen</ta>
            <ta e="T17" id="Seg_1820" s="T16">a</ta>
            <ta e="T18" id="Seg_1821" s="T17">bauška</ta>
            <ta e="T19" id="Seg_1822" s="T18">Warwara</ta>
            <ta e="T20" id="Seg_1823" s="T19">načʼa-n</ta>
            <ta e="T22" id="Seg_1824" s="T21">wargɨ-xa-ɣ</ta>
            <ta e="T24" id="Seg_1825" s="T23">üdenne</ta>
            <ta e="T25" id="Seg_1826" s="T24">qwes-sa-j</ta>
            <ta e="T27" id="Seg_1827" s="T26">taper</ta>
            <ta e="T28" id="Seg_1828" s="T27">tö-wa-j</ta>
            <ta e="T29" id="Seg_1829" s="T28">bauška</ta>
            <ta e="T30" id="Seg_1830" s="T29">Warwara</ta>
            <ta e="T31" id="Seg_1831" s="T30">nʼejtu-wa-ɣ</ta>
            <ta e="T32" id="Seg_1832" s="T31">na</ta>
            <ta e="T33" id="Seg_1833" s="T32">astanopte</ta>
            <ta e="T35" id="Seg_1834" s="T34">a</ta>
            <ta e="T36" id="Seg_1835" s="T35">mat</ta>
            <ta e="T37" id="Seg_1836" s="T36">Lüdka-n</ta>
            <ta e="T38" id="Seg_1837" s="T37">čenča-k</ta>
            <ta e="T39" id="Seg_1838" s="T38">manǯo-ɣa</ta>
            <ta e="T40" id="Seg_1839" s="T39">tawčitə</ta>
            <ta e="T41" id="Seg_1840" s="T40">qwel-la-j</ta>
            <ta e="T308" id="Seg_1841" s="T41">tawčit</ta>
            <ta e="T42" id="Seg_1842" s="T308">taw</ta>
            <ta e="T43" id="Seg_1843" s="T42">beleka-ɨt</ta>
            <ta e="T44" id="Seg_1844" s="T43">qwel-la-j</ta>
            <ta e="T45" id="Seg_1845" s="T44">tɨndə</ta>
            <ta e="T46" id="Seg_1846" s="T45">moʒet</ta>
            <ta e="T47" id="Seg_1847" s="T46">čobor</ta>
            <ta e="T48" id="Seg_1848" s="T47">ko-la-j-he</ta>
            <ta e="T50" id="Seg_1849" s="T49">taper</ta>
            <ta e="T51" id="Seg_1850" s="T50">üpoǯä-j</ta>
            <ta e="T52" id="Seg_1851" s="T51">čaːǯa-j</ta>
            <ta e="T54" id="Seg_1852" s="T53">a</ta>
            <ta e="T55" id="Seg_1853" s="T54">Lüdka</ta>
            <ta e="T56" id="Seg_1854" s="T55">meka</ta>
            <ta e="T309" id="Seg_1855" s="T56">čenča</ta>
            <ta e="T57" id="Seg_1856" s="T309">kawarit</ta>
            <ta e="T310" id="Seg_1857" s="T57">tɨndə</ta>
            <ta e="T58" id="Seg_1858" s="T310">kawarit</ta>
            <ta e="T59" id="Seg_1859" s="T58">qaj-qoj</ta>
            <ta e="T60" id="Seg_1860" s="T59">kwaja-ku-mbɨ</ta>
            <ta e="T62" id="Seg_1861" s="T61">a</ta>
            <ta e="T63" id="Seg_1862" s="T62">mat</ta>
            <ta e="T64" id="Seg_1863" s="T63">tab-e-n</ta>
            <ta e="T65" id="Seg_1864" s="T64">narošna-k</ta>
            <ta e="T66" id="Seg_1865" s="T65">čenča-k</ta>
            <ta e="T67" id="Seg_1866" s="T66">štobɨ</ta>
            <ta e="T68" id="Seg_1867" s="T67">tab</ta>
            <ta e="T69" id="Seg_1868" s="T68">ɨgɨ</ta>
            <ta e="T70" id="Seg_1869" s="T69">tanu-le-nd</ta>
            <ta e="T71" id="Seg_1870" s="T70">što</ta>
            <ta e="T72" id="Seg_1871" s="T71">urop</ta>
            <ta e="T73" id="Seg_1872" s="T72">kwaja-ku-mbɨ</ta>
            <ta e="T75" id="Seg_1873" s="T74">mat</ta>
            <ta e="T311" id="Seg_1874" s="T75">čenča-k</ta>
            <ta e="T76" id="Seg_1875" s="T311">a</ta>
            <ta e="T77" id="Seg_1876" s="T76">taw</ta>
            <ta e="T78" id="Seg_1877" s="T77">bauška-m-d</ta>
            <ta e="T79" id="Seg_1878" s="T78">qwaja-ku-mu-nda-ɣ</ta>
            <ta e="T81" id="Seg_1879" s="T80">čaǯa-j</ta>
            <ta e="T83" id="Seg_1880" s="T81">kalammut</ta>
            <ta e="T84" id="Seg_1881" s="T83">elʼǯʼä</ta>
            <ta e="T85" id="Seg_1882" s="T84">šɨtə</ta>
            <ta e="T86" id="Seg_1883" s="T85">par</ta>
            <ta e="T88" id="Seg_1884" s="T87">taperʼ</ta>
            <ta e="T89" id="Seg_1885" s="T88">man</ta>
            <ta e="T90" id="Seg_1886" s="T89">üngo-lʼǯe-mba-p</ta>
            <ta e="T91" id="Seg_1887" s="T90">mute</ta>
            <ta e="T92" id="Seg_1888" s="T91">ne-p</ta>
            <ta e="T93" id="Seg_1889" s="T92">aː</ta>
            <ta e="T94" id="Seg_1890" s="T93">üg-la</ta>
            <ta e="T96" id="Seg_1891" s="T95">čaǯa-j</ta>
            <ta e="T99" id="Seg_1892" s="T98">meka</ta>
            <ta e="T100" id="Seg_1893" s="T99">ne-m</ta>
            <ta e="T101" id="Seg_1894" s="T100">kurda</ta>
            <ta e="T102" id="Seg_1895" s="T101">qorɣa</ta>
            <ta e="T103" id="Seg_1896" s="T102">tona</ta>
            <ta e="T104" id="Seg_1897" s="T103">mat</ta>
            <ta e="T105" id="Seg_1898" s="T104">tabə-n</ta>
            <ta e="T106" id="Seg_1899" s="T105">barʒa-k</ta>
            <ta e="T107" id="Seg_1900" s="T106">kaj</ta>
            <ta e="T108" id="Seg_1901" s="T107">qorɣa</ta>
            <ta e="T110" id="Seg_1902" s="T109">inne</ta>
            <ta e="T111" id="Seg_1903" s="T110">manǯe-ǯä-k</ta>
            <ta e="T112" id="Seg_1904" s="T111">sabɨlʼ</ta>
            <ta e="T113" id="Seg_1905" s="T112">qorɣ</ta>
            <ta e="T114" id="Seg_1906" s="T113">nɨŋ-ga</ta>
            <ta e="T115" id="Seg_1907" s="T114">miʒe</ta>
            <ta e="T116" id="Seg_1908" s="T115">mane-mba</ta>
            <ta e="T118" id="Seg_1909" s="T117">metra</ta>
            <ta e="T119" id="Seg_1910" s="T118">šed</ta>
            <ta e="T120" id="Seg_1911" s="T119">metra</ta>
            <ta e="T121" id="Seg_1912" s="T120">nagur</ta>
            <ta e="T122" id="Seg_1913" s="T121">metra</ta>
            <ta e="T123" id="Seg_1914" s="T122">čʼomb</ta>
            <ta e="T124" id="Seg_1915" s="T123">nɨŋ-ga</ta>
            <ta e="T125" id="Seg_1916" s="T124">miʒe</ta>
            <ta e="T126" id="Seg_1917" s="T125">mane-mba</ta>
            <ta e="T128" id="Seg_1918" s="T127">mat</ta>
            <ta e="T129" id="Seg_1919" s="T128">toʒe</ta>
            <ta e="T130" id="Seg_1920" s="T129">kɨčwan-na-k</ta>
            <ta e="T132" id="Seg_1921" s="T131">tär-ba-k</ta>
            <ta e="T133" id="Seg_1922" s="T132">luče</ta>
            <ta e="T134" id="Seg_1923" s="T133">maʒek</ta>
            <ta e="T135" id="Seg_1924" s="T134">oral-le-nd</ta>
            <ta e="T137" id="Seg_1925" s="T136">mat</ta>
            <ta e="T138" id="Seg_1926" s="T137">ne-gend</ta>
            <ta e="T139" id="Seg_1927" s="T138">eǯa-l-g-wa-k</ta>
            <ta e="T140" id="Seg_1928" s="T139">nʼarg</ta>
            <ta e="T141" id="Seg_1929" s="T140">tae</ta>
            <ta e="T142" id="Seg_1930" s="T141">kur-a-le-šp-eš</ta>
            <ta e="T144" id="Seg_1931" s="T143">mat</ta>
            <ta e="T145" id="Seg_1932" s="T144">ne-m</ta>
            <ta e="T146" id="Seg_1933" s="T145">kur-ɨ-nna</ta>
            <ta e="T147" id="Seg_1934" s="T146">kaʒete</ta>
            <ta e="T149" id="Seg_1935" s="T148">a</ta>
            <ta e="T150" id="Seg_1936" s="T149">man</ta>
            <ta e="T151" id="Seg_1937" s="T150">eǯa-l-g-wa-k</ta>
            <ta e="T152" id="Seg_1938" s="T151">ɨg</ta>
            <ta e="T153" id="Seg_1939" s="T152">kaʒe-š</ta>
            <ta e="T154" id="Seg_1940" s="T153">pomalenʼku</ta>
            <ta e="T155" id="Seg_1941" s="T154">čaǯe-š</ta>
            <ta e="T157" id="Seg_1942" s="T156">onek</ta>
            <ta e="T158" id="Seg_1943" s="T157">nɨŋ-ga-k</ta>
            <ta e="T159" id="Seg_1944" s="T158">taperʼ</ta>
            <ta e="T160" id="Seg_1945" s="T159">mane-mba-k</ta>
            <ta e="T161" id="Seg_1946" s="T160">tab-e-n</ta>
            <ta e="T162" id="Seg_1947" s="T161">čenča-k</ta>
            <ta e="T163" id="Seg_1948" s="T162">tat</ta>
            <ta e="T164" id="Seg_1949" s="T163">kaj-ko</ta>
            <ta e="T165" id="Seg_1950" s="T164">nɨka</ta>
            <ta e="T166" id="Seg_1951" s="T165">tö-ha-nd</ta>
            <ta e="T167" id="Seg_1952" s="T166">man</ta>
            <ta e="T168" id="Seg_1953" s="T167">watto-n-d</ta>
            <ta e="T169" id="Seg_1954" s="T168">par-nd</ta>
            <ta e="T171" id="Seg_1955" s="T170">vsjo</ta>
            <ta e="T172" id="Seg_1956" s="T171">nɨŋ-ga-k</ta>
            <ta e="T173" id="Seg_1957" s="T172">mane-mba-k</ta>
            <ta e="T174" id="Seg_1958" s="T173">a</ta>
            <ta e="T175" id="Seg_1959" s="T174">tab</ta>
            <ta e="T176" id="Seg_1960" s="T175">naj</ta>
            <ta e="T177" id="Seg_1961" s="T176">mašek</ta>
            <ta e="T178" id="Seg_1962" s="T177">mane-mba</ta>
            <ta e="T180" id="Seg_1963" s="T179">Lüdka</ta>
            <ta e="T181" id="Seg_1964" s="T180">kur-ɨ-nna</ta>
            <ta e="T182" id="Seg_1965" s="T181">merda</ta>
            <ta e="T184" id="Seg_1966" s="T183">mat</ta>
            <ta e="T185" id="Seg_1967" s="T184">taperʼ</ta>
            <ta e="T186" id="Seg_1968" s="T185">našaŋ</ta>
            <ta e="T187" id="Seg_1969" s="T186">nɨŋ-ga-k</ta>
            <ta e="T188" id="Seg_1970" s="T187">naj</ta>
            <ta e="T189" id="Seg_1971" s="T188">üpoǯä-k</ta>
            <ta e="T190" id="Seg_1972" s="T189">aj</ta>
            <ta e="T191" id="Seg_1973" s="T190">manǯe-k-wa-m</ta>
            <ta e="T192" id="Seg_1974" s="T191">nɨŋ-ga</ta>
            <ta e="T193" id="Seg_1975" s="T192">aj</ta>
            <ta e="T194" id="Seg_1976" s="T193">mašek</ta>
            <ta e="T195" id="Seg_1977" s="T194">mane-mba</ta>
            <ta e="T196" id="Seg_1978" s="T195">qorɣ</ta>
            <ta e="T198" id="Seg_1979" s="T197">mat</ta>
            <ta e="T199" id="Seg_1980" s="T198">taperʼ</ta>
            <ta e="T200" id="Seg_1981" s="T199">medra-ge-ɣ</ta>
            <ta e="T201" id="Seg_1982" s="T200">pan-e-ǯi-k-wa-p</ta>
            <ta e="T202" id="Seg_1983" s="T201">nɨka</ta>
            <ta e="T203" id="Seg_1984" s="T202">elle</ta>
            <ta e="T204" id="Seg_1985" s="T203">kwe-lla</ta>
            <ta e="T205" id="Seg_1986" s="T204">vsʼakij</ta>
            <ta e="T206" id="Seg_1987" s="T205">a</ta>
            <ta e="T207" id="Seg_1988" s="T206">tab</ta>
            <ta e="T208" id="Seg_1989" s="T207">nöd-la</ta>
            <ta e="T209" id="Seg_1990" s="T208">uǯe-gɨ-gu</ta>
            <ta e="T211" id="Seg_1991" s="T210">Lüdka</ta>
            <ta e="T212" id="Seg_1992" s="T211">naj</ta>
            <ta e="T213" id="Seg_1993" s="T212">kɨbɨʒo-k</ta>
            <ta e="T215" id="Seg_1994" s="T214">kaj-da</ta>
            <ta e="T216" id="Seg_1995" s="T215">katol-gu-t</ta>
            <ta e="T217" id="Seg_1996" s="T216">nɨka</ta>
            <ta e="T218" id="Seg_1997" s="T217">wes</ta>
            <ta e="T219" id="Seg_1998" s="T218">tɨ</ta>
            <ta e="T220" id="Seg_1999" s="T219">mɨ-xe</ta>
            <ta e="T221" id="Seg_2000" s="T220">punur-xe</ta>
            <ta e="T222" id="Seg_2001" s="T221">katol-ba-t</ta>
            <ta e="T223" id="Seg_2002" s="T222">naj</ta>
            <ta e="T224" id="Seg_2003" s="T223">kur-a-lǯe-k-wa</ta>
            <ta e="T225" id="Seg_2004" s="T224">mašek</ta>
            <ta e="T226" id="Seg_2005" s="T225">mane-mba</ta>
            <ta e="T228" id="Seg_2006" s="T227">nʼar</ta>
            <ta e="T229" id="Seg_2007" s="T228">čʼeresʼ</ta>
            <ta e="T230" id="Seg_2008" s="T229">oni</ta>
            <ta e="T231" id="Seg_2009" s="T230">kanak</ta>
            <ta e="T232" id="Seg_2010" s="T231">bauška-n</ta>
            <ta e="T233" id="Seg_2011" s="T232">parku-nda-j</ta>
            <ta e="T235" id="Seg_2012" s="T234">potom</ta>
            <ta e="T236" id="Seg_2013" s="T235">tae</ta>
            <ta e="T237" id="Seg_2014" s="T236">pušla-j</ta>
            <ta e="T238" id="Seg_2015" s="T237">park-wa-j</ta>
            <ta e="T239" id="Seg_2016" s="T238">bauškə</ta>
            <ta e="T241" id="Seg_2017" s="T240">a</ta>
            <ta e="T242" id="Seg_2018" s="T241">bauška</ta>
            <ta e="T243" id="Seg_2019" s="T242">čenča</ta>
            <ta e="T244" id="Seg_2020" s="T243">kaj</ta>
            <ta e="T245" id="Seg_2021" s="T244">park-wa</ta>
            <ta e="T246" id="Seg_2022" s="T245">nača-t</ta>
            <ta e="T248" id="Seg_2023" s="T247">teperʼ</ta>
            <ta e="T249" id="Seg_2024" s="T248">mat</ta>
            <ta e="T250" id="Seg_2025" s="T249">Lüdka-n</ta>
            <ta e="T251" id="Seg_2026" s="T250">čenča-k</ta>
            <ta e="T252" id="Seg_2027" s="T251">wando-l</ta>
            <ta e="T253" id="Seg_2028" s="T252">kaj-ko</ta>
            <ta e="T254" id="Seg_2029" s="T253">nɨlʼǯi-k</ta>
            <ta e="T255" id="Seg_2030" s="T254">čaɣ</ta>
            <ta e="T256" id="Seg_2031" s="T255">čane-mba</ta>
            <ta e="T258" id="Seg_2032" s="T257">a</ta>
            <ta e="T259" id="Seg_2033" s="T258">man</ta>
            <ta e="T260" id="Seg_2034" s="T259">ne-m</ta>
            <ta e="T261" id="Seg_2035" s="T260">eǯal-g-wa</ta>
            <ta e="T262" id="Seg_2036" s="T261">a</ta>
            <ta e="T263" id="Seg_2037" s="T262">tan</ta>
            <ta e="T264" id="Seg_2038" s="T263">naj</ta>
            <ta e="T266" id="Seg_2039" s="T265">wando-l</ta>
            <ta e="T267" id="Seg_2040" s="T266">čag-e-j-ja</ta>
            <ta e="T269" id="Seg_2041" s="T268">no</ta>
            <ta e="T270" id="Seg_2042" s="T269">mi</ta>
            <ta e="T271" id="Seg_2043" s="T270">ur-u-k</ta>
            <ta e="T272" id="Seg_2044" s="T271">kɨčwan-na-j</ta>
            <ta e="T274" id="Seg_2045" s="T273">bauška-n</ta>
            <ta e="T275" id="Seg_2046" s="T274">tö-a-j</ta>
            <ta e="T276" id="Seg_2047" s="T275">bauška</ta>
            <ta e="T277" id="Seg_2048" s="T276">kaj</ta>
            <ta e="T278" id="Seg_2049" s="T277">kata-wa-nd</ta>
            <ta e="T280" id="Seg_2050" s="T279">manǯa-k</ta>
            <ta e="T281" id="Seg_2051" s="T280">mat</ta>
            <ta e="T282" id="Seg_2052" s="T281">mi</ta>
            <ta e="T283" id="Seg_2053" s="T282">qorɣo-p</ta>
            <ta e="T284" id="Seg_2054" s="T283">ombi</ta>
            <ta e="T285" id="Seg_2055" s="T284">konǯer-xa-j</ta>
            <ta e="T287" id="Seg_2056" s="T286">šinni</ta>
            <ta e="T288" id="Seg_2057" s="T287">taw</ta>
            <ta e="T289" id="Seg_2058" s="T288">kur-da-j</ta>
            <ta e="T291" id="Seg_2059" s="T290">a</ta>
            <ta e="T292" id="Seg_2060" s="T291">bauška</ta>
            <ta e="T293" id="Seg_2061" s="T292">nɨlʼǯi-k</ta>
            <ta e="T294" id="Seg_2062" s="T293">eːǯal-g-wa</ta>
            <ta e="T295" id="Seg_2063" s="T294">a</ta>
            <ta e="T296" id="Seg_2064" s="T295">nau</ta>
            <ta e="T297" id="Seg_2065" s="T296">kaj</ta>
            <ta e="T299" id="Seg_2066" s="T298">au-d</ta>
            <ta e="T300" id="Seg_2067" s="T299">man</ta>
            <ta e="T301" id="Seg_2068" s="T300">sat</ta>
            <ta e="T302" id="Seg_2069" s="T301">nɨka</ta>
            <ta e="T303" id="Seg_2070" s="T302">tade-mba-t</ta>
            <ta e="T304" id="Seg_2071" s="T303">wattə-t</ta>
            <ta e="T305" id="Seg_2072" s="T304">xaj-ɣet</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_2073" s="T1">qanduk</ta>
            <ta e="T3" id="Seg_2074" s="T2">qorqɨ-p</ta>
            <ta e="T4" id="Seg_2075" s="T3">qonǯer-ŋɨ-dət</ta>
            <ta e="T6" id="Seg_2076" s="T5">man</ta>
            <ta e="T306" id="Seg_2077" s="T6">neː-n</ta>
            <ta e="T7" id="Seg_2078" s="T306">optɨ</ta>
            <ta e="T8" id="Seg_2079" s="T7">čoːbɛr-tqo</ta>
            <ta e="T9" id="Seg_2080" s="T8">qwän-sɨ-j</ta>
            <ta e="T307" id="Seg_2081" s="T9">Lüdmila-n</ta>
            <ta e="T10" id="Seg_2082" s="T307">optɨ</ta>
            <ta e="T12" id="Seg_2083" s="T11">a</ta>
            <ta e="T13" id="Seg_2084" s="T12">mi</ta>
            <ta e="T14" id="Seg_2085" s="T13">abɨʒejkat</ta>
            <ta e="T15" id="Seg_2086" s="T14">qwän</ta>
            <ta e="T17" id="Seg_2087" s="T16">a</ta>
            <ta e="T18" id="Seg_2088" s="T17">bauška</ta>
            <ta e="T19" id="Seg_2089" s="T18">Warwara</ta>
            <ta e="T20" id="Seg_2090" s="T19">nača-n</ta>
            <ta e="T22" id="Seg_2091" s="T21">wargɨ-sɨ-k</ta>
            <ta e="T24" id="Seg_2092" s="T23">üdenne</ta>
            <ta e="T25" id="Seg_2093" s="T24">qwän-sɨ-j</ta>
            <ta e="T27" id="Seg_2094" s="T26">taper</ta>
            <ta e="T28" id="Seg_2095" s="T27">töː-wa-j</ta>
            <ta e="T29" id="Seg_2096" s="T28">bauška</ta>
            <ta e="T30" id="Seg_2097" s="T29">Warwara</ta>
            <ta e="T31" id="Seg_2098" s="T30">nʼetu-wa-k</ta>
            <ta e="T32" id="Seg_2099" s="T31">na</ta>
            <ta e="T33" id="Seg_2100" s="T32">astanofke</ta>
            <ta e="T35" id="Seg_2101" s="T34">a</ta>
            <ta e="T36" id="Seg_2102" s="T35">man</ta>
            <ta e="T37" id="Seg_2103" s="T36">Lüdka-ni</ta>
            <ta e="T38" id="Seg_2104" s="T37">čenčɨ-k</ta>
            <ta e="T39" id="Seg_2105" s="T38">manǯu-äšɨk</ta>
            <ta e="T40" id="Seg_2106" s="T39">tawčid</ta>
            <ta e="T41" id="Seg_2107" s="T40">qwän-le-j</ta>
            <ta e="T308" id="Seg_2108" s="T41">tawčid</ta>
            <ta e="T42" id="Seg_2109" s="T308">taw</ta>
            <ta e="T43" id="Seg_2110" s="T42">pelka-un</ta>
            <ta e="T44" id="Seg_2111" s="T43">qwän-le-j</ta>
            <ta e="T45" id="Seg_2112" s="T44">tɨnd</ta>
            <ta e="T46" id="Seg_2113" s="T45">moʒət</ta>
            <ta e="T47" id="Seg_2114" s="T46">čoːbɛr</ta>
            <ta e="T48" id="Seg_2115" s="T47">qo-le-j-se</ta>
            <ta e="T50" id="Seg_2116" s="T49">taper</ta>
            <ta e="T51" id="Seg_2117" s="T50">üːbɨǯe-j</ta>
            <ta e="T52" id="Seg_2118" s="T51">čaːǯɨ-j</ta>
            <ta e="T54" id="Seg_2119" s="T53">a</ta>
            <ta e="T55" id="Seg_2120" s="T54">Lüdka</ta>
            <ta e="T56" id="Seg_2121" s="T55">mäkkä</ta>
            <ta e="T309" id="Seg_2122" s="T56">čenčɨ</ta>
            <ta e="T57" id="Seg_2123" s="T309">kawarit</ta>
            <ta e="T310" id="Seg_2124" s="T57">tɨnd</ta>
            <ta e="T58" id="Seg_2125" s="T310">kawarit</ta>
            <ta e="T59" id="Seg_2126" s="T58">qaj-qoj</ta>
            <ta e="T60" id="Seg_2127" s="T59">qwaja-ku-mbɨ</ta>
            <ta e="T62" id="Seg_2128" s="T61">a</ta>
            <ta e="T63" id="Seg_2129" s="T62">man</ta>
            <ta e="T64" id="Seg_2130" s="T63">tab-ɨ-n</ta>
            <ta e="T65" id="Seg_2131" s="T64">narošna-k</ta>
            <ta e="T66" id="Seg_2132" s="T65">čenčɨ-k</ta>
            <ta e="T67" id="Seg_2133" s="T66">štobɨ</ta>
            <ta e="T68" id="Seg_2134" s="T67">tab</ta>
            <ta e="T69" id="Seg_2135" s="T68">ɨgɨ</ta>
            <ta e="T70" id="Seg_2136" s="T69">tanu-le-nd</ta>
            <ta e="T71" id="Seg_2137" s="T70">što</ta>
            <ta e="T72" id="Seg_2138" s="T71">surup</ta>
            <ta e="T73" id="Seg_2139" s="T72">qwaja-ku-mbɨ</ta>
            <ta e="T75" id="Seg_2140" s="T74">man</ta>
            <ta e="T311" id="Seg_2141" s="T75">čenčɨ-k</ta>
            <ta e="T76" id="Seg_2142" s="T311">a</ta>
            <ta e="T77" id="Seg_2143" s="T76">taw</ta>
            <ta e="T78" id="Seg_2144" s="T77">bauška-mɨ-t</ta>
            <ta e="T79" id="Seg_2145" s="T78">qwaja-ku-mbɨ-ndɨ-q</ta>
            <ta e="T81" id="Seg_2146" s="T80">čaːǯɨ-j</ta>
            <ta e="T83" id="Seg_2147" s="T81">kalammut</ta>
            <ta e="T84" id="Seg_2148" s="T83">elʼǯʼä</ta>
            <ta e="T85" id="Seg_2149" s="T84">šitə</ta>
            <ta e="T86" id="Seg_2150" s="T85">bar</ta>
            <ta e="T88" id="Seg_2151" s="T87">taper</ta>
            <ta e="T89" id="Seg_2152" s="T88">man</ta>
            <ta e="T90" id="Seg_2153" s="T89">undɛ-lʼčǝ-mbɨ-m</ta>
            <ta e="T91" id="Seg_2154" s="T90">mute</ta>
            <ta e="T92" id="Seg_2155" s="T91">neː-m</ta>
            <ta e="T93" id="Seg_2156" s="T92">aː</ta>
            <ta e="T94" id="Seg_2157" s="T93">ügə-la</ta>
            <ta e="T96" id="Seg_2158" s="T95">čaːǯɨ-j</ta>
            <ta e="T99" id="Seg_2159" s="T98">mäkkä</ta>
            <ta e="T100" id="Seg_2160" s="T99">neː-mɨ</ta>
            <ta e="T101" id="Seg_2161" s="T100">kurda</ta>
            <ta e="T102" id="Seg_2162" s="T101">qorqɨ</ta>
            <ta e="T103" id="Seg_2163" s="T102">tona</ta>
            <ta e="T104" id="Seg_2164" s="T103">man</ta>
            <ta e="T105" id="Seg_2165" s="T104">tab-ni</ta>
            <ta e="T106" id="Seg_2166" s="T105">barʒa-k</ta>
            <ta e="T107" id="Seg_2167" s="T106">qaj</ta>
            <ta e="T108" id="Seg_2168" s="T107">qorqɨ</ta>
            <ta e="T110" id="Seg_2169" s="T109">inne</ta>
            <ta e="T111" id="Seg_2170" s="T110">manǯu-nǯe-k</ta>
            <ta e="T112" id="Seg_2171" s="T111">sabɨlʼ</ta>
            <ta e="T113" id="Seg_2172" s="T112">qorqɨ</ta>
            <ta e="T114" id="Seg_2173" s="T113">nɨŋ-kɨ</ta>
            <ta e="T115" id="Seg_2174" s="T114">miǯnɨt</ta>
            <ta e="T116" id="Seg_2175" s="T115">mantɨ-mbɨ</ta>
            <ta e="T118" id="Seg_2176" s="T117">metr</ta>
            <ta e="T119" id="Seg_2177" s="T118">šitə</ta>
            <ta e="T120" id="Seg_2178" s="T119">metr</ta>
            <ta e="T121" id="Seg_2179" s="T120">nagur</ta>
            <ta e="T122" id="Seg_2180" s="T121">metr</ta>
            <ta e="T123" id="Seg_2181" s="T122">čʼomb</ta>
            <ta e="T124" id="Seg_2182" s="T123">nɨŋ-kɨ</ta>
            <ta e="T125" id="Seg_2183" s="T124">miǯnɨt</ta>
            <ta e="T126" id="Seg_2184" s="T125">mantɨ-mbɨ</ta>
            <ta e="T128" id="Seg_2185" s="T127">man</ta>
            <ta e="T129" id="Seg_2186" s="T128">toʒe</ta>
            <ta e="T130" id="Seg_2187" s="T129">kɨčwan-ŋɨ-k</ta>
            <ta e="T132" id="Seg_2188" s="T131">tär-mbɨ-k</ta>
            <ta e="T133" id="Seg_2189" s="T132">lučše</ta>
            <ta e="T134" id="Seg_2190" s="T133">maᴣik</ta>
            <ta e="T135" id="Seg_2191" s="T134">oral-le-nd</ta>
            <ta e="T137" id="Seg_2192" s="T136">man</ta>
            <ta e="T138" id="Seg_2193" s="T137">neː-qɨnt</ta>
            <ta e="T139" id="Seg_2194" s="T138">ɛːǯal-lɨ-ku-wa-k</ta>
            <ta e="T140" id="Seg_2195" s="T139">närq</ta>
            <ta e="T141" id="Seg_2196" s="T140">tae</ta>
            <ta e="T142" id="Seg_2197" s="T141">kur-ɨ-lɨ-špɨ-äšɨk</ta>
            <ta e="T144" id="Seg_2198" s="T143">man</ta>
            <ta e="T145" id="Seg_2199" s="T144">neː-mɨ</ta>
            <ta e="T146" id="Seg_2200" s="T145">kur-ɨ-ŋɨ</ta>
            <ta e="T147" id="Seg_2201" s="T146">kaʒɨk</ta>
            <ta e="T149" id="Seg_2202" s="T148">a</ta>
            <ta e="T150" id="Seg_2203" s="T149">man</ta>
            <ta e="T151" id="Seg_2204" s="T150">ɛːǯal-lɨ-ku-wa-k</ta>
            <ta e="T152" id="Seg_2205" s="T151">ɨgɨ</ta>
            <ta e="T153" id="Seg_2206" s="T152">kaʒan-äšɨk</ta>
            <ta e="T154" id="Seg_2207" s="T153">pomalenʼku</ta>
            <ta e="T155" id="Seg_2208" s="T154">čaːǯɨ-äšɨk</ta>
            <ta e="T157" id="Seg_2209" s="T156">onek</ta>
            <ta e="T158" id="Seg_2210" s="T157">nɨŋ-kɨ-k</ta>
            <ta e="T159" id="Seg_2211" s="T158">taper</ta>
            <ta e="T160" id="Seg_2212" s="T159">mantɨ-mbɨ-k</ta>
            <ta e="T161" id="Seg_2213" s="T160">tab-ɨ-n</ta>
            <ta e="T162" id="Seg_2214" s="T161">čenčɨ-k</ta>
            <ta e="T163" id="Seg_2215" s="T162">tan</ta>
            <ta e="T164" id="Seg_2216" s="T163">qaj-tqo</ta>
            <ta e="T165" id="Seg_2217" s="T164">nɨka</ta>
            <ta e="T166" id="Seg_2218" s="T165">töː-ŋɨ-nd</ta>
            <ta e="T167" id="Seg_2219" s="T166">man</ta>
            <ta e="T168" id="Seg_2220" s="T167">wattə-n-tɨ</ta>
            <ta e="T169" id="Seg_2221" s="T168">par-nde</ta>
            <ta e="T171" id="Seg_2222" s="T170">wsjo</ta>
            <ta e="T172" id="Seg_2223" s="T171">nɨŋ-kɨ-k</ta>
            <ta e="T173" id="Seg_2224" s="T172">mantɨ-mbɨ-k</ta>
            <ta e="T174" id="Seg_2225" s="T173">a</ta>
            <ta e="T175" id="Seg_2226" s="T174">tab</ta>
            <ta e="T176" id="Seg_2227" s="T175">naj</ta>
            <ta e="T177" id="Seg_2228" s="T176">maᴣik</ta>
            <ta e="T178" id="Seg_2229" s="T177">mantɨ-mbɨ</ta>
            <ta e="T180" id="Seg_2230" s="T179">Lüdka</ta>
            <ta e="T181" id="Seg_2231" s="T180">kur-ɨ-ŋɨ</ta>
            <ta e="T182" id="Seg_2232" s="T181">merda</ta>
            <ta e="T184" id="Seg_2233" s="T183">man</ta>
            <ta e="T185" id="Seg_2234" s="T184">taper</ta>
            <ta e="T186" id="Seg_2235" s="T185">naššak</ta>
            <ta e="T187" id="Seg_2236" s="T186">nɨŋ-kɨ-k</ta>
            <ta e="T188" id="Seg_2237" s="T187">naj</ta>
            <ta e="T189" id="Seg_2238" s="T188">üːbɨǯe-k</ta>
            <ta e="T190" id="Seg_2239" s="T189">aj</ta>
            <ta e="T191" id="Seg_2240" s="T190">manǯu-ku-wa-m</ta>
            <ta e="T192" id="Seg_2241" s="T191">nɨŋ-kɨ</ta>
            <ta e="T193" id="Seg_2242" s="T192">aj</ta>
            <ta e="T194" id="Seg_2243" s="T193">maᴣik</ta>
            <ta e="T195" id="Seg_2244" s="T194">mantɨ-mbɨ</ta>
            <ta e="T196" id="Seg_2245" s="T195">qorqɨ</ta>
            <ta e="T198" id="Seg_2246" s="T197">man</ta>
            <ta e="T199" id="Seg_2247" s="T198">taper</ta>
            <ta e="T200" id="Seg_2248" s="T199">medra-ku-k</ta>
            <ta e="T201" id="Seg_2249" s="T200">pan-ɨ-nǯe-ku-wa-m</ta>
            <ta e="T202" id="Seg_2250" s="T201">nɨka</ta>
            <ta e="T203" id="Seg_2251" s="T202">illä</ta>
            <ta e="T204" id="Seg_2252" s="T203">qweː-le</ta>
            <ta e="T205" id="Seg_2253" s="T204">vsʼakij</ta>
            <ta e="T206" id="Seg_2254" s="T205">a</ta>
            <ta e="T207" id="Seg_2255" s="T206">tab</ta>
            <ta e="T208" id="Seg_2256" s="T207">nöd-le</ta>
            <ta e="T209" id="Seg_2257" s="T208">uǯe-ku-gu</ta>
            <ta e="T211" id="Seg_2258" s="T210">Lüdka</ta>
            <ta e="T212" id="Seg_2259" s="T211">naj</ta>
            <ta e="T213" id="Seg_2260" s="T212">kɨbɨǯga-k</ta>
            <ta e="T215" id="Seg_2261" s="T214">qaj-da</ta>
            <ta e="T216" id="Seg_2262" s="T215">qadol-ku-tɨ</ta>
            <ta e="T217" id="Seg_2263" s="T216">nɨka</ta>
            <ta e="T218" id="Seg_2264" s="T217">wesʼ</ta>
            <ta e="T219" id="Seg_2265" s="T218">tɨ</ta>
            <ta e="T220" id="Seg_2266" s="T219">mɨ-se</ta>
            <ta e="T221" id="Seg_2267" s="T220">punur-se</ta>
            <ta e="T222" id="Seg_2268" s="T221">qadol-mbɨ-tɨ</ta>
            <ta e="T223" id="Seg_2269" s="T222">naj</ta>
            <ta e="T224" id="Seg_2270" s="T223">kur-ɨ-lʼčǝ-ku-ŋɨ</ta>
            <ta e="T225" id="Seg_2271" s="T224">maᴣik</ta>
            <ta e="T226" id="Seg_2272" s="T225">mantɨ-mbɨ</ta>
            <ta e="T228" id="Seg_2273" s="T227">nʼar</ta>
            <ta e="T229" id="Seg_2274" s="T228">tʼeres</ta>
            <ta e="T230" id="Seg_2275" s="T229">oni</ta>
            <ta e="T231" id="Seg_2276" s="T230">kanak</ta>
            <ta e="T232" id="Seg_2277" s="T231">bauška-ni</ta>
            <ta e="T233" id="Seg_2278" s="T232">parka-ndɨ-j</ta>
            <ta e="T235" id="Seg_2279" s="T234">patom</ta>
            <ta e="T236" id="Seg_2280" s="T235">tae</ta>
            <ta e="T237" id="Seg_2281" s="T236">pušla-j</ta>
            <ta e="T238" id="Seg_2282" s="T237">parka-wa-j</ta>
            <ta e="T239" id="Seg_2283" s="T238">bauška</ta>
            <ta e="T241" id="Seg_2284" s="T240">a</ta>
            <ta e="T242" id="Seg_2285" s="T241">bauška</ta>
            <ta e="T243" id="Seg_2286" s="T242">čenčɨ</ta>
            <ta e="T244" id="Seg_2287" s="T243">qaj</ta>
            <ta e="T245" id="Seg_2288" s="T244">parka-wa</ta>
            <ta e="T246" id="Seg_2289" s="T245">nača-tɨ</ta>
            <ta e="T248" id="Seg_2290" s="T247">taper</ta>
            <ta e="T249" id="Seg_2291" s="T248">man</ta>
            <ta e="T250" id="Seg_2292" s="T249">Lüdka-ni</ta>
            <ta e="T251" id="Seg_2293" s="T250">čenčɨ-k</ta>
            <ta e="T252" id="Seg_2294" s="T251">wando-lə</ta>
            <ta e="T253" id="Seg_2295" s="T252">qaj-tqo</ta>
            <ta e="T254" id="Seg_2296" s="T253">nɨlʼǯi-k</ta>
            <ta e="T255" id="Seg_2297" s="T254">čeq</ta>
            <ta e="T256" id="Seg_2298" s="T255">čane-mbɨ</ta>
            <ta e="T258" id="Seg_2299" s="T257">a</ta>
            <ta e="T259" id="Seg_2300" s="T258">man</ta>
            <ta e="T260" id="Seg_2301" s="T259">neː-mɨ</ta>
            <ta e="T261" id="Seg_2302" s="T260">ɛːǯal-ku-wa</ta>
            <ta e="T262" id="Seg_2303" s="T261">a</ta>
            <ta e="T263" id="Seg_2304" s="T262">tan</ta>
            <ta e="T264" id="Seg_2305" s="T263">naj</ta>
            <ta e="T266" id="Seg_2306" s="T265">wando-lə</ta>
            <ta e="T267" id="Seg_2307" s="T266">čeq-ɨ-j-ja</ta>
            <ta e="T269" id="Seg_2308" s="T268">nu</ta>
            <ta e="T270" id="Seg_2309" s="T269">mi</ta>
            <ta e="T271" id="Seg_2310" s="T270">or-ɨ-k</ta>
            <ta e="T272" id="Seg_2311" s="T271">kɨčwan-ŋɨ-j</ta>
            <ta e="T274" id="Seg_2312" s="T273">bauška-ni</ta>
            <ta e="T275" id="Seg_2313" s="T274">töː-ɨ-j</ta>
            <ta e="T276" id="Seg_2314" s="T275">bauška</ta>
            <ta e="T277" id="Seg_2315" s="T276">qaj</ta>
            <ta e="T278" id="Seg_2316" s="T277">kata-wa-nd</ta>
            <ta e="T280" id="Seg_2317" s="T279">manǯu-k</ta>
            <ta e="T281" id="Seg_2318" s="T280">man</ta>
            <ta e="T282" id="Seg_2319" s="T281">mi</ta>
            <ta e="T283" id="Seg_2320" s="T282">qorqɨ-p</ta>
            <ta e="T284" id="Seg_2321" s="T283">ombi</ta>
            <ta e="T285" id="Seg_2322" s="T284">qonǯer-ŋɨ-j</ta>
            <ta e="T287" id="Seg_2323" s="T286">šinni</ta>
            <ta e="T288" id="Seg_2324" s="T287">taw</ta>
            <ta e="T289" id="Seg_2325" s="T288">kur-ntɨ-j</ta>
            <ta e="T291" id="Seg_2326" s="T290">a</ta>
            <ta e="T292" id="Seg_2327" s="T291">bauška</ta>
            <ta e="T293" id="Seg_2328" s="T292">nɨlʼǯi-k</ta>
            <ta e="T294" id="Seg_2329" s="T293">ɛːǯal-ku-wa</ta>
            <ta e="T295" id="Seg_2330" s="T294">a</ta>
            <ta e="T296" id="Seg_2331" s="T295">na</ta>
            <ta e="T297" id="Seg_2332" s="T296">qaj</ta>
            <ta e="T299" id="Seg_2333" s="T298">awe-tɨ</ta>
            <ta e="T300" id="Seg_2334" s="T299">man</ta>
            <ta e="T301" id="Seg_2335" s="T300">sat</ta>
            <ta e="T302" id="Seg_2336" s="T301">nɨka</ta>
            <ta e="T303" id="Seg_2337" s="T302">tade-mbɨ-tɨ</ta>
            <ta e="T304" id="Seg_2338" s="T303">wattə-n</ta>
            <ta e="T305" id="Seg_2339" s="T304">saj-qɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_2340" s="T1">how</ta>
            <ta e="T3" id="Seg_2341" s="T2">bear-ACC</ta>
            <ta e="T4" id="Seg_2342" s="T3">see-CO-3PL</ta>
            <ta e="T6" id="Seg_2343" s="T5">I.NOM</ta>
            <ta e="T306" id="Seg_2344" s="T6">daughter-GEN</ta>
            <ta e="T7" id="Seg_2345" s="T306">with</ta>
            <ta e="T8" id="Seg_2346" s="T7">berry-TRL</ta>
            <ta e="T9" id="Seg_2347" s="T8">go.away-PST-1DU</ta>
            <ta e="T307" id="Seg_2348" s="T9">Lyudmila-GEN</ta>
            <ta e="T10" id="Seg_2349" s="T307">with</ta>
            <ta e="T12" id="Seg_2350" s="T11">but</ta>
            <ta e="T13" id="Seg_2351" s="T12">we.DU.NOM</ta>
            <ta e="T14" id="Seg_2352" s="T13">%%</ta>
            <ta e="T15" id="Seg_2353" s="T14">go.away</ta>
            <ta e="T17" id="Seg_2354" s="T16">but</ta>
            <ta e="T18" id="Seg_2355" s="T17">grandmother.[NOM]</ta>
            <ta e="T19" id="Seg_2356" s="T18">Warwara.[NOM]</ta>
            <ta e="T20" id="Seg_2357" s="T19">there-ADV.LOC</ta>
            <ta e="T22" id="Seg_2358" s="T21">live-PST-3SG.S</ta>
            <ta e="T24" id="Seg_2359" s="T23">on.foot</ta>
            <ta e="T25" id="Seg_2360" s="T24">go.away-PST-1DU</ta>
            <ta e="T27" id="Seg_2361" s="T26">now</ta>
            <ta e="T28" id="Seg_2362" s="T27">come-CO-1DU</ta>
            <ta e="T29" id="Seg_2363" s="T28">grandmother.[NOM]</ta>
            <ta e="T30" id="Seg_2364" s="T29">Warwara.[NOM]</ta>
            <ta e="T31" id="Seg_2365" s="T30">NEG.EX-CO-3SG.S</ta>
            <ta e="T32" id="Seg_2366" s="T31">on</ta>
            <ta e="T33" id="Seg_2367" s="T32">station.LOC</ta>
            <ta e="T35" id="Seg_2368" s="T34">but</ta>
            <ta e="T36" id="Seg_2369" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_2370" s="T36">Ljudka-ALL</ta>
            <ta e="T38" id="Seg_2371" s="T37">say-1SG.S</ta>
            <ta e="T39" id="Seg_2372" s="T38">look-IMP.2SG.S</ta>
            <ta e="T40" id="Seg_2373" s="T39">here</ta>
            <ta e="T41" id="Seg_2374" s="T40">go.away-OPT-1DU</ta>
            <ta e="T308" id="Seg_2375" s="T41">here</ta>
            <ta e="T42" id="Seg_2376" s="T308">this</ta>
            <ta e="T43" id="Seg_2377" s="T42">side-PROL</ta>
            <ta e="T44" id="Seg_2378" s="T43">go.away-OPT-1DU</ta>
            <ta e="T45" id="Seg_2379" s="T44">here</ta>
            <ta e="T46" id="Seg_2380" s="T45">maybe</ta>
            <ta e="T47" id="Seg_2381" s="T46">berry.[NOM]</ta>
            <ta e="T48" id="Seg_2382" s="T47">find-OPT-1DU-OPT</ta>
            <ta e="T50" id="Seg_2383" s="T49">now</ta>
            <ta e="T51" id="Seg_2384" s="T50">depart-1DU</ta>
            <ta e="T52" id="Seg_2385" s="T51">run-1DU</ta>
            <ta e="T54" id="Seg_2386" s="T53">but</ta>
            <ta e="T55" id="Seg_2387" s="T54">Ljudka.[NOM]</ta>
            <ta e="T56" id="Seg_2388" s="T55">I.ALL</ta>
            <ta e="T309" id="Seg_2389" s="T56">say.[3SG.S]</ta>
            <ta e="T57" id="Seg_2390" s="T309">say.3SG</ta>
            <ta e="T310" id="Seg_2391" s="T57">here</ta>
            <ta e="T58" id="Seg_2392" s="T310">say.3SG</ta>
            <ta e="T59" id="Seg_2393" s="T58">what-%%</ta>
            <ta e="T60" id="Seg_2394" s="T59">go-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_2395" s="T61">but</ta>
            <ta e="T63" id="Seg_2396" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_2397" s="T63">(s)he-EP-ILL2</ta>
            <ta e="T65" id="Seg_2398" s="T64">deliberately-ADVZ</ta>
            <ta e="T66" id="Seg_2399" s="T65">say-1SG.S</ta>
            <ta e="T67" id="Seg_2400" s="T66">that</ta>
            <ta e="T68" id="Seg_2401" s="T67">(s)he.[NOM]</ta>
            <ta e="T69" id="Seg_2402" s="T68">NEG.IMP</ta>
            <ta e="T70" id="Seg_2403" s="T69">know-OPT-2SG.S</ta>
            <ta e="T71" id="Seg_2404" s="T70">that</ta>
            <ta e="T72" id="Seg_2405" s="T71">wild.animal.[NOM]</ta>
            <ta e="T73" id="Seg_2406" s="T72">go-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T75" id="Seg_2407" s="T74">I.NOM</ta>
            <ta e="T311" id="Seg_2408" s="T75">say-1SG.S</ta>
            <ta e="T76" id="Seg_2409" s="T311">ah</ta>
            <ta e="T77" id="Seg_2410" s="T76">this.[NOM]</ta>
            <ta e="T78" id="Seg_2411" s="T77">grandmother-1SG-PL</ta>
            <ta e="T79" id="Seg_2412" s="T78">go-HAB-DUR-INFER-3DU.S</ta>
            <ta e="T81" id="Seg_2413" s="T80">run-1DU</ta>
            <ta e="T83" id="Seg_2414" s="T81">%%</ta>
            <ta e="T84" id="Seg_2415" s="T83">%%</ta>
            <ta e="T85" id="Seg_2416" s="T84">two</ta>
            <ta e="T86" id="Seg_2417" s="T85">time.[NOM]</ta>
            <ta e="T88" id="Seg_2418" s="T87">now</ta>
            <ta e="T89" id="Seg_2419" s="T88">I.NOM</ta>
            <ta e="T90" id="Seg_2420" s="T89">hear-PFV-PST.NAR-1SG.O</ta>
            <ta e="T91" id="Seg_2421" s="T90">%%</ta>
            <ta e="T92" id="Seg_2422" s="T91">daughter-ACC</ta>
            <ta e="T93" id="Seg_2423" s="T92">NEG</ta>
            <ta e="T94" id="Seg_2424" s="T93">pull.away-FUT.[3SG.S]</ta>
            <ta e="T96" id="Seg_2425" s="T95">run-1DU</ta>
            <ta e="T99" id="Seg_2426" s="T98">I.ALL</ta>
            <ta e="T100" id="Seg_2427" s="T99">daughter-1SG</ta>
            <ta e="T101" id="Seg_2428" s="T100">come.running.[3SG.S]</ta>
            <ta e="T102" id="Seg_2429" s="T101">bear.[NOM]</ta>
            <ta e="T103" id="Seg_2430" s="T102">there</ta>
            <ta e="T104" id="Seg_2431" s="T103">I.NOM</ta>
            <ta e="T105" id="Seg_2432" s="T104">(s)he-ALL</ta>
            <ta e="T106" id="Seg_2433" s="T105">%cry-1SG.S</ta>
            <ta e="T107" id="Seg_2434" s="T106">what.[NOM]</ta>
            <ta e="T108" id="Seg_2435" s="T107">bear.[NOM]</ta>
            <ta e="T110" id="Seg_2436" s="T109">up</ta>
            <ta e="T111" id="Seg_2437" s="T110">look-IPFV3-1SG.S</ta>
            <ta e="T112" id="Seg_2438" s="T111">indeed</ta>
            <ta e="T113" id="Seg_2439" s="T112">bear.[NOM]</ta>
            <ta e="T114" id="Seg_2440" s="T113">stand-HAB.[3SG.S]</ta>
            <ta e="T115" id="Seg_2441" s="T114">we.DU.ACC</ta>
            <ta e="T116" id="Seg_2442" s="T115">look-PST.NAR.[3SG.S]</ta>
            <ta e="T118" id="Seg_2443" s="T117">meter.[NOM]</ta>
            <ta e="T119" id="Seg_2444" s="T118">two</ta>
            <ta e="T120" id="Seg_2445" s="T119">meter.[NOM]</ta>
            <ta e="T121" id="Seg_2446" s="T120">three</ta>
            <ta e="T122" id="Seg_2447" s="T121">meter.[NOM]</ta>
            <ta e="T123" id="Seg_2448" s="T122">during</ta>
            <ta e="T124" id="Seg_2449" s="T123">stand-HAB.[3SG.S]</ta>
            <ta e="T125" id="Seg_2450" s="T124">we.DU.ACC</ta>
            <ta e="T126" id="Seg_2451" s="T125">look-PST.NAR.[3SG.S]</ta>
            <ta e="T128" id="Seg_2452" s="T127">I.NOM</ta>
            <ta e="T129" id="Seg_2453" s="T128">also</ta>
            <ta e="T130" id="Seg_2454" s="T129">get.angry-CO-1SG.S</ta>
            <ta e="T132" id="Seg_2455" s="T131">think-PST.NAR-1SG.S</ta>
            <ta e="T133" id="Seg_2456" s="T132">better</ta>
            <ta e="T134" id="Seg_2457" s="T133">I.ACC</ta>
            <ta e="T135" id="Seg_2458" s="T134">catch-OPT-2SG.S</ta>
            <ta e="T137" id="Seg_2459" s="T136">I.NOM</ta>
            <ta e="T138" id="Seg_2460" s="T137">daughter-ILL.3SG</ta>
            <ta e="T139" id="Seg_2461" s="T138">say-RES-HAB-CO-1SG.S</ta>
            <ta e="T140" id="Seg_2462" s="T139">willow.[NOM]</ta>
            <ta e="T141" id="Seg_2463" s="T140">for</ta>
            <ta e="T142" id="Seg_2464" s="T141">run-EP-RES-IPFV2-IMP.2SG.S</ta>
            <ta e="T144" id="Seg_2465" s="T143">I.NOM</ta>
            <ta e="T145" id="Seg_2466" s="T144">daughter-1SG</ta>
            <ta e="T146" id="Seg_2467" s="T145">run-EP-CO.[3SG.S]</ta>
            <ta e="T147" id="Seg_2468" s="T146">at.a.run</ta>
            <ta e="T149" id="Seg_2469" s="T148">but</ta>
            <ta e="T150" id="Seg_2470" s="T149">I.NOM</ta>
            <ta e="T151" id="Seg_2471" s="T150">say-RES-HAB-CO-1SG.S</ta>
            <ta e="T152" id="Seg_2472" s="T151">NEG.IMP</ta>
            <ta e="T153" id="Seg_2473" s="T152">run-IMP.2SG.S</ta>
            <ta e="T154" id="Seg_2474" s="T153">slowly</ta>
            <ta e="T155" id="Seg_2475" s="T154">go-IMP.2SG.S</ta>
            <ta e="T157" id="Seg_2476" s="T156">oneself.1SG</ta>
            <ta e="T158" id="Seg_2477" s="T157">stand-HAB-1SG.S</ta>
            <ta e="T159" id="Seg_2478" s="T158">now</ta>
            <ta e="T160" id="Seg_2479" s="T159">look-DUR-1SG.S</ta>
            <ta e="T161" id="Seg_2480" s="T160">(s)he-EP-ILL2</ta>
            <ta e="T162" id="Seg_2481" s="T161">say-1SG.S</ta>
            <ta e="T163" id="Seg_2482" s="T162">you.SG.NOM</ta>
            <ta e="T164" id="Seg_2483" s="T163">what-TRL</ta>
            <ta e="T165" id="Seg_2484" s="T164">here</ta>
            <ta e="T166" id="Seg_2485" s="T165">come-CO-2SG.S</ta>
            <ta e="T167" id="Seg_2486" s="T166">I.NOM</ta>
            <ta e="T168" id="Seg_2487" s="T167">road-GEN-3SG</ta>
            <ta e="T169" id="Seg_2488" s="T168">top-ILL</ta>
            <ta e="T171" id="Seg_2489" s="T170">all.[3SG.S]</ta>
            <ta e="T172" id="Seg_2490" s="T171">stand-HAB-1SG.S</ta>
            <ta e="T173" id="Seg_2491" s="T172">look-DUR-1SG.S</ta>
            <ta e="T174" id="Seg_2492" s="T173">but</ta>
            <ta e="T175" id="Seg_2493" s="T174">(s)he.[NOM]</ta>
            <ta e="T176" id="Seg_2494" s="T175">also</ta>
            <ta e="T177" id="Seg_2495" s="T176">I.ACC</ta>
            <ta e="T178" id="Seg_2496" s="T177">look-PST.NAR.[3SG.S]</ta>
            <ta e="T180" id="Seg_2497" s="T179">Ljudka.[NOM]</ta>
            <ta e="T181" id="Seg_2498" s="T180">run-EP-CO.[3SG.S]</ta>
            <ta e="T182" id="Seg_2499" s="T181">%%</ta>
            <ta e="T184" id="Seg_2500" s="T183">I.NOM</ta>
            <ta e="T185" id="Seg_2501" s="T184">now</ta>
            <ta e="T186" id="Seg_2502" s="T185">so.much</ta>
            <ta e="T187" id="Seg_2503" s="T186">stand-HAB-1SG.S</ta>
            <ta e="T188" id="Seg_2504" s="T187">also</ta>
            <ta e="T189" id="Seg_2505" s="T188">depart-1SG.S</ta>
            <ta e="T190" id="Seg_2506" s="T189">again</ta>
            <ta e="T191" id="Seg_2507" s="T190">look-HAB-CO-1SG.O</ta>
            <ta e="T192" id="Seg_2508" s="T191">stand-HAB.[3SG.S]</ta>
            <ta e="T193" id="Seg_2509" s="T192">again</ta>
            <ta e="T194" id="Seg_2510" s="T193">I.ACC</ta>
            <ta e="T195" id="Seg_2511" s="T194">look-PST.NAR.[3SG.S]</ta>
            <ta e="T196" id="Seg_2512" s="T195">bear.[NOM]</ta>
            <ta e="T198" id="Seg_2513" s="T197">I.NOM</ta>
            <ta e="T199" id="Seg_2514" s="T198">now</ta>
            <ta e="T200" id="Seg_2515" s="T199">catch.up-HAB-1SG.S</ta>
            <ta e="T201" id="Seg_2516" s="T200">put-EP-IPFV3-HAB-CO-1SG.O</ta>
            <ta e="T202" id="Seg_2517" s="T201">here</ta>
            <ta e="T203" id="Seg_2518" s="T202">down</ta>
            <ta e="T204" id="Seg_2519" s="T203">##birch-OPT.[NOM]</ta>
            <ta e="T205" id="Seg_2520" s="T204">all.kinds.of</ta>
            <ta e="T206" id="Seg_2521" s="T205">but</ta>
            <ta e="T207" id="Seg_2522" s="T206">(s)he.[NOM]</ta>
            <ta e="T208" id="Seg_2523" s="T207">drive-CVB</ta>
            <ta e="T209" id="Seg_2524" s="T208">keep.distance-HAB-INF</ta>
            <ta e="T211" id="Seg_2525" s="T210">Ljudka.[NOM]</ta>
            <ta e="T212" id="Seg_2526" s="T211">also</ta>
            <ta e="T213" id="Seg_2527" s="T212">small-ADVZ</ta>
            <ta e="T215" id="Seg_2528" s="T214">what-INDEF</ta>
            <ta e="T216" id="Seg_2529" s="T215">scratch-HAB-3SG.O</ta>
            <ta e="T217" id="Seg_2530" s="T216">here</ta>
            <ta e="T218" id="Seg_2531" s="T217">all</ta>
            <ta e="T219" id="Seg_2532" s="T218">this</ta>
            <ta e="T220" id="Seg_2533" s="T219">something-INSTR</ta>
            <ta e="T221" id="Seg_2534" s="T220">mushroom-INSTR</ta>
            <ta e="T222" id="Seg_2535" s="T221">scratch-DUR-3SG.O</ta>
            <ta e="T223" id="Seg_2536" s="T222">also</ta>
            <ta e="T224" id="Seg_2537" s="T223">run-EP-PFV-HAB-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_2538" s="T224">I.ACC</ta>
            <ta e="T226" id="Seg_2539" s="T225">look-PST.NAR.[3SG.S]</ta>
            <ta e="T228" id="Seg_2540" s="T227">swamp.[NOM]</ta>
            <ta e="T229" id="Seg_2541" s="T228">through</ta>
            <ta e="T230" id="Seg_2542" s="T229">they</ta>
            <ta e="T231" id="Seg_2543" s="T230">dog.[NOM]</ta>
            <ta e="T232" id="Seg_2544" s="T231">grandmother-ALL</ta>
            <ta e="T233" id="Seg_2545" s="T232">shout-INFER-1DU</ta>
            <ta e="T235" id="Seg_2546" s="T234">then</ta>
            <ta e="T236" id="Seg_2547" s="T235">for</ta>
            <ta e="T237" id="Seg_2548" s="T236">%%-1DU</ta>
            <ta e="T238" id="Seg_2549" s="T237">shout-CO-1DU</ta>
            <ta e="T239" id="Seg_2550" s="T238">grandmother.[NOM]</ta>
            <ta e="T241" id="Seg_2551" s="T240">but</ta>
            <ta e="T242" id="Seg_2552" s="T241">grandmother.[NOM]</ta>
            <ta e="T243" id="Seg_2553" s="T242">say.[3SG.S]</ta>
            <ta e="T244" id="Seg_2554" s="T243">what.[NOM]</ta>
            <ta e="T245" id="Seg_2555" s="T244">shout-CO.[3SG.S]</ta>
            <ta e="T246" id="Seg_2556" s="T245">there-ADV.LOC</ta>
            <ta e="T248" id="Seg_2557" s="T247">now</ta>
            <ta e="T249" id="Seg_2558" s="T248">I.NOM</ta>
            <ta e="T250" id="Seg_2559" s="T249">Ljudka-ALL</ta>
            <ta e="T251" id="Seg_2560" s="T250">say-1SG.S</ta>
            <ta e="T252" id="Seg_2561" s="T251">face-2SG</ta>
            <ta e="T253" id="Seg_2562" s="T252">what-TRL</ta>
            <ta e="T254" id="Seg_2563" s="T253">such-ADVZ</ta>
            <ta e="T255" id="Seg_2564" s="T254">white</ta>
            <ta e="T256" id="Seg_2565" s="T255">%%-PST.NAR.[3SG.S]</ta>
            <ta e="T258" id="Seg_2566" s="T257">but</ta>
            <ta e="T259" id="Seg_2567" s="T258">I.GEN</ta>
            <ta e="T260" id="Seg_2568" s="T259">daughter-1SG</ta>
            <ta e="T261" id="Seg_2569" s="T260">say-HAB-CO.[3SG.S]</ta>
            <ta e="T262" id="Seg_2570" s="T261">but</ta>
            <ta e="T263" id="Seg_2571" s="T262">you.SG.GEN</ta>
            <ta e="T264" id="Seg_2572" s="T263">also</ta>
            <ta e="T266" id="Seg_2573" s="T265">face-2SG</ta>
            <ta e="T267" id="Seg_2574" s="T266">white-EP-VBLZ-CO.[3SG.S]</ta>
            <ta e="T269" id="Seg_2575" s="T268">well</ta>
            <ta e="T270" id="Seg_2576" s="T269">we.DU.NOM</ta>
            <ta e="T271" id="Seg_2577" s="T270">force-EP-ADVZ</ta>
            <ta e="T272" id="Seg_2578" s="T271">get.angry-CO-1DU</ta>
            <ta e="T274" id="Seg_2579" s="T273">grandmother-ALL</ta>
            <ta e="T275" id="Seg_2580" s="T274">come-EP-1DU</ta>
            <ta e="T276" id="Seg_2581" s="T275">grandmother.[NOM]</ta>
            <ta e="T277" id="Seg_2582" s="T276">what.[NOM]</ta>
            <ta e="T278" id="Seg_2583" s="T277">happen-CO-2SG.S</ta>
            <ta e="T280" id="Seg_2584" s="T279">look-1SG.S</ta>
            <ta e="T281" id="Seg_2585" s="T280">I.NOM</ta>
            <ta e="T282" id="Seg_2586" s="T281">we.DU.NOM</ta>
            <ta e="T283" id="Seg_2587" s="T282">bear-ACC</ta>
            <ta e="T284" id="Seg_2588" s="T283">suddenly</ta>
            <ta e="T285" id="Seg_2589" s="T284">see-CO-1DU</ta>
            <ta e="T287" id="Seg_2590" s="T286">two.1DU </ta>
            <ta e="T288" id="Seg_2591" s="T287">this.[NOM]</ta>
            <ta e="T289" id="Seg_2592" s="T288">run-IPFV-1DU</ta>
            <ta e="T291" id="Seg_2593" s="T290">but</ta>
            <ta e="T292" id="Seg_2594" s="T291">grandmother.[NOM]</ta>
            <ta e="T293" id="Seg_2595" s="T292">such-ADVZ</ta>
            <ta e="T294" id="Seg_2596" s="T293">say-HAB-CO.[3SG.S]</ta>
            <ta e="T295" id="Seg_2597" s="T294">but</ta>
            <ta e="T296" id="Seg_2598" s="T295">this</ta>
            <ta e="T297" id="Seg_2599" s="T296">what.[NOM]</ta>
            <ta e="T299" id="Seg_2600" s="T298">mother-3SG</ta>
            <ta e="T300" id="Seg_2601" s="T299">I.NOM</ta>
            <ta e="T301" id="Seg_2602" s="T300">%%</ta>
            <ta e="T302" id="Seg_2603" s="T301">here</ta>
            <ta e="T303" id="Seg_2604" s="T302">bring-PST.NAR-3SG.O</ta>
            <ta e="T304" id="Seg_2605" s="T303">road-GEN</ta>
            <ta e="T305" id="Seg_2606" s="T304">eye-LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2607" s="T1">как</ta>
            <ta e="T3" id="Seg_2608" s="T2">медведь-ACC</ta>
            <ta e="T4" id="Seg_2609" s="T3">видеть-CO-3PL</ta>
            <ta e="T6" id="Seg_2610" s="T5">я.NOM</ta>
            <ta e="T306" id="Seg_2611" s="T6">дочь-GEN</ta>
            <ta e="T7" id="Seg_2612" s="T306">с</ta>
            <ta e="T8" id="Seg_2613" s="T7">ягода-TRL</ta>
            <ta e="T9" id="Seg_2614" s="T8">пойти-PST-1DU</ta>
            <ta e="T307" id="Seg_2615" s="T9">Людмила-GEN</ta>
            <ta e="T10" id="Seg_2616" s="T307">с</ta>
            <ta e="T12" id="Seg_2617" s="T11">а</ta>
            <ta e="T13" id="Seg_2618" s="T12">мы.DU.NOM</ta>
            <ta e="T14" id="Seg_2619" s="T13">%%</ta>
            <ta e="T15" id="Seg_2620" s="T14">пойти</ta>
            <ta e="T17" id="Seg_2621" s="T16">а</ta>
            <ta e="T18" id="Seg_2622" s="T17">бабушка.[NOM]</ta>
            <ta e="T19" id="Seg_2623" s="T18">Варвара.[NOM]</ta>
            <ta e="T20" id="Seg_2624" s="T19">туда-ADV.LOC</ta>
            <ta e="T22" id="Seg_2625" s="T21">жить-PST-3SG.S</ta>
            <ta e="T24" id="Seg_2626" s="T23">пешком</ta>
            <ta e="T25" id="Seg_2627" s="T24">пойти-PST-1DU</ta>
            <ta e="T27" id="Seg_2628" s="T26">теперь</ta>
            <ta e="T28" id="Seg_2629" s="T27">прийти-CO-1DU</ta>
            <ta e="T29" id="Seg_2630" s="T28">бабушка.[NOM]</ta>
            <ta e="T30" id="Seg_2631" s="T29">Варвара.[NOM]</ta>
            <ta e="T31" id="Seg_2632" s="T30">NEG.EX-CO-3SG.S</ta>
            <ta e="T32" id="Seg_2633" s="T31">на</ta>
            <ta e="T33" id="Seg_2634" s="T32">остановка.LOC</ta>
            <ta e="T35" id="Seg_2635" s="T34">а</ta>
            <ta e="T36" id="Seg_2636" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_2637" s="T36">Людка-ALL</ta>
            <ta e="T38" id="Seg_2638" s="T37">сказать-1SG.S</ta>
            <ta e="T39" id="Seg_2639" s="T38">смотреть-IMP.2SG.S</ta>
            <ta e="T40" id="Seg_2640" s="T39">сюда</ta>
            <ta e="T41" id="Seg_2641" s="T40">пойти-OPT-1DU</ta>
            <ta e="T308" id="Seg_2642" s="T41">сюда</ta>
            <ta e="T42" id="Seg_2643" s="T308">этот</ta>
            <ta e="T43" id="Seg_2644" s="T42">сторона-PROL</ta>
            <ta e="T44" id="Seg_2645" s="T43">пойти-OPT-1DU</ta>
            <ta e="T45" id="Seg_2646" s="T44">здесь</ta>
            <ta e="T46" id="Seg_2647" s="T45">может.быть</ta>
            <ta e="T47" id="Seg_2648" s="T46">ягода.[NOM]</ta>
            <ta e="T48" id="Seg_2649" s="T47">найти-OPT-1DU-OPT</ta>
            <ta e="T50" id="Seg_2650" s="T49">теперь</ta>
            <ta e="T51" id="Seg_2651" s="T50">отправляться-1DU</ta>
            <ta e="T52" id="Seg_2652" s="T51">бегать-1DU</ta>
            <ta e="T54" id="Seg_2653" s="T53">а</ta>
            <ta e="T55" id="Seg_2654" s="T54">Людка.[NOM]</ta>
            <ta e="T56" id="Seg_2655" s="T55">я.ALL</ta>
            <ta e="T309" id="Seg_2656" s="T56">сказать.[3SG.S]</ta>
            <ta e="T57" id="Seg_2657" s="T309">говорить.3SG</ta>
            <ta e="T310" id="Seg_2658" s="T57">здесь</ta>
            <ta e="T58" id="Seg_2659" s="T310">говорить.3SG</ta>
            <ta e="T59" id="Seg_2660" s="T58">что-%%</ta>
            <ta e="T60" id="Seg_2661" s="T59">идти-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T62" id="Seg_2662" s="T61">а</ta>
            <ta e="T63" id="Seg_2663" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_2664" s="T63">он(а)-EP-ILL2</ta>
            <ta e="T65" id="Seg_2665" s="T64">нарочно-ADVZ</ta>
            <ta e="T66" id="Seg_2666" s="T65">сказать-1SG.S</ta>
            <ta e="T67" id="Seg_2667" s="T66">чтобы</ta>
            <ta e="T68" id="Seg_2668" s="T67">он(а).[NOM]</ta>
            <ta e="T69" id="Seg_2669" s="T68">NEG.IMP</ta>
            <ta e="T70" id="Seg_2670" s="T69">знать-OPT-2SG.S</ta>
            <ta e="T71" id="Seg_2671" s="T70">что</ta>
            <ta e="T72" id="Seg_2672" s="T71">зверь.[NOM]</ta>
            <ta e="T73" id="Seg_2673" s="T72">идти-HAB-PST.NAR.[3SG.S]</ta>
            <ta e="T75" id="Seg_2674" s="T74">я.NOM</ta>
            <ta e="T311" id="Seg_2675" s="T75">сказать-1SG.S</ta>
            <ta e="T76" id="Seg_2676" s="T311">a</ta>
            <ta e="T77" id="Seg_2677" s="T76">этот.[NOM]</ta>
            <ta e="T78" id="Seg_2678" s="T77">бабушка-1SG-PL</ta>
            <ta e="T79" id="Seg_2679" s="T78">идти-HAB-DUR-INFER-3DU.S</ta>
            <ta e="T81" id="Seg_2680" s="T80">бегать-1DU</ta>
            <ta e="T83" id="Seg_2681" s="T81">%%</ta>
            <ta e="T84" id="Seg_2682" s="T83">%%</ta>
            <ta e="T85" id="Seg_2683" s="T84">два</ta>
            <ta e="T86" id="Seg_2684" s="T85">время.[NOM]</ta>
            <ta e="T88" id="Seg_2685" s="T87">теперь</ta>
            <ta e="T89" id="Seg_2686" s="T88">я.NOM</ta>
            <ta e="T90" id="Seg_2687" s="T89">слышать-PFV-PST.NAR-1SG.O</ta>
            <ta e="T91" id="Seg_2688" s="T90">%%</ta>
            <ta e="T92" id="Seg_2689" s="T91">дочь-ACC</ta>
            <ta e="T93" id="Seg_2690" s="T92">NEG</ta>
            <ta e="T94" id="Seg_2691" s="T93">оттащить-FUT.[3SG.S]</ta>
            <ta e="T96" id="Seg_2692" s="T95">бегать-1DU</ta>
            <ta e="T99" id="Seg_2693" s="T98">я.ALL</ta>
            <ta e="T100" id="Seg_2694" s="T99">дочь-1SG</ta>
            <ta e="T101" id="Seg_2695" s="T100">прибежать.[3SG.S]</ta>
            <ta e="T102" id="Seg_2696" s="T101">медведь.[NOM]</ta>
            <ta e="T103" id="Seg_2697" s="T102">там</ta>
            <ta e="T104" id="Seg_2698" s="T103">я.NOM</ta>
            <ta e="T105" id="Seg_2699" s="T104">он(а)-ALL</ta>
            <ta e="T106" id="Seg_2700" s="T105">%кричать-1SG.S</ta>
            <ta e="T107" id="Seg_2701" s="T106">что.[NOM]</ta>
            <ta e="T108" id="Seg_2702" s="T107">медведь.[NOM]</ta>
            <ta e="T110" id="Seg_2703" s="T109">вверх</ta>
            <ta e="T111" id="Seg_2704" s="T110">смотреть-IPFV3-1SG.S</ta>
            <ta e="T112" id="Seg_2705" s="T111">в.самом.деле</ta>
            <ta e="T113" id="Seg_2706" s="T112">медведь.[NOM]</ta>
            <ta e="T114" id="Seg_2707" s="T113">стоять-HAB.[3SG.S]</ta>
            <ta e="T115" id="Seg_2708" s="T114">мы.DU.ACC</ta>
            <ta e="T116" id="Seg_2709" s="T115">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T118" id="Seg_2710" s="T117">метр.[NOM]</ta>
            <ta e="T119" id="Seg_2711" s="T118">два</ta>
            <ta e="T120" id="Seg_2712" s="T119">метр.[NOM]</ta>
            <ta e="T121" id="Seg_2713" s="T120">три</ta>
            <ta e="T122" id="Seg_2714" s="T121">метр.[NOM]</ta>
            <ta e="T123" id="Seg_2715" s="T122">в.течении</ta>
            <ta e="T124" id="Seg_2716" s="T123">стоять-HAB.[3SG.S]</ta>
            <ta e="T125" id="Seg_2717" s="T124">we.DU.ACC</ta>
            <ta e="T126" id="Seg_2718" s="T125">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T128" id="Seg_2719" s="T127">я.NOM</ta>
            <ta e="T129" id="Seg_2720" s="T128">тоже</ta>
            <ta e="T130" id="Seg_2721" s="T129">испугаться-CO-1SG.S</ta>
            <ta e="T132" id="Seg_2722" s="T131">думать-PST.NAR-1SG.S</ta>
            <ta e="T133" id="Seg_2723" s="T132">лучше</ta>
            <ta e="T134" id="Seg_2724" s="T133">я.ACC</ta>
            <ta e="T135" id="Seg_2725" s="T134">поймать-OPT-2SG.S</ta>
            <ta e="T137" id="Seg_2726" s="T136">я.NOM</ta>
            <ta e="T138" id="Seg_2727" s="T137">дочь-ILL.3SG</ta>
            <ta e="T139" id="Seg_2728" s="T138">сказать-RES-HAB-CO-1SG.S</ta>
            <ta e="T140" id="Seg_2729" s="T139">тальник.[NOM]</ta>
            <ta e="T141" id="Seg_2730" s="T140">за</ta>
            <ta e="T142" id="Seg_2731" s="T141">бегать-EP-RES-IPFV2-IMP.2SG.S</ta>
            <ta e="T144" id="Seg_2732" s="T143">я.NOM</ta>
            <ta e="T145" id="Seg_2733" s="T144">дочь-1SG</ta>
            <ta e="T146" id="Seg_2734" s="T145">бегать-EP-CO.[3SG.S]</ta>
            <ta e="T147" id="Seg_2735" s="T146">бегом</ta>
            <ta e="T149" id="Seg_2736" s="T148">а</ta>
            <ta e="T150" id="Seg_2737" s="T149">я.NOM</ta>
            <ta e="T151" id="Seg_2738" s="T150">сказать-RES-HAB-CO-1SG.S</ta>
            <ta e="T152" id="Seg_2739" s="T151">NEG.IMP</ta>
            <ta e="T153" id="Seg_2740" s="T152">бежать-IMP.2SG.S</ta>
            <ta e="T154" id="Seg_2741" s="T153">‎‎помаленьку</ta>
            <ta e="T155" id="Seg_2742" s="T154">-IMP.2SG.S</ta>
            <ta e="T157" id="Seg_2743" s="T156">сам.1SG</ta>
            <ta e="T158" id="Seg_2744" s="T157">стоять-HAB-1SG.S</ta>
            <ta e="T159" id="Seg_2745" s="T158">теперь</ta>
            <ta e="T160" id="Seg_2746" s="T159">смотреть-DUR-1SG.S</ta>
            <ta e="T161" id="Seg_2747" s="T160">он(а)-EP-ILL2</ta>
            <ta e="T162" id="Seg_2748" s="T161">сказать-1SG.S</ta>
            <ta e="T163" id="Seg_2749" s="T162">ты.NOM</ta>
            <ta e="T164" id="Seg_2750" s="T163">что-TRL</ta>
            <ta e="T165" id="Seg_2751" s="T164">сюда</ta>
            <ta e="T166" id="Seg_2752" s="T165">прийти-CO-2SG.S</ta>
            <ta e="T167" id="Seg_2753" s="T166">я.NOM</ta>
            <ta e="T168" id="Seg_2754" s="T167">путь-GEN-3SG</ta>
            <ta e="T169" id="Seg_2755" s="T168">верхняя.часть-ILL</ta>
            <ta e="T171" id="Seg_2756" s="T170">всё.[3SG.S]</ta>
            <ta e="T172" id="Seg_2757" s="T171">стоять-HAB-1SG.S</ta>
            <ta e="T173" id="Seg_2758" s="T172">смотреть-DUR-1SG.S</ta>
            <ta e="T174" id="Seg_2759" s="T173">а</ta>
            <ta e="T175" id="Seg_2760" s="T174">он(а).[NOM]</ta>
            <ta e="T176" id="Seg_2761" s="T175">тоже</ta>
            <ta e="T177" id="Seg_2762" s="T176">я.ACC</ta>
            <ta e="T178" id="Seg_2763" s="T177">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T180" id="Seg_2764" s="T179">Людка.[NOM]</ta>
            <ta e="T181" id="Seg_2765" s="T180">бегать-EP-CO.[3SG.S]</ta>
            <ta e="T182" id="Seg_2766" s="T181">%%</ta>
            <ta e="T184" id="Seg_2767" s="T183">я.NOM</ta>
            <ta e="T185" id="Seg_2768" s="T184">теперь</ta>
            <ta e="T186" id="Seg_2769" s="T185">столько</ta>
            <ta e="T187" id="Seg_2770" s="T186">стоять-HAB-1SG.S</ta>
            <ta e="T188" id="Seg_2771" s="T187">тоже</ta>
            <ta e="T189" id="Seg_2772" s="T188">отправляться-1SG.S</ta>
            <ta e="T190" id="Seg_2773" s="T189">опять</ta>
            <ta e="T191" id="Seg_2774" s="T190">смотреть-HAB-CO-1SG.O</ta>
            <ta e="T192" id="Seg_2775" s="T191">стоять-HAB.[3SG.S]</ta>
            <ta e="T193" id="Seg_2776" s="T192">опять</ta>
            <ta e="T194" id="Seg_2777" s="T193">я.ACC</ta>
            <ta e="T195" id="Seg_2778" s="T194">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T196" id="Seg_2779" s="T195">медведь.[NOM]</ta>
            <ta e="T198" id="Seg_2780" s="T197">я.NOM</ta>
            <ta e="T199" id="Seg_2781" s="T198">теперь</ta>
            <ta e="T200" id="Seg_2782" s="T199">догонять-HAB-1SG.S</ta>
            <ta e="T201" id="Seg_2783" s="T200">положить-EP-IPFV3-HAB-CO-1SG.O</ta>
            <ta e="T202" id="Seg_2784" s="T201">сюда</ta>
            <ta e="T203" id="Seg_2785" s="T202">вниз</ta>
            <ta e="T204" id="Seg_2786" s="T203">берёза-OPT.[NOM]</ta>
            <ta e="T205" id="Seg_2787" s="T204">всякий</ta>
            <ta e="T206" id="Seg_2788" s="T205">а</ta>
            <ta e="T207" id="Seg_2789" s="T206">он(а).[NOM]</ta>
            <ta e="T208" id="Seg_2790" s="T207">гнать-CVB</ta>
            <ta e="T209" id="Seg_2791" s="T208">удержать-HAB-INF</ta>
            <ta e="T211" id="Seg_2792" s="T210">Людка.[NOM]</ta>
            <ta e="T212" id="Seg_2793" s="T211">тоже</ta>
            <ta e="T213" id="Seg_2794" s="T212">маленький-ADVZ</ta>
            <ta e="T215" id="Seg_2795" s="T214">что-INDEF</ta>
            <ta e="T216" id="Seg_2796" s="T215">царапать-HAB-3SG.O</ta>
            <ta e="T217" id="Seg_2797" s="T216">сюда</ta>
            <ta e="T218" id="Seg_2798" s="T217">весь</ta>
            <ta e="T219" id="Seg_2799" s="T218">этот</ta>
            <ta e="T220" id="Seg_2800" s="T219">нечто-INSTR</ta>
            <ta e="T221" id="Seg_2801" s="T220">гриб-INSTR</ta>
            <ta e="T222" id="Seg_2802" s="T221">царапать-DUR-3SG.O</ta>
            <ta e="T223" id="Seg_2803" s="T222">тоже</ta>
            <ta e="T224" id="Seg_2804" s="T223">бегать-EP-PFV-HAB-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_2805" s="T224">я.ACC</ta>
            <ta e="T226" id="Seg_2806" s="T225">смотреть-PST.NAR.[3SG.S]</ta>
            <ta e="T228" id="Seg_2807" s="T227">болото.[NOM]</ta>
            <ta e="T229" id="Seg_2808" s="T228">через</ta>
            <ta e="T230" id="Seg_2809" s="T229">они</ta>
            <ta e="T231" id="Seg_2810" s="T230">собака.[NOM]</ta>
            <ta e="T232" id="Seg_2811" s="T231">бабушка-ALL</ta>
            <ta e="T233" id="Seg_2812" s="T232">кричать-INFER-1DU</ta>
            <ta e="T235" id="Seg_2813" s="T234">потом</ta>
            <ta e="T236" id="Seg_2814" s="T235">за</ta>
            <ta e="T237" id="Seg_2815" s="T236">%%-1DU</ta>
            <ta e="T238" id="Seg_2816" s="T237">кричать-CO-1DU</ta>
            <ta e="T239" id="Seg_2817" s="T238">бабушка.[NOM]</ta>
            <ta e="T241" id="Seg_2818" s="T240">а</ta>
            <ta e="T242" id="Seg_2819" s="T241">бабушка.[NOM]</ta>
            <ta e="T243" id="Seg_2820" s="T242">сказать.[3SG.S]</ta>
            <ta e="T244" id="Seg_2821" s="T243">что.[NOM]</ta>
            <ta e="T245" id="Seg_2822" s="T244">кричать-CO.[3SG.S]</ta>
            <ta e="T246" id="Seg_2823" s="T245">туда-ADV.LOC</ta>
            <ta e="T248" id="Seg_2824" s="T247">теперь</ta>
            <ta e="T249" id="Seg_2825" s="T248">я.NOM</ta>
            <ta e="T250" id="Seg_2826" s="T249">Людка-ALL</ta>
            <ta e="T251" id="Seg_2827" s="T250">сказать-1SG.S</ta>
            <ta e="T252" id="Seg_2828" s="T251">лицо-2SG</ta>
            <ta e="T253" id="Seg_2829" s="T252">что-TRL</ta>
            <ta e="T254" id="Seg_2830" s="T253">такой-ADVZ</ta>
            <ta e="T255" id="Seg_2831" s="T254">белый</ta>
            <ta e="T256" id="Seg_2832" s="T255">%%-PST.NAR.[3SG.S]</ta>
            <ta e="T258" id="Seg_2833" s="T257">а</ta>
            <ta e="T259" id="Seg_2834" s="T258">я.GEN</ta>
            <ta e="T260" id="Seg_2835" s="T259">дочь-1SG</ta>
            <ta e="T261" id="Seg_2836" s="T260">сказать-HAB-CO.[3SG.S]</ta>
            <ta e="T262" id="Seg_2837" s="T261">а</ta>
            <ta e="T263" id="Seg_2838" s="T262">ты.GEN</ta>
            <ta e="T264" id="Seg_2839" s="T263">тоже</ta>
            <ta e="T266" id="Seg_2840" s="T265">лицо-2SG</ta>
            <ta e="T267" id="Seg_2841" s="T266">белый-EP-VBLZ-CO.[3SG.S]</ta>
            <ta e="T269" id="Seg_2842" s="T268">ну</ta>
            <ta e="T270" id="Seg_2843" s="T269">мы.DU.NOM</ta>
            <ta e="T271" id="Seg_2844" s="T270">сила-EP-ADVZ</ta>
            <ta e="T272" id="Seg_2845" s="T271">испугаться-CO-1DU</ta>
            <ta e="T274" id="Seg_2846" s="T273">бабушка-ALL</ta>
            <ta e="T275" id="Seg_2847" s="T274">прийти-EP-1DU</ta>
            <ta e="T276" id="Seg_2848" s="T275">бабушка.[NOM]</ta>
            <ta e="T277" id="Seg_2849" s="T276">что.[NOM]</ta>
            <ta e="T278" id="Seg_2850" s="T277">случиться-CO-2SG.S</ta>
            <ta e="T280" id="Seg_2851" s="T279">смотреть-1SG.S</ta>
            <ta e="T281" id="Seg_2852" s="T280">я.NOM</ta>
            <ta e="T282" id="Seg_2853" s="T281">мы.DU.NOM</ta>
            <ta e="T283" id="Seg_2854" s="T282">медведь-ACC</ta>
            <ta e="T284" id="Seg_2855" s="T283">вдруг</ta>
            <ta e="T285" id="Seg_2856" s="T284">видеть-CO-1DU</ta>
            <ta e="T287" id="Seg_2857" s="T286">двое.1DU </ta>
            <ta e="T288" id="Seg_2858" s="T287">этот.[NOM]</ta>
            <ta e="T289" id="Seg_2859" s="T288">бегать-IPFV-1DU</ta>
            <ta e="T291" id="Seg_2860" s="T290">а</ta>
            <ta e="T292" id="Seg_2861" s="T291">бабушка.[NOM]</ta>
            <ta e="T293" id="Seg_2862" s="T292">такой-ADVZ</ta>
            <ta e="T294" id="Seg_2863" s="T293">сказать-HAB-CO.[3SG.S]</ta>
            <ta e="T295" id="Seg_2864" s="T294">а</ta>
            <ta e="T296" id="Seg_2865" s="T295">этот</ta>
            <ta e="T297" id="Seg_2866" s="T296">что.[NOM]</ta>
            <ta e="T299" id="Seg_2867" s="T298">мать-3SG</ta>
            <ta e="T300" id="Seg_2868" s="T299">я.NOM</ta>
            <ta e="T301" id="Seg_2869" s="T300">%%</ta>
            <ta e="T302" id="Seg_2870" s="T301">сюда</ta>
            <ta e="T303" id="Seg_2871" s="T302">принести-PST.NAR-3SG.O</ta>
            <ta e="T304" id="Seg_2872" s="T303">путь-GEN</ta>
            <ta e="T305" id="Seg_2873" s="T304">глаз-LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2874" s="T1">interrog</ta>
            <ta e="T3" id="Seg_2875" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2876" s="T3">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_2877" s="T5">pers</ta>
            <ta e="T306" id="Seg_2878" s="T6">n-n:case</ta>
            <ta e="T7" id="Seg_2879" s="T306">pp</ta>
            <ta e="T8" id="Seg_2880" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2881" s="T8">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_2882" s="T9">nprop-n:case</ta>
            <ta e="T10" id="Seg_2883" s="T307">pp</ta>
            <ta e="T12" id="Seg_2884" s="T11">conj</ta>
            <ta e="T13" id="Seg_2885" s="T12">pers</ta>
            <ta e="T14" id="Seg_2886" s="T13">%%</ta>
            <ta e="T15" id="Seg_2887" s="T14">v</ta>
            <ta e="T17" id="Seg_2888" s="T16">conj</ta>
            <ta e="T18" id="Seg_2889" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_2890" s="T18">nprop.[n:case]</ta>
            <ta e="T20" id="Seg_2891" s="T19">adv-adv:case</ta>
            <ta e="T22" id="Seg_2892" s="T21">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_2893" s="T23">adv</ta>
            <ta e="T25" id="Seg_2894" s="T24">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_2895" s="T26">adv</ta>
            <ta e="T28" id="Seg_2896" s="T27">v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_2897" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_2898" s="T29">nprop.[n:case]</ta>
            <ta e="T31" id="Seg_2899" s="T30">v-v:ins-v:pn</ta>
            <ta e="T32" id="Seg_2900" s="T31">prep</ta>
            <ta e="T33" id="Seg_2901" s="T32">n</ta>
            <ta e="T35" id="Seg_2902" s="T34">conj</ta>
            <ta e="T36" id="Seg_2903" s="T35">pers</ta>
            <ta e="T37" id="Seg_2904" s="T36">nprop-n:case</ta>
            <ta e="T38" id="Seg_2905" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_2906" s="T38">v-v:mood.pn</ta>
            <ta e="T40" id="Seg_2907" s="T39">adv</ta>
            <ta e="T41" id="Seg_2908" s="T40">v-v:mood-v:pn</ta>
            <ta e="T308" id="Seg_2909" s="T41">adv</ta>
            <ta e="T42" id="Seg_2910" s="T308">dem</ta>
            <ta e="T43" id="Seg_2911" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_2912" s="T43">v-v:mood-v:pn</ta>
            <ta e="T45" id="Seg_2913" s="T44">adv</ta>
            <ta e="T46" id="Seg_2914" s="T45">v</ta>
            <ta e="T47" id="Seg_2915" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_2916" s="T47">v-v:mood-v:pn-v:mood</ta>
            <ta e="T50" id="Seg_2917" s="T49">adv</ta>
            <ta e="T51" id="Seg_2918" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_2919" s="T51">v-v:pn</ta>
            <ta e="T54" id="Seg_2920" s="T53">conj</ta>
            <ta e="T55" id="Seg_2921" s="T54">nprop.[n:case]</ta>
            <ta e="T56" id="Seg_2922" s="T55">pers</ta>
            <ta e="T309" id="Seg_2923" s="T56">v.[v:pn]</ta>
            <ta e="T57" id="Seg_2924" s="T309">v</ta>
            <ta e="T310" id="Seg_2925" s="T57">adv</ta>
            <ta e="T58" id="Seg_2926" s="T310">v</ta>
            <ta e="T59" id="Seg_2927" s="T58">interrog-pro&gt;pro</ta>
            <ta e="T60" id="Seg_2928" s="T59">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T62" id="Seg_2929" s="T61">conj</ta>
            <ta e="T63" id="Seg_2930" s="T62">pers</ta>
            <ta e="T64" id="Seg_2931" s="T63">pers-n:ins-n:case</ta>
            <ta e="T65" id="Seg_2932" s="T64">adv-adj&gt;adv</ta>
            <ta e="T66" id="Seg_2933" s="T65">v-v:pn</ta>
            <ta e="T67" id="Seg_2934" s="T66">conj</ta>
            <ta e="T68" id="Seg_2935" s="T67">pers.[n:case]</ta>
            <ta e="T69" id="Seg_2936" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2937" s="T69">v-v:mood-v:pn</ta>
            <ta e="T71" id="Seg_2938" s="T70">conj</ta>
            <ta e="T72" id="Seg_2939" s="T71">n.[n:case]</ta>
            <ta e="T73" id="Seg_2940" s="T72">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T75" id="Seg_2941" s="T74">pers</ta>
            <ta e="T311" id="Seg_2942" s="T75">v-v:pn</ta>
            <ta e="T76" id="Seg_2943" s="T311">interj</ta>
            <ta e="T77" id="Seg_2944" s="T76">dem.[n:case]</ta>
            <ta e="T78" id="Seg_2945" s="T77">n-n:poss-n:num</ta>
            <ta e="T79" id="Seg_2946" s="T78">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T81" id="Seg_2947" s="T80">v-v:pn</ta>
            <ta e="T83" id="Seg_2948" s="T81">%%</ta>
            <ta e="T84" id="Seg_2949" s="T83">%%</ta>
            <ta e="T85" id="Seg_2950" s="T84">num</ta>
            <ta e="T86" id="Seg_2951" s="T85">n.[n:case]</ta>
            <ta e="T88" id="Seg_2952" s="T87">adv</ta>
            <ta e="T89" id="Seg_2953" s="T88">pers</ta>
            <ta e="T90" id="Seg_2954" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2955" s="T90">conj</ta>
            <ta e="T92" id="Seg_2956" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_2957" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_2958" s="T93">v-v:tense.[v:pn]</ta>
            <ta e="T96" id="Seg_2959" s="T95">v-v:pn</ta>
            <ta e="T99" id="Seg_2960" s="T98">pers</ta>
            <ta e="T100" id="Seg_2961" s="T99">n-n:poss</ta>
            <ta e="T101" id="Seg_2962" s="T100">v.[v:pn]</ta>
            <ta e="T102" id="Seg_2963" s="T101">n.[n:case]</ta>
            <ta e="T103" id="Seg_2964" s="T102">adv</ta>
            <ta e="T104" id="Seg_2965" s="T103">pers</ta>
            <ta e="T105" id="Seg_2966" s="T104">pers-n:case</ta>
            <ta e="T106" id="Seg_2967" s="T105">v-v:pn</ta>
            <ta e="T107" id="Seg_2968" s="T106">interrog.[n:case]</ta>
            <ta e="T108" id="Seg_2969" s="T107">n.[n:case]</ta>
            <ta e="T110" id="Seg_2970" s="T109">preverb</ta>
            <ta e="T111" id="Seg_2971" s="T110">v-v&gt;v-v:pn</ta>
            <ta e="T112" id="Seg_2972" s="T111">adv</ta>
            <ta e="T113" id="Seg_2973" s="T112">n.[n:case]</ta>
            <ta e="T114" id="Seg_2974" s="T113">v-v&gt;v.[v:pn]</ta>
            <ta e="T115" id="Seg_2975" s="T114">pers</ta>
            <ta e="T116" id="Seg_2976" s="T115">v-v:tense.[v:pn]</ta>
            <ta e="T118" id="Seg_2977" s="T117">n.[n:case]</ta>
            <ta e="T119" id="Seg_2978" s="T118">num</ta>
            <ta e="T120" id="Seg_2979" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_2980" s="T120">num</ta>
            <ta e="T122" id="Seg_2981" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_2982" s="T122">pp</ta>
            <ta e="T124" id="Seg_2983" s="T123">v-v&gt;v.[v:pn]</ta>
            <ta e="T125" id="Seg_2984" s="T124">pers</ta>
            <ta e="T126" id="Seg_2985" s="T125">v-v:tense.[v:pn]</ta>
            <ta e="T128" id="Seg_2986" s="T127">pers</ta>
            <ta e="T129" id="Seg_2987" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_2988" s="T129">v-v:ins-v:pn</ta>
            <ta e="T132" id="Seg_2989" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_2990" s="T132">adj</ta>
            <ta e="T134" id="Seg_2991" s="T133">pers</ta>
            <ta e="T135" id="Seg_2992" s="T134">v-v:mood-v:pn</ta>
            <ta e="T137" id="Seg_2993" s="T136">pers</ta>
            <ta e="T138" id="Seg_2994" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_2995" s="T138">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T140" id="Seg_2996" s="T139">n.[n:case]</ta>
            <ta e="T141" id="Seg_2997" s="T140">pp</ta>
            <ta e="T142" id="Seg_2998" s="T141">v-n:ins-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T144" id="Seg_2999" s="T143">pers</ta>
            <ta e="T145" id="Seg_3000" s="T144">n-n:poss</ta>
            <ta e="T146" id="Seg_3001" s="T145">v-n:ins-v:ins.[v:pn]</ta>
            <ta e="T147" id="Seg_3002" s="T146">adv</ta>
            <ta e="T149" id="Seg_3003" s="T148">conj</ta>
            <ta e="T150" id="Seg_3004" s="T149">pers</ta>
            <ta e="T151" id="Seg_3005" s="T150">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T152" id="Seg_3006" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_3007" s="T152">v-v:mood.pn</ta>
            <ta e="T154" id="Seg_3008" s="T153">adv</ta>
            <ta e="T155" id="Seg_3009" s="T154">v-v:mood.pn</ta>
            <ta e="T157" id="Seg_3010" s="T156">emphpro</ta>
            <ta e="T158" id="Seg_3011" s="T157">v-v&gt;v-v:pn</ta>
            <ta e="T159" id="Seg_3012" s="T158">adv</ta>
            <ta e="T160" id="Seg_3013" s="T159">v-v&gt;v-v:pn</ta>
            <ta e="T161" id="Seg_3014" s="T160">pers-n:ins-n:case</ta>
            <ta e="T162" id="Seg_3015" s="T161">v-v:pn</ta>
            <ta e="T163" id="Seg_3016" s="T162">pers</ta>
            <ta e="T164" id="Seg_3017" s="T163">interrog-n:case</ta>
            <ta e="T165" id="Seg_3018" s="T164">adv</ta>
            <ta e="T166" id="Seg_3019" s="T165">v-v:ins-v:pn</ta>
            <ta e="T167" id="Seg_3020" s="T166">pers</ta>
            <ta e="T168" id="Seg_3021" s="T167">n-n:case-n:poss</ta>
            <ta e="T169" id="Seg_3022" s="T168">n-n:case</ta>
            <ta e="T171" id="Seg_3023" s="T170">pro.[v:pn]</ta>
            <ta e="T172" id="Seg_3024" s="T171">v-v&gt;v-v:pn</ta>
            <ta e="T173" id="Seg_3025" s="T172">v-v&gt;v-v:pn</ta>
            <ta e="T174" id="Seg_3026" s="T173">conj</ta>
            <ta e="T175" id="Seg_3027" s="T174">pers.[n:case]</ta>
            <ta e="T176" id="Seg_3028" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_3029" s="T176">pers</ta>
            <ta e="T178" id="Seg_3030" s="T177">v-v:tense.[v:pn]</ta>
            <ta e="T180" id="Seg_3031" s="T179">nprop.[n:case]</ta>
            <ta e="T181" id="Seg_3032" s="T180">v-n:ins-v:ins.[v:pn]</ta>
            <ta e="T182" id="Seg_3033" s="T181">%%</ta>
            <ta e="T184" id="Seg_3034" s="T183">pers</ta>
            <ta e="T185" id="Seg_3035" s="T184">adv</ta>
            <ta e="T186" id="Seg_3036" s="T185">quant</ta>
            <ta e="T187" id="Seg_3037" s="T186">v-v&gt;v-v:pn</ta>
            <ta e="T188" id="Seg_3038" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_3039" s="T188">v-v:pn</ta>
            <ta e="T190" id="Seg_3040" s="T189">adv</ta>
            <ta e="T191" id="Seg_3041" s="T190">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T192" id="Seg_3042" s="T191">v-v&gt;v.[v:pn]</ta>
            <ta e="T193" id="Seg_3043" s="T192">adv</ta>
            <ta e="T194" id="Seg_3044" s="T193">pers</ta>
            <ta e="T195" id="Seg_3045" s="T194">v-v:tense.[v:pn]</ta>
            <ta e="T196" id="Seg_3046" s="T195">n.[n:case]</ta>
            <ta e="T198" id="Seg_3047" s="T197">pers</ta>
            <ta e="T199" id="Seg_3048" s="T198">adv</ta>
            <ta e="T200" id="Seg_3049" s="T199">v-v&gt;v-v:pn</ta>
            <ta e="T201" id="Seg_3050" s="T200">v-v:ins-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T202" id="Seg_3051" s="T201">adv</ta>
            <ta e="T203" id="Seg_3052" s="T202">adv</ta>
            <ta e="T204" id="Seg_3053" s="T203">n-v:mood.[n:case]</ta>
            <ta e="T205" id="Seg_3054" s="T204">adj</ta>
            <ta e="T206" id="Seg_3055" s="T205">conj</ta>
            <ta e="T207" id="Seg_3056" s="T206">pers.[n:case]</ta>
            <ta e="T208" id="Seg_3057" s="T207">v-v&gt;adv</ta>
            <ta e="T209" id="Seg_3058" s="T208">v-v&gt;v-v:inf</ta>
            <ta e="T211" id="Seg_3059" s="T210">nprop.[n:case]</ta>
            <ta e="T212" id="Seg_3060" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_3061" s="T212">adj-adj&gt;adv</ta>
            <ta e="T215" id="Seg_3062" s="T214">interrog-clit</ta>
            <ta e="T216" id="Seg_3063" s="T215">v-v&gt;v-v:pn</ta>
            <ta e="T217" id="Seg_3064" s="T216">adv</ta>
            <ta e="T218" id="Seg_3065" s="T217">quant</ta>
            <ta e="T219" id="Seg_3066" s="T218">dem</ta>
            <ta e="T220" id="Seg_3067" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_3068" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_3069" s="T221">v-v&gt;v-v:pn</ta>
            <ta e="T223" id="Seg_3070" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_3071" s="T223">v-n:ins-v&gt;v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T225" id="Seg_3072" s="T224">pers</ta>
            <ta e="T226" id="Seg_3073" s="T225">v-v:tense.[v:pn]</ta>
            <ta e="T228" id="Seg_3074" s="T227">n.[n:case]</ta>
            <ta e="T229" id="Seg_3075" s="T228">pp</ta>
            <ta e="T230" id="Seg_3076" s="T229">pers</ta>
            <ta e="T231" id="Seg_3077" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_3078" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_3079" s="T232">v-v:mood-v:pn</ta>
            <ta e="T235" id="Seg_3080" s="T234">adv</ta>
            <ta e="T236" id="Seg_3081" s="T235">pp</ta>
            <ta e="T237" id="Seg_3082" s="T236">v-v:pn</ta>
            <ta e="T238" id="Seg_3083" s="T237">v-v:ins-v:pn</ta>
            <ta e="T239" id="Seg_3084" s="T238">n.[n:case]</ta>
            <ta e="T241" id="Seg_3085" s="T240">conj</ta>
            <ta e="T242" id="Seg_3086" s="T241">n.[n:case]</ta>
            <ta e="T243" id="Seg_3087" s="T242">v.[v:pn]</ta>
            <ta e="T244" id="Seg_3088" s="T243">interrog.[n:case]</ta>
            <ta e="T245" id="Seg_3089" s="T244">v-v:ins.[v:pn]</ta>
            <ta e="T246" id="Seg_3090" s="T245">adv-adv:case</ta>
            <ta e="T248" id="Seg_3091" s="T247">adv</ta>
            <ta e="T249" id="Seg_3092" s="T248">pers</ta>
            <ta e="T250" id="Seg_3093" s="T249">nprop-n:case</ta>
            <ta e="T251" id="Seg_3094" s="T250">v-v:pn</ta>
            <ta e="T252" id="Seg_3095" s="T251">n-n:poss</ta>
            <ta e="T253" id="Seg_3096" s="T252">interrog-n:case</ta>
            <ta e="T254" id="Seg_3097" s="T253">dem-adj&gt;adv</ta>
            <ta e="T255" id="Seg_3098" s="T254">adj</ta>
            <ta e="T256" id="Seg_3099" s="T255">v-v:tense.[v:pn]</ta>
            <ta e="T258" id="Seg_3100" s="T257">conj</ta>
            <ta e="T259" id="Seg_3101" s="T258">pers</ta>
            <ta e="T260" id="Seg_3102" s="T259">n-n:poss</ta>
            <ta e="T261" id="Seg_3103" s="T260">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T262" id="Seg_3104" s="T261">conj</ta>
            <ta e="T263" id="Seg_3105" s="T262">pers</ta>
            <ta e="T264" id="Seg_3106" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_3107" s="T265">n-n:poss</ta>
            <ta e="T267" id="Seg_3108" s="T266">adj-n:ins-n&gt;v-v:ins.[v:pn]</ta>
            <ta e="T269" id="Seg_3109" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_3110" s="T269">pers</ta>
            <ta e="T271" id="Seg_3111" s="T270">n-n:ins-adj&gt;adv</ta>
            <ta e="T272" id="Seg_3112" s="T271">v-v:ins-v:pn</ta>
            <ta e="T274" id="Seg_3113" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_3114" s="T274">v-n:ins-v:pn</ta>
            <ta e="T276" id="Seg_3115" s="T275">n.[n:case]</ta>
            <ta e="T277" id="Seg_3116" s="T276">interrog.[n:case]</ta>
            <ta e="T278" id="Seg_3117" s="T277">v-v:ins-v:pn</ta>
            <ta e="T280" id="Seg_3118" s="T279">v-v:pn</ta>
            <ta e="T281" id="Seg_3119" s="T280">pers</ta>
            <ta e="T282" id="Seg_3120" s="T281">pers</ta>
            <ta e="T283" id="Seg_3121" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_3122" s="T283">adv</ta>
            <ta e="T285" id="Seg_3123" s="T284">v-v:ins-v:pn</ta>
            <ta e="T287" id="Seg_3124" s="T286">adv</ta>
            <ta e="T288" id="Seg_3125" s="T287">dem.[n:case]</ta>
            <ta e="T289" id="Seg_3126" s="T288">v-v&gt;v-v:pn</ta>
            <ta e="T291" id="Seg_3127" s="T290">conj</ta>
            <ta e="T292" id="Seg_3128" s="T291">n.[n:case]</ta>
            <ta e="T293" id="Seg_3129" s="T292">dem-adj&gt;adv</ta>
            <ta e="T294" id="Seg_3130" s="T293">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T295" id="Seg_3131" s="T294">conj</ta>
            <ta e="T296" id="Seg_3132" s="T295">dem</ta>
            <ta e="T297" id="Seg_3133" s="T296">interrog.[n:case]</ta>
            <ta e="T299" id="Seg_3134" s="T298">n-n:poss</ta>
            <ta e="T300" id="Seg_3135" s="T299">pers</ta>
            <ta e="T301" id="Seg_3136" s="T300">%%</ta>
            <ta e="T302" id="Seg_3137" s="T301">adv</ta>
            <ta e="T303" id="Seg_3138" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_3139" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_3140" s="T304">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_3141" s="T1">interrog</ta>
            <ta e="T3" id="Seg_3142" s="T2">n</ta>
            <ta e="T4" id="Seg_3143" s="T3">v</ta>
            <ta e="T6" id="Seg_3144" s="T5">pers</ta>
            <ta e="T306" id="Seg_3145" s="T6">n</ta>
            <ta e="T7" id="Seg_3146" s="T306">pp</ta>
            <ta e="T8" id="Seg_3147" s="T7">n</ta>
            <ta e="T9" id="Seg_3148" s="T8">v</ta>
            <ta e="T307" id="Seg_3149" s="T9">n</ta>
            <ta e="T10" id="Seg_3150" s="T307">pp</ta>
            <ta e="T12" id="Seg_3151" s="T11">conj</ta>
            <ta e="T13" id="Seg_3152" s="T12">pers</ta>
            <ta e="T14" id="Seg_3153" s="T13">%%</ta>
            <ta e="T15" id="Seg_3154" s="T14">v</ta>
            <ta e="T17" id="Seg_3155" s="T16">conj</ta>
            <ta e="T18" id="Seg_3156" s="T17">n</ta>
            <ta e="T19" id="Seg_3157" s="T18">nprop</ta>
            <ta e="T20" id="Seg_3158" s="T19">adv</ta>
            <ta e="T22" id="Seg_3159" s="T21">v</ta>
            <ta e="T24" id="Seg_3160" s="T23">adv</ta>
            <ta e="T25" id="Seg_3161" s="T24">v</ta>
            <ta e="T27" id="Seg_3162" s="T26">adv</ta>
            <ta e="T28" id="Seg_3163" s="T27">v</ta>
            <ta e="T29" id="Seg_3164" s="T28">n</ta>
            <ta e="T30" id="Seg_3165" s="T29">nprop</ta>
            <ta e="T31" id="Seg_3166" s="T30">v</ta>
            <ta e="T32" id="Seg_3167" s="T31">prep</ta>
            <ta e="T33" id="Seg_3168" s="T32">n</ta>
            <ta e="T35" id="Seg_3169" s="T34">conj</ta>
            <ta e="T36" id="Seg_3170" s="T35">pers</ta>
            <ta e="T37" id="Seg_3171" s="T36">nprop</ta>
            <ta e="T38" id="Seg_3172" s="T37">v</ta>
            <ta e="T39" id="Seg_3173" s="T38">v</ta>
            <ta e="T40" id="Seg_3174" s="T39">adv</ta>
            <ta e="T41" id="Seg_3175" s="T40">v</ta>
            <ta e="T308" id="Seg_3176" s="T41">adv</ta>
            <ta e="T42" id="Seg_3177" s="T308">dem</ta>
            <ta e="T43" id="Seg_3178" s="T42">n</ta>
            <ta e="T44" id="Seg_3179" s="T43">v</ta>
            <ta e="T45" id="Seg_3180" s="T44">adv</ta>
            <ta e="T46" id="Seg_3181" s="T45">v</ta>
            <ta e="T47" id="Seg_3182" s="T46">n</ta>
            <ta e="T48" id="Seg_3183" s="T47">v</ta>
            <ta e="T50" id="Seg_3184" s="T49">adv</ta>
            <ta e="T51" id="Seg_3185" s="T50">v</ta>
            <ta e="T52" id="Seg_3186" s="T51">v</ta>
            <ta e="T54" id="Seg_3187" s="T53">conj</ta>
            <ta e="T55" id="Seg_3188" s="T54">nprop</ta>
            <ta e="T56" id="Seg_3189" s="T55">pers</ta>
            <ta e="T309" id="Seg_3190" s="T56">v</ta>
            <ta e="T57" id="Seg_3191" s="T309">v</ta>
            <ta e="T310" id="Seg_3192" s="T57">adv</ta>
            <ta e="T58" id="Seg_3193" s="T310">v</ta>
            <ta e="T59" id="Seg_3194" s="T58">pro</ta>
            <ta e="T60" id="Seg_3195" s="T59">v</ta>
            <ta e="T62" id="Seg_3196" s="T61">conj</ta>
            <ta e="T63" id="Seg_3197" s="T62">pers</ta>
            <ta e="T64" id="Seg_3198" s="T63">pers</ta>
            <ta e="T65" id="Seg_3199" s="T64">adv</ta>
            <ta e="T66" id="Seg_3200" s="T65">v</ta>
            <ta e="T67" id="Seg_3201" s="T66">conj</ta>
            <ta e="T68" id="Seg_3202" s="T67">pers</ta>
            <ta e="T69" id="Seg_3203" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_3204" s="T69">v</ta>
            <ta e="T71" id="Seg_3205" s="T70">conj</ta>
            <ta e="T72" id="Seg_3206" s="T71">n</ta>
            <ta e="T73" id="Seg_3207" s="T72">v</ta>
            <ta e="T75" id="Seg_3208" s="T74">pers</ta>
            <ta e="T311" id="Seg_3209" s="T75">v</ta>
            <ta e="T76" id="Seg_3210" s="T311">interj</ta>
            <ta e="T77" id="Seg_3211" s="T76">dem</ta>
            <ta e="T78" id="Seg_3212" s="T77">n</ta>
            <ta e="T79" id="Seg_3213" s="T78">v</ta>
            <ta e="T81" id="Seg_3214" s="T80">v</ta>
            <ta e="T83" id="Seg_3215" s="T81">%%</ta>
            <ta e="T84" id="Seg_3216" s="T83">%%</ta>
            <ta e="T85" id="Seg_3217" s="T84">num</ta>
            <ta e="T86" id="Seg_3218" s="T85">n</ta>
            <ta e="T88" id="Seg_3219" s="T87">adv</ta>
            <ta e="T89" id="Seg_3220" s="T88">pers</ta>
            <ta e="T90" id="Seg_3221" s="T89">v</ta>
            <ta e="T91" id="Seg_3222" s="T90">conj</ta>
            <ta e="T92" id="Seg_3223" s="T91">n</ta>
            <ta e="T93" id="Seg_3224" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_3225" s="T93">v</ta>
            <ta e="T96" id="Seg_3226" s="T95">v</ta>
            <ta e="T99" id="Seg_3227" s="T98">pers</ta>
            <ta e="T100" id="Seg_3228" s="T99">n</ta>
            <ta e="T101" id="Seg_3229" s="T100">v</ta>
            <ta e="T102" id="Seg_3230" s="T101">n</ta>
            <ta e="T103" id="Seg_3231" s="T102">adv</ta>
            <ta e="T104" id="Seg_3232" s="T103">pers</ta>
            <ta e="T105" id="Seg_3233" s="T104">pers</ta>
            <ta e="T106" id="Seg_3234" s="T105">v</ta>
            <ta e="T107" id="Seg_3235" s="T106">interrog</ta>
            <ta e="T108" id="Seg_3236" s="T107">n</ta>
            <ta e="T110" id="Seg_3237" s="T109">preverb</ta>
            <ta e="T111" id="Seg_3238" s="T110">v</ta>
            <ta e="T112" id="Seg_3239" s="T111">adv</ta>
            <ta e="T113" id="Seg_3240" s="T112">n</ta>
            <ta e="T114" id="Seg_3241" s="T113">v</ta>
            <ta e="T115" id="Seg_3242" s="T114">pers</ta>
            <ta e="T116" id="Seg_3243" s="T115">v</ta>
            <ta e="T118" id="Seg_3244" s="T117">n</ta>
            <ta e="T119" id="Seg_3245" s="T118">num</ta>
            <ta e="T120" id="Seg_3246" s="T119">n</ta>
            <ta e="T121" id="Seg_3247" s="T120">num</ta>
            <ta e="T122" id="Seg_3248" s="T121">n</ta>
            <ta e="T123" id="Seg_3249" s="T122">pp</ta>
            <ta e="T124" id="Seg_3250" s="T123">v</ta>
            <ta e="T125" id="Seg_3251" s="T124">pers</ta>
            <ta e="T126" id="Seg_3252" s="T125">v</ta>
            <ta e="T128" id="Seg_3253" s="T127">pers</ta>
            <ta e="T129" id="Seg_3254" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3255" s="T129">v</ta>
            <ta e="T132" id="Seg_3256" s="T131">v</ta>
            <ta e="T133" id="Seg_3257" s="T132">adj</ta>
            <ta e="T134" id="Seg_3258" s="T133">pers</ta>
            <ta e="T135" id="Seg_3259" s="T134">v</ta>
            <ta e="T137" id="Seg_3260" s="T136">pers</ta>
            <ta e="T138" id="Seg_3261" s="T137">n</ta>
            <ta e="T139" id="Seg_3262" s="T138">v</ta>
            <ta e="T140" id="Seg_3263" s="T139">n</ta>
            <ta e="T141" id="Seg_3264" s="T140">pp</ta>
            <ta e="T142" id="Seg_3265" s="T141">v</ta>
            <ta e="T144" id="Seg_3266" s="T143">pers</ta>
            <ta e="T145" id="Seg_3267" s="T144">n</ta>
            <ta e="T146" id="Seg_3268" s="T145">v</ta>
            <ta e="T147" id="Seg_3269" s="T146">adv</ta>
            <ta e="T149" id="Seg_3270" s="T148">conj</ta>
            <ta e="T150" id="Seg_3271" s="T149">pers</ta>
            <ta e="T151" id="Seg_3272" s="T150">v</ta>
            <ta e="T152" id="Seg_3273" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_3274" s="T152">v</ta>
            <ta e="T154" id="Seg_3275" s="T153">adv</ta>
            <ta e="T155" id="Seg_3276" s="T154">v</ta>
            <ta e="T157" id="Seg_3277" s="T156">emphpro</ta>
            <ta e="T158" id="Seg_3278" s="T157">v</ta>
            <ta e="T159" id="Seg_3279" s="T158">adv</ta>
            <ta e="T160" id="Seg_3280" s="T159">v</ta>
            <ta e="T161" id="Seg_3281" s="T160">pers</ta>
            <ta e="T162" id="Seg_3282" s="T161">v</ta>
            <ta e="T163" id="Seg_3283" s="T162">pers</ta>
            <ta e="T164" id="Seg_3284" s="T163">interrog</ta>
            <ta e="T165" id="Seg_3285" s="T164">adv</ta>
            <ta e="T166" id="Seg_3286" s="T165">v</ta>
            <ta e="T167" id="Seg_3287" s="T166">pers</ta>
            <ta e="T168" id="Seg_3288" s="T167">n</ta>
            <ta e="T169" id="Seg_3289" s="T168">n</ta>
            <ta e="T171" id="Seg_3290" s="T170">pro</ta>
            <ta e="T172" id="Seg_3291" s="T171">v</ta>
            <ta e="T173" id="Seg_3292" s="T172">v</ta>
            <ta e="T174" id="Seg_3293" s="T173">conj</ta>
            <ta e="T175" id="Seg_3294" s="T174">pers</ta>
            <ta e="T176" id="Seg_3295" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_3296" s="T176">pers</ta>
            <ta e="T178" id="Seg_3297" s="T177">v</ta>
            <ta e="T180" id="Seg_3298" s="T179">nprop</ta>
            <ta e="T181" id="Seg_3299" s="T180">v</ta>
            <ta e="T182" id="Seg_3300" s="T181">pro</ta>
            <ta e="T184" id="Seg_3301" s="T183">pers</ta>
            <ta e="T185" id="Seg_3302" s="T184">adv</ta>
            <ta e="T186" id="Seg_3303" s="T185">adv</ta>
            <ta e="T187" id="Seg_3304" s="T186">v</ta>
            <ta e="T188" id="Seg_3305" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_3306" s="T188">v</ta>
            <ta e="T190" id="Seg_3307" s="T189">adv</ta>
            <ta e="T191" id="Seg_3308" s="T190">v</ta>
            <ta e="T192" id="Seg_3309" s="T191">v</ta>
            <ta e="T193" id="Seg_3310" s="T192">adv</ta>
            <ta e="T194" id="Seg_3311" s="T193">pers</ta>
            <ta e="T195" id="Seg_3312" s="T194">v</ta>
            <ta e="T196" id="Seg_3313" s="T195">n</ta>
            <ta e="T198" id="Seg_3314" s="T197">pers</ta>
            <ta e="T199" id="Seg_3315" s="T198">adv</ta>
            <ta e="T200" id="Seg_3316" s="T199">v</ta>
            <ta e="T201" id="Seg_3317" s="T200">v</ta>
            <ta e="T202" id="Seg_3318" s="T201">adv</ta>
            <ta e="T203" id="Seg_3319" s="T202">adv</ta>
            <ta e="T204" id="Seg_3320" s="T203">n</ta>
            <ta e="T205" id="Seg_3321" s="T204">adj</ta>
            <ta e="T206" id="Seg_3322" s="T205">conj</ta>
            <ta e="T207" id="Seg_3323" s="T206">pers</ta>
            <ta e="T208" id="Seg_3324" s="T207">v</ta>
            <ta e="T209" id="Seg_3325" s="T208">v</ta>
            <ta e="T211" id="Seg_3326" s="T210">nprop</ta>
            <ta e="T212" id="Seg_3327" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_3328" s="T212">adv</ta>
            <ta e="T215" id="Seg_3329" s="T214">pro</ta>
            <ta e="T216" id="Seg_3330" s="T215">v</ta>
            <ta e="T217" id="Seg_3331" s="T216">adv</ta>
            <ta e="T218" id="Seg_3332" s="T217">quant</ta>
            <ta e="T219" id="Seg_3333" s="T218">dem</ta>
            <ta e="T220" id="Seg_3334" s="T219">n</ta>
            <ta e="T221" id="Seg_3335" s="T220">n</ta>
            <ta e="T222" id="Seg_3336" s="T221">v</ta>
            <ta e="T223" id="Seg_3337" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_3338" s="T223">v</ta>
            <ta e="T225" id="Seg_3339" s="T224">pers</ta>
            <ta e="T226" id="Seg_3340" s="T225">v</ta>
            <ta e="T228" id="Seg_3341" s="T227">n</ta>
            <ta e="T229" id="Seg_3342" s="T228">pp</ta>
            <ta e="T230" id="Seg_3343" s="T229">pers</ta>
            <ta e="T231" id="Seg_3344" s="T230">n</ta>
            <ta e="T232" id="Seg_3345" s="T231">n</ta>
            <ta e="T233" id="Seg_3346" s="T232">v</ta>
            <ta e="T235" id="Seg_3347" s="T234">adv</ta>
            <ta e="T236" id="Seg_3348" s="T235">pp</ta>
            <ta e="T237" id="Seg_3349" s="T236">v</ta>
            <ta e="T238" id="Seg_3350" s="T237">v</ta>
            <ta e="T239" id="Seg_3351" s="T238">n</ta>
            <ta e="T241" id="Seg_3352" s="T240">conj</ta>
            <ta e="T242" id="Seg_3353" s="T241">n</ta>
            <ta e="T243" id="Seg_3354" s="T242">v</ta>
            <ta e="T244" id="Seg_3355" s="T243">interrog</ta>
            <ta e="T245" id="Seg_3356" s="T244">v</ta>
            <ta e="T246" id="Seg_3357" s="T245">adv</ta>
            <ta e="T248" id="Seg_3358" s="T247">adv</ta>
            <ta e="T249" id="Seg_3359" s="T248">pers</ta>
            <ta e="T250" id="Seg_3360" s="T249">nprop</ta>
            <ta e="T251" id="Seg_3361" s="T250">v</ta>
            <ta e="T252" id="Seg_3362" s="T251">n</ta>
            <ta e="T253" id="Seg_3363" s="T252">interrog</ta>
            <ta e="T254" id="Seg_3364" s="T253">adv</ta>
            <ta e="T255" id="Seg_3365" s="T254">adj</ta>
            <ta e="T256" id="Seg_3366" s="T255">v</ta>
            <ta e="T258" id="Seg_3367" s="T257">conj</ta>
            <ta e="T259" id="Seg_3368" s="T258">pers</ta>
            <ta e="T260" id="Seg_3369" s="T259">n</ta>
            <ta e="T261" id="Seg_3370" s="T260">v</ta>
            <ta e="T262" id="Seg_3371" s="T261">conj</ta>
            <ta e="T263" id="Seg_3372" s="T262">pers</ta>
            <ta e="T264" id="Seg_3373" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_3374" s="T265">n</ta>
            <ta e="T267" id="Seg_3375" s="T266">adj</ta>
            <ta e="T269" id="Seg_3376" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_3377" s="T269">pers</ta>
            <ta e="T271" id="Seg_3378" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_3379" s="T271">v</ta>
            <ta e="T274" id="Seg_3380" s="T273">n</ta>
            <ta e="T275" id="Seg_3381" s="T274">v</ta>
            <ta e="T276" id="Seg_3382" s="T275">n</ta>
            <ta e="T277" id="Seg_3383" s="T276">interrog</ta>
            <ta e="T278" id="Seg_3384" s="T277">v</ta>
            <ta e="T280" id="Seg_3385" s="T279">v</ta>
            <ta e="T281" id="Seg_3386" s="T280">pers</ta>
            <ta e="T282" id="Seg_3387" s="T281">pers</ta>
            <ta e="T283" id="Seg_3388" s="T282">n</ta>
            <ta e="T284" id="Seg_3389" s="T283">adv</ta>
            <ta e="T285" id="Seg_3390" s="T284">v</ta>
            <ta e="T287" id="Seg_3391" s="T286">adv</ta>
            <ta e="T288" id="Seg_3392" s="T287">dem</ta>
            <ta e="T289" id="Seg_3393" s="T288">v</ta>
            <ta e="T291" id="Seg_3394" s="T290">conj</ta>
            <ta e="T292" id="Seg_3395" s="T291">n</ta>
            <ta e="T293" id="Seg_3396" s="T292">adv</ta>
            <ta e="T294" id="Seg_3397" s="T293">v</ta>
            <ta e="T295" id="Seg_3398" s="T294">conj</ta>
            <ta e="T296" id="Seg_3399" s="T295">dem</ta>
            <ta e="T297" id="Seg_3400" s="T296">interrog</ta>
            <ta e="T299" id="Seg_3401" s="T298">n</ta>
            <ta e="T300" id="Seg_3402" s="T299">pers</ta>
            <ta e="T302" id="Seg_3403" s="T301">adv</ta>
            <ta e="T303" id="Seg_3404" s="T302">v</ta>
            <ta e="T304" id="Seg_3405" s="T303">n</ta>
            <ta e="T305" id="Seg_3406" s="T304">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_3407" s="T2">np:Th</ta>
            <ta e="T4" id="Seg_3408" s="T3">0.3.h:E</ta>
            <ta e="T6" id="Seg_3409" s="T5">pro.h:A</ta>
            <ta e="T306" id="Seg_3410" s="T6">pp:Com</ta>
            <ta e="T307" id="Seg_3411" s="T9">pp:Com</ta>
            <ta e="T13" id="Seg_3412" s="T12">pro.h:A</ta>
            <ta e="T19" id="Seg_3413" s="T18">np.h:Th</ta>
            <ta e="T20" id="Seg_3414" s="T19">adv:L</ta>
            <ta e="T25" id="Seg_3415" s="T24">0.1.h:A</ta>
            <ta e="T27" id="Seg_3416" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_3417" s="T27">0.1.h:A</ta>
            <ta e="T30" id="Seg_3418" s="T29">np.h:Th</ta>
            <ta e="T33" id="Seg_3419" s="T32">np:L</ta>
            <ta e="T36" id="Seg_3420" s="T35">pro.h:A</ta>
            <ta e="T37" id="Seg_3421" s="T36">np.h:R</ta>
            <ta e="T39" id="Seg_3422" s="T38">0.2.h:A</ta>
            <ta e="T40" id="Seg_3423" s="T39">adv:G</ta>
            <ta e="T41" id="Seg_3424" s="T40">0.1.h:A</ta>
            <ta e="T43" id="Seg_3425" s="T42">np:L</ta>
            <ta e="T44" id="Seg_3426" s="T43">0.1.h:A</ta>
            <ta e="T47" id="Seg_3427" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_3428" s="T47">0.1.h:B</ta>
            <ta e="T50" id="Seg_3429" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_3430" s="T50">0.1.h:A</ta>
            <ta e="T52" id="Seg_3431" s="T51">0.1.h:A</ta>
            <ta e="T55" id="Seg_3432" s="T54">np.h:A</ta>
            <ta e="T56" id="Seg_3433" s="T55">pro.h:R</ta>
            <ta e="T310" id="Seg_3434" s="T57">adv:L</ta>
            <ta e="T59" id="Seg_3435" s="T58">pro.h:A</ta>
            <ta e="T63" id="Seg_3436" s="T62">pro.h:A</ta>
            <ta e="T64" id="Seg_3437" s="T63">pro.h:R</ta>
            <ta e="T68" id="Seg_3438" s="T67">pro.h:E</ta>
            <ta e="T72" id="Seg_3439" s="T71">np:A</ta>
            <ta e="T75" id="Seg_3440" s="T74">pro.h:A</ta>
            <ta e="T78" id="Seg_3441" s="T77">np.h:A</ta>
            <ta e="T81" id="Seg_3442" s="T80">0.1.h:A</ta>
            <ta e="T86" id="Seg_3443" s="T85">np:Time</ta>
            <ta e="T88" id="Seg_3444" s="T87">adv:Time</ta>
            <ta e="T89" id="Seg_3445" s="T88">pro.h:A</ta>
            <ta e="T92" id="Seg_3446" s="T91">np.h:Th</ta>
            <ta e="T94" id="Seg_3447" s="T93">0.3:A</ta>
            <ta e="T96" id="Seg_3448" s="T95">0.1.h:A</ta>
            <ta e="T99" id="Seg_3449" s="T98">pro.h:G</ta>
            <ta e="T100" id="Seg_3450" s="T99">np.h:A 0.1.h:Poss</ta>
            <ta e="T104" id="Seg_3451" s="T103">pro.h:A</ta>
            <ta e="T105" id="Seg_3452" s="T104">pro.h:R</ta>
            <ta e="T111" id="Seg_3453" s="T110">0.1.h:A</ta>
            <ta e="T113" id="Seg_3454" s="T112">np:Th</ta>
            <ta e="T115" id="Seg_3455" s="T114">pro.h:Th</ta>
            <ta e="T116" id="Seg_3456" s="T115">0.3:A</ta>
            <ta e="T124" id="Seg_3457" s="T123">0.3:Th</ta>
            <ta e="T125" id="Seg_3458" s="T124">pro.h:Th</ta>
            <ta e="T126" id="Seg_3459" s="T125">0.3:A</ta>
            <ta e="T128" id="Seg_3460" s="T127">pro.h:E</ta>
            <ta e="T132" id="Seg_3461" s="T131">0.1.h:E</ta>
            <ta e="T134" id="Seg_3462" s="T133">pro.h:P</ta>
            <ta e="T135" id="Seg_3463" s="T134">0.2:A</ta>
            <ta e="T137" id="Seg_3464" s="T136">pro.h:A</ta>
            <ta e="T138" id="Seg_3465" s="T137">np.h:R</ta>
            <ta e="T140" id="Seg_3466" s="T139">pp:G</ta>
            <ta e="T142" id="Seg_3467" s="T141">0.2.h:A</ta>
            <ta e="T144" id="Seg_3468" s="T143">pro.h:Poss</ta>
            <ta e="T145" id="Seg_3469" s="T144">np.h:A</ta>
            <ta e="T150" id="Seg_3470" s="T149">pro.h:A</ta>
            <ta e="T153" id="Seg_3471" s="T152">0.2.h:A</ta>
            <ta e="T155" id="Seg_3472" s="T154">0.2.h:A</ta>
            <ta e="T158" id="Seg_3473" s="T157">0.1.h:Th</ta>
            <ta e="T159" id="Seg_3474" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_3475" s="T159">0.1.h:A</ta>
            <ta e="T161" id="Seg_3476" s="T160">pro:R</ta>
            <ta e="T162" id="Seg_3477" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_3478" s="T162">pro:A</ta>
            <ta e="T165" id="Seg_3479" s="T164">adv:G</ta>
            <ta e="T167" id="Seg_3480" s="T166">pro.h:Poss</ta>
            <ta e="T168" id="Seg_3481" s="T167">np:Poss</ta>
            <ta e="T169" id="Seg_3482" s="T168">np:G</ta>
            <ta e="T172" id="Seg_3483" s="T171">0.1.h:Th</ta>
            <ta e="T173" id="Seg_3484" s="T172">0.1.h:A</ta>
            <ta e="T175" id="Seg_3485" s="T174">pro:A</ta>
            <ta e="T177" id="Seg_3486" s="T176">pro.h:Th</ta>
            <ta e="T180" id="Seg_3487" s="T179">np.h:A</ta>
            <ta e="T184" id="Seg_3488" s="T183">pro.h:Th</ta>
            <ta e="T185" id="Seg_3489" s="T184">adv:Time</ta>
            <ta e="T189" id="Seg_3490" s="T188">0.1.h:A</ta>
            <ta e="T191" id="Seg_3491" s="T190">0.1.h:A</ta>
            <ta e="T194" id="Seg_3492" s="T193">pro.h:Th</ta>
            <ta e="T196" id="Seg_3493" s="T195">np:A</ta>
            <ta e="T198" id="Seg_3494" s="T197">pro.h:A</ta>
            <ta e="T199" id="Seg_3495" s="T198">adv:Time</ta>
            <ta e="T201" id="Seg_3496" s="T200">0.1.h:A</ta>
            <ta e="T202" id="Seg_3497" s="T201">adv:L</ta>
            <ta e="T204" id="Seg_3498" s="T203">np:Th</ta>
            <ta e="T207" id="Seg_3499" s="T206">pro.h:A</ta>
            <ta e="T211" id="Seg_3500" s="T210">np.h:A</ta>
            <ta e="T215" id="Seg_3501" s="T214">pro:P</ta>
            <ta e="T220" id="Seg_3502" s="T219">np:Ins</ta>
            <ta e="T221" id="Seg_3503" s="T220">np:Ins</ta>
            <ta e="T222" id="Seg_3504" s="T221">0.3.h:A 0.3:P</ta>
            <ta e="T224" id="Seg_3505" s="T223">0.3.h:A</ta>
            <ta e="T225" id="Seg_3506" s="T224">pro.h:Th</ta>
            <ta e="T226" id="Seg_3507" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_3508" s="T227">pp:L</ta>
            <ta e="T230" id="Seg_3509" s="T229">pro.h:Th</ta>
            <ta e="T232" id="Seg_3510" s="T231">np.h:R</ta>
            <ta e="T233" id="Seg_3511" s="T232">0.1.h:A</ta>
            <ta e="T235" id="Seg_3512" s="T234">adv:Time</ta>
            <ta e="T237" id="Seg_3513" s="T236">0.1.h:A</ta>
            <ta e="T238" id="Seg_3514" s="T237">0.1.h:A</ta>
            <ta e="T242" id="Seg_3515" s="T241">np.h:A</ta>
            <ta e="T244" id="Seg_3516" s="T243">pro.h:A</ta>
            <ta e="T246" id="Seg_3517" s="T245">adv:L</ta>
            <ta e="T248" id="Seg_3518" s="T247">adv:Time</ta>
            <ta e="T249" id="Seg_3519" s="T248">pro.h:A</ta>
            <ta e="T250" id="Seg_3520" s="T249">np.h:R</ta>
            <ta e="T252" id="Seg_3521" s="T251">np:Th 0.2.h:Poss</ta>
            <ta e="T259" id="Seg_3522" s="T258">pro.h:Poss</ta>
            <ta e="T260" id="Seg_3523" s="T259">np.h:A </ta>
            <ta e="T263" id="Seg_3524" s="T262">pro.h:Poss</ta>
            <ta e="T266" id="Seg_3525" s="T265">np:P</ta>
            <ta e="T270" id="Seg_3526" s="T269">pro.h:E</ta>
            <ta e="T274" id="Seg_3527" s="T273">np.h:G</ta>
            <ta e="T275" id="Seg_3528" s="T274">0.1.h:A</ta>
            <ta e="T278" id="Seg_3529" s="T277">0.2.h:P</ta>
            <ta e="T281" id="Seg_3530" s="T280">pro.h:A</ta>
            <ta e="T282" id="Seg_3531" s="T281">pro.h:E</ta>
            <ta e="T283" id="Seg_3532" s="T282">np:Th</ta>
            <ta e="T289" id="Seg_3533" s="T288">0.1.h:A</ta>
            <ta e="T292" id="Seg_3534" s="T291">np.h:A</ta>
            <ta e="T299" id="Seg_3535" s="T298">np.h:A</ta>
            <ta e="T300" id="Seg_3536" s="T299">pro.h:Poss</ta>
            <ta e="T304" id="Seg_3537" s="T303">np:Poss</ta>
            <ta e="T305" id="Seg_3538" s="T304">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_3539" s="T2">np:O</ta>
            <ta e="T4" id="Seg_3540" s="T3">0.3.h:S v:pred</ta>
            <ta e="T6" id="Seg_3541" s="T5">pro.h:S</ta>
            <ta e="T9" id="Seg_3542" s="T8">v:pred</ta>
            <ta e="T13" id="Seg_3543" s="T12">pro.h:S</ta>
            <ta e="T15" id="Seg_3544" s="T14">v:pred</ta>
            <ta e="T19" id="Seg_3545" s="T18">np.h:S</ta>
            <ta e="T22" id="Seg_3546" s="T21">v:pred</ta>
            <ta e="T25" id="Seg_3547" s="T24">0.1.h:S v:pred</ta>
            <ta e="T28" id="Seg_3548" s="T27">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_3549" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_3550" s="T30">v:pred</ta>
            <ta e="T36" id="Seg_3551" s="T35">pro.h:S</ta>
            <ta e="T38" id="Seg_3552" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_3553" s="T38">0.2.h:S v:pred</ta>
            <ta e="T41" id="Seg_3554" s="T40">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_3555" s="T43">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_3556" s="T46">np:O</ta>
            <ta e="T48" id="Seg_3557" s="T47">0.1.h:S v:pred</ta>
            <ta e="T51" id="Seg_3558" s="T50">0.1.h:S v:pred</ta>
            <ta e="T52" id="Seg_3559" s="T51">0.1.h:S v:pred</ta>
            <ta e="T55" id="Seg_3560" s="T54">np.h:S</ta>
            <ta e="T309" id="Seg_3561" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_3562" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_3563" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_3564" s="T62">pro.h:S</ta>
            <ta e="T66" id="Seg_3565" s="T65">v:pred</ta>
            <ta e="T70" id="Seg_3566" s="T66">s:purp</ta>
            <ta e="T73" id="Seg_3567" s="T70">s:compl</ta>
            <ta e="T75" id="Seg_3568" s="T74">pro.h:S</ta>
            <ta e="T311" id="Seg_3569" s="T75">v:pred</ta>
            <ta e="T78" id="Seg_3570" s="T77">np.h:S</ta>
            <ta e="T79" id="Seg_3571" s="T78">v:pred</ta>
            <ta e="T81" id="Seg_3572" s="T80">0.1.h:S v:pred</ta>
            <ta e="T89" id="Seg_3573" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_3574" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_3575" s="T91">np.h:O</ta>
            <ta e="T94" id="Seg_3576" s="T93">0.3:S v:pred</ta>
            <ta e="T96" id="Seg_3577" s="T95">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_3578" s="T99">np.h:S</ta>
            <ta e="T101" id="Seg_3579" s="T100">v:pred</ta>
            <ta e="T104" id="Seg_3580" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_3581" s="T105">v:pred</ta>
            <ta e="T111" id="Seg_3582" s="T110">0.1.h:S v:pred</ta>
            <ta e="T113" id="Seg_3583" s="T112">np:S</ta>
            <ta e="T114" id="Seg_3584" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_3585" s="T114">pro.h:O</ta>
            <ta e="T116" id="Seg_3586" s="T115">0.3:S v:pred</ta>
            <ta e="T124" id="Seg_3587" s="T123">0.3:S v:pred</ta>
            <ta e="T125" id="Seg_3588" s="T124">pro.h:O</ta>
            <ta e="T126" id="Seg_3589" s="T125">0.3:S v:pred</ta>
            <ta e="T128" id="Seg_3590" s="T127">pro.h:S</ta>
            <ta e="T130" id="Seg_3591" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_3592" s="T131">0.1.h:S v:pred</ta>
            <ta e="T134" id="Seg_3593" s="T133">pro.h:O</ta>
            <ta e="T135" id="Seg_3594" s="T134">0.2:S v:pred</ta>
            <ta e="T137" id="Seg_3595" s="T136">pro.h:S</ta>
            <ta e="T139" id="Seg_3596" s="T138">v:pred</ta>
            <ta e="T142" id="Seg_3597" s="T141">0.2.h:S v:pred</ta>
            <ta e="T145" id="Seg_3598" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_3599" s="T145">v:pred</ta>
            <ta e="T150" id="Seg_3600" s="T149">pro.h:S</ta>
            <ta e="T151" id="Seg_3601" s="T150">v:pred</ta>
            <ta e="T153" id="Seg_3602" s="T152">0.2.h:S v:pred</ta>
            <ta e="T155" id="Seg_3603" s="T154">0.2.h:S v:pred</ta>
            <ta e="T158" id="Seg_3604" s="T157">0.1.h:S v:pred</ta>
            <ta e="T160" id="Seg_3605" s="T159">0.1.h:S v:pred</ta>
            <ta e="T162" id="Seg_3606" s="T161">0.1.h:S v:pred</ta>
            <ta e="T163" id="Seg_3607" s="T162">pro:S</ta>
            <ta e="T166" id="Seg_3608" s="T165">v:pred</ta>
            <ta e="T172" id="Seg_3609" s="T171">0.1.h:S v:pred</ta>
            <ta e="T173" id="Seg_3610" s="T172">0.1.h:S v:pred</ta>
            <ta e="T175" id="Seg_3611" s="T174">pro:S</ta>
            <ta e="T177" id="Seg_3612" s="T176">pro.h:O</ta>
            <ta e="T178" id="Seg_3613" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_3614" s="T179">np.h:S</ta>
            <ta e="T181" id="Seg_3615" s="T180">v:pred</ta>
            <ta e="T184" id="Seg_3616" s="T183">pro.h:S</ta>
            <ta e="T187" id="Seg_3617" s="T186">v:pred</ta>
            <ta e="T189" id="Seg_3618" s="T188">0.1.h:S v:pred</ta>
            <ta e="T191" id="Seg_3619" s="T190">0.1.h:S v:pred</ta>
            <ta e="T192" id="Seg_3620" s="T191">v:pred</ta>
            <ta e="T194" id="Seg_3621" s="T193">pro.h:O</ta>
            <ta e="T195" id="Seg_3622" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_3623" s="T195">np:S</ta>
            <ta e="T198" id="Seg_3624" s="T197">pro.h:S</ta>
            <ta e="T200" id="Seg_3625" s="T199">v:pred</ta>
            <ta e="T201" id="Seg_3626" s="T200">0.1.h:S v:pred</ta>
            <ta e="T204" id="Seg_3627" s="T203">np:O</ta>
            <ta e="T208" id="Seg_3628" s="T205">s:temp</ta>
            <ta e="T211" id="Seg_3629" s="T210">np.h:S</ta>
            <ta e="T215" id="Seg_3630" s="T214">pro:O</ta>
            <ta e="T216" id="Seg_3631" s="T215">v:pred</ta>
            <ta e="T222" id="Seg_3632" s="T221">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T224" id="Seg_3633" s="T223">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_3634" s="T224">pro.h:O</ta>
            <ta e="T226" id="Seg_3635" s="T225">0.3.h:S v:pred</ta>
            <ta e="T230" id="Seg_3636" s="T229">pro.h:S</ta>
            <ta e="T233" id="Seg_3637" s="T232">0.1.h:S v:pred</ta>
            <ta e="T237" id="Seg_3638" s="T236">0.1.h:S v:pred</ta>
            <ta e="T238" id="Seg_3639" s="T237">0.1.h:S v:pred</ta>
            <ta e="T242" id="Seg_3640" s="T241">np.h:S</ta>
            <ta e="T243" id="Seg_3641" s="T242">v:pred</ta>
            <ta e="T244" id="Seg_3642" s="T243">pro.h:S</ta>
            <ta e="T245" id="Seg_3643" s="T244">v:pred</ta>
            <ta e="T249" id="Seg_3644" s="T248">pro.h:S</ta>
            <ta e="T251" id="Seg_3645" s="T250">v:pred</ta>
            <ta e="T252" id="Seg_3646" s="T251">np:S</ta>
            <ta e="T256" id="Seg_3647" s="T255">v:pred</ta>
            <ta e="T260" id="Seg_3648" s="T259">np.h:S</ta>
            <ta e="T261" id="Seg_3649" s="T260">v:pred</ta>
            <ta e="T266" id="Seg_3650" s="T265">np:S</ta>
            <ta e="T267" id="Seg_3651" s="T266">v:pred</ta>
            <ta e="T270" id="Seg_3652" s="T269">pro.h:S</ta>
            <ta e="T272" id="Seg_3653" s="T271">v:pred</ta>
            <ta e="T275" id="Seg_3654" s="T274">0.1.h:S v:pred</ta>
            <ta e="T278" id="Seg_3655" s="T277">0.2.h:S v:pred</ta>
            <ta e="T280" id="Seg_3656" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_3657" s="T280">pro.h:S</ta>
            <ta e="T282" id="Seg_3658" s="T281">pro.h:S</ta>
            <ta e="T283" id="Seg_3659" s="T282">np:O</ta>
            <ta e="T285" id="Seg_3660" s="T284">v:pred</ta>
            <ta e="T289" id="Seg_3661" s="T288">0.1.h:S v:pred</ta>
            <ta e="T292" id="Seg_3662" s="T291">np.h:S</ta>
            <ta e="T294" id="Seg_3663" s="T293">v:pred</ta>
            <ta e="T299" id="Seg_3664" s="T298">np.h:S</ta>
            <ta e="T303" id="Seg_3665" s="T302">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T307" id="Seg_3666" s="T9">RUS:cult</ta>
            <ta e="T12" id="Seg_3667" s="T11">RUS:gram</ta>
            <ta e="T17" id="Seg_3668" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_3669" s="T17">RUS:core</ta>
            <ta e="T19" id="Seg_3670" s="T18">RUS:cult</ta>
            <ta e="T27" id="Seg_3671" s="T26">RUS:core</ta>
            <ta e="T29" id="Seg_3672" s="T28">RUS:core</ta>
            <ta e="T30" id="Seg_3673" s="T29">RUS:cult</ta>
            <ta e="T31" id="Seg_3674" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_3675" s="T31">RUS:gram</ta>
            <ta e="T33" id="Seg_3676" s="T32">RUS:cult</ta>
            <ta e="T35" id="Seg_3677" s="T34">RUS:gram</ta>
            <ta e="T37" id="Seg_3678" s="T36">RUS:cult</ta>
            <ta e="T46" id="Seg_3679" s="T45">RUS:core</ta>
            <ta e="T50" id="Seg_3680" s="T49">RUS:core</ta>
            <ta e="T54" id="Seg_3681" s="T53">RUS:gram</ta>
            <ta e="T55" id="Seg_3682" s="T54">RUS:cult</ta>
            <ta e="T57" id="Seg_3683" s="T309">RUS:disc</ta>
            <ta e="T58" id="Seg_3684" s="T310">RUS:disc</ta>
            <ta e="T62" id="Seg_3685" s="T61">RUS:gram</ta>
            <ta e="T65" id="Seg_3686" s="T64">RUS:core</ta>
            <ta e="T67" id="Seg_3687" s="T66">RUS:gram</ta>
            <ta e="T71" id="Seg_3688" s="T70">RUS:gram</ta>
            <ta e="T78" id="Seg_3689" s="T77">RUS:core</ta>
            <ta e="T88" id="Seg_3690" s="T87">RUS:core</ta>
            <ta e="T118" id="Seg_3691" s="T117">RUS:core</ta>
            <ta e="T120" id="Seg_3692" s="T119">RUS:core</ta>
            <ta e="T122" id="Seg_3693" s="T121">RUS:core</ta>
            <ta e="T129" id="Seg_3694" s="T128">RUS:core</ta>
            <ta e="T133" id="Seg_3695" s="T132">RUS:core</ta>
            <ta e="T149" id="Seg_3696" s="T148">RUS:gram</ta>
            <ta e="T154" id="Seg_3697" s="T153">RUS:core</ta>
            <ta e="T159" id="Seg_3698" s="T158">RUS:core</ta>
            <ta e="T171" id="Seg_3699" s="T170">RUS:core</ta>
            <ta e="T174" id="Seg_3700" s="T173">RUS:gram</ta>
            <ta e="T180" id="Seg_3701" s="T179">RUS:cult</ta>
            <ta e="T185" id="Seg_3702" s="T184">RUS:core</ta>
            <ta e="T199" id="Seg_3703" s="T198">RUS:core</ta>
            <ta e="T206" id="Seg_3704" s="T205">RUS:gram</ta>
            <ta e="T211" id="Seg_3705" s="T210">RUS:cult</ta>
            <ta e="T218" id="Seg_3706" s="T217">RUS:gram</ta>
            <ta e="T229" id="Seg_3707" s="T228">RUS:core</ta>
            <ta e="T230" id="Seg_3708" s="T229">RUS:core</ta>
            <ta e="T232" id="Seg_3709" s="T231">RUS:core</ta>
            <ta e="T235" id="Seg_3710" s="T234">RUS:core</ta>
            <ta e="T239" id="Seg_3711" s="T238">RUS:core</ta>
            <ta e="T241" id="Seg_3712" s="T240">RUS:gram</ta>
            <ta e="T242" id="Seg_3713" s="T241">RUS:core</ta>
            <ta e="T248" id="Seg_3714" s="T247">RUS:core</ta>
            <ta e="T250" id="Seg_3715" s="T249">RUS:cult</ta>
            <ta e="T258" id="Seg_3716" s="T257">RUS:gram</ta>
            <ta e="T262" id="Seg_3717" s="T261">RUS:gram</ta>
            <ta e="T265" id="Seg_3718" s="T264">RUS:core</ta>
            <ta e="T269" id="Seg_3719" s="T268">RUS:disc</ta>
            <ta e="T274" id="Seg_3720" s="T273">RUS:core</ta>
            <ta e="T276" id="Seg_3721" s="T275">RUS:core</ta>
            <ta e="T291" id="Seg_3722" s="T290">RUS:gram</ta>
            <ta e="T292" id="Seg_3723" s="T291">RUS:core</ta>
            <ta e="T295" id="Seg_3724" s="T294">RUS:gram</ta>
            <ta e="T298" id="Seg_3725" s="T297">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T18" id="Seg_3726" s="T17">medCdel</ta>
            <ta e="T29" id="Seg_3727" s="T28">medCdel</ta>
            <ta e="T78" id="Seg_3728" s="T77">medCdel</ta>
            <ta e="T118" id="Seg_3729" s="T117">medVdel finVins</ta>
            <ta e="T120" id="Seg_3730" s="T119">medVdel finVins</ta>
            <ta e="T122" id="Seg_3731" s="T121">medVdel finVins</ta>
            <ta e="T232" id="Seg_3732" s="T231">medCdel</ta>
            <ta e="T239" id="Seg_3733" s="T238">medCdel</ta>
            <ta e="T242" id="Seg_3734" s="T241">medCdel</ta>
            <ta e="T274" id="Seg_3735" s="T273">medCdel</ta>
            <ta e="T276" id="Seg_3736" s="T275">medCdel</ta>
            <ta e="T292" id="Seg_3737" s="T291">medCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T307" id="Seg_3738" s="T9">dir:infl</ta>
            <ta e="T18" id="Seg_3739" s="T17">dir:bare</ta>
            <ta e="T19" id="Seg_3740" s="T18">dir:bare</ta>
            <ta e="T27" id="Seg_3741" s="T26">dir:bare</ta>
            <ta e="T29" id="Seg_3742" s="T28">dir:bare</ta>
            <ta e="T30" id="Seg_3743" s="T29">dir:bare</ta>
            <ta e="T31" id="Seg_3744" s="T30">dir:infl</ta>
            <ta e="T33" id="Seg_3745" s="T32">parad:bare</ta>
            <ta e="T37" id="Seg_3746" s="T36">dir:infl</ta>
            <ta e="T65" id="Seg_3747" s="T64">indir:bare</ta>
            <ta e="T78" id="Seg_3748" s="T77">dir:infl</ta>
            <ta e="T129" id="Seg_3749" s="T128">dir:bare</ta>
            <ta e="T232" id="Seg_3750" s="T231">dir:infl</ta>
            <ta e="T239" id="Seg_3751" s="T238">dir:infl</ta>
            <ta e="T250" id="Seg_3752" s="T249">dir:infl</ta>
            <ta e="T265" id="Seg_3753" s="T264">parad:bare</ta>
            <ta e="T274" id="Seg_3754" s="T273">dir:infl</ta>
            <ta e="T276" id="Seg_3755" s="T275">dir:infl</ta>
            <ta e="T298" id="Seg_3756" s="T297">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T33" id="Seg_3757" s="T31">RUS:int.alt</ta>
            <ta e="T129" id="Seg_3758" s="T128">RUS:int.ins</ta>
            <ta e="T133" id="Seg_3759" s="T132">RUS:int.ins</ta>
            <ta e="T154" id="Seg_3760" s="T153">RUS:int.ins</ta>
            <ta e="T214" id="Seg_3761" s="T213">RUS:int.ins</ta>
            <ta e="T230" id="Seg_3762" s="T228">RUS:int.alt</ta>
            <ta e="T265" id="Seg_3763" s="T264">RUS:int.ins</ta>
            <ta e="T298" id="Seg_3764" s="T297">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3765" s="T1">How we saw a bear.</ta>
            <ta e="T10" id="Seg_3766" s="T5">My daughter Ljudmilla and I went to pick berries.</ta>
            <ta e="T15" id="Seg_3767" s="T11">We set out carrying things. [?]</ta>
            <ta e="T22" id="Seg_3768" s="T16">And grandma Varvara lived there.</ta>
            <ta e="T25" id="Seg_3769" s="T23">We went on foot.</ta>
            <ta e="T33" id="Seg_3770" s="T26">Then we arrived – grandma Varvara is not there at the stop.</ta>
            <ta e="T44" id="Seg_3771" s="T34">I say to Ljudka: “Look, we’ll go here, we’ll go by this side.</ta>
            <ta e="T48" id="Seg_3772" s="T44">Here, maybe we’ll find berries.”</ta>
            <ta e="T52" id="Seg_3773" s="T49">We set out. We’re walking.</ta>
            <ta e="T60" id="Seg_3774" s="T53">And Ljudka says to me: “Somebody walked here before us.”</ta>
            <ta e="T73" id="Seg_3775" s="T61">And I tell her on purpose, so she wouldn’t know that it was a bear.</ta>
            <ta e="T79" id="Seg_3776" s="T74">I tell her: “Grandmas were here.”</ta>
            <ta e="T86" id="Seg_3777" s="T80">We walk, and it walked by us twice.</ta>
            <ta e="T94" id="Seg_3778" s="T87">Now I listen so that it wouldn’t take my daughter.</ta>
            <ta e="T96" id="Seg_3779" s="T95">We walk.</ta>
            <ta e="T103" id="Seg_3780" s="T97">My daughter runs to me: “What, a bear?” (%)</ta>
            <ta e="T108" id="Seg_3781" s="T103">I (cry?) to her: “What, a bear?” (%)</ta>
            <ta e="T116" id="Seg_3782" s="T109">She looked up – truly, there is a bear standing there, looking at us.</ta>
            <ta e="T126" id="Seg_3783" s="T117">A meter, two meters, three meters tall, looking at us.</ta>
            <ta e="T130" id="Seg_3784" s="T127">I got scared too.</ta>
            <ta e="T135" id="Seg_3785" s="T131">I’m thinking: “It better get me.”</ta>
            <ta e="T142" id="Seg_3786" s="T136">I tell my daughter: “Run behind the willow!”</ta>
            <ta e="T147" id="Seg_3787" s="T143">My daughter ran away running.</ta>
            <ta e="T153" id="Seg_3788" s="T148">And I say: “Don’t run!</ta>
            <ta e="T155" id="Seg_3789" s="T153">Go slowly!”</ta>
            <ta e="T169" id="Seg_3790" s="T156">I’m standing there, looking, telling the bear: “Why did you come here, crossing my way?”</ta>
            <ta e="T178" id="Seg_3791" s="T170">I’m standing, looking, and it is looking at me.</ta>
            <ta e="T182" id="Seg_3792" s="T179">[Ljudka] started running, she ran.</ta>
            <ta e="T196" id="Seg_3793" s="T183">And now I stood a long time, and I also took off and saw: the bear is standing and looking at me.</ta>
            <ta e="T209" id="Seg_3794" s="T197">I’m catching up, putting some birch bark here, and it is going to catch up, keeping the distance.</ta>
            <ta e="T226" id="Seg_3795" s="T210">Ljudka is also scratching something with something, here, scratching something with these, the mushrooms, she also runs to me, looking at me.</ta>
            <ta e="T233" id="Seg_3796" s="T227">Across the lake, there they are: the dog and we’re shouting to grandma.</ta>
            <ta e="T239" id="Seg_3797" s="T234">Then we crossed, shouting: “Grandma!”</ta>
            <ta e="T246" id="Seg_3798" s="T240">And grandma says: “Who’s shouting there?”</ta>
            <ta e="T256" id="Seg_3799" s="T247">I now say to Ljudka: “Why is your face so pale?”</ta>
            <ta e="T267" id="Seg_3800" s="T257">And my daughter says: “Your face also went pale.”</ta>
            <ta e="T272" id="Seg_3801" s="T268">Well, we got seriously scared.</ta>
            <ta e="T275" id="Seg_3802" s="T273">We came to grandma’s.</ta>
            <ta e="T278" id="Seg_3803" s="T275">Grandma: “What’s happened?”</ta>
            <ta e="T285" id="Seg_3804" s="T279">I’m looking, we have just seen a bear.</ta>
            <ta e="T289" id="Seg_3805" s="T286">Both of us came running.</ta>
            <ta e="T305" id="Seg_3806" s="T290">And grandma says: “And what is this, she says, my mother walked here, on the road?”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3807" s="T1">Wie wir einen Bären sahen.</ta>
            <ta e="T10" id="Seg_3808" s="T5">Meine Tochter Ljudmilla und ich gingen Beeren pflücken.</ta>
            <ta e="T15" id="Seg_3809" s="T11">Wir zogen los mit vielen Sachen. [?]</ta>
            <ta e="T22" id="Seg_3810" s="T16">Und Großmutter Varvara lebte dort.</ta>
            <ta e="T25" id="Seg_3811" s="T23">Wir gingen zu Fuß.</ta>
            <ta e="T33" id="Seg_3812" s="T26">Dann kamen wir an – Großmutter Varvara ist nicht da an der Haltestelle.</ta>
            <ta e="T44" id="Seg_3813" s="T34">Ich sagte Ljudka: „Schau, wir gehen dorthin, wir gehen zu dieser Seite.</ta>
            <ta e="T48" id="Seg_3814" s="T44">Hier, vielleicht, finden wir Beeren.“</ta>
            <ta e="T52" id="Seg_3815" s="T49">Wir zogen los. Wir liefen.</ta>
            <ta e="T60" id="Seg_3816" s="T53">Und Ljudka sagte zu mir: „Jemand ist hier vor uns gelaufen.“</ta>
            <ta e="T73" id="Seg_3817" s="T61">Und ich sag es ihr absichtlich, damit sie nicht wissen würde, dass es ein Bär war.</ta>
            <ta e="T79" id="Seg_3818" s="T74">Ich erzähle ihr: „Großmütter waren hier.“</ta>
            <ta e="T86" id="Seg_3819" s="T80">Wir laufen, und es lief zweimal an uns vorbei.</ta>
            <ta e="T94" id="Seg_3820" s="T87">Nun lausche ich, damit es nicht meine Tochter nimmt.</ta>
            <ta e="T96" id="Seg_3821" s="T95">Wir laufen.</ta>
            <ta e="T103" id="Seg_3822" s="T97">Meine Tochter rennt zu mir: „Was, ein Bär?“ (%)</ta>
            <ta e="T108" id="Seg_3823" s="T103">Ich (schreie?): „Was, ein Bär?“ (%)</ta>
            <ta e="T116" id="Seg_3824" s="T109">Sie schaut hinauf – wahrlich, es ist ein Bär, der dort steht, und schaut uns an.</ta>
            <ta e="T126" id="Seg_3825" s="T117">Ein Meter, zwei Meter, drei Meter groß, schaut uns an.</ta>
            <ta e="T130" id="Seg_3826" s="T127">Ich bekam auch Angst.</ta>
            <ta e="T135" id="Seg_3827" s="T131">Ich denke: „Er nimmt besser mich.“</ta>
            <ta e="T142" id="Seg_3828" s="T136">Ich sag meiner Tochter: „Lauf hinter die Weide!“</ta>
            <ta e="T147" id="Seg_3829" s="T143">Meine Tochter lief weg, rannte.</ta>
            <ta e="T153" id="Seg_3830" s="T148">Und ich sagte: „Renne nicht!</ta>
            <ta e="T155" id="Seg_3831" s="T153">Mach langsam!“</ta>
            <ta e="T169" id="Seg_3832" s="T156">Ich stehe da, schaue, sage dem Bären: „Warum bist du hergekommen und hast meinen Weg gekreuzt?“</ta>
            <ta e="T178" id="Seg_3833" s="T170">Ich stehe, schaue, und er schaut mich an.</ta>
            <ta e="T182" id="Seg_3834" s="T179">[Ljudka] fing an zu rennen, sie rannte.</ta>
            <ta e="T196" id="Seg_3835" s="T183">Und nun stand ich langer Zeit, und ich haute auch ab und sah: der Bär steht und schaut mich an.</ta>
            <ta e="T209" id="Seg_3836" s="T197">Ich hole auf, lass etwas Birkenrinde hier, und er wird mich einholen, hält den Abstand.</ta>
            <ta e="T226" id="Seg_3837" s="T210">Ljudka kratzt auch an etwas mit irgendwas, hier, kratzt etwas mit diesen, die Pilze, sie läuft auch zu mir, schaut mich an.</ta>
            <ta e="T233" id="Seg_3838" s="T227">Über dem See, da sind sie: der Hund, und wir rufen zur Großmutter.</ta>
            <ta e="T239" id="Seg_3839" s="T234">Dann überquerten wir, und riefen: „Großmutter!“</ta>
            <ta e="T246" id="Seg_3840" s="T240">Und Großmutter sagt: „Wer ruft da?“</ta>
            <ta e="T256" id="Seg_3841" s="T247">Nun sage ich zu Ljudka: „Warum ist dein Gesicht so bleich?“</ta>
            <ta e="T267" id="Seg_3842" s="T257">Und meine Tochter sagt: „Dein Gesicht wurde auch bleich.“</ta>
            <ta e="T272" id="Seg_3843" s="T268">Also, wir hatten gescheit Angst.</ta>
            <ta e="T275" id="Seg_3844" s="T273">Wir kamen bei Großmutter an.</ta>
            <ta e="T278" id="Seg_3845" s="T275">Großmutter: „Was ist geschehen?“</ta>
            <ta e="T285" id="Seg_3846" s="T279">Ich schaue, wir haben eben einen Bären gesehen.</ta>
            <ta e="T289" id="Seg_3847" s="T286">Wir beide kamen angerannt.</ta>
            <ta e="T305" id="Seg_3848" s="T290">Und Großmutter sagt: „Und was soll das, sagt sie, meine Mutter lief hier entlang, auf der Straße?“</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_3849" s="T1">Как мы медведя видели.</ta>
            <ta e="T10" id="Seg_3850" s="T5">Мы с дочерью по ягоды пошли, с Людмилой.</ta>
            <ta e="T15" id="Seg_3851" s="T11">А мы волоком отправились. [?]</ta>
            <ta e="T22" id="Seg_3852" s="T16">А бабушка Варвара там жила.</ta>
            <ta e="T25" id="Seg_3853" s="T23">Пешком пошли.</ta>
            <ta e="T33" id="Seg_3854" s="T26">Теперь пришли — бабушки Варвары нет на остановке.</ta>
            <ta e="T44" id="Seg_3855" s="T34">А я Людке говорю: «Смотри — сюда пойдём, с этой стороны пройдём.</ta>
            <ta e="T48" id="Seg_3856" s="T44">Здесь, может, ягоду найдём».</ta>
            <ta e="T52" id="Seg_3857" s="T49">Теперь отправились. Идём.</ta>
            <ta e="T60" id="Seg_3858" s="T53">А Людка мне говорит: «Тут кто-то ходил».</ta>
            <ta e="T73" id="Seg_3859" s="T61">А я ей нарочно говорю, чтобы она не знала, что медведь ходил.</ta>
            <ta e="T79" id="Seg_3860" s="T74">Я говорю: «А, это бабушки ходили».</ta>
            <ta e="T86" id="Seg_3861" s="T80">Идём, а он прошёл мимо два раза.</ta>
            <ta e="T94" id="Seg_3862" s="T87">Теперь я слушаю, чтоб дочь не утащил.</ta>
            <ta e="T96" id="Seg_3863" s="T95">Идём.</ta>
            <ta e="T103" id="Seg_3864" s="T97">Ко мне дочь прибежала: «Медведь, (вон там?)!» (%)</ta>
            <ta e="T108" id="Seg_3865" s="T103">Я ей (кричу?): «Как медведь?» (%)</ta>
            <ta e="T116" id="Seg_3866" s="T109">Наверх взглянула — правда, медведь стоит, на нас смотрит.</ta>
            <ta e="T126" id="Seg_3867" s="T117">Метр, два метра, три метра длиной стоит, на нас смотрит.</ta>
            <ta e="T130" id="Seg_3868" s="T127">Я тоже испугалась.</ta>
            <ta e="T135" id="Seg_3869" s="T131">Думаю: «Лучше меня схвати».</ta>
            <ta e="T142" id="Seg_3870" s="T136">Я дочери говорю: «За тальник беги!»</ta>
            <ta e="T147" id="Seg_3871" s="T143">Моя дочь побежала бегом.</ta>
            <ta e="T153" id="Seg_3872" s="T148">А я говорю: «Не беги!</ta>
            <ta e="T155" id="Seg_3873" s="T153">Помаленьку иди!»</ta>
            <ta e="T169" id="Seg_3874" s="T156">Сама стою теперь, смотрю, ему говорю: «Ты зачем сюда пришёл, на мою дорогу?»</ta>
            <ta e="T178" id="Seg_3875" s="T170">Всё стою, смотрю, а он на меня смотрит.</ta>
            <ta e="T182" id="Seg_3876" s="T179">[Людка] побежала, добежала.</ta>
            <ta e="T196" id="Seg_3877" s="T183">Я теперь сколько стояла, тоже отправилась и смотрю: стоит и смотрит на меня медведь.</ta>
            <ta e="T209" id="Seg_3878" s="T197">Я теперь догоняю, кладу сюда вниз бересту всякую, а он догонять будет — удержать.</ta>
            <ta e="T226" id="Seg_3879" s="T210">Людка тоже маленько чем-то что-то царапает, сюда всё с этими, грибами, царапает, тоже прибежит — на меня смотрит.</ta>
            <ta e="T233" id="Seg_3880" s="T227">Через болото они: собака, бабушке закричали.</ta>
            <ta e="T239" id="Seg_3881" s="T234">Потом переехали, кричим: «Бабушка!!!»</ta>
            <ta e="T246" id="Seg_3882" s="T240">А бабушка говорит: «Кто кричит там?»</ta>
            <ta e="T256" id="Seg_3883" s="T247">Теперь я Людке говорю: «Лицо почему так побледнело?»</ta>
            <ta e="T267" id="Seg_3884" s="T257">А моя дочь говорит: «А твоё тоже, говорит, лицо бледное».</ta>
            <ta e="T272" id="Seg_3885" s="T268">Но, мы сильно испугались.</ta>
            <ta e="T275" id="Seg_3886" s="T273">К бабушке пришли.</ta>
            <ta e="T278" id="Seg_3887" s="T275">Бабушка: «Что случилось?»</ta>
            <ta e="T285" id="Seg_3888" s="T279">Смотрю я, мы медведя только что видели.</ta>
            <ta e="T289" id="Seg_3889" s="T286">Вдвоём сюда прибежали.</ta>
            <ta e="T305" id="Seg_3890" s="T290">А бабушка говорит: «А это что, говорит, мать моя вброд сюда привела, на дорогу?»</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_3891" s="T1">Как мы медведя видели.</ta>
            <ta e="T10" id="Seg_3892" s="T5">Мы с дочерью по ягоды пошли, с Людмилой.</ta>
            <ta e="T15" id="Seg_3893" s="T11">А мы волоком отправились.</ta>
            <ta e="T22" id="Seg_3894" s="T16">А бабушка Варвара там жила.</ta>
            <ta e="T25" id="Seg_3895" s="T23">Пешком пошли.</ta>
            <ta e="T33" id="Seg_3896" s="T26">Теперь пришли — бабушки Варвары нет на остановке.</ta>
            <ta e="T44" id="Seg_3897" s="T34">А я Людке говорю: «Смотри — сюда пойдём, в эту сторону пойдём.</ta>
            <ta e="T48" id="Seg_3898" s="T44">Здесь, может, ягоду найдём».</ta>
            <ta e="T52" id="Seg_3899" s="T49">Теперь отправились. Идём.</ta>
            <ta e="T60" id="Seg_3900" s="T53">А Людка мне говорит: «Тут кто-то ходил».</ta>
            <ta e="T73" id="Seg_3901" s="T61">А я ей нарочно говорю, чтобы она не знала, какой зверь ходил.</ta>
            <ta e="T79" id="Seg_3902" s="T74">Я говорю: «Это бабушки ходили».</ta>
            <ta e="T86" id="Seg_3903" s="T80">Идём, а он прошёл мимо два раза.</ta>
            <ta e="T94" id="Seg_3904" s="T87">Теперь я слушаю, чтоб дочь не утащил.</ta>
            <ta e="T96" id="Seg_3905" s="T95">Идём.</ta>
            <ta e="T103" id="Seg_3906" s="T97">Ко мне дочь прибежала: «Медведь (?)!» (%)</ta>
            <ta e="T108" id="Seg_3907" s="T103">Я ей (кричу?): «Как медведь?» (%)</ta>
            <ta e="T116" id="Seg_3908" s="T109">Наверх взглянула — правда, медведь стоит, на нас смотрит.</ta>
            <ta e="T126" id="Seg_3909" s="T117">Метр, два метра, три метра длиной стоит, на нас смотрит.</ta>
            <ta e="T130" id="Seg_3910" s="T127">Я тоже испугалась.</ta>
            <ta e="T135" id="Seg_3911" s="T131">Думаю: «Лучше меня схвати».</ta>
            <ta e="T142" id="Seg_3912" s="T136">Я дочери говорю: «За тальник беги!»</ta>
            <ta e="T147" id="Seg_3913" s="T143">Моя дочь побежала бегом.</ta>
            <ta e="T153" id="Seg_3914" s="T148">А я говорю: «Не беги!</ta>
            <ta e="T155" id="Seg_3915" s="T153">Помаленьку иди!»</ta>
            <ta e="T169" id="Seg_3916" s="T156">Сама стою теперь, смотрю, ему говорю: «Ты зачем сюда пришёл, на мою дорогу?»</ta>
            <ta e="T178" id="Seg_3917" s="T170">Всё стою, смотрю, а он на меня смотрит.</ta>
            <ta e="T182" id="Seg_3918" s="T179">[Людка] побежала, добежала.</ta>
            <ta e="T196" id="Seg_3919" s="T183">Я теперь сколько стояла, тоже отправилась и смотрю: стоит и смотрит на меня медведь.</ta>
            <ta e="T209" id="Seg_3920" s="T197">Я теперь догоняю, кладу сюда вниз бересту всякую, а он догонять будет — удержать.</ta>
            <ta e="T226" id="Seg_3921" s="T210">Людка тоже маленько чем-то что-то царапает, сюда всё с этими, грибами, царапает, тоже прибежит — на меня смотрит.</ta>
            <ta e="T233" id="Seg_3922" s="T227">Через болото они: собака, бабушке закричали.</ta>
            <ta e="T239" id="Seg_3923" s="T234">Потом переехали, кричим: «Бабушка!!!»</ta>
            <ta e="T246" id="Seg_3924" s="T240">А бабушка говорит: «Кто кричит там?»</ta>
            <ta e="T256" id="Seg_3925" s="T247">Теперь я Людке говорю: «Лицо почему так побледнело?»</ta>
            <ta e="T267" id="Seg_3926" s="T257">А моя дочь говорит: «А твоё тоже, говорит, лицо бледное».</ta>
            <ta e="T272" id="Seg_3927" s="T268">Но, мы сильно испугались.</ta>
            <ta e="T275" id="Seg_3928" s="T273">К бабушке пришли.</ta>
            <ta e="T278" id="Seg_3929" s="T275">Бабушка: «Что случилось?»</ta>
            <ta e="T285" id="Seg_3930" s="T279">Смотрю я, мы медведя только что видели.</ta>
            <ta e="T289" id="Seg_3931" s="T286">Вдвоём сюда прибежали.</ta>
            <ta e="T305" id="Seg_3932" s="T290">А бабушка говорит: «А это что, говорит, мать моя вброд сюда привела, на дорогу?»</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T15" id="Seg_3933" s="T11">[AAV:] qwen: zero in 1DU?</ta>
            <ta e="T44" id="Seg_3934" s="T34">[AAV:] Manǯoɣa ?; belekaɨt ?</ta>
            <ta e="T73" id="Seg_3935" s="T61">[AAV:] kwajakumbɨ &gt; kwajamənd? [WNB:] tänulemd is corrected as tänulend</ta>
            <ta e="T79" id="Seg_3936" s="T74">[AAV:] bauškamd &gt; bauškanmɨxɨ ?; qwajakumundaɣ ; 3DU or 3SG ?</ta>
            <ta e="T86" id="Seg_3937" s="T80">[AAV:] a tab menteǯä &gt; kalammut elʼǯʼä ? Cf. ильҗя /об. Ч/ "дедушка; Бог; медведь" (Б 29); cf. KFN_1967_Language_flk.014</ta>
            <ta e="T94" id="Seg_3938" s="T87">[AAV:] nep aː ügla &gt; ne (x)ügla ?</ta>
            <ta e="T103" id="Seg_3939" s="T97">[AAV:] (ogugu it pabə) ?</ta>
            <ta e="T116" id="Seg_3940" s="T109">[AAV:] sabɨlʼ &gt; anun ?</ta>
            <ta e="T135" id="Seg_3941" s="T131">[AAV:] maʒek &gt; maʒem; orallend &gt; ora(lle)nnɨ ?</ta>
            <ta e="T142" id="Seg_3942" s="T136">[AAV:] missing words before "Nʼarg"</ta>
            <ta e="T182" id="Seg_3943" s="T179">[AAV:] kuranna &gt; kuran; merda &gt; nʼartaɣ ?</ta>
            <ta e="T196" id="Seg_3944" s="T183">[AAV:] ай ма́шэк &gt; maʒem</ta>
            <ta e="T209" id="Seg_3945" s="T197">[AAV:] a tab nödla &gt; tarbak nödəla ? uǯegɨgu &gt; uǯettəgu ?</ta>
            <ta e="T226" id="Seg_3946" s="T210">[AAV:] naj kuralǯekwa &gt; naj kuralešpa ? ма́шэк &gt; maʒem</ta>
            <ta e="T233" id="Seg_3947" s="T227">[AAV:] Nʼar &gt; me nʼar; oni &gt; oli parkwaj ? bauškan &gt; "Bauška!" ?</ta>
            <ta e="T246" id="Seg_3948" s="T240">[AAV:] (in the end) taɣɨt ?</ta>
            <ta e="T256" id="Seg_3949" s="T247">[AAV:] čanemba &gt; čomnembad ?</ta>
            <ta e="T267" id="Seg_3950" s="T257">[AAV:] A tan naj &gt; A tanan naj ?</ta>
            <ta e="T285" id="Seg_3951" s="T279">[AAV:] Manǯak matənaq, qorɣop ?</ta>
            <ta e="T305" id="Seg_3952" s="T290">[AAV:] xajɣet &gt; xalʼken ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T306" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T307" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T308" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T309" />
            <conversion-tli id="T57" />
            <conversion-tli id="T310" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T311" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
