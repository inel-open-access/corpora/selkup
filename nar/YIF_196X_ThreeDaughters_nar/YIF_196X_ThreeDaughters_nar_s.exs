<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_1965_ThreeDaughters_nar</transcription-name>
         <referenced-file url="YIF_196X_ThreeDaughters_nar.wav" />
         <referenced-file url="YIF_196X_ThreeDaughters_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_ThreeDaughters_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">133</ud-information>
            <ud-information attribute-name="# HIAT:w">103</ud-information>
            <ud-information attribute-name="# e">103</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">18</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="1.065" type="appl" />
         <tli id="T3" time="2.13" type="appl" />
         <tli id="T4" time="2.15" type="appl" />
         <tli id="T5" time="2.69625" type="appl" />
         <tli id="T6" time="3.2424999999999997" type="appl" />
         <tli id="T7" time="3.7887499999999994" type="appl" />
         <tli id="T8" time="4.334999999999999" type="appl" />
         <tli id="T9" time="4.88125" type="appl" />
         <tli id="T10" time="5.427499999999999" type="appl" />
         <tli id="T11" time="5.973749999999999" type="appl" />
         <tli id="T12" time="6.52" type="appl" />
         <tli id="T13" time="7.235" type="appl" />
         <tli id="T14" time="7.8221428571428575" type="appl" />
         <tli id="T15" time="8.409285714285714" type="appl" />
         <tli id="T16" time="8.996428571428572" type="appl" />
         <tli id="T17" time="9.583571428571428" type="appl" />
         <tli id="T18" time="10.170714285714286" type="appl" />
         <tli id="T19" time="10.757857142857143" type="appl" />
         <tli id="T20" time="11.344999999999999" type="appl" />
         <tli id="T21" time="11.932142857142857" type="appl" />
         <tli id="T22" time="12.519285714285715" type="appl" />
         <tli id="T23" time="13.106428571428571" type="appl" />
         <tli id="T24" time="13.693571428571428" type="appl" />
         <tli id="T25" time="14.280714285714286" type="appl" />
         <tli id="T26" time="14.867857142857142" type="appl" />
         <tli id="T27" time="15.454999999999998" type="appl" />
         <tli id="T28" time="15.455" type="appl" />
         <tli id="T29" time="15.7" type="appl" />
         <tli id="T30" time="16.41076923076923" type="appl" />
         <tli id="T31" time="17.12153846153846" type="appl" />
         <tli id="T32" time="17.832307692307694" type="appl" />
         <tli id="T33" time="18.543076923076924" type="appl" />
         <tli id="T34" time="19.253846153846155" type="appl" />
         <tli id="T35" time="19.964615384615385" type="appl" />
         <tli id="T36" time="20.675384615384615" type="appl" />
         <tli id="T37" time="21.386153846153846" type="appl" />
         <tli id="T38" time="22.096923076923076" type="appl" />
         <tli id="T39" time="22.807692307692307" type="appl" />
         <tli id="T40" time="23.51846153846154" type="appl" />
         <tli id="T41" time="24.22923076923077" type="appl" />
         <tli id="T42" time="24.94" type="appl" />
         <tli id="T43" time="25.245" type="appl" />
         <tli id="T44" time="25.872812500000002" type="appl" />
         <tli id="T45" time="26.500625" type="appl" />
         <tli id="T46" time="27.1284375" type="appl" />
         <tli id="T47" time="27.75625" type="appl" />
         <tli id="T48" time="28.3840625" type="appl" />
         <tli id="T49" time="29.011875" type="appl" />
         <tli id="T50" time="29.6396875" type="appl" />
         <tli id="T51" time="30.2675" type="appl" />
         <tli id="T52" time="30.8953125" type="appl" />
         <tli id="T53" time="31.523125" type="appl" />
         <tli id="T54" time="32.1509375" type="appl" />
         <tli id="T55" time="32.77875" type="appl" />
         <tli id="T56" time="33.4065625" type="appl" />
         <tli id="T57" time="34.034375" type="appl" />
         <tli id="T58" time="34.6621875" type="appl" />
         <tli id="T59" time="35.29" type="appl" />
         <tli id="T60" time="35.62" type="appl" />
         <tli id="T61" time="36.31333333333333" type="appl" />
         <tli id="T62" time="37.00666666666666" type="appl" />
         <tli id="T63" time="37.699999999999996" type="appl" />
         <tli id="T64" time="38.39333333333333" type="appl" />
         <tli id="T65" time="39.086666666666666" type="appl" />
         <tli id="T66" time="39.78" type="appl" />
         <tli id="T67" time="40.47333333333333" type="appl" />
         <tli id="T68" time="41.166666666666664" type="appl" />
         <tli id="T69" time="41.86" type="appl" />
         <tli id="T70" time="42.55333333333333" type="appl" />
         <tli id="T71" time="43.24666666666666" type="appl" />
         <tli id="T72" time="43.94" type="appl" />
         <tli id="T73" time="44.365" type="appl" />
         <tli id="T74" time="44.99142857142857" type="appl" />
         <tli id="T75" time="45.61785714285715" type="appl" />
         <tli id="T76" time="46.244285714285716" type="appl" />
         <tli id="T77" time="46.870714285714286" type="appl" />
         <tli id="T78" time="47.497142857142855" type="appl" />
         <tli id="T79" time="48.12357142857143" type="appl" />
         <tli id="T80" time="48.75" type="appl" />
         <tli id="T81" time="49.37642857142857" type="appl" />
         <tli id="T82" time="50.00285714285714" type="appl" />
         <tli id="T83" time="50.629285714285714" type="appl" />
         <tli id="T84" time="51.255714285714284" type="appl" />
         <tli id="T85" time="51.88214285714285" type="appl" />
         <tli id="T86" time="52.50857142857143" type="appl" />
         <tli id="T87" time="53.135" type="appl" />
         <tli id="T88" time="53.62" type="appl" />
         <tli id="T89" time="54.193" type="appl" />
         <tli id="T90" time="54.766" type="appl" />
         <tli id="T91" time="55.339" type="appl" />
         <tli id="T92" time="55.912" type="appl" />
         <tli id="T93" time="56.485" type="appl" />
         <tli id="T94" time="57.058" type="appl" />
         <tli id="T95" time="57.631" type="appl" />
         <tli id="T96" time="58.204" type="appl" />
         <tli id="T97" time="58.777" type="appl" />
         <tli id="T98" time="59.35" type="appl" />
         <tli id="T99" time="59.635" type="appl" />
         <tli id="T100" time="60.31535714285714" type="appl" />
         <tli id="T101" time="60.995714285714286" type="appl" />
         <tli id="T102" time="61.676071428571426" type="appl" />
         <tli id="T103" time="62.356428571428566" type="appl" />
         <tli id="T104" time="63.03678571428571" type="appl" />
         <tli id="T105" time="63.71714285714285" type="appl" />
         <tli id="T106" time="64.3975" type="appl" />
         <tli id="T107" time="65.07785714285714" type="appl" />
         <tli id="T108" time="65.75821428571429" type="appl" />
         <tli id="T109" time="66.43857142857142" type="appl" />
         <tli id="T110" time="67.11892857142857" type="appl" />
         <tli id="T111" time="67.79928571428572" type="appl" />
         <tli id="T112" time="68.47964285714285" type="appl" />
         <tli id="T113" time="69.16" type="appl" />
         <tli id="T0" time="71.394" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T3" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nagur</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ned</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T12" id="Seg_10" n="sc" s="T4">
               <ts e="T12" id="Seg_12" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">Mannan</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">nagur</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">nem</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">no</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">tärbak</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">štobɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">wes</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">ogolalǯijadɨt</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T28" id="Seg_40" n="sc" s="T13">
               <ts e="T28" id="Seg_42" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">Oqqer</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">warg</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">nem</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">ogolemba</ts>
                  <nts id="Seg_54" n="HIAT:ip">,</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_57" n="HIAT:w" s="T17">i</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_60" n="HIAT:w" s="T18">tab</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_63" n="HIAT:w" s="T19">uǯä</ts>
                  <nts id="Seg_64" n="HIAT:ip">,</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">i</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">wak</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">uǯä</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_77" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">kočʼek</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_83" n="HIAT:w" s="T25">sarabatɨwat</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_85" n="HIAT:ip">—</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_88" n="HIAT:w" s="T26">ogolalǯemba</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T42" id="Seg_91" n="sc" s="T29">
               <ts e="T42" id="Seg_93" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_95" n="HIAT:w" s="T29">Ešo</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_98" n="HIAT:w" s="T30">šed</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_101" n="HIAT:w" s="T31">nem</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_104" n="HIAT:w" s="T32">uǯäɣ</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_108" n="HIAT:w" s="T33">no</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_111" n="HIAT:w" s="T34">po</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_114" n="HIAT:w" s="T35">desʼatʼ</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_117" n="HIAT:w" s="T36">klassow</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_120" n="HIAT:w" s="T37">končimbadi</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_124" n="HIAT:w" s="T38">a</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_127" n="HIAT:w" s="T39">nikakoj</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_130" n="HIAT:w" s="T40">specialʼnostʼ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_133" n="HIAT:w" s="T41">netu</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_136" n="sc" s="T43">
               <ts e="T59" id="Seg_138" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_140" n="HIAT:w" s="T43">Mat</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_143" n="HIAT:w" s="T44">kɨgak</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_147" n="HIAT:w" s="T45">čtobɨ</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_150" n="HIAT:w" s="T46">üčega</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_153" n="HIAT:w" s="T47">nem</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_156" n="HIAT:w" s="T48">ešё</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_159" n="HIAT:w" s="T49">ogolaǯešpegu</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_162" n="HIAT:w" s="T50">kwennɨja</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_165" n="HIAT:w" s="T51">i</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_168" n="HIAT:w" s="T52">ogolǯɨja</ts>
                  <nts id="Seg_169" n="HIAT:ip">,</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_172" n="HIAT:w" s="T53">čtobɨ</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_175" n="HIAT:w" s="T54">mat</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_178" n="HIAT:w" s="T55">tare</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_182" n="HIAT:w" s="T56">kanǯem</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_185" n="HIAT:w" s="T57">ɨg</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_188" n="HIAT:w" s="T58">ügolbikujamt</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_191" n="sc" s="T60">
               <ts e="T72" id="Seg_193" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_195" n="HIAT:w" s="T60">A</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_198" n="HIAT:w" s="T61">to</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_201" n="HIAT:w" s="T62">mat</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_204" n="HIAT:w" s="T63">wsü</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_207" n="HIAT:w" s="T64">ʒiznʼ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_210" n="HIAT:w" s="T65">kanǯep</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_213" n="HIAT:w" s="T66">ügolbɨhak</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_217" n="HIAT:w" s="T67">tabem</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_220" n="HIAT:w" s="T68">nörguhak</ts>
                  <nts id="Seg_221" n="HIAT:ip">,</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_224" n="HIAT:w" s="T69">maǯöɣend</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_227" n="HIAT:w" s="T70">wezde</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_230" n="HIAT:w" s="T71">kwajakuhak</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T87" id="Seg_233" n="sc" s="T73">
               <ts e="T87" id="Seg_235" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_237" n="HIAT:w" s="T73">Kak</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_240" n="HIAT:w" s="T74">koška</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_243" n="HIAT:w" s="T75">maǯöɣet</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_246" n="HIAT:w" s="T76">kwajagu</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_250" n="HIAT:w" s="T77">na</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_253" n="HIAT:w" s="T78">xɨrem</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_256" n="HIAT:w" s="T79">madergu</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_259" n="HIAT:w" s="T80">wezde</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_263" n="HIAT:w" s="T81">tabep</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_266" n="HIAT:w" s="T82">pegu</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_270" n="HIAT:w" s="T83">wezde</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_273" n="HIAT:w" s="T84">pot</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_276" n="HIAT:w" s="T85">par</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_279" n="HIAT:w" s="T86">manembegu</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T98" id="Seg_282" n="sc" s="T88">
               <ts e="T98" id="Seg_284" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_286" n="HIAT:w" s="T88">I</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_289" n="HIAT:w" s="T89">mat</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_292" n="HIAT:w" s="T90">tärbak</ts>
                  <nts id="Seg_293" n="HIAT:ip">:</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_296" n="HIAT:w" s="T91">man</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_299" n="HIAT:w" s="T92">newem</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_302" n="HIAT:w" s="T93">puskaj</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_305" n="HIAT:w" s="T94">ogolalǯešpijamd</ts>
                  <nts id="Seg_306" n="HIAT:ip">,</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_309" n="HIAT:w" s="T95">i</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_312" n="HIAT:w" s="T96">uǯijamdet</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_315" n="HIAT:w" s="T97">wak</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_318" n="sc" s="T99">
               <ts e="T113" id="Seg_320" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_322" n="HIAT:w" s="T99">I</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_325" n="HIAT:w" s="T100">tabedet</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_328" n="HIAT:w" s="T101">qumat</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_331" n="HIAT:w" s="T102">kɨgak</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_335" n="HIAT:w" s="T103">štobɨ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_338" n="HIAT:w" s="T104">tabet</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_341" n="HIAT:w" s="T105">ogolalǯɨjamdet</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_344" n="HIAT:w" s="T106">i</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_347" n="HIAT:w" s="T107">uǯikujamdet</ts>
                  <nts id="Seg_348" n="HIAT:ip">,</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_351" n="HIAT:w" s="T108">štobɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_354" n="HIAT:w" s="T109">na</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_357" n="HIAT:w" s="T110">nɨlʼǯiɣen</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_360" n="HIAT:w" s="T111">ilkijamdet</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_363" n="HIAT:w" s="T112">wak</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T3" id="Seg_366" n="sc" s="T1">
               <ts e="T2" id="Seg_368" n="e" s="T1">Nagur </ts>
               <ts e="T3" id="Seg_370" n="e" s="T2">ned. </ts>
            </ts>
            <ts e="T12" id="Seg_371" n="sc" s="T4">
               <ts e="T5" id="Seg_373" n="e" s="T4">Mannan </ts>
               <ts e="T6" id="Seg_375" n="e" s="T5">nagur </ts>
               <ts e="T7" id="Seg_377" n="e" s="T6">nem, </ts>
               <ts e="T8" id="Seg_379" n="e" s="T7">no </ts>
               <ts e="T9" id="Seg_381" n="e" s="T8">tärbak, </ts>
               <ts e="T10" id="Seg_383" n="e" s="T9">štobɨ </ts>
               <ts e="T11" id="Seg_385" n="e" s="T10">wes </ts>
               <ts e="T12" id="Seg_387" n="e" s="T11">ogolalǯijadɨt. </ts>
            </ts>
            <ts e="T28" id="Seg_388" n="sc" s="T13">
               <ts e="T14" id="Seg_390" n="e" s="T13">Oqqer </ts>
               <ts e="T15" id="Seg_392" n="e" s="T14">warg </ts>
               <ts e="T16" id="Seg_394" n="e" s="T15">nem </ts>
               <ts e="T17" id="Seg_396" n="e" s="T16">ogolemba, </ts>
               <ts e="T18" id="Seg_398" n="e" s="T17">i </ts>
               <ts e="T19" id="Seg_400" n="e" s="T18">tab </ts>
               <ts e="T20" id="Seg_402" n="e" s="T19">uǯä, </ts>
               <ts e="T21" id="Seg_404" n="e" s="T20">i </ts>
               <ts e="T22" id="Seg_406" n="e" s="T21">wak </ts>
               <ts e="T23" id="Seg_408" n="e" s="T22">uǯä, </ts>
               <ts e="T24" id="Seg_410" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_412" n="e" s="T24">kočʼek </ts>
               <ts e="T26" id="Seg_414" n="e" s="T25">sarabatɨwat — </ts>
               <ts e="T28" id="Seg_416" n="e" s="T26">ogolalǯemba. </ts>
            </ts>
            <ts e="T42" id="Seg_417" n="sc" s="T29">
               <ts e="T30" id="Seg_419" n="e" s="T29">Ešo </ts>
               <ts e="T31" id="Seg_421" n="e" s="T30">šed </ts>
               <ts e="T32" id="Seg_423" n="e" s="T31">nem </ts>
               <ts e="T33" id="Seg_425" n="e" s="T32">uǯäɣ, </ts>
               <ts e="T34" id="Seg_427" n="e" s="T33">no </ts>
               <ts e="T35" id="Seg_429" n="e" s="T34">po </ts>
               <ts e="T36" id="Seg_431" n="e" s="T35">desʼatʼ </ts>
               <ts e="T37" id="Seg_433" n="e" s="T36">klassow </ts>
               <ts e="T38" id="Seg_435" n="e" s="T37">končimbadi, </ts>
               <ts e="T39" id="Seg_437" n="e" s="T38">a </ts>
               <ts e="T40" id="Seg_439" n="e" s="T39">nikakoj </ts>
               <ts e="T41" id="Seg_441" n="e" s="T40">specialʼnostʼ </ts>
               <ts e="T42" id="Seg_443" n="e" s="T41">netu. </ts>
            </ts>
            <ts e="T59" id="Seg_444" n="sc" s="T43">
               <ts e="T44" id="Seg_446" n="e" s="T43">Mat </ts>
               <ts e="T45" id="Seg_448" n="e" s="T44">kɨgak, </ts>
               <ts e="T46" id="Seg_450" n="e" s="T45">čtobɨ </ts>
               <ts e="T47" id="Seg_452" n="e" s="T46">üčega </ts>
               <ts e="T48" id="Seg_454" n="e" s="T47">nem </ts>
               <ts e="T49" id="Seg_456" n="e" s="T48">ešё </ts>
               <ts e="T50" id="Seg_458" n="e" s="T49">ogolaǯešpegu </ts>
               <ts e="T51" id="Seg_460" n="e" s="T50">kwennɨja </ts>
               <ts e="T52" id="Seg_462" n="e" s="T51">i </ts>
               <ts e="T53" id="Seg_464" n="e" s="T52">ogolǯɨja, </ts>
               <ts e="T54" id="Seg_466" n="e" s="T53">čtobɨ </ts>
               <ts e="T55" id="Seg_468" n="e" s="T54">mat </ts>
               <ts e="T56" id="Seg_470" n="e" s="T55">tare, </ts>
               <ts e="T57" id="Seg_472" n="e" s="T56">kanǯem </ts>
               <ts e="T58" id="Seg_474" n="e" s="T57">ɨg </ts>
               <ts e="T59" id="Seg_476" n="e" s="T58">ügolbikujamt. </ts>
            </ts>
            <ts e="T72" id="Seg_477" n="sc" s="T60">
               <ts e="T61" id="Seg_479" n="e" s="T60">A </ts>
               <ts e="T62" id="Seg_481" n="e" s="T61">to </ts>
               <ts e="T63" id="Seg_483" n="e" s="T62">mat </ts>
               <ts e="T64" id="Seg_485" n="e" s="T63">wsü </ts>
               <ts e="T65" id="Seg_487" n="e" s="T64">ʒiznʼ </ts>
               <ts e="T66" id="Seg_489" n="e" s="T65">kanǯep </ts>
               <ts e="T67" id="Seg_491" n="e" s="T66">ügolbɨhak, </ts>
               <ts e="T68" id="Seg_493" n="e" s="T67">tabem </ts>
               <ts e="T69" id="Seg_495" n="e" s="T68">nörguhak, </ts>
               <ts e="T70" id="Seg_497" n="e" s="T69">maǯöɣend </ts>
               <ts e="T71" id="Seg_499" n="e" s="T70">wezde </ts>
               <ts e="T72" id="Seg_501" n="e" s="T71">kwajakuhak. </ts>
            </ts>
            <ts e="T87" id="Seg_502" n="sc" s="T73">
               <ts e="T74" id="Seg_504" n="e" s="T73">Kak </ts>
               <ts e="T75" id="Seg_506" n="e" s="T74">koška </ts>
               <ts e="T76" id="Seg_508" n="e" s="T75">maǯöɣet </ts>
               <ts e="T77" id="Seg_510" n="e" s="T76">kwajagu, </ts>
               <ts e="T78" id="Seg_512" n="e" s="T77">na </ts>
               <ts e="T79" id="Seg_514" n="e" s="T78">xɨrem </ts>
               <ts e="T80" id="Seg_516" n="e" s="T79">madergu </ts>
               <ts e="T81" id="Seg_518" n="e" s="T80">wezde, </ts>
               <ts e="T82" id="Seg_520" n="e" s="T81">tabep </ts>
               <ts e="T83" id="Seg_522" n="e" s="T82">pegu, </ts>
               <ts e="T84" id="Seg_524" n="e" s="T83">wezde </ts>
               <ts e="T85" id="Seg_526" n="e" s="T84">pot </ts>
               <ts e="T86" id="Seg_528" n="e" s="T85">par </ts>
               <ts e="T87" id="Seg_530" n="e" s="T86">manembegu. </ts>
            </ts>
            <ts e="T98" id="Seg_531" n="sc" s="T88">
               <ts e="T89" id="Seg_533" n="e" s="T88">I </ts>
               <ts e="T90" id="Seg_535" n="e" s="T89">mat </ts>
               <ts e="T91" id="Seg_537" n="e" s="T90">tärbak: </ts>
               <ts e="T92" id="Seg_539" n="e" s="T91">man </ts>
               <ts e="T93" id="Seg_541" n="e" s="T92">newem </ts>
               <ts e="T94" id="Seg_543" n="e" s="T93">puskaj </ts>
               <ts e="T95" id="Seg_545" n="e" s="T94">ogolalǯešpijamd, </ts>
               <ts e="T96" id="Seg_547" n="e" s="T95">i </ts>
               <ts e="T97" id="Seg_549" n="e" s="T96">uǯijamdet </ts>
               <ts e="T98" id="Seg_551" n="e" s="T97">wak. </ts>
            </ts>
            <ts e="T113" id="Seg_552" n="sc" s="T99">
               <ts e="T100" id="Seg_554" n="e" s="T99">I </ts>
               <ts e="T101" id="Seg_556" n="e" s="T100">tabedet </ts>
               <ts e="T102" id="Seg_558" n="e" s="T101">qumat </ts>
               <ts e="T103" id="Seg_560" n="e" s="T102">kɨgak, </ts>
               <ts e="T104" id="Seg_562" n="e" s="T103">štobɨ </ts>
               <ts e="T105" id="Seg_564" n="e" s="T104">tabet </ts>
               <ts e="T106" id="Seg_566" n="e" s="T105">ogolalǯɨjamdet </ts>
               <ts e="T107" id="Seg_568" n="e" s="T106">i </ts>
               <ts e="T108" id="Seg_570" n="e" s="T107">uǯikujamdet, </ts>
               <ts e="T109" id="Seg_572" n="e" s="T108">štobɨ </ts>
               <ts e="T110" id="Seg_574" n="e" s="T109">na </ts>
               <ts e="T111" id="Seg_576" n="e" s="T110">nɨlʼǯiɣen </ts>
               <ts e="T112" id="Seg_578" n="e" s="T111">ilkijamdet </ts>
               <ts e="T113" id="Seg_580" n="e" s="T112">wak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_581" s="T1">YIF_196X_ThreeDaughters_nar.001 (001)</ta>
            <ta e="T12" id="Seg_582" s="T4">YIF_196X_ThreeDaughters_nar.002 (002)</ta>
            <ta e="T28" id="Seg_583" s="T13">YIF_196X_ThreeDaughters_nar.003 (003)</ta>
            <ta e="T42" id="Seg_584" s="T29">YIF_196X_ThreeDaughters_nar.004 (004)</ta>
            <ta e="T59" id="Seg_585" s="T43">YIF_196X_ThreeDaughters_nar.005 (005)</ta>
            <ta e="T72" id="Seg_586" s="T60">YIF_196X_ThreeDaughters_nar.006 (006)</ta>
            <ta e="T87" id="Seg_587" s="T73">YIF_196X_ThreeDaughters_nar.007 (007)</ta>
            <ta e="T98" id="Seg_588" s="T88">YIF_196X_ThreeDaughters_nar.008 (008)</ta>
            <ta e="T113" id="Seg_589" s="T99">YIF_196X_ThreeDaughters_nar.009 (009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_590" s="T1">На́гур нэд.</ta>
            <ta e="T12" id="Seg_591" s="T4">Манна́н на́гур нэм, но тӓрба́к, чтобы вес о́голалджэшпиямд.</ta>
            <ta e="T28" id="Seg_592" s="T13">О́ӄӄэр варг нэм о́голэмба, и таб у́джӓ, и фак у́джӓ, и ко́чек сараба́тывает — о́голалджэмба.</ta>
            <ta e="T42" id="Seg_593" s="T29">Ещё шэд нэм у́джӓӷ, но по де́сять кла́ссов ко́нчимбадi, а никако́й специа́льность не́ту.</ta>
            <ta e="T59" id="Seg_594" s="T43">Мат кыга́к, чтобы ӱҷэга́ нэм ещё о́голаджэшпэгу квэ́нныя и о́голджыя, что́бы мат таре́, ка́нджэм ыг ӱ́голбикуямд.</ta>
            <ta e="T72" id="Seg_595" s="T60">А то мат всю жизнь ка́нджэп ӱголбыхак, табе́м нӧрхугак, маджӧӷэнд везде́ квая́куҳак.</ta>
            <ta e="T87" id="Seg_596" s="T73">Как кошка́ маджӧӷэт квая́гу, на хы́рэм ма́дэргу везде́, табе́п пе́гу, везде́ пот пар манэмбэгу́.</ta>
            <ta e="T98" id="Seg_597" s="T88">И мат тӓрба́к: ман нэ́вем пуска́й о́голалджэшпиямд, у́джиямдэт фак.</ta>
            <ta e="T113" id="Seg_598" s="T99">И та́бэдэт ӄу́мат кыга́к, чтоб та́бэт о́голалджыямдэт и у́джикуямдэт — на ны́льджиӷэн илки́янд фак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T3" id="Seg_599" s="T1">Nagur ned.</ta>
            <ta e="T12" id="Seg_600" s="T4">Mannan nagur nem, no tärbak, čʼtobɨ wes ogolalǯešpijamd.</ta>
            <ta e="T28" id="Seg_601" s="T13">Oqqer warg nem ogolemba, i tab uǯä, i fak uǯä, i kočʼek sarabatɨwaet — ogolalǯemba.</ta>
            <ta e="T42" id="Seg_602" s="T29">Ešё šed nem uǯäɣ, no po desʼatʼ klassow končʼimbadi, a nikakoj specialʼnostʼ netu.</ta>
            <ta e="T59" id="Seg_603" s="T43">Mat kɨgak, čʼtobɨ üčega nem ešё ogolaǯešpegu kwennɨja i ogolǯɨja, čʼtobɨ mat tare, kanǯem ɨg ügolbikujamd.</ta>
            <ta e="T72" id="Seg_604" s="T60">A to mat wsü ʒiznʼ kanǯep ügolbɨxak, tabem nörxugak, maǯöɣend wezde kwajakuxak.</ta>
            <ta e="T87" id="Seg_605" s="T73">Kak koška maǯöɣet kwajagu, na xɨrem madergu wezde, tabep pegu, wezde pot par manembegu.</ta>
            <ta e="T98" id="Seg_606" s="T88">I mat tärbak: man newem puskaj ogolalǯešpijamd, uǯijamdet fak.</ta>
            <ta e="T113" id="Seg_607" s="T99">I tabedet qumat kɨgak, čʼtob tabet ogolalǯɨjamdet i uǯikujamdet — na nɨlʼǯiɣen ilkijand fak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_608" s="T1">Nagur ned. </ta>
            <ta e="T12" id="Seg_609" s="T4">Mannan nagur nem, no tärbak, štobɨ wes ogolalǯijadɨt. </ta>
            <ta e="T28" id="Seg_610" s="T13">Oqqer warg nem ogolemba, i tab uǯä, i wak uǯä, i kočʼek sarabatɨwat — ogolalǯemba. </ta>
            <ta e="T42" id="Seg_611" s="T29">Ešo šed nem uǯäɣ, no po desʼatʼ klassow končimbadi, a nikakoj specialʼnostʼ netu. </ta>
            <ta e="T59" id="Seg_612" s="T43">Mat kɨgak, čtobɨ üčega nem ešё ogolaǯešpegu kwennɨja i ogolǯɨja, čtobɨ mat tare, kanǯem ɨg ügolbikujamt. </ta>
            <ta e="T72" id="Seg_613" s="T60">A to mat wsü ʒiznʼ kanǯep ügolbɨhak, tabem nörguhak, maǯöɣend wezde kwajakuhak. </ta>
            <ta e="T87" id="Seg_614" s="T73">Kak koška maǯöɣet kwajagu, na xɨrem madergu wezde, tabep pegu, wezde pot par manembegu. </ta>
            <ta e="T98" id="Seg_615" s="T88">I mat tärbak: man newem puskaj ogolalǯešpijamd, i uǯijamdet wak. </ta>
            <ta e="T113" id="Seg_616" s="T99">I tabedet qumat kɨgak, štobɨ tabet ogolalǯɨjamdet i uǯikujamdet, štobɨ na nɨlʼǯiɣen ilkijamdet wak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_617" s="T1">nagur</ta>
            <ta e="T3" id="Seg_618" s="T2">ne-d</ta>
            <ta e="T5" id="Seg_619" s="T4">man-nan</ta>
            <ta e="T6" id="Seg_620" s="T5">nagur</ta>
            <ta e="T7" id="Seg_621" s="T6">ne-m</ta>
            <ta e="T8" id="Seg_622" s="T7">no</ta>
            <ta e="T9" id="Seg_623" s="T8">tär-ba-k</ta>
            <ta e="T10" id="Seg_624" s="T9">štobɨ</ta>
            <ta e="T11" id="Seg_625" s="T10">wes</ta>
            <ta e="T12" id="Seg_626" s="T11">ogol-a-lǯi-ja-dɨt</ta>
            <ta e="T14" id="Seg_627" s="T13">oqqer</ta>
            <ta e="T15" id="Seg_628" s="T14">warg</ta>
            <ta e="T16" id="Seg_629" s="T15">ne-m</ta>
            <ta e="T17" id="Seg_630" s="T16">ogol-e-mba</ta>
            <ta e="T18" id="Seg_631" s="T17">i</ta>
            <ta e="T19" id="Seg_632" s="T18">tab</ta>
            <ta e="T20" id="Seg_633" s="T19">uǯä</ta>
            <ta e="T21" id="Seg_634" s="T20">i</ta>
            <ta e="T22" id="Seg_635" s="T21">wa-k</ta>
            <ta e="T23" id="Seg_636" s="T22">uǯä</ta>
            <ta e="T24" id="Seg_637" s="T23">i</ta>
            <ta e="T25" id="Seg_638" s="T24">kočʼe-k</ta>
            <ta e="T26" id="Seg_639" s="T25">sarabatɨwa-t</ta>
            <ta e="T28" id="Seg_640" s="T26">ogol-a-lǯe-mba</ta>
            <ta e="T30" id="Seg_641" s="T29">ešo</ta>
            <ta e="T31" id="Seg_642" s="T30">šed</ta>
            <ta e="T32" id="Seg_643" s="T31">ne-m</ta>
            <ta e="T33" id="Seg_644" s="T32">uǯä-ɣ</ta>
            <ta e="T34" id="Seg_645" s="T33">no</ta>
            <ta e="T38" id="Seg_646" s="T37">konči-mba-di</ta>
            <ta e="T39" id="Seg_647" s="T38">a</ta>
            <ta e="T42" id="Seg_648" s="T41">netu</ta>
            <ta e="T44" id="Seg_649" s="T43">mat</ta>
            <ta e="T45" id="Seg_650" s="T44">kɨga-k</ta>
            <ta e="T46" id="Seg_651" s="T45">čtobɨ</ta>
            <ta e="T47" id="Seg_652" s="T46">üčega</ta>
            <ta e="T48" id="Seg_653" s="T47">ne-m</ta>
            <ta e="T49" id="Seg_654" s="T48">ešё</ta>
            <ta e="T50" id="Seg_655" s="T49">ogol-a-ǯe-špe-gu</ta>
            <ta e="T51" id="Seg_656" s="T50">kwen-nɨ-ja</ta>
            <ta e="T52" id="Seg_657" s="T51">i</ta>
            <ta e="T53" id="Seg_658" s="T52">ogol-ǯɨ-ja</ta>
            <ta e="T54" id="Seg_659" s="T53">čtobɨ</ta>
            <ta e="T55" id="Seg_660" s="T54">mat</ta>
            <ta e="T56" id="Seg_661" s="T55">tare</ta>
            <ta e="T57" id="Seg_662" s="T56">kanǯe-m</ta>
            <ta e="T58" id="Seg_663" s="T57">ɨg</ta>
            <ta e="T59" id="Seg_664" s="T58">ügol-bi-ku-ja-mt</ta>
            <ta e="T61" id="Seg_665" s="T60">a</ta>
            <ta e="T62" id="Seg_666" s="T61">to</ta>
            <ta e="T63" id="Seg_667" s="T62">mat</ta>
            <ta e="T66" id="Seg_668" s="T65">kanǯe-p</ta>
            <ta e="T67" id="Seg_669" s="T66">ügol-bɨ-ha-k</ta>
            <ta e="T68" id="Seg_670" s="T67">tabe-m</ta>
            <ta e="T69" id="Seg_671" s="T68">nör-gu-ha-k</ta>
            <ta e="T70" id="Seg_672" s="T69">maǯö-ɣend</ta>
            <ta e="T71" id="Seg_673" s="T70">wezde</ta>
            <ta e="T72" id="Seg_674" s="T71">kwaja-ku-ha-k</ta>
            <ta e="T74" id="Seg_675" s="T73">kak</ta>
            <ta e="T75" id="Seg_676" s="T74">koška</ta>
            <ta e="T76" id="Seg_677" s="T75">maǯö-ɣet</ta>
            <ta e="T77" id="Seg_678" s="T76">kwaja-gu</ta>
            <ta e="T78" id="Seg_679" s="T77">na</ta>
            <ta e="T79" id="Seg_680" s="T78">xɨrem</ta>
            <ta e="T80" id="Seg_681" s="T79">made-r-gu</ta>
            <ta e="T81" id="Seg_682" s="T80">wezde</ta>
            <ta e="T82" id="Seg_683" s="T81">tabe-p</ta>
            <ta e="T83" id="Seg_684" s="T82">pe-gu</ta>
            <ta e="T84" id="Seg_685" s="T83">wezde</ta>
            <ta e="T85" id="Seg_686" s="T84">po-t</ta>
            <ta e="T86" id="Seg_687" s="T85">par</ta>
            <ta e="T87" id="Seg_688" s="T86">mane-mbe-gu</ta>
            <ta e="T89" id="Seg_689" s="T88">i</ta>
            <ta e="T90" id="Seg_690" s="T89">mat</ta>
            <ta e="T91" id="Seg_691" s="T90">tär-ba-k</ta>
            <ta e="T92" id="Seg_692" s="T91">man</ta>
            <ta e="T93" id="Seg_693" s="T92">ne-we-m</ta>
            <ta e="T94" id="Seg_694" s="T93">puskaj</ta>
            <ta e="T95" id="Seg_695" s="T94">ogol-a-lǯe-špi-ja-md</ta>
            <ta e="T96" id="Seg_696" s="T95">i</ta>
            <ta e="T97" id="Seg_697" s="T96">uǯi-ja-mdet</ta>
            <ta e="T98" id="Seg_698" s="T97">wa-k</ta>
            <ta e="T100" id="Seg_699" s="T99">i</ta>
            <ta e="T101" id="Seg_700" s="T100">tab-e-d-e-t</ta>
            <ta e="T102" id="Seg_701" s="T101">qum-a-t</ta>
            <ta e="T103" id="Seg_702" s="T102">kɨga-k</ta>
            <ta e="T104" id="Seg_703" s="T103">štobɨ</ta>
            <ta e="T105" id="Seg_704" s="T104">tab-e-t</ta>
            <ta e="T106" id="Seg_705" s="T105">ogol-a-lǯɨ-ja-mdet</ta>
            <ta e="T107" id="Seg_706" s="T106">i</ta>
            <ta e="T108" id="Seg_707" s="T107">uǯi-ku-ja-mdet</ta>
            <ta e="T109" id="Seg_708" s="T108">štobɨ</ta>
            <ta e="T110" id="Seg_709" s="T109">na</ta>
            <ta e="T111" id="Seg_710" s="T110">nɨlʼǯi-ɣen</ta>
            <ta e="T112" id="Seg_711" s="T111">il-ki-ja-mdet</ta>
            <ta e="T113" id="Seg_712" s="T112">wa-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_713" s="T1">nagur</ta>
            <ta e="T3" id="Seg_714" s="T2">neː-tɨ</ta>
            <ta e="T5" id="Seg_715" s="T4">man-nan</ta>
            <ta e="T6" id="Seg_716" s="T5">nagur</ta>
            <ta e="T7" id="Seg_717" s="T6">neː-mɨ</ta>
            <ta e="T8" id="Seg_718" s="T7">nu</ta>
            <ta e="T9" id="Seg_719" s="T8">tär-mbɨ-k</ta>
            <ta e="T10" id="Seg_720" s="T9">štobɨ</ta>
            <ta e="T11" id="Seg_721" s="T10">wesʼ</ta>
            <ta e="T12" id="Seg_722" s="T11">oqol-ɨ-lʼčǝ-ja-dət</ta>
            <ta e="T14" id="Seg_723" s="T13">okkər</ta>
            <ta e="T15" id="Seg_724" s="T14">wargɨ</ta>
            <ta e="T16" id="Seg_725" s="T15">neː-mɨ</ta>
            <ta e="T17" id="Seg_726" s="T16">oqol-ɨ-mbɨ</ta>
            <ta e="T18" id="Seg_727" s="T17">i</ta>
            <ta e="T19" id="Seg_728" s="T18">tab</ta>
            <ta e="T20" id="Seg_729" s="T19">üdʼa</ta>
            <ta e="T21" id="Seg_730" s="T20">i</ta>
            <ta e="T22" id="Seg_731" s="T21">swa-k</ta>
            <ta e="T23" id="Seg_732" s="T22">üdʼa</ta>
            <ta e="T24" id="Seg_733" s="T23">i</ta>
            <ta e="T25" id="Seg_734" s="T24">koček-k</ta>
            <ta e="T26" id="Seg_735" s="T25">sarabatɨwa-tɨ</ta>
            <ta e="T28" id="Seg_736" s="T26">oqol-ɨ-lʼčǝ-mbɨ</ta>
            <ta e="T30" id="Seg_737" s="T29">iššo</ta>
            <ta e="T31" id="Seg_738" s="T30">šitə</ta>
            <ta e="T32" id="Seg_739" s="T31">neː-mɨ</ta>
            <ta e="T33" id="Seg_740" s="T32">üdʼa-q</ta>
            <ta e="T34" id="Seg_741" s="T33">nu</ta>
            <ta e="T38" id="Seg_742" s="T37">konči-mbɨ-di</ta>
            <ta e="T39" id="Seg_743" s="T38">a</ta>
            <ta e="T42" id="Seg_744" s="T41">nʼetu</ta>
            <ta e="T44" id="Seg_745" s="T43">man</ta>
            <ta e="T45" id="Seg_746" s="T44">kɨge-k</ta>
            <ta e="T46" id="Seg_747" s="T45">štobɨ</ta>
            <ta e="T47" id="Seg_748" s="T46">üčega</ta>
            <ta e="T48" id="Seg_749" s="T47">neː-mɨ</ta>
            <ta e="T49" id="Seg_750" s="T48">iššo</ta>
            <ta e="T50" id="Seg_751" s="T49">oqol-ɨ-nǯe-špɨ-gu</ta>
            <ta e="T51" id="Seg_752" s="T50">qwän-ntɨ-ja</ta>
            <ta e="T52" id="Seg_753" s="T51">i</ta>
            <ta e="T53" id="Seg_754" s="T52">oqol-nǯe-ja</ta>
            <ta e="T54" id="Seg_755" s="T53">štobɨ</ta>
            <ta e="T55" id="Seg_756" s="T54">man</ta>
            <ta e="T56" id="Seg_757" s="T55">tare</ta>
            <ta e="T57" id="Seg_758" s="T56">qanǯe-m</ta>
            <ta e="T58" id="Seg_759" s="T57">ɨgɨ</ta>
            <ta e="T59" id="Seg_760" s="T58">ügol-mbɨ-ku-ja-md</ta>
            <ta e="T61" id="Seg_761" s="T60">a</ta>
            <ta e="T62" id="Seg_762" s="T61">to</ta>
            <ta e="T63" id="Seg_763" s="T62">man</ta>
            <ta e="T66" id="Seg_764" s="T65">qanǯe-p</ta>
            <ta e="T67" id="Seg_765" s="T66">ügol-mbɨ-ŋɨ-k</ta>
            <ta e="T68" id="Seg_766" s="T67">tabek-m</ta>
            <ta e="T69" id="Seg_767" s="T68">nʼür-ku-ŋɨ-k</ta>
            <ta e="T70" id="Seg_768" s="T69">maǯʼo-qɨnt</ta>
            <ta e="T71" id="Seg_769" s="T70">wezde</ta>
            <ta e="T72" id="Seg_770" s="T71">qwaja-ku-ŋɨ-k</ta>
            <ta e="T74" id="Seg_771" s="T73">kak</ta>
            <ta e="T75" id="Seg_772" s="T74">qošqa</ta>
            <ta e="T76" id="Seg_773" s="T75">maǯʼo-qɨn</ta>
            <ta e="T77" id="Seg_774" s="T76">qwaja-gu</ta>
            <ta e="T78" id="Seg_775" s="T77">na</ta>
            <ta e="T79" id="Seg_776" s="T78">hurəm</ta>
            <ta e="T80" id="Seg_777" s="T79">madɨ-r-gu</ta>
            <ta e="T81" id="Seg_778" s="T80">wezde</ta>
            <ta e="T82" id="Seg_779" s="T81">tabek-p</ta>
            <ta e="T83" id="Seg_780" s="T82">peː-gu</ta>
            <ta e="T84" id="Seg_781" s="T83">wezde</ta>
            <ta e="T85" id="Seg_782" s="T84">po-n</ta>
            <ta e="T86" id="Seg_783" s="T85">par</ta>
            <ta e="T87" id="Seg_784" s="T86">mantɨ-mbɨ-gu</ta>
            <ta e="T89" id="Seg_785" s="T88">i</ta>
            <ta e="T90" id="Seg_786" s="T89">man</ta>
            <ta e="T91" id="Seg_787" s="T90">tär-mbɨ-k</ta>
            <ta e="T92" id="Seg_788" s="T91">man</ta>
            <ta e="T93" id="Seg_789" s="T92">neː-mɨ-m</ta>
            <ta e="T94" id="Seg_790" s="T93">puskaj</ta>
            <ta e="T95" id="Seg_791" s="T94">oqol-ɨ-lǯe-špɨ-ja-md</ta>
            <ta e="T96" id="Seg_792" s="T95">i</ta>
            <ta e="T97" id="Seg_793" s="T96">üdʼa-ja-mdet</ta>
            <ta e="T98" id="Seg_794" s="T97">swa-k</ta>
            <ta e="T100" id="Seg_795" s="T99">i</ta>
            <ta e="T101" id="Seg_796" s="T100">tab-ɨ-t-ɨ-n</ta>
            <ta e="T102" id="Seg_797" s="T101">qum-ɨ-t</ta>
            <ta e="T103" id="Seg_798" s="T102">kɨge-k</ta>
            <ta e="T104" id="Seg_799" s="T103">štobɨ</ta>
            <ta e="T105" id="Seg_800" s="T104">tab-ɨ-t</ta>
            <ta e="T106" id="Seg_801" s="T105">oqol-ɨ-lʼčǝ-ja-mdet</ta>
            <ta e="T107" id="Seg_802" s="T106">i</ta>
            <ta e="T108" id="Seg_803" s="T107">üdʼa-ku-ja-mdet</ta>
            <ta e="T109" id="Seg_804" s="T108">štobɨ</ta>
            <ta e="T110" id="Seg_805" s="T109">na</ta>
            <ta e="T111" id="Seg_806" s="T110">nɨlʼǯi-qɨn</ta>
            <ta e="T112" id="Seg_807" s="T111">ele-ku-ja-dət</ta>
            <ta e="T113" id="Seg_808" s="T112">swa-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_809" s="T1">three</ta>
            <ta e="T3" id="Seg_810" s="T2">daughter-3SG</ta>
            <ta e="T5" id="Seg_811" s="T4">I.NOM-ADES</ta>
            <ta e="T6" id="Seg_812" s="T5">three</ta>
            <ta e="T7" id="Seg_813" s="T6">daughter-1SG</ta>
            <ta e="T8" id="Seg_814" s="T7">well</ta>
            <ta e="T9" id="Seg_815" s="T8">think-PST.NAR-1SG.S</ta>
            <ta e="T10" id="Seg_816" s="T9">that</ta>
            <ta e="T11" id="Seg_817" s="T10">all</ta>
            <ta e="T12" id="Seg_818" s="T11">learn-EP-PFV-CO-3PL</ta>
            <ta e="T14" id="Seg_819" s="T13">one</ta>
            <ta e="T15" id="Seg_820" s="T14">big</ta>
            <ta e="T16" id="Seg_821" s="T15">daughter-1SG</ta>
            <ta e="T17" id="Seg_822" s="T16">learn-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T18" id="Seg_823" s="T17">and</ta>
            <ta e="T19" id="Seg_824" s="T18">(s)he.[NOM]</ta>
            <ta e="T20" id="Seg_825" s="T19">work.[3SG.S]</ta>
            <ta e="T21" id="Seg_826" s="T20">and</ta>
            <ta e="T22" id="Seg_827" s="T21">good-ADVZ</ta>
            <ta e="T23" id="Seg_828" s="T22">work.[3SG.S]</ta>
            <ta e="T24" id="Seg_829" s="T23">and</ta>
            <ta e="T25" id="Seg_830" s="T24">much-ADVZ</ta>
            <ta e="T26" id="Seg_831" s="T25">earn-3SG.O</ta>
            <ta e="T28" id="Seg_832" s="T26">learn-EP-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T30" id="Seg_833" s="T29">still</ta>
            <ta e="T31" id="Seg_834" s="T30">two</ta>
            <ta e="T32" id="Seg_835" s="T31">daughter-1SG</ta>
            <ta e="T33" id="Seg_836" s="T32">work-3DU.S</ta>
            <ta e="T34" id="Seg_837" s="T33">well</ta>
            <ta e="T38" id="Seg_838" s="T37">end-PST.NAR-3DU.O</ta>
            <ta e="T39" id="Seg_839" s="T38">but</ta>
            <ta e="T42" id="Seg_840" s="T41">NEG.EX.[3SG.S]</ta>
            <ta e="T44" id="Seg_841" s="T43">I.NOM</ta>
            <ta e="T45" id="Seg_842" s="T44">want-1SG.S</ta>
            <ta e="T46" id="Seg_843" s="T45">that</ta>
            <ta e="T47" id="Seg_844" s="T46">small</ta>
            <ta e="T48" id="Seg_845" s="T47">daughter-1SG</ta>
            <ta e="T49" id="Seg_846" s="T48">still</ta>
            <ta e="T50" id="Seg_847" s="T49">learn-EP-IPFV3-IPFV2-INF</ta>
            <ta e="T51" id="Seg_848" s="T50">go.away-IPFV-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_849" s="T51">and</ta>
            <ta e="T53" id="Seg_850" s="T52">learn-IPFV3-CO.[3SG.S]</ta>
            <ta e="T54" id="Seg_851" s="T53">that</ta>
            <ta e="T55" id="Seg_852" s="T54">I.GEN</ta>
            <ta e="T56" id="Seg_853" s="T55">like</ta>
            <ta e="T57" id="Seg_854" s="T56">sledge-ACC</ta>
            <ta e="T58" id="Seg_855" s="T57">NEG.IMP</ta>
            <ta e="T59" id="Seg_856" s="T58">pull.out-DUR-HAB-CO-IMP.3SG</ta>
            <ta e="T61" id="Seg_857" s="T60">but</ta>
            <ta e="T62" id="Seg_858" s="T61">that</ta>
            <ta e="T63" id="Seg_859" s="T62">I.GEN</ta>
            <ta e="T66" id="Seg_860" s="T65">sledge-ACC</ta>
            <ta e="T67" id="Seg_861" s="T66">pull.out-DUR-CO-1SG.S</ta>
            <ta e="T68" id="Seg_862" s="T67">squirrel-ACC</ta>
            <ta e="T69" id="Seg_863" s="T68">drive-HAB-CO-1SG.S</ta>
            <ta e="T70" id="Seg_864" s="T69">taiga-ILL.3SG</ta>
            <ta e="T71" id="Seg_865" s="T70">everywhere</ta>
            <ta e="T72" id="Seg_866" s="T71">go-HAB-CO-1SG.S</ta>
            <ta e="T74" id="Seg_867" s="T73">how</ta>
            <ta e="T75" id="Seg_868" s="T74">bad</ta>
            <ta e="T76" id="Seg_869" s="T75">taiga-LOC</ta>
            <ta e="T77" id="Seg_870" s="T76">go-INF</ta>
            <ta e="T78" id="Seg_871" s="T77">this</ta>
            <ta e="T79" id="Seg_872" s="T78">wild.animal.[NOM]</ta>
            <ta e="T80" id="Seg_873" s="T79">ask-FRQ-INF</ta>
            <ta e="T81" id="Seg_874" s="T80">everywhere</ta>
            <ta e="T82" id="Seg_875" s="T81">squirrel-ACC</ta>
            <ta e="T83" id="Seg_876" s="T82">look.for-INF</ta>
            <ta e="T84" id="Seg_877" s="T83">everywhere</ta>
            <ta e="T85" id="Seg_878" s="T84">tree-GEN</ta>
            <ta e="T86" id="Seg_879" s="T85">top.[NOM]</ta>
            <ta e="T87" id="Seg_880" s="T86">look-DUR-INF</ta>
            <ta e="T89" id="Seg_881" s="T88">and</ta>
            <ta e="T90" id="Seg_882" s="T89">I.NOM</ta>
            <ta e="T91" id="Seg_883" s="T90">think-PST.NAR-1SG.S</ta>
            <ta e="T92" id="Seg_884" s="T91">I.GEN</ta>
            <ta e="T93" id="Seg_885" s="T92">daughter-1SG-ACC</ta>
            <ta e="T94" id="Seg_886" s="T93">JUSS</ta>
            <ta e="T95" id="Seg_887" s="T94">learn-EP-TR-IPFV2-CO-IMP.3SG</ta>
            <ta e="T96" id="Seg_888" s="T95">and</ta>
            <ta e="T97" id="Seg_889" s="T96">work-CO-IMP.3PL</ta>
            <ta e="T98" id="Seg_890" s="T97">good-ADVZ</ta>
            <ta e="T100" id="Seg_891" s="T99">and</ta>
            <ta e="T101" id="Seg_892" s="T100">(s)he-EP-PL-EP-GEN</ta>
            <ta e="T102" id="Seg_893" s="T101">human.being-EP-PL.[NOM]</ta>
            <ta e="T103" id="Seg_894" s="T102">want-1SG.S</ta>
            <ta e="T104" id="Seg_895" s="T103">that</ta>
            <ta e="T105" id="Seg_896" s="T104">(s)he-EP-PL.[NOM]</ta>
            <ta e="T106" id="Seg_897" s="T105">learn-EP-PFV-CO-IMP.3PL</ta>
            <ta e="T107" id="Seg_898" s="T106">and</ta>
            <ta e="T108" id="Seg_899" s="T107">work-HAB-CO-IMP.3PL</ta>
            <ta e="T109" id="Seg_900" s="T108">that</ta>
            <ta e="T110" id="Seg_901" s="T109">this</ta>
            <ta e="T111" id="Seg_902" s="T110">such-LOC</ta>
            <ta e="T112" id="Seg_903" s="T111">live-HAB-CO-3PL</ta>
            <ta e="T113" id="Seg_904" s="T112">good-ADVZ</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_905" s="T1">три</ta>
            <ta e="T3" id="Seg_906" s="T2">дочь-3SG</ta>
            <ta e="T5" id="Seg_907" s="T4">я.NOM-ADES</ta>
            <ta e="T6" id="Seg_908" s="T5">три</ta>
            <ta e="T7" id="Seg_909" s="T6">дочь-1SG</ta>
            <ta e="T8" id="Seg_910" s="T7">ну</ta>
            <ta e="T9" id="Seg_911" s="T8">думать-PST.NAR-1SG.S</ta>
            <ta e="T10" id="Seg_912" s="T9">чтобы</ta>
            <ta e="T11" id="Seg_913" s="T10">весь</ta>
            <ta e="T12" id="Seg_914" s="T11">обучить-EP-PFV-CO-3PL</ta>
            <ta e="T14" id="Seg_915" s="T13">один</ta>
            <ta e="T15" id="Seg_916" s="T14">большой</ta>
            <ta e="T16" id="Seg_917" s="T15">дочь-1SG</ta>
            <ta e="T17" id="Seg_918" s="T16">учиться-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T18" id="Seg_919" s="T17">и</ta>
            <ta e="T19" id="Seg_920" s="T18">он(а).[NOM]</ta>
            <ta e="T20" id="Seg_921" s="T19">работать.[3SG.S]</ta>
            <ta e="T21" id="Seg_922" s="T20">и</ta>
            <ta e="T22" id="Seg_923" s="T21">хороший-ADVZ</ta>
            <ta e="T23" id="Seg_924" s="T22">работать.[3SG.S]</ta>
            <ta e="T24" id="Seg_925" s="T23">и</ta>
            <ta e="T25" id="Seg_926" s="T24">много-ADVZ</ta>
            <ta e="T26" id="Seg_927" s="T25">зарабатывать-3SG.O</ta>
            <ta e="T28" id="Seg_928" s="T26">учиться-EP-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T30" id="Seg_929" s="T29">еще</ta>
            <ta e="T31" id="Seg_930" s="T30">два</ta>
            <ta e="T32" id="Seg_931" s="T31">дочь-1SG</ta>
            <ta e="T33" id="Seg_932" s="T32">работать-3DU.S</ta>
            <ta e="T34" id="Seg_933" s="T33">ну</ta>
            <ta e="T38" id="Seg_934" s="T37">кончить-PST.NAR-3DU.O</ta>
            <ta e="T39" id="Seg_935" s="T38">а</ta>
            <ta e="T42" id="Seg_936" s="T41">NEG.EX.[3SG.S]</ta>
            <ta e="T44" id="Seg_937" s="T43">я.NOM</ta>
            <ta e="T45" id="Seg_938" s="T44">хотеть-1SG.S</ta>
            <ta e="T46" id="Seg_939" s="T45">чтобы</ta>
            <ta e="T47" id="Seg_940" s="T46">маленький</ta>
            <ta e="T48" id="Seg_941" s="T47">дочь-1SG</ta>
            <ta e="T49" id="Seg_942" s="T48">еще</ta>
            <ta e="T50" id="Seg_943" s="T49">обучить-EP-IPFV3-IPFV2-INF</ta>
            <ta e="T51" id="Seg_944" s="T50">пойти-IPFV-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_945" s="T51">и</ta>
            <ta e="T53" id="Seg_946" s="T52">обучить-IPFV3-CO.[3SG.S]</ta>
            <ta e="T54" id="Seg_947" s="T53">чтобы</ta>
            <ta e="T55" id="Seg_948" s="T54">я.GEN</ta>
            <ta e="T56" id="Seg_949" s="T55">как</ta>
            <ta e="T57" id="Seg_950" s="T56">нарта-ACC</ta>
            <ta e="T58" id="Seg_951" s="T57">NEG.IMP</ta>
            <ta e="T59" id="Seg_952" s="T58">вытащить-DUR-HAB-CO-IMP.3SG</ta>
            <ta e="T61" id="Seg_953" s="T60">а</ta>
            <ta e="T62" id="Seg_954" s="T61">тот</ta>
            <ta e="T63" id="Seg_955" s="T62">я.GEN</ta>
            <ta e="T66" id="Seg_956" s="T65">нарта-ACC</ta>
            <ta e="T67" id="Seg_957" s="T66">вытащить-DUR-CO-1SG.S</ta>
            <ta e="T68" id="Seg_958" s="T67">белка-ACC</ta>
            <ta e="T69" id="Seg_959" s="T68">гонять-HAB-CO-1SG.S</ta>
            <ta e="T70" id="Seg_960" s="T69">тайга-ILL.3SG</ta>
            <ta e="T71" id="Seg_961" s="T70">везде</ta>
            <ta e="T72" id="Seg_962" s="T71">идти-HAB-CO-1SG.S</ta>
            <ta e="T74" id="Seg_963" s="T73">как</ta>
            <ta e="T75" id="Seg_964" s="T74">плохо</ta>
            <ta e="T76" id="Seg_965" s="T75">тайга-LOC</ta>
            <ta e="T77" id="Seg_966" s="T76">идти-INF</ta>
            <ta e="T78" id="Seg_967" s="T77">этот</ta>
            <ta e="T79" id="Seg_968" s="T78">зверь.[NOM]</ta>
            <ta e="T80" id="Seg_969" s="T79">просить-FRQ-INF</ta>
            <ta e="T81" id="Seg_970" s="T80">везде</ta>
            <ta e="T82" id="Seg_971" s="T81">белка-ACC</ta>
            <ta e="T83" id="Seg_972" s="T82">искать-INF</ta>
            <ta e="T84" id="Seg_973" s="T83">везде</ta>
            <ta e="T85" id="Seg_974" s="T84">дерево-GEN</ta>
            <ta e="T86" id="Seg_975" s="T85">верхняя.часть.[NOM]</ta>
            <ta e="T87" id="Seg_976" s="T86">смотреть-DUR-INF</ta>
            <ta e="T89" id="Seg_977" s="T88">и</ta>
            <ta e="T90" id="Seg_978" s="T89">я.NOM</ta>
            <ta e="T91" id="Seg_979" s="T90">думать-PST.NAR-1SG.S</ta>
            <ta e="T92" id="Seg_980" s="T91">я.GEN</ta>
            <ta e="T93" id="Seg_981" s="T92">дочь-1SG-ACC</ta>
            <ta e="T94" id="Seg_982" s="T93">JUSS</ta>
            <ta e="T95" id="Seg_983" s="T94">обучить-EP-TR-IPFV2-CO-IMP.3SG</ta>
            <ta e="T96" id="Seg_984" s="T95">и</ta>
            <ta e="T97" id="Seg_985" s="T96">работать-CO-IMP.3PL</ta>
            <ta e="T98" id="Seg_986" s="T97">хороший-ADVZ</ta>
            <ta e="T100" id="Seg_987" s="T99">и</ta>
            <ta e="T101" id="Seg_988" s="T100">он(а)-EP-PL-EP-GEN</ta>
            <ta e="T102" id="Seg_989" s="T101">человек-EP-PL.[NOM]</ta>
            <ta e="T103" id="Seg_990" s="T102">хотеть-1SG.S</ta>
            <ta e="T104" id="Seg_991" s="T103">чтобы</ta>
            <ta e="T105" id="Seg_992" s="T104">он(а)-EP-PL.[NOM]</ta>
            <ta e="T106" id="Seg_993" s="T105">обучить-EP-PFV-CO-IMP.3PL</ta>
            <ta e="T107" id="Seg_994" s="T106">и</ta>
            <ta e="T108" id="Seg_995" s="T107">работать-HAB-CO-IMP.3PL</ta>
            <ta e="T109" id="Seg_996" s="T108">чтобы</ta>
            <ta e="T110" id="Seg_997" s="T109">этот</ta>
            <ta e="T111" id="Seg_998" s="T110">такой-LOC</ta>
            <ta e="T112" id="Seg_999" s="T111">жить-HAB-CO-3PL</ta>
            <ta e="T113" id="Seg_1000" s="T112">хороший-ADVZ</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1001" s="T1">num</ta>
            <ta e="T3" id="Seg_1002" s="T2">n-n:poss</ta>
            <ta e="T5" id="Seg_1003" s="T4">pers-n:case</ta>
            <ta e="T6" id="Seg_1004" s="T5">num</ta>
            <ta e="T7" id="Seg_1005" s="T6">n-n:poss</ta>
            <ta e="T8" id="Seg_1006" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_1007" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_1008" s="T9">conj</ta>
            <ta e="T11" id="Seg_1009" s="T10">quant</ta>
            <ta e="T12" id="Seg_1010" s="T11">v-n:ins-v&gt;v-v:ins-v:pn</ta>
            <ta e="T14" id="Seg_1011" s="T13">num</ta>
            <ta e="T15" id="Seg_1012" s="T14">adj</ta>
            <ta e="T16" id="Seg_1013" s="T15">n-n:poss</ta>
            <ta e="T17" id="Seg_1014" s="T16">v-n:ins-v:tense.[v:pn]</ta>
            <ta e="T18" id="Seg_1015" s="T17">conj</ta>
            <ta e="T19" id="Seg_1016" s="T18">pers.[n:case]</ta>
            <ta e="T20" id="Seg_1017" s="T19">v.[v:pn]</ta>
            <ta e="T21" id="Seg_1018" s="T20">conj</ta>
            <ta e="T22" id="Seg_1019" s="T21">adj-adj&gt;adv</ta>
            <ta e="T23" id="Seg_1020" s="T22">v.[v:pn]</ta>
            <ta e="T24" id="Seg_1021" s="T23">conj</ta>
            <ta e="T25" id="Seg_1022" s="T24">quant-adj&gt;adv</ta>
            <ta e="T26" id="Seg_1023" s="T25">v-v:pn</ta>
            <ta e="T28" id="Seg_1024" s="T26">v-n:ins-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T30" id="Seg_1025" s="T29">adv</ta>
            <ta e="T31" id="Seg_1026" s="T30">num</ta>
            <ta e="T32" id="Seg_1027" s="T31">n-n:poss</ta>
            <ta e="T33" id="Seg_1028" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_1029" s="T33">ptcl</ta>
            <ta e="T38" id="Seg_1030" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_1031" s="T38">conj</ta>
            <ta e="T42" id="Seg_1032" s="T41">v.[v:pn]</ta>
            <ta e="T44" id="Seg_1033" s="T43">pers</ta>
            <ta e="T45" id="Seg_1034" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_1035" s="T45">conj</ta>
            <ta e="T47" id="Seg_1036" s="T46">adj</ta>
            <ta e="T48" id="Seg_1037" s="T47">n-n:poss</ta>
            <ta e="T49" id="Seg_1038" s="T48">adv</ta>
            <ta e="T50" id="Seg_1039" s="T49">v-n:ins-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T51" id="Seg_1040" s="T50">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T52" id="Seg_1041" s="T51">conj</ta>
            <ta e="T53" id="Seg_1042" s="T52">v-v&gt;v-v:ins.[v:pn]</ta>
            <ta e="T54" id="Seg_1043" s="T53">conj</ta>
            <ta e="T55" id="Seg_1044" s="T54">pers</ta>
            <ta e="T56" id="Seg_1045" s="T55">pp</ta>
            <ta e="T57" id="Seg_1046" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1047" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1048" s="T58">v-v&gt;v-v&gt;v-v:ins-v:mood</ta>
            <ta e="T61" id="Seg_1049" s="T60">conj</ta>
            <ta e="T62" id="Seg_1050" s="T61">dem</ta>
            <ta e="T63" id="Seg_1051" s="T62">pers</ta>
            <ta e="T66" id="Seg_1052" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1053" s="T66">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T68" id="Seg_1054" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1055" s="T68">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T70" id="Seg_1056" s="T69">n-n:case.poss</ta>
            <ta e="T71" id="Seg_1057" s="T70">adv</ta>
            <ta e="T72" id="Seg_1058" s="T71">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T74" id="Seg_1059" s="T73">interrog</ta>
            <ta e="T75" id="Seg_1060" s="T74">adv</ta>
            <ta e="T76" id="Seg_1061" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_1062" s="T76">v-v:inf</ta>
            <ta e="T78" id="Seg_1063" s="T77">dem</ta>
            <ta e="T79" id="Seg_1064" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_1065" s="T79">v-v&gt;v-v:inf</ta>
            <ta e="T81" id="Seg_1066" s="T80">adv</ta>
            <ta e="T82" id="Seg_1067" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1068" s="T82">v-v:inf</ta>
            <ta e="T84" id="Seg_1069" s="T83">adv</ta>
            <ta e="T85" id="Seg_1070" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1071" s="T85">n.[n:case]</ta>
            <ta e="T87" id="Seg_1072" s="T86">v-v&gt;v-v:inf</ta>
            <ta e="T89" id="Seg_1073" s="T88">conj</ta>
            <ta e="T90" id="Seg_1074" s="T89">pers</ta>
            <ta e="T91" id="Seg_1075" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1076" s="T91">pers</ta>
            <ta e="T93" id="Seg_1077" s="T92">n-n:poss-n:case</ta>
            <ta e="T94" id="Seg_1078" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_1079" s="T94">v-n:ins-v&gt;v-v&gt;v-v:ins-v:mood</ta>
            <ta e="T96" id="Seg_1080" s="T95">conj</ta>
            <ta e="T97" id="Seg_1081" s="T96">v-v:ins-v:mood</ta>
            <ta e="T98" id="Seg_1082" s="T97">adj-adj&gt;adv</ta>
            <ta e="T100" id="Seg_1083" s="T99">conj</ta>
            <ta e="T101" id="Seg_1084" s="T100">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T102" id="Seg_1085" s="T101">n-n:ins-n:num.[n:case]</ta>
            <ta e="T103" id="Seg_1086" s="T102">v-v:pn</ta>
            <ta e="T104" id="Seg_1087" s="T103">conj</ta>
            <ta e="T105" id="Seg_1088" s="T104">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T106" id="Seg_1089" s="T105">v-n:ins-v&gt;v-v:ins-v:mood</ta>
            <ta e="T107" id="Seg_1090" s="T106">conj</ta>
            <ta e="T108" id="Seg_1091" s="T107">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T109" id="Seg_1092" s="T108">conj</ta>
            <ta e="T110" id="Seg_1093" s="T109">dem</ta>
            <ta e="T111" id="Seg_1094" s="T110">dem-n:case</ta>
            <ta e="T112" id="Seg_1095" s="T111">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T113" id="Seg_1096" s="T112">adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1097" s="T1">num</ta>
            <ta e="T3" id="Seg_1098" s="T2">n</ta>
            <ta e="T5" id="Seg_1099" s="T4">pers</ta>
            <ta e="T6" id="Seg_1100" s="T5">num</ta>
            <ta e="T7" id="Seg_1101" s="T6">n</ta>
            <ta e="T8" id="Seg_1102" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_1103" s="T8">v</ta>
            <ta e="T10" id="Seg_1104" s="T9">conj</ta>
            <ta e="T11" id="Seg_1105" s="T10">quant</ta>
            <ta e="T12" id="Seg_1106" s="T11">v</ta>
            <ta e="T14" id="Seg_1107" s="T13">num</ta>
            <ta e="T15" id="Seg_1108" s="T14">adj</ta>
            <ta e="T16" id="Seg_1109" s="T15">n</ta>
            <ta e="T17" id="Seg_1110" s="T16">v</ta>
            <ta e="T18" id="Seg_1111" s="T17">conj</ta>
            <ta e="T19" id="Seg_1112" s="T18">pers</ta>
            <ta e="T20" id="Seg_1113" s="T19">v</ta>
            <ta e="T21" id="Seg_1114" s="T20">conj</ta>
            <ta e="T22" id="Seg_1115" s="T21">adv</ta>
            <ta e="T23" id="Seg_1116" s="T22">v</ta>
            <ta e="T24" id="Seg_1117" s="T23">conj</ta>
            <ta e="T25" id="Seg_1118" s="T24">quant</ta>
            <ta e="T26" id="Seg_1119" s="T25">v</ta>
            <ta e="T28" id="Seg_1120" s="T26">v</ta>
            <ta e="T30" id="Seg_1121" s="T29">adv</ta>
            <ta e="T31" id="Seg_1122" s="T30">num</ta>
            <ta e="T32" id="Seg_1123" s="T31">n</ta>
            <ta e="T33" id="Seg_1124" s="T32">v</ta>
            <ta e="T34" id="Seg_1125" s="T33">ptcl</ta>
            <ta e="T38" id="Seg_1126" s="T37">v</ta>
            <ta e="T39" id="Seg_1127" s="T38">conj</ta>
            <ta e="T42" id="Seg_1128" s="T41">v</ta>
            <ta e="T44" id="Seg_1129" s="T43">pers</ta>
            <ta e="T45" id="Seg_1130" s="T44">v</ta>
            <ta e="T46" id="Seg_1131" s="T45">conj</ta>
            <ta e="T47" id="Seg_1132" s="T46">adj</ta>
            <ta e="T48" id="Seg_1133" s="T47">n</ta>
            <ta e="T49" id="Seg_1134" s="T48">adv</ta>
            <ta e="T50" id="Seg_1135" s="T49">v</ta>
            <ta e="T51" id="Seg_1136" s="T50">v</ta>
            <ta e="T52" id="Seg_1137" s="T51">conj</ta>
            <ta e="T53" id="Seg_1138" s="T52">v</ta>
            <ta e="T54" id="Seg_1139" s="T53">conj</ta>
            <ta e="T55" id="Seg_1140" s="T54">pers</ta>
            <ta e="T56" id="Seg_1141" s="T55">pp</ta>
            <ta e="T57" id="Seg_1142" s="T56">n</ta>
            <ta e="T58" id="Seg_1143" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1144" s="T58">v</ta>
            <ta e="T61" id="Seg_1145" s="T60">conj</ta>
            <ta e="T62" id="Seg_1146" s="T61">dem</ta>
            <ta e="T63" id="Seg_1147" s="T62">pers</ta>
            <ta e="T66" id="Seg_1148" s="T65">n</ta>
            <ta e="T67" id="Seg_1149" s="T66">v</ta>
            <ta e="T68" id="Seg_1150" s="T67">n</ta>
            <ta e="T69" id="Seg_1151" s="T68">n</ta>
            <ta e="T70" id="Seg_1152" s="T69">n</ta>
            <ta e="T71" id="Seg_1153" s="T70">adv</ta>
            <ta e="T72" id="Seg_1154" s="T71">v</ta>
            <ta e="T74" id="Seg_1155" s="T73">interrog</ta>
            <ta e="T75" id="Seg_1156" s="T74">adv</ta>
            <ta e="T76" id="Seg_1157" s="T75">n</ta>
            <ta e="T77" id="Seg_1158" s="T76">v</ta>
            <ta e="T78" id="Seg_1159" s="T77">pro</ta>
            <ta e="T79" id="Seg_1160" s="T78">n</ta>
            <ta e="T80" id="Seg_1161" s="T79">v</ta>
            <ta e="T81" id="Seg_1162" s="T80">adv</ta>
            <ta e="T82" id="Seg_1163" s="T81">n</ta>
            <ta e="T83" id="Seg_1164" s="T82">v</ta>
            <ta e="T84" id="Seg_1165" s="T83">adv</ta>
            <ta e="T85" id="Seg_1166" s="T84">n</ta>
            <ta e="T86" id="Seg_1167" s="T85">n</ta>
            <ta e="T87" id="Seg_1168" s="T86">v</ta>
            <ta e="T89" id="Seg_1169" s="T88">conj</ta>
            <ta e="T90" id="Seg_1170" s="T89">pers</ta>
            <ta e="T91" id="Seg_1171" s="T90">v</ta>
            <ta e="T92" id="Seg_1172" s="T91">pers</ta>
            <ta e="T93" id="Seg_1173" s="T92">n</ta>
            <ta e="T94" id="Seg_1174" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_1175" s="T94">v</ta>
            <ta e="T96" id="Seg_1176" s="T95">conj</ta>
            <ta e="T97" id="Seg_1177" s="T96">v</ta>
            <ta e="T98" id="Seg_1178" s="T97">adv</ta>
            <ta e="T100" id="Seg_1179" s="T99">conj</ta>
            <ta e="T101" id="Seg_1180" s="T100">pers</ta>
            <ta e="T102" id="Seg_1181" s="T101">n</ta>
            <ta e="T103" id="Seg_1182" s="T102">v</ta>
            <ta e="T104" id="Seg_1183" s="T103">conj</ta>
            <ta e="T105" id="Seg_1184" s="T104">n</ta>
            <ta e="T106" id="Seg_1185" s="T105">v</ta>
            <ta e="T107" id="Seg_1186" s="T106">conj</ta>
            <ta e="T108" id="Seg_1187" s="T107">v</ta>
            <ta e="T109" id="Seg_1188" s="T108">conj</ta>
            <ta e="T110" id="Seg_1189" s="T109">pro</ta>
            <ta e="T111" id="Seg_1190" s="T110">adj</ta>
            <ta e="T112" id="Seg_1191" s="T111">v</ta>
            <ta e="T113" id="Seg_1192" s="T112">adv</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T7" id="Seg_1193" s="T6">np.h:S</ta>
            <ta e="T9" id="Seg_1194" s="T8">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_1195" s="T9">s:compl</ta>
            <ta e="T16" id="Seg_1196" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_1197" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_1198" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_1199" s="T19">v:pred</ta>
            <ta e="T23" id="Seg_1200" s="T22">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_1201" s="T25">0.3.h:S v:pred</ta>
            <ta e="T28" id="Seg_1202" s="T26">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_1203" s="T31">np.h:S</ta>
            <ta e="T33" id="Seg_1204" s="T32">v:pred</ta>
            <ta e="T37" id="Seg_1205" s="T36">np:O</ta>
            <ta e="T38" id="Seg_1206" s="T37">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_1207" s="T39">np:S</ta>
            <ta e="T42" id="Seg_1208" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_1209" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_1210" s="T44">v:pred</ta>
            <ta e="T50" id="Seg_1211" s="T45">s:compl</ta>
            <ta e="T51" id="Seg_1212" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_1213" s="T52">0.3.h:S v:pred</ta>
            <ta e="T59" id="Seg_1214" s="T53">s:purp</ta>
            <ta e="T63" id="Seg_1215" s="T62">pro.h:S</ta>
            <ta e="T66" id="Seg_1216" s="T65">np:O</ta>
            <ta e="T67" id="Seg_1217" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_1218" s="T67">np:O</ta>
            <ta e="T69" id="Seg_1219" s="T68">0.1.h:S v:pred</ta>
            <ta e="T72" id="Seg_1220" s="T71">0.1.h:S v:pred</ta>
            <ta e="T75" id="Seg_1221" s="T74">adj:pred</ta>
            <ta e="T77" id="Seg_1222" s="T76">v:S</ta>
            <ta e="T80" id="Seg_1223" s="T79">v:S</ta>
            <ta e="T83" id="Seg_1224" s="T82">v:S</ta>
            <ta e="T87" id="Seg_1225" s="T86">v:S</ta>
            <ta e="T90" id="Seg_1226" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_1227" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_1228" s="T92">np.h:O</ta>
            <ta e="T95" id="Seg_1229" s="T94">0.3.h:S v:pred</ta>
            <ta e="T97" id="Seg_1230" s="T96">0.3.h:S v:pred</ta>
            <ta e="T103" id="Seg_1231" s="T102">0.1.h:S v:pred</ta>
            <ta e="T108" id="Seg_1232" s="T103">s:compl</ta>
            <ta e="T113" id="Seg_1233" s="T108">s:purp</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T5" id="Seg_1234" s="T4">pro.h:Poss</ta>
            <ta e="T7" id="Seg_1235" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_1236" s="T8">0.1.h:E</ta>
            <ta e="T12" id="Seg_1237" s="T11">0.3.h:A</ta>
            <ta e="T16" id="Seg_1238" s="T15">pro.h:A 0.1.h:Poss</ta>
            <ta e="T19" id="Seg_1239" s="T18">pro.h:A</ta>
            <ta e="T23" id="Seg_1240" s="T22">0.3.h:A</ta>
            <ta e="T26" id="Seg_1241" s="T25">0.3.h:Th</ta>
            <ta e="T28" id="Seg_1242" s="T26">0.3.h:A</ta>
            <ta e="T32" id="Seg_1243" s="T31">np.h:A 0.1.h:Poss</ta>
            <ta e="T37" id="Seg_1244" s="T36">np:Th</ta>
            <ta e="T38" id="Seg_1245" s="T37">0.3.h:A</ta>
            <ta e="T41" id="Seg_1246" s="T39">np:Th</ta>
            <ta e="T44" id="Seg_1247" s="T43">pro.h:E</ta>
            <ta e="T48" id="Seg_1248" s="T47">np.h:A 0.1.h:Poss</ta>
            <ta e="T51" id="Seg_1249" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_1250" s="T52">0.3.h:A</ta>
            <ta e="T57" id="Seg_1251" s="T56">np:Th</ta>
            <ta e="T59" id="Seg_1252" s="T58">0.3.h:A</ta>
            <ta e="T63" id="Seg_1253" s="T62">pro.h:A</ta>
            <ta e="T66" id="Seg_1254" s="T65">np:Th</ta>
            <ta e="T68" id="Seg_1255" s="T67">np:Th</ta>
            <ta e="T69" id="Seg_1256" s="T68">0.1.h:A</ta>
            <ta e="T70" id="Seg_1257" s="T69">np:G</ta>
            <ta e="T72" id="Seg_1258" s="T71">0.1.h:A</ta>
            <ta e="T76" id="Seg_1259" s="T75">np:L</ta>
            <ta e="T77" id="Seg_1260" s="T76">v:Th</ta>
            <ta e="T80" id="Seg_1261" s="T79">v:Th</ta>
            <ta e="T81" id="Seg_1262" s="T80">adv:L</ta>
            <ta e="T82" id="Seg_1263" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_1264" s="T82">v:Th</ta>
            <ta e="T84" id="Seg_1265" s="T83">adv:L</ta>
            <ta e="T85" id="Seg_1266" s="T84">np:Poss</ta>
            <ta e="T87" id="Seg_1267" s="T86">v:Th</ta>
            <ta e="T90" id="Seg_1268" s="T89">pro.h:E</ta>
            <ta e="T92" id="Seg_1269" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_1270" s="T92">np.h:Th</ta>
            <ta e="T95" id="Seg_1271" s="T94">0.3.h:A</ta>
            <ta e="T97" id="Seg_1272" s="T96">0.3.h:A</ta>
            <ta e="T101" id="Seg_1273" s="T100">pro.h:Poss</ta>
            <ta e="T103" id="Seg_1274" s="T102">0.1.h:E</ta>
            <ta e="T105" id="Seg_1275" s="T104">pro.h:A</ta>
            <ta e="T112" id="Seg_1276" s="T111">0.3.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_1277" s="T7">RUS:disc</ta>
            <ta e="T10" id="Seg_1278" s="T9">RUS:gram</ta>
            <ta e="T11" id="Seg_1279" s="T10">RUS:core</ta>
            <ta e="T18" id="Seg_1280" s="T17">RUS:gram</ta>
            <ta e="T21" id="Seg_1281" s="T20">RUS:gram</ta>
            <ta e="T24" id="Seg_1282" s="T23">RUS:gram</ta>
            <ta e="T26" id="Seg_1283" s="T25">RUS:cult</ta>
            <ta e="T30" id="Seg_1284" s="T29">RUS:gram</ta>
            <ta e="T34" id="Seg_1285" s="T33">RUS:disc</ta>
            <ta e="T38" id="Seg_1286" s="T37">RUS:core</ta>
            <ta e="T39" id="Seg_1287" s="T38">RUS:gram</ta>
            <ta e="T42" id="Seg_1288" s="T41">RUS:gram</ta>
            <ta e="T46" id="Seg_1289" s="T45">RUS:gram</ta>
            <ta e="T49" id="Seg_1290" s="T48">RUS:gram</ta>
            <ta e="T52" id="Seg_1291" s="T51">RUS:gram</ta>
            <ta e="T54" id="Seg_1292" s="T53">RUS:gram</ta>
            <ta e="T61" id="Seg_1293" s="T60">RUS:gram</ta>
            <ta e="T71" id="Seg_1294" s="T70">RUS:core</ta>
            <ta e="T74" id="Seg_1295" s="T73">RUS:gram</ta>
            <ta e="T81" id="Seg_1296" s="T80">RUS:core</ta>
            <ta e="T84" id="Seg_1297" s="T83">RUS:core</ta>
            <ta e="T89" id="Seg_1298" s="T88">RUS:gram</ta>
            <ta e="T94" id="Seg_1299" s="T93">RUS:gram</ta>
            <ta e="T96" id="Seg_1300" s="T95">RUS:gram</ta>
            <ta e="T100" id="Seg_1301" s="T99">RUS:gram</ta>
            <ta e="T104" id="Seg_1302" s="T103">RUS:gram</ta>
            <ta e="T107" id="Seg_1303" s="T106">RUS:gram</ta>
            <ta e="T109" id="Seg_1304" s="T108">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T37" id="Seg_1305" s="T34">RUS:int.ins</ta>
            <ta e="T41" id="Seg_1306" s="T39">RUS:int.ins</ta>
            <ta e="T65" id="Seg_1307" s="T63">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1308" s="T1">Three daughters.</ta>
            <ta e="T12" id="Seg_1309" s="T4">I have three daughters, but I think that they would all be educated.</ta>
            <ta e="T28" id="Seg_1310" s="T13">One oldest daughter got educated, and she works, and she works well and makes a lot of money – she is educated.</ta>
            <ta e="T42" id="Seg_1311" s="T29">Two more daughters are working, but they finished ten grades and have no specializations.</ta>
            <ta e="T59" id="Seg_1312" s="T43">I want my youngest daughter to go and study more, get educated, so she wouldn’t pull sleds like me.</ta>
            <ta e="T72" id="Seg_1313" s="T60">I pulled sleds all my life, chased squirrels, and walked the taiga.</ta>
            <ta e="T87" id="Seg_1314" s="T73">It’s so bad to walk the taiga, ask this beast everywhere, look for squirrels, look up to the tops of trees everywhere.</ta>
            <ta e="T98" id="Seg_1315" s="T88">And I think: my daughters should get educated and work well.</ta>
            <ta e="T113" id="Seg_1316" s="T99">I want their husbands to be educated and working – to live well in this world.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1317" s="T1">Drei Töchter</ta>
            <ta e="T12" id="Seg_1318" s="T4">Ich habe drei Töchter, aber ich meine, sie würden alle gebildet werden.</ta>
            <ta e="T28" id="Seg_1319" s="T13">Eine, älteste Tochter hatte eine Bildung, und sie arbeitet, und sie arbeitet gut und verdient viel Geld – sie ist gebildet.</ta>
            <ta e="T42" id="Seg_1320" s="T29">Zwei weitere Töchter arbeiten, aber sie schlossen zehn Klassen ab und haben sich nicht spezialisiert.</ta>
            <ta e="T59" id="Seg_1321" s="T43">Ich will dass meine jüngste Tochter weiter studieren geht, gebildet wird, damit sie nicht Schlitten ziehen würde wie ich.</ta>
            <ta e="T72" id="Seg_1322" s="T60">Ich zog Schlitten meines ganzen Lebens, jagte Eichhörnchen, und wanderte die Taiga.</ta>
            <ta e="T87" id="Seg_1323" s="T73">Es ist so schlimm in der Taiga zu wandern, frage dieses Tier überall, schaue nach Eichhörnchen, schaue überall zu den Baumwipfeln hinauf.</ta>
            <ta e="T98" id="Seg_1324" s="T88">Und ich meine: meine Töchter sollten eine Bildung machen und gut arbeiten.</ta>
            <ta e="T113" id="Seg_1325" s="T99">Ich will dass ihre Ehemänner gebildet sind und berufstätig – um gut zu leben in dieser Welt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1326" s="T1">Три дочери.</ta>
            <ta e="T12" id="Seg_1327" s="T4">У меня три дочери, но думаю, чтобы все выучились.</ta>
            <ta e="T28" id="Seg_1328" s="T13">Одна старшая дочь выучилась, и она работает, и хорошо работает, и много зарабатывает — выучилась.</ta>
            <ta e="T42" id="Seg_1329" s="T29">Ещё две дочери работают, но по десять классов закончили, а никакой специальности нет.</ta>
            <ta e="T59" id="Seg_1330" s="T43">Я хочу, чтобы младшая дочь ещё учиться пошла и выучилась, чтобы, как я, нарты не таскала.</ta>
            <ta e="T72" id="Seg_1331" s="T60">А то я всю жизнь нарты таскала, за белками гонялась, в тайге везде ходила.</ta>
            <ta e="T87" id="Seg_1332" s="T73">Как плохо по тайге ходить, этого зверя просить везде, белку искать, везде на верхушки деревьев смотреть.</ta>
            <ta e="T98" id="Seg_1333" s="T88">И я думаю: мои дочери пусть выучатся, работают хорошо.</ta>
            <ta e="T113" id="Seg_1334" s="T99">И хочу, чтобы их мужья выучились и работали — на этом свете хорошо жили.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_1335" s="T4">[AAV:] ogolalǯijamdɨt ? [WNB:] m is superfluous</ta>
            <ta e="T28" id="Seg_1336" s="T13">[AAV:] fak &gt; wak; сараба́тывает &gt; сараба́тыват</ta>
            <ta e="T59" id="Seg_1337" s="T43">[AAV:] ogolaǯešpegu &gt; ogolalǯešpegu; ogolǯɨja &gt; ogolalǯɨja; ɨg ügolbikujamd &gt; ɨg ügolbikijamst ? </ta>
            <ta e="T72" id="Seg_1338" s="T60">[AAV:] nörxugak &gt; nörguxak ? маджӧӷэнд &gt; maǯʼöən ?</ta>
            <ta e="T98" id="Seg_1339" s="T88">[AAV:] ogolalǯešpijamd &gt; ogolalǯešpijamdət; у́джиямдэт фак &gt; и у́джиямдэт вак [WNB:] newem - glossing is tentative</ta>
            <ta e="T113" id="Seg_1340" s="T99">[AAV:] чтоб та́бэт &gt; чтобы та́бэт; — &gt; чтобы; илки́янд фак &gt; элки́яндэт вак</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
