<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_196X_WorkAsPostwoman_nar</transcription-name>
         <referenced-file url="YIF_196X_WorkAsPostwoman_nar.wav" />
         <referenced-file url="YIF_196X_WorkAsPostwoman_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_WorkAsPostwoman_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">177</ud-information>
            <ud-information attribute-name="# HIAT:w">113</ud-information>
            <ud-information attribute-name="# e">117</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T118" time="0.0" />
         <tli id="T0" time="1.61" type="appl" />
         <tli id="T1" time="2.642" type="appl" />
         <tli id="T2" time="3.675" type="appl" />
         <tli id="T3" time="4.708" type="appl" />
         <tli id="T4" time="5.74" type="appl" />
         <tli id="T5" time="6.99" type="appl" />
         <tli id="T6" time="7.79" type="appl" />
         <tli id="T7" time="8.746328094477" />
         <tli id="T8" time="8.99" type="appl" />
         <tli id="T9" time="9.435" type="appl" />
         <tli id="T10" time="9.88" type="appl" />
         <tli id="T11" time="10.325" type="appl" />
         <tli id="T12" time="10.77" type="appl" />
         <tli id="T13" time="11.392892312089327" />
         <tli id="T14" time="11.652882247824543" />
         <tli id="T15" time="12.05286676434026" />
         <tli id="T16" time="12.839502980154501" />
         <tli id="T17" time="13.459478980753863" />
         <tli id="T18" time="14.192783927699342" />
         <tli id="T19" time="15.058" type="appl" />
         <tli id="T20" time="15.717" type="appl" />
         <tli id="T21" time="16.375" type="appl" />
         <tli id="T22" time="17.034" type="appl" />
         <tli id="T23" time="17.692" type="appl" />
         <tli id="T24" time="18.351" type="appl" />
         <tli id="T25" time="19.009" type="appl" />
         <tli id="T26" time="19.668" type="appl" />
         <tli id="T27" time="20.326" type="appl" />
         <tli id="T28" time="20.985" type="appl" />
         <tli id="T119" time="21.314" type="intp" />
         <tli id="T29" time="21.643" type="appl" />
         <tli id="T31" time="23.545755205558518" />
         <tli id="T32" time="24.264" type="appl" />
         <tli id="T33" time="24.908" type="appl" />
         <tli id="T34" time="25.553" type="appl" />
         <tli id="T35" time="26.197" type="appl" />
         <tli id="T36" time="26.841" type="appl" />
         <tli id="T37" time="27.485" type="appl" />
         <tli id="T38" time="28.129" type="appl" />
         <tli id="T39" time="28.773" type="appl" />
         <tli id="T40" time="29.418" type="appl" />
         <tli id="T41" time="30.062" type="appl" />
         <tli id="T42" time="30.706" type="appl" />
         <tli id="T43" time="31.35" type="appl" />
         <tli id="T44" time="32.414" type="appl" />
         <tli id="T45" time="33.069" type="appl" />
         <tli id="T46" time="33.723" type="appl" />
         <tli id="T47" time="34.377" type="appl" />
         <tli id="T48" time="35.031" type="appl" />
         <tli id="T49" time="35.686" type="appl" />
         <tli id="T50" time="36.34" type="appl" />
         <tli id="T51" time="37.494" type="appl" />
         <tli id="T52" time="38.219" type="appl" />
         <tli id="T53" time="38.943" type="appl" />
         <tli id="T54" time="39.667" type="appl" />
         <tli id="T55" time="40.391" type="appl" />
         <tli id="T56" time="41.116" type="appl" />
         <tli id="T57" time="41.84" type="appl" />
         <tli id="T58" time="42.847" type="appl" />
         <tli id="T59" time="43.415" type="appl" />
         <tli id="T60" time="43.982" type="appl" />
         <tli id="T61" time="44.549" type="appl" />
         <tli id="T62" time="45.116" type="appl" />
         <tli id="T63" time="45.684" type="appl" />
         <tli id="T64" time="46.251" type="appl" />
         <tli id="T65" time="46.818" type="appl" />
         <tli id="T66" time="47.385" type="appl" />
         <tli id="T67" time="47.953" type="appl" />
         <tli id="T68" time="48.8314430579604" />
         <tli id="T69" time="49.475" type="appl" />
         <tli id="T70" time="50.0" type="appl" />
         <tli id="T71" time="50.525" type="appl" />
         <tli id="T72" time="51.05" type="appl" />
         <tli id="T73" time="51.575" type="appl" />
         <tli id="T74" time="52.1" type="appl" />
         <tli id="T75" time="52.625" type="appl" />
         <tli id="T76" time="54.21790121370538" />
         <tli id="T77" time="54.78" type="appl" />
         <tli id="T78" time="55.35" type="appl" />
         <tli id="T79" time="55.92" type="appl" />
         <tli id="T80" time="56.96446156044664" />
         <tli id="T81" time="57.466" type="appl" />
         <tli id="T82" time="58.002" type="appl" />
         <tli id="T83" time="58.538" type="appl" />
         <tli id="T84" time="59.073" type="appl" />
         <tli id="T85" time="59.609" type="appl" />
         <tli id="T86" time="60.145" type="appl" />
         <tli id="T87" time="60.681" type="appl" />
         <tli id="T88" time="61.217" type="appl" />
         <tli id="T89" time="61.752" type="appl" />
         <tli id="T116" time="62.019999999999996" type="intp" />
         <tli id="T90" time="62.437583028103354" />
         <tli id="T91" time="62.824" type="appl" />
         <tli id="T92" time="63.36" type="appl" />
         <tli id="T93" time="63.875" type="appl" />
         <tli id="T94" time="64.23" type="appl" />
         <tli id="T95" time="64.585" type="appl" />
         <tli id="T96" time="64.94" type="appl" />
         <tli id="T97" time="65.295" type="appl" />
         <tli id="T98" time="65.93744754761588" />
         <tli id="T99" time="66.487" type="appl" />
         <tli id="T100" time="67.063" type="appl" />
         <tli id="T101" time="68.35735387253595" />
         <tli id="T102" time="68.863" type="appl" />
         <tli id="T103" time="69.307" type="appl" />
         <tli id="T104" time="69.75" type="appl" />
         <tli id="T105" time="70.193" type="appl" />
         <tli id="T106" time="70.637" type="appl" />
         <tli id="T107" time="71.49056591857574" />
         <tli id="T108" time="71.907" type="appl" />
         <tli id="T109" time="72.295" type="appl" />
         <tli id="T110" time="72.684" type="appl" />
         <tli id="T111" time="73.073" type="appl" />
         <tli id="T112" time="73.461" type="appl" />
         <tli id="T113" time="73.85" type="appl" />
         <tli id="T115" time="74.627" type="appl" />
         <tli id="T117" time="75.077" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T117" id="Seg_0" n="sc" s="T118">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T118">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T0" id="Seg_5" n="HIAT:non-pho" s="T118">…</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1" id="Seg_10" n="HIAT:w" s="T0">Počta</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_13" n="HIAT:w" s="T1">wazimbukuhap</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_16" n="HIAT:w" s="T2">Lɨmbelʼmute</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_19" n="HIAT:w" s="T3">Kananatan</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_23" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_25" n="HIAT:w" s="T4">Nɨlʼǯik</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_28" n="HIAT:w" s="T5">täšekwa</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_32" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_34" n="HIAT:w" s="T6">Aštehe</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_37" n="HIAT:w" s="T7">woobše</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_40" n="HIAT:w" s="T8">man</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">nadrap</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">čʼem</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">čünduhe</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_54" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_56" n="HIAT:w" s="T12">Man</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_59" n="HIAT:w" s="T13">nɨlʼǯiŋ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14">ašten</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">ogolembuhak</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_69" n="HIAT:w" s="T16">qwajakuhaŋ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">aštehe</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_76" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_78" n="HIAT:w" s="T18">Kak</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_81" n="HIAT:w" s="T19">pen</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_84" n="HIAT:w" s="T20">üpoʒəlage</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">kaŋ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">aštep</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">šurnirlebe</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_96" n="HIAT:ip">—</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">ašte</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">nɨlʼǯik</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">čʼek</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">kuralešpa</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_111" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_113" n="HIAT:w" s="T28">wet</ts>
                  <nts id="Seg_114" n="HIAT:ip">)</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <nts id="Seg_118" n="HIAT:ip">(</nts>
                  <ats e="T29" id="Seg_119" n="HIAT:non-pho" s="T119">…</ats>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip">)</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T29">kuralešpa</ts>
                  <nts id="Seg_125" n="HIAT:ip">!</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_128" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_130" n="HIAT:w" s="T31">No</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_134" n="HIAT:w" s="T32">mat</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_137" n="HIAT:w" s="T33">kušid</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_140" n="HIAT:w" s="T34">pot</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_143" n="HIAT:w" s="T35">kund</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_146" n="HIAT:w" s="T36">xɨr</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_149" n="HIAT:w" s="T37">aštəhə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_152" n="HIAT:w" s="T38">počta</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_155" n="HIAT:w" s="T39">wazimbham</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">aštehe</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">Lɨmbelʼmutte</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">Kananatʼan</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_168" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Našaqotte</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">nʼejtuha</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">wattə</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">a</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">tolʼko</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">aštehe</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">qwajakkuhut</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">I</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">maǯʼön</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">aštehe</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">qwajakkuhak</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_208" n="HIAT:w" s="T54">i</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_211" n="HIAT:w" s="T55">počtap</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_214" n="HIAT:w" s="T56">taskajembukuhak</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_218" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">No</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">taper</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_226" n="HIAT:w" s="T59">aštep</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_230" n="HIAT:w" s="T60">potom</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_233" n="HIAT:w" s="T61">kalfos</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_236" n="HIAT:w" s="T62">teː</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_239" n="HIAT:w" s="T63">mɨxat</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">wesʼ</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">na</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">aštep</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">taːrxat</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_256" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_258" n="HIAT:w" s="T68">Meka</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_261" n="HIAT:w" s="T69">naj</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_264" n="HIAT:w" s="T70">tetə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_267" n="HIAT:w" s="T71">para</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72">mekuhat</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">mat</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">potom</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">merəxap</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_284" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_286" n="HIAT:w" s="T76">Nau</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">tawčin</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_293" n="HIAT:w" s="T78">Napas</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_297" n="HIAT:w" s="T79">tögu</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_301" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">I</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_306" n="HIAT:w" s="T81">loɣam</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_309" n="HIAT:w" s="T82">eha</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_313" n="HIAT:w" s="T83">tetə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_316" n="HIAT:w" s="T84">loɣam</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_319" n="HIAT:w" s="T85">eha</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_323" n="HIAT:w" s="T86">na</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_326" n="HIAT:w" s="T87">loɣam</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_329" n="HIAT:w" s="T88">naj</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_332" n="HIAT:w" s="T89">merəxap</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T116">tɨː</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_339" n="HIAT:w" s="T90">Napas</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_342" n="HIAT:w" s="T91">tögu</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_346" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_348" n="HIAT:w" s="T92">Taper</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_352" n="HIAT:w" s="T93">Napas</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_355" n="HIAT:w" s="T94">töle</ts>
                  <nts id="Seg_356" n="HIAT:ip">,</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_359" n="HIAT:w" s="T95">tau</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_362" n="HIAT:w" s="T96">wargak</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_365" n="HIAT:w" s="T97">tɨnd</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_369" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_371" n="HIAT:w" s="T98">Napasqən</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_374" n="HIAT:w" s="T99">maːt</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_377" n="HIAT:w" s="T100">tawha</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_381" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_383" n="HIAT:w" s="T101">A</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_386" n="HIAT:w" s="T102">warg</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_389" n="HIAT:w" s="T103">nem</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_392" n="HIAT:w" s="T104">ogolalǯha</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_396" n="HIAT:w" s="T105">udʼa</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">qwenba</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_404" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_406" n="HIAT:w" s="T107">Üčega</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_408" n="HIAT:ip">(</nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">nem</ts>
                  <nts id="Seg_411" n="HIAT:ip">)</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_414" n="HIAT:w" s="T109">naj</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">taper</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">ogolalǯemba</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_424" n="HIAT:w" s="T112">ne</ts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_427" n="HIAT:ip">(</nts>
                  <nts id="Seg_428" n="HIAT:ip">(</nts>
                  <ats e="T115" id="Seg_429" n="HIAT:non-pho" s="T113">qwirɣələ</ats>
                  <nts id="Seg_430" n="HIAT:ip">)</nts>
                  <nts id="Seg_431" n="HIAT:ip">)</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_433" n="HIAT:ip">(</nts>
                  <nts id="Seg_434" n="HIAT:ip">(</nts>
                  <ats e="T117" id="Seg_435" n="HIAT:non-pho" s="T115">BRK</ats>
                  <nts id="Seg_436" n="HIAT:ip">)</nts>
                  <nts id="Seg_437" n="HIAT:ip">)</nts>
                  <nts id="Seg_438" n="HIAT:ip">.</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T117" id="Seg_440" n="sc" s="T118">
               <ts e="T0" id="Seg_442" n="e" s="T118">((…)) </ts>
               <ts e="T1" id="Seg_444" n="e" s="T0">Počta </ts>
               <ts e="T2" id="Seg_446" n="e" s="T1">wazimbukuhap </ts>
               <ts e="T3" id="Seg_448" n="e" s="T2">Lɨmbelʼmute </ts>
               <ts e="T4" id="Seg_450" n="e" s="T3">Kananatan. </ts>
               <ts e="T5" id="Seg_452" n="e" s="T4">Nɨlʼǯik </ts>
               <ts e="T6" id="Seg_454" n="e" s="T5">täšekwa. </ts>
               <ts e="T7" id="Seg_456" n="e" s="T6">Aštehe </ts>
               <ts e="T8" id="Seg_458" n="e" s="T7">woobše </ts>
               <ts e="T9" id="Seg_460" n="e" s="T8">man </ts>
               <ts e="T10" id="Seg_462" n="e" s="T9">nadrap, </ts>
               <ts e="T11" id="Seg_464" n="e" s="T10">čʼem </ts>
               <ts e="T12" id="Seg_466" n="e" s="T11">čünduhe. </ts>
               <ts e="T13" id="Seg_468" n="e" s="T12">Man </ts>
               <ts e="T14" id="Seg_470" n="e" s="T13">nɨlʼǯiŋ </ts>
               <ts e="T15" id="Seg_472" n="e" s="T14">ašten </ts>
               <ts e="T16" id="Seg_474" n="e" s="T15">ogolembuhak, </ts>
               <ts e="T17" id="Seg_476" n="e" s="T16">qwajakuhaŋ </ts>
               <ts e="T18" id="Seg_478" n="e" s="T17">aštehe. </ts>
               <ts e="T19" id="Seg_480" n="e" s="T18">Kak </ts>
               <ts e="T20" id="Seg_482" n="e" s="T19">pen </ts>
               <ts e="T21" id="Seg_484" n="e" s="T20">üpoʒəlage, </ts>
               <ts e="T22" id="Seg_486" n="e" s="T21">kaŋ </ts>
               <ts e="T23" id="Seg_488" n="e" s="T22">aštep </ts>
               <ts e="T24" id="Seg_490" n="e" s="T23">šurnirlebe — </ts>
               <ts e="T25" id="Seg_492" n="e" s="T24">ašte </ts>
               <ts e="T26" id="Seg_494" n="e" s="T25">nɨlʼǯik </ts>
               <ts e="T27" id="Seg_496" n="e" s="T26">čʼek </ts>
               <ts e="T28" id="Seg_498" n="e" s="T27">kuralešpa, </ts>
               <ts e="T119" id="Seg_500" n="e" s="T28">(wet) </ts>
               <ts e="T29" id="Seg_502" n="e" s="T119"> ((…)) </ts>
               <ts e="T31" id="Seg_504" n="e" s="T29">kuralešpa! </ts>
               <ts e="T32" id="Seg_506" n="e" s="T31">No, </ts>
               <ts e="T33" id="Seg_508" n="e" s="T32">mat </ts>
               <ts e="T34" id="Seg_510" n="e" s="T33">kušid </ts>
               <ts e="T35" id="Seg_512" n="e" s="T34">pot </ts>
               <ts e="T36" id="Seg_514" n="e" s="T35">kund </ts>
               <ts e="T37" id="Seg_516" n="e" s="T36">xɨr </ts>
               <ts e="T38" id="Seg_518" n="e" s="T37">aštəhə </ts>
               <ts e="T39" id="Seg_520" n="e" s="T38">počta </ts>
               <ts e="T40" id="Seg_522" n="e" s="T39">wazimbham </ts>
               <ts e="T41" id="Seg_524" n="e" s="T40">aštehe </ts>
               <ts e="T42" id="Seg_526" n="e" s="T41">Lɨmbelʼmutte </ts>
               <ts e="T43" id="Seg_528" n="e" s="T42">Kananatʼan. </ts>
               <ts e="T44" id="Seg_530" n="e" s="T43">Našaqotte </ts>
               <ts e="T45" id="Seg_532" n="e" s="T44">nʼejtuha </ts>
               <ts e="T46" id="Seg_534" n="e" s="T45">wattə, </ts>
               <ts e="T47" id="Seg_536" n="e" s="T46">a </ts>
               <ts e="T48" id="Seg_538" n="e" s="T47">tolʼko </ts>
               <ts e="T49" id="Seg_540" n="e" s="T48">aštehe </ts>
               <ts e="T50" id="Seg_542" n="e" s="T49">qwajakkuhut. </ts>
               <ts e="T51" id="Seg_544" n="e" s="T50">I </ts>
               <ts e="T52" id="Seg_546" n="e" s="T51">maǯʼön </ts>
               <ts e="T53" id="Seg_548" n="e" s="T52">aštehe </ts>
               <ts e="T54" id="Seg_550" n="e" s="T53">qwajakkuhak, </ts>
               <ts e="T55" id="Seg_552" n="e" s="T54">i </ts>
               <ts e="T56" id="Seg_554" n="e" s="T55">počtap </ts>
               <ts e="T57" id="Seg_556" n="e" s="T56">taskajembukuhak. </ts>
               <ts e="T58" id="Seg_558" n="e" s="T57">No </ts>
               <ts e="T59" id="Seg_560" n="e" s="T58">taper </ts>
               <ts e="T60" id="Seg_562" n="e" s="T59">aštep, </ts>
               <ts e="T61" id="Seg_564" n="e" s="T60">potom </ts>
               <ts e="T62" id="Seg_566" n="e" s="T61">kalfos </ts>
               <ts e="T63" id="Seg_568" n="e" s="T62">teː </ts>
               <ts e="T64" id="Seg_570" n="e" s="T63">mɨxat, </ts>
               <ts e="T65" id="Seg_572" n="e" s="T64">wesʼ </ts>
               <ts e="T66" id="Seg_574" n="e" s="T65">na </ts>
               <ts e="T67" id="Seg_576" n="e" s="T66">aštep </ts>
               <ts e="T68" id="Seg_578" n="e" s="T67">taːrxat. </ts>
               <ts e="T69" id="Seg_580" n="e" s="T68">Meka </ts>
               <ts e="T70" id="Seg_582" n="e" s="T69">naj </ts>
               <ts e="T71" id="Seg_584" n="e" s="T70">tetə </ts>
               <ts e="T72" id="Seg_586" n="e" s="T71">para </ts>
               <ts e="T73" id="Seg_588" n="e" s="T72">mekuhat, </ts>
               <ts e="T74" id="Seg_590" n="e" s="T73">mat </ts>
               <ts e="T75" id="Seg_592" n="e" s="T74">potom </ts>
               <ts e="T76" id="Seg_594" n="e" s="T75">merəxap. </ts>
               <ts e="T77" id="Seg_596" n="e" s="T76">Nau </ts>
               <ts e="T78" id="Seg_598" n="e" s="T77">tawčin, </ts>
               <ts e="T79" id="Seg_600" n="e" s="T78">Napas, </ts>
               <ts e="T80" id="Seg_602" n="e" s="T79">tögu. </ts>
               <ts e="T81" id="Seg_604" n="e" s="T80">I </ts>
               <ts e="T82" id="Seg_606" n="e" s="T81">loɣam </ts>
               <ts e="T83" id="Seg_608" n="e" s="T82">eha, </ts>
               <ts e="T84" id="Seg_610" n="e" s="T83">tetə </ts>
               <ts e="T85" id="Seg_612" n="e" s="T84">loɣam </ts>
               <ts e="T86" id="Seg_614" n="e" s="T85">eha, </ts>
               <ts e="T87" id="Seg_616" n="e" s="T86">na </ts>
               <ts e="T88" id="Seg_618" n="e" s="T87">loɣam </ts>
               <ts e="T89" id="Seg_620" n="e" s="T88">naj </ts>
               <ts e="T116" id="Seg_622" n="e" s="T89">merəxap, </ts>
               <ts e="T90" id="Seg_624" n="e" s="T116">tɨː </ts>
               <ts e="T91" id="Seg_626" n="e" s="T90">Napas </ts>
               <ts e="T92" id="Seg_628" n="e" s="T91">tögu. </ts>
               <ts e="T93" id="Seg_630" n="e" s="T92">Taper, </ts>
               <ts e="T94" id="Seg_632" n="e" s="T93">Napas </ts>
               <ts e="T95" id="Seg_634" n="e" s="T94">töle, </ts>
               <ts e="T96" id="Seg_636" n="e" s="T95">tau </ts>
               <ts e="T97" id="Seg_638" n="e" s="T96">wargak </ts>
               <ts e="T98" id="Seg_640" n="e" s="T97">tɨnd. </ts>
               <ts e="T99" id="Seg_642" n="e" s="T98">Napasqən </ts>
               <ts e="T100" id="Seg_644" n="e" s="T99">maːt </ts>
               <ts e="T101" id="Seg_646" n="e" s="T100">tawha. </ts>
               <ts e="T102" id="Seg_648" n="e" s="T101">A </ts>
               <ts e="T103" id="Seg_650" n="e" s="T102">warg </ts>
               <ts e="T104" id="Seg_652" n="e" s="T103">nem </ts>
               <ts e="T105" id="Seg_654" n="e" s="T104">ogolalǯha, </ts>
               <ts e="T106" id="Seg_656" n="e" s="T105">udʼa, </ts>
               <ts e="T107" id="Seg_658" n="e" s="T106">qwenba. </ts>
               <ts e="T108" id="Seg_660" n="e" s="T107">Üčega </ts>
               <ts e="T109" id="Seg_662" n="e" s="T108">(nem) </ts>
               <ts e="T110" id="Seg_664" n="e" s="T109">naj </ts>
               <ts e="T111" id="Seg_666" n="e" s="T110">taper </ts>
               <ts e="T112" id="Seg_668" n="e" s="T111">ogolalǯemba, </ts>
               <ts e="T113" id="Seg_670" n="e" s="T112">ne, </ts>
               <ts e="T115" id="Seg_672" n="e" s="T113">((qwirɣələ)) </ts>
               <ts e="T117" id="Seg_674" n="e" s="T115">((BRK)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_675" s="T118">YIF_196X_WorkAsPostwoman_nar.001 (001)</ta>
            <ta e="T6" id="Seg_676" s="T4">YIF_196X_WorkAsPostwoman_nar.002 (002)</ta>
            <ta e="T12" id="Seg_677" s="T6">YIF_196X_WorkAsPostwoman_nar.003 (003)</ta>
            <ta e="T18" id="Seg_678" s="T12">YIF_196X_WorkAsPostwoman_nar.004 (004)</ta>
            <ta e="T31" id="Seg_679" s="T18">YIF_196X_WorkAsPostwoman_nar.005 (005)</ta>
            <ta e="T43" id="Seg_680" s="T31">YIF_196X_WorkAsPostwoman_nar.006 (006)</ta>
            <ta e="T50" id="Seg_681" s="T43">YIF_196X_WorkAsPostwoman_nar.007 (007)</ta>
            <ta e="T57" id="Seg_682" s="T50">YIF_196X_WorkAsPostwoman_nar.008 (008)</ta>
            <ta e="T68" id="Seg_683" s="T57">YIF_196X_WorkAsPostwoman_nar.009 (009)</ta>
            <ta e="T76" id="Seg_684" s="T68">YIF_196X_WorkAsPostwoman_nar.010 (010)</ta>
            <ta e="T80" id="Seg_685" s="T76">YIF_196X_WorkAsPostwoman_nar.011 (011)</ta>
            <ta e="T92" id="Seg_686" s="T80">YIF_196X_WorkAsPostwoman_nar.012 (012)</ta>
            <ta e="T98" id="Seg_687" s="T92">YIF_196X_WorkAsPostwoman_nar.013 (013)</ta>
            <ta e="T101" id="Seg_688" s="T98">YIF_196X_WorkAsPostwoman_nar.014 (014)</ta>
            <ta e="T107" id="Seg_689" s="T101">YIF_196X_WorkAsPostwoman_nar.015 (015)</ta>
            <ta e="T117" id="Seg_690" s="T107">YIF_196X_WorkAsPostwoman_nar.016 (016)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_691" s="T118">Почтап вазимбухак Лымбельмутэ Кананакан.</ta>
            <ta e="T6" id="Seg_692" s="T4">Ныльджик тӓшэква.</ta>
            <ta e="T12" id="Seg_693" s="T6">Аштэҳе вообще мат надрап, чем чундэҳе.</ta>
            <ta e="T18" id="Seg_694" s="T12">Ман ныльджик аштэн оголэмбыҳак, кваякэҳу аштэхе.</ta>
            <ta e="T31" id="Seg_695" s="T18">Как пен ӱподжьлаге, как аштэп шурниылебе — аштэ ныльджик чек куралешпа!</ta>
            <ta e="T43" id="Seg_696" s="T31">Но, мат кушид поткунд почтап вазимбэҳам аштэҳе Лымбельмуттэ Кананакан.</ta>
            <ta e="T50" id="Seg_697" s="T43">Нашаӄоуттэ нетуа ватт, а только аштэҳе ӄваяӄӄуҳут.</ta>
            <ta e="T57" id="Seg_698" s="T50">И маджӧн аштэҳе кваяккуҳак, и почтап таскаембыкухак.</ta>
            <ta e="T68" id="Seg_699" s="T57">Но тепер аштэп, потом калхост тэ мыҳат, весь на аштэп тархат.</ta>
            <ta e="T76" id="Seg_700" s="T68">Мека най тэт пара мекуҳат, мат потом мерэхап.</ta>
            <ta e="T80" id="Seg_701" s="T76">Нау тавчин, Напас, тӧгу.</ta>
            <ta e="T92" id="Seg_702" s="T80">И логам эҳа, тэт лога эҳа, най мерэҳап, Напас тӧгу.</ta>
            <ta e="T98" id="Seg_703" s="T92">Тапер, Напас тӧле, тынд варгак.</ta>
            <ta e="T101" id="Seg_704" s="T98">Напаскэн ма̄т тавҳап.</ta>
            <ta e="T107" id="Seg_705" s="T101">А варг нэм оголалджэкуҳа, уджӓ, квэнба.</ta>
            <ta e="T117" id="Seg_706" s="T107">Ӱҷэга нэм най тапер оголалджэмба, нэ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_707" s="T118">Počtap wazimbuxak Lɨmbelʼmute Kananakan.</ta>
            <ta e="T6" id="Seg_708" s="T4">Nɨlʼǯik täšekwa.</ta>
            <ta e="T12" id="Seg_709" s="T6">Aštexe woobše mat nadrap, čʼem čundexe.</ta>
            <ta e="T18" id="Seg_710" s="T12">Man nɨlʼǯik ašten ogolembɨxak, kwajakexul aštexe.</ta>
            <ta e="T31" id="Seg_711" s="T18">Kak pen üpoǯʼlage, kak aštep šurniɨlebe — ašte nɨlʼǯik čʼek kuralešpa!</ta>
            <ta e="T43" id="Seg_712" s="T31">No, mat kušid potkund počʼtap wazimbexam aštexe Lɨmbelʼmutte Kananakan.</ta>
            <ta e="T50" id="Seg_713" s="T43">Našaqoutte netua watt, a tolʼko aštexe qwajaqquxut.</ta>
            <ta e="T57" id="Seg_714" s="T50">I maǯön aštexe kwajakkuxak, i počʼtap taskaembɨkuxak.</ta>
            <ta e="T68" id="Seg_715" s="T57">No teper aštep, potom kalxost te mɨxat, wesʼ na aštep tarxat.</ta>
            <ta e="T76" id="Seg_716" s="T68">Meka naj tet para mekuxat, mat potom merexap.</ta>
            <ta e="T80" id="Seg_717" s="T76">Nau tawčʼin, Napas, tögu.</ta>
            <ta e="T92" id="Seg_718" s="T80">I logam exa, tet loga exa, naj merexap, Napas tögu.</ta>
            <ta e="T98" id="Seg_719" s="T92">Taper, Napas töle, tɨnd wargak.</ta>
            <ta e="T101" id="Seg_720" s="T98">Napasken maːt tawxap.</ta>
            <ta e="T107" id="Seg_721" s="T101">A warg nem ogolalǯekuxa, uǯä, kwenba.</ta>
            <ta e="T117" id="Seg_722" s="T107">Üčega nem naj taper ogolalǯemba, ne.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_723" s="T118">((…)) Počta wazimbukuhap Lɨmbelʼmute Kananatan. </ta>
            <ta e="T6" id="Seg_724" s="T4">Nɨlʼǯik täšekwa. </ta>
            <ta e="T12" id="Seg_725" s="T6">Aštehe woobše man nadrap, čʼem čünduhe. </ta>
            <ta e="T18" id="Seg_726" s="T12">Man nɨlʼǯiŋ ašten ogolembuhak, qwajakuhaŋ aštehe. </ta>
            <ta e="T31" id="Seg_727" s="T18">Kak pen üpoʒəlage, kaŋ aštep šurnirlebe — ašte nɨlʼǯik čʼek kuralešpa, (wet) ((…)) kuralešpa! </ta>
            <ta e="T43" id="Seg_728" s="T31">No, mat kušid pot kund xɨr aštəhə počta wazimbham aštehe Lɨmbelʼmutte Kananatʼan. </ta>
            <ta e="T50" id="Seg_729" s="T43">Našaqotte nʼejtuha wattə, a tolʼko aštehe qwajakkuhut. </ta>
            <ta e="T57" id="Seg_730" s="T50">I maǯʼön aštehe qwajakkuhak, i počtap taskajembukuhak. </ta>
            <ta e="T68" id="Seg_731" s="T57">No taper aštep, potom kalfos teː mɨxat, wesʼ na aštep taːrxat. </ta>
            <ta e="T76" id="Seg_732" s="T68">Meka naj tetə para mekuhat, mat potom merəxap. </ta>
            <ta e="T80" id="Seg_733" s="T76">Nau tawčin, Napas, tögu. </ta>
            <ta e="T92" id="Seg_734" s="T80">I loɣam eha, tetə loɣam eha, na loɣam naj merəxap, tɨː Napas tögu. </ta>
            <ta e="T98" id="Seg_735" s="T92">Taper, Napas töle, tau wargak tɨnd. </ta>
            <ta e="T101" id="Seg_736" s="T98">Napasqən maːt tawha. </ta>
            <ta e="T107" id="Seg_737" s="T101">A warg nem ogolalǯha, udʼa, qwenba. </ta>
            <ta e="T117" id="Seg_738" s="T107">Üčega (nem) naj taper ogolalǯemba, ne, ((qwirɣələ…)) ((BRK)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_739" s="T0">počta</ta>
            <ta e="T2" id="Seg_740" s="T1">wazi-mbu-ku-ha-p</ta>
            <ta e="T3" id="Seg_741" s="T2">Lɨmbelʼ-mute</ta>
            <ta e="T4" id="Seg_742" s="T3">Kananat-a-n</ta>
            <ta e="T5" id="Seg_743" s="T4">nɨlʼǯi-k</ta>
            <ta e="T6" id="Seg_744" s="T5">täše-k-wa</ta>
            <ta e="T7" id="Seg_745" s="T6">ašte-he</ta>
            <ta e="T8" id="Seg_746" s="T7">woobše</ta>
            <ta e="T9" id="Seg_747" s="T8">man</ta>
            <ta e="T10" id="Seg_748" s="T9">nadr-a-p</ta>
            <ta e="T11" id="Seg_749" s="T10">čʼem</ta>
            <ta e="T12" id="Seg_750" s="T11">čünd-u-he</ta>
            <ta e="T13" id="Seg_751" s="T12">man</ta>
            <ta e="T14" id="Seg_752" s="T13">nɨlʼǯi-ŋ</ta>
            <ta e="T15" id="Seg_753" s="T14">ašte-n</ta>
            <ta e="T16" id="Seg_754" s="T15">ogole-mbu-ha-k</ta>
            <ta e="T17" id="Seg_755" s="T16">qwaja-ku-ha-ŋ</ta>
            <ta e="T18" id="Seg_756" s="T17">ašte-he</ta>
            <ta e="T19" id="Seg_757" s="T18">kak</ta>
            <ta e="T20" id="Seg_758" s="T19">pet-n</ta>
            <ta e="T21" id="Seg_759" s="T20">üpoʒə-la-ge</ta>
            <ta e="T22" id="Seg_760" s="T21">kaŋ</ta>
            <ta e="T23" id="Seg_761" s="T22">ašte-p</ta>
            <ta e="T24" id="Seg_762" s="T23">šurni-r-lebe</ta>
            <ta e="T25" id="Seg_763" s="T24">ašte</ta>
            <ta e="T26" id="Seg_764" s="T25">nɨlʼǯi-k</ta>
            <ta e="T27" id="Seg_765" s="T26">čʼek</ta>
            <ta e="T28" id="Seg_766" s="T27">kur-a-le-špa</ta>
            <ta e="T31" id="Seg_767" s="T29">kur-a-le-špa</ta>
            <ta e="T32" id="Seg_768" s="T31">no</ta>
            <ta e="T33" id="Seg_769" s="T32">mat</ta>
            <ta e="T34" id="Seg_770" s="T33">kušid</ta>
            <ta e="T35" id="Seg_771" s="T34">po-t</ta>
            <ta e="T36" id="Seg_772" s="T35">kund</ta>
            <ta e="T37" id="Seg_773" s="T36">xɨr</ta>
            <ta e="T38" id="Seg_774" s="T37">aštə-hə</ta>
            <ta e="T39" id="Seg_775" s="T38">počta</ta>
            <ta e="T40" id="Seg_776" s="T39">wazi-mb-ha-m</ta>
            <ta e="T41" id="Seg_777" s="T40">ašte-he</ta>
            <ta e="T42" id="Seg_778" s="T41">Lɨmbelʼ-mutte</ta>
            <ta e="T43" id="Seg_779" s="T42">Kananatʼ-a-n</ta>
            <ta e="T44" id="Seg_780" s="T43">našaqotte</ta>
            <ta e="T45" id="Seg_781" s="T44">nʼejtu-ha</ta>
            <ta e="T46" id="Seg_782" s="T45">wattə</ta>
            <ta e="T47" id="Seg_783" s="T46">a</ta>
            <ta e="T48" id="Seg_784" s="T47">tolʼko</ta>
            <ta e="T49" id="Seg_785" s="T48">ašte-he</ta>
            <ta e="T50" id="Seg_786" s="T49">qwaja-kku-hu-t</ta>
            <ta e="T51" id="Seg_787" s="T50">i</ta>
            <ta e="T52" id="Seg_788" s="T51">maǯʼö-n</ta>
            <ta e="T53" id="Seg_789" s="T52">ašte-he</ta>
            <ta e="T54" id="Seg_790" s="T53">qwaja-kku-ha-k</ta>
            <ta e="T55" id="Seg_791" s="T54">i</ta>
            <ta e="T56" id="Seg_792" s="T55">počta-p</ta>
            <ta e="T57" id="Seg_793" s="T56">taskaj-e-mbu-ku-ha-k</ta>
            <ta e="T58" id="Seg_794" s="T57">no</ta>
            <ta e="T59" id="Seg_795" s="T58">taper</ta>
            <ta e="T60" id="Seg_796" s="T59">ašte-p</ta>
            <ta e="T61" id="Seg_797" s="T60">potom</ta>
            <ta e="T62" id="Seg_798" s="T61">kalfos</ta>
            <ta e="T63" id="Seg_799" s="T62">teː</ta>
            <ta e="T64" id="Seg_800" s="T63">mɨ-xa-t</ta>
            <ta e="T65" id="Seg_801" s="T64">wesʼ</ta>
            <ta e="T66" id="Seg_802" s="T65">na</ta>
            <ta e="T67" id="Seg_803" s="T66">ašte-p</ta>
            <ta e="T68" id="Seg_804" s="T67">taːr-xa-t</ta>
            <ta e="T69" id="Seg_805" s="T68">meka</ta>
            <ta e="T70" id="Seg_806" s="T69">naj</ta>
            <ta e="T71" id="Seg_807" s="T70">tetə</ta>
            <ta e="T72" id="Seg_808" s="T71">para</ta>
            <ta e="T73" id="Seg_809" s="T72">me-ku-ha-t</ta>
            <ta e="T74" id="Seg_810" s="T73">mat</ta>
            <ta e="T75" id="Seg_811" s="T74">potom</ta>
            <ta e="T76" id="Seg_812" s="T75">mer-ə-xa-p</ta>
            <ta e="T77" id="Seg_813" s="T76">nau</ta>
            <ta e="T78" id="Seg_814" s="T77">taw-čin</ta>
            <ta e="T79" id="Seg_815" s="T78">Napas</ta>
            <ta e="T80" id="Seg_816" s="T79">tö-gu</ta>
            <ta e="T81" id="Seg_817" s="T80">i</ta>
            <ta e="T82" id="Seg_818" s="T81">loɣa-m</ta>
            <ta e="T83" id="Seg_819" s="T82">e-ha</ta>
            <ta e="T84" id="Seg_820" s="T83">tetə</ta>
            <ta e="T85" id="Seg_821" s="T84">loɣa-m</ta>
            <ta e="T86" id="Seg_822" s="T85">e-ha</ta>
            <ta e="T87" id="Seg_823" s="T86">na</ta>
            <ta e="T88" id="Seg_824" s="T87">loɣa-m</ta>
            <ta e="T89" id="Seg_825" s="T88">naj</ta>
            <ta e="T116" id="Seg_826" s="T89">mer-ə-xa-p</ta>
            <ta e="T90" id="Seg_827" s="T116">tɨː</ta>
            <ta e="T91" id="Seg_828" s="T90">Napas</ta>
            <ta e="T92" id="Seg_829" s="T91">tö-gu</ta>
            <ta e="T93" id="Seg_830" s="T92">taper</ta>
            <ta e="T94" id="Seg_831" s="T93">Napas</ta>
            <ta e="T95" id="Seg_832" s="T94">tö-le</ta>
            <ta e="T96" id="Seg_833" s="T95">tau</ta>
            <ta e="T97" id="Seg_834" s="T96">warga-k</ta>
            <ta e="T98" id="Seg_835" s="T97">tɨnd</ta>
            <ta e="T99" id="Seg_836" s="T98">Napas-qən</ta>
            <ta e="T100" id="Seg_837" s="T99">maːt</ta>
            <ta e="T101" id="Seg_838" s="T100">taw-ha</ta>
            <ta e="T102" id="Seg_839" s="T101">a</ta>
            <ta e="T103" id="Seg_840" s="T102">warg</ta>
            <ta e="T104" id="Seg_841" s="T103">ne-m</ta>
            <ta e="T105" id="Seg_842" s="T104">ogol-a-lǯ-ha</ta>
            <ta e="T106" id="Seg_843" s="T105">udʼa</ta>
            <ta e="T107" id="Seg_844" s="T106">qwen-ba</ta>
            <ta e="T108" id="Seg_845" s="T107">üčega</ta>
            <ta e="T109" id="Seg_846" s="T108">ne-m</ta>
            <ta e="T110" id="Seg_847" s="T109">naj</ta>
            <ta e="T111" id="Seg_848" s="T110">taper</ta>
            <ta e="T112" id="Seg_849" s="T111">ogol-a-lǯe-mba</ta>
            <ta e="T113" id="Seg_850" s="T112">ne</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_851" s="T0">počta</ta>
            <ta e="T2" id="Seg_852" s="T1">wazi-mbɨ-ku-sɨ-m</ta>
            <ta e="T3" id="Seg_853" s="T2">Lɨmbelʼ-ute</ta>
            <ta e="T4" id="Seg_854" s="T3">Kananat-ɨ-n</ta>
            <ta e="T5" id="Seg_855" s="T4">nɨlʼǯi-k</ta>
            <ta e="T6" id="Seg_856" s="T5">taše-ku-wa</ta>
            <ta e="T7" id="Seg_857" s="T6">aste-se</ta>
            <ta e="T8" id="Seg_858" s="T7">woobše</ta>
            <ta e="T9" id="Seg_859" s="T8">man</ta>
            <ta e="T10" id="Seg_860" s="T9">nadɨr-ɨ-m</ta>
            <ta e="T11" id="Seg_861" s="T10">čʼeːm</ta>
            <ta e="T12" id="Seg_862" s="T11">čünd-ɨ-se</ta>
            <ta e="T13" id="Seg_863" s="T12">man</ta>
            <ta e="T14" id="Seg_864" s="T13">nɨlʼǯi-k</ta>
            <ta e="T15" id="Seg_865" s="T14">aste-n</ta>
            <ta e="T16" id="Seg_866" s="T15">ogole-mbɨ-sɨ-k</ta>
            <ta e="T17" id="Seg_867" s="T16">qwaja-ku-sɨ-k</ta>
            <ta e="T18" id="Seg_868" s="T17">aste-se</ta>
            <ta e="T19" id="Seg_869" s="T18">kak</ta>
            <ta e="T20" id="Seg_870" s="T19">pet-n</ta>
            <ta e="T21" id="Seg_871" s="T20">üːbɨǯe-la-k</ta>
            <ta e="T22" id="Seg_872" s="T21">kak</ta>
            <ta e="T23" id="Seg_873" s="T22">aste-p</ta>
            <ta e="T24" id="Seg_874" s="T23">šurni-r-lewle</ta>
            <ta e="T25" id="Seg_875" s="T24">aste</ta>
            <ta e="T26" id="Seg_876" s="T25">nɨlʼǯi-k</ta>
            <ta e="T27" id="Seg_877" s="T26">ček</ta>
            <ta e="T28" id="Seg_878" s="T27">kur-ɨ-lɨ-špɨ</ta>
            <ta e="T31" id="Seg_879" s="T29">kur-ɨ-lɨ-špɨ</ta>
            <ta e="T32" id="Seg_880" s="T31">nu</ta>
            <ta e="T33" id="Seg_881" s="T32">man</ta>
            <ta e="T34" id="Seg_882" s="T33">kušan</ta>
            <ta e="T35" id="Seg_883" s="T34">po-n</ta>
            <ta e="T36" id="Seg_884" s="T35">kundɨ</ta>
            <ta e="T37" id="Seg_885" s="T36">sɨr</ta>
            <ta e="T38" id="Seg_886" s="T37">aste-se</ta>
            <ta e="T39" id="Seg_887" s="T38">počta</ta>
            <ta e="T40" id="Seg_888" s="T39">wazi-mbɨ-sɨ-m</ta>
            <ta e="T41" id="Seg_889" s="T40">aste-se</ta>
            <ta e="T42" id="Seg_890" s="T41">Lɨmbelʼ-ute</ta>
            <ta e="T43" id="Seg_891" s="T42">Kananat-ɨ-n</ta>
            <ta e="T44" id="Seg_892" s="T43">našaqqet</ta>
            <ta e="T45" id="Seg_893" s="T44">nʼetu-sɨ</ta>
            <ta e="T46" id="Seg_894" s="T45">watt</ta>
            <ta e="T47" id="Seg_895" s="T46">a</ta>
            <ta e="T48" id="Seg_896" s="T47">tolʼko</ta>
            <ta e="T49" id="Seg_897" s="T48">aste-se</ta>
            <ta e="T50" id="Seg_898" s="T49">qwaja-ku-sɨ-dət</ta>
            <ta e="T51" id="Seg_899" s="T50">i</ta>
            <ta e="T52" id="Seg_900" s="T51">maǯʼo-n</ta>
            <ta e="T53" id="Seg_901" s="T52">aste-se</ta>
            <ta e="T54" id="Seg_902" s="T53">qwaja-ku-sɨ-k</ta>
            <ta e="T55" id="Seg_903" s="T54">i</ta>
            <ta e="T56" id="Seg_904" s="T55">počta-p</ta>
            <ta e="T57" id="Seg_905" s="T56">taskaj-ɨ-mbɨ-ku-sɨ-k</ta>
            <ta e="T58" id="Seg_906" s="T57">nu</ta>
            <ta e="T59" id="Seg_907" s="T58">taper</ta>
            <ta e="T60" id="Seg_908" s="T59">aste-p</ta>
            <ta e="T61" id="Seg_909" s="T60">patom</ta>
            <ta e="T62" id="Seg_910" s="T61">kalxos</ta>
            <ta e="T63" id="Seg_911" s="T62">teː</ta>
            <ta e="T64" id="Seg_912" s="T63">me-sɨ-dət</ta>
            <ta e="T65" id="Seg_913" s="T64">wesʼ</ta>
            <ta e="T66" id="Seg_914" s="T65">na</ta>
            <ta e="T67" id="Seg_915" s="T66">aste-p</ta>
            <ta e="T68" id="Seg_916" s="T67">tar-sɨ-dət</ta>
            <ta e="T69" id="Seg_917" s="T68">mäkkä</ta>
            <ta e="T70" id="Seg_918" s="T69">naj</ta>
            <ta e="T71" id="Seg_919" s="T70">tettɨ</ta>
            <ta e="T72" id="Seg_920" s="T71">para</ta>
            <ta e="T73" id="Seg_921" s="T72">me-ku-sɨ-dət</ta>
            <ta e="T74" id="Seg_922" s="T73">man</ta>
            <ta e="T75" id="Seg_923" s="T74">patom</ta>
            <ta e="T76" id="Seg_924" s="T75">mer-ɨ-sɨ-m</ta>
            <ta e="T77" id="Seg_925" s="T76">na</ta>
            <ta e="T78" id="Seg_926" s="T77">taw-%%</ta>
            <ta e="T79" id="Seg_927" s="T78">Napas</ta>
            <ta e="T80" id="Seg_928" s="T79">töː-gu</ta>
            <ta e="T81" id="Seg_929" s="T80">i</ta>
            <ta e="T82" id="Seg_930" s="T81">loga-mɨ</ta>
            <ta e="T83" id="Seg_931" s="T82">e-sɨ</ta>
            <ta e="T84" id="Seg_932" s="T83">tettɨ</ta>
            <ta e="T85" id="Seg_933" s="T84">loga-mɨ</ta>
            <ta e="T86" id="Seg_934" s="T85">e-sɨ</ta>
            <ta e="T87" id="Seg_935" s="T86">na</ta>
            <ta e="T88" id="Seg_936" s="T87">loga-mɨ</ta>
            <ta e="T89" id="Seg_937" s="T88">naj</ta>
            <ta e="T116" id="Seg_938" s="T89">mer-ɨ-sɨ-m</ta>
            <ta e="T90" id="Seg_939" s="T116">tɨː</ta>
            <ta e="T91" id="Seg_940" s="T90">Napas</ta>
            <ta e="T92" id="Seg_941" s="T91">töː-gu</ta>
            <ta e="T93" id="Seg_942" s="T92">taper</ta>
            <ta e="T94" id="Seg_943" s="T93">Napas</ta>
            <ta e="T95" id="Seg_944" s="T94">töː-le</ta>
            <ta e="T96" id="Seg_945" s="T95">taw</ta>
            <ta e="T97" id="Seg_946" s="T96">wargɨ-k</ta>
            <ta e="T98" id="Seg_947" s="T97">tɨnd</ta>
            <ta e="T99" id="Seg_948" s="T98">Napas-qɨn</ta>
            <ta e="T100" id="Seg_949" s="T99">maːt</ta>
            <ta e="T101" id="Seg_950" s="T100">taw-sɨ</ta>
            <ta e="T102" id="Seg_951" s="T101">a</ta>
            <ta e="T103" id="Seg_952" s="T102">wargɨ</ta>
            <ta e="T104" id="Seg_953" s="T103">neː-mɨ</ta>
            <ta e="T105" id="Seg_954" s="T104">oqol-ɨ-lʼčǝ-sɨ</ta>
            <ta e="T106" id="Seg_955" s="T105">üdʼa</ta>
            <ta e="T107" id="Seg_956" s="T106">qwän-mbɨ</ta>
            <ta e="T108" id="Seg_957" s="T107">üčega</ta>
            <ta e="T109" id="Seg_958" s="T108">neː-mɨ</ta>
            <ta e="T110" id="Seg_959" s="T109">naj</ta>
            <ta e="T111" id="Seg_960" s="T110">taper</ta>
            <ta e="T112" id="Seg_961" s="T111">oqol-ɨ-lǯe-mbɨ</ta>
            <ta e="T113" id="Seg_962" s="T112">neː</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_963" s="T0">mail.[NOM]</ta>
            <ta e="T2" id="Seg_964" s="T1">carry-DUR-HAB-PST-1SG.O</ta>
            <ta e="T3" id="Seg_965" s="T2">Lymbel_Karamo-ABL2</ta>
            <ta e="T4" id="Seg_966" s="T3">Kananak-EP-ILL2</ta>
            <ta e="T5" id="Seg_967" s="T4">such-ADVZ</ta>
            <ta e="T6" id="Seg_968" s="T5">be.cold-HAB-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_969" s="T6">reindeer-INSTR</ta>
            <ta e="T8" id="Seg_970" s="T7">at.all</ta>
            <ta e="T9" id="Seg_971" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_972" s="T9">love-EP-1SG.O</ta>
            <ta e="T11" id="Seg_973" s="T10">than</ta>
            <ta e="T12" id="Seg_974" s="T11">horse-EP-INSTR</ta>
            <ta e="T13" id="Seg_975" s="T12">I.NOM</ta>
            <ta e="T14" id="Seg_976" s="T13">such-ADVZ</ta>
            <ta e="T15" id="Seg_977" s="T14">reindeer-INSTR2</ta>
            <ta e="T16" id="Seg_978" s="T15">get.used.to-DUR-PST-1SG.S</ta>
            <ta e="T17" id="Seg_979" s="T16">go-HAB-PST-1SG.S</ta>
            <ta e="T18" id="Seg_980" s="T17">reindeer-INSTR</ta>
            <ta e="T19" id="Seg_981" s="T18">how</ta>
            <ta e="T20" id="Seg_982" s="T19">night-ADV.LOC</ta>
            <ta e="T21" id="Seg_983" s="T20">depart-FUT-1SG.S</ta>
            <ta e="T22" id="Seg_984" s="T21">how</ta>
            <ta e="T23" id="Seg_985" s="T22">reindeer-ACC</ta>
            <ta e="T24" id="Seg_986" s="T23">%urge-FRQ-CVB2</ta>
            <ta e="T25" id="Seg_987" s="T24">reindeer.[NOM]</ta>
            <ta e="T26" id="Seg_988" s="T25">such-ADVZ</ta>
            <ta e="T27" id="Seg_989" s="T26">fast</ta>
            <ta e="T28" id="Seg_990" s="T27">run-EP-RES-IPFV2.[3SG.S]</ta>
            <ta e="T31" id="Seg_991" s="T29">run-EP-RES-IPFV2.[3SG.S]</ta>
            <ta e="T32" id="Seg_992" s="T31">well</ta>
            <ta e="T33" id="Seg_993" s="T32">I.NOM</ta>
            <ta e="T34" id="Seg_994" s="T33">how.many</ta>
            <ta e="T35" id="Seg_995" s="T34">year-GEN</ta>
            <ta e="T36" id="Seg_996" s="T35">during</ta>
            <ta e="T37" id="Seg_997" s="T36">cow.[NOM]</ta>
            <ta e="T38" id="Seg_998" s="T37">reindeer-INSTR</ta>
            <ta e="T39" id="Seg_999" s="T38">mail.[NOM]</ta>
            <ta e="T40" id="Seg_1000" s="T39">carry-DUR-PST-1SG.O</ta>
            <ta e="T41" id="Seg_1001" s="T40">reindeer-INSTR</ta>
            <ta e="T42" id="Seg_1002" s="T41">Lymbel_Karamo-ABL2</ta>
            <ta e="T43" id="Seg_1003" s="T42">Kananak-EP-ILL2</ta>
            <ta e="T44" id="Seg_1004" s="T43">then</ta>
            <ta e="T45" id="Seg_1005" s="T44">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_1006" s="T45">road.[NOM]</ta>
            <ta e="T47" id="Seg_1007" s="T46">but</ta>
            <ta e="T48" id="Seg_1008" s="T47">only</ta>
            <ta e="T49" id="Seg_1009" s="T48">reindeer-INSTR</ta>
            <ta e="T50" id="Seg_1010" s="T49">go-HAB-PST-3PL</ta>
            <ta e="T51" id="Seg_1011" s="T50">and</ta>
            <ta e="T52" id="Seg_1012" s="T51">taiga-ILL2</ta>
            <ta e="T53" id="Seg_1013" s="T52">reindeer-INSTR</ta>
            <ta e="T54" id="Seg_1014" s="T53">go-HAB-PST-1SG.S</ta>
            <ta e="T55" id="Seg_1015" s="T54">and</ta>
            <ta e="T56" id="Seg_1016" s="T55">mail-ACC</ta>
            <ta e="T57" id="Seg_1017" s="T56">drag-EP-DUR-HAB-PST-1SG.S</ta>
            <ta e="T58" id="Seg_1018" s="T57">well</ta>
            <ta e="T59" id="Seg_1019" s="T58">now</ta>
            <ta e="T60" id="Seg_1020" s="T59">reindeer-ACC</ta>
            <ta e="T61" id="Seg_1021" s="T60">then</ta>
            <ta e="T62" id="Seg_1022" s="T61">kolkhoz.[NOM]</ta>
            <ta e="T63" id="Seg_1023" s="T62">away</ta>
            <ta e="T64" id="Seg_1024" s="T63">do-PST-3PL</ta>
            <ta e="T65" id="Seg_1025" s="T64">all</ta>
            <ta e="T66" id="Seg_1026" s="T65">this</ta>
            <ta e="T67" id="Seg_1027" s="T66">reindeer-ACC</ta>
            <ta e="T68" id="Seg_1028" s="T67">split-PST-3PL</ta>
            <ta e="T69" id="Seg_1029" s="T68">I.ALL</ta>
            <ta e="T70" id="Seg_1030" s="T69">also</ta>
            <ta e="T71" id="Seg_1031" s="T70">four</ta>
            <ta e="T72" id="Seg_1032" s="T71">pair</ta>
            <ta e="T73" id="Seg_1033" s="T72">give-HAB-PST-3PL</ta>
            <ta e="T74" id="Seg_1034" s="T73">I.NOM</ta>
            <ta e="T75" id="Seg_1035" s="T74">then</ta>
            <ta e="T76" id="Seg_1036" s="T75">sell-EP-PST-1SG.O</ta>
            <ta e="T77" id="Seg_1037" s="T76">this</ta>
            <ta e="T78" id="Seg_1038" s="T77">there-%%</ta>
            <ta e="T79" id="Seg_1039" s="T78">Napas.[NOM]</ta>
            <ta e="T80" id="Seg_1040" s="T79">come-INF</ta>
            <ta e="T81" id="Seg_1041" s="T80">and</ta>
            <ta e="T82" id="Seg_1042" s="T81">fox-1SG</ta>
            <ta e="T83" id="Seg_1043" s="T82">be-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_1044" s="T83">four</ta>
            <ta e="T85" id="Seg_1045" s="T84">fox-1SG</ta>
            <ta e="T86" id="Seg_1046" s="T85">be-PST.[3SG.S]</ta>
            <ta e="T87" id="Seg_1047" s="T86">this</ta>
            <ta e="T88" id="Seg_1048" s="T87">fox-1SG</ta>
            <ta e="T89" id="Seg_1049" s="T88">also</ta>
            <ta e="T116" id="Seg_1050" s="T89">sell-EP-PST-1SG.O</ta>
            <ta e="T90" id="Seg_1051" s="T116">here</ta>
            <ta e="T91" id="Seg_1052" s="T90">Napas.[NOM]</ta>
            <ta e="T92" id="Seg_1053" s="T91">come-INF</ta>
            <ta e="T93" id="Seg_1054" s="T92">now</ta>
            <ta e="T94" id="Seg_1055" s="T93">Napas.[NOM]</ta>
            <ta e="T95" id="Seg_1056" s="T94">come-CVB</ta>
            <ta e="T96" id="Seg_1057" s="T95">this</ta>
            <ta e="T97" id="Seg_1058" s="T96">live-1SG.S</ta>
            <ta e="T98" id="Seg_1059" s="T97">here</ta>
            <ta e="T99" id="Seg_1060" s="T98">Napas-LOC</ta>
            <ta e="T100" id="Seg_1061" s="T99">house.[NOM]</ta>
            <ta e="T101" id="Seg_1062" s="T100">buy-PST</ta>
            <ta e="T102" id="Seg_1063" s="T101">but</ta>
            <ta e="T103" id="Seg_1064" s="T102">big</ta>
            <ta e="T104" id="Seg_1065" s="T103">daughter-1SG</ta>
            <ta e="T105" id="Seg_1066" s="T104">learn-EP-PFV-PST.[3SG.S]</ta>
            <ta e="T106" id="Seg_1067" s="T105">work.[3SG.S]</ta>
            <ta e="T107" id="Seg_1068" s="T106">go.away-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1069" s="T107">small</ta>
            <ta e="T109" id="Seg_1070" s="T108">daughter-1SG</ta>
            <ta e="T110" id="Seg_1071" s="T109">also</ta>
            <ta e="T111" id="Seg_1072" s="T110">now</ta>
            <ta e="T112" id="Seg_1073" s="T111">learn-EP-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T113" id="Seg_1074" s="T112">daughter.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1075" s="T0">почта.[NOM]</ta>
            <ta e="T2" id="Seg_1076" s="T1">возить-DUR-HAB-PST-1SG.O</ta>
            <ta e="T3" id="Seg_1077" s="T2">Лымбель_Карамо-ABL2</ta>
            <ta e="T4" id="Seg_1078" s="T3">Кананак-EP-ILL2</ta>
            <ta e="T5" id="Seg_1079" s="T4">такой-ADVZ</ta>
            <ta e="T6" id="Seg_1080" s="T5">быть.морозу-HAB-CO.[3SG.S]</ta>
            <ta e="T7" id="Seg_1081" s="T6">олень-INSTR</ta>
            <ta e="T8" id="Seg_1082" s="T7">вообще</ta>
            <ta e="T9" id="Seg_1083" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_1084" s="T9">любит-EP-1SG.O</ta>
            <ta e="T11" id="Seg_1085" s="T10">чем</ta>
            <ta e="T12" id="Seg_1086" s="T11">лошадь-EP-INSTR</ta>
            <ta e="T13" id="Seg_1087" s="T12">я.NOM</ta>
            <ta e="T14" id="Seg_1088" s="T13">такой-ADVZ</ta>
            <ta e="T15" id="Seg_1089" s="T14">олень-INSTR2</ta>
            <ta e="T16" id="Seg_1090" s="T15">привыкнуть-DUR-PST-1SG.S</ta>
            <ta e="T17" id="Seg_1091" s="T16">идти-HAB-PST-1SG.S</ta>
            <ta e="T18" id="Seg_1092" s="T17">олень-INSTR</ta>
            <ta e="T19" id="Seg_1093" s="T18">как</ta>
            <ta e="T20" id="Seg_1094" s="T19">ночь-ADV.LOC</ta>
            <ta e="T21" id="Seg_1095" s="T20">отправляться-FUT-1SG.S</ta>
            <ta e="T22" id="Seg_1096" s="T21">как</ta>
            <ta e="T23" id="Seg_1097" s="T22">олень-ACC</ta>
            <ta e="T24" id="Seg_1098" s="T23">%подстегнуть-FRQ-CVB2</ta>
            <ta e="T25" id="Seg_1099" s="T24">олень.[NOM]</ta>
            <ta e="T26" id="Seg_1100" s="T25">такой-ADVZ</ta>
            <ta e="T27" id="Seg_1101" s="T26">быстро</ta>
            <ta e="T28" id="Seg_1102" s="T27">бегать-EP-RES-IPFV2.[3SG.S]</ta>
            <ta e="T31" id="Seg_1103" s="T29">бегать-EP-RES-IPFV2.[3SG.S]</ta>
            <ta e="T32" id="Seg_1104" s="T31">ну</ta>
            <ta e="T33" id="Seg_1105" s="T32">я.NOM</ta>
            <ta e="T34" id="Seg_1106" s="T33">сколько</ta>
            <ta e="T35" id="Seg_1107" s="T34">год-GEN</ta>
            <ta e="T36" id="Seg_1108" s="T35">в.течение</ta>
            <ta e="T37" id="Seg_1109" s="T36">корова.[NOM]</ta>
            <ta e="T38" id="Seg_1110" s="T37">олень-INSTR</ta>
            <ta e="T39" id="Seg_1111" s="T38">почта.[NOM]</ta>
            <ta e="T40" id="Seg_1112" s="T39">возить-DUR-PST-1SG.O</ta>
            <ta e="T41" id="Seg_1113" s="T40">олень-INSTR</ta>
            <ta e="T42" id="Seg_1114" s="T41">Лымбель_Карамо-ABL2</ta>
            <ta e="T43" id="Seg_1115" s="T42">Кананак-EP-ILL2</ta>
            <ta e="T44" id="Seg_1116" s="T43">тогда</ta>
            <ta e="T45" id="Seg_1117" s="T44">NEG.EX-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_1118" s="T45">путь.[NOM]</ta>
            <ta e="T47" id="Seg_1119" s="T46">а</ta>
            <ta e="T48" id="Seg_1120" s="T47">только</ta>
            <ta e="T49" id="Seg_1121" s="T48">олень-INSTR</ta>
            <ta e="T50" id="Seg_1122" s="T49">идти-HAB-PST-3PL</ta>
            <ta e="T51" id="Seg_1123" s="T50">и</ta>
            <ta e="T52" id="Seg_1124" s="T51">тайга-ILL2</ta>
            <ta e="T53" id="Seg_1125" s="T52">олень-INSTR</ta>
            <ta e="T54" id="Seg_1126" s="T53">идти-HAB-PST-1SG.S</ta>
            <ta e="T55" id="Seg_1127" s="T54">и</ta>
            <ta e="T56" id="Seg_1128" s="T55">почта-ACC</ta>
            <ta e="T57" id="Seg_1129" s="T56">таскать-EP-DUR-HAB-PST-1SG.S</ta>
            <ta e="T58" id="Seg_1130" s="T57">ну</ta>
            <ta e="T59" id="Seg_1131" s="T58">теперь</ta>
            <ta e="T60" id="Seg_1132" s="T59">олень-ACC</ta>
            <ta e="T61" id="Seg_1133" s="T60">потом</ta>
            <ta e="T62" id="Seg_1134" s="T61">колхоз.[NOM]</ta>
            <ta e="T63" id="Seg_1135" s="T62">прочь</ta>
            <ta e="T64" id="Seg_1136" s="T63">делать-PST-3PL</ta>
            <ta e="T65" id="Seg_1137" s="T64">весь</ta>
            <ta e="T66" id="Seg_1138" s="T65">этот</ta>
            <ta e="T67" id="Seg_1139" s="T66">олень-ACC</ta>
            <ta e="T68" id="Seg_1140" s="T67">поделить-PST-3PL</ta>
            <ta e="T69" id="Seg_1141" s="T68">я.ALL</ta>
            <ta e="T70" id="Seg_1142" s="T69">тоже</ta>
            <ta e="T71" id="Seg_1143" s="T70">четыре</ta>
            <ta e="T72" id="Seg_1144" s="T71">пара</ta>
            <ta e="T73" id="Seg_1145" s="T72">дать-HAB-PST-3PL</ta>
            <ta e="T74" id="Seg_1146" s="T73">я.NOM</ta>
            <ta e="T75" id="Seg_1147" s="T74">потом</ta>
            <ta e="T76" id="Seg_1148" s="T75">продать-EP-PST-1SG.O</ta>
            <ta e="T77" id="Seg_1149" s="T76">этот</ta>
            <ta e="T78" id="Seg_1150" s="T77">там-%%</ta>
            <ta e="T79" id="Seg_1151" s="T78">Напас.[NOM]</ta>
            <ta e="T80" id="Seg_1152" s="T79">прийти-INF</ta>
            <ta e="T81" id="Seg_1153" s="T80">и</ta>
            <ta e="T82" id="Seg_1154" s="T81">лиса-1SG</ta>
            <ta e="T83" id="Seg_1155" s="T82">быть-PST.[3SG.S]</ta>
            <ta e="T84" id="Seg_1156" s="T83">четыре</ta>
            <ta e="T85" id="Seg_1157" s="T84">лиса-1SG</ta>
            <ta e="T86" id="Seg_1158" s="T85">быть-PST.[3SG.S]</ta>
            <ta e="T87" id="Seg_1159" s="T86">этот</ta>
            <ta e="T88" id="Seg_1160" s="T87">лиса-1SG</ta>
            <ta e="T89" id="Seg_1161" s="T88">тоже</ta>
            <ta e="T116" id="Seg_1162" s="T89">продать-EP-PST-1SG.O</ta>
            <ta e="T90" id="Seg_1163" s="T116">сюда</ta>
            <ta e="T91" id="Seg_1164" s="T90">Напас.[NOM]</ta>
            <ta e="T92" id="Seg_1165" s="T91">прийти-INF</ta>
            <ta e="T93" id="Seg_1166" s="T92">теперь</ta>
            <ta e="T94" id="Seg_1167" s="T93">Напас.[NOM]</ta>
            <ta e="T95" id="Seg_1168" s="T94">прийти-CVB</ta>
            <ta e="T96" id="Seg_1169" s="T95">этот</ta>
            <ta e="T97" id="Seg_1170" s="T96">жить-1SG.S</ta>
            <ta e="T98" id="Seg_1171" s="T97">здесь</ta>
            <ta e="T99" id="Seg_1172" s="T98">Напас-LOC</ta>
            <ta e="T100" id="Seg_1173" s="T99">дом.[NOM]</ta>
            <ta e="T101" id="Seg_1174" s="T100">купить-PST</ta>
            <ta e="T102" id="Seg_1175" s="T101">а</ta>
            <ta e="T103" id="Seg_1176" s="T102">большой</ta>
            <ta e="T104" id="Seg_1177" s="T103">дочь-1SG</ta>
            <ta e="T105" id="Seg_1178" s="T104">обучить-EP-PFV-PST.[3SG.S]</ta>
            <ta e="T106" id="Seg_1179" s="T105">работать.[3SG.S]</ta>
            <ta e="T107" id="Seg_1180" s="T106">пойти-PST.NAR.[3SG.S]</ta>
            <ta e="T108" id="Seg_1181" s="T107">маленький</ta>
            <ta e="T109" id="Seg_1182" s="T108">дочь-1SG</ta>
            <ta e="T110" id="Seg_1183" s="T109">тоже</ta>
            <ta e="T111" id="Seg_1184" s="T110">теперь</ta>
            <ta e="T112" id="Seg_1185" s="T111">обучить-EP-TR-PST.NAR.[3SG.S]</ta>
            <ta e="T113" id="Seg_1186" s="T112">дочь.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1187" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1188" s="T1">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1189" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_1190" s="T3">nprop-n:ins-n:case</ta>
            <ta e="T5" id="Seg_1191" s="T4">dem-adj&gt;adv</ta>
            <ta e="T6" id="Seg_1192" s="T5">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_1193" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1194" s="T7">adv</ta>
            <ta e="T9" id="Seg_1195" s="T8">pers</ta>
            <ta e="T10" id="Seg_1196" s="T9">v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_1197" s="T10">conj</ta>
            <ta e="T12" id="Seg_1198" s="T11">n-n:ins-n:case</ta>
            <ta e="T13" id="Seg_1199" s="T12">pers</ta>
            <ta e="T14" id="Seg_1200" s="T13">dem-adj&gt;adv</ta>
            <ta e="T15" id="Seg_1201" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1202" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1203" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1204" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1205" s="T18">conj</ta>
            <ta e="T20" id="Seg_1206" s="T19">n-adv:case</ta>
            <ta e="T21" id="Seg_1207" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1208" s="T21">conj</ta>
            <ta e="T23" id="Seg_1209" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1210" s="T23">v-v&gt;v-v&gt;adv</ta>
            <ta e="T25" id="Seg_1211" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1212" s="T25">dem-adj&gt;adv</ta>
            <ta e="T27" id="Seg_1213" s="T26">adv</ta>
            <ta e="T28" id="Seg_1214" s="T27">v-v:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_1215" s="T29">v-v:ins-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T32" id="Seg_1216" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1217" s="T32">pers</ta>
            <ta e="T34" id="Seg_1218" s="T33">interrog</ta>
            <ta e="T35" id="Seg_1219" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1220" s="T35">pp</ta>
            <ta e="T37" id="Seg_1221" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1222" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1223" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1224" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1225" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1226" s="T41">nprop-n:case</ta>
            <ta e="T43" id="Seg_1227" s="T42">nprop-n:ins-n:case</ta>
            <ta e="T44" id="Seg_1228" s="T43">adv</ta>
            <ta e="T45" id="Seg_1229" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1230" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1231" s="T46">conj</ta>
            <ta e="T48" id="Seg_1232" s="T47">adv</ta>
            <ta e="T49" id="Seg_1233" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_1234" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1235" s="T50">conj</ta>
            <ta e="T52" id="Seg_1236" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_1237" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1238" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1239" s="T54">conj</ta>
            <ta e="T56" id="Seg_1240" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1241" s="T56">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1242" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1243" s="T58">adv</ta>
            <ta e="T60" id="Seg_1244" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1245" s="T60">adv</ta>
            <ta e="T62" id="Seg_1246" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_1247" s="T62">preverb</ta>
            <ta e="T64" id="Seg_1248" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1249" s="T64">quant</ta>
            <ta e="T66" id="Seg_1250" s="T65">dem</ta>
            <ta e="T67" id="Seg_1251" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1252" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1253" s="T68">pers</ta>
            <ta e="T70" id="Seg_1254" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1255" s="T70">num</ta>
            <ta e="T72" id="Seg_1256" s="T71">n</ta>
            <ta e="T73" id="Seg_1257" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1258" s="T73">pers</ta>
            <ta e="T75" id="Seg_1259" s="T74">adv</ta>
            <ta e="T76" id="Seg_1260" s="T75">v-v:ins-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1261" s="T76">dem</ta>
            <ta e="T78" id="Seg_1262" s="T77">adv</ta>
            <ta e="T79" id="Seg_1263" s="T78">nprop-n:case</ta>
            <ta e="T80" id="Seg_1264" s="T79">v-v:inf</ta>
            <ta e="T81" id="Seg_1265" s="T80">conj</ta>
            <ta e="T82" id="Seg_1266" s="T81">n-n:poss</ta>
            <ta e="T83" id="Seg_1267" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1268" s="T83">num</ta>
            <ta e="T85" id="Seg_1269" s="T84">n-n:poss</ta>
            <ta e="T86" id="Seg_1270" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1271" s="T86">dem</ta>
            <ta e="T88" id="Seg_1272" s="T87">n-n:poss</ta>
            <ta e="T89" id="Seg_1273" s="T88">ptcl</ta>
            <ta e="T116" id="Seg_1274" s="T89">v-v:ins-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1275" s="T116">adv</ta>
            <ta e="T91" id="Seg_1276" s="T90">nprop-n:case</ta>
            <ta e="T92" id="Seg_1277" s="T91">v-v:inf</ta>
            <ta e="T93" id="Seg_1278" s="T92">adv</ta>
            <ta e="T94" id="Seg_1279" s="T93">nprop-n:case</ta>
            <ta e="T95" id="Seg_1280" s="T94">v-v&gt;adv</ta>
            <ta e="T96" id="Seg_1281" s="T95">dem</ta>
            <ta e="T97" id="Seg_1282" s="T96">v-v:pn</ta>
            <ta e="T98" id="Seg_1283" s="T97">adv</ta>
            <ta e="T99" id="Seg_1284" s="T98">nprop-n:case</ta>
            <ta e="T100" id="Seg_1285" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_1286" s="T100">v-v:tense</ta>
            <ta e="T102" id="Seg_1287" s="T101">conj</ta>
            <ta e="T103" id="Seg_1288" s="T102">adj</ta>
            <ta e="T104" id="Seg_1289" s="T103">n-n:poss</ta>
            <ta e="T105" id="Seg_1290" s="T104">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1291" s="T105">v-v:pn</ta>
            <ta e="T107" id="Seg_1292" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1293" s="T107">adj</ta>
            <ta e="T109" id="Seg_1294" s="T108">n-n:poss</ta>
            <ta e="T110" id="Seg_1295" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1296" s="T110">adv</ta>
            <ta e="T112" id="Seg_1297" s="T111">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1298" s="T112">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1299" s="T0">n</ta>
            <ta e="T2" id="Seg_1300" s="T1">v</ta>
            <ta e="T3" id="Seg_1301" s="T2">nprop</ta>
            <ta e="T4" id="Seg_1302" s="T3">nprop</ta>
            <ta e="T5" id="Seg_1303" s="T4">adv</ta>
            <ta e="T6" id="Seg_1304" s="T5">v</ta>
            <ta e="T7" id="Seg_1305" s="T6">n</ta>
            <ta e="T8" id="Seg_1306" s="T7">adv</ta>
            <ta e="T9" id="Seg_1307" s="T8">pers</ta>
            <ta e="T10" id="Seg_1308" s="T9">v</ta>
            <ta e="T11" id="Seg_1309" s="T10">conj</ta>
            <ta e="T12" id="Seg_1310" s="T11">n</ta>
            <ta e="T13" id="Seg_1311" s="T12">pers</ta>
            <ta e="T14" id="Seg_1312" s="T13">adv</ta>
            <ta e="T15" id="Seg_1313" s="T14">n</ta>
            <ta e="T16" id="Seg_1314" s="T15">v</ta>
            <ta e="T17" id="Seg_1315" s="T16">v</ta>
            <ta e="T18" id="Seg_1316" s="T17">n</ta>
            <ta e="T19" id="Seg_1317" s="T18">conj</ta>
            <ta e="T20" id="Seg_1318" s="T19">n</ta>
            <ta e="T21" id="Seg_1319" s="T20">v</ta>
            <ta e="T22" id="Seg_1320" s="T21">conj</ta>
            <ta e="T24" id="Seg_1321" s="T23">adv</ta>
            <ta e="T25" id="Seg_1322" s="T24">n</ta>
            <ta e="T26" id="Seg_1323" s="T25">adv</ta>
            <ta e="T27" id="Seg_1324" s="T26">adv</ta>
            <ta e="T28" id="Seg_1325" s="T27">v</ta>
            <ta e="T31" id="Seg_1326" s="T29">v</ta>
            <ta e="T32" id="Seg_1327" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1328" s="T32">pers</ta>
            <ta e="T34" id="Seg_1329" s="T33">interrog</ta>
            <ta e="T35" id="Seg_1330" s="T34">n</ta>
            <ta e="T36" id="Seg_1331" s="T35">pp</ta>
            <ta e="T37" id="Seg_1332" s="T36">n</ta>
            <ta e="T38" id="Seg_1333" s="T37">n</ta>
            <ta e="T39" id="Seg_1334" s="T38">n</ta>
            <ta e="T40" id="Seg_1335" s="T39">v</ta>
            <ta e="T41" id="Seg_1336" s="T40">n</ta>
            <ta e="T42" id="Seg_1337" s="T41">nprop</ta>
            <ta e="T43" id="Seg_1338" s="T42">nprop</ta>
            <ta e="T44" id="Seg_1339" s="T43">adv</ta>
            <ta e="T45" id="Seg_1340" s="T44">v</ta>
            <ta e="T46" id="Seg_1341" s="T45">n</ta>
            <ta e="T47" id="Seg_1342" s="T46">conj</ta>
            <ta e="T48" id="Seg_1343" s="T47">adv</ta>
            <ta e="T49" id="Seg_1344" s="T48">n</ta>
            <ta e="T50" id="Seg_1345" s="T49">v</ta>
            <ta e="T51" id="Seg_1346" s="T50">conj</ta>
            <ta e="T52" id="Seg_1347" s="T51">n</ta>
            <ta e="T53" id="Seg_1348" s="T52">n</ta>
            <ta e="T54" id="Seg_1349" s="T53">v</ta>
            <ta e="T55" id="Seg_1350" s="T54">conj</ta>
            <ta e="T56" id="Seg_1351" s="T55">n</ta>
            <ta e="T57" id="Seg_1352" s="T56">v</ta>
            <ta e="T58" id="Seg_1353" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_1354" s="T58">adv</ta>
            <ta e="T60" id="Seg_1355" s="T59">n</ta>
            <ta e="T61" id="Seg_1356" s="T60">adv</ta>
            <ta e="T62" id="Seg_1357" s="T61">n</ta>
            <ta e="T63" id="Seg_1358" s="T62">preverb</ta>
            <ta e="T64" id="Seg_1359" s="T63">v</ta>
            <ta e="T65" id="Seg_1360" s="T64">quant</ta>
            <ta e="T66" id="Seg_1361" s="T65">pro</ta>
            <ta e="T67" id="Seg_1362" s="T66">n</ta>
            <ta e="T68" id="Seg_1363" s="T67">v</ta>
            <ta e="T69" id="Seg_1364" s="T68">pers</ta>
            <ta e="T70" id="Seg_1365" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_1366" s="T70">num</ta>
            <ta e="T72" id="Seg_1367" s="T71">n</ta>
            <ta e="T73" id="Seg_1368" s="T72">v</ta>
            <ta e="T74" id="Seg_1369" s="T73">pers</ta>
            <ta e="T75" id="Seg_1370" s="T74">adv</ta>
            <ta e="T76" id="Seg_1371" s="T75">v</ta>
            <ta e="T77" id="Seg_1372" s="T76">dem</ta>
            <ta e="T78" id="Seg_1373" s="T77">adv</ta>
            <ta e="T79" id="Seg_1374" s="T78">nprop</ta>
            <ta e="T80" id="Seg_1375" s="T79">v</ta>
            <ta e="T81" id="Seg_1376" s="T80">conj</ta>
            <ta e="T82" id="Seg_1377" s="T81">n</ta>
            <ta e="T83" id="Seg_1378" s="T82">v</ta>
            <ta e="T84" id="Seg_1379" s="T83">num</ta>
            <ta e="T85" id="Seg_1380" s="T84">n</ta>
            <ta e="T86" id="Seg_1381" s="T85">v</ta>
            <ta e="T87" id="Seg_1382" s="T86">pro</ta>
            <ta e="T88" id="Seg_1383" s="T87">n</ta>
            <ta e="T89" id="Seg_1384" s="T88">ptcl</ta>
            <ta e="T116" id="Seg_1385" s="T89">v</ta>
            <ta e="T90" id="Seg_1386" s="T116">adv</ta>
            <ta e="T91" id="Seg_1387" s="T90">nprop</ta>
            <ta e="T92" id="Seg_1388" s="T91">v</ta>
            <ta e="T93" id="Seg_1389" s="T92">adv</ta>
            <ta e="T94" id="Seg_1390" s="T93">nprop</ta>
            <ta e="T95" id="Seg_1391" s="T94">adv</ta>
            <ta e="T96" id="Seg_1392" s="T95">adv</ta>
            <ta e="T97" id="Seg_1393" s="T96">v</ta>
            <ta e="T98" id="Seg_1394" s="T97">adv</ta>
            <ta e="T99" id="Seg_1395" s="T98">nprop</ta>
            <ta e="T100" id="Seg_1396" s="T99">pers</ta>
            <ta e="T101" id="Seg_1397" s="T100">v</ta>
            <ta e="T102" id="Seg_1398" s="T101">conj</ta>
            <ta e="T103" id="Seg_1399" s="T102">adj</ta>
            <ta e="T104" id="Seg_1400" s="T103">n</ta>
            <ta e="T105" id="Seg_1401" s="T104">v</ta>
            <ta e="T106" id="Seg_1402" s="T105">v</ta>
            <ta e="T107" id="Seg_1403" s="T106">v</ta>
            <ta e="T108" id="Seg_1404" s="T107">adj</ta>
            <ta e="T109" id="Seg_1405" s="T108">n</ta>
            <ta e="T110" id="Seg_1406" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1407" s="T110">adv</ta>
            <ta e="T112" id="Seg_1408" s="T111">v</ta>
            <ta e="T113" id="Seg_1409" s="T112">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1410" s="T0">np:O</ta>
            <ta e="T2" id="Seg_1411" s="T1">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_1412" s="T5">0.3:S v:pred</ta>
            <ta e="T9" id="Seg_1413" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1414" s="T9">v:pred 0.3:O</ta>
            <ta e="T13" id="Seg_1415" s="T12">pro.h:S</ta>
            <ta e="T16" id="Seg_1416" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_1417" s="T16">v:pred</ta>
            <ta e="T21" id="Seg_1418" s="T20">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_1419" s="T24">np:S</ta>
            <ta e="T28" id="Seg_1420" s="T27">v:pred</ta>
            <ta e="T31" id="Seg_1421" s="T29">0.3:S v:pred</ta>
            <ta e="T33" id="Seg_1422" s="T32">pro.h:S</ta>
            <ta e="T39" id="Seg_1423" s="T38">np:O</ta>
            <ta e="T40" id="Seg_1424" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_1425" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_1426" s="T45">np:S</ta>
            <ta e="T50" id="Seg_1427" s="T49">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_1428" s="T53">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_1429" s="T55">np:O</ta>
            <ta e="T57" id="Seg_1430" s="T56">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_1431" s="T59">np:O</ta>
            <ta e="T62" id="Seg_1432" s="T61">np:O</ta>
            <ta e="T64" id="Seg_1433" s="T63">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_1434" s="T66">np:O</ta>
            <ta e="T68" id="Seg_1435" s="T67">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_1436" s="T71">np:O</ta>
            <ta e="T73" id="Seg_1437" s="T72">0.3.h:S v:pred</ta>
            <ta e="T74" id="Seg_1438" s="T73">pro.h:S</ta>
            <ta e="T76" id="Seg_1439" s="T75">v:pred 0.3:O</ta>
            <ta e="T80" id="Seg_1440" s="T76">s:purp</ta>
            <ta e="T82" id="Seg_1441" s="T81">np:S</ta>
            <ta e="T83" id="Seg_1442" s="T82">v:pred</ta>
            <ta e="T85" id="Seg_1443" s="T84">np:S</ta>
            <ta e="T86" id="Seg_1444" s="T85">v:pred</ta>
            <ta e="T88" id="Seg_1445" s="T87">np:O</ta>
            <ta e="T116" id="Seg_1446" s="T89">0.1.h:S v:pred</ta>
            <ta e="T92" id="Seg_1447" s="T116">s:purp</ta>
            <ta e="T95" id="Seg_1448" s="T93">s:temp</ta>
            <ta e="T97" id="Seg_1449" s="T96">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_1450" s="T99">np:O</ta>
            <ta e="T101" id="Seg_1451" s="T100">0.1.h:S v:pred</ta>
            <ta e="T104" id="Seg_1452" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_1453" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_1454" s="T105">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_1455" s="T106">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1456" s="T108">np.h:S</ta>
            <ta e="T112" id="Seg_1457" s="T111">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1458" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_1459" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_1460" s="T2">np:S</ta>
            <ta e="T4" id="Seg_1461" s="T3">np:G</ta>
            <ta e="T6" id="Seg_1462" s="T5">0.3:Th</ta>
            <ta e="T7" id="Seg_1463" s="T6">np:Ins</ta>
            <ta e="T9" id="Seg_1464" s="T8">pro.h:E</ta>
            <ta e="T10" id="Seg_1465" s="T9">0.3:Th</ta>
            <ta e="T12" id="Seg_1466" s="T11">np:Ins</ta>
            <ta e="T13" id="Seg_1467" s="T12">pro.h:A</ta>
            <ta e="T15" id="Seg_1468" s="T14">np:Ins</ta>
            <ta e="T18" id="Seg_1469" s="T17">np:Ins</ta>
            <ta e="T20" id="Seg_1470" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_1471" s="T20">0.1.h:A</ta>
            <ta e="T23" id="Seg_1472" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_1473" s="T24">np:A</ta>
            <ta e="T31" id="Seg_1474" s="T29">0.3:A</ta>
            <ta e="T33" id="Seg_1475" s="T32">pro.h:A</ta>
            <ta e="T36" id="Seg_1476" s="T34">pp:Time</ta>
            <ta e="T38" id="Seg_1477" s="T37">np:Ins</ta>
            <ta e="T39" id="Seg_1478" s="T38">np:Th</ta>
            <ta e="T41" id="Seg_1479" s="T40">np:Ins</ta>
            <ta e="T42" id="Seg_1480" s="T41">np:So</ta>
            <ta e="T43" id="Seg_1481" s="T42">np:G</ta>
            <ta e="T44" id="Seg_1482" s="T43">adv:Time</ta>
            <ta e="T46" id="Seg_1483" s="T45">np:Th</ta>
            <ta e="T49" id="Seg_1484" s="T48">np:Ins</ta>
            <ta e="T50" id="Seg_1485" s="T49">0.3.h:A</ta>
            <ta e="T52" id="Seg_1486" s="T51">np:G</ta>
            <ta e="T53" id="Seg_1487" s="T52">np:Ins</ta>
            <ta e="T54" id="Seg_1488" s="T53">0.1.h:A</ta>
            <ta e="T56" id="Seg_1489" s="T55">np:Th</ta>
            <ta e="T57" id="Seg_1490" s="T56">0.1.h:A</ta>
            <ta e="T59" id="Seg_1491" s="T58">adv:Time</ta>
            <ta e="T60" id="Seg_1492" s="T59">np:Th</ta>
            <ta e="T61" id="Seg_1493" s="T60">adv:Time</ta>
            <ta e="T62" id="Seg_1494" s="T61">np:P</ta>
            <ta e="T64" id="Seg_1495" s="T63">0.3.h:A</ta>
            <ta e="T67" id="Seg_1496" s="T66">np:Th</ta>
            <ta e="T68" id="Seg_1497" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_1498" s="T68">pro.h:R</ta>
            <ta e="T72" id="Seg_1499" s="T71">np:Th</ta>
            <ta e="T73" id="Seg_1500" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_1501" s="T73">pro.h:A</ta>
            <ta e="T75" id="Seg_1502" s="T74">adv:Time</ta>
            <ta e="T76" id="Seg_1503" s="T75">0.3:Th</ta>
            <ta e="T78" id="Seg_1504" s="T77">adv:L</ta>
            <ta e="T79" id="Seg_1505" s="T78">np:G</ta>
            <ta e="T82" id="Seg_1506" s="T81">np:Th 0.1.h:Poss</ta>
            <ta e="T85" id="Seg_1507" s="T84">np:Th 0.1.h:Poss</ta>
            <ta e="T88" id="Seg_1508" s="T87">np:Th 0.1.h:Poss</ta>
            <ta e="T116" id="Seg_1509" s="T89">0.1.h:A</ta>
            <ta e="T90" id="Seg_1510" s="T116">adv:G</ta>
            <ta e="T91" id="Seg_1511" s="T90">np:G</ta>
            <ta e="T93" id="Seg_1512" s="T92">adv:Time</ta>
            <ta e="T94" id="Seg_1513" s="T93">np:G</ta>
            <ta e="T97" id="Seg_1514" s="T96">0.1.h:Th</ta>
            <ta e="T98" id="Seg_1515" s="T97">adv:L</ta>
            <ta e="T99" id="Seg_1516" s="T98">np:L</ta>
            <ta e="T100" id="Seg_1517" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_1518" s="T100">0.1.h:A</ta>
            <ta e="T104" id="Seg_1519" s="T103">np.h:A 0.1.h:Poss</ta>
            <ta e="T106" id="Seg_1520" s="T105">0.3.h:A</ta>
            <ta e="T107" id="Seg_1521" s="T106">0.3.h:A</ta>
            <ta e="T109" id="Seg_1522" s="T108">np.h:A 0.1.h:Poss</ta>
            <ta e="T111" id="Seg_1523" s="T110">adv:Time</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_1524" s="T0">RUS:cult</ta>
            <ta e="T2" id="Seg_1525" s="T1">RUS:core</ta>
            <ta e="T8" id="Seg_1526" s="T7">RUS:core</ta>
            <ta e="T11" id="Seg_1527" s="T10">RUS:gram</ta>
            <ta e="T19" id="Seg_1528" s="T18">RUS:gram</ta>
            <ta e="T22" id="Seg_1529" s="T21">RUS:gram</ta>
            <ta e="T24" id="Seg_1530" s="T23">RUS:core</ta>
            <ta e="T32" id="Seg_1531" s="T31">RUS:disc</ta>
            <ta e="T39" id="Seg_1532" s="T38">RUS:cult</ta>
            <ta e="T40" id="Seg_1533" s="T39">RUS:core</ta>
            <ta e="T45" id="Seg_1534" s="T44">RUS:gram</ta>
            <ta e="T47" id="Seg_1535" s="T46">RUS:gram</ta>
            <ta e="T48" id="Seg_1536" s="T47">RUS:gram</ta>
            <ta e="T51" id="Seg_1537" s="T50">RUS:gram</ta>
            <ta e="T55" id="Seg_1538" s="T54">RUS:gram</ta>
            <ta e="T56" id="Seg_1539" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_1540" s="T56">RUS:core</ta>
            <ta e="T58" id="Seg_1541" s="T57">RUS:disc</ta>
            <ta e="T59" id="Seg_1542" s="T58">RUS:core</ta>
            <ta e="T61" id="Seg_1543" s="T60">RUS:core</ta>
            <ta e="T65" id="Seg_1544" s="T64">RUS:core</ta>
            <ta e="T72" id="Seg_1545" s="T71">RUS:cult</ta>
            <ta e="T75" id="Seg_1546" s="T74">RUS:core</ta>
            <ta e="T81" id="Seg_1547" s="T80">RUS:gram</ta>
            <ta e="T93" id="Seg_1548" s="T92">RUS:core</ta>
            <ta e="T102" id="Seg_1549" s="T101">RUS:gram</ta>
            <ta e="T111" id="Seg_1550" s="T110">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1551" s="T118">Почту возила из Лымбеля в Кананак.</ta>
            <ta e="T6" id="Seg_1552" s="T4">Так холодно бывает.</ta>
            <ta e="T12" id="Seg_1553" s="T6">На олене вообще я больше люблю, чем на лошади.</ta>
            <ta e="T18" id="Seg_1554" s="T12">Я так на олене научилась, ездила на олене.</ta>
            <ta e="T31" id="Seg_1555" s="T18">Как ночью соберусь, как оленя (подстегну?) — олень так быстро бежит, (…) бежит!</ta>
            <ta e="T43" id="Seg_1556" s="T31">Да, я сколько лет почту возила на оленихе из Лымбеля в Кананак.</ta>
            <ta e="T50" id="Seg_1557" s="T43">Тогда не было дороги, а только на олене ездили.</ta>
            <ta e="T57" id="Seg_1558" s="T50">И в тайгу на олене ездила, и почту возила.</ta>
            <ta e="T68" id="Seg_1559" s="T57">Но теперь оленя, потом колхоз закрыли, всех этих оленей раздали.</ta>
            <ta e="T76" id="Seg_1560" s="T68">Мне тоже четыре пары давали, я потом продала.</ta>
            <ta e="T80" id="Seg_1561" s="T76">Сюда, в Напас, приезжать.</ta>
            <ta e="T92" id="Seg_1562" s="T80">И лиса была, четыре лисы было, этих лис тоже продала, чтобы сюда, в Напас приехать.</ta>
            <ta e="T98" id="Seg_1563" s="T92">Теперь, в Напас приехав, здесь живу.</ta>
            <ta e="T101" id="Seg_1564" s="T98">В Напасе дом купила.</ta>
            <ta e="T107" id="Seg_1565" s="T101">А старшая дочь выучилась, работает, уехала.</ta>
            <ta e="T117" id="Seg_1566" s="T107">Младшая дочь тоже теперь выучилась, дочь (…)</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1567" s="T118">I carried mail from Lymbelj to Kananak.</ta>
            <ta e="T6" id="Seg_1568" s="T4">It gets cold.</ta>
            <ta e="T12" id="Seg_1569" s="T6">I like it better with the reindeer than with the horse.</ta>
            <ta e="T18" id="Seg_1570" s="T12">This way I learned to ride a reindeer, I used to ride a reindeer.</ta>
            <ta e="T31" id="Seg_1571" s="T18">I would get ready at night, (urge?) the reindeer – and the reindeer would run so fast, (…) would run (…)!</ta>
            <ta e="T43" id="Seg_1572" s="T31">Yes, I carried mail for so many years on a female reindeer from Lymbelj to Sagandukovo.</ta>
            <ta e="T50" id="Seg_1573" s="T43">There was no road then, we would ride the reindeer.</ta>
            <ta e="T57" id="Seg_1574" s="T50">And I would go to the taiga on a reindeer, carrying the mail.</ta>
            <ta e="T68" id="Seg_1575" s="T57">But now the reindeer, then they closed the cooperative, and the reindeer were given away.</ta>
            <ta e="T76" id="Seg_1576" s="T68">I was also given four pairs, I sold them later.</ta>
            <ta e="T80" id="Seg_1577" s="T76">Here, to Napas, to come here.</ta>
            <ta e="T92" id="Seg_1578" s="T80">And there was a fox, there were four foxes, I sold those foxes as well, to come here, to Napas.</ta>
            <ta e="T98" id="Seg_1579" s="T92">Now, having come to Napas, I live here.</ta>
            <ta e="T101" id="Seg_1580" s="T98">I bought a house in Napas.</ta>
            <ta e="T107" id="Seg_1581" s="T101">And my older daughter got an education, she works, she left.</ta>
            <ta e="T117" id="Seg_1582" s="T107">My younger daughter also got an education, my daughter (…)</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1583" s="T118">Ich trug Post von Lymbelj nach Kananak.</ta>
            <ta e="T6" id="Seg_1584" s="T4">Es wird kalt.</ta>
            <ta e="T12" id="Seg_1585" s="T6">Ich mag es lieber mit den Rentieren als mit den Pferdne.</ta>
            <ta e="T18" id="Seg_1586" s="T12">Ich habe so gelernt ein Rentier zu reiten, ich ging auf ein Rentier.</ta>
            <ta e="T31" id="Seg_1587" s="T18">Ich habe mich nachts fertig gemacht, das Rentier (getrieben?) — und das Rentier ist so schnell gerannt, (…) gerannt!</ta>
            <ta e="T43" id="Seg_1588" s="T31">Ja, ich habe so viele Jahre die Post mit einer Rentierkuh von Lymbelj nach Sagandukovo ausgetragen.</ta>
            <ta e="T50" id="Seg_1589" s="T43">Damals gab es dort keine Straße, wir haben Rentiere geritten.</ta>
            <ta e="T57" id="Seg_1590" s="T50">Und ich bin in die Taiga auf einem Rentier geritten, um die Post auszutragen.</ta>
            <ta e="T68" id="Seg_1591" s="T57">Aber jetzt die Rentiere, dann haben sie die Kolchose geschlossen, und die Rentiere wurden weggegeben.</ta>
            <ta e="T76" id="Seg_1592" s="T68">Mir wurden auch vier Paare gegeben, ich habe sie später verkauft.</ta>
            <ta e="T80" id="Seg_1593" s="T76">Hier, nach Napas, um hier her zu kommen.</ta>
            <ta e="T92" id="Seg_1594" s="T80">Und da war ein Fuchs, da waren vier Füchse, ich habe diese Füchse auch verkauft um hier, nach Napas zu kommen.</ta>
            <ta e="T98" id="Seg_1595" s="T92">Jetzt, nachdem ich nach Napas gekommen bin, lebe ich hier.</ta>
            <ta e="T101" id="Seg_1596" s="T98">Ich habe ein Haus in Napas gekauft.</ta>
            <ta e="T107" id="Seg_1597" s="T101">Und meine ältere Tochter hat eine Ausbildung bekommen, sie arbeitet, sie ging weg.</ta>
            <ta e="T117" id="Seg_1598" s="T107">Meine jüngere Tochter hat auch eine Ausbildung, meine Tochter (…)</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1599" s="T118">Почту возила из Лымбеля в Кананак.</ta>
            <ta e="T6" id="Seg_1600" s="T4">Так холодно бывает.</ta>
            <ta e="T12" id="Seg_1601" s="T6">На олене вообще я больше люблю, чем на лошади.</ta>
            <ta e="T18" id="Seg_1602" s="T12">Я так на олене научилась, ездить на олене.</ta>
            <ta e="T31" id="Seg_1603" s="T18">Как ночью соберусь, как оленя шурану — олень так быстро бежит!</ta>
            <ta e="T43" id="Seg_1604" s="T31">Да, я сколько лет почту возила на олене из Лымбеля в Сагандуково.</ta>
            <ta e="T50" id="Seg_1605" s="T43">Тогда не было дороги, а только на олене ездили и почту возила.</ta>
            <ta e="T57" id="Seg_1606" s="T50">И в тайгу на олене ездила, и почту возила.</ta>
            <ta e="T68" id="Seg_1607" s="T57">Но теперь оленя, потом колхоз закрыли, всех этих оленей раздали.</ta>
            <ta e="T76" id="Seg_1608" s="T68">Мне тоже четыре пары давали, я потом продала.</ta>
            <ta e="T80" id="Seg_1609" s="T76">Сюда, в Напас, приезжать.</ta>
            <ta e="T92" id="Seg_1610" s="T80">И лиса была, четыре лисы было, тоже продала, в Напас уехать.</ta>
            <ta e="T98" id="Seg_1611" s="T92">Теперь, в Напас приехав, здесь живу.</ta>
            <ta e="T101" id="Seg_1612" s="T98">В Напасе дом купила.</ta>
            <ta e="T107" id="Seg_1613" s="T101">А старшая дочь выучилась, работает, уехала.</ta>
            <ta e="T117" id="Seg_1614" s="T107">Младшая дочь тоже теперь выучилась, дочь.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_1615" s="T118">[AAV:] Kananartʼan ?</ta>
            <ta e="T18" id="Seg_1616" s="T12">[AAV:] ogolembhwak; qwajakhwaŋ [-1SG.S] ?</ta>
            <ta e="T31" id="Seg_1617" s="T18">[AAV:] wet, aǯʼiŋ nalʼǯʼ kuralešpa ?</ta>
            <ta e="T43" id="Seg_1618" s="T31">[AAV:] (potkund) xɨr aštəhə ? Kananartʼan ?</ta>
            <ta e="T57" id="Seg_1619" s="T50">[AAV:] taskaimbukhwak</ta>
            <ta e="T68" id="Seg_1620" s="T57">[AAV:] voiceless bilabial instead of velar in kalfos ([ʍ]/[ɸ]?); velar (not laryngeal) in mɨɣat, taːrxat </ta>
            <ta e="T76" id="Seg_1621" s="T68">[AAV:] Meːka; param [-ACC]?; mekkuhwat; mörɨxːap (strong [x:]).</ta>
            <ta e="T101" id="Seg_1622" s="T98">[AAV:] expected tawhap [-1SG.O]</ta>
            <ta e="T107" id="Seg_1623" s="T101">[AAV:] tape damaged</ta>
            <ta e="T117" id="Seg_1624" s="T107">[AAV:] tape damaged; (in the end) naj qwirɣələ ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T118" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T119" />
            <conversion-tli id="T29" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T116" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T115" />
            <conversion-tli id="T117" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
