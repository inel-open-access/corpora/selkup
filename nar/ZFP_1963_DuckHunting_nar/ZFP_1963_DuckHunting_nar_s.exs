<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ZFP_1963_DuckHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ZFP_1963_DuckHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">52</ud-information>
            <ud-information attribute-name="# HIAT:w">45</ud-information>
            <ud-information attribute-name="# e">45</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ZFP">
            <abbreviation>ZFP</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ZFP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T46" id="Seg_0" n="sc" s="T1">
               <ts e="T13" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">uʒe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kundoqɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">moglʼi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">qoldigu</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">što</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">üːt</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_26" n="HIAT:w" s="T8">me</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_29" n="HIAT:w" s="T9">anduqɨnat</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_32" n="HIAT:w" s="T10">okkɨni</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_35" n="HIAT:w" s="T11">tiːrilʼe</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_38" n="HIAT:w" s="T12">čaːčaŋ</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_42" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">üːtannin</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">nʼäbɨla</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">wässakulʼlʼe</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">ollɨtat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">nildʼi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">wärgɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">tabunɨŋ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">što</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_68" n="HIAT:w" s="T21">me</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_71" n="HIAT:w" s="T22">tʼüldiseːlawɨt</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_74" n="HIAT:w" s="T23">apsɨtɨgu</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_77" n="HIAT:w" s="T24">assə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_80" n="HIAT:w" s="T25">uspeluwukussot</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_84" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_86" n="HIAT:w" s="T26">kündɨlʼe</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_89" n="HIAT:w" s="T27">meː</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_92" n="HIAT:w" s="T28">andundɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_95" n="HIAT:w" s="T29">assə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_98" n="HIAT:w" s="T30">manǯekussot</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_102" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">i</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_107" n="HIAT:w" s="T32">qaːɣi</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_110" n="HIAT:w" s="T33">Ermolaj</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_113" n="HIAT:w" s="T34">qwätpɨdi</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_116" n="HIAT:w" s="T35">nʼäbam</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_119" n="HIAT:w" s="T36">iːgu</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_122" n="HIAT:w" s="T37">kɨnǯalǯɨs</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_126" n="HIAT:w" s="T38">me</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_129" n="HIAT:w" s="T39">anduwɨt</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_132" n="HIAT:w" s="T40">pokkun</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_135" n="HIAT:w" s="T41">piːgəlʼeŋ</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_139" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_141" n="HIAT:w" s="T42">i</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_144" n="HIAT:w" s="T43">me</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_147" n="HIAT:w" s="T44">üːtqan</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_150" n="HIAT:w" s="T45">počeot</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T46" id="Seg_153" n="sc" s="T1">
               <ts e="T2" id="Seg_155" n="e" s="T1">me </ts>
               <ts e="T3" id="Seg_157" n="e" s="T2">uʒe </ts>
               <ts e="T4" id="Seg_159" n="e" s="T3">kundoqɨn </ts>
               <ts e="T5" id="Seg_161" n="e" s="T4">moglʼi </ts>
               <ts e="T6" id="Seg_163" n="e" s="T5">qoldigu, </ts>
               <ts e="T7" id="Seg_165" n="e" s="T6">što </ts>
               <ts e="T8" id="Seg_167" n="e" s="T7">üːt </ts>
               <ts e="T9" id="Seg_169" n="e" s="T8">me </ts>
               <ts e="T10" id="Seg_171" n="e" s="T9">anduqɨnat </ts>
               <ts e="T11" id="Seg_173" n="e" s="T10">okkɨni </ts>
               <ts e="T12" id="Seg_175" n="e" s="T11">tiːrilʼe </ts>
               <ts e="T13" id="Seg_177" n="e" s="T12">čaːčaŋ. </ts>
               <ts e="T14" id="Seg_179" n="e" s="T13">üːtannin </ts>
               <ts e="T15" id="Seg_181" n="e" s="T14">nʼäbɨla </ts>
               <ts e="T16" id="Seg_183" n="e" s="T15">wässakulʼlʼe </ts>
               <ts e="T17" id="Seg_185" n="e" s="T16">ollɨtat </ts>
               <ts e="T18" id="Seg_187" n="e" s="T17">nildʼi </ts>
               <ts e="T19" id="Seg_189" n="e" s="T18">wärgɨ </ts>
               <ts e="T20" id="Seg_191" n="e" s="T19">tabunɨŋ </ts>
               <ts e="T21" id="Seg_193" n="e" s="T20">što </ts>
               <ts e="T22" id="Seg_195" n="e" s="T21">me </ts>
               <ts e="T23" id="Seg_197" n="e" s="T22">tʼüldiseːlawɨt </ts>
               <ts e="T24" id="Seg_199" n="e" s="T23">apsɨtɨgu </ts>
               <ts e="T25" id="Seg_201" n="e" s="T24">assə </ts>
               <ts e="T26" id="Seg_203" n="e" s="T25">uspeluwukussot. </ts>
               <ts e="T27" id="Seg_205" n="e" s="T26">kündɨlʼe </ts>
               <ts e="T28" id="Seg_207" n="e" s="T27">meː </ts>
               <ts e="T29" id="Seg_209" n="e" s="T28">andundɨ </ts>
               <ts e="T30" id="Seg_211" n="e" s="T29">assə </ts>
               <ts e="T31" id="Seg_213" n="e" s="T30">manǯekussot. </ts>
               <ts e="T32" id="Seg_215" n="e" s="T31">i </ts>
               <ts e="T33" id="Seg_217" n="e" s="T32">qaːɣi </ts>
               <ts e="T34" id="Seg_219" n="e" s="T33">Ermolaj </ts>
               <ts e="T35" id="Seg_221" n="e" s="T34">qwätpɨdi </ts>
               <ts e="T36" id="Seg_223" n="e" s="T35">nʼäbam </ts>
               <ts e="T37" id="Seg_225" n="e" s="T36">iːgu </ts>
               <ts e="T38" id="Seg_227" n="e" s="T37">kɨnǯalǯɨs, </ts>
               <ts e="T39" id="Seg_229" n="e" s="T38">me </ts>
               <ts e="T40" id="Seg_231" n="e" s="T39">anduwɨt </ts>
               <ts e="T41" id="Seg_233" n="e" s="T40">pokkun </ts>
               <ts e="T42" id="Seg_235" n="e" s="T41">piːgəlʼeŋ. </ts>
               <ts e="T43" id="Seg_237" n="e" s="T42">i </ts>
               <ts e="T44" id="Seg_239" n="e" s="T43">me </ts>
               <ts e="T45" id="Seg_241" n="e" s="T44">üːtqan </ts>
               <ts e="T46" id="Seg_243" n="e" s="T45">počeot. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T13" id="Seg_244" s="T1">ZFP_1963_DuckHunting_nar.001 (001.001)</ta>
            <ta e="T26" id="Seg_245" s="T13">ZFP_1963_DuckHunting_nar.002 (001.002)</ta>
            <ta e="T31" id="Seg_246" s="T26">ZFP_1963_DuckHunting_nar.003 (001.003)</ta>
            <ta e="T42" id="Seg_247" s="T31">ZFP_1963_DuckHunting_nar.004 (001.004)</ta>
            <ta e="T46" id="Seg_248" s="T42">ZFP_1963_DuckHunting_nar.005 (001.005)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T13" id="Seg_249" s="T1">ме уже кун′доkын моглʼи ′kоlдигу, што ӱ̄т ме ‵ан′дуkы′нат оккы′ни ′тӣрилʼе ′тша̄тшаң.</ta>
            <ta e="T26" id="Seg_250" s="T13">′ӱ̄та′ннин ′нʼӓбыла ‵вӓссаку′лʼлʼе ′оllы‵тат ниl′дʼи вӓр′гы та′буның што ме ‵тʼӱlди′се̨̄лавыт ′апсытыгу ′ассъ ус′пелу‵вуку′ссот.</ta>
            <ta e="T31" id="Seg_251" s="T26">′кӱндылʼе ме̄ ′андунды ′ассъ ман′джекуссот.</ta>
            <ta e="T42" id="Seg_252" s="T31">и ′kа̄ɣи Ермолай ′kwӓтпы‵ди ′нʼӓбам ′ӣгу кын′джалджы(и)с, ме ′андувыт ′поккун ′пӣгълʼең.</ta>
            <ta e="T46" id="Seg_253" s="T42">и ме ӱ̄т′kан по′тшеот.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T13" id="Seg_254" s="T1">me uʒe kundoqɨn moglʼi qoldigu, što üːt me anduqɨnat okkɨni tiːrilʼe tšaːtšaŋ.</ta>
            <ta e="T26" id="Seg_255" s="T13">üːtannin nʼäbɨla vässakulʼlʼe ollɨtat nildʼi värgɨ tabunɨŋ što me tʼüldiseːlavɨt apsɨtɨgu assə uspeluvukussot.</ta>
            <ta e="T31" id="Seg_256" s="T26">kündɨlʼe meː andundɨ assə manǯekussot.</ta>
            <ta e="T42" id="Seg_257" s="T31">i qaːɣi Еrmolaj qwätpɨdi nʼäbam iːgu kɨnǯalǯɨ(i)s, me anduvɨt pokkun piːgəlʼeŋ.</ta>
            <ta e="T46" id="Seg_258" s="T42">i me üːtqan potšeot.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T13" id="Seg_259" s="T1">me uʒe kundoqɨn moglʼi qoldigu, što üːt me anduqɨnat okkɨni tiːrilʼe čaːčaŋ. </ta>
            <ta e="T26" id="Seg_260" s="T13">üːtannin nʼäbɨla wässakulʼlʼe ollɨtat nildʼi wärgɨ tabunɨŋ što me tʼüldiseːlawɨt apsɨtɨgu assə uspeluwukussot. </ta>
            <ta e="T31" id="Seg_261" s="T26">kündɨlʼe meː andundɨ assə manǯekussot. </ta>
            <ta e="T42" id="Seg_262" s="T31">i qaːɣi Ermolaj qwätpɨdi nʼäbam iːgu kɨnǯalǯɨs, me anduwɨt pokkun piːgəlʼeŋ. </ta>
            <ta e="T46" id="Seg_263" s="T42">i me üːtqan počeot. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_264" s="T1">me</ta>
            <ta e="T3" id="Seg_265" s="T2">uʒe</ta>
            <ta e="T4" id="Seg_266" s="T3">kundo-qən</ta>
            <ta e="T5" id="Seg_267" s="T4">moglʼi</ta>
            <ta e="T6" id="Seg_268" s="T5">qo-ldi-gu</ta>
            <ta e="T7" id="Seg_269" s="T6">što</ta>
            <ta e="T8" id="Seg_270" s="T7">üːt</ta>
            <ta e="T9" id="Seg_271" s="T8">me</ta>
            <ta e="T10" id="Seg_272" s="T9">andu-qɨnat</ta>
            <ta e="T11" id="Seg_273" s="T10">okkɨ-ni</ta>
            <ta e="T12" id="Seg_274" s="T11">tiːri-lʼe</ta>
            <ta e="T13" id="Seg_275" s="T12">čaːča-ŋ</ta>
            <ta e="T14" id="Seg_276" s="T13">üːta-n-nin</ta>
            <ta e="T15" id="Seg_277" s="T14">nʼäb-ɨ-la</ta>
            <ta e="T16" id="Seg_278" s="T15">wässa-ku-lʼlʼe</ta>
            <ta e="T17" id="Seg_279" s="T16">ollɨ-tat</ta>
            <ta e="T18" id="Seg_280" s="T17">nildʼi</ta>
            <ta e="T19" id="Seg_281" s="T18">wärgɨ</ta>
            <ta e="T20" id="Seg_282" s="T19">tabun-ɨ-ŋ</ta>
            <ta e="T21" id="Seg_283" s="T20">što</ta>
            <ta e="T22" id="Seg_284" s="T21">me</ta>
            <ta e="T23" id="Seg_285" s="T22">tʼüldi-seː-la-wɨt</ta>
            <ta e="T24" id="Seg_286" s="T23">apsɨtɨ-gu</ta>
            <ta e="T25" id="Seg_287" s="T24">assə</ta>
            <ta e="T26" id="Seg_288" s="T25">uspel-u-wu-ku-ss-ot</ta>
            <ta e="T27" id="Seg_289" s="T26">kündɨ-lʼe</ta>
            <ta e="T28" id="Seg_290" s="T27">meː</ta>
            <ta e="T29" id="Seg_291" s="T28">andu-ndɨ</ta>
            <ta e="T30" id="Seg_292" s="T29">assə</ta>
            <ta e="T31" id="Seg_293" s="T30">manǯe-ku-ss-ot</ta>
            <ta e="T32" id="Seg_294" s="T31">i</ta>
            <ta e="T33" id="Seg_295" s="T32">qaːɣi</ta>
            <ta e="T34" id="Seg_296" s="T33">Ermolaj</ta>
            <ta e="T35" id="Seg_297" s="T34">qwät-pɨdi</ta>
            <ta e="T36" id="Seg_298" s="T35">nʼäb-a-m</ta>
            <ta e="T37" id="Seg_299" s="T36">iː-gu</ta>
            <ta e="T38" id="Seg_300" s="T37">kɨnǯa-lǯɨ-s</ta>
            <ta e="T39" id="Seg_301" s="T38">me</ta>
            <ta e="T40" id="Seg_302" s="T39">andu-wɨt</ta>
            <ta e="T41" id="Seg_303" s="T40">pokku-n</ta>
            <ta e="T42" id="Seg_304" s="T41">piːgəlʼ-e-ŋ</ta>
            <ta e="T43" id="Seg_305" s="T42">i</ta>
            <ta e="T44" id="Seg_306" s="T43">me</ta>
            <ta e="T45" id="Seg_307" s="T44">üːt-qan</ta>
            <ta e="T46" id="Seg_308" s="T45">poččɨ-ot</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_309" s="T1">meː</ta>
            <ta e="T3" id="Seg_310" s="T2">uʒe</ta>
            <ta e="T4" id="Seg_311" s="T3">kundɨ-qən</ta>
            <ta e="T5" id="Seg_312" s="T4">moglʼi</ta>
            <ta e="T6" id="Seg_313" s="T5">qo-lʼčǝ-gu</ta>
            <ta e="T7" id="Seg_314" s="T6">što</ta>
            <ta e="T8" id="Seg_315" s="T7">üt</ta>
            <ta e="T9" id="Seg_316" s="T8">meː</ta>
            <ta e="T10" id="Seg_317" s="T9">andu-qɨnat</ta>
            <ta e="T11" id="Seg_318" s="T10">okkɨr-nɨ</ta>
            <ta e="T12" id="Seg_319" s="T11">tiːri-le</ta>
            <ta e="T13" id="Seg_320" s="T12">čaːǯɨ-n</ta>
            <ta e="T14" id="Seg_321" s="T13">üːdɨ-n-nan</ta>
            <ta e="T15" id="Seg_322" s="T14">nʼaːb-ɨ-la</ta>
            <ta e="T16" id="Seg_323" s="T15">waše-ku-le</ta>
            <ta e="T17" id="Seg_324" s="T16">oldǝ-tɨt</ta>
            <ta e="T18" id="Seg_325" s="T17">nɨlʼǯi</ta>
            <ta e="T19" id="Seg_326" s="T18">wargɨ</ta>
            <ta e="T20" id="Seg_327" s="T19">tabun-ɨ-n</ta>
            <ta e="T21" id="Seg_328" s="T20">što</ta>
            <ta e="T22" id="Seg_329" s="T21">meː</ta>
            <ta e="T23" id="Seg_330" s="T22">tülʼse-seː-la-wɨt</ta>
            <ta e="T24" id="Seg_331" s="T23">apsɨtɨ-gu</ta>
            <ta e="T25" id="Seg_332" s="T24">assɨ</ta>
            <ta e="T26" id="Seg_333" s="T25">uspel-ɨ-wa-ku-sɨ-ut</ta>
            <ta e="T27" id="Seg_334" s="T26">qɨndɨ-le</ta>
            <ta e="T28" id="Seg_335" s="T27">meː</ta>
            <ta e="T29" id="Seg_336" s="T28">andu-ndɨ</ta>
            <ta e="T30" id="Seg_337" s="T29">assɨ</ta>
            <ta e="T31" id="Seg_338" s="T30">manǯɨ-ku-sɨ-ut</ta>
            <ta e="T32" id="Seg_339" s="T31">i</ta>
            <ta e="T33" id="Seg_340" s="T32">qaqä</ta>
            <ta e="T34" id="Seg_341" s="T33">Jermolaj</ta>
            <ta e="T35" id="Seg_342" s="T34">qwat-mbɨdi</ta>
            <ta e="T36" id="Seg_343" s="T35">nʼaːb-ɨ-m</ta>
            <ta e="T37" id="Seg_344" s="T36">iː-gu</ta>
            <ta e="T38" id="Seg_345" s="T37">kɨnǯa-lʼčǝ-sɨ</ta>
            <ta e="T39" id="Seg_346" s="T38">meː</ta>
            <ta e="T40" id="Seg_347" s="T39">andu-wɨt</ta>
            <ta e="T41" id="Seg_348" s="T40">poku-n</ta>
            <ta e="T42" id="Seg_349" s="T41">piːgəlʼ-ɨ-n</ta>
            <ta e="T43" id="Seg_350" s="T42">i</ta>
            <ta e="T44" id="Seg_351" s="T43">meː</ta>
            <ta e="T45" id="Seg_352" s="T44">üt-qən</ta>
            <ta e="T46" id="Seg_353" s="T45">poččɨ-ut</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_354" s="T1">we.NOM</ta>
            <ta e="T3" id="Seg_355" s="T2">already</ta>
            <ta e="T4" id="Seg_356" s="T3">long-LOC</ta>
            <ta e="T5" id="Seg_357" s="T4">can</ta>
            <ta e="T6" id="Seg_358" s="T5">see-PFV-INF</ta>
            <ta e="T7" id="Seg_359" s="T6">what</ta>
            <ta e="T8" id="Seg_360" s="T7">water.[NOM]</ta>
            <ta e="T9" id="Seg_361" s="T8">we.PL.NOM</ta>
            <ta e="T10" id="Seg_362" s="T9">boat-ILL.1PL</ta>
            <ta e="T11" id="Seg_363" s="T10">one-ALL</ta>
            <ta e="T12" id="Seg_364" s="T11">fill.up-CVB</ta>
            <ta e="T13" id="Seg_365" s="T12">travel-3SG.S</ta>
            <ta e="T14" id="Seg_366" s="T13">evening-GEN-ADES</ta>
            <ta e="T15" id="Seg_367" s="T14">duck-EP-PL.[NOM]</ta>
            <ta e="T16" id="Seg_368" s="T15">fly-HAB-CVB</ta>
            <ta e="T17" id="Seg_369" s="T16">begin-3PL</ta>
            <ta e="T18" id="Seg_370" s="T17">such</ta>
            <ta e="T19" id="Seg_371" s="T18">big</ta>
            <ta e="T20" id="Seg_372" s="T19">herd-EP-3SG.S</ta>
            <ta e="T21" id="Seg_373" s="T20">what</ta>
            <ta e="T22" id="Seg_374" s="T21">we.NOM</ta>
            <ta e="T23" id="Seg_375" s="T22">rifle-%%-PL-1PL</ta>
            <ta e="T24" id="Seg_376" s="T23">feed-INF</ta>
            <ta e="T25" id="Seg_377" s="T24">NEG</ta>
            <ta e="T26" id="Seg_378" s="T25">be.in.time-EP-DRV-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_379" s="T26">shoot-CVB</ta>
            <ta e="T28" id="Seg_380" s="T27">we.NOM</ta>
            <ta e="T29" id="Seg_381" s="T28">boat-ILL</ta>
            <ta e="T30" id="Seg_382" s="T29">NEG</ta>
            <ta e="T31" id="Seg_383" s="T30">look-HAB-PST-1PL</ta>
            <ta e="T32" id="Seg_384" s="T31">and</ta>
            <ta e="T33" id="Seg_385" s="T32">when</ta>
            <ta e="T34" id="Seg_386" s="T33">Yermolaj.[NOM]</ta>
            <ta e="T35" id="Seg_387" s="T34">kill-PTCP.PST</ta>
            <ta e="T36" id="Seg_388" s="T35">duck-EP-ACC</ta>
            <ta e="T37" id="Seg_389" s="T36">take-INF</ta>
            <ta e="T38" id="Seg_390" s="T37">stretch-PFV-PST.[3SG.S]</ta>
            <ta e="T39" id="Seg_391" s="T38">we.PL.GEN</ta>
            <ta e="T40" id="Seg_392" s="T39">boat-1PL</ta>
            <ta e="T41" id="Seg_393" s="T40">side-INSTR2</ta>
            <ta e="T42" id="Seg_394" s="T41">turn.around-EP-3SG.S</ta>
            <ta e="T43" id="Seg_395" s="T42">and</ta>
            <ta e="T44" id="Seg_396" s="T43">we.DU.NOM</ta>
            <ta e="T45" id="Seg_397" s="T44">water-LOC</ta>
            <ta e="T46" id="Seg_398" s="T45">recover-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_399" s="T1">мы.NOM</ta>
            <ta e="T3" id="Seg_400" s="T2">уже</ta>
            <ta e="T4" id="Seg_401" s="T3">долго-LOC</ta>
            <ta e="T5" id="Seg_402" s="T4">могли</ta>
            <ta e="T6" id="Seg_403" s="T5">увидеть-PFV-INF</ta>
            <ta e="T7" id="Seg_404" s="T6">что</ta>
            <ta e="T8" id="Seg_405" s="T7">вода.[NOM]</ta>
            <ta e="T9" id="Seg_406" s="T8">мы.PL.NOM</ta>
            <ta e="T10" id="Seg_407" s="T9">обласок-ILL.1PL</ta>
            <ta e="T11" id="Seg_408" s="T10">один-ALL</ta>
            <ta e="T12" id="Seg_409" s="T11">наполниться-CVB</ta>
            <ta e="T13" id="Seg_410" s="T12">ехать-3SG.S</ta>
            <ta e="T14" id="Seg_411" s="T13">вечер-GEN-ADES</ta>
            <ta e="T15" id="Seg_412" s="T14">утка-EP-PL.[NOM]</ta>
            <ta e="T16" id="Seg_413" s="T15">летать-HAB-CVB</ta>
            <ta e="T17" id="Seg_414" s="T16">начать-3PL</ta>
            <ta e="T18" id="Seg_415" s="T17">такой</ta>
            <ta e="T19" id="Seg_416" s="T18">большой</ta>
            <ta e="T20" id="Seg_417" s="T19">табун-EP-3SG.S</ta>
            <ta e="T21" id="Seg_418" s="T20">что</ta>
            <ta e="T22" id="Seg_419" s="T21">мы.NOM</ta>
            <ta e="T23" id="Seg_420" s="T22">ружье-%%-PL-1PL</ta>
            <ta e="T24" id="Seg_421" s="T23">накормить-INF</ta>
            <ta e="T25" id="Seg_422" s="T24">NEG</ta>
            <ta e="T26" id="Seg_423" s="T25">успеть-EP-DRV-HAB-PST-1PL</ta>
            <ta e="T27" id="Seg_424" s="T26">стрелять-CVB</ta>
            <ta e="T28" id="Seg_425" s="T27">мы.NOM</ta>
            <ta e="T29" id="Seg_426" s="T28">обласок-ILL</ta>
            <ta e="T30" id="Seg_427" s="T29">NEG</ta>
            <ta e="T31" id="Seg_428" s="T30">смотреть-HAB-PST-1PL</ta>
            <ta e="T32" id="Seg_429" s="T31">и</ta>
            <ta e="T33" id="Seg_430" s="T32">когда</ta>
            <ta e="T34" id="Seg_431" s="T33">Ермолай.[NOM]</ta>
            <ta e="T35" id="Seg_432" s="T34">убить-PTCP.PST</ta>
            <ta e="T36" id="Seg_433" s="T35">утка-EP-ACC</ta>
            <ta e="T37" id="Seg_434" s="T36">взять-INF</ta>
            <ta e="T38" id="Seg_435" s="T37">потянуться-PFV-PST.[3SG.S]</ta>
            <ta e="T39" id="Seg_436" s="T38">мы.PL.GEN</ta>
            <ta e="T40" id="Seg_437" s="T39">обласок-1PL</ta>
            <ta e="T41" id="Seg_438" s="T40">бок-INSTR2</ta>
            <ta e="T42" id="Seg_439" s="T41">повернуться-EP-3SG.S</ta>
            <ta e="T43" id="Seg_440" s="T42">и</ta>
            <ta e="T44" id="Seg_441" s="T43">мы.DU.NOM</ta>
            <ta e="T45" id="Seg_442" s="T44">вода-LOC</ta>
            <ta e="T46" id="Seg_443" s="T45">прийти.в.себя-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_444" s="T1">pers</ta>
            <ta e="T3" id="Seg_445" s="T2">adv</ta>
            <ta e="T4" id="Seg_446" s="T3">adv-n:case</ta>
            <ta e="T5" id="Seg_447" s="T4">v</ta>
            <ta e="T6" id="Seg_448" s="T5">v-v&gt;v-v:inf</ta>
            <ta e="T7" id="Seg_449" s="T6">conj</ta>
            <ta e="T8" id="Seg_450" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_451" s="T8">pers</ta>
            <ta e="T10" id="Seg_452" s="T9">n-n:case.poss</ta>
            <ta e="T11" id="Seg_453" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_454" s="T11">v-v&gt;adv</ta>
            <ta e="T13" id="Seg_455" s="T12">v-v:pn</ta>
            <ta e="T14" id="Seg_456" s="T13">n-n:case-n:case</ta>
            <ta e="T15" id="Seg_457" s="T14">n-n:ins-n:num.[n:case]</ta>
            <ta e="T16" id="Seg_458" s="T15">v-v&gt;v-v&gt;adv</ta>
            <ta e="T17" id="Seg_459" s="T16">v-v:pn</ta>
            <ta e="T18" id="Seg_460" s="T17">dem</ta>
            <ta e="T19" id="Seg_461" s="T18">adj</ta>
            <ta e="T20" id="Seg_462" s="T19">n-n:ins-v:pn</ta>
            <ta e="T21" id="Seg_463" s="T20">conj</ta>
            <ta e="T22" id="Seg_464" s="T21">pers</ta>
            <ta e="T23" id="Seg_465" s="T22">n-n&gt;n-n:num-n:poss</ta>
            <ta e="T24" id="Seg_466" s="T23">v-v:inf</ta>
            <ta e="T25" id="Seg_467" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_468" s="T25">v-n:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_469" s="T26">v-v&gt;adv</ta>
            <ta e="T28" id="Seg_470" s="T27">pers</ta>
            <ta e="T29" id="Seg_471" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_472" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_473" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_474" s="T31">conj</ta>
            <ta e="T33" id="Seg_475" s="T32">conj</ta>
            <ta e="T34" id="Seg_476" s="T33">nprop.[n:case]</ta>
            <ta e="T35" id="Seg_477" s="T34">v-v&gt;ptcp</ta>
            <ta e="T36" id="Seg_478" s="T35">n-n:ins-n:case</ta>
            <ta e="T37" id="Seg_479" s="T36">v-v:inf</ta>
            <ta e="T38" id="Seg_480" s="T37">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_481" s="T38">pers</ta>
            <ta e="T40" id="Seg_482" s="T39">n-n:poss</ta>
            <ta e="T41" id="Seg_483" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_484" s="T41">v-v:ins-v:pn</ta>
            <ta e="T43" id="Seg_485" s="T42">conj</ta>
            <ta e="T44" id="Seg_486" s="T43">pers</ta>
            <ta e="T45" id="Seg_487" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_488" s="T45">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_489" s="T1">pers</ta>
            <ta e="T3" id="Seg_490" s="T2">adv</ta>
            <ta e="T4" id="Seg_491" s="T3">adv</ta>
            <ta e="T5" id="Seg_492" s="T4">v</ta>
            <ta e="T6" id="Seg_493" s="T5">v</ta>
            <ta e="T7" id="Seg_494" s="T6">conj</ta>
            <ta e="T8" id="Seg_495" s="T7">n</ta>
            <ta e="T9" id="Seg_496" s="T8">pers</ta>
            <ta e="T10" id="Seg_497" s="T9">n</ta>
            <ta e="T11" id="Seg_498" s="T10">num</ta>
            <ta e="T12" id="Seg_499" s="T11">adv</ta>
            <ta e="T13" id="Seg_500" s="T12">v</ta>
            <ta e="T14" id="Seg_501" s="T13">n</ta>
            <ta e="T15" id="Seg_502" s="T14">n</ta>
            <ta e="T16" id="Seg_503" s="T15">adv</ta>
            <ta e="T17" id="Seg_504" s="T16">v</ta>
            <ta e="T18" id="Seg_505" s="T17">dem</ta>
            <ta e="T19" id="Seg_506" s="T18">adj</ta>
            <ta e="T20" id="Seg_507" s="T19">n</ta>
            <ta e="T21" id="Seg_508" s="T20">conj</ta>
            <ta e="T22" id="Seg_509" s="T21">pers</ta>
            <ta e="T23" id="Seg_510" s="T22">n</ta>
            <ta e="T24" id="Seg_511" s="T23">v</ta>
            <ta e="T25" id="Seg_512" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_513" s="T25">v</ta>
            <ta e="T27" id="Seg_514" s="T26">adv</ta>
            <ta e="T28" id="Seg_515" s="T27">pers</ta>
            <ta e="T29" id="Seg_516" s="T28">n</ta>
            <ta e="T30" id="Seg_517" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_518" s="T30">v</ta>
            <ta e="T32" id="Seg_519" s="T31">conj</ta>
            <ta e="T33" id="Seg_520" s="T32">interrog</ta>
            <ta e="T34" id="Seg_521" s="T33">nprop</ta>
            <ta e="T35" id="Seg_522" s="T34">ptcp</ta>
            <ta e="T36" id="Seg_523" s="T35">n</ta>
            <ta e="T37" id="Seg_524" s="T36">v</ta>
            <ta e="T38" id="Seg_525" s="T37">v</ta>
            <ta e="T39" id="Seg_526" s="T38">pers</ta>
            <ta e="T40" id="Seg_527" s="T39">n</ta>
            <ta e="T41" id="Seg_528" s="T40">n</ta>
            <ta e="T42" id="Seg_529" s="T41">v</ta>
            <ta e="T43" id="Seg_530" s="T42">conj</ta>
            <ta e="T44" id="Seg_531" s="T43">pers</ta>
            <ta e="T45" id="Seg_532" s="T44">n</ta>
            <ta e="T46" id="Seg_533" s="T45">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_534" s="T1">pro.h:E</ta>
            <ta e="T4" id="Seg_535" s="T3">adv:Time</ta>
            <ta e="T6" id="Seg_536" s="T5">v:Th</ta>
            <ta e="T8" id="Seg_537" s="T7">np:A</ta>
            <ta e="T9" id="Seg_538" s="T8">pro.h:Poss</ta>
            <ta e="T10" id="Seg_539" s="T9">np:G</ta>
            <ta e="T14" id="Seg_540" s="T13">np:Time</ta>
            <ta e="T15" id="Seg_541" s="T14">np:A</ta>
            <ta e="T20" id="Seg_542" s="T19">np:Th 0.3:Poss</ta>
            <ta e="T22" id="Seg_543" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_544" s="T22">np:Th 0.1.h:Poss</ta>
            <ta e="T28" id="Seg_545" s="T27">pro.h:A</ta>
            <ta e="T29" id="Seg_546" s="T28">np:G</ta>
            <ta e="T34" id="Seg_547" s="T33">np.h:A</ta>
            <ta e="T36" id="Seg_548" s="T35">np:Th</ta>
            <ta e="T39" id="Seg_549" s="T38">pro.h:Poss</ta>
            <ta e="T40" id="Seg_550" s="T39">np:P</ta>
            <ta e="T44" id="Seg_551" s="T43">pro.h:Th</ta>
            <ta e="T45" id="Seg_552" s="T44">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_553" s="T1">pro.h:S</ta>
            <ta e="T5" id="Seg_554" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_555" s="T5">v:O</ta>
            <ta e="T13" id="Seg_556" s="T6">s:compl</ta>
            <ta e="T15" id="Seg_557" s="T14">np:S</ta>
            <ta e="T17" id="Seg_558" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_559" s="T19">np:S</ta>
            <ta e="T22" id="Seg_560" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_561" s="T22">s:purp</ta>
            <ta e="T26" id="Seg_562" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_563" s="T26">s:temp</ta>
            <ta e="T28" id="Seg_564" s="T27">pro.h:S</ta>
            <ta e="T31" id="Seg_565" s="T30">v:pred</ta>
            <ta e="T38" id="Seg_566" s="T31">s:temp</ta>
            <ta e="T40" id="Seg_567" s="T39">np:S</ta>
            <ta e="T42" id="Seg_568" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_569" s="T43">pro.h:S</ta>
            <ta e="T46" id="Seg_570" s="T45">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_571" s="T2">RUS:core</ta>
            <ta e="T5" id="Seg_572" s="T4">RUS:core</ta>
            <ta e="T7" id="Seg_573" s="T6">RUS:gram</ta>
            <ta e="T21" id="Seg_574" s="T20">RUS:gram</ta>
            <ta e="T32" id="Seg_575" s="T31">RUS:gram</ta>
            <ta e="T34" id="Seg_576" s="T33">RUS:cult</ta>
            <ta e="T41" id="Seg_577" s="T40">RUS:cult</ta>
            <ta e="T43" id="Seg_578" s="T42">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T13" id="Seg_579" s="T1">We could have already noticed some time ago, that our boat leaked.</ta>
            <ta e="T26" id="Seg_580" s="T13">In the evening the ducks were flying in such big flocks, that we could hardly manage to reload our guns.</ta>
            <ta e="T31" id="Seg_581" s="T26">We didn't look at the boat while shooting.</ta>
            <ta e="T42" id="Seg_582" s="T31">When Yermolaj reached for a killed duck, our boat turned over.</ta>
            <ta e="T46" id="Seg_583" s="T42">We found ourselves in water.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T13" id="Seg_584" s="T1">Wir hätten schon lange bemerken können, dass unser Boot leckte.</ta>
            <ta e="T26" id="Seg_585" s="T13">Am Abend flogen die Enten in so großen Schwärmen, dass wir kaum dazu kamen, unsere Gewehre nachzuladen.</ta>
            <ta e="T31" id="Seg_586" s="T26">Wir achteten nicht auf das Boot während dem Schießen.</ta>
            <ta e="T42" id="Seg_587" s="T31">Als Jermolaj sich nach einer getöteten Ente streckte, kenterte unser Boot.</ta>
            <ta e="T46" id="Seg_588" s="T42">Wir fanden uns im Wasser wieder.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T13" id="Seg_589" s="T1">Мы уже давно могли заметить, что в нашу лодку все время подтекает вода.</ta>
            <ta e="T26" id="Seg_590" s="T13">К вечеру утки подниматься начали такими большими стаями, что мы ружья заряжать не успевали.</ta>
            <ta e="T31" id="Seg_591" s="T26">Стреляя, мы на лодку не смотрели.</ta>
            <ta e="T42" id="Seg_592" s="T31">Когда Ермолай потянулся, чтобы взять убитую утку, наша лодка перевернулась на бок.</ta>
            <ta e="T46" id="Seg_593" s="T42">Мы очутились в воде.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T13" id="Seg_594" s="T1">мы уже давно могли увидеть вода в нашу лодку все время наполняется идет</ta>
            <ta e="T26" id="Seg_595" s="T13">к вечеру утки подниматься начали таким большим табуном что мы ружья заряжать не успевали</ta>
            <ta e="T31" id="Seg_596" s="T26">стреляя мы на лодку не смотрели</ta>
            <ta e="T42" id="Seg_597" s="T31">когда убитую утку взять потянулся наша лодка боком повернулась</ta>
            <ta e="T46" id="Seg_598" s="T42">мы в воде очутились</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
