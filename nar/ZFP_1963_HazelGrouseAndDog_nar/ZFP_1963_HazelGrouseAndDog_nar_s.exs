<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ZFP_1963_HazelGrouseAndDog_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ZFP_1963_HazelGrouseAndDog_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">66</ud-information>
            <ud-information attribute-name="# e">66</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ZFP">
            <abbreviation>ZFP</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ZFP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T67" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">čaːčisaŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">qottä</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">peːmambilʼiwlʼe</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kənakam</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">üːtimbis</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">nʼärnännɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kənaŋmɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">posiŋ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">uttəreɣan</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">nännɨ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">qolǯilʼe</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">qwannɨ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">qaːinta</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">aːptam</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">iːlʼewlʼe</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_62" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">man</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">manǯendaŋ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">nʼärnä</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">wättondə</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">i</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">qoldʼäm</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">nʼünʼüka</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">pekʼkʼejam</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_89" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">täp</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">aːmtis</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">wättan</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">qɨːgan</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">i</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">kutʼäna</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">assə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">kəːdiŋ</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_116" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">man</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">kənaŋmɨ</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">erukkaŋ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_127" n="HIAT:w" s="T36">täpanni</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_130" n="HIAT:w" s="T37">miːttɨlʼlʼe</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_133" n="HIAT:w" s="T38">čaːčas</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_137" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_139" n="HIAT:w" s="T39">poːsiŋ</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_142" n="HIAT:w" s="T40">wärgə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_145" n="HIAT:w" s="T41">pekʼkʼe</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">kutʼita</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_151" n="HIAT:w" s="T43">poːɣannɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_154" n="HIAT:w" s="T44">wassälʼewlʼe</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_157" n="HIAT:w" s="T45">omdalʼdiŋ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_160" n="HIAT:w" s="T46">kənnan</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_163" n="HIAT:w" s="T47">qaːttɨ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_166" n="HIAT:w" s="T48">i</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_169" n="HIAT:w" s="T49">čupalamdə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_172" n="HIAT:w" s="T50">wärlolǯɨlʼe</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_175" n="HIAT:w" s="T51">šɨtaŋ</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_178" n="HIAT:w" s="T52">qaj</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_181" n="HIAT:w" s="T53">paqtaŋ</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_184" n="HIAT:w" s="T54">kəːnnanni</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_188" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_190" n="HIAT:w" s="T55">man</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_193" n="HIAT:w" s="T56">kənnaŋmɨ</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_196" n="HIAT:w" s="T57">uttɨrɨŋ</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_199" n="HIAT:w" s="T58">i</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_202" n="HIAT:w" s="T59">qottä</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_205" n="HIAT:w" s="T60">piːgalǯiŋ</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_209" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">man</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_214" n="HIAT:w" s="T62">kwäram</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_217" n="HIAT:w" s="T63">kənnaŋmɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_220" n="HIAT:w" s="T64">i</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">qwannaŋ</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_226" n="HIAT:w" s="T66">maːttə</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T67" id="Seg_229" n="sc" s="T1">
               <ts e="T2" id="Seg_231" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_233" n="e" s="T2">čaːčisaŋ </ts>
               <ts e="T4" id="Seg_235" n="e" s="T3">qottä </ts>
               <ts e="T5" id="Seg_237" n="e" s="T4">peːmambilʼiwlʼe. </ts>
               <ts e="T6" id="Seg_239" n="e" s="T5">kənakam </ts>
               <ts e="T7" id="Seg_241" n="e" s="T6">üːtimbis </ts>
               <ts e="T8" id="Seg_243" n="e" s="T7">nʼärnännɨ. </ts>
               <ts e="T9" id="Seg_245" n="e" s="T8">kənaŋmɨ </ts>
               <ts e="T10" id="Seg_247" n="e" s="T9">posiŋ </ts>
               <ts e="T11" id="Seg_249" n="e" s="T10">uttəreɣan. </ts>
               <ts e="T12" id="Seg_251" n="e" s="T11">nännɨ </ts>
               <ts e="T13" id="Seg_253" n="e" s="T12">qolǯilʼe </ts>
               <ts e="T14" id="Seg_255" n="e" s="T13">qwannɨ </ts>
               <ts e="T15" id="Seg_257" n="e" s="T14">qaːinta </ts>
               <ts e="T16" id="Seg_259" n="e" s="T15">aːptam </ts>
               <ts e="T17" id="Seg_261" n="e" s="T16">iːlʼewlʼe. </ts>
               <ts e="T18" id="Seg_263" n="e" s="T17">man </ts>
               <ts e="T19" id="Seg_265" n="e" s="T18">manǯendaŋ </ts>
               <ts e="T20" id="Seg_267" n="e" s="T19">nʼärnä </ts>
               <ts e="T21" id="Seg_269" n="e" s="T20">wättondə </ts>
               <ts e="T22" id="Seg_271" n="e" s="T21">i </ts>
               <ts e="T23" id="Seg_273" n="e" s="T22">qoldʼäm </ts>
               <ts e="T24" id="Seg_275" n="e" s="T23">nʼünʼüka </ts>
               <ts e="T25" id="Seg_277" n="e" s="T24">pekʼkʼejam. </ts>
               <ts e="T26" id="Seg_279" n="e" s="T25">täp </ts>
               <ts e="T27" id="Seg_281" n="e" s="T26">aːmtis </ts>
               <ts e="T28" id="Seg_283" n="e" s="T27">wättan </ts>
               <ts e="T29" id="Seg_285" n="e" s="T28">qɨːgan </ts>
               <ts e="T30" id="Seg_287" n="e" s="T29">i </ts>
               <ts e="T31" id="Seg_289" n="e" s="T30">kutʼäna </ts>
               <ts e="T32" id="Seg_291" n="e" s="T31">assə </ts>
               <ts e="T33" id="Seg_293" n="e" s="T32">kəːdiŋ. </ts>
               <ts e="T34" id="Seg_295" n="e" s="T33">man </ts>
               <ts e="T35" id="Seg_297" n="e" s="T34">kənaŋmɨ </ts>
               <ts e="T36" id="Seg_299" n="e" s="T35">erukkaŋ </ts>
               <ts e="T37" id="Seg_301" n="e" s="T36">täpanni </ts>
               <ts e="T38" id="Seg_303" n="e" s="T37">miːttɨlʼlʼe </ts>
               <ts e="T39" id="Seg_305" n="e" s="T38">čaːčas. </ts>
               <ts e="T40" id="Seg_307" n="e" s="T39">poːsiŋ </ts>
               <ts e="T41" id="Seg_309" n="e" s="T40">wärgə </ts>
               <ts e="T42" id="Seg_311" n="e" s="T41">pekʼkʼe </ts>
               <ts e="T43" id="Seg_313" n="e" s="T42">kutʼita </ts>
               <ts e="T44" id="Seg_315" n="e" s="T43">poːɣannɨ </ts>
               <ts e="T45" id="Seg_317" n="e" s="T44">wassälʼewlʼe </ts>
               <ts e="T46" id="Seg_319" n="e" s="T45">omdalʼdiŋ </ts>
               <ts e="T47" id="Seg_321" n="e" s="T46">kənnan </ts>
               <ts e="T48" id="Seg_323" n="e" s="T47">qaːttɨ </ts>
               <ts e="T49" id="Seg_325" n="e" s="T48">i </ts>
               <ts e="T50" id="Seg_327" n="e" s="T49">čupalamdə </ts>
               <ts e="T51" id="Seg_329" n="e" s="T50">wärlolǯɨlʼe </ts>
               <ts e="T52" id="Seg_331" n="e" s="T51">šɨtaŋ </ts>
               <ts e="T53" id="Seg_333" n="e" s="T52">qaj </ts>
               <ts e="T54" id="Seg_335" n="e" s="T53">paqtaŋ </ts>
               <ts e="T55" id="Seg_337" n="e" s="T54">kəːnnanni. </ts>
               <ts e="T56" id="Seg_339" n="e" s="T55">man </ts>
               <ts e="T57" id="Seg_341" n="e" s="T56">kənnaŋmɨ </ts>
               <ts e="T58" id="Seg_343" n="e" s="T57">uttɨrɨŋ </ts>
               <ts e="T59" id="Seg_345" n="e" s="T58">i </ts>
               <ts e="T60" id="Seg_347" n="e" s="T59">qottä </ts>
               <ts e="T61" id="Seg_349" n="e" s="T60">piːgalǯiŋ. </ts>
               <ts e="T62" id="Seg_351" n="e" s="T61">man </ts>
               <ts e="T63" id="Seg_353" n="e" s="T62">kwäram </ts>
               <ts e="T64" id="Seg_355" n="e" s="T63">kənnaŋmɨ </ts>
               <ts e="T65" id="Seg_357" n="e" s="T64">i </ts>
               <ts e="T66" id="Seg_359" n="e" s="T65">qwannaŋ </ts>
               <ts e="T67" id="Seg_361" n="e" s="T66">maːttə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_362" s="T1">ZFP_1963_HazelGrouseAndDog_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_363" s="T5">ZFP_1963_HazelGrouseAndDog_nar.002 (001.002)</ta>
            <ta e="T11" id="Seg_364" s="T8">ZFP_1963_HazelGrouseAndDog_nar.003 (001.003)</ta>
            <ta e="T17" id="Seg_365" s="T11">ZFP_1963_HazelGrouseAndDog_nar.004 (001.004)</ta>
            <ta e="T25" id="Seg_366" s="T17">ZFP_1963_HazelGrouseAndDog_nar.005 (001.005)</ta>
            <ta e="T33" id="Seg_367" s="T25">ZFP_1963_HazelGrouseAndDog_nar.006 (001.006)</ta>
            <ta e="T39" id="Seg_368" s="T33">ZFP_1963_HazelGrouseAndDog_nar.007 (001.007)</ta>
            <ta e="T55" id="Seg_369" s="T39">ZFP_1963_HazelGrouseAndDog_nar.008 (001.008)</ta>
            <ta e="T61" id="Seg_370" s="T55">ZFP_1963_HazelGrouseAndDog_nar.009 (001.009)</ta>
            <ta e="T67" id="Seg_371" s="T61">ZFP_1963_HazelGrouseAndDog_nar.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_372" s="T1">ман ′тша̄тшис(з)аң kо′ттӓ ′пе̄мамбилʼивлʼе.</ta>
            <ta e="T8" id="Seg_373" s="T5">′къ̊на‵кам ′ӱ̄тимбис нʼӓр′нӓнны.</ta>
            <ta e="T11" id="Seg_374" s="T8">‵къ̊′наңмы по′сиң ‵уттъ′ре̨ɣан.</ta>
            <ta e="T17" id="Seg_375" s="T11">′нӓнны ′kоlджилʼе ′kwанны ′kа̄инта ′а̄птам ′ӣлʼевлʼе.</ta>
            <ta e="T25" id="Seg_376" s="T17">ман ман′джендаң нʼӓ′рнӓ вӓ′ттондъ и ′kоlдʼӓм ′нʼӱнʼӱка ′пекʼкʼе‵jам.</ta>
            <ta e="T33" id="Seg_377" s="T25">′тӓп ′а̄мтис вӓ′ттан ′kы̄г̂ан и ку′тʼӓ на ′ассъ къ̊̄′диң.</ta>
            <ta e="T39" id="Seg_378" s="T33">ман къ̊′наңмы е̨ру′ккаң ′тӓпанни ′мӣтты‵лʼлʼе ′тша̄тшас.</ta>
            <ta e="T55" id="Seg_379" s="T39">по̄′сиң ′вӓргъ ′пекʼкʼе ку′тʼита ′по̄ɣанны вассӓ′лʼевлʼе ′омд̂(т)алʼдиң къ̊′ннан ′kа̄тты и ‵тшупа′lамдъ вӓр′lолджылʼ(лʼ)е ′шытаң kай ′паkтаң къ̊̄′ннанни.</ta>
            <ta e="T61" id="Seg_380" s="T55">ман къ̊′ннаңмы ′уттырың и kоттӓ ′пӣгалджиң.</ta>
            <ta e="T67" id="Seg_381" s="T61">ман ′кwӓрам къ′ннаңмы и kwа′ннаң ′ма̄ттъ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_382" s="T1">man tšaːtšis(z)aŋ qottä peːmambilʼivlʼe.</ta>
            <ta e="T8" id="Seg_383" s="T5">kənakam üːtimbis nʼärnännɨ.</ta>
            <ta e="T11" id="Seg_384" s="T8">kənaŋmɨ posiŋ uttəreɣan.</ta>
            <ta e="T17" id="Seg_385" s="T11">nännɨ qolǯilʼe qwannɨ qaːinta aːptam iːlʼevlʼe.</ta>
            <ta e="T25" id="Seg_386" s="T17">man manǯendaŋ nʼärnä vättondə i qoldʼäm nʼünʼüka pekʼkʼejam.</ta>
            <ta e="T33" id="Seg_387" s="T25">täp aːmtis vättan qɨːĝan i kutʼä na assə kəːdiŋ.</ta>
            <ta e="T39" id="Seg_388" s="T33">man kənaŋmɨ erukkaŋ täpanni miːttɨlʼlʼe tšaːtšas.</ta>
            <ta e="T55" id="Seg_389" s="T39">poːsiŋ värgə pekʼkʼe kutʼita poːɣannɨ vassälʼevlʼe omd̂(t)alʼdiŋ kənnan qaːttɨ i tšupalamdə värlolǯɨlʼ(lʼ)e šɨtaŋ qaj paqtaŋ kəːnnanni.</ta>
            <ta e="T61" id="Seg_390" s="T55">man kənnaŋmɨ uttɨrɨŋ i qottä piːgalǯiŋ.</ta>
            <ta e="T67" id="Seg_391" s="T61">man kwäram kənnaŋmɨ i qwannaŋ maːttə.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_392" s="T1">man čaːčisaŋ qottä peːmambilʼiwlʼe. </ta>
            <ta e="T8" id="Seg_393" s="T5">kənakam üːtimbis nʼärnännɨ. </ta>
            <ta e="T11" id="Seg_394" s="T8">kənaŋmɨ posiŋ uttəreɣan. </ta>
            <ta e="T17" id="Seg_395" s="T11">nännɨ qolǯilʼe qwannɨ qaːinta aːptam iːlʼewlʼe. </ta>
            <ta e="T25" id="Seg_396" s="T17">man manǯendaŋ nʼärnä wättondə i qoldʼäm nʼünʼüka pekʼkʼejam. </ta>
            <ta e="T33" id="Seg_397" s="T25">täp aːmtis wättan qɨːgan i kutʼäna assə kəːdiŋ. </ta>
            <ta e="T39" id="Seg_398" s="T33">man kənaŋmɨ erukkaŋ täpanni miːttɨlʼlʼe čaːčas. </ta>
            <ta e="T55" id="Seg_399" s="T39">poːsiŋ wärgə pekʼkʼe kutʼita poːɣannɨ wassälʼewlʼe omdalʼdiŋ kənnan qaːttɨ i čupalamdə wärlolǯɨlʼe šɨtaŋ qaj paqtaŋ kəːnnanni. </ta>
            <ta e="T61" id="Seg_400" s="T55">man kənnaŋmɨ uttɨrɨŋ i qottä piːgalǯiŋ. </ta>
            <ta e="T67" id="Seg_401" s="T61">man kwäram kənnaŋmɨ i qwannaŋ maːttə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_402" s="T1">man</ta>
            <ta e="T3" id="Seg_403" s="T2">čaːči-sa-ŋ</ta>
            <ta e="T4" id="Seg_404" s="T3">qottä</ta>
            <ta e="T5" id="Seg_405" s="T4">peːma-mbi-lʼi-wlʼe</ta>
            <ta e="T6" id="Seg_406" s="T5">kənak-a-m</ta>
            <ta e="T7" id="Seg_407" s="T6">üːti-mbi-s</ta>
            <ta e="T8" id="Seg_408" s="T7">nʼärnä-n-nɨ</ta>
            <ta e="T9" id="Seg_409" s="T8">kənaŋ-mɨ</ta>
            <ta e="T10" id="Seg_410" s="T9">posiŋ</ta>
            <ta e="T11" id="Seg_411" s="T10">uttəre-ɣa-n</ta>
            <ta e="T12" id="Seg_412" s="T11">nännɨ</ta>
            <ta e="T13" id="Seg_413" s="T12">qolǯi-lʼe</ta>
            <ta e="T14" id="Seg_414" s="T13">qwan-nɨ</ta>
            <ta e="T15" id="Seg_415" s="T14">qaːi-n-ta</ta>
            <ta e="T16" id="Seg_416" s="T15">aːpta-m</ta>
            <ta e="T17" id="Seg_417" s="T16">iː-lʼewlʼe</ta>
            <ta e="T18" id="Seg_418" s="T17">man</ta>
            <ta e="T19" id="Seg_419" s="T18">manǯe-nda-ŋ</ta>
            <ta e="T20" id="Seg_420" s="T19">nʼärnä</ta>
            <ta e="T21" id="Seg_421" s="T20">wätto-ndə</ta>
            <ta e="T22" id="Seg_422" s="T21">i</ta>
            <ta e="T23" id="Seg_423" s="T22">qo-ldʼä-m</ta>
            <ta e="T24" id="Seg_424" s="T23">nʼünʼü-ka</ta>
            <ta e="T25" id="Seg_425" s="T24">pekʼkʼe-ja-m</ta>
            <ta e="T26" id="Seg_426" s="T25">täp</ta>
            <ta e="T27" id="Seg_427" s="T26">aːmti-s</ta>
            <ta e="T28" id="Seg_428" s="T27">wätta-n</ta>
            <ta e="T29" id="Seg_429" s="T28">qɨː-gan</ta>
            <ta e="T30" id="Seg_430" s="T29">i</ta>
            <ta e="T31" id="Seg_431" s="T30">kutʼä-na</ta>
            <ta e="T32" id="Seg_432" s="T31">assə</ta>
            <ta e="T33" id="Seg_433" s="T32">kəːdi-ŋ</ta>
            <ta e="T34" id="Seg_434" s="T33">man</ta>
            <ta e="T35" id="Seg_435" s="T34">kənaŋ-mɨ</ta>
            <ta e="T36" id="Seg_436" s="T35">eru-kka-ŋ</ta>
            <ta e="T37" id="Seg_437" s="T36">täp-a-n-ni</ta>
            <ta e="T38" id="Seg_438" s="T37">miːttɨ-lʼlʼe</ta>
            <ta e="T39" id="Seg_439" s="T38">čaːča-s</ta>
            <ta e="T40" id="Seg_440" s="T39">poːsiŋ</ta>
            <ta e="T41" id="Seg_441" s="T40">wärgə</ta>
            <ta e="T42" id="Seg_442" s="T41">pekʼkʼe</ta>
            <ta e="T43" id="Seg_443" s="T42">kutʼi-ta</ta>
            <ta e="T44" id="Seg_444" s="T43">poː-ɣannɨ</ta>
            <ta e="T45" id="Seg_445" s="T44">wassä-lʼewlʼe</ta>
            <ta e="T46" id="Seg_446" s="T45">omda-lʼdi-ŋ</ta>
            <ta e="T47" id="Seg_447" s="T46">kənna-n</ta>
            <ta e="T48" id="Seg_448" s="T47">qaːt-tɨ</ta>
            <ta e="T49" id="Seg_449" s="T48">i</ta>
            <ta e="T50" id="Seg_450" s="T49">čupa-la-m-də</ta>
            <ta e="T51" id="Seg_451" s="T50">wärlo-lǯɨ-lʼe</ta>
            <ta e="T52" id="Seg_452" s="T51">šɨta-ŋ</ta>
            <ta e="T53" id="Seg_453" s="T52">qaj</ta>
            <ta e="T54" id="Seg_454" s="T53">paqta-ŋ</ta>
            <ta e="T55" id="Seg_455" s="T54">kəːnna-n-ni</ta>
            <ta e="T56" id="Seg_456" s="T55">man</ta>
            <ta e="T57" id="Seg_457" s="T56">kənnaŋ-mɨ</ta>
            <ta e="T58" id="Seg_458" s="T57">uttɨrɨ-ŋ</ta>
            <ta e="T59" id="Seg_459" s="T58">i</ta>
            <ta e="T60" id="Seg_460" s="T59">qottä</ta>
            <ta e="T61" id="Seg_461" s="T60">piːgal-ǯi-ŋ</ta>
            <ta e="T62" id="Seg_462" s="T61">man</ta>
            <ta e="T63" id="Seg_463" s="T62">kwära-m</ta>
            <ta e="T64" id="Seg_464" s="T63">kənnaŋ-mɨ</ta>
            <ta e="T65" id="Seg_465" s="T64">i</ta>
            <ta e="T66" id="Seg_466" s="T65">qwan-na-ŋ</ta>
            <ta e="T67" id="Seg_467" s="T66">maːt-tə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_468" s="T1">man</ta>
            <ta e="T3" id="Seg_469" s="T2">čaːǯɨ-sɨ-ŋ</ta>
            <ta e="T4" id="Seg_470" s="T3">qotä</ta>
            <ta e="T5" id="Seg_471" s="T4">pemɨ-mbɨ-lɨ-lewle</ta>
            <ta e="T6" id="Seg_472" s="T5">kanak-ɨ-mɨ</ta>
            <ta e="T7" id="Seg_473" s="T6">üːdi-mbɨ-sɨ</ta>
            <ta e="T8" id="Seg_474" s="T7">nʼarne-nɨ-nɨ</ta>
            <ta e="T9" id="Seg_475" s="T8">kanak-mɨ</ta>
            <ta e="T10" id="Seg_476" s="T9">poːsiŋ</ta>
            <ta e="T11" id="Seg_477" s="T10">uttɨrɨ-ŋɨ-n</ta>
            <ta e="T12" id="Seg_478" s="T11">nɨːnɨ</ta>
            <ta e="T13" id="Seg_479" s="T12">qolǯi-le</ta>
            <ta e="T14" id="Seg_480" s="T13">qwən-ŋɨ</ta>
            <ta e="T15" id="Seg_481" s="T14">qaj-n-tɨ</ta>
            <ta e="T16" id="Seg_482" s="T15">aptɨ-m</ta>
            <ta e="T17" id="Seg_483" s="T16">iː-lewle</ta>
            <ta e="T18" id="Seg_484" s="T17">man</ta>
            <ta e="T19" id="Seg_485" s="T18">manǯɨ-ntɨ-ŋ</ta>
            <ta e="T20" id="Seg_486" s="T19">nʼarne</ta>
            <ta e="T21" id="Seg_487" s="T20">wattə-ndɨ</ta>
            <ta e="T22" id="Seg_488" s="T21">i</ta>
            <ta e="T23" id="Seg_489" s="T22">qo-lʼčǝ-m</ta>
            <ta e="T24" id="Seg_490" s="T23">nʼünʼü-ka</ta>
            <ta e="T25" id="Seg_491" s="T24">peːge-lä-m</ta>
            <ta e="T26" id="Seg_492" s="T25">tap</ta>
            <ta e="T27" id="Seg_493" s="T26">aːmdi-sɨ</ta>
            <ta e="T28" id="Seg_494" s="T27">wattə-n</ta>
            <ta e="T29" id="Seg_495" s="T28">kɨ-qən</ta>
            <ta e="T30" id="Seg_496" s="T29">i</ta>
            <ta e="T31" id="Seg_497" s="T30">kuːča-naj</ta>
            <ta e="T32" id="Seg_498" s="T31">assɨ</ta>
            <ta e="T33" id="Seg_499" s="T32">köːdi-n</ta>
            <ta e="T34" id="Seg_500" s="T33">man</ta>
            <ta e="T35" id="Seg_501" s="T34">kanak-mɨ</ta>
            <ta e="T36" id="Seg_502" s="T35">ɨrɨ-ka-k</ta>
            <ta e="T37" id="Seg_503" s="T36">tap-ɨ-n-nɨ</ta>
            <ta e="T38" id="Seg_504" s="T37">mɨdɨ-le</ta>
            <ta e="T39" id="Seg_505" s="T38">čaːǯɨ-sɨ</ta>
            <ta e="T40" id="Seg_506" s="T39">poːsiŋ</ta>
            <ta e="T41" id="Seg_507" s="T40">wərkə</ta>
            <ta e="T42" id="Seg_508" s="T41">peːge</ta>
            <ta e="T43" id="Seg_509" s="T42">kuːča-ta</ta>
            <ta e="T44" id="Seg_510" s="T43">po-qɨnnɨ</ta>
            <ta e="T45" id="Seg_511" s="T44">waše-lewle</ta>
            <ta e="T46" id="Seg_512" s="T45">omdɨ-lʼčǝ-n</ta>
            <ta e="T47" id="Seg_513" s="T46">kanak-n</ta>
            <ta e="T48" id="Seg_514" s="T47">qaːt-tɨ</ta>
            <ta e="T49" id="Seg_515" s="T48">i</ta>
            <ta e="T50" id="Seg_516" s="T49">čuppa-la-m-tɨ</ta>
            <ta e="T51" id="Seg_517" s="T50">wärlo-lʼčǝ-le</ta>
            <ta e="T52" id="Seg_518" s="T51">šittə-k</ta>
            <ta e="T53" id="Seg_519" s="T52">qaj</ta>
            <ta e="T54" id="Seg_520" s="T53">paktǝ-ŋ</ta>
            <ta e="T55" id="Seg_521" s="T54">kanak-n-nɨ</ta>
            <ta e="T56" id="Seg_522" s="T55">man</ta>
            <ta e="T57" id="Seg_523" s="T56">kanak-mɨ</ta>
            <ta e="T58" id="Seg_524" s="T57">uttɨrɨ-n</ta>
            <ta e="T59" id="Seg_525" s="T58">i</ta>
            <ta e="T60" id="Seg_526" s="T59">qotä</ta>
            <ta e="T61" id="Seg_527" s="T60">paŋgal-nče-n</ta>
            <ta e="T62" id="Seg_528" s="T61">man</ta>
            <ta e="T63" id="Seg_529" s="T62">qwǝrǝ-m</ta>
            <ta e="T64" id="Seg_530" s="T63">kanak-mɨ</ta>
            <ta e="T65" id="Seg_531" s="T64">i</ta>
            <ta e="T66" id="Seg_532" s="T65">qwən-ŋɨ-ŋ</ta>
            <ta e="T67" id="Seg_533" s="T66">maːt-ndɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_534" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_535" s="T2">go-PST-1SG.S</ta>
            <ta e="T4" id="Seg_536" s="T3">back</ta>
            <ta e="T5" id="Seg_537" s="T4">hunt-DUR-RES-CVB2</ta>
            <ta e="T6" id="Seg_538" s="T5">dog-EP.[NOM]-1SG</ta>
            <ta e="T7" id="Seg_539" s="T6">run.away-DUR-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_540" s="T7">forward-OBL.1SG-ALL</ta>
            <ta e="T9" id="Seg_541" s="T8">dog.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_542" s="T9">suddenly</ta>
            <ta e="T11" id="Seg_543" s="T10">stop-CO-3SG.S</ta>
            <ta e="T12" id="Seg_544" s="T11">then</ta>
            <ta e="T13" id="Seg_545" s="T12">slink-CVB</ta>
            <ta e="T14" id="Seg_546" s="T13">go.away-CO.[3SG.S]</ta>
            <ta e="T15" id="Seg_547" s="T14">what-GEN-3SG</ta>
            <ta e="T16" id="Seg_548" s="T15">smell-ACC</ta>
            <ta e="T17" id="Seg_549" s="T16">take-CVB2</ta>
            <ta e="T18" id="Seg_550" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_551" s="T18">look-IPFV-1SG.S</ta>
            <ta e="T20" id="Seg_552" s="T19">forward</ta>
            <ta e="T21" id="Seg_553" s="T20">road-ILL</ta>
            <ta e="T22" id="Seg_554" s="T21">and</ta>
            <ta e="T23" id="Seg_555" s="T22">see-PFV-1SG.O</ta>
            <ta e="T24" id="Seg_556" s="T23">small-DIM</ta>
            <ta e="T25" id="Seg_557" s="T24">hazelhen-DIM-ACC</ta>
            <ta e="T26" id="Seg_558" s="T25">(s)he.[NOM]</ta>
            <ta e="T27" id="Seg_559" s="T26">sit-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_560" s="T27">road-GEN</ta>
            <ta e="T29" id="Seg_561" s="T28">middle-LOC</ta>
            <ta e="T30" id="Seg_562" s="T29">and</ta>
            <ta e="T31" id="Seg_563" s="T30">where-EMPH</ta>
            <ta e="T32" id="Seg_564" s="T31">NEG</ta>
            <ta e="T33" id="Seg_565" s="T32">go-3SG.S</ta>
            <ta e="T34" id="Seg_566" s="T33">I.GEN</ta>
            <ta e="T35" id="Seg_567" s="T34">dog.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_568" s="T35">slow-DIM-ADVZ</ta>
            <ta e="T37" id="Seg_569" s="T36">(s)he-EP-GEN-ALL</ta>
            <ta e="T38" id="Seg_570" s="T37">get.around-CVB</ta>
            <ta e="T39" id="Seg_571" s="T38">go-PST.[3SG.S]</ta>
            <ta e="T40" id="Seg_572" s="T39">suddenly</ta>
            <ta e="T41" id="Seg_573" s="T40">big</ta>
            <ta e="T42" id="Seg_574" s="T41">hazelhen.[NOM]</ta>
            <ta e="T43" id="Seg_575" s="T42">where-INDEF</ta>
            <ta e="T44" id="Seg_576" s="T43">tree-EL</ta>
            <ta e="T45" id="Seg_577" s="T44">fly-CVB2</ta>
            <ta e="T46" id="Seg_578" s="T45">sit.down-PFV-3SG.S</ta>
            <ta e="T47" id="Seg_579" s="T46">dog-GEN</ta>
            <ta e="T48" id="Seg_580" s="T47">before-3SG</ta>
            <ta e="T49" id="Seg_581" s="T48">and</ta>
            <ta e="T50" id="Seg_582" s="T49">wing-PL-ACC-3SG</ta>
            <ta e="T51" id="Seg_583" s="T50">%%-PFV-CVB</ta>
            <ta e="T52" id="Seg_584" s="T51">two-ADVZ</ta>
            <ta e="T53" id="Seg_585" s="T52">what</ta>
            <ta e="T54" id="Seg_586" s="T53">jump-3SG.S</ta>
            <ta e="T55" id="Seg_587" s="T54">dog-GEN-ALL</ta>
            <ta e="T56" id="Seg_588" s="T55">I.GEN</ta>
            <ta e="T57" id="Seg_589" s="T56">dog-1SG</ta>
            <ta e="T58" id="Seg_590" s="T57">stop-3SG.S</ta>
            <ta e="T59" id="Seg_591" s="T58">and</ta>
            <ta e="T60" id="Seg_592" s="T59">back</ta>
            <ta e="T61" id="Seg_593" s="T60">turn-IPFV3-3SG.S</ta>
            <ta e="T62" id="Seg_594" s="T61">I.NOM</ta>
            <ta e="T63" id="Seg_595" s="T62">call-1SG.O</ta>
            <ta e="T64" id="Seg_596" s="T63">dog.[NOM]-1SG</ta>
            <ta e="T65" id="Seg_597" s="T64">and</ta>
            <ta e="T66" id="Seg_598" s="T65">go.away-CO-1SG.S</ta>
            <ta e="T67" id="Seg_599" s="T66">house-ILL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_600" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_601" s="T2">идти-PST-1SG.S</ta>
            <ta e="T4" id="Seg_602" s="T3">назад</ta>
            <ta e="T5" id="Seg_603" s="T4">охотиться-DUR-RES-CVB2</ta>
            <ta e="T6" id="Seg_604" s="T5">собака-EP.[NOM]-1SG</ta>
            <ta e="T7" id="Seg_605" s="T6">убежать-DUR-PST.[3SG.S]</ta>
            <ta e="T8" id="Seg_606" s="T7">вперёд-OBL.1SG-ALL</ta>
            <ta e="T9" id="Seg_607" s="T8">собака.[NOM]-1SG</ta>
            <ta e="T10" id="Seg_608" s="T9">вдруг</ta>
            <ta e="T11" id="Seg_609" s="T10">остановиться-CO-3SG.S</ta>
            <ta e="T12" id="Seg_610" s="T11">потом</ta>
            <ta e="T13" id="Seg_611" s="T12">красться-CVB</ta>
            <ta e="T14" id="Seg_612" s="T13">уйти-CO.[3SG.S]</ta>
            <ta e="T15" id="Seg_613" s="T14">что-GEN-3SG</ta>
            <ta e="T16" id="Seg_614" s="T15">запах-ACC</ta>
            <ta e="T17" id="Seg_615" s="T16">взять-CVB2</ta>
            <ta e="T18" id="Seg_616" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_617" s="T18">смотреть-IPFV-1SG.S</ta>
            <ta e="T20" id="Seg_618" s="T19">вперёд</ta>
            <ta e="T21" id="Seg_619" s="T20">дорога-ILL</ta>
            <ta e="T22" id="Seg_620" s="T21">и</ta>
            <ta e="T23" id="Seg_621" s="T22">увидеть-PFV-1SG.O</ta>
            <ta e="T24" id="Seg_622" s="T23">маленький-DIM</ta>
            <ta e="T25" id="Seg_623" s="T24">рябчик-DIM-ACC</ta>
            <ta e="T26" id="Seg_624" s="T25">он(а).[NOM]</ta>
            <ta e="T27" id="Seg_625" s="T26">сидеть-PST.[3SG.S]</ta>
            <ta e="T28" id="Seg_626" s="T27">дорога-GEN</ta>
            <ta e="T29" id="Seg_627" s="T28">середина-LOC</ta>
            <ta e="T30" id="Seg_628" s="T29">и</ta>
            <ta e="T31" id="Seg_629" s="T30">куда-EMPH</ta>
            <ta e="T32" id="Seg_630" s="T31">NEG</ta>
            <ta e="T33" id="Seg_631" s="T32">ходить-3SG.S</ta>
            <ta e="T34" id="Seg_632" s="T33">я.GEN</ta>
            <ta e="T35" id="Seg_633" s="T34">собака.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_634" s="T35">медленный-DIM-ADVZ</ta>
            <ta e="T37" id="Seg_635" s="T36">он(а)-EP-GEN-ALL</ta>
            <ta e="T38" id="Seg_636" s="T37">подойти-CVB</ta>
            <ta e="T39" id="Seg_637" s="T38">идти-PST.[3SG.S]</ta>
            <ta e="T40" id="Seg_638" s="T39">вдруг</ta>
            <ta e="T41" id="Seg_639" s="T40">большой</ta>
            <ta e="T42" id="Seg_640" s="T41">рябчик.[NOM]</ta>
            <ta e="T43" id="Seg_641" s="T42">куда-INDEF</ta>
            <ta e="T44" id="Seg_642" s="T43">дерево-EL</ta>
            <ta e="T45" id="Seg_643" s="T44">летать-CVB2</ta>
            <ta e="T46" id="Seg_644" s="T45">сесть-PFV-3SG.S</ta>
            <ta e="T47" id="Seg_645" s="T46">собака-GEN</ta>
            <ta e="T48" id="Seg_646" s="T47">перед-3SG</ta>
            <ta e="T49" id="Seg_647" s="T48">и</ta>
            <ta e="T50" id="Seg_648" s="T49">крыло-PL-ACC-3SG</ta>
            <ta e="T51" id="Seg_649" s="T50">%%-PFV-CVB</ta>
            <ta e="T52" id="Seg_650" s="T51">два-ADVZ</ta>
            <ta e="T53" id="Seg_651" s="T52">что</ta>
            <ta e="T54" id="Seg_652" s="T53">прыгнуть-3SG.S</ta>
            <ta e="T55" id="Seg_653" s="T54">собака-GEN-ALL</ta>
            <ta e="T56" id="Seg_654" s="T55">я.GEN</ta>
            <ta e="T57" id="Seg_655" s="T56">собака-1SG</ta>
            <ta e="T58" id="Seg_656" s="T57">остановиться-3SG.S</ta>
            <ta e="T59" id="Seg_657" s="T58">и</ta>
            <ta e="T60" id="Seg_658" s="T59">назад</ta>
            <ta e="T61" id="Seg_659" s="T60">повернуть-IPFV3-3SG.S</ta>
            <ta e="T62" id="Seg_660" s="T61">я.NOM</ta>
            <ta e="T63" id="Seg_661" s="T62">позвать-1SG.O</ta>
            <ta e="T64" id="Seg_662" s="T63">собака.[NOM]-1SG</ta>
            <ta e="T65" id="Seg_663" s="T64">и</ta>
            <ta e="T66" id="Seg_664" s="T65">уйти-CO-1SG.S</ta>
            <ta e="T67" id="Seg_665" s="T66">дом-ILL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_666" s="T1">pers</ta>
            <ta e="T3" id="Seg_667" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_668" s="T3">adv</ta>
            <ta e="T5" id="Seg_669" s="T4">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T6" id="Seg_670" s="T5">n-n:ins.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_671" s="T6">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_672" s="T7">adv-n:obl.poss-n:case</ta>
            <ta e="T9" id="Seg_673" s="T8">n.[n:case]-n:poss</ta>
            <ta e="T10" id="Seg_674" s="T9">adv</ta>
            <ta e="T11" id="Seg_675" s="T10">v-v:ins-v:pn</ta>
            <ta e="T12" id="Seg_676" s="T11">adv</ta>
            <ta e="T13" id="Seg_677" s="T12">v-v&gt;adv</ta>
            <ta e="T14" id="Seg_678" s="T13">v-v:ins.[v:pn]</ta>
            <ta e="T15" id="Seg_679" s="T14">interrog-n:case-n:poss</ta>
            <ta e="T16" id="Seg_680" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_681" s="T16">v-v&gt;adv</ta>
            <ta e="T18" id="Seg_682" s="T17">pers</ta>
            <ta e="T19" id="Seg_683" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_684" s="T19">adv</ta>
            <ta e="T21" id="Seg_685" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_686" s="T21">conj</ta>
            <ta e="T23" id="Seg_687" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_688" s="T23">adj-n&gt;n</ta>
            <ta e="T25" id="Seg_689" s="T24">n-n&gt;n-n:case</ta>
            <ta e="T26" id="Seg_690" s="T25">pers.[n:case]</ta>
            <ta e="T27" id="Seg_691" s="T26">v-v:tense.[v:pn]</ta>
            <ta e="T28" id="Seg_692" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_693" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_694" s="T29">conj</ta>
            <ta e="T31" id="Seg_695" s="T30">interrog-clit</ta>
            <ta e="T32" id="Seg_696" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_697" s="T32">v-v:pn</ta>
            <ta e="T34" id="Seg_698" s="T33">pers</ta>
            <ta e="T35" id="Seg_699" s="T34">n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_700" s="T35">adj-n&gt;n-adj&gt;adv</ta>
            <ta e="T37" id="Seg_701" s="T36">pers-n:ins-n:case-n:case</ta>
            <ta e="T38" id="Seg_702" s="T37">v-v&gt;adv</ta>
            <ta e="T39" id="Seg_703" s="T38">v-v:tense.[v:pn]</ta>
            <ta e="T40" id="Seg_704" s="T39">adv</ta>
            <ta e="T41" id="Seg_705" s="T40">adj</ta>
            <ta e="T42" id="Seg_706" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_707" s="T42">interrog-clit</ta>
            <ta e="T44" id="Seg_708" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_709" s="T44">v-v&gt;adv</ta>
            <ta e="T46" id="Seg_710" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T47" id="Seg_711" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_712" s="T47">pp-n:poss</ta>
            <ta e="T49" id="Seg_713" s="T48">conj</ta>
            <ta e="T50" id="Seg_714" s="T49">n-n:num-n:case-n:poss</ta>
            <ta e="T51" id="Seg_715" s="T50">v-v&gt;v-v&gt;adv</ta>
            <ta e="T52" id="Seg_716" s="T51">num-adj&gt;adv</ta>
            <ta e="T53" id="Seg_717" s="T52">interrog</ta>
            <ta e="T54" id="Seg_718" s="T53">v-v:pn</ta>
            <ta e="T55" id="Seg_719" s="T54">n-n:case-n:case</ta>
            <ta e="T56" id="Seg_720" s="T55">pers</ta>
            <ta e="T57" id="Seg_721" s="T56">n-n:poss</ta>
            <ta e="T58" id="Seg_722" s="T57">v-v:pn</ta>
            <ta e="T59" id="Seg_723" s="T58">conj</ta>
            <ta e="T60" id="Seg_724" s="T59">adv</ta>
            <ta e="T61" id="Seg_725" s="T60">v-v&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_726" s="T61">pers</ta>
            <ta e="T63" id="Seg_727" s="T62">v-v:pn</ta>
            <ta e="T64" id="Seg_728" s="T63">n.[n:case]-n:poss</ta>
            <ta e="T65" id="Seg_729" s="T64">conj</ta>
            <ta e="T66" id="Seg_730" s="T65">v-v:ins-v:pn</ta>
            <ta e="T67" id="Seg_731" s="T66">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_732" s="T1">pers</ta>
            <ta e="T3" id="Seg_733" s="T2">v</ta>
            <ta e="T4" id="Seg_734" s="T3">adv</ta>
            <ta e="T5" id="Seg_735" s="T4">adv</ta>
            <ta e="T6" id="Seg_736" s="T5">n</ta>
            <ta e="T7" id="Seg_737" s="T6">v</ta>
            <ta e="T8" id="Seg_738" s="T7">adv</ta>
            <ta e="T9" id="Seg_739" s="T8">n</ta>
            <ta e="T10" id="Seg_740" s="T9">adv</ta>
            <ta e="T11" id="Seg_741" s="T10">v</ta>
            <ta e="T12" id="Seg_742" s="T11">adv</ta>
            <ta e="T13" id="Seg_743" s="T12">adv</ta>
            <ta e="T14" id="Seg_744" s="T13">v</ta>
            <ta e="T15" id="Seg_745" s="T14">interrog</ta>
            <ta e="T16" id="Seg_746" s="T15">n</ta>
            <ta e="T17" id="Seg_747" s="T16">adv</ta>
            <ta e="T18" id="Seg_748" s="T17">pers</ta>
            <ta e="T19" id="Seg_749" s="T18">v</ta>
            <ta e="T20" id="Seg_750" s="T19">adv</ta>
            <ta e="T21" id="Seg_751" s="T20">n</ta>
            <ta e="T22" id="Seg_752" s="T21">conj</ta>
            <ta e="T23" id="Seg_753" s="T22">v</ta>
            <ta e="T24" id="Seg_754" s="T23">adj</ta>
            <ta e="T25" id="Seg_755" s="T24">n</ta>
            <ta e="T26" id="Seg_756" s="T25">pers</ta>
            <ta e="T27" id="Seg_757" s="T26">v</ta>
            <ta e="T28" id="Seg_758" s="T27">n</ta>
            <ta e="T29" id="Seg_759" s="T28">n</ta>
            <ta e="T30" id="Seg_760" s="T29">conj</ta>
            <ta e="T31" id="Seg_761" s="T30">interrog</ta>
            <ta e="T32" id="Seg_762" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_763" s="T32">v</ta>
            <ta e="T34" id="Seg_764" s="T33">pers</ta>
            <ta e="T35" id="Seg_765" s="T34">n</ta>
            <ta e="T36" id="Seg_766" s="T35">adv</ta>
            <ta e="T37" id="Seg_767" s="T36">pers</ta>
            <ta e="T38" id="Seg_768" s="T37">adv</ta>
            <ta e="T39" id="Seg_769" s="T38">v</ta>
            <ta e="T40" id="Seg_770" s="T39">adv</ta>
            <ta e="T41" id="Seg_771" s="T40">adj</ta>
            <ta e="T42" id="Seg_772" s="T41">n</ta>
            <ta e="T43" id="Seg_773" s="T42">interrog</ta>
            <ta e="T44" id="Seg_774" s="T43">n</ta>
            <ta e="T45" id="Seg_775" s="T44">adv</ta>
            <ta e="T46" id="Seg_776" s="T45">v</ta>
            <ta e="T47" id="Seg_777" s="T46">n</ta>
            <ta e="T48" id="Seg_778" s="T47">pp</ta>
            <ta e="T49" id="Seg_779" s="T48">conj</ta>
            <ta e="T50" id="Seg_780" s="T49">n</ta>
            <ta e="T51" id="Seg_781" s="T50">adv</ta>
            <ta e="T52" id="Seg_782" s="T51">adv</ta>
            <ta e="T53" id="Seg_783" s="T52">interrog</ta>
            <ta e="T54" id="Seg_784" s="T53">v</ta>
            <ta e="T55" id="Seg_785" s="T54">n</ta>
            <ta e="T56" id="Seg_786" s="T55">pers</ta>
            <ta e="T57" id="Seg_787" s="T56">n</ta>
            <ta e="T58" id="Seg_788" s="T57">v</ta>
            <ta e="T59" id="Seg_789" s="T58">conj</ta>
            <ta e="T60" id="Seg_790" s="T59">adv</ta>
            <ta e="T61" id="Seg_791" s="T60">v</ta>
            <ta e="T62" id="Seg_792" s="T61">pers</ta>
            <ta e="T63" id="Seg_793" s="T62">v</ta>
            <ta e="T64" id="Seg_794" s="T63">n</ta>
            <ta e="T65" id="Seg_795" s="T64">conj</ta>
            <ta e="T66" id="Seg_796" s="T65">v</ta>
            <ta e="T67" id="Seg_797" s="T66">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_798" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_799" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_800" s="T5">np:S</ta>
            <ta e="T7" id="Seg_801" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_802" s="T8">np:S</ta>
            <ta e="T11" id="Seg_803" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_804" s="T12">s:adv</ta>
            <ta e="T14" id="Seg_805" s="T13">0.3:S v:pred</ta>
            <ta e="T17" id="Seg_806" s="T14">s:temp</ta>
            <ta e="T18" id="Seg_807" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_808" s="T18">v:pred</ta>
            <ta e="T23" id="Seg_809" s="T22">0.1.h:S v:pred</ta>
            <ta e="T25" id="Seg_810" s="T24">np:O</ta>
            <ta e="T26" id="Seg_811" s="T25">pro:S</ta>
            <ta e="T27" id="Seg_812" s="T26">v:pred</ta>
            <ta e="T33" id="Seg_813" s="T32">0.3:S v:pred</ta>
            <ta e="T35" id="Seg_814" s="T34">np:S</ta>
            <ta e="T39" id="Seg_815" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_816" s="T41">np:S</ta>
            <ta e="T46" id="Seg_817" s="T45">v:pred</ta>
            <ta e="T51" id="Seg_818" s="T49">s:adv</ta>
            <ta e="T54" id="Seg_819" s="T53">0.3:S v:pred</ta>
            <ta e="T57" id="Seg_820" s="T56">np:S</ta>
            <ta e="T58" id="Seg_821" s="T57">v:pred</ta>
            <ta e="T61" id="Seg_822" s="T60">0.3:S v:pred</ta>
            <ta e="T62" id="Seg_823" s="T61">pro.h:S</ta>
            <ta e="T63" id="Seg_824" s="T62">v:pred</ta>
            <ta e="T64" id="Seg_825" s="T63">np:O</ta>
            <ta e="T66" id="Seg_826" s="T65">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_827" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_828" s="T3">adv:G</ta>
            <ta e="T6" id="Seg_829" s="T5">np:A 0.1.h:Poss</ta>
            <ta e="T8" id="Seg_830" s="T7">adv:G</ta>
            <ta e="T9" id="Seg_831" s="T8">np:A 0.1.h:Poss</ta>
            <ta e="T12" id="Seg_832" s="T11">adv:Time</ta>
            <ta e="T14" id="Seg_833" s="T13">0.3:A</ta>
            <ta e="T15" id="Seg_834" s="T14">np:Poss</ta>
            <ta e="T16" id="Seg_835" s="T15">np:Th</ta>
            <ta e="T18" id="Seg_836" s="T17">pro.h:A</ta>
            <ta e="T20" id="Seg_837" s="T19">adv:G</ta>
            <ta e="T21" id="Seg_838" s="T20">np:G</ta>
            <ta e="T23" id="Seg_839" s="T22">0.1.h:E</ta>
            <ta e="T25" id="Seg_840" s="T24">np:Th</ta>
            <ta e="T26" id="Seg_841" s="T25">pro:Th</ta>
            <ta e="T28" id="Seg_842" s="T27">np:Poss</ta>
            <ta e="T29" id="Seg_843" s="T28">np:L</ta>
            <ta e="T33" id="Seg_844" s="T32">0.3:A</ta>
            <ta e="T34" id="Seg_845" s="T33">pro.h:Poss</ta>
            <ta e="T35" id="Seg_846" s="T34">np:A</ta>
            <ta e="T37" id="Seg_847" s="T36">pro:G</ta>
            <ta e="T42" id="Seg_848" s="T41">np:A</ta>
            <ta e="T44" id="Seg_849" s="T43">np:So</ta>
            <ta e="T48" id="Seg_850" s="T46">pp:G</ta>
            <ta e="T50" id="Seg_851" s="T49">np:Th 0.3:Poss</ta>
            <ta e="T54" id="Seg_852" s="T53">0.3:A</ta>
            <ta e="T55" id="Seg_853" s="T54">np:G</ta>
            <ta e="T56" id="Seg_854" s="T55">np.h:Poss</ta>
            <ta e="T57" id="Seg_855" s="T56">np:A</ta>
            <ta e="T60" id="Seg_856" s="T59">adv:G</ta>
            <ta e="T61" id="Seg_857" s="T60">0.3:A</ta>
            <ta e="T62" id="Seg_858" s="T61">pro.h:A</ta>
            <ta e="T64" id="Seg_859" s="T63">np:Th 0.1.h:Poss</ta>
            <ta e="T66" id="Seg_860" s="T65">0.1.h:A</ta>
            <ta e="T67" id="Seg_861" s="T66">np:G</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T22" id="Seg_862" s="T21">RUS:gram</ta>
            <ta e="T30" id="Seg_863" s="T29">RUS:gram</ta>
            <ta e="T31" id="Seg_864" s="T30">-</ta>
            <ta e="T49" id="Seg_865" s="T48">RUS:gram</ta>
            <ta e="T59" id="Seg_866" s="T58">RUS:gram</ta>
            <ta e="T65" id="Seg_867" s="T64">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_868" s="T1">I was going back from hunting.</ta>
            <ta e="T8" id="Seg_869" s="T5">My dog ran forward.</ta>
            <ta e="T11" id="Seg_870" s="T8">Suddenly my dog stopped.</ta>
            <ta e="T17" id="Seg_871" s="T11">Then it went slinking having scented one's smell.</ta>
            <ta e="T25" id="Seg_872" s="T17">I looked forward onto the road and saw a small hazel-grouse.</ta>
            <ta e="T33" id="Seg_873" s="T25">It sat on the road and wasn't going anywhere.</ta>
            <ta e="T39" id="Seg_874" s="T33">My dog approached it slowly.</ta>
            <ta e="T55" id="Seg_875" s="T39">Suddenly a big hazel-grouse having rosen from a tree sat before my dog and, having spread its wings (?), jumped twice towards my dog.</ta>
            <ta e="T61" id="Seg_876" s="T55">My dog stopped and turned back.</ta>
            <ta e="T67" id="Seg_877" s="T61">I called my dog and went home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_878" s="T1">Ich ging zurück vom Jagen.</ta>
            <ta e="T8" id="Seg_879" s="T5">Mein Hund rannte voraus.</ta>
            <ta e="T11" id="Seg_880" s="T8">Plötzlich hielt mein Hund an.</ta>
            <ta e="T17" id="Seg_881" s="T11">Dann ging er schleichend, als er den Geruch von etwas wahrnahm.</ta>
            <ta e="T25" id="Seg_882" s="T17">Ich sah voraus auf die Straße und sah ein kleines Haselhuhn.</ta>
            <ta e="T33" id="Seg_883" s="T25">Es saß mitten auf der Straße und ging nirgendwo hin.</ta>
            <ta e="T39" id="Seg_884" s="T33">Mein Hund näherte sich ihm langsam.</ta>
            <ta e="T55" id="Seg_885" s="T39">Plötzlich flog ein großes Haselhuhn aus einem Baum, landete vor meinem Hund und sprang mit ausgebreiteten Flügeln meinen Hund an.</ta>
            <ta e="T61" id="Seg_886" s="T55">Mein Hund hielt an und drehte sich um.</ta>
            <ta e="T67" id="Seg_887" s="T61">Ich rief meinen Hund und ging nach Hause.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_888" s="T1">Я шел назад с охоты.</ta>
            <ta e="T8" id="Seg_889" s="T5">Моя собака побежала впереди.</ta>
            <ta e="T11" id="Seg_890" s="T8">Моя собака вдруг остановилась.</ta>
            <ta e="T17" id="Seg_891" s="T11">Потом она крадучись пошла чей-то запах взяв (почуяв).</ta>
            <ta e="T25" id="Seg_892" s="T17">Я посмотрел вперед на дорогу и увидел маленького рябчика.</ta>
            <ta e="T33" id="Seg_893" s="T25">Он сидел посреди дороги и никуда не уходил.</ta>
            <ta e="T39" id="Seg_894" s="T33">Моя собака потихоньку к нему подошла.</ta>
            <ta e="T55" id="Seg_895" s="T39">Вдруг большой рябчик, с какого-то дерева поднявшись, сел перед собакой и растопырив крылья (?) два раза прыгнул к собаке.</ta>
            <ta e="T61" id="Seg_896" s="T55">Моя собака остановилась и обратно повернула.</ta>
            <ta e="T67" id="Seg_897" s="T61">Я позвал собаку и пошел домой.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_898" s="T1">я шел назад с охоты</ta>
            <ta e="T8" id="Seg_899" s="T5">моя собака побежала впереди</ta>
            <ta e="T11" id="Seg_900" s="T8">моя собака вдруг остановилась</ta>
            <ta e="T17" id="Seg_901" s="T11">потом крадучись пошла чей-то запах взяв (учуяв)</ta>
            <ta e="T25" id="Seg_902" s="T17">я посмотрел вперед на дорогу увидел маленького рябчика</ta>
            <ta e="T33" id="Seg_903" s="T25">он сидел посреди дороги и никуда не уходит</ta>
            <ta e="T39" id="Seg_904" s="T33">моя собака потихоньку к нему подошла</ta>
            <ta e="T55" id="Seg_905" s="T39">вдруг большой рябчик с какого-то дерева поднявшись (снявшись) сел перед собакой крылья растопырив раза два прыгнул к собаке</ta>
            <ta e="T61" id="Seg_906" s="T55">моя собака остановилась обратно повернула</ta>
            <ta e="T67" id="Seg_907" s="T61">я позвал собаку пошел домой</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
