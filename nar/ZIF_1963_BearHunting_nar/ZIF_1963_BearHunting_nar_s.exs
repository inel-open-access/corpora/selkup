<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ZIF_1963_BearHunting_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ZIF_1963_BearHunting_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">46</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ZIF">
            <abbreviation>ZIF</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ZIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T35" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">anduze</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">man</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Qätman</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">čaʒəzaŋ</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">sɨgrɨm</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qwärgä</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">nɨškalbat</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ambat</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">man</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">qouɣam</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">nadə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">qwätku</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">ranil</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_56" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_58" n="HIAT:w" s="T13">täp</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">tüan</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">mazɨm</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">tadʼernä</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_74" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">man</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">täpɨm</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">qwätnan</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">man</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">täbɨm</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qɨrram</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">i</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">čakam</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_104" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">täbɨm</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">nadə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">miːraŋu</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_116" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">man</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">uttəgalɨk</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_124" n="HIAT:w" s="T31">kalʼlʼiɣaŋ</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_128" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">sʼötakʼi</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">soː</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">ezuŋ</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T35" id="Seg_139" n="sc" s="T0">
               <ts e="T1" id="Seg_141" n="e" s="T0">anduze </ts>
               <ts e="T2" id="Seg_143" n="e" s="T1">man </ts>
               <ts e="T3" id="Seg_145" n="e" s="T2">Qätman </ts>
               <ts e="T4" id="Seg_147" n="e" s="T3">čaʒəzaŋ. </ts>
               <ts e="T5" id="Seg_149" n="e" s="T4">sɨgrɨm </ts>
               <ts e="T6" id="Seg_151" n="e" s="T5">qwärgä </ts>
               <ts e="T7" id="Seg_153" n="e" s="T6">nɨškalbat </ts>
               <ts e="T8" id="Seg_155" n="e" s="T7">ambat. </ts>
               <ts e="T9" id="Seg_157" n="e" s="T8">man </ts>
               <ts e="T10" id="Seg_159" n="e" s="T9">qouɣam. </ts>
               <ts e="T11" id="Seg_161" n="e" s="T10">nadə </ts>
               <ts e="T12" id="Seg_163" n="e" s="T11">qwätku. </ts>
               <ts e="T13" id="Seg_165" n="e" s="T12">ranil. </ts>
               <ts e="T14" id="Seg_167" n="e" s="T13">täp </ts>
               <ts e="T15" id="Seg_169" n="e" s="T14">tüan </ts>
               <ts e="T16" id="Seg_171" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_173" n="e" s="T16">mazɨm </ts>
               <ts e="T18" id="Seg_175" n="e" s="T17">tadʼernä. </ts>
               <ts e="T19" id="Seg_177" n="e" s="T18">man </ts>
               <ts e="T20" id="Seg_179" n="e" s="T19">täpɨm </ts>
               <ts e="T21" id="Seg_181" n="e" s="T20">qwätnan. </ts>
               <ts e="T22" id="Seg_183" n="e" s="T21">man </ts>
               <ts e="T23" id="Seg_185" n="e" s="T22">täbɨm </ts>
               <ts e="T24" id="Seg_187" n="e" s="T23">qɨrram </ts>
               <ts e="T25" id="Seg_189" n="e" s="T24">i </ts>
               <ts e="T26" id="Seg_191" n="e" s="T25">čakam. </ts>
               <ts e="T27" id="Seg_193" n="e" s="T26">täbɨm </ts>
               <ts e="T28" id="Seg_195" n="e" s="T27">nadə </ts>
               <ts e="T29" id="Seg_197" n="e" s="T28">miːraŋu. </ts>
               <ts e="T30" id="Seg_199" n="e" s="T29">man </ts>
               <ts e="T31" id="Seg_201" n="e" s="T30">uttəgalɨk </ts>
               <ts e="T32" id="Seg_203" n="e" s="T31">kalʼlʼiɣaŋ. </ts>
               <ts e="T33" id="Seg_205" n="e" s="T32">sʼötakʼi </ts>
               <ts e="T34" id="Seg_207" n="e" s="T33">soː </ts>
               <ts e="T35" id="Seg_209" n="e" s="T34">ezuŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_210" s="T0">ZIF_1963_BearHunting_nar.001 (001.001)</ta>
            <ta e="T8" id="Seg_211" s="T4">ZIF_1963_BearHunting_nar.002 (001.002)</ta>
            <ta e="T10" id="Seg_212" s="T8">ZIF_1963_BearHunting_nar.003 (001.003)</ta>
            <ta e="T12" id="Seg_213" s="T10">ZIF_1963_BearHunting_nar.004 (001.004)</ta>
            <ta e="T13" id="Seg_214" s="T12">ZIF_1963_BearHunting_nar.005 (001.005)</ta>
            <ta e="T18" id="Seg_215" s="T13">ZIF_1963_BearHunting_nar.006 (001.006)</ta>
            <ta e="T21" id="Seg_216" s="T18">ZIF_1963_BearHunting_nar.007 (001.007)</ta>
            <ta e="T26" id="Seg_217" s="T21">ZIF_1963_BearHunting_nar.008 (001.008)</ta>
            <ta e="T29" id="Seg_218" s="T26">ZIF_1963_BearHunting_nar.009 (001.009)</ta>
            <ta e="T32" id="Seg_219" s="T29">ZIF_1963_BearHunting_nar.010 (001.010)</ta>
            <ta e="T35" id="Seg_220" s="T32">ZIF_1963_BearHunting_nar.011 (001.011)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_221" s="T0">анду′зе ман ′kӓтман ′тшажъзаң.</ta>
            <ta e="T8" id="Seg_222" s="T4">сы′грым ′kwӓргӓ нышкал′бат ам′бат.</ta>
            <ta e="T10" id="Seg_223" s="T8">ман kоу′ɣам.</ta>
            <ta e="T12" id="Seg_224" s="T10">надъ kwӓт′ку.</ta>
            <ta e="T13" id="Seg_225" s="T12">′ранил.</ta>
            <ta e="T18" id="Seg_226" s="T13">тӓп ′тӱан и ма′зым ′тадʼернӓ.</ta>
            <ta e="T21" id="Seg_227" s="T18">ман тӓ′пым kwӓт′нан.</ta>
            <ta e="T26" id="Seg_228" s="T21">ман тӓ′бым kы′ррам и тч(ш)а′кам</ta>
            <ta e="T29" id="Seg_229" s="T26">тӓбым надъ мӣра′ңу.</ta>
            <ta e="T32" id="Seg_230" s="T29">ман ′уттъ‵галык ′калʼлʼиɣаң.</ta>
            <ta e="T35" id="Seg_231" s="T32">′сʼӧтакʼи со̄ е′з̂уң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_232" s="T0">anduze man qätman tšaʒəzaŋ.</ta>
            <ta e="T8" id="Seg_233" s="T4">sɨgrɨm qwärgä nɨškalbat ambat.</ta>
            <ta e="T10" id="Seg_234" s="T8">man qouɣam.</ta>
            <ta e="T12" id="Seg_235" s="T10">nadə qwätku.</ta>
            <ta e="T13" id="Seg_236" s="T12">ranil.</ta>
            <ta e="T18" id="Seg_237" s="T13">täp tüan i mazɨm tadʼernä.</ta>
            <ta e="T21" id="Seg_238" s="T18">man täpɨm qwätnan.</ta>
            <ta e="T26" id="Seg_239" s="T21">man täbɨm qɨrram i tč(š)akam</ta>
            <ta e="T29" id="Seg_240" s="T26">täbɨm nadə miːraŋu.</ta>
            <ta e="T32" id="Seg_241" s="T29">man uttəgalɨk kalʼlʼiɣaŋ.</ta>
            <ta e="T35" id="Seg_242" s="T32">sʼötakʼi soː eẑuŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_243" s="T0">anduze man Qätman čaʒəzaŋ. </ta>
            <ta e="T8" id="Seg_244" s="T4">sɨgrɨm qwärgä nɨškalbat ambat. </ta>
            <ta e="T10" id="Seg_245" s="T8">man qouɣam. </ta>
            <ta e="T12" id="Seg_246" s="T10">nadə qwätku. </ta>
            <ta e="T13" id="Seg_247" s="T12">ranil. </ta>
            <ta e="T18" id="Seg_248" s="T13">täp tüan i mazɨm tadʼernä. </ta>
            <ta e="T21" id="Seg_249" s="T18">man täpɨm qwätnan. </ta>
            <ta e="T26" id="Seg_250" s="T21">man täbɨm qɨrram i čakam. </ta>
            <ta e="T29" id="Seg_251" s="T26">täbɨm nadə miːraŋu. </ta>
            <ta e="T32" id="Seg_252" s="T29">man uttəgalɨk kalʼlʼiɣaŋ. </ta>
            <ta e="T35" id="Seg_253" s="T32">sʼötakʼi soː ezuŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_254" s="T0">andu-ze</ta>
            <ta e="T2" id="Seg_255" s="T1">man</ta>
            <ta e="T3" id="Seg_256" s="T2">Qät-man</ta>
            <ta e="T4" id="Seg_257" s="T3">čaʒə-za-ŋ</ta>
            <ta e="T5" id="Seg_258" s="T4">sɨgr-ɨ-m</ta>
            <ta e="T6" id="Seg_259" s="T5">qwärgä</ta>
            <ta e="T7" id="Seg_260" s="T6">nɨška-l-ba-t</ta>
            <ta e="T8" id="Seg_261" s="T7">am-ɨ-ba-t</ta>
            <ta e="T9" id="Seg_262" s="T8">man</ta>
            <ta e="T10" id="Seg_263" s="T9">qou-ɣa-m</ta>
            <ta e="T11" id="Seg_264" s="T10">nadə</ta>
            <ta e="T12" id="Seg_265" s="T11">qwät-ku</ta>
            <ta e="T14" id="Seg_266" s="T13">täp</ta>
            <ta e="T15" id="Seg_267" s="T14">tü-a-ŋ</ta>
            <ta e="T16" id="Seg_268" s="T15">i</ta>
            <ta e="T17" id="Seg_269" s="T16">mazɨm</ta>
            <ta e="T18" id="Seg_270" s="T17">tadʼe-r-nä</ta>
            <ta e="T19" id="Seg_271" s="T18">man</ta>
            <ta e="T20" id="Seg_272" s="T19">täp-ɨ-m</ta>
            <ta e="T21" id="Seg_273" s="T20">qwät-na-n</ta>
            <ta e="T22" id="Seg_274" s="T21">man</ta>
            <ta e="T23" id="Seg_275" s="T22">täb-ɨ-m</ta>
            <ta e="T24" id="Seg_276" s="T23">qɨrr-a-m</ta>
            <ta e="T25" id="Seg_277" s="T24">i</ta>
            <ta e="T26" id="Seg_278" s="T25">čaka-m</ta>
            <ta e="T27" id="Seg_279" s="T26">täb-ɨ-m</ta>
            <ta e="T28" id="Seg_280" s="T27">nadə</ta>
            <ta e="T29" id="Seg_281" s="T28">miːra-ŋu</ta>
            <ta e="T30" id="Seg_282" s="T29">man</ta>
            <ta e="T31" id="Seg_283" s="T30">uttə-galɨ-k</ta>
            <ta e="T32" id="Seg_284" s="T31">kalʼlʼi-ɣa-ŋ</ta>
            <ta e="T33" id="Seg_285" s="T32">%%</ta>
            <ta e="T34" id="Seg_286" s="T33">soː</ta>
            <ta e="T35" id="Seg_287" s="T34">e-zu-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_288" s="T0">andu-se</ta>
            <ta e="T2" id="Seg_289" s="T1">man</ta>
            <ta e="T3" id="Seg_290" s="T2">Qäd-mɨn</ta>
            <ta e="T4" id="Seg_291" s="T3">čaːǯɨ-sɨ-n</ta>
            <ta e="T5" id="Seg_292" s="T4">sɨr-ɨ-m</ta>
            <ta e="T6" id="Seg_293" s="T5">qwärqa</ta>
            <ta e="T7" id="Seg_294" s="T6">nɨškä-l-mbɨ-tɨ</ta>
            <ta e="T8" id="Seg_295" s="T7">am-ɨ-mbɨ-tɨ</ta>
            <ta e="T9" id="Seg_296" s="T8">man</ta>
            <ta e="T10" id="Seg_297" s="T9">qo-ŋɨ-m</ta>
            <ta e="T11" id="Seg_298" s="T10">nadə</ta>
            <ta e="T12" id="Seg_299" s="T11">qwat-gu</ta>
            <ta e="T14" id="Seg_300" s="T13">tap</ta>
            <ta e="T15" id="Seg_301" s="T14">tüː-ŋɨ-n</ta>
            <ta e="T16" id="Seg_302" s="T15">i</ta>
            <ta e="T17" id="Seg_303" s="T16">mašim</ta>
            <ta e="T18" id="Seg_304" s="T17">tadʼe-r-ŋɨ</ta>
            <ta e="T19" id="Seg_305" s="T18">man</ta>
            <ta e="T20" id="Seg_306" s="T19">tap-ɨ-m</ta>
            <ta e="T21" id="Seg_307" s="T20">qwat-ŋɨ-ŋ</ta>
            <ta e="T22" id="Seg_308" s="T21">man</ta>
            <ta e="T23" id="Seg_309" s="T22">tap-ɨ-m</ta>
            <ta e="T24" id="Seg_310" s="T23">qɨr-ɨ-m</ta>
            <ta e="T25" id="Seg_311" s="T24">i</ta>
            <ta e="T26" id="Seg_312" s="T25">čaqɨ-m</ta>
            <ta e="T27" id="Seg_313" s="T26">tap-ɨ-m</ta>
            <ta e="T28" id="Seg_314" s="T27">nadə</ta>
            <ta e="T29" id="Seg_315" s="T28">mirɨŋ-gu</ta>
            <ta e="T30" id="Seg_316" s="T29">man</ta>
            <ta e="T31" id="Seg_317" s="T30">uttɨ-gaːlɨ-k</ta>
            <ta e="T32" id="Seg_318" s="T31">qalɨ-ŋɨ-ŋ</ta>
            <ta e="T33" id="Seg_319" s="T32">%%</ta>
            <ta e="T34" id="Seg_320" s="T33">soː</ta>
            <ta e="T35" id="Seg_321" s="T34">eː-sɨ-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_322" s="T0">boat-INSTR</ta>
            <ta e="T2" id="Seg_323" s="T1">I.NOM</ta>
            <ta e="T3" id="Seg_324" s="T2">Ket-PROL</ta>
            <ta e="T4" id="Seg_325" s="T3">travel-PST-3SG.S</ta>
            <ta e="T5" id="Seg_326" s="T4">cow-EP-ACC</ta>
            <ta e="T6" id="Seg_327" s="T5">bear.[NOM]</ta>
            <ta e="T7" id="Seg_328" s="T6">tear-INCH-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_329" s="T7">eat-EP-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_330" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_331" s="T9">find-CO-1SG.O</ta>
            <ta e="T11" id="Seg_332" s="T10">one.should</ta>
            <ta e="T12" id="Seg_333" s="T11">kill-INF</ta>
            <ta e="T14" id="Seg_334" s="T13">(s)he.[NOM]</ta>
            <ta e="T15" id="Seg_335" s="T14">come-CO-3SG.S</ta>
            <ta e="T16" id="Seg_336" s="T15">and</ta>
            <ta e="T17" id="Seg_337" s="T16">I.ACC</ta>
            <ta e="T18" id="Seg_338" s="T17">%%-FRQ-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_339" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_340" s="T19">(s)he-EP-ACC</ta>
            <ta e="T21" id="Seg_341" s="T20">kill-CO-1SG.S</ta>
            <ta e="T22" id="Seg_342" s="T21">I.NOM</ta>
            <ta e="T23" id="Seg_343" s="T22">(s)he-EP-ACC</ta>
            <ta e="T24" id="Seg_344" s="T23">tear.off-EP-1SG.O</ta>
            <ta e="T25" id="Seg_345" s="T24">and</ta>
            <ta e="T26" id="Seg_346" s="T25">grind.out-1SG.O</ta>
            <ta e="T27" id="Seg_347" s="T26">(s)he-EP-ACC</ta>
            <ta e="T28" id="Seg_348" s="T27">one.should</ta>
            <ta e="T29" id="Seg_349" s="T28">sell-INF</ta>
            <ta e="T30" id="Seg_350" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_351" s="T30">hand-CAR-ADVZ</ta>
            <ta e="T32" id="Seg_352" s="T31">stay-CO-1SG.S</ta>
            <ta e="T33" id="Seg_353" s="T32">however</ta>
            <ta e="T34" id="Seg_354" s="T33">good</ta>
            <ta e="T35" id="Seg_355" s="T34">be-PST-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_356" s="T0">лодка-INSTR</ta>
            <ta e="T2" id="Seg_357" s="T1">я.NOM</ta>
            <ta e="T3" id="Seg_358" s="T2">Кеть-PROL</ta>
            <ta e="T4" id="Seg_359" s="T3">ехать-PST-3SG.S</ta>
            <ta e="T5" id="Seg_360" s="T4">корова-EP-ACC</ta>
            <ta e="T6" id="Seg_361" s="T5">медведь.[NOM]</ta>
            <ta e="T7" id="Seg_362" s="T6">сорвать-INCH-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_363" s="T7">съесть-EP-PST.NAR-3SG.O</ta>
            <ta e="T9" id="Seg_364" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_365" s="T9">найти-CO-1SG.O</ta>
            <ta e="T11" id="Seg_366" s="T10">надо</ta>
            <ta e="T12" id="Seg_367" s="T11">убить-INF</ta>
            <ta e="T14" id="Seg_368" s="T13">он(а).[NOM]</ta>
            <ta e="T15" id="Seg_369" s="T14">прийти-CO-3SG.S</ta>
            <ta e="T16" id="Seg_370" s="T15">и</ta>
            <ta e="T17" id="Seg_371" s="T16">я.ACC</ta>
            <ta e="T18" id="Seg_372" s="T17">%%-FRQ-CO.[3SG.S]</ta>
            <ta e="T19" id="Seg_373" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_374" s="T19">он(а)-EP-ACC</ta>
            <ta e="T21" id="Seg_375" s="T20">убить-CO-1SG.S</ta>
            <ta e="T22" id="Seg_376" s="T21">я.NOM</ta>
            <ta e="T23" id="Seg_377" s="T22">он(а)-EP-ACC</ta>
            <ta e="T24" id="Seg_378" s="T23">ободрать-EP-1SG.O</ta>
            <ta e="T25" id="Seg_379" s="T24">и</ta>
            <ta e="T26" id="Seg_380" s="T25">придавить-1SG.O</ta>
            <ta e="T27" id="Seg_381" s="T26">он(а)-EP-ACC</ta>
            <ta e="T28" id="Seg_382" s="T27">надо</ta>
            <ta e="T29" id="Seg_383" s="T28">продать-INF</ta>
            <ta e="T30" id="Seg_384" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_385" s="T30">рука-CAR-ADVZ</ta>
            <ta e="T32" id="Seg_386" s="T31">остаться-CO-1SG.S</ta>
            <ta e="T33" id="Seg_387" s="T32">все_таки</ta>
            <ta e="T34" id="Seg_388" s="T33">хороший</ta>
            <ta e="T35" id="Seg_389" s="T34">быть-PST-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_390" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_391" s="T1">pers</ta>
            <ta e="T3" id="Seg_392" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_393" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_394" s="T4">n-n:ins-n:case</ta>
            <ta e="T6" id="Seg_395" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_396" s="T6">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_397" s="T7">v-n:ins-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_398" s="T8">pers</ta>
            <ta e="T10" id="Seg_399" s="T9">v-v:ins-v:pn</ta>
            <ta e="T11" id="Seg_400" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_401" s="T11">v-v:inf</ta>
            <ta e="T14" id="Seg_402" s="T13">pers-n:case</ta>
            <ta e="T15" id="Seg_403" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_404" s="T15">conj</ta>
            <ta e="T17" id="Seg_405" s="T16">pers</ta>
            <ta e="T18" id="Seg_406" s="T17">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_407" s="T18">pers</ta>
            <ta e="T20" id="Seg_408" s="T19">pers-n:ins-n:case</ta>
            <ta e="T21" id="Seg_409" s="T20">v-v:ins-v:pn</ta>
            <ta e="T22" id="Seg_410" s="T21">pers</ta>
            <ta e="T23" id="Seg_411" s="T22">pers-n:ins-n:case</ta>
            <ta e="T24" id="Seg_412" s="T23">v-n:ins-v:pn</ta>
            <ta e="T25" id="Seg_413" s="T24">conj</ta>
            <ta e="T26" id="Seg_414" s="T25">v-v:pn</ta>
            <ta e="T27" id="Seg_415" s="T26">pers-n:ins-n:case</ta>
            <ta e="T28" id="Seg_416" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_417" s="T28">v-v:inf</ta>
            <ta e="T30" id="Seg_418" s="T29">pers</ta>
            <ta e="T31" id="Seg_419" s="T30">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T32" id="Seg_420" s="T31">v-v:ins-v:pn</ta>
            <ta e="T33" id="Seg_421" s="T32">adv</ta>
            <ta e="T34" id="Seg_422" s="T33">adj</ta>
            <ta e="T35" id="Seg_423" s="T34">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_424" s="T0">n</ta>
            <ta e="T2" id="Seg_425" s="T1">pers</ta>
            <ta e="T3" id="Seg_426" s="T2">nprop</ta>
            <ta e="T4" id="Seg_427" s="T3">v</ta>
            <ta e="T5" id="Seg_428" s="T4">n</ta>
            <ta e="T6" id="Seg_429" s="T5">n</ta>
            <ta e="T7" id="Seg_430" s="T6">v</ta>
            <ta e="T8" id="Seg_431" s="T7">v</ta>
            <ta e="T9" id="Seg_432" s="T8">pers</ta>
            <ta e="T10" id="Seg_433" s="T9">v</ta>
            <ta e="T11" id="Seg_434" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_435" s="T11">v</ta>
            <ta e="T14" id="Seg_436" s="T13">pers</ta>
            <ta e="T15" id="Seg_437" s="T14">v</ta>
            <ta e="T16" id="Seg_438" s="T15">conj</ta>
            <ta e="T17" id="Seg_439" s="T16">pers</ta>
            <ta e="T18" id="Seg_440" s="T17">v</ta>
            <ta e="T19" id="Seg_441" s="T18">pers</ta>
            <ta e="T20" id="Seg_442" s="T19">pers</ta>
            <ta e="T21" id="Seg_443" s="T20">v</ta>
            <ta e="T22" id="Seg_444" s="T21">pers</ta>
            <ta e="T23" id="Seg_445" s="T22">pers</ta>
            <ta e="T24" id="Seg_446" s="T23">v</ta>
            <ta e="T25" id="Seg_447" s="T24">conj</ta>
            <ta e="T26" id="Seg_448" s="T25">v</ta>
            <ta e="T27" id="Seg_449" s="T26">pers</ta>
            <ta e="T28" id="Seg_450" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_451" s="T28">v</ta>
            <ta e="T30" id="Seg_452" s="T29">pers</ta>
            <ta e="T32" id="Seg_453" s="T31">v</ta>
            <ta e="T33" id="Seg_454" s="T32">adv</ta>
            <ta e="T34" id="Seg_455" s="T33">adj</ta>
            <ta e="T35" id="Seg_456" s="T34">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_457" s="T1">pro.h:S</ta>
            <ta e="T4" id="Seg_458" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_459" s="T4">np:O</ta>
            <ta e="T6" id="Seg_460" s="T5">np:S</ta>
            <ta e="T7" id="Seg_461" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_462" s="T7">0.3:S 0.3:O v:pred</ta>
            <ta e="T9" id="Seg_463" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_464" s="T9">0.3:O v:pred</ta>
            <ta e="T11" id="Seg_465" s="T10">ptcl:pred</ta>
            <ta e="T12" id="Seg_466" s="T11">v:O</ta>
            <ta e="T14" id="Seg_467" s="T13">pro:S</ta>
            <ta e="T15" id="Seg_468" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_469" s="T16">pro.h:O</ta>
            <ta e="T18" id="Seg_470" s="T17">0.3:S v:pred</ta>
            <ta e="T19" id="Seg_471" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_472" s="T19">pro:O</ta>
            <ta e="T21" id="Seg_473" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_474" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_475" s="T22">pro:O</ta>
            <ta e="T24" id="Seg_476" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_477" s="T25">0.1.h:S 0.3:O</ta>
            <ta e="T28" id="Seg_478" s="T27">ptcl:pred</ta>
            <ta e="T29" id="Seg_479" s="T28">v:O</ta>
            <ta e="T30" id="Seg_480" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_481" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_482" s="T33">adj:pred</ta>
            <ta e="T35" id="Seg_483" s="T34">0.1.h:S cop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_484" s="T0">np:Ins</ta>
            <ta e="T2" id="Seg_485" s="T1">pro.h:A</ta>
            <ta e="T3" id="Seg_486" s="T2">np:Path</ta>
            <ta e="T5" id="Seg_487" s="T4">np:P</ta>
            <ta e="T6" id="Seg_488" s="T5">np:A</ta>
            <ta e="T8" id="Seg_489" s="T7">0.3:A 0.3:P</ta>
            <ta e="T9" id="Seg_490" s="T8">pro.h:A</ta>
            <ta e="T10" id="Seg_491" s="T9">0.3:Th</ta>
            <ta e="T12" id="Seg_492" s="T11">v:Th</ta>
            <ta e="T14" id="Seg_493" s="T13">pro:A</ta>
            <ta e="T17" id="Seg_494" s="T16">pro.h:P</ta>
            <ta e="T18" id="Seg_495" s="T17">0.3:A</ta>
            <ta e="T19" id="Seg_496" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_497" s="T19">pro:P</ta>
            <ta e="T22" id="Seg_498" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_499" s="T22">pro:P</ta>
            <ta e="T26" id="Seg_500" s="T25">0.1.h:A 0.3:P</ta>
            <ta e="T27" id="Seg_501" s="T26">pro:Th</ta>
            <ta e="T29" id="Seg_502" s="T28">v:Th</ta>
            <ta e="T30" id="Seg_503" s="T29">pro.h:Th</ta>
            <ta e="T35" id="Seg_504" s="T34">0.1.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_505" s="T10">RUS:gram</ta>
            <ta e="T16" id="Seg_506" s="T15">RUS:gram</ta>
            <ta e="T25" id="Seg_507" s="T24">RUS:gram</ta>
            <ta e="T28" id="Seg_508" s="T27">RUS:gram</ta>
            <ta e="T33" id="Seg_509" s="T32">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T11" id="Seg_510" s="T10">parad:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T13" id="Seg_511" s="T12">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_512" s="T0">По реки Кети я ехал на лодке.</ta>
            <ta e="T8" id="Seg_513" s="T4">Корову медведь разорвал и съел.</ta>
            <ta e="T10" id="Seg_514" s="T8">Я нашел его.</ta>
            <ta e="T12" id="Seg_515" s="T10">Надо его убить.</ta>
            <ta e="T13" id="Seg_516" s="T12">[Я его] ранил.</ta>
            <ta e="T18" id="Seg_517" s="T13">Он пришел и меня избил.</ta>
            <ta e="T21" id="Seg_518" s="T18">Я его убил.</ta>
            <ta e="T26" id="Seg_519" s="T21">Я его ободрал, (?).</ta>
            <ta e="T29" id="Seg_520" s="T26">Его надо сдать (продать).</ta>
            <ta e="T32" id="Seg_521" s="T29">Я без руки остался.</ta>
            <ta e="T35" id="Seg_522" s="T32">Всё-таки оправился.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_523" s="T0">I was going along the Ket river by boat.</ta>
            <ta e="T8" id="Seg_524" s="T4">A bear had torn a cow and eaten it.</ta>
            <ta e="T10" id="Seg_525" s="T8">I found it.</ta>
            <ta e="T12" id="Seg_526" s="T10">It should be killed.</ta>
            <ta e="T13" id="Seg_527" s="T12">[I] blessed [it].</ta>
            <ta e="T18" id="Seg_528" s="T13">It came and beat me.</ta>
            <ta e="T21" id="Seg_529" s="T18">I killed it.</ta>
            <ta e="T26" id="Seg_530" s="T21">I skinned it and (?) it.</ta>
            <ta e="T29" id="Seg_531" s="T26">It should be sold.</ta>
            <ta e="T32" id="Seg_532" s="T29">I stayed without a hand.</ta>
            <ta e="T35" id="Seg_533" s="T32">However I recovered.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_534" s="T0">Ich fuhr mit dem Boot den Fluss Ket entlang.</ta>
            <ta e="T8" id="Seg_535" s="T4">Ein Bär hatte eine Kuh gerissen und gefressen.</ta>
            <ta e="T10" id="Seg_536" s="T8">Ich fand ihn.</ta>
            <ta e="T12" id="Seg_537" s="T10">Man muss ihn töten.</ta>
            <ta e="T13" id="Seg_538" s="T12">[Ich] verwundete [ihn].</ta>
            <ta e="T18" id="Seg_539" s="T13">Er kam und schlug mich.</ta>
            <ta e="T21" id="Seg_540" s="T18">Ich tötete ihn.</ta>
            <ta e="T26" id="Seg_541" s="T21">Ich häutete ihn und zerteilte(?) ihn.</ta>
            <ta e="T29" id="Seg_542" s="T26">Er sollte verkauft werden.</ta>
            <ta e="T32" id="Seg_543" s="T29">Ich verlor eine Hand.</ta>
            <ta e="T35" id="Seg_544" s="T32">Dennoch wurde ich gesund.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_545" s="T0">по реки Кети я ехал на лодке</ta>
            <ta e="T8" id="Seg_546" s="T4">корову медведь разорвал съел</ta>
            <ta e="T10" id="Seg_547" s="T8">я нашел</ta>
            <ta e="T12" id="Seg_548" s="T10">надо убить</ta>
            <ta e="T18" id="Seg_549" s="T13">он пришел меня наволтузил</ta>
            <ta e="T21" id="Seg_550" s="T18">я его убил</ta>
            <ta e="T26" id="Seg_551" s="T21">я его ободрал обморкитанил (обделал)</ta>
            <ta e="T29" id="Seg_552" s="T26">его надо сдать (продать)</ta>
            <ta e="T32" id="Seg_553" s="T29">я без руки остался</ta>
            <ta e="T35" id="Seg_554" s="T32">ожился</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
