<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ZNP_1964_ThirdTime_nar</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ZNP_1964_ThirdTime_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">288</ud-information>
            <ud-information attribute-name="# HIAT:w">234</ud-information>
            <ud-information attribute-name="# e">234</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ZNP">
            <abbreviation>ZNP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ZNP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T235" id="Seg_0" n="sc" s="T1">
               <ts e="T12" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">nat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">jesan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">kunnä</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">nemcila</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">i</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">qassaɣɨla</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">müːdačisat</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">okkə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">ass</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_31" n="HIAT:w" s="T10">wärɣə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_34" n="HIAT:w" s="T11">korotka</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_38" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">nemcila</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">kotʼtʼi</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">quːlam</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">qwäm</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_54" n="HIAT:w" s="T16">plʼendɨ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_57" n="HIAT:w" s="T17">iːmbizattə</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_61" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">na</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">plʼendə</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">iːmbädi</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">quːlam</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_75" n="HIAT:w" s="T22">täpla</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">aːwatʼimda</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_81" n="HIAT:w" s="T24">qwatkuzattə</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_85" n="HIAT:w" s="T25">a</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_88" n="HIAT:w" s="T26">aːwatʼimda</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_91" n="HIAT:w" s="T27">laqqəlorǯambikuzattə</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_95" n="HIAT:w" s="T28">laqqəgu</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_98" n="HIAT:w" s="T29">kuralǯikuzattə</ts>
                  <nts id="Seg_99" n="HIAT:ip">)</nts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_103" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">na</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">quːlan</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">kottän</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">ässan</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">soŋ</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">maːkačundi</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">futbolsi</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_126" n="HIAT:w" s="T37">quːla</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_130" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">nemcila</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_135" n="HIAT:w" s="T39">tolgumbattə</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">na</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_141" n="HIAT:w" s="T41">qula</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_144" n="HIAT:w" s="T42">soːŋ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_147" n="HIAT:w" s="T43">maːkačuɣat</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_150" n="HIAT:w" s="T44">futbolse</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_154" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_156" n="HIAT:w" s="T45">täppɨdɨm</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_159" n="HIAT:w" s="T46">assə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_162" n="HIAT:w" s="T47">qwätpattə</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_166" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">a</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_171" n="HIAT:w" s="T49">täppɨdɨm</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_174" n="HIAT:w" s="T50">üːdambattə</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_177" n="HIAT:w" s="T51">štobɨ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_180" n="HIAT:w" s="T52">täbɨt</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_183" n="HIAT:w" s="T53">wolʼniŋ</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_186" n="HIAT:w" s="T54">palʼdʼüjjet</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_190" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_192" n="HIAT:w" s="T55">täbatni</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_195" n="HIAT:w" s="T56">so</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_198" n="HIAT:w" s="T57">maːt</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">mittäte</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_205" n="HIAT:w" s="T59">so</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_208" n="HIAT:w" s="T60">porgə</ts>
                  <nts id="Seg_209" n="HIAT:ip">,</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_212" n="HIAT:w" s="T61">pöːwä</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_216" n="HIAT:w" s="T62">so</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">aːwursaŋ</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_223" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_225" n="HIAT:w" s="T64">no</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_228" n="HIAT:w" s="T65">na</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_231" n="HIAT:w" s="T66">köt</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_234" n="HIAT:w" s="T67">qum</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_237" n="HIAT:w" s="T68">assə</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_240" n="HIAT:w" s="T69">kɨkkɨzat</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_243" n="HIAT:w" s="T70">nemcilanni</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_246" n="HIAT:w" s="T71">lakkɨgu</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_249" n="HIAT:w" s="T72">i</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_252" n="HIAT:w" s="T73">täbɨtnan</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_255" n="HIAT:w" s="T74">qaːligu</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_259" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_261" n="HIAT:w" s="T75">okkə</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_264" n="HIAT:w" s="T76">meː</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_267" n="HIAT:w" s="T77">quːmmat</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_270" n="HIAT:w" s="T78">qoːmbat</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_273" n="HIAT:w" s="T79">korotqɨn</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_276" n="HIAT:w" s="T80">tinnuwɨs</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_279" n="HIAT:w" s="T81">quwam</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_283" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_285" n="HIAT:w" s="T82">na</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_288" n="HIAT:w" s="T83">qum</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_291" n="HIAT:w" s="T84">kɨːgas</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_294" n="HIAT:w" s="T85">na</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_297" n="HIAT:w" s="T86">quːlam</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_299" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_301" n="HIAT:w" s="T87">quːlanni</ts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_305" n="HIAT:w" s="T88">peldɨku</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_308" n="HIAT:w" s="T89">kuːnägu</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_311" n="HIAT:w" s="T90">ondə</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">quːlaɣɨn</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_318" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_320" n="HIAT:w" s="T92">kuːnägu</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_323" n="HIAT:w" s="T93">täbatni</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_326" n="HIAT:w" s="T94">nada</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_329" n="HIAT:w" s="T95">wes</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_332" n="HIAT:w" s="T96">okkə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_335" n="HIAT:w" s="T97">mɨɣan</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_339" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_341" n="HIAT:w" s="T98">jeslʼi</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_344" n="HIAT:w" s="T99">okkə</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_347" n="HIAT:w" s="T100">qum</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_350" n="HIAT:w" s="T101">kuːni</ts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_354" n="HIAT:w" s="T102">to</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_357" n="HIAT:w" s="T103">nemcila</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_360" n="HIAT:w" s="T104">tʼäčenǯattə</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_364" n="HIAT:w" s="T105">tʼäčelǯenǯattə</ts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_368" n="HIAT:w" s="T106">wes</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_371" n="HIAT:w" s="T107">na</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_374" n="HIAT:w" s="T108">köt</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_377" n="HIAT:w" s="T109">quːlam</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_381" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_383" n="HIAT:w" s="T110">na</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_386" n="HIAT:w" s="T111">köt</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_389" n="HIAT:w" s="T112">quwankotin</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_392" n="HIAT:w" s="T113">okkər</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_395" n="HIAT:w" s="T114">assə</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_398" n="HIAT:w" s="T115">soː</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_401" n="HIAT:w" s="T116">qum</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_404" n="HIAT:w" s="T117">ässan</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_408" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_410" n="HIAT:w" s="T118">täp</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_413" n="HIAT:w" s="T119">toʒə</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_416" n="HIAT:w" s="T120">qassak</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_419" n="HIAT:w" s="T121">ässan</ts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_423" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_425" n="HIAT:w" s="T122">täpani</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_428" n="HIAT:w" s="T123">nʼemcila</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_431" n="HIAT:w" s="T124">kotʼtʼi</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_434" n="HIAT:w" s="T125">qomdäm</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_437" n="HIAT:w" s="T126">merǯekuzattə</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_439" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_441" n="HIAT:w" s="T127">merǯukumbattə</ts>
                  <nts id="Seg_442" n="HIAT:ip">)</nts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_446" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_448" n="HIAT:w" s="T128">na</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_451" n="HIAT:w" s="T129">qum</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_454" n="HIAT:w" s="T130">aːčikumbat</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_457" n="HIAT:w" s="T131">täppɨdɨm</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_461" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_463" n="HIAT:w" s="T132">maːnǯembikumbat</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_467" n="HIAT:w" s="T133">qai</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_470" n="HIAT:w" s="T134">kuldʼiŋ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_473" n="HIAT:w" s="T135">täːrba</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_477" n="HIAT:w" s="T136">qai</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_480" n="HIAT:w" s="T137">kuldʼiŋ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_483" n="HIAT:w" s="T138">meːkundɨt</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_487" n="HIAT:w" s="T139">qai</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_490" n="HIAT:w" s="T140">kutʼe</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_493" n="HIAT:w" s="T141">paldʼuŋ</ts>
                  <nts id="Seg_494" n="HIAT:ip">,</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_497" n="HIAT:w" s="T142">qaiɣum</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_500" n="HIAT:w" s="T143">qai</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_503" n="HIAT:w" s="T144">mendat</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_507" n="HIAT:w" s="T145">qaiɣum</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_510" n="HIAT:w" s="T146">qaj</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_513" n="HIAT:w" s="T147">qättet</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_517" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_519" n="HIAT:w" s="T148">naːtʼim</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_522" n="HIAT:w" s="T149">täp</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_525" n="HIAT:w" s="T150">wes</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_528" n="HIAT:w" s="T151">qätkumbat</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_531" n="HIAT:w" s="T152">nemcilanni</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_535" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_537" n="HIAT:w" s="T153">na</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_540" n="HIAT:w" s="T154">köt</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_543" n="HIAT:w" s="T155">qum</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_546" n="HIAT:w" s="T156">qaːɣi</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_549" n="HIAT:w" s="T157">qwänbatti</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_552" n="HIAT:w" s="T158">maːkačugu</ts>
                  <nts id="Seg_553" n="HIAT:ip">,</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_556" n="HIAT:w" s="T159">na</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_559" n="HIAT:w" s="T160">assə</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_562" n="HIAT:w" s="T161">so</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_565" n="HIAT:w" s="T162">qum</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_568" n="HIAT:w" s="T163">täbat</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_571" n="HIAT:w" s="T164">säppilaɣɨndɨt</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_574" n="HIAT:w" s="T165">kundə</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_577" n="HIAT:w" s="T166">peːrlʼi</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_580" n="HIAT:w" s="T167">qombat</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_583" n="HIAT:w" s="T168">täbat</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_586" n="HIAT:w" s="T169">näkɨrɨlamdɨt</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_590" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_592" n="HIAT:w" s="T170">täp</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_595" n="HIAT:w" s="T171">manǯämbat</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_598" n="HIAT:w" s="T172">na</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_601" n="HIAT:w" s="T173">näkɨrɨlam</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_605" n="HIAT:w" s="T174">no</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_608" n="HIAT:w" s="T175">ass</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_611" n="HIAT:w" s="T176">iːmbat</ts>
                  <nts id="Seg_612" n="HIAT:ip">,</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_615" n="HIAT:w" s="T177">qottä</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_618" n="HIAT:w" s="T178">pänbat</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_621" n="HIAT:w" s="T179">naʒə</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_624" n="HIAT:w" s="T180">seppilandə</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_628" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_630" n="HIAT:w" s="T181">a</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_633" n="HIAT:w" s="T182">nemcilanni</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_636" n="HIAT:w" s="T183">nadʼim</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_639" n="HIAT:w" s="T184">wes</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_642" n="HIAT:w" s="T185">qätpät</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_646" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_648" n="HIAT:w" s="T186">a</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_651" n="HIAT:w" s="T187">na</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_654" n="HIAT:w" s="T188">köt</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_657" n="HIAT:w" s="T189">qum</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_660" n="HIAT:w" s="T190">assə</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_663" n="HIAT:w" s="T191">tinnuwɨssattə</ts>
                  <nts id="Seg_664" n="HIAT:ip">,</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_667" n="HIAT:w" s="T192">što</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_670" n="HIAT:w" s="T193">täbat</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_673" n="HIAT:w" s="T194">tʼäːtə</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_676" n="HIAT:w" s="T195">nemcilane</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_679" n="HIAT:w" s="T196">uʒe</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_682" n="HIAT:w" s="T197">qətpa</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_686" n="HIAT:w" s="T198">što</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_689" n="HIAT:w" s="T199">na</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_692" n="HIAT:w" s="T200">quːla</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_695" n="HIAT:w" s="T201">kuːnäku</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_698" n="HIAT:w" s="T202">kɨkkattə</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_702" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_704" n="HIAT:w" s="T203">nännɨ</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_707" n="HIAT:w" s="T204">na</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_710" n="HIAT:w" s="T205">quːla</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_713" n="HIAT:w" s="T206">kuːnɨlʼe</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_716" n="HIAT:w" s="T207">qwännattə</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_719" n="HIAT:w" s="T208">i</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_722" n="HIAT:w" s="T209">kuːnɨlʼe</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_725" n="HIAT:w" s="T210">qwässoti</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_728" n="HIAT:w" s="T211">wagondə</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_731" n="HIAT:w" s="T212">assə</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_734" n="HIAT:w" s="T213">miːttattə</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_738" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_740" n="HIAT:w" s="T214">täppɨtɨm</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_743" n="HIAT:w" s="T215">oːrrannattə</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_746" n="HIAT:w" s="T216">nʼemcɨla</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_750" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_752" n="HIAT:w" s="T217">qottä</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_755" n="HIAT:w" s="T218">tatnattə</ts>
                  <nts id="Seg_756" n="HIAT:ip">.</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_759" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_761" n="HIAT:w" s="T219">soːɣajlasse</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_764" n="HIAT:w" s="T220">serapčattə</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_767" n="HIAT:w" s="T221">i</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_770" n="HIAT:w" s="T222">kuːralǯattə</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_773" n="HIAT:w" s="T223">maːkačugu</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_776" n="HIAT:w" s="T224">nemcilasse</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_779" n="HIAT:w" s="T225">futbolse</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_783" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_785" n="HIAT:w" s="T226">na</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_788" n="HIAT:w" s="T227">quːla</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_791" n="HIAT:w" s="T228">nemcilam</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_794" n="HIAT:w" s="T229">ɨːllaptattə</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_798" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_800" n="HIAT:w" s="T230">nännɨ</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_803" n="HIAT:w" s="T231">na</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_806" n="HIAT:w" s="T232">quːlam</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_809" n="HIAT:w" s="T233">nemcila</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_812" n="HIAT:w" s="T234">tʼäčelbat</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T235" id="Seg_815" n="sc" s="T1">
               <ts e="T2" id="Seg_817" n="e" s="T1">nat </ts>
               <ts e="T3" id="Seg_819" n="e" s="T2">jesan </ts>
               <ts e="T4" id="Seg_821" n="e" s="T3">kunnä </ts>
               <ts e="T5" id="Seg_823" n="e" s="T4">nemcila </ts>
               <ts e="T6" id="Seg_825" n="e" s="T5">i </ts>
               <ts e="T7" id="Seg_827" n="e" s="T6">qassaɣɨla </ts>
               <ts e="T8" id="Seg_829" n="e" s="T7">müːdačisat </ts>
               <ts e="T9" id="Seg_831" n="e" s="T8">okkə </ts>
               <ts e="T10" id="Seg_833" n="e" s="T9">ass </ts>
               <ts e="T11" id="Seg_835" n="e" s="T10">wärɣə </ts>
               <ts e="T12" id="Seg_837" n="e" s="T11">korotka. </ts>
               <ts e="T13" id="Seg_839" n="e" s="T12">nemcila </ts>
               <ts e="T14" id="Seg_841" n="e" s="T13">kotʼtʼi </ts>
               <ts e="T15" id="Seg_843" n="e" s="T14">quːlam </ts>
               <ts e="T16" id="Seg_845" n="e" s="T15">(qwäm) </ts>
               <ts e="T17" id="Seg_847" n="e" s="T16">plʼendɨ </ts>
               <ts e="T18" id="Seg_849" n="e" s="T17">iːmbizattə. </ts>
               <ts e="T19" id="Seg_851" n="e" s="T18">na </ts>
               <ts e="T20" id="Seg_853" n="e" s="T19">plʼendə </ts>
               <ts e="T21" id="Seg_855" n="e" s="T20">iːmbädi </ts>
               <ts e="T22" id="Seg_857" n="e" s="T21">quːlam </ts>
               <ts e="T23" id="Seg_859" n="e" s="T22">täpla </ts>
               <ts e="T24" id="Seg_861" n="e" s="T23">aːwatʼimda </ts>
               <ts e="T25" id="Seg_863" n="e" s="T24">qwatkuzattə, </ts>
               <ts e="T26" id="Seg_865" n="e" s="T25">a </ts>
               <ts e="T27" id="Seg_867" n="e" s="T26">aːwatʼimda </ts>
               <ts e="T28" id="Seg_869" n="e" s="T27">laqqəlorǯambikuzattə </ts>
               <ts e="T29" id="Seg_871" n="e" s="T28">(laqqəgu </ts>
               <ts e="T30" id="Seg_873" n="e" s="T29">kuralǯikuzattə). </ts>
               <ts e="T31" id="Seg_875" n="e" s="T30">na </ts>
               <ts e="T32" id="Seg_877" n="e" s="T31">quːlan </ts>
               <ts e="T33" id="Seg_879" n="e" s="T32">kottän </ts>
               <ts e="T34" id="Seg_881" n="e" s="T33">ässan </ts>
               <ts e="T35" id="Seg_883" n="e" s="T34">soŋ </ts>
               <ts e="T36" id="Seg_885" n="e" s="T35">maːkačundi </ts>
               <ts e="T37" id="Seg_887" n="e" s="T36">futbolsi </ts>
               <ts e="T38" id="Seg_889" n="e" s="T37">quːla. </ts>
               <ts e="T39" id="Seg_891" n="e" s="T38">nemcila </ts>
               <ts e="T40" id="Seg_893" n="e" s="T39">tolgumbattə </ts>
               <ts e="T41" id="Seg_895" n="e" s="T40">na </ts>
               <ts e="T42" id="Seg_897" n="e" s="T41">qula </ts>
               <ts e="T43" id="Seg_899" n="e" s="T42">soːŋ </ts>
               <ts e="T44" id="Seg_901" n="e" s="T43">maːkačuɣat </ts>
               <ts e="T45" id="Seg_903" n="e" s="T44">futbolse. </ts>
               <ts e="T46" id="Seg_905" n="e" s="T45">täppɨdɨm </ts>
               <ts e="T47" id="Seg_907" n="e" s="T46">assə </ts>
               <ts e="T48" id="Seg_909" n="e" s="T47">qwätpattə. </ts>
               <ts e="T49" id="Seg_911" n="e" s="T48">a </ts>
               <ts e="T50" id="Seg_913" n="e" s="T49">täppɨdɨm </ts>
               <ts e="T51" id="Seg_915" n="e" s="T50">üːdambattə </ts>
               <ts e="T52" id="Seg_917" n="e" s="T51">štobɨ </ts>
               <ts e="T53" id="Seg_919" n="e" s="T52">täbɨt </ts>
               <ts e="T54" id="Seg_921" n="e" s="T53">wolʼniŋ </ts>
               <ts e="T55" id="Seg_923" n="e" s="T54">palʼdʼüjjet. </ts>
               <ts e="T56" id="Seg_925" n="e" s="T55">täbatni </ts>
               <ts e="T57" id="Seg_927" n="e" s="T56">so </ts>
               <ts e="T58" id="Seg_929" n="e" s="T57">maːt </ts>
               <ts e="T59" id="Seg_931" n="e" s="T58">mittäte, </ts>
               <ts e="T60" id="Seg_933" n="e" s="T59">so </ts>
               <ts e="T61" id="Seg_935" n="e" s="T60">porgə, </ts>
               <ts e="T62" id="Seg_937" n="e" s="T61">pöːwä, </ts>
               <ts e="T63" id="Seg_939" n="e" s="T62">so </ts>
               <ts e="T64" id="Seg_941" n="e" s="T63">aːwursaŋ. </ts>
               <ts e="T65" id="Seg_943" n="e" s="T64">no </ts>
               <ts e="T66" id="Seg_945" n="e" s="T65">na </ts>
               <ts e="T67" id="Seg_947" n="e" s="T66">köt </ts>
               <ts e="T68" id="Seg_949" n="e" s="T67">qum </ts>
               <ts e="T69" id="Seg_951" n="e" s="T68">assə </ts>
               <ts e="T70" id="Seg_953" n="e" s="T69">kɨkkɨzat </ts>
               <ts e="T71" id="Seg_955" n="e" s="T70">nemcilanni </ts>
               <ts e="T72" id="Seg_957" n="e" s="T71">lakkɨgu </ts>
               <ts e="T73" id="Seg_959" n="e" s="T72">i </ts>
               <ts e="T74" id="Seg_961" n="e" s="T73">täbɨtnan </ts>
               <ts e="T75" id="Seg_963" n="e" s="T74">qaːligu. </ts>
               <ts e="T76" id="Seg_965" n="e" s="T75">okkə </ts>
               <ts e="T77" id="Seg_967" n="e" s="T76">meː </ts>
               <ts e="T78" id="Seg_969" n="e" s="T77">quːmmat </ts>
               <ts e="T79" id="Seg_971" n="e" s="T78">qoːmbat </ts>
               <ts e="T80" id="Seg_973" n="e" s="T79">korotqɨn </ts>
               <ts e="T81" id="Seg_975" n="e" s="T80">tinnuwɨs </ts>
               <ts e="T82" id="Seg_977" n="e" s="T81">quwam. </ts>
               <ts e="T83" id="Seg_979" n="e" s="T82">na </ts>
               <ts e="T84" id="Seg_981" n="e" s="T83">qum </ts>
               <ts e="T85" id="Seg_983" n="e" s="T84">kɨːgas </ts>
               <ts e="T86" id="Seg_985" n="e" s="T85">na </ts>
               <ts e="T87" id="Seg_987" n="e" s="T86">quːlam </ts>
               <ts e="T88" id="Seg_989" n="e" s="T87">(quːlanni) </ts>
               <ts e="T89" id="Seg_991" n="e" s="T88">peldɨku </ts>
               <ts e="T90" id="Seg_993" n="e" s="T89">kuːnägu </ts>
               <ts e="T91" id="Seg_995" n="e" s="T90">ondə </ts>
               <ts e="T92" id="Seg_997" n="e" s="T91">quːlaɣɨn. </ts>
               <ts e="T93" id="Seg_999" n="e" s="T92">kuːnägu </ts>
               <ts e="T94" id="Seg_1001" n="e" s="T93">täbatni </ts>
               <ts e="T95" id="Seg_1003" n="e" s="T94">nada </ts>
               <ts e="T96" id="Seg_1005" n="e" s="T95">wes </ts>
               <ts e="T97" id="Seg_1007" n="e" s="T96">okkə </ts>
               <ts e="T98" id="Seg_1009" n="e" s="T97">mɨɣan. </ts>
               <ts e="T99" id="Seg_1011" n="e" s="T98">jeslʼi </ts>
               <ts e="T100" id="Seg_1013" n="e" s="T99">okkə </ts>
               <ts e="T101" id="Seg_1015" n="e" s="T100">qum </ts>
               <ts e="T102" id="Seg_1017" n="e" s="T101">kuːni, </ts>
               <ts e="T103" id="Seg_1019" n="e" s="T102">to </ts>
               <ts e="T104" id="Seg_1021" n="e" s="T103">nemcila </ts>
               <ts e="T105" id="Seg_1023" n="e" s="T104">tʼäčenǯattə </ts>
               <ts e="T106" id="Seg_1025" n="e" s="T105">(tʼäčelǯenǯattə) </ts>
               <ts e="T107" id="Seg_1027" n="e" s="T106">wes </ts>
               <ts e="T108" id="Seg_1029" n="e" s="T107">na </ts>
               <ts e="T109" id="Seg_1031" n="e" s="T108">köt </ts>
               <ts e="T110" id="Seg_1033" n="e" s="T109">quːlam. </ts>
               <ts e="T111" id="Seg_1035" n="e" s="T110">na </ts>
               <ts e="T112" id="Seg_1037" n="e" s="T111">köt </ts>
               <ts e="T113" id="Seg_1039" n="e" s="T112">quwankotin </ts>
               <ts e="T114" id="Seg_1041" n="e" s="T113">okkər </ts>
               <ts e="T115" id="Seg_1043" n="e" s="T114">assə </ts>
               <ts e="T116" id="Seg_1045" n="e" s="T115">soː </ts>
               <ts e="T117" id="Seg_1047" n="e" s="T116">qum </ts>
               <ts e="T118" id="Seg_1049" n="e" s="T117">ässan. </ts>
               <ts e="T119" id="Seg_1051" n="e" s="T118">täp </ts>
               <ts e="T120" id="Seg_1053" n="e" s="T119">toʒə </ts>
               <ts e="T121" id="Seg_1055" n="e" s="T120">qassak </ts>
               <ts e="T122" id="Seg_1057" n="e" s="T121">ässan. </ts>
               <ts e="T123" id="Seg_1059" n="e" s="T122">täpani </ts>
               <ts e="T124" id="Seg_1061" n="e" s="T123">nʼemcila </ts>
               <ts e="T125" id="Seg_1063" n="e" s="T124">kotʼtʼi </ts>
               <ts e="T126" id="Seg_1065" n="e" s="T125">qomdäm </ts>
               <ts e="T127" id="Seg_1067" n="e" s="T126">merǯekuzattə </ts>
               <ts e="T128" id="Seg_1069" n="e" s="T127">(merǯukumbattə). </ts>
               <ts e="T129" id="Seg_1071" n="e" s="T128">na </ts>
               <ts e="T130" id="Seg_1073" n="e" s="T129">qum </ts>
               <ts e="T131" id="Seg_1075" n="e" s="T130">aːčikumbat </ts>
               <ts e="T132" id="Seg_1077" n="e" s="T131">täppɨdɨm. </ts>
               <ts e="T133" id="Seg_1079" n="e" s="T132">maːnǯembikumbat, </ts>
               <ts e="T134" id="Seg_1081" n="e" s="T133">qai </ts>
               <ts e="T135" id="Seg_1083" n="e" s="T134">kuldʼiŋ </ts>
               <ts e="T136" id="Seg_1085" n="e" s="T135">täːrba, </ts>
               <ts e="T137" id="Seg_1087" n="e" s="T136">qai </ts>
               <ts e="T138" id="Seg_1089" n="e" s="T137">kuldʼiŋ </ts>
               <ts e="T139" id="Seg_1091" n="e" s="T138">meːkundɨt, </ts>
               <ts e="T140" id="Seg_1093" n="e" s="T139">qai </ts>
               <ts e="T141" id="Seg_1095" n="e" s="T140">kutʼe </ts>
               <ts e="T142" id="Seg_1097" n="e" s="T141">paldʼuŋ, </ts>
               <ts e="T143" id="Seg_1099" n="e" s="T142">qaiɣum </ts>
               <ts e="T144" id="Seg_1101" n="e" s="T143">qai </ts>
               <ts e="T145" id="Seg_1103" n="e" s="T144">mendat, </ts>
               <ts e="T146" id="Seg_1105" n="e" s="T145">qaiɣum </ts>
               <ts e="T147" id="Seg_1107" n="e" s="T146">qaj </ts>
               <ts e="T148" id="Seg_1109" n="e" s="T147">qättet. </ts>
               <ts e="T149" id="Seg_1111" n="e" s="T148">naːtʼim </ts>
               <ts e="T150" id="Seg_1113" n="e" s="T149">täp </ts>
               <ts e="T151" id="Seg_1115" n="e" s="T150">wes </ts>
               <ts e="T152" id="Seg_1117" n="e" s="T151">qätkumbat </ts>
               <ts e="T153" id="Seg_1119" n="e" s="T152">nemcilanni. </ts>
               <ts e="T154" id="Seg_1121" n="e" s="T153">na </ts>
               <ts e="T155" id="Seg_1123" n="e" s="T154">köt </ts>
               <ts e="T156" id="Seg_1125" n="e" s="T155">qum </ts>
               <ts e="T157" id="Seg_1127" n="e" s="T156">qaːɣi </ts>
               <ts e="T158" id="Seg_1129" n="e" s="T157">qwänbatti </ts>
               <ts e="T159" id="Seg_1131" n="e" s="T158">maːkačugu, </ts>
               <ts e="T160" id="Seg_1133" n="e" s="T159">na </ts>
               <ts e="T161" id="Seg_1135" n="e" s="T160">assə </ts>
               <ts e="T162" id="Seg_1137" n="e" s="T161">so </ts>
               <ts e="T163" id="Seg_1139" n="e" s="T162">qum </ts>
               <ts e="T164" id="Seg_1141" n="e" s="T163">täbat </ts>
               <ts e="T165" id="Seg_1143" n="e" s="T164">säppilaɣɨndɨt </ts>
               <ts e="T166" id="Seg_1145" n="e" s="T165">kundə </ts>
               <ts e="T167" id="Seg_1147" n="e" s="T166">peːrlʼi </ts>
               <ts e="T168" id="Seg_1149" n="e" s="T167">qombat </ts>
               <ts e="T169" id="Seg_1151" n="e" s="T168">täbat </ts>
               <ts e="T170" id="Seg_1153" n="e" s="T169">näkɨrɨlamdɨt. </ts>
               <ts e="T171" id="Seg_1155" n="e" s="T170">täp </ts>
               <ts e="T172" id="Seg_1157" n="e" s="T171">manǯämbat </ts>
               <ts e="T173" id="Seg_1159" n="e" s="T172">na </ts>
               <ts e="T174" id="Seg_1161" n="e" s="T173">näkɨrɨlam, </ts>
               <ts e="T175" id="Seg_1163" n="e" s="T174">no </ts>
               <ts e="T176" id="Seg_1165" n="e" s="T175">ass </ts>
               <ts e="T177" id="Seg_1167" n="e" s="T176">iːmbat, </ts>
               <ts e="T178" id="Seg_1169" n="e" s="T177">qottä </ts>
               <ts e="T179" id="Seg_1171" n="e" s="T178">pänbat </ts>
               <ts e="T180" id="Seg_1173" n="e" s="T179">naʒə </ts>
               <ts e="T181" id="Seg_1175" n="e" s="T180">seppilandə. </ts>
               <ts e="T182" id="Seg_1177" n="e" s="T181">a </ts>
               <ts e="T183" id="Seg_1179" n="e" s="T182">nemcilanni </ts>
               <ts e="T184" id="Seg_1181" n="e" s="T183">nadʼim </ts>
               <ts e="T185" id="Seg_1183" n="e" s="T184">wes </ts>
               <ts e="T186" id="Seg_1185" n="e" s="T185">qätpät. </ts>
               <ts e="T187" id="Seg_1187" n="e" s="T186">a </ts>
               <ts e="T188" id="Seg_1189" n="e" s="T187">na </ts>
               <ts e="T189" id="Seg_1191" n="e" s="T188">köt </ts>
               <ts e="T190" id="Seg_1193" n="e" s="T189">qum </ts>
               <ts e="T191" id="Seg_1195" n="e" s="T190">assə </ts>
               <ts e="T192" id="Seg_1197" n="e" s="T191">tinnuwɨssattə, </ts>
               <ts e="T193" id="Seg_1199" n="e" s="T192">što </ts>
               <ts e="T194" id="Seg_1201" n="e" s="T193">täbat </ts>
               <ts e="T195" id="Seg_1203" n="e" s="T194">tʼäːtə </ts>
               <ts e="T196" id="Seg_1205" n="e" s="T195">nemcilane </ts>
               <ts e="T197" id="Seg_1207" n="e" s="T196">uʒe </ts>
               <ts e="T198" id="Seg_1209" n="e" s="T197">qətpa, </ts>
               <ts e="T199" id="Seg_1211" n="e" s="T198">što </ts>
               <ts e="T200" id="Seg_1213" n="e" s="T199">na </ts>
               <ts e="T201" id="Seg_1215" n="e" s="T200">quːla </ts>
               <ts e="T202" id="Seg_1217" n="e" s="T201">kuːnäku </ts>
               <ts e="T203" id="Seg_1219" n="e" s="T202">kɨkkattə. </ts>
               <ts e="T204" id="Seg_1221" n="e" s="T203">nännɨ </ts>
               <ts e="T205" id="Seg_1223" n="e" s="T204">na </ts>
               <ts e="T206" id="Seg_1225" n="e" s="T205">quːla </ts>
               <ts e="T207" id="Seg_1227" n="e" s="T206">kuːnɨlʼe </ts>
               <ts e="T208" id="Seg_1229" n="e" s="T207">qwännattə </ts>
               <ts e="T209" id="Seg_1231" n="e" s="T208">i </ts>
               <ts e="T210" id="Seg_1233" n="e" s="T209">kuːnɨlʼe </ts>
               <ts e="T211" id="Seg_1235" n="e" s="T210">qwässoti </ts>
               <ts e="T212" id="Seg_1237" n="e" s="T211">wagondə </ts>
               <ts e="T213" id="Seg_1239" n="e" s="T212">assə </ts>
               <ts e="T214" id="Seg_1241" n="e" s="T213">miːttattə. </ts>
               <ts e="T215" id="Seg_1243" n="e" s="T214">täppɨtɨm </ts>
               <ts e="T216" id="Seg_1245" n="e" s="T215">oːrrannattə </ts>
               <ts e="T217" id="Seg_1247" n="e" s="T216">nʼemcɨla. </ts>
               <ts e="T218" id="Seg_1249" n="e" s="T217">qottä </ts>
               <ts e="T219" id="Seg_1251" n="e" s="T218">tatnattə. </ts>
               <ts e="T220" id="Seg_1253" n="e" s="T219">soːɣajlasse </ts>
               <ts e="T221" id="Seg_1255" n="e" s="T220">serapčattə </ts>
               <ts e="T222" id="Seg_1257" n="e" s="T221">i </ts>
               <ts e="T223" id="Seg_1259" n="e" s="T222">kuːralǯattə </ts>
               <ts e="T224" id="Seg_1261" n="e" s="T223">maːkačugu </ts>
               <ts e="T225" id="Seg_1263" n="e" s="T224">nemcilasse </ts>
               <ts e="T226" id="Seg_1265" n="e" s="T225">futbolse. </ts>
               <ts e="T227" id="Seg_1267" n="e" s="T226">na </ts>
               <ts e="T228" id="Seg_1269" n="e" s="T227">quːla </ts>
               <ts e="T229" id="Seg_1271" n="e" s="T228">nemcilam </ts>
               <ts e="T230" id="Seg_1273" n="e" s="T229">ɨːllaptattə. </ts>
               <ts e="T231" id="Seg_1275" n="e" s="T230">nännɨ </ts>
               <ts e="T232" id="Seg_1277" n="e" s="T231">na </ts>
               <ts e="T233" id="Seg_1279" n="e" s="T232">quːlam </ts>
               <ts e="T234" id="Seg_1281" n="e" s="T233">nemcila </ts>
               <ts e="T235" id="Seg_1283" n="e" s="T234">tʼäčelbat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T12" id="Seg_1284" s="T1">ZNP_1964_ThirdTime_nar.001 (001.001)</ta>
            <ta e="T18" id="Seg_1285" s="T12">ZNP_1964_ThirdTime_nar.002 (001.002)</ta>
            <ta e="T30" id="Seg_1286" s="T18">ZNP_1964_ThirdTime_nar.003 (001.003)</ta>
            <ta e="T38" id="Seg_1287" s="T30">ZNP_1964_ThirdTime_nar.004 (001.004)</ta>
            <ta e="T45" id="Seg_1288" s="T38">ZNP_1964_ThirdTime_nar.005 (001.005)</ta>
            <ta e="T48" id="Seg_1289" s="T45">ZNP_1964_ThirdTime_nar.006 (001.006)</ta>
            <ta e="T55" id="Seg_1290" s="T48">ZNP_1964_ThirdTime_nar.007 (001.007)</ta>
            <ta e="T64" id="Seg_1291" s="T55">ZNP_1964_ThirdTime_nar.008 (001.008)</ta>
            <ta e="T75" id="Seg_1292" s="T64">ZNP_1964_ThirdTime_nar.009 (001.009)</ta>
            <ta e="T82" id="Seg_1293" s="T75">ZNP_1964_ThirdTime_nar.010 (001.010)</ta>
            <ta e="T92" id="Seg_1294" s="T82">ZNP_1964_ThirdTime_nar.011 (001.011)</ta>
            <ta e="T98" id="Seg_1295" s="T92">ZNP_1964_ThirdTime_nar.012 (001.012)</ta>
            <ta e="T110" id="Seg_1296" s="T98">ZNP_1964_ThirdTime_nar.013 (001.013)</ta>
            <ta e="T118" id="Seg_1297" s="T110">ZNP_1964_ThirdTime_nar.014 (001.014)</ta>
            <ta e="T122" id="Seg_1298" s="T118">ZNP_1964_ThirdTime_nar.015 (001.015)</ta>
            <ta e="T128" id="Seg_1299" s="T122">ZNP_1964_ThirdTime_nar.016 (001.016)</ta>
            <ta e="T132" id="Seg_1300" s="T128">ZNP_1964_ThirdTime_nar.017 (001.017)</ta>
            <ta e="T148" id="Seg_1301" s="T132">ZNP_1964_ThirdTime_nar.018 (001.018)</ta>
            <ta e="T153" id="Seg_1302" s="T148">ZNP_1964_ThirdTime_nar.019 (001.019)</ta>
            <ta e="T170" id="Seg_1303" s="T153">ZNP_1964_ThirdTime_nar.020 (001.020)</ta>
            <ta e="T181" id="Seg_1304" s="T170">ZNP_1964_ThirdTime_nar.021 (001.021)</ta>
            <ta e="T186" id="Seg_1305" s="T181">ZNP_1964_ThirdTime_nar.022 (001.022)</ta>
            <ta e="T203" id="Seg_1306" s="T186">ZNP_1964_ThirdTime_nar.023 (001.023)</ta>
            <ta e="T214" id="Seg_1307" s="T203">ZNP_1964_ThirdTime_nar.024 (001.024)</ta>
            <ta e="T217" id="Seg_1308" s="T214">ZNP_1964_ThirdTime_nar.025 (001.025)</ta>
            <ta e="T219" id="Seg_1309" s="T217">ZNP_1964_ThirdTime_nar.026 (001.026)</ta>
            <ta e="T226" id="Seg_1310" s="T219">ZNP_1964_ThirdTime_nar.027 (001.027)</ta>
            <ta e="T230" id="Seg_1311" s="T226">ZNP_1964_ThirdTime_nar.028 (001.028)</ta>
            <ta e="T235" id="Seg_1312" s="T230">ZNP_1964_ThirdTime_nar.029 (001.029)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T12" id="Seg_1313" s="T1">nat jesan kunnä nemcila i qassaɣɨla müːdatšisat okkə ass värɣə korotka.</ta>
            <ta e="T18" id="Seg_1314" s="T12">nemcila kotʼtʼi quːlam (qwäm) plʼendɨ iːmbizattə.</ta>
            <ta e="T30" id="Seg_1315" s="T18">na plʼendə iːmbäd(t)i quːlam täpla aːvatʼimda qwatkuzattə, a aːvatʼimda laqqəlorǯambikuzattə (laqqəgu kuralǯikuzattə).</ta>
            <ta e="T38" id="Seg_1316" s="T30">na quːlan kottän ässan soŋ maːkatšundi futbolsi quːla.</ta>
            <ta e="T45" id="Seg_1317" s="T38">nemcila tolgumbattə na qula soːŋ maːkatšuɣat futbolse.</ta>
            <ta e="T48" id="Seg_1318" s="T45">täppɨdɨm assə qwätpattə.</ta>
            <ta e="T55" id="Seg_1319" s="T48">a täppɨdɨm üːdambattə štobɨ täbɨt volʼniŋ palʼdʼüjjet.</ta>
            <ta e="T64" id="Seg_1320" s="T55">täbatni so maːt mittäte, so porgə, pöːvä, so aːvursaŋ.</ta>
            <ta e="T75" id="Seg_1321" s="T64">no na köt qum assə kɨkkɨzat nemcilanni lakkɨgu i täbɨtnan qaːligu.</ta>
            <ta e="T82" id="Seg_1322" s="T75">okkə meː quːmmat qoːmbat korotqɨn tinnuvɨs quvam.</ta>
            <ta e="T92" id="Seg_1323" s="T82">na qum kɨːgas na quːlam (quːlanni) peldɨku kuːnägu ondə quːlaɣɨn.</ta>
            <ta e="T98" id="Seg_1324" s="T92">kuːnägu täbatni nada ves okkə mɨɣan.</ta>
            <ta e="T110" id="Seg_1325" s="T98">jeslʼi okkə qum kuːni, to nemcila tʼätšenǯattə (tʼätšelǯenǯattə) ves na köt quːlam.</ta>
            <ta e="T118" id="Seg_1326" s="T110">na köt quvankotin okkər assə soː qum ässan.</ta>
            <ta e="T122" id="Seg_1327" s="T118">täp toʒə qassak ässan.</ta>
            <ta e="T128" id="Seg_1328" s="T122">täpani nʼemcila kotʼtʼi qomdäm merǯekuzattə (merǯukumbattə).</ta>
            <ta e="T132" id="Seg_1329" s="T128">na qum aːtšikumbat täppɨdɨm.</ta>
            <ta e="T148" id="Seg_1330" s="T132">maːnǯembikumbat, qai kuldʼiŋ täːrba, qai kuldʼiŋ meːkundɨt, qai kutʼe paldʼuŋ, qaiɣum qai mendat, qaiɣum qaj qä(ə)ttet.</ta>
            <ta e="T153" id="Seg_1331" s="T148">naːtʼim täp ves qätkumbat nemcilanni.</ta>
            <ta e="T170" id="Seg_1332" s="T153">na köt qum qaːɣi qwänbatti maːkatšugu, na assə so qum täbat säppilaɣɨndɨt kundə peːrlʼi qombat täbat näkɨrɨlamdɨt.</ta>
            <ta e="T181" id="Seg_1333" s="T170">täp manǯämbat na näkɨrɨlam, no ass iːmbat, qottä pänbat naʒə seppilandə.</ta>
            <ta e="T186" id="Seg_1334" s="T181">a nemcilanni nadʼim ves qätpät.</ta>
            <ta e="T203" id="Seg_1335" s="T186">a na köt qum assə tinnuvɨssattə, što täbat tʼäːtə nemcilane uʒe qətpa, što na quːla kuːnäku kɨkkattə.</ta>
            <ta e="T214" id="Seg_1336" s="T203">nännɨ na quːla kuːnɨlʼe qwännattə i kuːnɨlʼe qwässoti vagondə assə miːttattə.</ta>
            <ta e="T217" id="Seg_1337" s="T214">täppɨtɨm oːrrannattə nʼemcɨla.</ta>
            <ta e="T219" id="Seg_1338" s="T217">qottä tatnattə.</ta>
            <ta e="T226" id="Seg_1339" s="T219">soːɣajlasse seraptšattə i kuːralǯattə maːkatšugu nemcilasse futbolse.</ta>
            <ta e="T230" id="Seg_1340" s="T226">na quːla nemcilam ɨːllaptattə.</ta>
            <ta e="T235" id="Seg_1341" s="T230">nännɨ na quːlam nemcila tʼätšelbat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T12" id="Seg_1342" s="T1">nat jesan kunnä nemcila i qassaɣɨla müːdačisat okkə ass wärɣə korotka. </ta>
            <ta e="T18" id="Seg_1343" s="T12">nemcila kotʼtʼi quːlam (qwäm) plʼendɨ iːmbizattə. </ta>
            <ta e="T30" id="Seg_1344" s="T18">na plʼendə iːmbädi quːlam täpla aːwatʼimda qwatkuzattə, a aːwatʼimda laqqəlorǯambikuzattə (laqqəgu kuralǯikuzattə). </ta>
            <ta e="T38" id="Seg_1345" s="T30">na quːlan kottän ässan soŋ maːkačundi futbolsi quːla. </ta>
            <ta e="T45" id="Seg_1346" s="T38">nemcila tolgumbattə na qula soːŋ maːkačuɣat futbolse. </ta>
            <ta e="T48" id="Seg_1347" s="T45">täppɨdɨm assə qwätpattə. </ta>
            <ta e="T55" id="Seg_1348" s="T48">a täppɨdɨm üːdambattə štobɨ täbɨt wolʼniŋ palʼdʼüjjet. </ta>
            <ta e="T64" id="Seg_1349" s="T55">täbatni so maːt mittäte, so porgə, pöːwä, so aːwursaŋ. </ta>
            <ta e="T75" id="Seg_1350" s="T64">no na köt qum assə kɨkkɨzat nemcilanni lakkɨgu i täbɨtnan qaːligu. </ta>
            <ta e="T82" id="Seg_1351" s="T75">okkə meː quːmmat qoːmbat korotqɨn tinnuwɨs quwam. </ta>
            <ta e="T92" id="Seg_1352" s="T82">na qum kɨːgas na quːlam (quːlanni) peldɨku kuːnägu ondə quːlaɣɨn. </ta>
            <ta e="T98" id="Seg_1353" s="T92">kuːnägu täbatni nada wes okkə mɨɣan. </ta>
            <ta e="T110" id="Seg_1354" s="T98">jeslʼi okkə qum kuːni, to nemcila tʼäčenǯattə (tʼäčelǯenǯattə) wes na köt quːlam. </ta>
            <ta e="T118" id="Seg_1355" s="T110">na köt quwankotin okkər assə soː qum ässan. </ta>
            <ta e="T122" id="Seg_1356" s="T118">täp toʒə qassak ässan. </ta>
            <ta e="T128" id="Seg_1357" s="T122">täpani nʼemcila kotʼtʼi qomdäm merǯekuzattə (merǯukumbattə). </ta>
            <ta e="T132" id="Seg_1358" s="T128">na qum aːčikumbat täppɨdɨm. </ta>
            <ta e="T148" id="Seg_1359" s="T132">maːnǯembikumbat, qai kuldʼiŋ täːrba, qai kuldʼiŋ meːkundɨt, qai kutʼe paldʼuŋ, qaiɣum qai mendat, qaiɣum qaj qättet. </ta>
            <ta e="T153" id="Seg_1360" s="T148">naːtʼim täp wes qätkumbat nemcilanni. </ta>
            <ta e="T170" id="Seg_1361" s="T153">na köt qum qaːɣi qwänbatti maːkačugu, na assə so qum täbat säppilaɣɨndɨt kundə peːrlʼi qombat täbat näkɨrɨlamdɨt. </ta>
            <ta e="T181" id="Seg_1362" s="T170">täp manǯämbat na näkɨrɨlam, no ass iːmbat, qottä pänbat naʒə seppilandə. </ta>
            <ta e="T186" id="Seg_1363" s="T181">a nemcilanni nadʼim wes qätpät. </ta>
            <ta e="T203" id="Seg_1364" s="T186">a na köt qum assə tinnuwɨssattə, što täbat tʼäːtə nemcilane uʒe qətpa, što na quːla kuːnäku kɨkkattə. </ta>
            <ta e="T214" id="Seg_1365" s="T203">nännɨ na quːla kuːnɨlʼe qwännattə i kuːnɨlʼe qwässoti wagondə assə miːttattə. </ta>
            <ta e="T217" id="Seg_1366" s="T214">täppɨtɨm oːrrannattə nʼemcɨla. </ta>
            <ta e="T219" id="Seg_1367" s="T217">qottä tatnattə. </ta>
            <ta e="T226" id="Seg_1368" s="T219">soːɣajlasse serapčattə i kuːralǯattə maːkačugu nemcilasse futbolse. </ta>
            <ta e="T230" id="Seg_1369" s="T226">na quːla nemcilam ɨːllaptattə. </ta>
            <ta e="T235" id="Seg_1370" s="T230">nännɨ na quːlam nemcila tʼäčelbat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1371" s="T1">na-t</ta>
            <ta e="T3" id="Seg_1372" s="T2">je-sa-n</ta>
            <ta e="T4" id="Seg_1373" s="T3">kun-nä</ta>
            <ta e="T5" id="Seg_1374" s="T4">nemci-la</ta>
            <ta e="T6" id="Seg_1375" s="T5">i</ta>
            <ta e="T7" id="Seg_1376" s="T6">qassa-ɣɨ-la</ta>
            <ta e="T8" id="Seg_1377" s="T7">müːda-či-sa-t</ta>
            <ta e="T9" id="Seg_1378" s="T8">okkə</ta>
            <ta e="T10" id="Seg_1379" s="T9">ass</ta>
            <ta e="T11" id="Seg_1380" s="T10">wärɣə</ta>
            <ta e="T12" id="Seg_1381" s="T11">korot-ka</ta>
            <ta e="T13" id="Seg_1382" s="T12">nemci-la</ta>
            <ta e="T14" id="Seg_1383" s="T13">kotʼtʼi</ta>
            <ta e="T15" id="Seg_1384" s="T14">quː-la-m</ta>
            <ta e="T16" id="Seg_1385" s="T15">qwäm</ta>
            <ta e="T17" id="Seg_1386" s="T16">plʼen-dɨ</ta>
            <ta e="T18" id="Seg_1387" s="T17">iː-mbi-za-ttə</ta>
            <ta e="T19" id="Seg_1388" s="T18">na</ta>
            <ta e="T20" id="Seg_1389" s="T19">plʼen-də</ta>
            <ta e="T21" id="Seg_1390" s="T20">iː-mbädi</ta>
            <ta e="T22" id="Seg_1391" s="T21">quː-la-m</ta>
            <ta e="T23" id="Seg_1392" s="T22">täp-la</ta>
            <ta e="T24" id="Seg_1393" s="T23">aːwatʼi-m-da</ta>
            <ta e="T25" id="Seg_1394" s="T24">qwat-ku-za-ttə</ta>
            <ta e="T26" id="Seg_1395" s="T25">a</ta>
            <ta e="T27" id="Seg_1396" s="T26">aːwatʼi-m-da</ta>
            <ta e="T28" id="Seg_1397" s="T27">laqqə-lo-r-ǯa-mbi-ku-za-ttə</ta>
            <ta e="T29" id="Seg_1398" s="T28">laqqə-gu</ta>
            <ta e="T30" id="Seg_1399" s="T29">kuralǯi-ku-za-ttə</ta>
            <ta e="T31" id="Seg_1400" s="T30">na</ta>
            <ta e="T32" id="Seg_1401" s="T31">quː-la-n</ta>
            <ta e="T33" id="Seg_1402" s="T32">kottän</ta>
            <ta e="T34" id="Seg_1403" s="T33">ä-ssa-ŋ</ta>
            <ta e="T35" id="Seg_1404" s="T34">so-ŋ</ta>
            <ta e="T36" id="Seg_1405" s="T35">maːka-ču-ndi</ta>
            <ta e="T37" id="Seg_1406" s="T36">futbol-si</ta>
            <ta e="T38" id="Seg_1407" s="T37">quː-la</ta>
            <ta e="T39" id="Seg_1408" s="T38">nemci-la</ta>
            <ta e="T40" id="Seg_1409" s="T39">tol-gu-mba-ttə</ta>
            <ta e="T41" id="Seg_1410" s="T40">na</ta>
            <ta e="T42" id="Seg_1411" s="T41">qu-la</ta>
            <ta e="T43" id="Seg_1412" s="T42">soː-ŋ</ta>
            <ta e="T44" id="Seg_1413" s="T43">maːka-ču-ɣa-t</ta>
            <ta e="T45" id="Seg_1414" s="T44">futbol-se</ta>
            <ta e="T46" id="Seg_1415" s="T45">täpp-ɨ-d-ɨ-m</ta>
            <ta e="T47" id="Seg_1416" s="T46">assə</ta>
            <ta e="T48" id="Seg_1417" s="T47">qwät-pa-ttə</ta>
            <ta e="T49" id="Seg_1418" s="T48">a</ta>
            <ta e="T50" id="Seg_1419" s="T49">täpp-ɨ-d-ɨ-m</ta>
            <ta e="T51" id="Seg_1420" s="T50">üːda-mba-ttə</ta>
            <ta e="T52" id="Seg_1421" s="T51">štobɨ</ta>
            <ta e="T53" id="Seg_1422" s="T52">täb-ɨ-t</ta>
            <ta e="T54" id="Seg_1423" s="T53">wolʼni-ŋ</ta>
            <ta e="T55" id="Seg_1424" s="T54">palʼdʼü-jje-t</ta>
            <ta e="T56" id="Seg_1425" s="T55">täb-a-t-ni</ta>
            <ta e="T57" id="Seg_1426" s="T56">so</ta>
            <ta e="T58" id="Seg_1427" s="T57">maːt</ta>
            <ta e="T59" id="Seg_1428" s="T58">mi-ttä-te</ta>
            <ta e="T60" id="Seg_1429" s="T59">so</ta>
            <ta e="T61" id="Seg_1430" s="T60">porgə</ta>
            <ta e="T62" id="Seg_1431" s="T61">pöːwä</ta>
            <ta e="T63" id="Seg_1432" s="T62">so</ta>
            <ta e="T64" id="Seg_1433" s="T63">aːw-u-r-saŋ</ta>
            <ta e="T65" id="Seg_1434" s="T64">no</ta>
            <ta e="T66" id="Seg_1435" s="T65">na</ta>
            <ta e="T67" id="Seg_1436" s="T66">köt</ta>
            <ta e="T68" id="Seg_1437" s="T67">qum</ta>
            <ta e="T69" id="Seg_1438" s="T68">assə</ta>
            <ta e="T70" id="Seg_1439" s="T69">kɨkkɨ-za-t</ta>
            <ta e="T71" id="Seg_1440" s="T70">nemci-la-nni</ta>
            <ta e="T72" id="Seg_1441" s="T71">lakkɨ-gu</ta>
            <ta e="T73" id="Seg_1442" s="T72">i</ta>
            <ta e="T74" id="Seg_1443" s="T73">täb-ɨ-t-nan</ta>
            <ta e="T75" id="Seg_1444" s="T74">qaːli-gu</ta>
            <ta e="T76" id="Seg_1445" s="T75">okkə</ta>
            <ta e="T77" id="Seg_1446" s="T76">meː</ta>
            <ta e="T78" id="Seg_1447" s="T77">quːm-mat</ta>
            <ta e="T79" id="Seg_1448" s="T78">qoː-mba-t</ta>
            <ta e="T80" id="Seg_1449" s="T79">korot-qɨn</ta>
            <ta e="T81" id="Seg_1450" s="T80">tinnu-wɨ-s</ta>
            <ta e="T82" id="Seg_1451" s="T81">quw-a-m</ta>
            <ta e="T83" id="Seg_1452" s="T82">na</ta>
            <ta e="T84" id="Seg_1453" s="T83">qum</ta>
            <ta e="T85" id="Seg_1454" s="T84">kɨːga-s</ta>
            <ta e="T86" id="Seg_1455" s="T85">na</ta>
            <ta e="T87" id="Seg_1456" s="T86">quː-la-m</ta>
            <ta e="T88" id="Seg_1457" s="T87">quː-la-nni</ta>
            <ta e="T89" id="Seg_1458" s="T88">peldɨ-ku</ta>
            <ta e="T90" id="Seg_1459" s="T89">kuːnä-gu</ta>
            <ta e="T91" id="Seg_1460" s="T90">ondə</ta>
            <ta e="T92" id="Seg_1461" s="T91">quː-la-ɣɨn</ta>
            <ta e="T93" id="Seg_1462" s="T92">kuːnä-gu</ta>
            <ta e="T94" id="Seg_1463" s="T93">täb-a-t-ni</ta>
            <ta e="T95" id="Seg_1464" s="T94">nada</ta>
            <ta e="T96" id="Seg_1465" s="T95">wes</ta>
            <ta e="T97" id="Seg_1466" s="T96">okkə</ta>
            <ta e="T98" id="Seg_1467" s="T97">mɨ-ɣan</ta>
            <ta e="T99" id="Seg_1468" s="T98">jeslʼi</ta>
            <ta e="T100" id="Seg_1469" s="T99">okkə</ta>
            <ta e="T101" id="Seg_1470" s="T100">qum</ta>
            <ta e="T102" id="Seg_1471" s="T101">kuːni</ta>
            <ta e="T103" id="Seg_1472" s="T102">to</ta>
            <ta e="T104" id="Seg_1473" s="T103">nemci-la</ta>
            <ta e="T105" id="Seg_1474" s="T104">tʼäče-nǯa-ttə</ta>
            <ta e="T106" id="Seg_1475" s="T105">tʼäče-lǯe-nǯa-ttə</ta>
            <ta e="T107" id="Seg_1476" s="T106">wes</ta>
            <ta e="T108" id="Seg_1477" s="T107">na</ta>
            <ta e="T109" id="Seg_1478" s="T108">köt</ta>
            <ta e="T110" id="Seg_1479" s="T109">quː-la-m</ta>
            <ta e="T111" id="Seg_1480" s="T110">na</ta>
            <ta e="T112" id="Seg_1481" s="T111">köt</ta>
            <ta e="T113" id="Seg_1482" s="T112">qu-wan-kotin</ta>
            <ta e="T114" id="Seg_1483" s="T113">okkər</ta>
            <ta e="T115" id="Seg_1484" s="T114">assə</ta>
            <ta e="T116" id="Seg_1485" s="T115">soː</ta>
            <ta e="T117" id="Seg_1486" s="T116">qum</ta>
            <ta e="T118" id="Seg_1487" s="T117">ä-ssa-ŋ</ta>
            <ta e="T119" id="Seg_1488" s="T118">täp</ta>
            <ta e="T120" id="Seg_1489" s="T119">toʒə</ta>
            <ta e="T121" id="Seg_1490" s="T120">qassak</ta>
            <ta e="T122" id="Seg_1491" s="T121">ä-ssa-n</ta>
            <ta e="T123" id="Seg_1492" s="T122">täp-a-ni</ta>
            <ta e="T124" id="Seg_1493" s="T123">nʼemci-la</ta>
            <ta e="T125" id="Seg_1494" s="T124">kotʼtʼi</ta>
            <ta e="T126" id="Seg_1495" s="T125">qomdä-m</ta>
            <ta e="T127" id="Seg_1496" s="T126">merǯe-ku-za-ttə</ta>
            <ta e="T128" id="Seg_1497" s="T127">merǯu-ku-mba-ttə</ta>
            <ta e="T129" id="Seg_1498" s="T128">na</ta>
            <ta e="T130" id="Seg_1499" s="T129">qum</ta>
            <ta e="T131" id="Seg_1500" s="T130">aːči-ku-mba-t</ta>
            <ta e="T132" id="Seg_1501" s="T131">täpp-ɨ-d-ɨ-m</ta>
            <ta e="T133" id="Seg_1502" s="T132">maːnǯe-mbi-ku-mba-t</ta>
            <ta e="T134" id="Seg_1503" s="T133">qai</ta>
            <ta e="T135" id="Seg_1504" s="T134">kuldʼi-ŋ</ta>
            <ta e="T136" id="Seg_1505" s="T135">täːrba</ta>
            <ta e="T137" id="Seg_1506" s="T136">qai</ta>
            <ta e="T138" id="Seg_1507" s="T137">kuldʼi-ŋ</ta>
            <ta e="T139" id="Seg_1508" s="T138">meː-ku-ndɨ-t</ta>
            <ta e="T140" id="Seg_1509" s="T139">qai</ta>
            <ta e="T141" id="Seg_1510" s="T140">kutʼe</ta>
            <ta e="T142" id="Seg_1511" s="T141">paldʼu-ŋ</ta>
            <ta e="T143" id="Seg_1512" s="T142">qai-ɣum</ta>
            <ta e="T144" id="Seg_1513" s="T143">qai</ta>
            <ta e="T145" id="Seg_1514" s="T144">me-nda-t</ta>
            <ta e="T146" id="Seg_1515" s="T145">qai-ɣum</ta>
            <ta e="T147" id="Seg_1516" s="T146">qaj</ta>
            <ta e="T148" id="Seg_1517" s="T147">qätt-e-t</ta>
            <ta e="T149" id="Seg_1518" s="T148">naːtʼi-m</ta>
            <ta e="T150" id="Seg_1519" s="T149">täp</ta>
            <ta e="T151" id="Seg_1520" s="T150">wes</ta>
            <ta e="T152" id="Seg_1521" s="T151">qät-ku-mba-t</ta>
            <ta e="T153" id="Seg_1522" s="T152">nemci-la-n-ni</ta>
            <ta e="T154" id="Seg_1523" s="T153">na</ta>
            <ta e="T155" id="Seg_1524" s="T154">köt</ta>
            <ta e="T156" id="Seg_1525" s="T155">qum</ta>
            <ta e="T157" id="Seg_1526" s="T156">qaːɣi</ta>
            <ta e="T158" id="Seg_1527" s="T157">qwän-ba-tti</ta>
            <ta e="T159" id="Seg_1528" s="T158">maːka-ču-gu</ta>
            <ta e="T160" id="Seg_1529" s="T159">na</ta>
            <ta e="T161" id="Seg_1530" s="T160">assə</ta>
            <ta e="T162" id="Seg_1531" s="T161">so</ta>
            <ta e="T163" id="Seg_1532" s="T162">qum</ta>
            <ta e="T164" id="Seg_1533" s="T163">täb-a-t</ta>
            <ta e="T165" id="Seg_1534" s="T164">säppi-la-ɣɨn-dɨt</ta>
            <ta e="T166" id="Seg_1535" s="T165">kundə</ta>
            <ta e="T167" id="Seg_1536" s="T166">peː-r-lʼi</ta>
            <ta e="T168" id="Seg_1537" s="T167">qo-mba-t</ta>
            <ta e="T169" id="Seg_1538" s="T168">täb-a-t</ta>
            <ta e="T170" id="Seg_1539" s="T169">näkɨr-ɨ-la-m-dɨt</ta>
            <ta e="T171" id="Seg_1540" s="T170">täp</ta>
            <ta e="T172" id="Seg_1541" s="T171">manǯä-mba-t</ta>
            <ta e="T173" id="Seg_1542" s="T172">na</ta>
            <ta e="T174" id="Seg_1543" s="T173">näkɨr-ɨ-la-m</ta>
            <ta e="T175" id="Seg_1544" s="T174">no</ta>
            <ta e="T176" id="Seg_1545" s="T175">ass</ta>
            <ta e="T177" id="Seg_1546" s="T176">iː-mb-at</ta>
            <ta e="T178" id="Seg_1547" s="T177">qottä</ta>
            <ta e="T179" id="Seg_1548" s="T178">pän-ba-t</ta>
            <ta e="T180" id="Seg_1549" s="T179">na-ʒə</ta>
            <ta e="T181" id="Seg_1550" s="T180">seppi-la-ndə</ta>
            <ta e="T182" id="Seg_1551" s="T181">a</ta>
            <ta e="T183" id="Seg_1552" s="T182">nemci-la-n-ni</ta>
            <ta e="T184" id="Seg_1553" s="T183">nadʼi-m</ta>
            <ta e="T185" id="Seg_1554" s="T184">wes</ta>
            <ta e="T186" id="Seg_1555" s="T185">qät-pä-t</ta>
            <ta e="T187" id="Seg_1556" s="T186">a</ta>
            <ta e="T188" id="Seg_1557" s="T187">na</ta>
            <ta e="T189" id="Seg_1558" s="T188">köt</ta>
            <ta e="T190" id="Seg_1559" s="T189">qum</ta>
            <ta e="T191" id="Seg_1560" s="T190">assə</ta>
            <ta e="T192" id="Seg_1561" s="T191">tinnu-wɨ-ssa-ttə</ta>
            <ta e="T193" id="Seg_1562" s="T192">što</ta>
            <ta e="T194" id="Seg_1563" s="T193">täb-a-t</ta>
            <ta e="T195" id="Seg_1564" s="T194">tʼäːtə</ta>
            <ta e="T196" id="Seg_1565" s="T195">nemci-la-ne</ta>
            <ta e="T197" id="Seg_1566" s="T196">uʒe</ta>
            <ta e="T198" id="Seg_1567" s="T197">qət-pa</ta>
            <ta e="T199" id="Seg_1568" s="T198">što</ta>
            <ta e="T200" id="Seg_1569" s="T199">na</ta>
            <ta e="T201" id="Seg_1570" s="T200">quː-la</ta>
            <ta e="T202" id="Seg_1571" s="T201">kuːnä-ku</ta>
            <ta e="T203" id="Seg_1572" s="T202">kɨkka-ttə</ta>
            <ta e="T204" id="Seg_1573" s="T203">nännɨ</ta>
            <ta e="T205" id="Seg_1574" s="T204">na</ta>
            <ta e="T206" id="Seg_1575" s="T205">quː-la</ta>
            <ta e="T207" id="Seg_1576" s="T206">kuːnɨ-lʼe</ta>
            <ta e="T208" id="Seg_1577" s="T207">qwän-na-ttə</ta>
            <ta e="T209" id="Seg_1578" s="T208">i</ta>
            <ta e="T210" id="Seg_1579" s="T209">kuːnɨ-lʼe</ta>
            <ta e="T211" id="Seg_1580" s="T210">qwäs-so-ti</ta>
            <ta e="T212" id="Seg_1581" s="T211">wagon-də</ta>
            <ta e="T213" id="Seg_1582" s="T212">assə</ta>
            <ta e="T214" id="Seg_1583" s="T213">miːtta-ttə</ta>
            <ta e="T215" id="Seg_1584" s="T214">täpp-ɨ-t-ɨ-m</ta>
            <ta e="T216" id="Seg_1585" s="T215">oːrran-na-ttə</ta>
            <ta e="T217" id="Seg_1586" s="T216">nʼemcɨ-la</ta>
            <ta e="T218" id="Seg_1587" s="T217">qottä</ta>
            <ta e="T219" id="Seg_1588" s="T218">tat-na-ttə</ta>
            <ta e="T220" id="Seg_1589" s="T219">soː-ɣaj-la-s-se</ta>
            <ta e="T221" id="Seg_1590" s="T220">ser-a-p-ča-ttə</ta>
            <ta e="T222" id="Seg_1591" s="T221">i</ta>
            <ta e="T223" id="Seg_1592" s="T222">kuːra-lǯa-ttə</ta>
            <ta e="T224" id="Seg_1593" s="T223">maːka-ču-gu</ta>
            <ta e="T225" id="Seg_1594" s="T224">nemci-la-s-se</ta>
            <ta e="T226" id="Seg_1595" s="T225">futbol-se</ta>
            <ta e="T227" id="Seg_1596" s="T226">na</ta>
            <ta e="T228" id="Seg_1597" s="T227">quː-la</ta>
            <ta e="T229" id="Seg_1598" s="T228">nemci-la-m</ta>
            <ta e="T230" id="Seg_1599" s="T229">ɨːlla-pta-ttə</ta>
            <ta e="T231" id="Seg_1600" s="T230">nännɨ</ta>
            <ta e="T232" id="Seg_1601" s="T231">na</ta>
            <ta e="T233" id="Seg_1602" s="T232">quː-la-m</ta>
            <ta e="T234" id="Seg_1603" s="T233">nemci-la</ta>
            <ta e="T235" id="Seg_1604" s="T234">tʼäče-l-ba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1605" s="T1">na-tɨ</ta>
            <ta e="T3" id="Seg_1606" s="T2">eː-sɨ-n</ta>
            <ta e="T4" id="Seg_1607" s="T3">kuːn-naj</ta>
            <ta e="T5" id="Seg_1608" s="T4">nemci-la</ta>
            <ta e="T6" id="Seg_1609" s="T5">i</ta>
            <ta e="T7" id="Seg_1610" s="T6">qassaːq-sɨ-la</ta>
            <ta e="T8" id="Seg_1611" s="T7">müːdu-ču-sɨ-tɨt</ta>
            <ta e="T9" id="Seg_1612" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_1613" s="T9">assɨ</ta>
            <ta e="T11" id="Seg_1614" s="T10">wərkə</ta>
            <ta e="T12" id="Seg_1615" s="T11">korot-ka</ta>
            <ta e="T13" id="Seg_1616" s="T12">nemci-la</ta>
            <ta e="T14" id="Seg_1617" s="T13">koči</ta>
            <ta e="T15" id="Seg_1618" s="T14">qum-la-m</ta>
            <ta e="T16" id="Seg_1619" s="T15">qwäm</ta>
            <ta e="T17" id="Seg_1620" s="T16">plʼen-ndɨ</ta>
            <ta e="T18" id="Seg_1621" s="T17">iː-mbɨ-sɨ-tɨt</ta>
            <ta e="T19" id="Seg_1622" s="T18">na</ta>
            <ta e="T20" id="Seg_1623" s="T19">plʼen-ndɨ</ta>
            <ta e="T21" id="Seg_1624" s="T20">iː-mbɨdi</ta>
            <ta e="T22" id="Seg_1625" s="T21">qum-la-m</ta>
            <ta e="T23" id="Seg_1626" s="T22">tap-la</ta>
            <ta e="T24" id="Seg_1627" s="T23">aːwatʼi-m-ta</ta>
            <ta e="T25" id="Seg_1628" s="T24">qwat-ku-sɨ-tɨt</ta>
            <ta e="T26" id="Seg_1629" s="T25">a</ta>
            <ta e="T27" id="Seg_1630" s="T26">aːwatʼi-m-ta</ta>
            <ta e="T28" id="Seg_1631" s="T27">laqqɨ-lo-r-nče-mbɨ-ku-sɨ-tɨt</ta>
            <ta e="T29" id="Seg_1632" s="T28">laqqɨ-gu</ta>
            <ta e="T30" id="Seg_1633" s="T29">kuralǯi-ku-sɨ-tɨt</ta>
            <ta e="T31" id="Seg_1634" s="T30">na</ta>
            <ta e="T32" id="Seg_1635" s="T31">qum-la-n</ta>
            <ta e="T33" id="Seg_1636" s="T32">kottän</ta>
            <ta e="T34" id="Seg_1637" s="T33">eː-sɨ-n</ta>
            <ta e="T35" id="Seg_1638" s="T34">soː-k</ta>
            <ta e="T36" id="Seg_1639" s="T35">maːka-ču-ndi</ta>
            <ta e="T37" id="Seg_1640" s="T36">futbol-se</ta>
            <ta e="T38" id="Seg_1641" s="T37">qum-la</ta>
            <ta e="T39" id="Seg_1642" s="T38">nemci-la</ta>
            <ta e="T40" id="Seg_1643" s="T39">tol-ku-mbɨ-tɨt</ta>
            <ta e="T41" id="Seg_1644" s="T40">na</ta>
            <ta e="T42" id="Seg_1645" s="T41">qum-la</ta>
            <ta e="T43" id="Seg_1646" s="T42">soː-k</ta>
            <ta e="T44" id="Seg_1647" s="T43">maːka-ču-ŋɨ-tɨt</ta>
            <ta e="T45" id="Seg_1648" s="T44">futbol-se</ta>
            <ta e="T46" id="Seg_1649" s="T45">tap-ɨ-t-ɨ-m</ta>
            <ta e="T47" id="Seg_1650" s="T46">assɨ</ta>
            <ta e="T48" id="Seg_1651" s="T47">qwat-mbɨ-tɨt</ta>
            <ta e="T49" id="Seg_1652" s="T48">a</ta>
            <ta e="T50" id="Seg_1653" s="T49">tap-ɨ-t-ɨ-m</ta>
            <ta e="T51" id="Seg_1654" s="T50">üːtɨ-mbɨ-tɨt</ta>
            <ta e="T52" id="Seg_1655" s="T51">štobɨ</ta>
            <ta e="T53" id="Seg_1656" s="T52">tap-ɨ-tɨ</ta>
            <ta e="T54" id="Seg_1657" s="T53">wolʼni-k</ta>
            <ta e="T55" id="Seg_1658" s="T54">paldʼu-jä-tɨt</ta>
            <ta e="T56" id="Seg_1659" s="T55">tap-ɨ-t-nɨ</ta>
            <ta e="T57" id="Seg_1660" s="T56">soː</ta>
            <ta e="T58" id="Seg_1661" s="T57">maːt</ta>
            <ta e="T59" id="Seg_1662" s="T58">mi-tɨ-tɨt</ta>
            <ta e="T60" id="Seg_1663" s="T59">soː</ta>
            <ta e="T61" id="Seg_1664" s="T60">porqɨ</ta>
            <ta e="T62" id="Seg_1665" s="T61">pöwə</ta>
            <ta e="T63" id="Seg_1666" s="T62">soː</ta>
            <ta e="T64" id="Seg_1667" s="T63">am-ɨ-r-psan</ta>
            <ta e="T65" id="Seg_1668" s="T64">nu</ta>
            <ta e="T66" id="Seg_1669" s="T65">na</ta>
            <ta e="T67" id="Seg_1670" s="T66">köːt</ta>
            <ta e="T68" id="Seg_1671" s="T67">qum</ta>
            <ta e="T69" id="Seg_1672" s="T68">assɨ</ta>
            <ta e="T70" id="Seg_1673" s="T69">kɨkkɨ-sɨ-tɨt</ta>
            <ta e="T71" id="Seg_1674" s="T70">nemci-la-nɨ</ta>
            <ta e="T72" id="Seg_1675" s="T71">laqqɨ-gu</ta>
            <ta e="T73" id="Seg_1676" s="T72">i</ta>
            <ta e="T74" id="Seg_1677" s="T73">tap-ɨ-t-nan</ta>
            <ta e="T75" id="Seg_1678" s="T74">qalɨ-gu</ta>
            <ta e="T76" id="Seg_1679" s="T75">okkɨr</ta>
            <ta e="T77" id="Seg_1680" s="T76">meː</ta>
            <ta e="T78" id="Seg_1681" s="T77">qum-wɨt</ta>
            <ta e="T79" id="Seg_1682" s="T78">qo-mbɨ-tɨ</ta>
            <ta e="T80" id="Seg_1683" s="T79">korot-qən</ta>
            <ta e="T81" id="Seg_1684" s="T80">tinnɨ-ŋɨ-sɨ</ta>
            <ta e="T82" id="Seg_1685" s="T81">qum-ɨ-m</ta>
            <ta e="T83" id="Seg_1686" s="T82">na</ta>
            <ta e="T84" id="Seg_1687" s="T83">qum</ta>
            <ta e="T85" id="Seg_1688" s="T84">kɨkkɨ-sɨ</ta>
            <ta e="T86" id="Seg_1689" s="T85">na</ta>
            <ta e="T87" id="Seg_1690" s="T86">qum-la-m</ta>
            <ta e="T88" id="Seg_1691" s="T87">qum-la-nɨ</ta>
            <ta e="T89" id="Seg_1692" s="T88">päldɨ-gu</ta>
            <ta e="T90" id="Seg_1693" s="T89">kuːnɨ-gu</ta>
            <ta e="T91" id="Seg_1694" s="T90">ontɨ</ta>
            <ta e="T92" id="Seg_1695" s="T91">qum-la-qən</ta>
            <ta e="T93" id="Seg_1696" s="T92">kuːnɨ-gu</ta>
            <ta e="T94" id="Seg_1697" s="T93">tap-ɨ-t-nɨ</ta>
            <ta e="T95" id="Seg_1698" s="T94">nadə</ta>
            <ta e="T96" id="Seg_1699" s="T95">wesʼ</ta>
            <ta e="T97" id="Seg_1700" s="T96">okkɨr</ta>
            <ta e="T98" id="Seg_1701" s="T97">mɨ-qən</ta>
            <ta e="T99" id="Seg_1702" s="T98">jesʼli</ta>
            <ta e="T100" id="Seg_1703" s="T99">okkɨr</ta>
            <ta e="T101" id="Seg_1704" s="T100">qum</ta>
            <ta e="T102" id="Seg_1705" s="T101">kuːnɨ</ta>
            <ta e="T103" id="Seg_1706" s="T102">to</ta>
            <ta e="T104" id="Seg_1707" s="T103">nemci-la</ta>
            <ta e="T105" id="Seg_1708" s="T104">tʼaččɨ-nǯɨ-tɨt</ta>
            <ta e="T106" id="Seg_1709" s="T105">tʼaččɨ-lʼčǝ-nǯɨ-tɨt</ta>
            <ta e="T107" id="Seg_1710" s="T106">wesʼ</ta>
            <ta e="T108" id="Seg_1711" s="T107">na</ta>
            <ta e="T109" id="Seg_1712" s="T108">köːt</ta>
            <ta e="T110" id="Seg_1713" s="T109">qum-la-m</ta>
            <ta e="T111" id="Seg_1714" s="T110">na</ta>
            <ta e="T112" id="Seg_1715" s="T111">köːt</ta>
            <ta e="T113" id="Seg_1716" s="T112">qum-mɨn-kottän</ta>
            <ta e="T114" id="Seg_1717" s="T113">okkɨr</ta>
            <ta e="T115" id="Seg_1718" s="T114">assɨ</ta>
            <ta e="T116" id="Seg_1719" s="T115">soː</ta>
            <ta e="T117" id="Seg_1720" s="T116">qum</ta>
            <ta e="T118" id="Seg_1721" s="T117">eː-sɨ-n</ta>
            <ta e="T119" id="Seg_1722" s="T118">tap</ta>
            <ta e="T120" id="Seg_1723" s="T119">toʒə</ta>
            <ta e="T121" id="Seg_1724" s="T120">qassaːq</ta>
            <ta e="T122" id="Seg_1725" s="T121">eː-sɨ-n</ta>
            <ta e="T123" id="Seg_1726" s="T122">tap-ɨ-nɨ</ta>
            <ta e="T124" id="Seg_1727" s="T123">nemci-la</ta>
            <ta e="T125" id="Seg_1728" s="T124">koči</ta>
            <ta e="T126" id="Seg_1729" s="T125">qomdɛ-m</ta>
            <ta e="T127" id="Seg_1730" s="T126">merǯe-ku-sɨ-tɨt</ta>
            <ta e="T128" id="Seg_1731" s="T127">merǯe-ku-mbɨ-tɨt</ta>
            <ta e="T129" id="Seg_1732" s="T128">na</ta>
            <ta e="T130" id="Seg_1733" s="T129">qum</ta>
            <ta e="T131" id="Seg_1734" s="T130">atʼɨ-ku-mbɨ-tɨ</ta>
            <ta e="T132" id="Seg_1735" s="T131">tap-ɨ-t-ɨ-m</ta>
            <ta e="T133" id="Seg_1736" s="T132">manǯɨ-mbɨ-ku-mbɨ-tɨ</ta>
            <ta e="T134" id="Seg_1737" s="T133">qaj</ta>
            <ta e="T135" id="Seg_1738" s="T134">kulʼdi-k</ta>
            <ta e="T136" id="Seg_1739" s="T135">tärba</ta>
            <ta e="T137" id="Seg_1740" s="T136">qaj</ta>
            <ta e="T138" id="Seg_1741" s="T137">kulʼdi-k</ta>
            <ta e="T139" id="Seg_1742" s="T138">meː-ku-ntɨ-tɨ</ta>
            <ta e="T140" id="Seg_1743" s="T139">qaj</ta>
            <ta e="T141" id="Seg_1744" s="T140">kuːn</ta>
            <ta e="T142" id="Seg_1745" s="T141">paldʼu-n</ta>
            <ta e="T143" id="Seg_1746" s="T142">qaj-qum</ta>
            <ta e="T144" id="Seg_1747" s="T143">qaj</ta>
            <ta e="T145" id="Seg_1748" s="T144">meː-ntɨ-tɨ</ta>
            <ta e="T146" id="Seg_1749" s="T145">qaj-qum</ta>
            <ta e="T147" id="Seg_1750" s="T146">qaj</ta>
            <ta e="T148" id="Seg_1751" s="T147">kät-ɨ-tɨ</ta>
            <ta e="T149" id="Seg_1752" s="T148">naːtʼi-m</ta>
            <ta e="T150" id="Seg_1753" s="T149">tap</ta>
            <ta e="T151" id="Seg_1754" s="T150">wesʼ</ta>
            <ta e="T152" id="Seg_1755" s="T151">kät-ku-mbɨ-tɨ</ta>
            <ta e="T153" id="Seg_1756" s="T152">nemci-la-n-nɨ</ta>
            <ta e="T154" id="Seg_1757" s="T153">na</ta>
            <ta e="T155" id="Seg_1758" s="T154">köːt</ta>
            <ta e="T156" id="Seg_1759" s="T155">qum</ta>
            <ta e="T157" id="Seg_1760" s="T156">qaqä</ta>
            <ta e="T158" id="Seg_1761" s="T157">qwən-mbɨ-tɨt</ta>
            <ta e="T159" id="Seg_1762" s="T158">maːka-ču-gu</ta>
            <ta e="T160" id="Seg_1763" s="T159">na</ta>
            <ta e="T161" id="Seg_1764" s="T160">assɨ</ta>
            <ta e="T162" id="Seg_1765" s="T161">soː</ta>
            <ta e="T163" id="Seg_1766" s="T162">qum</ta>
            <ta e="T164" id="Seg_1767" s="T163">tap-ɨ-t</ta>
            <ta e="T165" id="Seg_1768" s="T164">säpi-la-qən-tɨt</ta>
            <ta e="T166" id="Seg_1769" s="T165">kundɨ</ta>
            <ta e="T167" id="Seg_1770" s="T166">peː-r-le</ta>
            <ta e="T168" id="Seg_1771" s="T167">qo-mbɨ-tɨ</ta>
            <ta e="T169" id="Seg_1772" s="T168">tap-ɨ-t</ta>
            <ta e="T170" id="Seg_1773" s="T169">näkɨr-ɨ-la-m-tɨt</ta>
            <ta e="T171" id="Seg_1774" s="T170">tap</ta>
            <ta e="T172" id="Seg_1775" s="T171">manǯɨ-mbɨ-tɨ</ta>
            <ta e="T173" id="Seg_1776" s="T172">na</ta>
            <ta e="T174" id="Seg_1777" s="T173">näkɨr-ɨ-la-m</ta>
            <ta e="T175" id="Seg_1778" s="T174">nu</ta>
            <ta e="T176" id="Seg_1779" s="T175">assɨ</ta>
            <ta e="T177" id="Seg_1780" s="T176">iː-mbɨ-ut</ta>
            <ta e="T178" id="Seg_1781" s="T177">qotä</ta>
            <ta e="T179" id="Seg_1782" s="T178">pan-mbɨ-tɨ</ta>
            <ta e="T180" id="Seg_1783" s="T179">na-ʒe</ta>
            <ta e="T181" id="Seg_1784" s="T180">säpi-la-ndɨ</ta>
            <ta e="T182" id="Seg_1785" s="T181">a</ta>
            <ta e="T183" id="Seg_1786" s="T182">nemci-la-n-nɨ</ta>
            <ta e="T184" id="Seg_1787" s="T183">naːtʼi-m</ta>
            <ta e="T185" id="Seg_1788" s="T184">wesʼ</ta>
            <ta e="T186" id="Seg_1789" s="T185">kät-mbɨ-tɨ</ta>
            <ta e="T187" id="Seg_1790" s="T186">a</ta>
            <ta e="T188" id="Seg_1791" s="T187">na</ta>
            <ta e="T189" id="Seg_1792" s="T188">köːt</ta>
            <ta e="T190" id="Seg_1793" s="T189">qum</ta>
            <ta e="T191" id="Seg_1794" s="T190">assɨ</ta>
            <ta e="T192" id="Seg_1795" s="T191">tinnɨ-wa-sɨ-tɨt</ta>
            <ta e="T193" id="Seg_1796" s="T192">što</ta>
            <ta e="T194" id="Seg_1797" s="T193">tap-ɨ-t</ta>
            <ta e="T195" id="Seg_1798" s="T194">tʼäːtə</ta>
            <ta e="T196" id="Seg_1799" s="T195">nemci-la-nɨ</ta>
            <ta e="T197" id="Seg_1800" s="T196">uʒe</ta>
            <ta e="T198" id="Seg_1801" s="T197">kät-mbɨ</ta>
            <ta e="T199" id="Seg_1802" s="T198">što</ta>
            <ta e="T200" id="Seg_1803" s="T199">na</ta>
            <ta e="T201" id="Seg_1804" s="T200">qum-la</ta>
            <ta e="T202" id="Seg_1805" s="T201">kuːnɨ-gu</ta>
            <ta e="T203" id="Seg_1806" s="T202">kɨkkɨ-tɨt</ta>
            <ta e="T204" id="Seg_1807" s="T203">nɨːnɨ</ta>
            <ta e="T205" id="Seg_1808" s="T204">na</ta>
            <ta e="T206" id="Seg_1809" s="T205">qum-la</ta>
            <ta e="T207" id="Seg_1810" s="T206">kuːnɨ-le</ta>
            <ta e="T208" id="Seg_1811" s="T207">qwən-ŋɨ-tɨt</ta>
            <ta e="T209" id="Seg_1812" s="T208">i</ta>
            <ta e="T210" id="Seg_1813" s="T209">kuːnɨ-le</ta>
            <ta e="T211" id="Seg_1814" s="T210">qwən-sɨ-tɨt</ta>
            <ta e="T212" id="Seg_1815" s="T211">wagon-ndɨ</ta>
            <ta e="T213" id="Seg_1816" s="T212">assɨ</ta>
            <ta e="T214" id="Seg_1817" s="T213">miːta-tɨt</ta>
            <ta e="T215" id="Seg_1818" s="T214">tap-ɨ-t-ɨ-m</ta>
            <ta e="T216" id="Seg_1819" s="T215">oral-ŋɨ-tɨt</ta>
            <ta e="T217" id="Seg_1820" s="T216">nemci-la</ta>
            <ta e="T218" id="Seg_1821" s="T217">qotä</ta>
            <ta e="T219" id="Seg_1822" s="T218">tadɨ-ŋɨ-tɨt</ta>
            <ta e="T220" id="Seg_1823" s="T219">soː-qaj-la-n-se</ta>
            <ta e="T221" id="Seg_1824" s="T220">šeːr-ɨ-mbɨ-nǯɨ-tɨt</ta>
            <ta e="T222" id="Seg_1825" s="T221">i</ta>
            <ta e="T223" id="Seg_1826" s="T222">qura-lʼčǝ-tɨt</ta>
            <ta e="T224" id="Seg_1827" s="T223">maːka-ču-gu</ta>
            <ta e="T225" id="Seg_1828" s="T224">nemci-la-n-se</ta>
            <ta e="T226" id="Seg_1829" s="T225">futbol-se</ta>
            <ta e="T227" id="Seg_1830" s="T226">na</ta>
            <ta e="T228" id="Seg_1831" s="T227">qum-la</ta>
            <ta e="T229" id="Seg_1832" s="T228">nemci-la-m</ta>
            <ta e="T230" id="Seg_1833" s="T229">ɨːlla-ptɨ-tɨt</ta>
            <ta e="T231" id="Seg_1834" s="T230">nɨːnɨ</ta>
            <ta e="T232" id="Seg_1835" s="T231">na</ta>
            <ta e="T233" id="Seg_1836" s="T232">qum-la-m</ta>
            <ta e="T234" id="Seg_1837" s="T233">nemci-la</ta>
            <ta e="T235" id="Seg_1838" s="T234">tʼaččɨ-lɨ-mbɨ-tɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1839" s="T1">this.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_1840" s="T2">be-PST-3SG.S</ta>
            <ta e="T4" id="Seg_1841" s="T3">where-EMPH</ta>
            <ta e="T5" id="Seg_1842" s="T4">German.PL-PL.[NOM]</ta>
            <ta e="T6" id="Seg_1843" s="T5">and</ta>
            <ta e="T7" id="Seg_1844" s="T6">Russian-DYA-PL.[NOM]</ta>
            <ta e="T8" id="Seg_1845" s="T7">war-TR-PST-3PL</ta>
            <ta e="T9" id="Seg_1846" s="T8">one</ta>
            <ta e="T10" id="Seg_1847" s="T9">NEG</ta>
            <ta e="T11" id="Seg_1848" s="T10">big</ta>
            <ta e="T12" id="Seg_1849" s="T11">town-DIM.[NOM]</ta>
            <ta e="T13" id="Seg_1850" s="T12">German.PL-PL.[NOM]</ta>
            <ta e="T14" id="Seg_1851" s="T13">much</ta>
            <ta e="T15" id="Seg_1852" s="T14">human.being-PL-ACC</ta>
            <ta e="T16" id="Seg_1853" s="T15">%%</ta>
            <ta e="T17" id="Seg_1854" s="T16">captivity-ILL</ta>
            <ta e="T18" id="Seg_1855" s="T17">take-DUR-PST-3PL</ta>
            <ta e="T19" id="Seg_1856" s="T18">this</ta>
            <ta e="T20" id="Seg_1857" s="T19">captivity-ILL</ta>
            <ta e="T21" id="Seg_1858" s="T20">take-PTCP.PST</ta>
            <ta e="T22" id="Seg_1859" s="T21">human.being-PL-ACC</ta>
            <ta e="T23" id="Seg_1860" s="T22">(s)he-PL.[NOM]</ta>
            <ta e="T24" id="Seg_1861" s="T23">some-ACC-INDEF</ta>
            <ta e="T25" id="Seg_1862" s="T24">kill-HAB-PST-3PL</ta>
            <ta e="T26" id="Seg_1863" s="T25">but</ta>
            <ta e="T27" id="Seg_1864" s="T26">some-ACC-INDEF</ta>
            <ta e="T28" id="Seg_1865" s="T27">work-%%-FRQ-IPFV3-DUR-HAB-PST-3PL</ta>
            <ta e="T29" id="Seg_1866" s="T28">work-INF</ta>
            <ta e="T30" id="Seg_1867" s="T29">force-HAB-PST-3PL</ta>
            <ta e="T31" id="Seg_1868" s="T30">this</ta>
            <ta e="T32" id="Seg_1869" s="T31">human.being-PL-GEN</ta>
            <ta e="T33" id="Seg_1870" s="T32">among</ta>
            <ta e="T34" id="Seg_1871" s="T33">be-PST-3SG.S</ta>
            <ta e="T35" id="Seg_1872" s="T34">good-ADVZ</ta>
            <ta e="T36" id="Seg_1873" s="T35">%%-DRV-PTCP.PRS</ta>
            <ta e="T37" id="Seg_1874" s="T36">football-INSTR</ta>
            <ta e="T38" id="Seg_1875" s="T37">human.being-PL.[NOM]</ta>
            <ta e="T39" id="Seg_1876" s="T38">German.PL-PL.[NOM]</ta>
            <ta e="T40" id="Seg_1877" s="T39">%%-HAB-PST.NAR-3PL</ta>
            <ta e="T41" id="Seg_1878" s="T40">this</ta>
            <ta e="T42" id="Seg_1879" s="T41">human.being-PL.[NOM]</ta>
            <ta e="T43" id="Seg_1880" s="T42">good-ADVZ</ta>
            <ta e="T44" id="Seg_1881" s="T43">%%-DRV-CO-3PL</ta>
            <ta e="T45" id="Seg_1882" s="T44">football-INSTR</ta>
            <ta e="T46" id="Seg_1883" s="T45">(s)he-EP-PL-EP-ACC</ta>
            <ta e="T47" id="Seg_1884" s="T46">NEG</ta>
            <ta e="T48" id="Seg_1885" s="T47">kill-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_1886" s="T48">but</ta>
            <ta e="T50" id="Seg_1887" s="T49">(s)he-EP-PL-EP-ACC</ta>
            <ta e="T51" id="Seg_1888" s="T50">let.go-PST.NAR-3PL</ta>
            <ta e="T52" id="Seg_1889" s="T51">so.that</ta>
            <ta e="T53" id="Seg_1890" s="T52">(s)he-EP-3SG.O.[NOM]</ta>
            <ta e="T54" id="Seg_1891" s="T53">freely-ADVZ</ta>
            <ta e="T55" id="Seg_1892" s="T54">go-IMP-3PL</ta>
            <ta e="T56" id="Seg_1893" s="T55">(s)he-EP-PL-ALL</ta>
            <ta e="T57" id="Seg_1894" s="T56">good</ta>
            <ta e="T58" id="Seg_1895" s="T57">house.[NOM]</ta>
            <ta e="T59" id="Seg_1896" s="T58">give-##DRV-3PL</ta>
            <ta e="T60" id="Seg_1897" s="T59">good</ta>
            <ta e="T61" id="Seg_1898" s="T60">clothing.[NOM]</ta>
            <ta e="T62" id="Seg_1899" s="T61">boots.[NOM]</ta>
            <ta e="T63" id="Seg_1900" s="T62">good</ta>
            <ta e="T64" id="Seg_1901" s="T63">eat-EP-FRQ-ACTN2</ta>
            <ta e="T65" id="Seg_1902" s="T64">now</ta>
            <ta e="T66" id="Seg_1903" s="T65">this</ta>
            <ta e="T67" id="Seg_1904" s="T66">ten</ta>
            <ta e="T68" id="Seg_1905" s="T67">human.being.[NOM]</ta>
            <ta e="T69" id="Seg_1906" s="T68">NEG</ta>
            <ta e="T70" id="Seg_1907" s="T69">want-PST-3PL</ta>
            <ta e="T71" id="Seg_1908" s="T70">German.PL-PL-ALL</ta>
            <ta e="T72" id="Seg_1909" s="T71">work-INF</ta>
            <ta e="T73" id="Seg_1910" s="T72">and</ta>
            <ta e="T74" id="Seg_1911" s="T73">(s)he-EP-PL-ADES</ta>
            <ta e="T75" id="Seg_1912" s="T74">stay-INF</ta>
            <ta e="T76" id="Seg_1913" s="T75">one</ta>
            <ta e="T77" id="Seg_1914" s="T76">we.PL.GEN</ta>
            <ta e="T78" id="Seg_1915" s="T77">human.being.[NOM]-1PL</ta>
            <ta e="T79" id="Seg_1916" s="T78">find-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1917" s="T79">town-LOC</ta>
            <ta e="T81" id="Seg_1918" s="T80">know-CO-PST.[3SG.S]</ta>
            <ta e="T82" id="Seg_1919" s="T81">human.being-EP-ACC</ta>
            <ta e="T83" id="Seg_1920" s="T82">this</ta>
            <ta e="T84" id="Seg_1921" s="T83">human.being.[NOM]</ta>
            <ta e="T85" id="Seg_1922" s="T84">want-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_1923" s="T85">this</ta>
            <ta e="T87" id="Seg_1924" s="T86">human.being-PL-ACC</ta>
            <ta e="T88" id="Seg_1925" s="T87">human.being-PL-ALL</ta>
            <ta e="T89" id="Seg_1926" s="T88">help-INF</ta>
            <ta e="T90" id="Seg_1927" s="T89">run.away-INF</ta>
            <ta e="T91" id="Seg_1928" s="T90">oneself.3SG</ta>
            <ta e="T92" id="Seg_1929" s="T91">human.being-PL-LOC</ta>
            <ta e="T93" id="Seg_1930" s="T92">run.away-INF</ta>
            <ta e="T94" id="Seg_1931" s="T93">(s)he-EP-PL-ALL</ta>
            <ta e="T95" id="Seg_1932" s="T94">one.should</ta>
            <ta e="T96" id="Seg_1933" s="T95">all</ta>
            <ta e="T97" id="Seg_1934" s="T96">one</ta>
            <ta e="T98" id="Seg_1935" s="T97">something-LOC</ta>
            <ta e="T99" id="Seg_1936" s="T98">if</ta>
            <ta e="T100" id="Seg_1937" s="T99">one</ta>
            <ta e="T101" id="Seg_1938" s="T100">human.being.[NOM]</ta>
            <ta e="T102" id="Seg_1939" s="T101">run.away.[3SG.S]</ta>
            <ta e="T103" id="Seg_1940" s="T102">that.[NOM]</ta>
            <ta e="T104" id="Seg_1941" s="T103">German.PL-PL.[NOM]</ta>
            <ta e="T105" id="Seg_1942" s="T104">shoot-FUT-3PL</ta>
            <ta e="T106" id="Seg_1943" s="T105">shoot-PFV-FUT-3PL</ta>
            <ta e="T107" id="Seg_1944" s="T106">all</ta>
            <ta e="T108" id="Seg_1945" s="T107">this</ta>
            <ta e="T109" id="Seg_1946" s="T108">ten</ta>
            <ta e="T110" id="Seg_1947" s="T109">human.being-PL-ACC</ta>
            <ta e="T111" id="Seg_1948" s="T110">this</ta>
            <ta e="T112" id="Seg_1949" s="T111">ten</ta>
            <ta e="T113" id="Seg_1950" s="T112">human.being-PROL-among</ta>
            <ta e="T114" id="Seg_1951" s="T113">one</ta>
            <ta e="T115" id="Seg_1952" s="T114">NEG</ta>
            <ta e="T116" id="Seg_1953" s="T115">good</ta>
            <ta e="T117" id="Seg_1954" s="T116">human.being.[NOM]</ta>
            <ta e="T118" id="Seg_1955" s="T117">be-PST-3SG.S</ta>
            <ta e="T119" id="Seg_1956" s="T118">(s)he</ta>
            <ta e="T120" id="Seg_1957" s="T119">also</ta>
            <ta e="T121" id="Seg_1958" s="T120">Russian</ta>
            <ta e="T122" id="Seg_1959" s="T121">be-PST-3SG.S</ta>
            <ta e="T123" id="Seg_1960" s="T122">(s)he-EP-ALL</ta>
            <ta e="T124" id="Seg_1961" s="T123">German.PL-PL.[NOM]</ta>
            <ta e="T125" id="Seg_1962" s="T124">much</ta>
            <ta e="T126" id="Seg_1963" s="T125">money-ACC</ta>
            <ta e="T127" id="Seg_1964" s="T126">pay-HAB-PST-3PL</ta>
            <ta e="T128" id="Seg_1965" s="T127">pay-HAB-PST.NAR-3PL</ta>
            <ta e="T129" id="Seg_1966" s="T128">this</ta>
            <ta e="T130" id="Seg_1967" s="T129">human.being.[NOM]</ta>
            <ta e="T131" id="Seg_1968" s="T130">watch.for-HAB-PST.NAR-3SG.O</ta>
            <ta e="T132" id="Seg_1969" s="T131">(s)he-EP-PL-EP-ACC</ta>
            <ta e="T133" id="Seg_1970" s="T132">look-DUR-HAB-PST.NAR-3SG.O</ta>
            <ta e="T134" id="Seg_1971" s="T133">what</ta>
            <ta e="T135" id="Seg_1972" s="T134">which-ADVZ</ta>
            <ta e="T136" id="Seg_1973" s="T135">think.[3SG.S]</ta>
            <ta e="T137" id="Seg_1974" s="T136">what</ta>
            <ta e="T138" id="Seg_1975" s="T137">which-ADVZ</ta>
            <ta e="T139" id="Seg_1976" s="T138">do-HAB-IPFV-3SG.O</ta>
            <ta e="T140" id="Seg_1977" s="T139">what</ta>
            <ta e="T141" id="Seg_1978" s="T140">where</ta>
            <ta e="T142" id="Seg_1979" s="T141">go-3SG.S</ta>
            <ta e="T143" id="Seg_1980" s="T142">what-human.being</ta>
            <ta e="T144" id="Seg_1981" s="T143">what.[NOM]</ta>
            <ta e="T145" id="Seg_1982" s="T144">do-INFER-3SG.O</ta>
            <ta e="T146" id="Seg_1983" s="T145">what-human.being.[NOM]</ta>
            <ta e="T147" id="Seg_1984" s="T146">what</ta>
            <ta e="T148" id="Seg_1985" s="T147">say-EP-3SG.O</ta>
            <ta e="T149" id="Seg_1986" s="T148">%%-ACC</ta>
            <ta e="T150" id="Seg_1987" s="T149">(s)he.[NOM]</ta>
            <ta e="T151" id="Seg_1988" s="T150">all</ta>
            <ta e="T152" id="Seg_1989" s="T151">say-HAB-PST.NAR-3SG.O</ta>
            <ta e="T153" id="Seg_1990" s="T152">German.PL-PL-GEN-ALL</ta>
            <ta e="T154" id="Seg_1991" s="T153">this</ta>
            <ta e="T155" id="Seg_1992" s="T154">ten</ta>
            <ta e="T156" id="Seg_1993" s="T155">human.being.[NOM]</ta>
            <ta e="T157" id="Seg_1994" s="T156">when</ta>
            <ta e="T158" id="Seg_1995" s="T157">go.away-PST.NAR-3PL</ta>
            <ta e="T159" id="Seg_1996" s="T158">%%-TR-INF</ta>
            <ta e="T160" id="Seg_1997" s="T159">this</ta>
            <ta e="T161" id="Seg_1998" s="T160">NEG</ta>
            <ta e="T162" id="Seg_1999" s="T161">good</ta>
            <ta e="T163" id="Seg_2000" s="T162">human.being.[NOM]</ta>
            <ta e="T164" id="Seg_2001" s="T163">(s)he-EP-PL.[NOM]</ta>
            <ta e="T165" id="Seg_2002" s="T164">pocket-PL-LOC-3PL</ta>
            <ta e="T166" id="Seg_2003" s="T165">long</ta>
            <ta e="T167" id="Seg_2004" s="T166">look.for-FRQ-CVB1</ta>
            <ta e="T168" id="Seg_2005" s="T167">find-PST.NAR-3SG.O</ta>
            <ta e="T169" id="Seg_2006" s="T168">(s)he-EP-PL.[NOM]</ta>
            <ta e="T170" id="Seg_2007" s="T169">paper-EP-PL-ACC-3PL</ta>
            <ta e="T171" id="Seg_2008" s="T170">(s)he.[NOM]</ta>
            <ta e="T172" id="Seg_2009" s="T171">look-PST.NAR-3SG.O</ta>
            <ta e="T173" id="Seg_2010" s="T172">this</ta>
            <ta e="T174" id="Seg_2011" s="T173">paper-EP-PL-ACC</ta>
            <ta e="T175" id="Seg_2012" s="T174">now</ta>
            <ta e="T176" id="Seg_2013" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2014" s="T176">take-DUR-1PL</ta>
            <ta e="T178" id="Seg_2015" s="T177">back</ta>
            <ta e="T179" id="Seg_2016" s="T178">put-PST.NAR-3SG.O</ta>
            <ta e="T180" id="Seg_2017" s="T179">this-EMPH3</ta>
            <ta e="T181" id="Seg_2018" s="T180">pocket-PL-ILL</ta>
            <ta e="T182" id="Seg_2019" s="T181">but</ta>
            <ta e="T183" id="Seg_2020" s="T182">German.PL-PL-GEN-ALL</ta>
            <ta e="T184" id="Seg_2021" s="T183">%%-ACC</ta>
            <ta e="T185" id="Seg_2022" s="T184">all</ta>
            <ta e="T186" id="Seg_2023" s="T185">say-DUR-3SG.O</ta>
            <ta e="T187" id="Seg_2024" s="T186">but</ta>
            <ta e="T188" id="Seg_2025" s="T187">this</ta>
            <ta e="T189" id="Seg_2026" s="T188">ten</ta>
            <ta e="T190" id="Seg_2027" s="T189">human.being.[NOM]</ta>
            <ta e="T191" id="Seg_2028" s="T190">NEG</ta>
            <ta e="T192" id="Seg_2029" s="T191">know-DRV-PST-3PL</ta>
            <ta e="T193" id="Seg_2030" s="T192">what</ta>
            <ta e="T194" id="Seg_2031" s="T193">(s)he-EP-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2032" s="T194">%%</ta>
            <ta e="T196" id="Seg_2033" s="T195">German.PL-PL-ALL</ta>
            <ta e="T197" id="Seg_2034" s="T196">already</ta>
            <ta e="T198" id="Seg_2035" s="T197">say-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_2036" s="T198">what</ta>
            <ta e="T200" id="Seg_2037" s="T199">this</ta>
            <ta e="T201" id="Seg_2038" s="T200">human.being-PL.[NOM]</ta>
            <ta e="T202" id="Seg_2039" s="T201">run.away-INF</ta>
            <ta e="T203" id="Seg_2040" s="T202">want-3PL</ta>
            <ta e="T204" id="Seg_2041" s="T203">then</ta>
            <ta e="T205" id="Seg_2042" s="T204">this</ta>
            <ta e="T206" id="Seg_2043" s="T205">human.being-PL.[NOM]</ta>
            <ta e="T207" id="Seg_2044" s="T206">run.away-CVB1</ta>
            <ta e="T208" id="Seg_2045" s="T207">go.away-CO-3PL</ta>
            <ta e="T209" id="Seg_2046" s="T208">and</ta>
            <ta e="T210" id="Seg_2047" s="T209">run.away-CVB1</ta>
            <ta e="T211" id="Seg_2048" s="T210">go.away-PST-3PL</ta>
            <ta e="T212" id="Seg_2049" s="T211">wagon-ILL</ta>
            <ta e="T213" id="Seg_2050" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2051" s="T213">achieve-3PL</ta>
            <ta e="T215" id="Seg_2052" s="T214">(s)he-EP-PL-EP-ACC</ta>
            <ta e="T216" id="Seg_2053" s="T215">catch-CO-3PL</ta>
            <ta e="T217" id="Seg_2054" s="T216">German.PL-PL.[NOM]</ta>
            <ta e="T218" id="Seg_2055" s="T217">back</ta>
            <ta e="T219" id="Seg_2056" s="T218">bring-CO-3PL</ta>
            <ta e="T220" id="Seg_2057" s="T219">good-what-PL-GEN-INSTR</ta>
            <ta e="T221" id="Seg_2058" s="T220">dress-EP-DUR-FUT-3PL</ta>
            <ta e="T222" id="Seg_2059" s="T221">and</ta>
            <ta e="T223" id="Seg_2060" s="T222">command-PFV-3PL</ta>
            <ta e="T224" id="Seg_2061" s="T223">%%-TR-INF</ta>
            <ta e="T225" id="Seg_2062" s="T224">German.PL-PL-GEN-COM</ta>
            <ta e="T226" id="Seg_2063" s="T225">football-INSTR</ta>
            <ta e="T227" id="Seg_2064" s="T226">this</ta>
            <ta e="T228" id="Seg_2065" s="T227">human.being-PL.[NOM]</ta>
            <ta e="T229" id="Seg_2066" s="T228">German.PL-PL-ACC</ta>
            <ta e="T230" id="Seg_2067" s="T229">%%-CAUS-3PL</ta>
            <ta e="T231" id="Seg_2068" s="T230">then</ta>
            <ta e="T232" id="Seg_2069" s="T231">this</ta>
            <ta e="T233" id="Seg_2070" s="T232">human.being-PL-ACC</ta>
            <ta e="T234" id="Seg_2071" s="T233">German.PL-PL.[NOM]</ta>
            <ta e="T235" id="Seg_2072" s="T234">shoot-RES-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2073" s="T1">этот.[NOM]-3SG</ta>
            <ta e="T3" id="Seg_2074" s="T2">быть-PST-3SG.S</ta>
            <ta e="T4" id="Seg_2075" s="T3">где-EMPH</ta>
            <ta e="T5" id="Seg_2076" s="T4">немец.PL-PL.[NOM]</ta>
            <ta e="T6" id="Seg_2077" s="T5">и</ta>
            <ta e="T7" id="Seg_2078" s="T6">русский-DYA-PL.[NOM]</ta>
            <ta e="T8" id="Seg_2079" s="T7">война-TR-PST-3PL</ta>
            <ta e="T9" id="Seg_2080" s="T8">один</ta>
            <ta e="T10" id="Seg_2081" s="T9">NEG</ta>
            <ta e="T11" id="Seg_2082" s="T10">большой</ta>
            <ta e="T12" id="Seg_2083" s="T11">город-DIM.[NOM]</ta>
            <ta e="T13" id="Seg_2084" s="T12">немец.PL-PL.[NOM]</ta>
            <ta e="T14" id="Seg_2085" s="T13">много</ta>
            <ta e="T15" id="Seg_2086" s="T14">человек-PL-ACC</ta>
            <ta e="T16" id="Seg_2087" s="T15">%%</ta>
            <ta e="T17" id="Seg_2088" s="T16">плен-ILL</ta>
            <ta e="T18" id="Seg_2089" s="T17">взять-DUR-PST-3PL</ta>
            <ta e="T19" id="Seg_2090" s="T18">этот</ta>
            <ta e="T20" id="Seg_2091" s="T19">плен-ILL</ta>
            <ta e="T21" id="Seg_2092" s="T20">взять-PTCP.PST</ta>
            <ta e="T22" id="Seg_2093" s="T21">человек-PL-ACC</ta>
            <ta e="T23" id="Seg_2094" s="T22">он(а)-PL.[NOM]</ta>
            <ta e="T24" id="Seg_2095" s="T23">некоторые-ACC-INDEF</ta>
            <ta e="T25" id="Seg_2096" s="T24">убить-HAB-PST-3PL</ta>
            <ta e="T26" id="Seg_2097" s="T25">а</ta>
            <ta e="T27" id="Seg_2098" s="T26">некоторые-ACC-INDEF</ta>
            <ta e="T28" id="Seg_2099" s="T27">работать-%%-FRQ-IPFV3-DUR-HAB-PST-3PL</ta>
            <ta e="T29" id="Seg_2100" s="T28">работать-INF</ta>
            <ta e="T30" id="Seg_2101" s="T29">заставить-HAB-PST-3PL</ta>
            <ta e="T31" id="Seg_2102" s="T30">этот</ta>
            <ta e="T32" id="Seg_2103" s="T31">человек-PL-GEN</ta>
            <ta e="T33" id="Seg_2104" s="T32">среди</ta>
            <ta e="T34" id="Seg_2105" s="T33">быть-PST-3SG.S</ta>
            <ta e="T35" id="Seg_2106" s="T34">хороший-ADVZ</ta>
            <ta e="T36" id="Seg_2107" s="T35">%%-DRV-PTCP.PRS</ta>
            <ta e="T37" id="Seg_2108" s="T36">футбол-INSTR</ta>
            <ta e="T38" id="Seg_2109" s="T37">человек-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2110" s="T38">немец.PL-PL.[NOM]</ta>
            <ta e="T40" id="Seg_2111" s="T39">%%-HAB-PST.NAR-3PL</ta>
            <ta e="T41" id="Seg_2112" s="T40">этот</ta>
            <ta e="T42" id="Seg_2113" s="T41">человек-PL.[NOM]</ta>
            <ta e="T43" id="Seg_2114" s="T42">хороший-ADVZ</ta>
            <ta e="T44" id="Seg_2115" s="T43">%%-DRV-CO-3PL</ta>
            <ta e="T45" id="Seg_2116" s="T44">футбол-INSTR</ta>
            <ta e="T46" id="Seg_2117" s="T45">он(а)-EP-PL-EP-ACC</ta>
            <ta e="T47" id="Seg_2118" s="T46">NEG</ta>
            <ta e="T48" id="Seg_2119" s="T47">убить-PST.NAR-3PL</ta>
            <ta e="T49" id="Seg_2120" s="T48">а</ta>
            <ta e="T50" id="Seg_2121" s="T49">он(а)-EP-PL-EP-ACC</ta>
            <ta e="T51" id="Seg_2122" s="T50">пустить-PST.NAR-3PL</ta>
            <ta e="T52" id="Seg_2123" s="T51">чтобы</ta>
            <ta e="T53" id="Seg_2124" s="T52">он(а)-EP-3SG.O.[NOM]</ta>
            <ta e="T54" id="Seg_2125" s="T53">вольно-ADVZ</ta>
            <ta e="T55" id="Seg_2126" s="T54">ходить-IMP-3PL</ta>
            <ta e="T56" id="Seg_2127" s="T55">он(а)-EP-PL-ALL</ta>
            <ta e="T57" id="Seg_2128" s="T56">хороший</ta>
            <ta e="T58" id="Seg_2129" s="T57">дом.[NOM]</ta>
            <ta e="T59" id="Seg_2130" s="T58">дать-DRV-3PL</ta>
            <ta e="T60" id="Seg_2131" s="T59">хороший</ta>
            <ta e="T61" id="Seg_2132" s="T60">одежда.[NOM]</ta>
            <ta e="T62" id="Seg_2133" s="T61">чирки.[NOM]</ta>
            <ta e="T63" id="Seg_2134" s="T62">хороший</ta>
            <ta e="T64" id="Seg_2135" s="T63">съесть-EP-FRQ-ACTN2</ta>
            <ta e="T65" id="Seg_2136" s="T64">ну</ta>
            <ta e="T66" id="Seg_2137" s="T65">этот</ta>
            <ta e="T67" id="Seg_2138" s="T66">десять</ta>
            <ta e="T68" id="Seg_2139" s="T67">человек.[NOM]</ta>
            <ta e="T69" id="Seg_2140" s="T68">NEG</ta>
            <ta e="T70" id="Seg_2141" s="T69">хотеть-PST-3PL</ta>
            <ta e="T71" id="Seg_2142" s="T70">немец.PL-PL-ALL</ta>
            <ta e="T72" id="Seg_2143" s="T71">работать-INF</ta>
            <ta e="T73" id="Seg_2144" s="T72">и</ta>
            <ta e="T74" id="Seg_2145" s="T73">он(а)-EP-PL-ADES</ta>
            <ta e="T75" id="Seg_2146" s="T74">остаться-INF</ta>
            <ta e="T76" id="Seg_2147" s="T75">один</ta>
            <ta e="T77" id="Seg_2148" s="T76">мы.PL.GEN</ta>
            <ta e="T78" id="Seg_2149" s="T77">человек.[NOM]-1PL</ta>
            <ta e="T79" id="Seg_2150" s="T78">найти-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_2151" s="T79">город-LOC</ta>
            <ta e="T81" id="Seg_2152" s="T80">знать-CO-PST.[3SG.S]</ta>
            <ta e="T82" id="Seg_2153" s="T81">человек-EP-ACC</ta>
            <ta e="T83" id="Seg_2154" s="T82">этот</ta>
            <ta e="T84" id="Seg_2155" s="T83">человек.[NOM]</ta>
            <ta e="T85" id="Seg_2156" s="T84">хотеть-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_2157" s="T85">этот</ta>
            <ta e="T87" id="Seg_2158" s="T86">человек-PL-ACC</ta>
            <ta e="T88" id="Seg_2159" s="T87">человек-PL-ALL</ta>
            <ta e="T89" id="Seg_2160" s="T88">помочь-INF</ta>
            <ta e="T90" id="Seg_2161" s="T89">убежать-INF</ta>
            <ta e="T91" id="Seg_2162" s="T90">сам.3SG</ta>
            <ta e="T92" id="Seg_2163" s="T91">человек-PL-LOC</ta>
            <ta e="T93" id="Seg_2164" s="T92">убежать-INF</ta>
            <ta e="T94" id="Seg_2165" s="T93">он(а)-EP-PL-ALL</ta>
            <ta e="T95" id="Seg_2166" s="T94">надо</ta>
            <ta e="T96" id="Seg_2167" s="T95">весь</ta>
            <ta e="T97" id="Seg_2168" s="T96">один</ta>
            <ta e="T98" id="Seg_2169" s="T97">нечто-LOC</ta>
            <ta e="T99" id="Seg_2170" s="T98">если</ta>
            <ta e="T100" id="Seg_2171" s="T99">один</ta>
            <ta e="T101" id="Seg_2172" s="T100">человек.[NOM]</ta>
            <ta e="T102" id="Seg_2173" s="T101">убежать.[3SG.S]</ta>
            <ta e="T103" id="Seg_2174" s="T102">тот.[NOM]</ta>
            <ta e="T104" id="Seg_2175" s="T103">немец.PL-PL.[NOM]</ta>
            <ta e="T105" id="Seg_2176" s="T104">стрелять-FUT-3PL</ta>
            <ta e="T106" id="Seg_2177" s="T105">стрелять-PFV-FUT-3PL</ta>
            <ta e="T107" id="Seg_2178" s="T106">весь</ta>
            <ta e="T108" id="Seg_2179" s="T107">этот</ta>
            <ta e="T109" id="Seg_2180" s="T108">десять</ta>
            <ta e="T110" id="Seg_2181" s="T109">человек-PL-ACC</ta>
            <ta e="T111" id="Seg_2182" s="T110">этот</ta>
            <ta e="T112" id="Seg_2183" s="T111">десять</ta>
            <ta e="T113" id="Seg_2184" s="T112">человек-PROL-среди</ta>
            <ta e="T114" id="Seg_2185" s="T113">один</ta>
            <ta e="T115" id="Seg_2186" s="T114">NEG</ta>
            <ta e="T116" id="Seg_2187" s="T115">хороший</ta>
            <ta e="T117" id="Seg_2188" s="T116">человек.[NOM]</ta>
            <ta e="T118" id="Seg_2189" s="T117">быть-PST-3SG.S</ta>
            <ta e="T119" id="Seg_2190" s="T118">он(а)</ta>
            <ta e="T120" id="Seg_2191" s="T119">тоже</ta>
            <ta e="T121" id="Seg_2192" s="T120">русский</ta>
            <ta e="T122" id="Seg_2193" s="T121">быть-PST-3SG.S</ta>
            <ta e="T123" id="Seg_2194" s="T122">он(а)-EP-ALL</ta>
            <ta e="T124" id="Seg_2195" s="T123">немец.PL-PL.[NOM]</ta>
            <ta e="T125" id="Seg_2196" s="T124">много</ta>
            <ta e="T126" id="Seg_2197" s="T125">деньги-ACC</ta>
            <ta e="T127" id="Seg_2198" s="T126">платить-HAB-PST-3PL</ta>
            <ta e="T128" id="Seg_2199" s="T127">платить-HAB-PST.NAR-3PL</ta>
            <ta e="T129" id="Seg_2200" s="T128">этот</ta>
            <ta e="T130" id="Seg_2201" s="T129">человек.[NOM]</ta>
            <ta e="T131" id="Seg_2202" s="T130">караулить-HAB-PST.NAR-3SG.O</ta>
            <ta e="T132" id="Seg_2203" s="T131">он(а)-EP-PL-EP-ACC</ta>
            <ta e="T133" id="Seg_2204" s="T132">смотреть-DUR-HAB-PST.NAR-3SG.O</ta>
            <ta e="T134" id="Seg_2205" s="T133">что</ta>
            <ta e="T135" id="Seg_2206" s="T134">какой-ADVZ</ta>
            <ta e="T136" id="Seg_2207" s="T135">думать.[3SG.S]</ta>
            <ta e="T137" id="Seg_2208" s="T136">что</ta>
            <ta e="T138" id="Seg_2209" s="T137">какой-ADVZ</ta>
            <ta e="T139" id="Seg_2210" s="T138">делать-HAB-IPFV-3SG.O</ta>
            <ta e="T140" id="Seg_2211" s="T139">что</ta>
            <ta e="T141" id="Seg_2212" s="T140">где</ta>
            <ta e="T142" id="Seg_2213" s="T141">ходить-3SG.S</ta>
            <ta e="T143" id="Seg_2214" s="T142">что-человек</ta>
            <ta e="T144" id="Seg_2215" s="T143">что.[NOM]</ta>
            <ta e="T145" id="Seg_2216" s="T144">делать-INFER-3SG.O</ta>
            <ta e="T146" id="Seg_2217" s="T145">что-человек.[NOM]</ta>
            <ta e="T147" id="Seg_2218" s="T146">что</ta>
            <ta e="T148" id="Seg_2219" s="T147">сказать-EP-3SG.O</ta>
            <ta e="T149" id="Seg_2220" s="T148">%%-ACC</ta>
            <ta e="T150" id="Seg_2221" s="T149">он(а).[NOM]</ta>
            <ta e="T151" id="Seg_2222" s="T150">весь</ta>
            <ta e="T152" id="Seg_2223" s="T151">сказать-HAB-PST.NAR-3SG.O</ta>
            <ta e="T153" id="Seg_2224" s="T152">немец.PL-PL-GEN-ALL</ta>
            <ta e="T154" id="Seg_2225" s="T153">этот</ta>
            <ta e="T155" id="Seg_2226" s="T154">десять</ta>
            <ta e="T156" id="Seg_2227" s="T155">человек.[NOM]</ta>
            <ta e="T157" id="Seg_2228" s="T156">когда</ta>
            <ta e="T158" id="Seg_2229" s="T157">уйти-PST.NAR-3PL</ta>
            <ta e="T159" id="Seg_2230" s="T158">%%-TR-INF</ta>
            <ta e="T160" id="Seg_2231" s="T159">этот</ta>
            <ta e="T161" id="Seg_2232" s="T160">NEG</ta>
            <ta e="T162" id="Seg_2233" s="T161">хороший</ta>
            <ta e="T163" id="Seg_2234" s="T162">человек.[NOM]</ta>
            <ta e="T164" id="Seg_2235" s="T163">он(а)-EP-PL.[NOM]</ta>
            <ta e="T165" id="Seg_2236" s="T164">карман-PL-LOC-3PL</ta>
            <ta e="T166" id="Seg_2237" s="T165">долго</ta>
            <ta e="T167" id="Seg_2238" s="T166">искать-FRQ-CVB1</ta>
            <ta e="T168" id="Seg_2239" s="T167">найти-PST.NAR-3SG.O</ta>
            <ta e="T169" id="Seg_2240" s="T168">он(а)-EP-PL.[NOM]</ta>
            <ta e="T170" id="Seg_2241" s="T169">бумага-EP-PL-ACC-3PL</ta>
            <ta e="T171" id="Seg_2242" s="T170">он(а).[NOM]</ta>
            <ta e="T172" id="Seg_2243" s="T171">смотреть-PST.NAR-3SG.O</ta>
            <ta e="T173" id="Seg_2244" s="T172">этот</ta>
            <ta e="T174" id="Seg_2245" s="T173">бумага-EP-PL-ACC</ta>
            <ta e="T175" id="Seg_2246" s="T174">ну</ta>
            <ta e="T176" id="Seg_2247" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2248" s="T176">взять-DUR-1PL</ta>
            <ta e="T178" id="Seg_2249" s="T177">назад</ta>
            <ta e="T179" id="Seg_2250" s="T178">положить-PST.NAR-3SG.O</ta>
            <ta e="T180" id="Seg_2251" s="T179">этот-EMPH3</ta>
            <ta e="T181" id="Seg_2252" s="T180">карман-PL-ILL</ta>
            <ta e="T182" id="Seg_2253" s="T181">а</ta>
            <ta e="T183" id="Seg_2254" s="T182">немец.PL-PL-GEN-ALL</ta>
            <ta e="T184" id="Seg_2255" s="T183">%%-ACC</ta>
            <ta e="T185" id="Seg_2256" s="T184">весь</ta>
            <ta e="T186" id="Seg_2257" s="T185">сказать-DUR-3SG.O</ta>
            <ta e="T187" id="Seg_2258" s="T186">а</ta>
            <ta e="T188" id="Seg_2259" s="T187">этот</ta>
            <ta e="T189" id="Seg_2260" s="T188">десять</ta>
            <ta e="T190" id="Seg_2261" s="T189">человек.[NOM]</ta>
            <ta e="T191" id="Seg_2262" s="T190">NEG</ta>
            <ta e="T192" id="Seg_2263" s="T191">знать-DRV-PST-3PL</ta>
            <ta e="T193" id="Seg_2264" s="T192">что</ta>
            <ta e="T194" id="Seg_2265" s="T193">он(а)-EP-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2266" s="T194">%%</ta>
            <ta e="T196" id="Seg_2267" s="T195">немец.PL-PL-ALL</ta>
            <ta e="T197" id="Seg_2268" s="T196">уже</ta>
            <ta e="T198" id="Seg_2269" s="T197">сказать-PST.NAR.[3SG.S]</ta>
            <ta e="T199" id="Seg_2270" s="T198">что</ta>
            <ta e="T200" id="Seg_2271" s="T199">этот</ta>
            <ta e="T201" id="Seg_2272" s="T200">человек-PL.[NOM]</ta>
            <ta e="T202" id="Seg_2273" s="T201">убежать-INF</ta>
            <ta e="T203" id="Seg_2274" s="T202">хотеть-3PL</ta>
            <ta e="T204" id="Seg_2275" s="T203">потом</ta>
            <ta e="T205" id="Seg_2276" s="T204">этот</ta>
            <ta e="T206" id="Seg_2277" s="T205">человек-PL.[NOM]</ta>
            <ta e="T207" id="Seg_2278" s="T206">убежать-CVB1</ta>
            <ta e="T208" id="Seg_2279" s="T207">уйти-CO-3PL</ta>
            <ta e="T209" id="Seg_2280" s="T208">и</ta>
            <ta e="T210" id="Seg_2281" s="T209">убежать-CVB1</ta>
            <ta e="T211" id="Seg_2282" s="T210">уйти-PST-3PL</ta>
            <ta e="T212" id="Seg_2283" s="T211">вагон-ILL</ta>
            <ta e="T213" id="Seg_2284" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2285" s="T213">достичь-3PL</ta>
            <ta e="T215" id="Seg_2286" s="T214">он(а)-EP-PL-EP-ACC</ta>
            <ta e="T216" id="Seg_2287" s="T215">поймать-CO-3PL</ta>
            <ta e="T217" id="Seg_2288" s="T216">немец.PL-PL.[NOM]</ta>
            <ta e="T218" id="Seg_2289" s="T217">назад</ta>
            <ta e="T219" id="Seg_2290" s="T218">принести-CO-3PL</ta>
            <ta e="T220" id="Seg_2291" s="T219">хороший-что-PL-GEN-INSTR</ta>
            <ta e="T221" id="Seg_2292" s="T220">одеться-EP-DUR-FUT-3PL</ta>
            <ta e="T222" id="Seg_2293" s="T221">и</ta>
            <ta e="T223" id="Seg_2294" s="T222">приказать-PFV-3PL</ta>
            <ta e="T224" id="Seg_2295" s="T223">%%-TR-INF</ta>
            <ta e="T225" id="Seg_2296" s="T224">немец.PL-PL-GEN-COM</ta>
            <ta e="T226" id="Seg_2297" s="T225">футбол-INSTR</ta>
            <ta e="T227" id="Seg_2298" s="T226">этот</ta>
            <ta e="T228" id="Seg_2299" s="T227">человек-PL.[NOM]</ta>
            <ta e="T229" id="Seg_2300" s="T228">немец.PL-PL-ACC</ta>
            <ta e="T230" id="Seg_2301" s="T229">%%-CAUS-3PL</ta>
            <ta e="T231" id="Seg_2302" s="T230">потом</ta>
            <ta e="T232" id="Seg_2303" s="T231">этот</ta>
            <ta e="T233" id="Seg_2304" s="T232">человек-PL-ACC</ta>
            <ta e="T234" id="Seg_2305" s="T233">немец.PL-PL.[NOM]</ta>
            <ta e="T235" id="Seg_2306" s="T234">стрелять-RES-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2307" s="T1">dem.[n:case]-n:poss</ta>
            <ta e="T3" id="Seg_2308" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2309" s="T3">interrog-clit</ta>
            <ta e="T5" id="Seg_2310" s="T4">n-n:num.[n:case]</ta>
            <ta e="T6" id="Seg_2311" s="T5">conj</ta>
            <ta e="T7" id="Seg_2312" s="T6">n-n&gt;n-n:num.[n:case]</ta>
            <ta e="T8" id="Seg_2313" s="T7">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_2314" s="T8">num</ta>
            <ta e="T10" id="Seg_2315" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_2316" s="T10">adj</ta>
            <ta e="T12" id="Seg_2317" s="T11">n-n&gt;n.[n:case]</ta>
            <ta e="T13" id="Seg_2318" s="T12">n-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_2319" s="T13">quant</ta>
            <ta e="T15" id="Seg_2320" s="T14">n-n:num-n:case</ta>
            <ta e="T16" id="Seg_2321" s="T15">n</ta>
            <ta e="T17" id="Seg_2322" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_2323" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_2324" s="T18">dem</ta>
            <ta e="T20" id="Seg_2325" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_2326" s="T20">v-v&gt;ptcp</ta>
            <ta e="T22" id="Seg_2327" s="T21">n-n:num-n:case</ta>
            <ta e="T23" id="Seg_2328" s="T22">pers-n:num.[n:case]</ta>
            <ta e="T24" id="Seg_2329" s="T23">quant-n:case-clit</ta>
            <ta e="T25" id="Seg_2330" s="T24">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2331" s="T25">conj</ta>
            <ta e="T27" id="Seg_2332" s="T26">quant-n:case-clit</ta>
            <ta e="T28" id="Seg_2333" s="T27">v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_2334" s="T28">v-v:inf</ta>
            <ta e="T30" id="Seg_2335" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_2336" s="T30">dem</ta>
            <ta e="T32" id="Seg_2337" s="T31">n-n:num-n:case</ta>
            <ta e="T33" id="Seg_2338" s="T32">pp</ta>
            <ta e="T34" id="Seg_2339" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_2340" s="T34">adj-adj&gt;adv</ta>
            <ta e="T36" id="Seg_2341" s="T35">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T37" id="Seg_2342" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_2343" s="T37">n-n:num.[n:case]</ta>
            <ta e="T39" id="Seg_2344" s="T38">n-n:num.[n:case]</ta>
            <ta e="T40" id="Seg_2345" s="T39">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_2346" s="T40">dem</ta>
            <ta e="T42" id="Seg_2347" s="T41">n-n:num.[n:case]</ta>
            <ta e="T43" id="Seg_2348" s="T42">adj-adj&gt;adv</ta>
            <ta e="T44" id="Seg_2349" s="T43">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T45" id="Seg_2350" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2351" s="T45">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T47" id="Seg_2352" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_2353" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2354" s="T48">conj</ta>
            <ta e="T50" id="Seg_2355" s="T49">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T51" id="Seg_2356" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_2357" s="T51">conj</ta>
            <ta e="T53" id="Seg_2358" s="T52">pers-n:ins-v:pn.[n:case]</ta>
            <ta e="T54" id="Seg_2359" s="T53">adv-adj&gt;adv</ta>
            <ta e="T55" id="Seg_2360" s="T54">v-v:mood-v:pn</ta>
            <ta e="T56" id="Seg_2361" s="T55">pers-n:ins-n:num-n:case</ta>
            <ta e="T57" id="Seg_2362" s="T56">adj</ta>
            <ta e="T58" id="Seg_2363" s="T57">n.[n:case]</ta>
            <ta e="T59" id="Seg_2364" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_2365" s="T59">adj</ta>
            <ta e="T61" id="Seg_2366" s="T60">n.[n:case]</ta>
            <ta e="T62" id="Seg_2367" s="T61">n.[n:case]</ta>
            <ta e="T63" id="Seg_2368" s="T62">adj</ta>
            <ta e="T64" id="Seg_2369" s="T63">v-n:ins-v&gt;v-v&gt;n</ta>
            <ta e="T65" id="Seg_2370" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2371" s="T65">dem</ta>
            <ta e="T67" id="Seg_2372" s="T66">num</ta>
            <ta e="T68" id="Seg_2373" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_2374" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2375" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_2376" s="T70">n-n:num-n:case</ta>
            <ta e="T72" id="Seg_2377" s="T71">v-v:inf</ta>
            <ta e="T73" id="Seg_2378" s="T72">conj</ta>
            <ta e="T74" id="Seg_2379" s="T73">pers-n:ins-n:num-n:case</ta>
            <ta e="T75" id="Seg_2380" s="T74">v-v:inf</ta>
            <ta e="T76" id="Seg_2381" s="T75">num</ta>
            <ta e="T77" id="Seg_2382" s="T76">pers</ta>
            <ta e="T78" id="Seg_2383" s="T77">n.[n:case]-n:poss</ta>
            <ta e="T79" id="Seg_2384" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_2385" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2386" s="T80">v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T82" id="Seg_2387" s="T81">n-n:ins-n:case</ta>
            <ta e="T83" id="Seg_2388" s="T82">dem</ta>
            <ta e="T84" id="Seg_2389" s="T83">n.[n:case]</ta>
            <ta e="T85" id="Seg_2390" s="T84">v-v:tense.[v:pn]</ta>
            <ta e="T86" id="Seg_2391" s="T85">dem</ta>
            <ta e="T87" id="Seg_2392" s="T86">n-n:num-n:case</ta>
            <ta e="T88" id="Seg_2393" s="T87">n-n:num-n:case</ta>
            <ta e="T89" id="Seg_2394" s="T88">v-v:inf</ta>
            <ta e="T90" id="Seg_2395" s="T89">v-v:inf</ta>
            <ta e="T91" id="Seg_2396" s="T90">emphpro</ta>
            <ta e="T92" id="Seg_2397" s="T91">n-n:num-n:case</ta>
            <ta e="T93" id="Seg_2398" s="T92">v-v:inf</ta>
            <ta e="T94" id="Seg_2399" s="T93">pers-n:ins-n:num-n:case</ta>
            <ta e="T95" id="Seg_2400" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2401" s="T95">quant</ta>
            <ta e="T97" id="Seg_2402" s="T96">num</ta>
            <ta e="T98" id="Seg_2403" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_2404" s="T98">conj</ta>
            <ta e="T100" id="Seg_2405" s="T99">num</ta>
            <ta e="T101" id="Seg_2406" s="T100">n.[n:case]</ta>
            <ta e="T102" id="Seg_2407" s="T101">v.[v:pn]</ta>
            <ta e="T103" id="Seg_2408" s="T102">dem.[n:case]</ta>
            <ta e="T104" id="Seg_2409" s="T103">n-n:num.[n:case]</ta>
            <ta e="T105" id="Seg_2410" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_2411" s="T105">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_2412" s="T106">quant</ta>
            <ta e="T108" id="Seg_2413" s="T107">dem</ta>
            <ta e="T109" id="Seg_2414" s="T108">num</ta>
            <ta e="T110" id="Seg_2415" s="T109">n-n:num-n:case</ta>
            <ta e="T111" id="Seg_2416" s="T110">dem</ta>
            <ta e="T112" id="Seg_2417" s="T111">num</ta>
            <ta e="T113" id="Seg_2418" s="T112">n-n:case-pp</ta>
            <ta e="T114" id="Seg_2419" s="T113">num</ta>
            <ta e="T115" id="Seg_2420" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_2421" s="T115">adj</ta>
            <ta e="T117" id="Seg_2422" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_2423" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2424" s="T118">pers</ta>
            <ta e="T120" id="Seg_2425" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2426" s="T120">n</ta>
            <ta e="T122" id="Seg_2427" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2428" s="T122">pers-n:ins-n:case</ta>
            <ta e="T124" id="Seg_2429" s="T123">n-n:num.[n:case]</ta>
            <ta e="T125" id="Seg_2430" s="T124">quant</ta>
            <ta e="T126" id="Seg_2431" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2432" s="T126">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2433" s="T127">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_2434" s="T128">dem</ta>
            <ta e="T130" id="Seg_2435" s="T129">n.[n:case]</ta>
            <ta e="T131" id="Seg_2436" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2437" s="T131">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T133" id="Seg_2438" s="T132">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2439" s="T133">interrog</ta>
            <ta e="T135" id="Seg_2440" s="T134">interrog-adj&gt;adv</ta>
            <ta e="T136" id="Seg_2441" s="T135">v.[v:pn]</ta>
            <ta e="T137" id="Seg_2442" s="T136">interrog</ta>
            <ta e="T138" id="Seg_2443" s="T137">interrog-adj&gt;adv</ta>
            <ta e="T139" id="Seg_2444" s="T138">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T140" id="Seg_2445" s="T139">interrog</ta>
            <ta e="T141" id="Seg_2446" s="T140">interrog</ta>
            <ta e="T142" id="Seg_2447" s="T141">v-v:pn</ta>
            <ta e="T143" id="Seg_2448" s="T142">interrog-n</ta>
            <ta e="T144" id="Seg_2449" s="T143">interrog.[n:case]</ta>
            <ta e="T145" id="Seg_2450" s="T144">v-v:mood-v:pn</ta>
            <ta e="T146" id="Seg_2451" s="T145">interrog-n.[n:case]</ta>
            <ta e="T147" id="Seg_2452" s="T146">interrog</ta>
            <ta e="T148" id="Seg_2453" s="T147">v-n:ins-v:pn</ta>
            <ta e="T149" id="Seg_2454" s="T148">pro-n:case</ta>
            <ta e="T150" id="Seg_2455" s="T149">pers.[n:case]</ta>
            <ta e="T151" id="Seg_2456" s="T150">quant</ta>
            <ta e="T152" id="Seg_2457" s="T151">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2458" s="T152">n-n:num-n:case-n:case</ta>
            <ta e="T154" id="Seg_2459" s="T153">dem</ta>
            <ta e="T155" id="Seg_2460" s="T154">num</ta>
            <ta e="T156" id="Seg_2461" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_2462" s="T156">conj</ta>
            <ta e="T158" id="Seg_2463" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_2464" s="T158">v-n&gt;v-v:inf</ta>
            <ta e="T160" id="Seg_2465" s="T159">dem</ta>
            <ta e="T161" id="Seg_2466" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_2467" s="T161">adj</ta>
            <ta e="T163" id="Seg_2468" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_2469" s="T163">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T165" id="Seg_2470" s="T164">n-n:num-n:case-n:poss</ta>
            <ta e="T166" id="Seg_2471" s="T165">adv</ta>
            <ta e="T167" id="Seg_2472" s="T166">v-v&gt;v-v&gt;adv</ta>
            <ta e="T168" id="Seg_2473" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_2474" s="T168">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T170" id="Seg_2475" s="T169">n-n:ins-n:num-n:case-v:pn</ta>
            <ta e="T171" id="Seg_2476" s="T170">pers.[n:case]</ta>
            <ta e="T172" id="Seg_2477" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2478" s="T172">dem</ta>
            <ta e="T174" id="Seg_2479" s="T173">n-n:ins-n:num-n:case</ta>
            <ta e="T175" id="Seg_2480" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2481" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2482" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_2483" s="T177">adv</ta>
            <ta e="T179" id="Seg_2484" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_2485" s="T179">dem-clit</ta>
            <ta e="T181" id="Seg_2486" s="T180">n-n:num-n:case</ta>
            <ta e="T182" id="Seg_2487" s="T181">conj</ta>
            <ta e="T183" id="Seg_2488" s="T182">n-n:num-n:case-n:case</ta>
            <ta e="T184" id="Seg_2489" s="T183">pro-n:case</ta>
            <ta e="T185" id="Seg_2490" s="T184">quant</ta>
            <ta e="T186" id="Seg_2491" s="T185">v-v&gt;v-v:pn</ta>
            <ta e="T187" id="Seg_2492" s="T186">conj</ta>
            <ta e="T188" id="Seg_2493" s="T187">dem</ta>
            <ta e="T189" id="Seg_2494" s="T188">num</ta>
            <ta e="T190" id="Seg_2495" s="T189">n.[n:case]</ta>
            <ta e="T191" id="Seg_2496" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_2497" s="T191">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_2498" s="T192">conj</ta>
            <ta e="T194" id="Seg_2499" s="T193">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T195" id="Seg_2500" s="T194">%%</ta>
            <ta e="T196" id="Seg_2501" s="T195">n-n:num-n:case</ta>
            <ta e="T197" id="Seg_2502" s="T196">adv</ta>
            <ta e="T198" id="Seg_2503" s="T197">v-v:tense.[v:pn]</ta>
            <ta e="T199" id="Seg_2504" s="T198">conj</ta>
            <ta e="T200" id="Seg_2505" s="T199">dem</ta>
            <ta e="T201" id="Seg_2506" s="T200">n-n:num.[n:case]</ta>
            <ta e="T202" id="Seg_2507" s="T201">v-v:inf</ta>
            <ta e="T203" id="Seg_2508" s="T202">v-v:pn</ta>
            <ta e="T204" id="Seg_2509" s="T203">adv</ta>
            <ta e="T205" id="Seg_2510" s="T204">dem</ta>
            <ta e="T206" id="Seg_2511" s="T205">n-n:num.[n:case]</ta>
            <ta e="T207" id="Seg_2512" s="T206">v-v&gt;adv</ta>
            <ta e="T208" id="Seg_2513" s="T207">v-v:ins-v:pn</ta>
            <ta e="T209" id="Seg_2514" s="T208">conj</ta>
            <ta e="T210" id="Seg_2515" s="T209">v-v&gt;adv</ta>
            <ta e="T211" id="Seg_2516" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_2517" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_2518" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2519" s="T213">v-v:pn</ta>
            <ta e="T215" id="Seg_2520" s="T214">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T216" id="Seg_2521" s="T215">v-v:ins-v:pn</ta>
            <ta e="T217" id="Seg_2522" s="T216">n-n:num.[n:case]</ta>
            <ta e="T218" id="Seg_2523" s="T217">adv</ta>
            <ta e="T219" id="Seg_2524" s="T218">v-v:ins-v:pn</ta>
            <ta e="T220" id="Seg_2525" s="T219">adj-interrog-n:num-n:case-n:case</ta>
            <ta e="T221" id="Seg_2526" s="T220">v-n:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_2527" s="T221">conj</ta>
            <ta e="T223" id="Seg_2528" s="T222">v-v&gt;v-v:pn</ta>
            <ta e="T224" id="Seg_2529" s="T223">v-n&gt;v-v:inf</ta>
            <ta e="T225" id="Seg_2530" s="T224">n-n:num-n:case-n:case</ta>
            <ta e="T226" id="Seg_2531" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_2532" s="T226">dem</ta>
            <ta e="T228" id="Seg_2533" s="T227">n-n:num.[n:case]</ta>
            <ta e="T229" id="Seg_2534" s="T228">n-n:num-n:case</ta>
            <ta e="T230" id="Seg_2535" s="T229">v-v&gt;v-v:pn</ta>
            <ta e="T231" id="Seg_2536" s="T230">adv</ta>
            <ta e="T232" id="Seg_2537" s="T231">dem</ta>
            <ta e="T233" id="Seg_2538" s="T232">n-n:num-n:case</ta>
            <ta e="T234" id="Seg_2539" s="T233">n-n:num.[n:case]</ta>
            <ta e="T235" id="Seg_2540" s="T234">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2541" s="T1">dem</ta>
            <ta e="T3" id="Seg_2542" s="T2">v</ta>
            <ta e="T4" id="Seg_2543" s="T3">pro</ta>
            <ta e="T5" id="Seg_2544" s="T4">n</ta>
            <ta e="T6" id="Seg_2545" s="T5">conj</ta>
            <ta e="T7" id="Seg_2546" s="T6">n</ta>
            <ta e="T8" id="Seg_2547" s="T7">v</ta>
            <ta e="T9" id="Seg_2548" s="T8">num</ta>
            <ta e="T10" id="Seg_2549" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_2550" s="T10">adj</ta>
            <ta e="T12" id="Seg_2551" s="T11">n</ta>
            <ta e="T13" id="Seg_2552" s="T12">n</ta>
            <ta e="T14" id="Seg_2553" s="T13">quant</ta>
            <ta e="T15" id="Seg_2554" s="T14">n</ta>
            <ta e="T16" id="Seg_2555" s="T15">n</ta>
            <ta e="T17" id="Seg_2556" s="T16">n</ta>
            <ta e="T18" id="Seg_2557" s="T17">v</ta>
            <ta e="T19" id="Seg_2558" s="T18">dem</ta>
            <ta e="T20" id="Seg_2559" s="T19">n</ta>
            <ta e="T21" id="Seg_2560" s="T20">adj</ta>
            <ta e="T22" id="Seg_2561" s="T21">n</ta>
            <ta e="T23" id="Seg_2562" s="T22">pers</ta>
            <ta e="T24" id="Seg_2563" s="T23">quant</ta>
            <ta e="T25" id="Seg_2564" s="T24">v</ta>
            <ta e="T26" id="Seg_2565" s="T25">conj</ta>
            <ta e="T27" id="Seg_2566" s="T26">quant</ta>
            <ta e="T28" id="Seg_2567" s="T27">v</ta>
            <ta e="T29" id="Seg_2568" s="T28">v</ta>
            <ta e="T30" id="Seg_2569" s="T29">v</ta>
            <ta e="T31" id="Seg_2570" s="T30">dem</ta>
            <ta e="T32" id="Seg_2571" s="T31">n</ta>
            <ta e="T33" id="Seg_2572" s="T32">pp</ta>
            <ta e="T34" id="Seg_2573" s="T33">v</ta>
            <ta e="T35" id="Seg_2574" s="T34">adj</ta>
            <ta e="T36" id="Seg_2575" s="T35">ptcp</ta>
            <ta e="T37" id="Seg_2576" s="T36">n</ta>
            <ta e="T38" id="Seg_2577" s="T37">n</ta>
            <ta e="T39" id="Seg_2578" s="T38">n</ta>
            <ta e="T40" id="Seg_2579" s="T39">v</ta>
            <ta e="T41" id="Seg_2580" s="T40">dem</ta>
            <ta e="T42" id="Seg_2581" s="T41">n</ta>
            <ta e="T43" id="Seg_2582" s="T42">adv</ta>
            <ta e="T44" id="Seg_2583" s="T43">v</ta>
            <ta e="T45" id="Seg_2584" s="T44">n</ta>
            <ta e="T46" id="Seg_2585" s="T45">pers</ta>
            <ta e="T47" id="Seg_2586" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_2587" s="T47">v</ta>
            <ta e="T49" id="Seg_2588" s="T48">conj</ta>
            <ta e="T50" id="Seg_2589" s="T49">pers</ta>
            <ta e="T51" id="Seg_2590" s="T50">v</ta>
            <ta e="T52" id="Seg_2591" s="T51">conj</ta>
            <ta e="T53" id="Seg_2592" s="T52">pers</ta>
            <ta e="T54" id="Seg_2593" s="T53">adv</ta>
            <ta e="T55" id="Seg_2594" s="T54">v</ta>
            <ta e="T56" id="Seg_2595" s="T55">pers</ta>
            <ta e="T57" id="Seg_2596" s="T56">adj</ta>
            <ta e="T58" id="Seg_2597" s="T57">n</ta>
            <ta e="T59" id="Seg_2598" s="T58">v</ta>
            <ta e="T60" id="Seg_2599" s="T59">adj</ta>
            <ta e="T61" id="Seg_2600" s="T60">n</ta>
            <ta e="T62" id="Seg_2601" s="T61">n</ta>
            <ta e="T63" id="Seg_2602" s="T62">adj</ta>
            <ta e="T64" id="Seg_2603" s="T63">n</ta>
            <ta e="T65" id="Seg_2604" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2605" s="T65">dem</ta>
            <ta e="T67" id="Seg_2606" s="T66">num</ta>
            <ta e="T68" id="Seg_2607" s="T67">n</ta>
            <ta e="T69" id="Seg_2608" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2609" s="T69">v</ta>
            <ta e="T71" id="Seg_2610" s="T70">n</ta>
            <ta e="T72" id="Seg_2611" s="T71">v</ta>
            <ta e="T73" id="Seg_2612" s="T72">conj</ta>
            <ta e="T74" id="Seg_2613" s="T73">pers</ta>
            <ta e="T75" id="Seg_2614" s="T74">v</ta>
            <ta e="T76" id="Seg_2615" s="T75">num</ta>
            <ta e="T77" id="Seg_2616" s="T76">pers</ta>
            <ta e="T78" id="Seg_2617" s="T77">n</ta>
            <ta e="T79" id="Seg_2618" s="T78">v</ta>
            <ta e="T80" id="Seg_2619" s="T79">n</ta>
            <ta e="T81" id="Seg_2620" s="T80">v</ta>
            <ta e="T82" id="Seg_2621" s="T81">n</ta>
            <ta e="T83" id="Seg_2622" s="T82">dem</ta>
            <ta e="T84" id="Seg_2623" s="T83">n</ta>
            <ta e="T85" id="Seg_2624" s="T84">v</ta>
            <ta e="T86" id="Seg_2625" s="T85">dem</ta>
            <ta e="T87" id="Seg_2626" s="T86">n</ta>
            <ta e="T88" id="Seg_2627" s="T87">n</ta>
            <ta e="T89" id="Seg_2628" s="T88">v</ta>
            <ta e="T90" id="Seg_2629" s="T89">v</ta>
            <ta e="T91" id="Seg_2630" s="T90">emphpro</ta>
            <ta e="T92" id="Seg_2631" s="T91">pers</ta>
            <ta e="T93" id="Seg_2632" s="T92">v</ta>
            <ta e="T94" id="Seg_2633" s="T93">pers</ta>
            <ta e="T95" id="Seg_2634" s="T94">v</ta>
            <ta e="T96" id="Seg_2635" s="T95">quant</ta>
            <ta e="T97" id="Seg_2636" s="T96">num</ta>
            <ta e="T98" id="Seg_2637" s="T97">n</ta>
            <ta e="T99" id="Seg_2638" s="T98">conj</ta>
            <ta e="T100" id="Seg_2639" s="T99">num</ta>
            <ta e="T101" id="Seg_2640" s="T100">n</ta>
            <ta e="T102" id="Seg_2641" s="T101">v</ta>
            <ta e="T103" id="Seg_2642" s="T102">dem</ta>
            <ta e="T104" id="Seg_2643" s="T103">n</ta>
            <ta e="T105" id="Seg_2644" s="T104">v</ta>
            <ta e="T106" id="Seg_2645" s="T105">v</ta>
            <ta e="T107" id="Seg_2646" s="T106">quant</ta>
            <ta e="T108" id="Seg_2647" s="T107">dem</ta>
            <ta e="T109" id="Seg_2648" s="T108">num</ta>
            <ta e="T110" id="Seg_2649" s="T109">n</ta>
            <ta e="T111" id="Seg_2650" s="T110">dem</ta>
            <ta e="T112" id="Seg_2651" s="T111">num</ta>
            <ta e="T113" id="Seg_2652" s="T112">n</ta>
            <ta e="T114" id="Seg_2653" s="T113">num</ta>
            <ta e="T115" id="Seg_2654" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_2655" s="T115">adj</ta>
            <ta e="T117" id="Seg_2656" s="T116">n</ta>
            <ta e="T118" id="Seg_2657" s="T117">v</ta>
            <ta e="T119" id="Seg_2658" s="T118">pers</ta>
            <ta e="T120" id="Seg_2659" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2660" s="T120">n</ta>
            <ta e="T122" id="Seg_2661" s="T121">v</ta>
            <ta e="T123" id="Seg_2662" s="T122">pers</ta>
            <ta e="T124" id="Seg_2663" s="T123">n</ta>
            <ta e="T125" id="Seg_2664" s="T124">quant</ta>
            <ta e="T126" id="Seg_2665" s="T125">n</ta>
            <ta e="T127" id="Seg_2666" s="T126">v</ta>
            <ta e="T128" id="Seg_2667" s="T127">v</ta>
            <ta e="T129" id="Seg_2668" s="T128">dem</ta>
            <ta e="T130" id="Seg_2669" s="T129">n</ta>
            <ta e="T131" id="Seg_2670" s="T130">v</ta>
            <ta e="T132" id="Seg_2671" s="T131">pers</ta>
            <ta e="T133" id="Seg_2672" s="T132">v</ta>
            <ta e="T134" id="Seg_2673" s="T133">interrog</ta>
            <ta e="T135" id="Seg_2674" s="T134">adv</ta>
            <ta e="T136" id="Seg_2675" s="T135">v</ta>
            <ta e="T137" id="Seg_2676" s="T136">interrog</ta>
            <ta e="T138" id="Seg_2677" s="T137">adv</ta>
            <ta e="T139" id="Seg_2678" s="T138">v</ta>
            <ta e="T140" id="Seg_2679" s="T139">interrog</ta>
            <ta e="T141" id="Seg_2680" s="T140">interrog</ta>
            <ta e="T142" id="Seg_2681" s="T141">v</ta>
            <ta e="T143" id="Seg_2682" s="T142">n</ta>
            <ta e="T144" id="Seg_2683" s="T143">interrog</ta>
            <ta e="T145" id="Seg_2684" s="T144">v</ta>
            <ta e="T146" id="Seg_2685" s="T145">n</ta>
            <ta e="T147" id="Seg_2686" s="T146">interrog</ta>
            <ta e="T148" id="Seg_2687" s="T147">v</ta>
            <ta e="T149" id="Seg_2688" s="T148">pro</ta>
            <ta e="T150" id="Seg_2689" s="T149">pers</ta>
            <ta e="T151" id="Seg_2690" s="T150">quant</ta>
            <ta e="T152" id="Seg_2691" s="T151">v</ta>
            <ta e="T153" id="Seg_2692" s="T152">n</ta>
            <ta e="T154" id="Seg_2693" s="T153">dem</ta>
            <ta e="T155" id="Seg_2694" s="T154">num</ta>
            <ta e="T156" id="Seg_2695" s="T155">n</ta>
            <ta e="T157" id="Seg_2696" s="T156">interrog</ta>
            <ta e="T158" id="Seg_2697" s="T157">v</ta>
            <ta e="T159" id="Seg_2698" s="T158">v</ta>
            <ta e="T160" id="Seg_2699" s="T159">dem</ta>
            <ta e="T161" id="Seg_2700" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_2701" s="T161">adj</ta>
            <ta e="T163" id="Seg_2702" s="T162">n</ta>
            <ta e="T164" id="Seg_2703" s="T163">pers</ta>
            <ta e="T165" id="Seg_2704" s="T164">n</ta>
            <ta e="T166" id="Seg_2705" s="T165">adj</ta>
            <ta e="T167" id="Seg_2706" s="T166">adv</ta>
            <ta e="T168" id="Seg_2707" s="T167">v</ta>
            <ta e="T169" id="Seg_2708" s="T168">pers</ta>
            <ta e="T170" id="Seg_2709" s="T169">n</ta>
            <ta e="T171" id="Seg_2710" s="T170">pers</ta>
            <ta e="T172" id="Seg_2711" s="T171">v</ta>
            <ta e="T173" id="Seg_2712" s="T172">dem</ta>
            <ta e="T174" id="Seg_2713" s="T173">n</ta>
            <ta e="T175" id="Seg_2714" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2715" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2716" s="T176">v</ta>
            <ta e="T178" id="Seg_2717" s="T177">adv</ta>
            <ta e="T179" id="Seg_2718" s="T178">v</ta>
            <ta e="T180" id="Seg_2719" s="T179">dem</ta>
            <ta e="T181" id="Seg_2720" s="T180">n</ta>
            <ta e="T182" id="Seg_2721" s="T181">conj</ta>
            <ta e="T183" id="Seg_2722" s="T182">n</ta>
            <ta e="T184" id="Seg_2723" s="T183">pro</ta>
            <ta e="T185" id="Seg_2724" s="T184">quant</ta>
            <ta e="T186" id="Seg_2725" s="T185">v</ta>
            <ta e="T187" id="Seg_2726" s="T186">conj</ta>
            <ta e="T188" id="Seg_2727" s="T187">dem</ta>
            <ta e="T189" id="Seg_2728" s="T188">num</ta>
            <ta e="T190" id="Seg_2729" s="T189">n</ta>
            <ta e="T191" id="Seg_2730" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_2731" s="T191">v</ta>
            <ta e="T193" id="Seg_2732" s="T192">conj</ta>
            <ta e="T194" id="Seg_2733" s="T193">pers</ta>
            <ta e="T196" id="Seg_2734" s="T195">n</ta>
            <ta e="T197" id="Seg_2735" s="T196">adv</ta>
            <ta e="T198" id="Seg_2736" s="T197">v</ta>
            <ta e="T199" id="Seg_2737" s="T198">conj</ta>
            <ta e="T200" id="Seg_2738" s="T199">dem</ta>
            <ta e="T201" id="Seg_2739" s="T200">n</ta>
            <ta e="T202" id="Seg_2740" s="T201">v</ta>
            <ta e="T203" id="Seg_2741" s="T202">v</ta>
            <ta e="T204" id="Seg_2742" s="T203">adv</ta>
            <ta e="T205" id="Seg_2743" s="T204">dem</ta>
            <ta e="T206" id="Seg_2744" s="T205">n</ta>
            <ta e="T207" id="Seg_2745" s="T206">adv</ta>
            <ta e="T208" id="Seg_2746" s="T207">v</ta>
            <ta e="T209" id="Seg_2747" s="T208">conj</ta>
            <ta e="T210" id="Seg_2748" s="T209">adv</ta>
            <ta e="T211" id="Seg_2749" s="T210">v</ta>
            <ta e="T212" id="Seg_2750" s="T211">n</ta>
            <ta e="T213" id="Seg_2751" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2752" s="T213">v</ta>
            <ta e="T215" id="Seg_2753" s="T214">pers</ta>
            <ta e="T216" id="Seg_2754" s="T215">v</ta>
            <ta e="T217" id="Seg_2755" s="T216">n</ta>
            <ta e="T218" id="Seg_2756" s="T217">adv</ta>
            <ta e="T219" id="Seg_2757" s="T218">v</ta>
            <ta e="T220" id="Seg_2758" s="T219">n</ta>
            <ta e="T221" id="Seg_2759" s="T220">v</ta>
            <ta e="T222" id="Seg_2760" s="T221">conj</ta>
            <ta e="T223" id="Seg_2761" s="T222">v</ta>
            <ta e="T224" id="Seg_2762" s="T223">v</ta>
            <ta e="T225" id="Seg_2763" s="T224">n</ta>
            <ta e="T226" id="Seg_2764" s="T225">n</ta>
            <ta e="T227" id="Seg_2765" s="T226">dem</ta>
            <ta e="T228" id="Seg_2766" s="T227">n</ta>
            <ta e="T229" id="Seg_2767" s="T228">n</ta>
            <ta e="T230" id="Seg_2768" s="T229">v</ta>
            <ta e="T231" id="Seg_2769" s="T230">adv</ta>
            <ta e="T232" id="Seg_2770" s="T231">dem</ta>
            <ta e="T233" id="Seg_2771" s="T232">n</ta>
            <ta e="T234" id="Seg_2772" s="T233">n</ta>
            <ta e="T235" id="Seg_2773" s="T234">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_2774" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_2775" s="T5">RUS:gram</ta>
            <ta e="T12" id="Seg_2776" s="T11">RUS:cult</ta>
            <ta e="T13" id="Seg_2777" s="T12">RUS:cult</ta>
            <ta e="T17" id="Seg_2778" s="T16">RUS:cult</ta>
            <ta e="T20" id="Seg_2779" s="T19">RUS:cult</ta>
            <ta e="T26" id="Seg_2780" s="T25">RUS:gram</ta>
            <ta e="T37" id="Seg_2781" s="T36">RUS:cult</ta>
            <ta e="T39" id="Seg_2782" s="T38">RUS:cult</ta>
            <ta e="T45" id="Seg_2783" s="T44">RUS:cult</ta>
            <ta e="T49" id="Seg_2784" s="T48">RUS:gram</ta>
            <ta e="T52" id="Seg_2785" s="T51">RUS:gram</ta>
            <ta e="T54" id="Seg_2786" s="T53">RUS:core</ta>
            <ta e="T65" id="Seg_2787" s="T64">RUS:disc</ta>
            <ta e="T71" id="Seg_2788" s="T70">RUS:cult</ta>
            <ta e="T73" id="Seg_2789" s="T72">RUS:gram</ta>
            <ta e="T80" id="Seg_2790" s="T79">RUS:cult</ta>
            <ta e="T95" id="Seg_2791" s="T94">RUS:mod</ta>
            <ta e="T96" id="Seg_2792" s="T95">RUS:core</ta>
            <ta e="T99" id="Seg_2793" s="T98">RUS:gram</ta>
            <ta e="T104" id="Seg_2794" s="T103">RUS:cult</ta>
            <ta e="T107" id="Seg_2795" s="T106">RUS:core</ta>
            <ta e="T120" id="Seg_2796" s="T119">RUS:core</ta>
            <ta e="T124" id="Seg_2797" s="T123">RUS:cult</ta>
            <ta e="T151" id="Seg_2798" s="T150">RUS:core</ta>
            <ta e="T153" id="Seg_2799" s="T152">RUS:cult</ta>
            <ta e="T175" id="Seg_2800" s="T174">RUS:disc</ta>
            <ta e="T180" id="Seg_2801" s="T179">-RUS:disc</ta>
            <ta e="T182" id="Seg_2802" s="T181">RUS:gram</ta>
            <ta e="T183" id="Seg_2803" s="T182">RUS:cult</ta>
            <ta e="T185" id="Seg_2804" s="T184">RUS:core</ta>
            <ta e="T187" id="Seg_2805" s="T186">RUS:gram</ta>
            <ta e="T193" id="Seg_2806" s="T192">RUS:gram</ta>
            <ta e="T196" id="Seg_2807" s="T195">RUS:cult</ta>
            <ta e="T197" id="Seg_2808" s="T196">RUS:core</ta>
            <ta e="T199" id="Seg_2809" s="T198">RUS:gram</ta>
            <ta e="T209" id="Seg_2810" s="T208">RUS:gram</ta>
            <ta e="T212" id="Seg_2811" s="T211">RUS:cult</ta>
            <ta e="T217" id="Seg_2812" s="T216">RUS:cult</ta>
            <ta e="T222" id="Seg_2813" s="T221">RUS:gram</ta>
            <ta e="T225" id="Seg_2814" s="T224">RUS:cult</ta>
            <ta e="T226" id="Seg_2815" s="T225">RUS:cult</ta>
            <ta e="T229" id="Seg_2816" s="T228">RUS:cult</ta>
            <ta e="T234" id="Seg_2817" s="T233">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T12" id="Seg_2818" s="T1">It was when the Germans and Russians fought in a small town.</ta>
            <ta e="T55" id="Seg_2819" s="T48">Tthey were let go so they could walk freely.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T12" id="Seg_2820" s="T1">Es war als die Deutschen und Russen in einer kleinen Stadt kämpften.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T12" id="Seg_2821" s="T1">Это было когда немцы и русские воевали в одном небольшом городе.</ta>
            <ta e="T18" id="Seg_2822" s="T12">Немцы много людей взяли в плен.</ta>
            <ta e="T30" id="Seg_2823" s="T18">Этих взятых в плен людей они некоторых убивали, а некоторых работать заставляли.</ta>
            <ta e="T38" id="Seg_2824" s="T30">Среди этих людей были люди, которые хорошо играли в футбол. </ta>
            <ta e="T45" id="Seg_2825" s="T38">немцы узнали эти люди хорошо играют в футбол</ta>
            <ta e="T48" id="Seg_2826" s="T45">Их не убили.</ta>
            <ta e="T55" id="Seg_2827" s="T48">их отпустили чтоб они вольно ходили</ta>
            <ta e="T64" id="Seg_2828" s="T55">им хороший дом отдали, хорошую одежду, обувь, хорошее питание</ta>
            <ta e="T75" id="Seg_2829" s="T64">но эти десять человек не хотели немцам работать и у них оставаться</ta>
            <ta e="T82" id="Seg_2830" s="T75">один наш человек нашел в городе знакомого человека</ta>
            <ta e="T92" id="Seg_2831" s="T82">этот человек хотел этим людям помочь сбежать к своим людям</ta>
            <ta e="T98" id="Seg_2832" s="T92">бежать им надо всем вместе</ta>
            <ta e="T110" id="Seg_2833" s="T98">если один человек убежит, расстреляют всех этих десять человек</ta>
            <ta e="T118" id="Seg_2834" s="T110">среди этих десяти человек был нехороший человек</ta>
            <ta e="T122" id="Seg_2835" s="T118">он тоже был русский</ta>
            <ta e="T128" id="Seg_2836" s="T122">ему немцы много денег платили</ta>
            <ta e="T132" id="Seg_2837" s="T128">этот человек караулил за ними</ta>
            <ta e="T148" id="Seg_2838" s="T132">рассматривал кто, как думает, кто как делает, кто куда ходит, кто что сделал кто что сказал</ta>
            <ta e="T153" id="Seg_2839" s="T148">это он все сказывал немцам</ta>
            <ta e="T170" id="Seg_2840" s="T153">эти десять людей когда пошли играть этот плохой человек в их карманах долго искав нашел ихние документы</ta>
            <ta e="T181" id="Seg_2841" s="T170">он просмотрел эти документы но не взял обратно положил в те же карманы</ta>
            <ta e="T186" id="Seg_2842" s="T181">а немцам это все рассказал</ta>
            <ta e="T203" id="Seg_2843" s="T186">а эти десять человек не знали что про них немцам уже сказано что эти люди убежать хотят</ta>
            <ta e="T214" id="Seg_2844" s="T203">потом эти люди бежать ушли, убегая уехать до вагона не дошли</ta>
            <ta e="T217" id="Seg_2845" s="T214">Их поймали немцы.</ta>
            <ta e="T219" id="Seg_2846" s="T217">Обратно привезли.</ta>
            <ta e="T226" id="Seg_2847" s="T219">хорошими вещами одели заставили играть с немцами в футбол</ta>
            <ta e="T230" id="Seg_2848" s="T226">эти люди победили (перебороли)</ta>
            <ta e="T235" id="Seg_2849" s="T230">потом этих людей немцы расстреляли</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T12" id="Seg_2850" s="T1">это было когда немцы и русские воевали в одном небольшом городе</ta>
            <ta e="T18" id="Seg_2851" s="T12">немцы много людей в плен взяты были</ta>
            <ta e="T30" id="Seg_2852" s="T18">этих взятых в плен людей они некоторых убивали а некоторых работать заставляли</ta>
            <ta e="T38" id="Seg_2853" s="T30">этих людей среди были хорошо играющие в футбол люди</ta>
            <ta e="T45" id="Seg_2854" s="T38">немцы узнали эти люди хорошо играют в футбол</ta>
            <ta e="T48" id="Seg_2855" s="T45">их не убили</ta>
            <ta e="T55" id="Seg_2856" s="T48">их отпустили чтоб они вольно ходили</ta>
            <ta e="T64" id="Seg_2857" s="T55">им хороший дом отдали, хорошую одежду, обувь, хорошее питание</ta>
            <ta e="T75" id="Seg_2858" s="T64">но эти десять человек не хотели немцам работать и у них оставаться</ta>
            <ta e="T82" id="Seg_2859" s="T75">один наш человек нашел в городе знакомого человека</ta>
            <ta e="T92" id="Seg_2860" s="T82">этот человек хотел этим людям помочь сбежать к своим людям</ta>
            <ta e="T98" id="Seg_2861" s="T92">бежать им надо всем вместе</ta>
            <ta e="T110" id="Seg_2862" s="T98">если один человек убежит расстреляют всех этих десять человек</ta>
            <ta e="T118" id="Seg_2863" s="T110">среди этих десяти человек был нехороший человек</ta>
            <ta e="T122" id="Seg_2864" s="T118">он тоже был русский</ta>
            <ta e="T128" id="Seg_2865" s="T122">ему немцы много денег платили</ta>
            <ta e="T132" id="Seg_2866" s="T128">этот человек караулил за ними</ta>
            <ta e="T148" id="Seg_2867" s="T132">рассматривал кто как думает кто как делает кто куда ходит кто что сделал кто что сказал</ta>
            <ta e="T153" id="Seg_2868" s="T148">это он все сказывал немцам</ta>
            <ta e="T170" id="Seg_2869" s="T153">эти десять людей когда пошли играть этот плохой человек в их карманах долго искав нашел ихние документы</ta>
            <ta e="T181" id="Seg_2870" s="T170">он просмотрел эти документы но не взял обратно положил в те же карманы</ta>
            <ta e="T186" id="Seg_2871" s="T181">а немцам это все рассказал</ta>
            <ta e="T203" id="Seg_2872" s="T186">а эти десять человек не знали что про них немцам уже сказано что эти люди убежать хотят</ta>
            <ta e="T214" id="Seg_2873" s="T203">потом эти люди бежать ушли убегая уехать до вагона не дошли</ta>
            <ta e="T217" id="Seg_2874" s="T214">их поймали немцы</ta>
            <ta e="T219" id="Seg_2875" s="T217">обратно привезли</ta>
            <ta e="T226" id="Seg_2876" s="T219">хорошими вещами одели заставили играть с немцами в футбол</ta>
            <ta e="T230" id="Seg_2877" s="T226">эти люди победили (перебороли)</ta>
            <ta e="T235" id="Seg_2878" s="T230">потом этих людей немцы расстреляли</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
