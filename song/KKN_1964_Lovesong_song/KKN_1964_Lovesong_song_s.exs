<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KKN_1964_Lovesong_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KKN_1964_Lovesong_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">60</ud-information>
            <ud-information attribute-name="# HIAT:w">35</ud-information>
            <ud-information attribute-name="# e">35</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KKN">
            <abbreviation>KKN</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KKN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T36" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">me</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">iːlakusso</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">täpse</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">čačeqɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">/</nts>
                  <nts id="Seg_15" n="HIAT:ip">/</nts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_19" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">kuti</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">na</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">mezɨni</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">ass</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">tinnɨwɨs</ts>
                  <nts id="Seg_34" n="HIAT:ip">/</nts>
                  <nts id="Seg_35" n="HIAT:ip">/</nts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">lʼiš</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">tinnowɨzät</ts>
                  <nts id="Seg_45" n="HIAT:ip">/</nts>
                  <nts id="Seg_46" n="HIAT:ip">/</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">man</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">siːdʼüŋawɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip">/</nts>
                  <nts id="Seg_54" n="HIAT:ip">/</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">täban</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">säːɣɨn</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">saila</ts>
                  <nts id="Seg_67" n="HIAT:ip">/</nts>
                  <nts id="Seg_68" n="HIAT:ip">/</nts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">nʼenʼäm</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">maːziŋ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">tiwalʼesan</ts>
                  <nts id="Seg_81" n="HIAT:ip">/</nts>
                  <nts id="Seg_82" n="HIAT:ip">/</nts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_86" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">i</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">tʼäŋu</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">maːtqan</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">meŋ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_100" n="HIAT:w" s="T25">ilɨsaŋ</ts>
                  <nts id="Seg_101" n="HIAT:ip">/</nts>
                  <nts id="Seg_102" n="HIAT:ip">/</nts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_106" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">tüːlaŋ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">mattə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">sim</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">tʼäraŋguat</ts>
                  <nts id="Seg_118" n="HIAT:ip">/</nts>
                  <nts id="Seg_119" n="HIAT:ip">/</nts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_123" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">üːdəquːɣat</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">kun</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">essaŋ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">man</ts>
                  <nts id="Seg_135" n="HIAT:ip">/</nts>
                  <nts id="Seg_136" n="HIAT:ip">/</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_139" n="HIAT:w" s="T34">soːrendi</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">qupse</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T36" id="Seg_145" n="sc" s="T1">
               <ts e="T2" id="Seg_147" n="e" s="T1">me </ts>
               <ts e="T3" id="Seg_149" n="e" s="T2">iːlakusso </ts>
               <ts e="T4" id="Seg_151" n="e" s="T3">täpse </ts>
               <ts e="T5" id="Seg_153" n="e" s="T4">čačeqɨn//. </ts>
               <ts e="T6" id="Seg_155" n="e" s="T5">kuti </ts>
               <ts e="T7" id="Seg_157" n="e" s="T6">na </ts>
               <ts e="T8" id="Seg_159" n="e" s="T7">mezɨni </ts>
               <ts e="T9" id="Seg_161" n="e" s="T8">ass </ts>
               <ts e="T10" id="Seg_163" n="e" s="T9">tinnɨwɨs//. </ts>
               <ts e="T11" id="Seg_165" n="e" s="T10">lʼiš </ts>
               <ts e="T12" id="Seg_167" n="e" s="T11">tinnowɨzät// </ts>
               <ts e="T13" id="Seg_169" n="e" s="T12">man </ts>
               <ts e="T14" id="Seg_171" n="e" s="T13">siːdʼüŋawɨ// </ts>
               <ts e="T15" id="Seg_173" n="e" s="T14">da </ts>
               <ts e="T16" id="Seg_175" n="e" s="T15">täban </ts>
               <ts e="T17" id="Seg_177" n="e" s="T16">säːɣɨn </ts>
               <ts e="T18" id="Seg_179" n="e" s="T17">saila//. </ts>
               <ts e="T19" id="Seg_181" n="e" s="T18">nʼenʼäm </ts>
               <ts e="T20" id="Seg_183" n="e" s="T19">maːziŋ </ts>
               <ts e="T21" id="Seg_185" n="e" s="T20">tiwalʼesan//. </ts>
               <ts e="T22" id="Seg_187" n="e" s="T21">i </ts>
               <ts e="T23" id="Seg_189" n="e" s="T22">tʼäŋu </ts>
               <ts e="T24" id="Seg_191" n="e" s="T23">maːtqan </ts>
               <ts e="T25" id="Seg_193" n="e" s="T24">meŋ </ts>
               <ts e="T26" id="Seg_195" n="e" s="T25">ilɨsaŋ//. </ts>
               <ts e="T27" id="Seg_197" n="e" s="T26">tüːlaŋ </ts>
               <ts e="T28" id="Seg_199" n="e" s="T27">mattə </ts>
               <ts e="T29" id="Seg_201" n="e" s="T28">sim </ts>
               <ts e="T30" id="Seg_203" n="e" s="T29">tʼäraŋguat//. </ts>
               <ts e="T31" id="Seg_205" n="e" s="T30">üːdəquːɣat </ts>
               <ts e="T32" id="Seg_207" n="e" s="T31">kun </ts>
               <ts e="T33" id="Seg_209" n="e" s="T32">essaŋ </ts>
               <ts e="T34" id="Seg_211" n="e" s="T33">man// </ts>
               <ts e="T35" id="Seg_213" n="e" s="T34">soːrendi </ts>
               <ts e="T36" id="Seg_215" n="e" s="T35">qupse. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_216" s="T1">KKN_1964_Lovesong_song.001 (001.001)</ta>
            <ta e="T10" id="Seg_217" s="T5">KKN_1964_Lovesong_song.002 (001.002)</ta>
            <ta e="T18" id="Seg_218" s="T10">KKN_1964_Lovesong_song.003 (001.003)</ta>
            <ta e="T21" id="Seg_219" s="T18">KKN_1964_Lovesong_song.004 (001.004)</ta>
            <ta e="T26" id="Seg_220" s="T21">KKN_1964_Lovesong_song.005 (001.005)</ta>
            <ta e="T30" id="Seg_221" s="T26">KKN_1964_Lovesong_song.006 (001.006)</ta>
            <ta e="T36" id="Seg_222" s="T30">KKN_1964_Lovesong_song.007 (001.007)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_223" s="T1">ме ′ӣлакуссо тӓп′се тша′тшеkын //.</ta>
            <ta e="T10" id="Seg_224" s="T5">ку′ти на ′мезыни ′асс ′тиннывыс //.</ta>
            <ta e="T18" id="Seg_225" s="T10">лʼишʼ ′тинновызӓт // ман ′сӣдʼӱңавы // да ′тӓбан ′сӓ̄ɣын са′ила //.</ta>
            <ta e="T21" id="Seg_226" s="T18">′нʼенʼӓм ма̄′зиң тива′лʼесан //.</ta>
            <ta e="T26" id="Seg_227" s="T21">и ′тʼӓңу ′ма̄тkан ′мең илы′саң //.</ta>
            <ta e="T30" id="Seg_228" s="T26">тӱ̄лаң ′маттъ сим тʼӓраңгуат //.</ta>
            <ta e="T36" id="Seg_229" s="T30">ӱ̄дъkӯɣат кун ′ессаң ман // ′со̄ренди ′kупсе.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_230" s="T1">me iːlakusso täpse tšatšeqɨn //.</ta>
            <ta e="T10" id="Seg_231" s="T5">kuti na mezɨni ass tinnɨvɨs //.</ta>
            <ta e="T18" id="Seg_232" s="T10">lʼiš tinnovɨzät // man siːdʼüŋavɨ // da täban säːɣɨn saila //.</ta>
            <ta e="T21" id="Seg_233" s="T18">nʼenʼäm maːziŋ tivalʼesan //.</ta>
            <ta e="T26" id="Seg_234" s="T21">i tʼäŋu maːtqan meŋ ilɨsaŋ //.</ta>
            <ta e="T30" id="Seg_235" s="T26">tüːlaŋ mattə sim tʼäraŋguat //.</ta>
            <ta e="T36" id="Seg_236" s="T30">üːdəquːɣat kun essaŋ man // soːrendi qupse.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_237" s="T1">me iːlakusso täpse čačeqɨn//. </ta>
            <ta e="T10" id="Seg_238" s="T5">kuti na mezɨni ass tinnɨwɨs//. </ta>
            <ta e="T18" id="Seg_239" s="T10">lʼiš tinnowɨzät// man siːdʼüŋawɨ// da täban säːɣɨn saila//. </ta>
            <ta e="T21" id="Seg_240" s="T18">nʼenʼäm maːziŋ tiwalʼesan//. </ta>
            <ta e="T26" id="Seg_241" s="T21">i tʼäŋu maːtqan meŋ ilɨsaŋ//. </ta>
            <ta e="T30" id="Seg_242" s="T26">tüːlaŋ mattə sim tʼäraŋguat//. </ta>
            <ta e="T36" id="Seg_243" s="T30">üːdəquːɣat kun essaŋ man// soːrendi qupse. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_244" s="T1">me</ta>
            <ta e="T3" id="Seg_245" s="T2">iːla-ku-ss-o</ta>
            <ta e="T4" id="Seg_246" s="T3">täp-se</ta>
            <ta e="T5" id="Seg_247" s="T4">čačeqɨn</ta>
            <ta e="T6" id="Seg_248" s="T5">kuti</ta>
            <ta e="T7" id="Seg_249" s="T6">na</ta>
            <ta e="T8" id="Seg_250" s="T7">mezɨni</ta>
            <ta e="T9" id="Seg_251" s="T8">ass</ta>
            <ta e="T10" id="Seg_252" s="T9">tinnɨ-wɨ-s</ta>
            <ta e="T11" id="Seg_253" s="T10">lʼiš</ta>
            <ta e="T12" id="Seg_254" s="T11">tinno-wɨ-zä-t</ta>
            <ta e="T13" id="Seg_255" s="T12">man</ta>
            <ta e="T14" id="Seg_256" s="T13">siːdʼü-ŋa-wɨ</ta>
            <ta e="T15" id="Seg_257" s="T14">da</ta>
            <ta e="T16" id="Seg_258" s="T15">täb-a-n</ta>
            <ta e="T17" id="Seg_259" s="T16">säːɣɨ-n</ta>
            <ta e="T18" id="Seg_260" s="T17">sai-la</ta>
            <ta e="T19" id="Seg_261" s="T18">nʼenʼä-m</ta>
            <ta e="T20" id="Seg_262" s="T19">maːziŋ</ta>
            <ta e="T21" id="Seg_263" s="T20">tiwalʼ-e-sa-n</ta>
            <ta e="T22" id="Seg_264" s="T21">i</ta>
            <ta e="T23" id="Seg_265" s="T22">tʼäŋu</ta>
            <ta e="T24" id="Seg_266" s="T23">maːt-qan</ta>
            <ta e="T25" id="Seg_267" s="T24">meŋ</ta>
            <ta e="T26" id="Seg_268" s="T25">ilɨ-sa-ŋ</ta>
            <ta e="T27" id="Seg_269" s="T26">tüː-la-ŋ</ta>
            <ta e="T28" id="Seg_270" s="T27">mat-tə</ta>
            <ta e="T29" id="Seg_271" s="T28">sim</ta>
            <ta e="T30" id="Seg_272" s="T29">tʼäraŋ-gu-a-t</ta>
            <ta e="T31" id="Seg_273" s="T30">üːdə-quː-ɣa-t</ta>
            <ta e="T32" id="Seg_274" s="T31">kun</ta>
            <ta e="T33" id="Seg_275" s="T32">e-ssa-ŋ</ta>
            <ta e="T34" id="Seg_276" s="T33">man</ta>
            <ta e="T35" id="Seg_277" s="T34">soːre-ndi</ta>
            <ta e="T36" id="Seg_278" s="T35">qup-se</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_279" s="T1">meː</ta>
            <ta e="T3" id="Seg_280" s="T2">illɨ-ku-sɨ-ut</ta>
            <ta e="T4" id="Seg_281" s="T3">tap-se</ta>
            <ta e="T5" id="Seg_282" s="T4">čačeqɨn</ta>
            <ta e="T6" id="Seg_283" s="T5">kutti</ta>
            <ta e="T7" id="Seg_284" s="T6">naj</ta>
            <ta e="T8" id="Seg_285" s="T7">meːzanäd</ta>
            <ta e="T9" id="Seg_286" s="T8">assɨ</ta>
            <ta e="T10" id="Seg_287" s="T9">tinnɨ-ŋɨ-sɨ</ta>
            <ta e="T11" id="Seg_288" s="T10">lʼiš</ta>
            <ta e="T12" id="Seg_289" s="T11">tinnɨ-wa-sɨ-tɨ</ta>
            <ta e="T13" id="Seg_290" s="T12">man</ta>
            <ta e="T14" id="Seg_291" s="T13">šide-ka-mɨ</ta>
            <ta e="T15" id="Seg_292" s="T14">da</ta>
            <ta e="T16" id="Seg_293" s="T15">tap-ɨ-n</ta>
            <ta e="T17" id="Seg_294" s="T16">säɣə-naj</ta>
            <ta e="T18" id="Seg_295" s="T17">saj-la</ta>
            <ta e="T19" id="Seg_296" s="T18">nenʼnʼa-mɨ</ta>
            <ta e="T20" id="Seg_297" s="T19">mašim</ta>
            <ta e="T21" id="Seg_298" s="T20">tiwal-ɨ-sɨ-n</ta>
            <ta e="T22" id="Seg_299" s="T21">i</ta>
            <ta e="T23" id="Seg_300" s="T22">tʼäŋu</ta>
            <ta e="T24" id="Seg_301" s="T23">maːt-qən</ta>
            <ta e="T25" id="Seg_302" s="T24">mäkkä</ta>
            <ta e="T26" id="Seg_303" s="T25">illɨ-sɨ-ŋ</ta>
            <ta e="T27" id="Seg_304" s="T26">tüː-la-ŋ</ta>
            <ta e="T28" id="Seg_305" s="T27">maːt-ndɨ</ta>
            <ta e="T29" id="Seg_306" s="T28">mašim</ta>
            <ta e="T30" id="Seg_307" s="T29">tʼarɨŋ-ku-ɨ-tɨt</ta>
            <ta e="T31" id="Seg_308" s="T30">üːtɨ-ku-ŋɨ-tɨt</ta>
            <ta e="T32" id="Seg_309" s="T31">kuːn</ta>
            <ta e="T33" id="Seg_310" s="T32">eː-sɨ-ŋ</ta>
            <ta e="T34" id="Seg_311" s="T33">man</ta>
            <ta e="T35" id="Seg_312" s="T34">soːri-ntɨ</ta>
            <ta e="T36" id="Seg_313" s="T35">qum-se</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_314" s="T1">we.NOM</ta>
            <ta e="T3" id="Seg_315" s="T2">live-HAB-PST-1PL</ta>
            <ta e="T4" id="Seg_316" s="T3">(s)he-COM</ta>
            <ta e="T5" id="Seg_317" s="T4">near</ta>
            <ta e="T6" id="Seg_318" s="T5">who</ta>
            <ta e="T7" id="Seg_319" s="T6">EMPH</ta>
            <ta e="T8" id="Seg_320" s="T7">we.ACC</ta>
            <ta e="T9" id="Seg_321" s="T8">NEG</ta>
            <ta e="T10" id="Seg_322" s="T9">know-CO-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_323" s="T10">only</ta>
            <ta e="T12" id="Seg_324" s="T11">know-DRV-PST-3SG.O</ta>
            <ta e="T13" id="Seg_325" s="T12">I.GEN</ta>
            <ta e="T14" id="Seg_326" s="T13">heart-DIM-1SG</ta>
            <ta e="T15" id="Seg_327" s="T14">and</ta>
            <ta e="T16" id="Seg_328" s="T15">(s)he-EP-GEN</ta>
            <ta e="T17" id="Seg_329" s="T16">black-EMPH</ta>
            <ta e="T18" id="Seg_330" s="T17">eye-PL.[NOM]</ta>
            <ta e="T19" id="Seg_331" s="T18">sister-1SG</ta>
            <ta e="T20" id="Seg_332" s="T19">I.ACC</ta>
            <ta e="T21" id="Seg_333" s="T20">surprise-EP-PST-3SG.S</ta>
            <ta e="T22" id="Seg_334" s="T21">and</ta>
            <ta e="T23" id="Seg_335" s="T22">NEG.EX.[3SG.S]</ta>
            <ta e="T24" id="Seg_336" s="T23">house-LOC</ta>
            <ta e="T25" id="Seg_337" s="T24">I.ALL</ta>
            <ta e="T26" id="Seg_338" s="T25">live-PST-1SG.S</ta>
            <ta e="T27" id="Seg_339" s="T26">fire-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_340" s="T27">house-ILL</ta>
            <ta e="T29" id="Seg_341" s="T28">I.ACC</ta>
            <ta e="T30" id="Seg_342" s="T29">swear-HAB-EP-3PL</ta>
            <ta e="T31" id="Seg_343" s="T30">send-HAB-CO-3PL</ta>
            <ta e="T32" id="Seg_344" s="T31">where</ta>
            <ta e="T33" id="Seg_345" s="T32">be-PST-1SG.S</ta>
            <ta e="T34" id="Seg_346" s="T33">I.NOM</ta>
            <ta e="T35" id="Seg_347" s="T34">love-PTCP.PRS</ta>
            <ta e="T36" id="Seg_348" s="T35">human.being-COM</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_349" s="T1">мы.NOM</ta>
            <ta e="T3" id="Seg_350" s="T2">жить-HAB-PST-1PL</ta>
            <ta e="T4" id="Seg_351" s="T3">он(а)-COM</ta>
            <ta e="T5" id="Seg_352" s="T4">вблизи</ta>
            <ta e="T6" id="Seg_353" s="T5">кто</ta>
            <ta e="T7" id="Seg_354" s="T6">EMPH</ta>
            <ta e="T8" id="Seg_355" s="T7">мы.ACC</ta>
            <ta e="T9" id="Seg_356" s="T8">NEG</ta>
            <ta e="T10" id="Seg_357" s="T9">знать-CO-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_358" s="T10">лишь</ta>
            <ta e="T12" id="Seg_359" s="T11">знать-DRV-PST-3SG.O</ta>
            <ta e="T13" id="Seg_360" s="T12">я.GEN</ta>
            <ta e="T14" id="Seg_361" s="T13">сердце-DIM-1SG</ta>
            <ta e="T15" id="Seg_362" s="T14">и</ta>
            <ta e="T16" id="Seg_363" s="T15">он(а)-EP-GEN</ta>
            <ta e="T17" id="Seg_364" s="T16">чёрный-EMPH</ta>
            <ta e="T18" id="Seg_365" s="T17">глаз-PL.[NOM]</ta>
            <ta e="T19" id="Seg_366" s="T18">сестра-1SG</ta>
            <ta e="T20" id="Seg_367" s="T19">я.ACC</ta>
            <ta e="T21" id="Seg_368" s="T20">удивить-EP-PST-3SG.S</ta>
            <ta e="T22" id="Seg_369" s="T21">и</ta>
            <ta e="T23" id="Seg_370" s="T22">NEG.EX.[3SG.S]</ta>
            <ta e="T24" id="Seg_371" s="T23">дом-LOC</ta>
            <ta e="T25" id="Seg_372" s="T24">я.ALL</ta>
            <ta e="T26" id="Seg_373" s="T25">жить-PST-1SG.S</ta>
            <ta e="T27" id="Seg_374" s="T26">огонь-FUT-1SG.S</ta>
            <ta e="T28" id="Seg_375" s="T27">дом-ILL</ta>
            <ta e="T29" id="Seg_376" s="T28">я.ACC</ta>
            <ta e="T30" id="Seg_377" s="T29">ругать-HAB-EP-3PL</ta>
            <ta e="T31" id="Seg_378" s="T30">посылать-HAB-CO-3PL</ta>
            <ta e="T32" id="Seg_379" s="T31">где</ta>
            <ta e="T33" id="Seg_380" s="T32">быть-PST-1SG.S</ta>
            <ta e="T34" id="Seg_381" s="T33">я.NOM</ta>
            <ta e="T35" id="Seg_382" s="T34">любить-PTCP.PRS</ta>
            <ta e="T36" id="Seg_383" s="T35">человек-COM</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_384" s="T1">pers</ta>
            <ta e="T3" id="Seg_385" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_386" s="T3">pers-n:case</ta>
            <ta e="T5" id="Seg_387" s="T4">adv</ta>
            <ta e="T6" id="Seg_388" s="T5">interrog</ta>
            <ta e="T7" id="Seg_389" s="T6">clit</ta>
            <ta e="T8" id="Seg_390" s="T7">pers</ta>
            <ta e="T9" id="Seg_391" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_392" s="T9">v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T11" id="Seg_393" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_394" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_395" s="T12">pers</ta>
            <ta e="T14" id="Seg_396" s="T13">n-n&gt;n-n:poss</ta>
            <ta e="T15" id="Seg_397" s="T14">conj</ta>
            <ta e="T16" id="Seg_398" s="T15">pers-n:ins-n:case</ta>
            <ta e="T17" id="Seg_399" s="T16">adj-clit</ta>
            <ta e="T18" id="Seg_400" s="T17">n-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_401" s="T18">n-n:poss</ta>
            <ta e="T20" id="Seg_402" s="T19">pers</ta>
            <ta e="T21" id="Seg_403" s="T20">v-n:ins-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_404" s="T21">conj</ta>
            <ta e="T23" id="Seg_405" s="T22">v.[v:pn]</ta>
            <ta e="T24" id="Seg_406" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_407" s="T24">pers</ta>
            <ta e="T26" id="Seg_408" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_409" s="T26">n-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_410" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_411" s="T28">pers</ta>
            <ta e="T30" id="Seg_412" s="T29">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T31" id="Seg_413" s="T30">v-v&gt;v-v:ins-n:poss</ta>
            <ta e="T32" id="Seg_414" s="T31">interrog</ta>
            <ta e="T33" id="Seg_415" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_416" s="T33">pers</ta>
            <ta e="T35" id="Seg_417" s="T34">v-v&gt;ptcp</ta>
            <ta e="T36" id="Seg_418" s="T35">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_419" s="T1">pers</ta>
            <ta e="T3" id="Seg_420" s="T2">v</ta>
            <ta e="T4" id="Seg_421" s="T3">pers</ta>
            <ta e="T5" id="Seg_422" s="T4">adv</ta>
            <ta e="T6" id="Seg_423" s="T5">interrog</ta>
            <ta e="T7" id="Seg_424" s="T6">clit</ta>
            <ta e="T8" id="Seg_425" s="T7">pers</ta>
            <ta e="T9" id="Seg_426" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_427" s="T9">v</ta>
            <ta e="T11" id="Seg_428" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_429" s="T11">v</ta>
            <ta e="T13" id="Seg_430" s="T12">pers</ta>
            <ta e="T14" id="Seg_431" s="T13">n</ta>
            <ta e="T15" id="Seg_432" s="T14">conj</ta>
            <ta e="T16" id="Seg_433" s="T15">pers</ta>
            <ta e="T17" id="Seg_434" s="T16">n</ta>
            <ta e="T18" id="Seg_435" s="T17">n</ta>
            <ta e="T19" id="Seg_436" s="T18">n</ta>
            <ta e="T20" id="Seg_437" s="T19">pers</ta>
            <ta e="T21" id="Seg_438" s="T20">v</ta>
            <ta e="T22" id="Seg_439" s="T21">conj</ta>
            <ta e="T23" id="Seg_440" s="T22">v</ta>
            <ta e="T24" id="Seg_441" s="T23">n</ta>
            <ta e="T25" id="Seg_442" s="T24">pers</ta>
            <ta e="T26" id="Seg_443" s="T25">v</ta>
            <ta e="T27" id="Seg_444" s="T26">n</ta>
            <ta e="T28" id="Seg_445" s="T27">n</ta>
            <ta e="T29" id="Seg_446" s="T28">pers</ta>
            <ta e="T30" id="Seg_447" s="T29">v</ta>
            <ta e="T31" id="Seg_448" s="T30">v</ta>
            <ta e="T32" id="Seg_449" s="T31">interrog</ta>
            <ta e="T33" id="Seg_450" s="T32">v</ta>
            <ta e="T34" id="Seg_451" s="T33">pers</ta>
            <ta e="T35" id="Seg_452" s="T34">adj</ta>
            <ta e="T36" id="Seg_453" s="T35">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_454" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_455" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_456" s="T5">pro.h:S</ta>
            <ta e="T8" id="Seg_457" s="T7">pro.h:O</ta>
            <ta e="T10" id="Seg_458" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_459" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_460" s="T13">np:S</ta>
            <ta e="T18" id="Seg_461" s="T17">np:S</ta>
            <ta e="T19" id="Seg_462" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_463" s="T19">pro.h:O</ta>
            <ta e="T21" id="Seg_464" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_465" s="T22">0.3:S v:pred</ta>
            <ta e="T26" id="Seg_466" s="T25">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_467" s="T26">0.1.h:S v:pred</ta>
            <ta e="T29" id="Seg_468" s="T28">pro.h:O</ta>
            <ta e="T30" id="Seg_469" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_470" s="T30">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_471" s="T31">s:rel</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_472" s="T1">pro.h:Th</ta>
            <ta e="T4" id="Seg_473" s="T3">pro:Com</ta>
            <ta e="T5" id="Seg_474" s="T4">adv:L</ta>
            <ta e="T6" id="Seg_475" s="T5">pro.h:E</ta>
            <ta e="T8" id="Seg_476" s="T7">pro.h:Th</ta>
            <ta e="T13" id="Seg_477" s="T12">pro.h:Poss</ta>
            <ta e="T14" id="Seg_478" s="T13">np:E</ta>
            <ta e="T16" id="Seg_479" s="T15">pro.h:Poss</ta>
            <ta e="T18" id="Seg_480" s="T17">np:E</ta>
            <ta e="T19" id="Seg_481" s="T18">np.h:A 0.1.h:Poss</ta>
            <ta e="T20" id="Seg_482" s="T19">pro.h:Th</ta>
            <ta e="T23" id="Seg_483" s="T22">0.3:Th</ta>
            <ta e="T24" id="Seg_484" s="T23">np:L</ta>
            <ta e="T25" id="Seg_485" s="T24">pro.h:B</ta>
            <ta e="T26" id="Seg_486" s="T25">0.1.h:Th</ta>
            <ta e="T27" id="Seg_487" s="T26">0.1.h:A</ta>
            <ta e="T28" id="Seg_488" s="T27">np:G</ta>
            <ta e="T29" id="Seg_489" s="T28">pro.h:Th</ta>
            <ta e="T30" id="Seg_490" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_491" s="T30">0.3.h:A</ta>
            <ta e="T34" id="Seg_492" s="T33">pro.h:Th</ta>
            <ta e="T36" id="Seg_493" s="T35">np:Com</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_494" s="T10">RUS:gram</ta>
            <ta e="T15" id="Seg_495" s="T14">RUS:gram</ta>
            <ta e="T22" id="Seg_496" s="T21">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_497" s="T1">We lived near you (him?).</ta>
            <ta e="T10" id="Seg_498" s="T5">Noone knew us.</ta>
            <ta e="T18" id="Seg_499" s="T10">Only my heart and his black eyes did know.</ta>
            <ta e="T21" id="Seg_500" s="T18">My sister gave me away.</ta>
            <ta e="T26" id="Seg_501" s="T21">And there is no life for me at home.</ta>
            <ta e="T30" id="Seg_502" s="T26">When I come home, they scold on me.</ta>
            <ta e="T36" id="Seg_503" s="T30">They send me there, where I was with my beloved person.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_504" s="T1">Wir lebten nah bei ihm.</ta>
            <ta e="T10" id="Seg_505" s="T5">Keiner kannte uns.</ta>
            <ta e="T18" id="Seg_506" s="T10">Nur mein Herz und seine schwarzen Augen wussten es.</ta>
            <ta e="T21" id="Seg_507" s="T18">Meine Schwester gab mich weg.</ta>
            <ta e="T26" id="Seg_508" s="T21">Und es gibt im Haus für mich kein Leben.</ta>
            <ta e="T30" id="Seg_509" s="T26">Wenn ich nach Hause komme, schimpfen sie mich aus.</ta>
            <ta e="T36" id="Seg_510" s="T30">Sie schickten mich dorthin, wo ich mit meinem geliebten Menschen war.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_511" s="T1">Мы жили с тобой (с ним?) по соседству (близко).</ta>
            <ta e="T10" id="Seg_512" s="T5">Никто нас не знал.</ta>
            <ta e="T18" id="Seg_513" s="T10">Лишь только знала мое сердечко, да его черные глаза.</ta>
            <ta e="T21" id="Seg_514" s="T18">Подруга (старшая сестра) меня выдала.</ta>
            <ta e="T26" id="Seg_515" s="T21">Нет дома мне житья.</ta>
            <ta e="T30" id="Seg_516" s="T26">Когда я прихожу домой, меня ругают.</ta>
            <ta e="T36" id="Seg_517" s="T30">Посылают туда, где я была с любимым человеком.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_518" s="T1">мы жили с тобой по соседству (близко)</ta>
            <ta e="T10" id="Seg_519" s="T5">никто нас не знал</ta>
            <ta e="T18" id="Seg_520" s="T10">лишь только знала мое сердце (сердечко) да его черные глаза</ta>
            <ta e="T21" id="Seg_521" s="T18">подруга (старшая сестра) меня (обо мне) доложила (выдала)</ta>
            <ta e="T26" id="Seg_522" s="T21">нет дома мне житья</ta>
            <ta e="T30" id="Seg_523" s="T26">приду домой меня ругают</ta>
            <ta e="T36" id="Seg_524" s="T30">посылают где я была с милым (любимым) человеком</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
