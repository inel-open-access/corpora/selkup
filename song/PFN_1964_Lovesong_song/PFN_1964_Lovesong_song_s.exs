<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PFN_1964_Lovesong_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PFN_1964_Lovesong_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">13</ud-information>
            <ud-information attribute-name="# HIAT:u">2</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PFN">
            <abbreviation>PFN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PFN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T14" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">tanɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">man</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">tanɨ</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">man</ts>
                  <nts id="Seg_15" n="HIAT:ip">/</nts>
                  <nts id="Seg_16" n="HIAT:ip">/</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tanɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">manɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">enǯandɨ</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_29" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">tan</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">naverno</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">mazɨŋ</ts>
                  <nts id="Seg_38" n="HIAT:ip">/</nts>
                  <nts id="Seg_39" n="HIAT:ip">/</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">tʼäk</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">ass</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">äwelʼǯenǯandɨ</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T14" id="Seg_51" n="sc" s="T1">
               <ts e="T2" id="Seg_53" n="e" s="T1">tanɨ </ts>
               <ts e="T3" id="Seg_55" n="e" s="T2">man, </ts>
               <ts e="T4" id="Seg_57" n="e" s="T3">tanɨ </ts>
               <ts e="T5" id="Seg_59" n="e" s="T4">man// </ts>
               <ts e="T6" id="Seg_61" n="e" s="T5">tanɨ </ts>
               <ts e="T7" id="Seg_63" n="e" s="T6">manɨ </ts>
               <ts e="T8" id="Seg_65" n="e" s="T7">enǯandɨ. </ts>
               <ts e="T9" id="Seg_67" n="e" s="T8">tan </ts>
               <ts e="T10" id="Seg_69" n="e" s="T9">naverno </ts>
               <ts e="T11" id="Seg_71" n="e" s="T10">mazɨŋ// </ts>
               <ts e="T12" id="Seg_73" n="e" s="T11">tʼäk </ts>
               <ts e="T13" id="Seg_75" n="e" s="T12">ass </ts>
               <ts e="T14" id="Seg_77" n="e" s="T13">äwelʼǯenǯandɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_78" s="T1">PFN_1964_Lovesong_song.001 (001.001)</ta>
            <ta e="T14" id="Seg_79" s="T8">PFN_1964_Lovesong_song.002 (001.002)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_80" s="T1">′таны ман, ′таны ман // ′таны ′маны ′енджанды.</ta>
            <ta e="T14" id="Seg_81" s="T8">тан на′вʼерно ма′зың // ′тʼӓк асс ′ӓвелʼ′дженджанды.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_82" s="T1">tanɨ man, tanɨ man // tanɨ manɨ enǯandɨ.</ta>
            <ta e="T14" id="Seg_83" s="T8">tan navʼerno mazɨŋ // tʼäk ass ävelʼǯenǯandɨ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_84" s="T1">tanɨ man, tanɨ man// tanɨ manɨ enǯandɨ. </ta>
            <ta e="T14" id="Seg_85" s="T8">tan naverno mazɨŋ// tʼäk ass äwelʼǯenǯandɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_86" s="T1">tanɨ</ta>
            <ta e="T3" id="Seg_87" s="T2">man</ta>
            <ta e="T4" id="Seg_88" s="T3">tanɨ</ta>
            <ta e="T5" id="Seg_89" s="T4">man</ta>
            <ta e="T6" id="Seg_90" s="T5">tanɨ</ta>
            <ta e="T7" id="Seg_91" s="T6">manɨ</ta>
            <ta e="T8" id="Seg_92" s="T7">e-nǯa-ndɨ</ta>
            <ta e="T9" id="Seg_93" s="T8">tat</ta>
            <ta e="T10" id="Seg_94" s="T9">naverno</ta>
            <ta e="T11" id="Seg_95" s="T10">mazɨŋ</ta>
            <ta e="T12" id="Seg_96" s="T11">tʼäk</ta>
            <ta e="T13" id="Seg_97" s="T12">ass</ta>
            <ta e="T14" id="Seg_98" s="T13">äwelʼǯe-nǯa-ndɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_99" s="T1">tan</ta>
            <ta e="T3" id="Seg_100" s="T2">man</ta>
            <ta e="T4" id="Seg_101" s="T3">tan</ta>
            <ta e="T5" id="Seg_102" s="T4">man</ta>
            <ta e="T6" id="Seg_103" s="T5">tan</ta>
            <ta e="T7" id="Seg_104" s="T6">man</ta>
            <ta e="T8" id="Seg_105" s="T7">eː-nǯɨ-ndɨ</ta>
            <ta e="T9" id="Seg_106" s="T8">tan</ta>
            <ta e="T10" id="Seg_107" s="T9">nawerna</ta>
            <ta e="T11" id="Seg_108" s="T10">mašim</ta>
            <ta e="T12" id="Seg_109" s="T11">tʼak</ta>
            <ta e="T13" id="Seg_110" s="T12">assɨ</ta>
            <ta e="T14" id="Seg_111" s="T13">äwɨlǯi-nǯɨ-ndɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_112" s="T1">you.SG.NOM</ta>
            <ta e="T3" id="Seg_113" s="T2">I.GEN</ta>
            <ta e="T4" id="Seg_114" s="T3">you.SG.NOM</ta>
            <ta e="T5" id="Seg_115" s="T4">I.GEN</ta>
            <ta e="T6" id="Seg_116" s="T5">you.SG.NOM</ta>
            <ta e="T7" id="Seg_117" s="T6">I.GEN</ta>
            <ta e="T8" id="Seg_118" s="T7">be-FUT-2SG.S</ta>
            <ta e="T9" id="Seg_119" s="T8">you.SG.NOM</ta>
            <ta e="T10" id="Seg_120" s="T9">probably</ta>
            <ta e="T11" id="Seg_121" s="T10">I.ACC</ta>
            <ta e="T12" id="Seg_122" s="T11">fast</ta>
            <ta e="T13" id="Seg_123" s="T12">NEG</ta>
            <ta e="T14" id="Seg_124" s="T13">forgot-FUT-2SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_125" s="T1">ты.NOM</ta>
            <ta e="T3" id="Seg_126" s="T2">я.GEN</ta>
            <ta e="T4" id="Seg_127" s="T3">ты.NOM</ta>
            <ta e="T5" id="Seg_128" s="T4">я.GEN</ta>
            <ta e="T6" id="Seg_129" s="T5">ты.NOM</ta>
            <ta e="T7" id="Seg_130" s="T6">я.GEN</ta>
            <ta e="T8" id="Seg_131" s="T7">быть-FUT-2SG.S</ta>
            <ta e="T9" id="Seg_132" s="T8">ты.NOM</ta>
            <ta e="T10" id="Seg_133" s="T9">наверное</ta>
            <ta e="T11" id="Seg_134" s="T10">я.ACC</ta>
            <ta e="T12" id="Seg_135" s="T11">быстро</ta>
            <ta e="T13" id="Seg_136" s="T12">NEG</ta>
            <ta e="T14" id="Seg_137" s="T13">забыть-FUT-2SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_138" s="T1">pers</ta>
            <ta e="T3" id="Seg_139" s="T2">pers</ta>
            <ta e="T4" id="Seg_140" s="T3">pers</ta>
            <ta e="T5" id="Seg_141" s="T4">pers</ta>
            <ta e="T6" id="Seg_142" s="T5">pers</ta>
            <ta e="T7" id="Seg_143" s="T6">pers</ta>
            <ta e="T8" id="Seg_144" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_145" s="T8">pers</ta>
            <ta e="T10" id="Seg_146" s="T9">adv</ta>
            <ta e="T11" id="Seg_147" s="T10">pers</ta>
            <ta e="T12" id="Seg_148" s="T11">adv</ta>
            <ta e="T13" id="Seg_149" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_150" s="T13">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_151" s="T1">pers</ta>
            <ta e="T3" id="Seg_152" s="T2">pers</ta>
            <ta e="T4" id="Seg_153" s="T3">pers</ta>
            <ta e="T5" id="Seg_154" s="T4">pers</ta>
            <ta e="T6" id="Seg_155" s="T5">pers</ta>
            <ta e="T7" id="Seg_156" s="T6">pers</ta>
            <ta e="T8" id="Seg_157" s="T7">v</ta>
            <ta e="T9" id="Seg_158" s="T8">pers</ta>
            <ta e="T10" id="Seg_159" s="T9">adv</ta>
            <ta e="T11" id="Seg_160" s="T10">pers</ta>
            <ta e="T12" id="Seg_161" s="T11">adv</ta>
            <ta e="T13" id="Seg_162" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_163" s="T13">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T6" id="Seg_164" s="T5">pro.h:S</ta>
            <ta e="T8" id="Seg_165" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_166" s="T8">pro.h:S</ta>
            <ta e="T11" id="Seg_167" s="T10">pro.h:O</ta>
            <ta e="T14" id="Seg_168" s="T13">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_169" s="T1">pro.h:Th</ta>
            <ta e="T3" id="Seg_170" s="T2">pro.h:Poss</ta>
            <ta e="T4" id="Seg_171" s="T3">pro.h:Th</ta>
            <ta e="T5" id="Seg_172" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_173" s="T5">pro.h:Th</ta>
            <ta e="T7" id="Seg_174" s="T6">pro.h:Poss</ta>
            <ta e="T9" id="Seg_175" s="T8">pro.h:E</ta>
            <ta e="T11" id="Seg_176" s="T10">pro.h:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_177" s="T9">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_178" s="T1">You are mine, you are mine, you will be mine.</ta>
            <ta e="T14" id="Seg_179" s="T8">Probably you won't forget me fast.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_180" s="T1">Du bist mein, du bist mein, du wirst mein sein.</ta>
            <ta e="T14" id="Seg_181" s="T8">Vielleicht wirst du mich nicht schnell vergessen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_182" s="T1">Ты моя, ты моя, ты моя и будешь.</ta>
            <ta e="T14" id="Seg_183" s="T8">Ты, наверное, меня скоро не забудешь.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
