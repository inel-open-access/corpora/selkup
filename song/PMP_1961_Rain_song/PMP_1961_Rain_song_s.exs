<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PMP_1961_Rain_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PMP_1961_Rain_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">122</ud-information>
            <ud-information attribute-name="# HIAT:w">79</ud-information>
            <ud-information attribute-name="# e">79</ud-information>
            <ud-information attribute-name="# HIAT:u">15</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PMP">
            <abbreviation>PMP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PMP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T80" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nom</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">sorolǯiŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip">/</nts>
                  <nts id="Seg_9" n="HIAT:ip">/</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">nom</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">tesunǯiŋ</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Eut</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">iːm</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">qonǯit</ts>
                  <nts id="Seg_31" n="HIAT:ip">/</nts>
                  <nts id="Seg_32" n="HIAT:ip">/</nts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Werä</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">qum</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">ennanǯ</ts>
                  <nts id="Seg_45" n="HIAT:ip">/</nts>
                  <nts id="Seg_46" n="HIAT:ip">/</nts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_50" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Nom</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">sorok</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">nom</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">sorok</ts>
                  <nts id="Seg_63" n="HIAT:ip">/</nts>
                  <nts id="Seg_64" n="HIAT:ip">/</nts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_68" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_70" n="HIAT:w" s="T16">Man</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_73" n="HIAT:w" s="T17">uloɣeŋ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">i</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">qulan</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">olond</ts>
                  <nts id="Seg_83" n="HIAT:ip">/</nts>
                  <nts id="Seg_84" n="HIAT:ip">/</nts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_88" n="HIAT:w" s="T21">mannani</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">soːrtej</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">qone</ts>
                  <nts id="Seg_95" n="HIAT:ip">/</nts>
                  <nts id="Seg_96" n="HIAT:ip">/</nts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">okkɨr</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">kɨdʼirenij</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">ik</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">tʼälʼdik</ts>
                  <nts id="Seg_110" n="HIAT:ip">/</nts>
                  <nts id="Seg_111" n="HIAT:ip">/</nts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_115" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_117" n="HIAT:w" s="T28">Nom</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">sorolǯiŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T30">nom</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">sorolǯiŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip">/</nts>
                  <nts id="Seg_129" n="HIAT:ip">/</nts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_133" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_135" n="HIAT:w" s="T32">Tʼäptä</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_138" n="HIAT:w" s="T33">tʼäptädʼiŋ</ts>
                  <nts id="Seg_139" n="HIAT:ip">/</nts>
                  <nts id="Seg_140" n="HIAT:ip">/</nts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_144" n="HIAT:w" s="T34">pon</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_147" n="HIAT:w" s="T35">olond</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_150" n="HIAT:w" s="T36">tʼänǯilʼe</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_153" n="HIAT:w" s="T37">čagediŋ</ts>
                  <nts id="Seg_154" n="HIAT:ip">/</nts>
                  <nts id="Seg_155" n="HIAT:ip">/</nts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_159" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_161" n="HIAT:w" s="T38">Aj</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_164" n="HIAT:w" s="T39">tʼäptädʼiŋ</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">pon</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_171" n="HIAT:w" s="T41">olont</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">čanǯilʼe</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_177" n="HIAT:w" s="T43">čagedʼiŋ</ts>
                  <nts id="Seg_178" n="HIAT:ip">/</nts>
                  <nts id="Seg_179" n="HIAT:ip">/</nts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_183" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_185" n="HIAT:w" s="T44">Tau</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_188" n="HIAT:w" s="T45">tʼäptäm</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">ugon</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_194" n="HIAT:w" s="T47">man</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_197" n="HIAT:w" s="T48">kelʼlʼe</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_200" n="HIAT:w" s="T49">taːdərkuzaw</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_203" n="HIAT:w" s="T50">oneŋ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_206" n="HIAT:w" s="T51">kɨba</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_209" n="HIAT:w" s="T52">mɨlawne</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_213" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">Tabla</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_218" n="HIAT:w" s="T54">tʼäptäm</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">soːrkuzattə</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_225" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_227" n="HIAT:w" s="T56">Mazɨm</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_230" n="HIAT:w" s="T57">awan</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_233" n="HIAT:w" s="T58">as</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">tämbɨkuzatte</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_240" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_242" n="HIAT:w" s="T60">A</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_245" n="HIAT:w" s="T61">man</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_248" n="HIAT:w" s="T62">tablane</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_251" n="HIAT:w" s="T63">kɨdaŋ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_254" n="HIAT:w" s="T64">tau</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_257" n="HIAT:w" s="T65">tʼäptäm</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_260" n="HIAT:w" s="T66">kelʼlʼe</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_263" n="HIAT:w" s="T67">taːderkuzaw</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_267" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_269" n="HIAT:w" s="T68">Tau</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_272" n="HIAT:w" s="T69">tʼäptä</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">sidʼäkaj</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_279" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_281" n="HIAT:w" s="T71">Man</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_284" n="HIAT:w" s="T72">tablane</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_287" n="HIAT:w" s="T73">sidʼäŋ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_290" n="HIAT:w" s="T74">kelʼlʼe</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_293" n="HIAT:w" s="T75">taːdərkuzaw</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_297" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_299" n="HIAT:w" s="T76">A</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_302" n="HIAT:w" s="T77">kɨba</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_305" n="HIAT:w" s="T78">mɨla</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_308" n="HIAT:w" s="T79">üŋgulǯimbɨzatte</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T80" id="Seg_311" n="sc" s="T1">
               <ts e="T2" id="Seg_313" n="e" s="T1">Nom </ts>
               <ts e="T3" id="Seg_315" n="e" s="T2">sorolǯiŋ// </ts>
               <ts e="T4" id="Seg_317" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_319" n="e" s="T4">nom </ts>
               <ts e="T6" id="Seg_321" n="e" s="T5">tesunǯiŋ. </ts>
               <ts e="T7" id="Seg_323" n="e" s="T6">Eut </ts>
               <ts e="T8" id="Seg_325" n="e" s="T7">iːm </ts>
               <ts e="T9" id="Seg_327" n="e" s="T8">qonǯit//. </ts>
               <ts e="T10" id="Seg_329" n="e" s="T9">Werä </ts>
               <ts e="T11" id="Seg_331" n="e" s="T10">qum </ts>
               <ts e="T12" id="Seg_333" n="e" s="T11">ennanǯ//. </ts>
               <ts e="T13" id="Seg_335" n="e" s="T12">Nom </ts>
               <ts e="T14" id="Seg_337" n="e" s="T13">sorok, </ts>
               <ts e="T15" id="Seg_339" n="e" s="T14">nom </ts>
               <ts e="T16" id="Seg_341" n="e" s="T15">sorok//. </ts>
               <ts e="T17" id="Seg_343" n="e" s="T16">Man </ts>
               <ts e="T18" id="Seg_345" n="e" s="T17">uloɣeŋ </ts>
               <ts e="T19" id="Seg_347" n="e" s="T18">i </ts>
               <ts e="T20" id="Seg_349" n="e" s="T19">qulan </ts>
               <ts e="T21" id="Seg_351" n="e" s="T20">olond//, </ts>
               <ts e="T22" id="Seg_353" n="e" s="T21">mannani </ts>
               <ts e="T23" id="Seg_355" n="e" s="T22">soːrtej </ts>
               <ts e="T24" id="Seg_357" n="e" s="T23">qone//, </ts>
               <ts e="T25" id="Seg_359" n="e" s="T24">okkɨr </ts>
               <ts e="T26" id="Seg_361" n="e" s="T25">kɨdʼirenij </ts>
               <ts e="T27" id="Seg_363" n="e" s="T26">ik </ts>
               <ts e="T28" id="Seg_365" n="e" s="T27">tʼälʼdik//. </ts>
               <ts e="T29" id="Seg_367" n="e" s="T28">Nom </ts>
               <ts e="T30" id="Seg_369" n="e" s="T29">sorolǯiŋ, </ts>
               <ts e="T31" id="Seg_371" n="e" s="T30">nom </ts>
               <ts e="T32" id="Seg_373" n="e" s="T31">sorolǯiŋ//. </ts>
               <ts e="T33" id="Seg_375" n="e" s="T32">Tʼäptä </ts>
               <ts e="T34" id="Seg_377" n="e" s="T33">tʼäptädʼiŋ//, </ts>
               <ts e="T35" id="Seg_379" n="e" s="T34">pon </ts>
               <ts e="T36" id="Seg_381" n="e" s="T35">olond </ts>
               <ts e="T37" id="Seg_383" n="e" s="T36">tʼänǯilʼe </ts>
               <ts e="T38" id="Seg_385" n="e" s="T37">čagediŋ//. </ts>
               <ts e="T39" id="Seg_387" n="e" s="T38">Aj </ts>
               <ts e="T40" id="Seg_389" n="e" s="T39">tʼäptädʼiŋ, </ts>
               <ts e="T41" id="Seg_391" n="e" s="T40">pon </ts>
               <ts e="T42" id="Seg_393" n="e" s="T41">olont </ts>
               <ts e="T43" id="Seg_395" n="e" s="T42">čanǯilʼe </ts>
               <ts e="T44" id="Seg_397" n="e" s="T43">čagedʼiŋ//. </ts>
               <ts e="T45" id="Seg_399" n="e" s="T44">Tau </ts>
               <ts e="T46" id="Seg_401" n="e" s="T45">tʼäptäm </ts>
               <ts e="T47" id="Seg_403" n="e" s="T46">ugon </ts>
               <ts e="T48" id="Seg_405" n="e" s="T47">man </ts>
               <ts e="T49" id="Seg_407" n="e" s="T48">kelʼlʼe </ts>
               <ts e="T50" id="Seg_409" n="e" s="T49">taːdərkuzaw </ts>
               <ts e="T51" id="Seg_411" n="e" s="T50">oneŋ </ts>
               <ts e="T52" id="Seg_413" n="e" s="T51">kɨba </ts>
               <ts e="T53" id="Seg_415" n="e" s="T52">mɨlawne. </ts>
               <ts e="T54" id="Seg_417" n="e" s="T53">Tabla </ts>
               <ts e="T55" id="Seg_419" n="e" s="T54">tʼäptäm </ts>
               <ts e="T56" id="Seg_421" n="e" s="T55">soːrkuzattə. </ts>
               <ts e="T57" id="Seg_423" n="e" s="T56">Mazɨm </ts>
               <ts e="T58" id="Seg_425" n="e" s="T57">awan </ts>
               <ts e="T59" id="Seg_427" n="e" s="T58">as </ts>
               <ts e="T60" id="Seg_429" n="e" s="T59">tämbɨkuzatte. </ts>
               <ts e="T61" id="Seg_431" n="e" s="T60">A </ts>
               <ts e="T62" id="Seg_433" n="e" s="T61">man </ts>
               <ts e="T63" id="Seg_435" n="e" s="T62">tablane </ts>
               <ts e="T64" id="Seg_437" n="e" s="T63">kɨdaŋ </ts>
               <ts e="T65" id="Seg_439" n="e" s="T64">tau </ts>
               <ts e="T66" id="Seg_441" n="e" s="T65">tʼäptäm </ts>
               <ts e="T67" id="Seg_443" n="e" s="T66">kelʼlʼe </ts>
               <ts e="T68" id="Seg_445" n="e" s="T67">taːderkuzaw. </ts>
               <ts e="T69" id="Seg_447" n="e" s="T68">Tau </ts>
               <ts e="T70" id="Seg_449" n="e" s="T69">tʼäptä </ts>
               <ts e="T71" id="Seg_451" n="e" s="T70">sidʼäkaj. </ts>
               <ts e="T72" id="Seg_453" n="e" s="T71">Man </ts>
               <ts e="T73" id="Seg_455" n="e" s="T72">tablane </ts>
               <ts e="T74" id="Seg_457" n="e" s="T73">sidʼäŋ </ts>
               <ts e="T75" id="Seg_459" n="e" s="T74">kelʼlʼe </ts>
               <ts e="T76" id="Seg_461" n="e" s="T75">taːdərkuzaw. </ts>
               <ts e="T77" id="Seg_463" n="e" s="T76">A </ts>
               <ts e="T78" id="Seg_465" n="e" s="T77">kɨba </ts>
               <ts e="T79" id="Seg_467" n="e" s="T78">mɨla </ts>
               <ts e="T80" id="Seg_469" n="e" s="T79">üŋgulǯimbɨzatte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_470" s="T1">PMP_1961_Rain_song.001 (001.001)</ta>
            <ta e="T9" id="Seg_471" s="T6">PMP_1961_Rain_song.002 (001.002)</ta>
            <ta e="T12" id="Seg_472" s="T9">PMP_1961_Rain_song.003 (001.003)</ta>
            <ta e="T16" id="Seg_473" s="T12">PMP_1961_Rain_song.004 (001.004)</ta>
            <ta e="T28" id="Seg_474" s="T16">PMP_1961_Rain_song.005 (001.005)</ta>
            <ta e="T32" id="Seg_475" s="T28">PMP_1961_Rain_song.006 (001.006)</ta>
            <ta e="T38" id="Seg_476" s="T32">PMP_1961_Rain_song.007 (001.007)</ta>
            <ta e="T44" id="Seg_477" s="T38">PMP_1961_Rain_song.008 (001.008)</ta>
            <ta e="T53" id="Seg_478" s="T44">PMP_1961_Rain_song.009 (001.009)</ta>
            <ta e="T56" id="Seg_479" s="T53">PMP_1961_Rain_song.010 (001.010)</ta>
            <ta e="T60" id="Seg_480" s="T56">PMP_1961_Rain_song.011 (001.011)</ta>
            <ta e="T68" id="Seg_481" s="T60">PMP_1961_Rain_song.012 (001.012)</ta>
            <ta e="T71" id="Seg_482" s="T68">PMP_1961_Rain_song.013 (001.013)</ta>
            <ta e="T76" id="Seg_483" s="T71">PMP_1961_Rain_song.014 (001.014)</ta>
            <ta e="T80" id="Seg_484" s="T76">PMP_1961_Rain_song.015 (001.015)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_485" s="T1">ном со′ролджиң // и ном те̨′сунджиң.</ta>
            <ta e="T9" id="Seg_486" s="T6">е′ут ӣм kонд′жит //.</ta>
            <ta e="T12" id="Seg_487" s="T9">′верӓ ′kум ′еннандж //.</ta>
            <ta e="T16" id="Seg_488" s="T12">ном со′рок, ном со′рок //.</ta>
            <ta e="T28" id="Seg_489" s="T16">ман у(о)′лоɣең и kу′лан о′лонд //, ма′ннани ′со̄ртей kо′не //, оккыр кыдʼи′рений ик ′тʼӓлʼдик //.</ta>
            <ta e="T32" id="Seg_490" s="T28">ном со′ролджиң, ном со′ролджиң //.</ta>
            <ta e="T38" id="Seg_491" s="T32">тʼӓп′тӓ тʼӓп′тӓдʼиң //, пон о′лонд тʼӓнджилʼе тʼша′гедиң //.</ta>
            <ta e="T44" id="Seg_492" s="T38">ай тʼӓп′тӓдʼиң, пон о′лонт тшанджи′лʼе тша′гедʼиң //.</ta>
            <ta e="T53" id="Seg_493" s="T44">′тау тʼӓп′тӓм у′гон ман ′келʼлʼе ′та̄дърку′заw о′нең кы′ба мы′лавне.</ta>
            <ta e="T56" id="Seg_494" s="T53">таб′ла тʼӓп′тӓм ′со̄рку′заттъ.</ta>
            <ta e="T60" id="Seg_495" s="T56">ма′зым а′ван ас тӓмбыку′затте.</ta>
            <ta e="T68" id="Seg_496" s="T60">а ман таб′лане ′кыдаң тау тʼӓп′тӓм ′келʼлʼе ′та̄дерку′заw.</ta>
            <ta e="T71" id="Seg_497" s="T68">′тау тʼӓп′тӓ си′дʼӓкай.</ta>
            <ta e="T76" id="Seg_498" s="T71">ман таб′лане си′дʼӓң ке′лʼлʼе ‵та̄дърку′заw.</ta>
            <ta e="T80" id="Seg_499" s="T76">а кы′ба мы′ла ӱңгулджимбы′затте.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_500" s="T1">nom sorolǯiŋ // i nom tesunǯiŋ.</ta>
            <ta e="T9" id="Seg_501" s="T6">eut iːm qonǯit //.</ta>
            <ta e="T12" id="Seg_502" s="T9">werä qum ennanǯ //.</ta>
            <ta e="T16" id="Seg_503" s="T12">nom sorok, nom sorok //.</ta>
            <ta e="T28" id="Seg_504" s="T16">man u(o)loɣeŋ i qulan olond //, mannani soːrtej qone //, okkɨr kɨdʼirenij ik tʼälʼdik //.</ta>
            <ta e="T32" id="Seg_505" s="T28">nom sorolǯiŋ, nom sorolǯiŋ //.</ta>
            <ta e="T38" id="Seg_506" s="T32">tʼäptä tʼäptädʼiŋ //, pon olond tʼänǯilʼe tʼšagediŋ //.</ta>
            <ta e="T44" id="Seg_507" s="T38">aj tʼäptädʼiŋ, pon olont čanǯilʼe čagedʼiŋ //.</ta>
            <ta e="T53" id="Seg_508" s="T44">tau tʼäptäm ugon man kelʼlʼe taːdərkuzaw oneŋ kɨba mɨlawne.</ta>
            <ta e="T56" id="Seg_509" s="T53">tabla tʼäptäm soːrkuzattə.</ta>
            <ta e="T60" id="Seg_510" s="T56">mazɨm awan as tämbɨkuzatte.</ta>
            <ta e="T68" id="Seg_511" s="T60">a man tablane kɨdaŋ tau tʼäptäm kelʼlʼe taːderkuzaw.</ta>
            <ta e="T71" id="Seg_512" s="T68">tau tʼäptä sidʼäkaj.</ta>
            <ta e="T76" id="Seg_513" s="T71">man tablane sidʼäŋ kelʼlʼe taːdərkuzaw.</ta>
            <ta e="T80" id="Seg_514" s="T76">a kɨba mɨla üŋgulǯimbɨzatte.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_515" s="T1">Nom sorolǯiŋ// i nom tesunǯiŋ. </ta>
            <ta e="T9" id="Seg_516" s="T6">Eut iːm qonǯit//. </ta>
            <ta e="T12" id="Seg_517" s="T9">Werä qum ennanǯ//. </ta>
            <ta e="T16" id="Seg_518" s="T12">Nom sorok, nom sorok//. </ta>
            <ta e="T28" id="Seg_519" s="T16">Man uloɣeŋ i qulan olond//, mannani soːrtej qone//, okkɨr kɨdʼirenij ik tʼälʼdik//. </ta>
            <ta e="T32" id="Seg_520" s="T28">Nom sorolǯiŋ, nom sorolǯiŋ//. </ta>
            <ta e="T38" id="Seg_521" s="T32">Tʼäptä tʼäptädʼiŋ//, pon olond tʼänǯilʼe čagediŋ//. </ta>
            <ta e="T44" id="Seg_522" s="T38">Aj tʼäptädʼiŋ, pon olont čanǯilʼe čagedʼiŋ//. </ta>
            <ta e="T53" id="Seg_523" s="T44">Tau tʼäptäm ugon man kelʼlʼe taːdərkuzaw oneŋ kɨba mɨlawne. </ta>
            <ta e="T56" id="Seg_524" s="T53">Tabla tʼäptäm soːrkuzattə. </ta>
            <ta e="T60" id="Seg_525" s="T56">Mazɨm awan as tämbɨkuzatte. </ta>
            <ta e="T68" id="Seg_526" s="T60">A man tablane kɨdaŋ tau tʼäptäm kelʼlʼe taːderkuzaw. </ta>
            <ta e="T71" id="Seg_527" s="T68">Tau tʼäptä sidʼäkaj. </ta>
            <ta e="T76" id="Seg_528" s="T71">Man tablane sidʼäŋ kelʼlʼe taːdərkuzaw. </ta>
            <ta e="T80" id="Seg_529" s="T76">A kɨba mɨla üŋgulǯimbɨzatte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_530" s="T1">nom</ta>
            <ta e="T3" id="Seg_531" s="T2">soro-l-ǯi-ŋ</ta>
            <ta e="T4" id="Seg_532" s="T3">i</ta>
            <ta e="T5" id="Seg_533" s="T4">nom</ta>
            <ta e="T6" id="Seg_534" s="T5">tesu-nǯi-ŋ</ta>
            <ta e="T7" id="Seg_535" s="T6">eu-t</ta>
            <ta e="T8" id="Seg_536" s="T7">iː-m</ta>
            <ta e="T9" id="Seg_537" s="T8">qo-nǯi-t</ta>
            <ta e="T10" id="Seg_538" s="T9">werä</ta>
            <ta e="T11" id="Seg_539" s="T10">qum</ta>
            <ta e="T12" id="Seg_540" s="T11">e-nna-nǯ</ta>
            <ta e="T13" id="Seg_541" s="T12">nom</ta>
            <ta e="T14" id="Seg_542" s="T13">soro-k</ta>
            <ta e="T15" id="Seg_543" s="T14">nom</ta>
            <ta e="T16" id="Seg_544" s="T15">soro-k</ta>
            <ta e="T17" id="Seg_545" s="T16">man</ta>
            <ta e="T18" id="Seg_546" s="T17">ulo-ɣeŋ</ta>
            <ta e="T19" id="Seg_547" s="T18">i</ta>
            <ta e="T20" id="Seg_548" s="T19">qu-la-n</ta>
            <ta e="T21" id="Seg_549" s="T20">olo-nd</ta>
            <ta e="T22" id="Seg_550" s="T21">man-nan-i</ta>
            <ta e="T23" id="Seg_551" s="T22">soːr-tej</ta>
            <ta e="T24" id="Seg_552" s="T23">qo-ne</ta>
            <ta e="T25" id="Seg_553" s="T24">okkɨr</ta>
            <ta e="T26" id="Seg_554" s="T25">kɨdʼirenij</ta>
            <ta e="T27" id="Seg_555" s="T26">ik</ta>
            <ta e="T28" id="Seg_556" s="T27">tʼälʼdi-k</ta>
            <ta e="T29" id="Seg_557" s="T28">nom</ta>
            <ta e="T30" id="Seg_558" s="T29">soro-l-ǯi-ŋ</ta>
            <ta e="T31" id="Seg_559" s="T30">nom</ta>
            <ta e="T32" id="Seg_560" s="T31">soro-l-ǯi-ŋ</ta>
            <ta e="T33" id="Seg_561" s="T32">tʼäptä</ta>
            <ta e="T34" id="Seg_562" s="T33">tʼäptä-dʼi-ŋ</ta>
            <ta e="T35" id="Seg_563" s="T34">po-n</ta>
            <ta e="T36" id="Seg_564" s="T35">olo-nd</ta>
            <ta e="T37" id="Seg_565" s="T36">tʼänǯi-lʼe</ta>
            <ta e="T38" id="Seg_566" s="T37">čage-di-ŋ</ta>
            <ta e="T39" id="Seg_567" s="T38">aj</ta>
            <ta e="T40" id="Seg_568" s="T39">tʼäptä-dʼi-ŋ</ta>
            <ta e="T41" id="Seg_569" s="T40">po-n</ta>
            <ta e="T42" id="Seg_570" s="T41">olo-nt</ta>
            <ta e="T43" id="Seg_571" s="T42">čanǯi-lʼe</ta>
            <ta e="T44" id="Seg_572" s="T43">čage-dʼi-ŋ</ta>
            <ta e="T45" id="Seg_573" s="T44">tau</ta>
            <ta e="T46" id="Seg_574" s="T45">tʼäptä-m</ta>
            <ta e="T47" id="Seg_575" s="T46">ugon</ta>
            <ta e="T48" id="Seg_576" s="T47">man</ta>
            <ta e="T49" id="Seg_577" s="T48">kelʼ-lʼe</ta>
            <ta e="T50" id="Seg_578" s="T49">taːd-ə-r-ku-za-w</ta>
            <ta e="T51" id="Seg_579" s="T50">oneŋ</ta>
            <ta e="T52" id="Seg_580" s="T51">kɨba</ta>
            <ta e="T53" id="Seg_581" s="T52">mɨ-la-w-ne</ta>
            <ta e="T54" id="Seg_582" s="T53">tab-la</ta>
            <ta e="T55" id="Seg_583" s="T54">tʼäptä-m</ta>
            <ta e="T56" id="Seg_584" s="T55">soːr-ku-za-ttə</ta>
            <ta e="T57" id="Seg_585" s="T56">mazɨm</ta>
            <ta e="T58" id="Seg_586" s="T57">awa-n</ta>
            <ta e="T59" id="Seg_587" s="T58">as</ta>
            <ta e="T60" id="Seg_588" s="T59">tämbɨ-ku-za-tte</ta>
            <ta e="T61" id="Seg_589" s="T60">a</ta>
            <ta e="T62" id="Seg_590" s="T61">man</ta>
            <ta e="T63" id="Seg_591" s="T62">tab-la-ne</ta>
            <ta e="T64" id="Seg_592" s="T63">kɨdaŋ</ta>
            <ta e="T65" id="Seg_593" s="T64">tau</ta>
            <ta e="T66" id="Seg_594" s="T65">tʼäptä-m</ta>
            <ta e="T67" id="Seg_595" s="T66">kelʼ-lʼe</ta>
            <ta e="T68" id="Seg_596" s="T67">taːd-e-r-ku-za-w</ta>
            <ta e="T69" id="Seg_597" s="T68">tau</ta>
            <ta e="T70" id="Seg_598" s="T69">tʼäptä</ta>
            <ta e="T71" id="Seg_599" s="T70">sidʼä-ka-j</ta>
            <ta e="T72" id="Seg_600" s="T71">man</ta>
            <ta e="T73" id="Seg_601" s="T72">tab-la-ne</ta>
            <ta e="T74" id="Seg_602" s="T73">sidʼä-ŋ</ta>
            <ta e="T75" id="Seg_603" s="T74">kelʼ-lʼe</ta>
            <ta e="T76" id="Seg_604" s="T75">taːd-ə-r-ku-za-w</ta>
            <ta e="T77" id="Seg_605" s="T76">a</ta>
            <ta e="T78" id="Seg_606" s="T77">kɨba</ta>
            <ta e="T79" id="Seg_607" s="T78">mɨ-la</ta>
            <ta e="T80" id="Seg_608" s="T79">üŋgulǯi-mbɨ-za-tte</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_609" s="T1">nom</ta>
            <ta e="T3" id="Seg_610" s="T2">soro-l-enǯɨ-n</ta>
            <ta e="T4" id="Seg_611" s="T3">i</ta>
            <ta e="T5" id="Seg_612" s="T4">nom</ta>
            <ta e="T6" id="Seg_613" s="T5">tesu-enǯɨ-n</ta>
            <ta e="T7" id="Seg_614" s="T6">awa-tə</ta>
            <ta e="T8" id="Seg_615" s="T7">iː-w</ta>
            <ta e="T9" id="Seg_616" s="T8">qo-enǯɨ-t</ta>
            <ta e="T10" id="Seg_617" s="T9">wəri</ta>
            <ta e="T11" id="Seg_618" s="T10">qum</ta>
            <ta e="T12" id="Seg_619" s="T11">eː-ntɨ-enǯɨ</ta>
            <ta e="T13" id="Seg_620" s="T12">nom</ta>
            <ta e="T14" id="Seg_621" s="T13">soro-kɨ</ta>
            <ta e="T15" id="Seg_622" s="T14">nom</ta>
            <ta e="T16" id="Seg_623" s="T15">soro-kɨ</ta>
            <ta e="T17" id="Seg_624" s="T16">man</ta>
            <ta e="T18" id="Seg_625" s="T17">olə-qɨn</ta>
            <ta e="T19" id="Seg_626" s="T18">i</ta>
            <ta e="T20" id="Seg_627" s="T19">qum-la-n</ta>
            <ta e="T21" id="Seg_628" s="T20">olə-ntə</ta>
            <ta e="T22" id="Seg_629" s="T21">man-nan-lʼ</ta>
            <ta e="T23" id="Seg_630" s="T22">soːrɨ-ntɨ</ta>
            <ta e="T24" id="Seg_631" s="T23">kö-nä</ta>
            <ta e="T25" id="Seg_632" s="T24">okkɨr</ta>
            <ta e="T26" id="Seg_633" s="T25">kɨdʼirenij</ta>
            <ta e="T27" id="Seg_634" s="T26">igə</ta>
            <ta e="T28" id="Seg_635" s="T27">tʼälʼdʼzʼi-kɨ</ta>
            <ta e="T29" id="Seg_636" s="T28">nom</ta>
            <ta e="T30" id="Seg_637" s="T29">soro-l-enǯɨ-n</ta>
            <ta e="T31" id="Seg_638" s="T30">nom</ta>
            <ta e="T32" id="Seg_639" s="T31">soro-l-enǯɨ-n</ta>
            <ta e="T33" id="Seg_640" s="T32">tʼäptä</ta>
            <ta e="T34" id="Seg_641" s="T33">tʼäptə-dʼi-n</ta>
            <ta e="T35" id="Seg_642" s="T34">po-n</ta>
            <ta e="T36" id="Seg_643" s="T35">olə-ntə</ta>
            <ta e="T37" id="Seg_644" s="T36">čanǯu-le</ta>
            <ta e="T38" id="Seg_645" s="T37">čagɨ-dʼi-n</ta>
            <ta e="T39" id="Seg_646" s="T38">aj</ta>
            <ta e="T40" id="Seg_647" s="T39">tʼäptə-dʼi-n</ta>
            <ta e="T41" id="Seg_648" s="T40">po-n</ta>
            <ta e="T42" id="Seg_649" s="T41">olə-ntə</ta>
            <ta e="T43" id="Seg_650" s="T42">čanǯu-le</ta>
            <ta e="T44" id="Seg_651" s="T43">čaǯɨ-dʼi-n</ta>
            <ta e="T45" id="Seg_652" s="T44">taw</ta>
            <ta e="T46" id="Seg_653" s="T45">tʼäptä-m</ta>
            <ta e="T47" id="Seg_654" s="T46">ugon</ta>
            <ta e="T48" id="Seg_655" s="T47">man</ta>
            <ta e="T49" id="Seg_656" s="T48">ket-le</ta>
            <ta e="T50" id="Seg_657" s="T49">tat-ɨ-r-ku-sɨ-w</ta>
            <ta e="T51" id="Seg_658" s="T50">oneŋ</ta>
            <ta e="T52" id="Seg_659" s="T51">qɨba</ta>
            <ta e="T53" id="Seg_660" s="T52">mɨ-la-w-nä</ta>
            <ta e="T54" id="Seg_661" s="T53">täp-la</ta>
            <ta e="T55" id="Seg_662" s="T54">tʼäptä-m</ta>
            <ta e="T56" id="Seg_663" s="T55">soːrɨ-ku-sɨ-tɨn</ta>
            <ta e="T57" id="Seg_664" s="T56">mazɨm</ta>
            <ta e="T58" id="Seg_665" s="T57">awa-ŋ</ta>
            <ta e="T59" id="Seg_666" s="T58">asa</ta>
            <ta e="T60" id="Seg_667" s="T59">tämbɨ-ku-sɨ-tɨn</ta>
            <ta e="T61" id="Seg_668" s="T60">a</ta>
            <ta e="T62" id="Seg_669" s="T61">man</ta>
            <ta e="T63" id="Seg_670" s="T62">täp-la-nä</ta>
            <ta e="T64" id="Seg_671" s="T63">qɨdan</ta>
            <ta e="T65" id="Seg_672" s="T64">taw</ta>
            <ta e="T66" id="Seg_673" s="T65">tʼäptä-m</ta>
            <ta e="T67" id="Seg_674" s="T66">ket-le</ta>
            <ta e="T68" id="Seg_675" s="T67">tat-ɨ-r-ku-sɨ-w</ta>
            <ta e="T69" id="Seg_676" s="T68">taw</ta>
            <ta e="T70" id="Seg_677" s="T69">tʼäptä</ta>
            <ta e="T71" id="Seg_678" s="T70">sidʼä-ka-lʼ</ta>
            <ta e="T72" id="Seg_679" s="T71">man</ta>
            <ta e="T73" id="Seg_680" s="T72">täp-la-nä</ta>
            <ta e="T74" id="Seg_681" s="T73">sidʼä-ŋ</ta>
            <ta e="T75" id="Seg_682" s="T74">ket-le</ta>
            <ta e="T76" id="Seg_683" s="T75">tat-ɨ-r-ku-sɨ-w</ta>
            <ta e="T77" id="Seg_684" s="T76">a</ta>
            <ta e="T78" id="Seg_685" s="T77">qɨba</ta>
            <ta e="T79" id="Seg_686" s="T78">mɨ-la</ta>
            <ta e="T80" id="Seg_687" s="T79">üŋgulǯu-mbɨ-sɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_688" s="T1">sky.[NOM]</ta>
            <ta e="T3" id="Seg_689" s="T2">rain-INCH-FUT-3SG.S</ta>
            <ta e="T4" id="Seg_690" s="T3">and</ta>
            <ta e="T5" id="Seg_691" s="T4">sky.[NOM]</ta>
            <ta e="T6" id="Seg_692" s="T5">be.frosty-FUT-3SG.S</ta>
            <ta e="T7" id="Seg_693" s="T6">mother.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_694" s="T7">son.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_695" s="T8">give.birth-FUT-3SG.O</ta>
            <ta e="T10" id="Seg_696" s="T9">restless</ta>
            <ta e="T11" id="Seg_697" s="T10">human.being.[NOM]</ta>
            <ta e="T12" id="Seg_698" s="T11">be-INFER-FUT.[3SG.S]</ta>
            <ta e="T13" id="Seg_699" s="T12">sky.[NOM]</ta>
            <ta e="T14" id="Seg_700" s="T13">rain-IMP.2SG.S</ta>
            <ta e="T15" id="Seg_701" s="T14">sky.[NOM]</ta>
            <ta e="T16" id="Seg_702" s="T15">rain-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_703" s="T16">I.GEN</ta>
            <ta e="T18" id="Seg_704" s="T17">top-LOC</ta>
            <ta e="T19" id="Seg_705" s="T18">and</ta>
            <ta e="T20" id="Seg_706" s="T19">human.being-PL-GEN</ta>
            <ta e="T21" id="Seg_707" s="T20">top-ILL</ta>
            <ta e="T22" id="Seg_708" s="T21">I-ADES-ADJZ</ta>
            <ta e="T23" id="Seg_709" s="T22">love-PTCP.PRS</ta>
            <ta e="T24" id="Seg_710" s="T23">side-ALL</ta>
            <ta e="T25" id="Seg_711" s="T24">one</ta>
            <ta e="T26" id="Seg_712" s="T25">%%</ta>
            <ta e="T27" id="Seg_713" s="T26">NEG.IMP</ta>
            <ta e="T28" id="Seg_714" s="T27">%%-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_715" s="T28">sky.[NOM]</ta>
            <ta e="T30" id="Seg_716" s="T29">rain-INCH-FUT-3SG.S</ta>
            <ta e="T31" id="Seg_717" s="T30">sky.[NOM]</ta>
            <ta e="T32" id="Seg_718" s="T31">rain-INCH-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_719" s="T32">tale.[NOM]</ta>
            <ta e="T34" id="Seg_720" s="T33">get.wet-DRV-3SG.S</ta>
            <ta e="T35" id="Seg_721" s="T34">tree-GEN</ta>
            <ta e="T36" id="Seg_722" s="T35">top-ILL</ta>
            <ta e="T37" id="Seg_723" s="T36">climb.onto-CVB</ta>
            <ta e="T38" id="Seg_724" s="T37">dry-DRV-3SG.S</ta>
            <ta e="T39" id="Seg_725" s="T38">again</ta>
            <ta e="T40" id="Seg_726" s="T39">get.wet-DRV-3SG.S</ta>
            <ta e="T41" id="Seg_727" s="T40">tree-GEN</ta>
            <ta e="T42" id="Seg_728" s="T41">top-ILL</ta>
            <ta e="T43" id="Seg_729" s="T42">climb.onto-CVB</ta>
            <ta e="T44" id="Seg_730" s="T43">run-RFL-3SG.S</ta>
            <ta e="T45" id="Seg_731" s="T44">this</ta>
            <ta e="T46" id="Seg_732" s="T45">tale-ACC</ta>
            <ta e="T47" id="Seg_733" s="T46">earlier</ta>
            <ta e="T48" id="Seg_734" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_735" s="T48">say-CVB</ta>
            <ta e="T50" id="Seg_736" s="T49">bring-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T51" id="Seg_737" s="T50">own.1SG</ta>
            <ta e="T52" id="Seg_738" s="T51">small</ta>
            <ta e="T53" id="Seg_739" s="T52">something-PL-1SG-ALL</ta>
            <ta e="T54" id="Seg_740" s="T53">(s)he-PL.[NOM]</ta>
            <ta e="T55" id="Seg_741" s="T54">tale-ACC</ta>
            <ta e="T56" id="Seg_742" s="T55">love-HAB-PST-3PL</ta>
            <ta e="T57" id="Seg_743" s="T56">I.ACC</ta>
            <ta e="T58" id="Seg_744" s="T57">bad-ADVZ</ta>
            <ta e="T59" id="Seg_745" s="T58">NEG</ta>
            <ta e="T60" id="Seg_746" s="T59">%%-HAB-PST-3PL</ta>
            <ta e="T61" id="Seg_747" s="T60">and</ta>
            <ta e="T62" id="Seg_748" s="T61">I.NOM</ta>
            <ta e="T63" id="Seg_749" s="T62">(s)he-PL-ALL</ta>
            <ta e="T64" id="Seg_750" s="T63">all.the.time</ta>
            <ta e="T65" id="Seg_751" s="T64">this</ta>
            <ta e="T66" id="Seg_752" s="T65">tale-ACC</ta>
            <ta e="T67" id="Seg_753" s="T66">say-CVB</ta>
            <ta e="T68" id="Seg_754" s="T67">bring-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T69" id="Seg_755" s="T68">this</ta>
            <ta e="T70" id="Seg_756" s="T69">tale.[NOM]</ta>
            <ta e="T71" id="Seg_757" s="T70">lie-DIM-ADJZ</ta>
            <ta e="T72" id="Seg_758" s="T71">I.NOM</ta>
            <ta e="T73" id="Seg_759" s="T72">(s)he-PL-ALL</ta>
            <ta e="T74" id="Seg_760" s="T73">lie-ADVZ</ta>
            <ta e="T75" id="Seg_761" s="T74">say-CVB</ta>
            <ta e="T76" id="Seg_762" s="T75">bring-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T77" id="Seg_763" s="T76">and</ta>
            <ta e="T78" id="Seg_764" s="T77">small</ta>
            <ta e="T79" id="Seg_765" s="T78">something-PL.[NOM]</ta>
            <ta e="T80" id="Seg_766" s="T79">listen.to-DUR-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_767" s="T1">небо.[NOM]</ta>
            <ta e="T3" id="Seg_768" s="T2">идти(про_дождь)-INCH-FUT-3SG.S</ta>
            <ta e="T4" id="Seg_769" s="T3">и</ta>
            <ta e="T5" id="Seg_770" s="T4">небо.[NOM]</ta>
            <ta e="T6" id="Seg_771" s="T5">быть.морозу-FUT-3SG.S</ta>
            <ta e="T7" id="Seg_772" s="T6">мать.[NOM]-3SG</ta>
            <ta e="T8" id="Seg_773" s="T7">сын.[NOM]-1SG</ta>
            <ta e="T9" id="Seg_774" s="T8">родить-FUT-3SG.O</ta>
            <ta e="T10" id="Seg_775" s="T9">непоседливый</ta>
            <ta e="T11" id="Seg_776" s="T10">человек.[NOM]</ta>
            <ta e="T12" id="Seg_777" s="T11">быть-INFER-FUT.[3SG.S]</ta>
            <ta e="T13" id="Seg_778" s="T12">небо.[NOM]</ta>
            <ta e="T14" id="Seg_779" s="T13">идти(про_дождь)-IMP.2SG.S</ta>
            <ta e="T15" id="Seg_780" s="T14">небо.[NOM]</ta>
            <ta e="T16" id="Seg_781" s="T15">идти(про_дождь)-IMP.2SG.S</ta>
            <ta e="T17" id="Seg_782" s="T16">я.GEN</ta>
            <ta e="T18" id="Seg_783" s="T17">верхушка-LOC</ta>
            <ta e="T19" id="Seg_784" s="T18">и</ta>
            <ta e="T20" id="Seg_785" s="T19">человек-PL-GEN</ta>
            <ta e="T21" id="Seg_786" s="T20">верхушка-ILL</ta>
            <ta e="T22" id="Seg_787" s="T21">я-ADES-ADJZ</ta>
            <ta e="T23" id="Seg_788" s="T22">любить-PTCP.PRS</ta>
            <ta e="T24" id="Seg_789" s="T23">бок-ALL</ta>
            <ta e="T25" id="Seg_790" s="T24">один</ta>
            <ta e="T26" id="Seg_791" s="T25">%%</ta>
            <ta e="T27" id="Seg_792" s="T26">NEG.IMP</ta>
            <ta e="T28" id="Seg_793" s="T27">%%-IMP.2SG.S</ta>
            <ta e="T29" id="Seg_794" s="T28">небо.[NOM]</ta>
            <ta e="T30" id="Seg_795" s="T29">идти(про_дождь)-INCH-FUT-3SG.S</ta>
            <ta e="T31" id="Seg_796" s="T30">небо.[NOM]</ta>
            <ta e="T32" id="Seg_797" s="T31">идти(про_дождь)-INCH-FUT-3SG.S</ta>
            <ta e="T33" id="Seg_798" s="T32">сказка.[NOM]</ta>
            <ta e="T34" id="Seg_799" s="T33">намокнуть-DRV-3SG.S</ta>
            <ta e="T35" id="Seg_800" s="T34">дерево-GEN</ta>
            <ta e="T36" id="Seg_801" s="T35">верхушка-ILL</ta>
            <ta e="T37" id="Seg_802" s="T36">залезть-CVB</ta>
            <ta e="T38" id="Seg_803" s="T37">высохнуть-DRV-3SG.S</ta>
            <ta e="T39" id="Seg_804" s="T38">опять</ta>
            <ta e="T40" id="Seg_805" s="T39">намокнуть-DRV-3SG.S</ta>
            <ta e="T41" id="Seg_806" s="T40">дерево-GEN</ta>
            <ta e="T42" id="Seg_807" s="T41">верхушка-ILL</ta>
            <ta e="T43" id="Seg_808" s="T42">залезть-CVB</ta>
            <ta e="T44" id="Seg_809" s="T43">бегать-RFL-3SG.S</ta>
            <ta e="T45" id="Seg_810" s="T44">этот</ta>
            <ta e="T46" id="Seg_811" s="T45">сказка-ACC</ta>
            <ta e="T47" id="Seg_812" s="T46">раньше</ta>
            <ta e="T48" id="Seg_813" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_814" s="T48">сказать-CVB</ta>
            <ta e="T50" id="Seg_815" s="T49">принести-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T51" id="Seg_816" s="T50">свой.1SG</ta>
            <ta e="T52" id="Seg_817" s="T51">маленький</ta>
            <ta e="T53" id="Seg_818" s="T52">нечто-PL-1SG-ALL</ta>
            <ta e="T54" id="Seg_819" s="T53">он(а)-PL.[NOM]</ta>
            <ta e="T55" id="Seg_820" s="T54">сказка-ACC</ta>
            <ta e="T56" id="Seg_821" s="T55">любить-HAB-PST-3PL</ta>
            <ta e="T57" id="Seg_822" s="T56">я.ACC</ta>
            <ta e="T58" id="Seg_823" s="T57">плохой-ADVZ</ta>
            <ta e="T59" id="Seg_824" s="T58">NEG</ta>
            <ta e="T60" id="Seg_825" s="T59">%%-HAB-PST-3PL</ta>
            <ta e="T61" id="Seg_826" s="T60">а</ta>
            <ta e="T62" id="Seg_827" s="T61">я.NOM</ta>
            <ta e="T63" id="Seg_828" s="T62">он(а)-PL-ALL</ta>
            <ta e="T64" id="Seg_829" s="T63">все.время</ta>
            <ta e="T65" id="Seg_830" s="T64">этот</ta>
            <ta e="T66" id="Seg_831" s="T65">сказка-ACC</ta>
            <ta e="T67" id="Seg_832" s="T66">сказать-CVB</ta>
            <ta e="T68" id="Seg_833" s="T67">принести-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T69" id="Seg_834" s="T68">этот</ta>
            <ta e="T70" id="Seg_835" s="T69">сказка.[NOM]</ta>
            <ta e="T71" id="Seg_836" s="T70">ложь-DIM-ADJZ</ta>
            <ta e="T72" id="Seg_837" s="T71">я.NOM</ta>
            <ta e="T73" id="Seg_838" s="T72">он(а)-PL-ALL</ta>
            <ta e="T74" id="Seg_839" s="T73">ложь-ADVZ</ta>
            <ta e="T75" id="Seg_840" s="T74">сказать-CVB</ta>
            <ta e="T76" id="Seg_841" s="T75">принести-EP-FRQ-HAB-PST-1SG.O</ta>
            <ta e="T77" id="Seg_842" s="T76">а</ta>
            <ta e="T78" id="Seg_843" s="T77">маленький</ta>
            <ta e="T79" id="Seg_844" s="T78">нечто-PL.[NOM]</ta>
            <ta e="T80" id="Seg_845" s="T79">послушаться-DUR-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_846" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_847" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_848" s="T3">conj</ta>
            <ta e="T5" id="Seg_849" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_850" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_851" s="T6">n.[n:case]-n:poss</ta>
            <ta e="T8" id="Seg_852" s="T7">n.[n:case]-n:poss</ta>
            <ta e="T9" id="Seg_853" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_854" s="T9">adj</ta>
            <ta e="T11" id="Seg_855" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_856" s="T11">v-v:mood-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_857" s="T12">n.[n:case]</ta>
            <ta e="T14" id="Seg_858" s="T13">v-v:mood.pn</ta>
            <ta e="T15" id="Seg_859" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_860" s="T15">v-v:mood.pn</ta>
            <ta e="T17" id="Seg_861" s="T16">pers</ta>
            <ta e="T18" id="Seg_862" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_863" s="T18">conj</ta>
            <ta e="T20" id="Seg_864" s="T19">n-n:num-n:case</ta>
            <ta e="T21" id="Seg_865" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_866" s="T21">pers-n:case-n&gt;adj</ta>
            <ta e="T23" id="Seg_867" s="T22">v-v&gt;ptcp</ta>
            <ta e="T24" id="Seg_868" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_869" s="T24">num</ta>
            <ta e="T26" id="Seg_870" s="T25">pro</ta>
            <ta e="T27" id="Seg_871" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_872" s="T27">v-v:mood.pn</ta>
            <ta e="T29" id="Seg_873" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_874" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_875" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_876" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_877" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_878" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_879" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_880" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_881" s="T36">v-v&gt;adv</ta>
            <ta e="T38" id="Seg_882" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_883" s="T38">adv</ta>
            <ta e="T40" id="Seg_884" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_885" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_886" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_887" s="T42">v-v&gt;adv</ta>
            <ta e="T44" id="Seg_888" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T45" id="Seg_889" s="T44">dem</ta>
            <ta e="T46" id="Seg_890" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_891" s="T46">adv</ta>
            <ta e="T48" id="Seg_892" s="T47">pers</ta>
            <ta e="T49" id="Seg_893" s="T48">v-v&gt;adv</ta>
            <ta e="T50" id="Seg_894" s="T49">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_895" s="T50">emphpro</ta>
            <ta e="T52" id="Seg_896" s="T51">adj</ta>
            <ta e="T53" id="Seg_897" s="T52">n-n:num-n:poss-n:case</ta>
            <ta e="T54" id="Seg_898" s="T53">pers-n:num.[n:case]</ta>
            <ta e="T55" id="Seg_899" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_900" s="T55">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_901" s="T56">pers</ta>
            <ta e="T58" id="Seg_902" s="T57">adj-adj&gt;adv</ta>
            <ta e="T59" id="Seg_903" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_904" s="T59">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_905" s="T60">conj</ta>
            <ta e="T62" id="Seg_906" s="T61">pers</ta>
            <ta e="T63" id="Seg_907" s="T62">pers-n:num-n:case</ta>
            <ta e="T64" id="Seg_908" s="T63">adv</ta>
            <ta e="T65" id="Seg_909" s="T64">dem</ta>
            <ta e="T66" id="Seg_910" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_911" s="T66">v-v&gt;adv</ta>
            <ta e="T68" id="Seg_912" s="T67">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_913" s="T68">dem</ta>
            <ta e="T70" id="Seg_914" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_915" s="T70">n-n&gt;n-n&gt;adj</ta>
            <ta e="T72" id="Seg_916" s="T71">pers</ta>
            <ta e="T73" id="Seg_917" s="T72">pers-n:num-n:case</ta>
            <ta e="T74" id="Seg_918" s="T73">n-n&gt;adv</ta>
            <ta e="T75" id="Seg_919" s="T74">v-v&gt;adv</ta>
            <ta e="T76" id="Seg_920" s="T75">v-v:ins-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_921" s="T76">conj</ta>
            <ta e="T78" id="Seg_922" s="T77">adj</ta>
            <ta e="T79" id="Seg_923" s="T78">n-n:num.[n:case]</ta>
            <ta e="T80" id="Seg_924" s="T79">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_925" s="T1">n</ta>
            <ta e="T3" id="Seg_926" s="T2">v</ta>
            <ta e="T4" id="Seg_927" s="T3">conj</ta>
            <ta e="T5" id="Seg_928" s="T4">n</ta>
            <ta e="T6" id="Seg_929" s="T5">v</ta>
            <ta e="T7" id="Seg_930" s="T6">n</ta>
            <ta e="T8" id="Seg_931" s="T7">n</ta>
            <ta e="T9" id="Seg_932" s="T8">v</ta>
            <ta e="T10" id="Seg_933" s="T9">adj</ta>
            <ta e="T11" id="Seg_934" s="T10">n</ta>
            <ta e="T12" id="Seg_935" s="T11">v</ta>
            <ta e="T13" id="Seg_936" s="T12">n</ta>
            <ta e="T14" id="Seg_937" s="T13">v</ta>
            <ta e="T15" id="Seg_938" s="T14">n</ta>
            <ta e="T16" id="Seg_939" s="T15">v</ta>
            <ta e="T17" id="Seg_940" s="T16">pers</ta>
            <ta e="T18" id="Seg_941" s="T17">n</ta>
            <ta e="T19" id="Seg_942" s="T18">conj</ta>
            <ta e="T20" id="Seg_943" s="T19">n</ta>
            <ta e="T21" id="Seg_944" s="T20">n</ta>
            <ta e="T22" id="Seg_945" s="T21">adj</ta>
            <ta e="T23" id="Seg_946" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_947" s="T23">n</ta>
            <ta e="T25" id="Seg_948" s="T24">num</ta>
            <ta e="T27" id="Seg_949" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_950" s="T27">v</ta>
            <ta e="T29" id="Seg_951" s="T28">n</ta>
            <ta e="T30" id="Seg_952" s="T29">v</ta>
            <ta e="T31" id="Seg_953" s="T30">n</ta>
            <ta e="T32" id="Seg_954" s="T31">v</ta>
            <ta e="T33" id="Seg_955" s="T32">n</ta>
            <ta e="T34" id="Seg_956" s="T33">v</ta>
            <ta e="T35" id="Seg_957" s="T34">n</ta>
            <ta e="T36" id="Seg_958" s="T35">n</ta>
            <ta e="T37" id="Seg_959" s="T36">adv</ta>
            <ta e="T38" id="Seg_960" s="T37">v</ta>
            <ta e="T39" id="Seg_961" s="T38">adv</ta>
            <ta e="T40" id="Seg_962" s="T39">v</ta>
            <ta e="T41" id="Seg_963" s="T40">n</ta>
            <ta e="T42" id="Seg_964" s="T41">n</ta>
            <ta e="T43" id="Seg_965" s="T42">adv</ta>
            <ta e="T44" id="Seg_966" s="T43">v</ta>
            <ta e="T45" id="Seg_967" s="T44">dem</ta>
            <ta e="T46" id="Seg_968" s="T45">n</ta>
            <ta e="T47" id="Seg_969" s="T46">adv</ta>
            <ta e="T48" id="Seg_970" s="T47">pers</ta>
            <ta e="T49" id="Seg_971" s="T48">adv</ta>
            <ta e="T50" id="Seg_972" s="T49">v</ta>
            <ta e="T51" id="Seg_973" s="T50">emphpro</ta>
            <ta e="T52" id="Seg_974" s="T51">adj</ta>
            <ta e="T53" id="Seg_975" s="T52">n</ta>
            <ta e="T54" id="Seg_976" s="T53">pers</ta>
            <ta e="T55" id="Seg_977" s="T54">n</ta>
            <ta e="T56" id="Seg_978" s="T55">v</ta>
            <ta e="T57" id="Seg_979" s="T56">pers</ta>
            <ta e="T58" id="Seg_980" s="T57">adv</ta>
            <ta e="T59" id="Seg_981" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_982" s="T59">v</ta>
            <ta e="T61" id="Seg_983" s="T60">conj</ta>
            <ta e="T62" id="Seg_984" s="T61">pers</ta>
            <ta e="T63" id="Seg_985" s="T62">pers</ta>
            <ta e="T64" id="Seg_986" s="T63">adv</ta>
            <ta e="T65" id="Seg_987" s="T64">dem</ta>
            <ta e="T66" id="Seg_988" s="T65">n</ta>
            <ta e="T67" id="Seg_989" s="T66">adv</ta>
            <ta e="T68" id="Seg_990" s="T67">v</ta>
            <ta e="T69" id="Seg_991" s="T68">dem</ta>
            <ta e="T70" id="Seg_992" s="T69">n</ta>
            <ta e="T71" id="Seg_993" s="T70">adj</ta>
            <ta e="T72" id="Seg_994" s="T71">pers</ta>
            <ta e="T73" id="Seg_995" s="T72">pers</ta>
            <ta e="T74" id="Seg_996" s="T73">adv</ta>
            <ta e="T75" id="Seg_997" s="T74">adv</ta>
            <ta e="T76" id="Seg_998" s="T75">v</ta>
            <ta e="T77" id="Seg_999" s="T76">conj</ta>
            <ta e="T78" id="Seg_1000" s="T77">adj</ta>
            <ta e="T79" id="Seg_1001" s="T78">n</ta>
            <ta e="T80" id="Seg_1002" s="T79">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1003" s="T3">RUS:gram</ta>
            <ta e="T19" id="Seg_1004" s="T18">RUS:gram</ta>
            <ta e="T61" id="Seg_1005" s="T60">RUS:gram</ta>
            <ta e="T77" id="Seg_1006" s="T76">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1007" s="T1">It will rain and it will freeze.</ta>
            <ta e="T9" id="Seg_1008" s="T6">A mother will give birth to a son.</ta>
            <ta e="T12" id="Seg_1009" s="T9">He will be naughty.</ta>
            <ta e="T16" id="Seg_1010" s="T12">Rain, come, rain, come.</ta>
            <ta e="T28" id="Seg_1011" s="T16">[Rain] onto me and onto people, but (not a single drop)? onto my beloved one.</ta>
            <ta e="T32" id="Seg_1012" s="T28">It will rain, it will rain.</ta>
            <ta e="T38" id="Seg_1013" s="T32">The tale got wet, climbed onto a tree and dried for a while.</ta>
            <ta e="T44" id="Seg_1014" s="T38">It got wet again, climbed onto a tree, dried for a while.</ta>
            <ta e="T53" id="Seg_1015" s="T44">Earlier I used to tell this tale to my children.</ta>
            <ta e="T56" id="Seg_1016" s="T53">They loved fairy tales.</ta>
            <ta e="T60" id="Seg_1017" s="T56">They didn't disturb(?) me badly.</ta>
            <ta e="T68" id="Seg_1018" s="T60">And I was telling them this tale all the time.</ta>
            <ta e="T71" id="Seg_1019" s="T68">This tale is made up.</ta>
            <ta e="T76" id="Seg_1020" s="T71">I told it to them for fun.</ta>
            <ta e="T80" id="Seg_1021" s="T76">And the children were listening.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1022" s="T1">Es wird regnen und es wird Frost geben.</ta>
            <ta e="T9" id="Seg_1023" s="T6">Eine Mutter wird einen Sohn zur Welt bringen.</ta>
            <ta e="T12" id="Seg_1024" s="T9">Er wird ungezogen sein.</ta>
            <ta e="T16" id="Seg_1025" s="T12">Regen, komm, Regen, komm.</ta>
            <ta e="T28" id="Seg_1026" s="T16">[Regen] auf mich und auf die Leute, aber (kein einziger Tropfen)? auf meinen Geliebten.</ta>
            <ta e="T32" id="Seg_1027" s="T28">Es wird regnen, es wird regnen.</ta>
            <ta e="T38" id="Seg_1028" s="T32">Die Geschichte wurde nass, kletterte auf einen Baum und trocknete eine Weile.</ta>
            <ta e="T44" id="Seg_1029" s="T38">Sie wurde wieder nass, kletterte auf einen Baum, trocknete eine Weile.</ta>
            <ta e="T53" id="Seg_1030" s="T44">Früher habe ich diese Geschichte immer meinen Kindern erzählt.</ta>
            <ta e="T56" id="Seg_1031" s="T53">Sie haben Märchen geliebt.</ta>
            <ta e="T60" id="Seg_1032" s="T56">Sie haben mich nicht so sehr gestört(?).</ta>
            <ta e="T68" id="Seg_1033" s="T60">Und ich habe ihnen diese Geschichte immer erzählt.</ta>
            <ta e="T71" id="Seg_1034" s="T68">Diese Geschichte ist erfunden.</ta>
            <ta e="T76" id="Seg_1035" s="T71">Ich habe sie ihnen zum Spaß erzählt.</ta>
            <ta e="T80" id="Seg_1036" s="T76">Und die Kinder hörten zu.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1037" s="T1">Дождик будет и мороз будет.</ta>
            <ta e="T9" id="Seg_1038" s="T6">Мать сына принесет.</ta>
            <ta e="T12" id="Seg_1039" s="T9">Хулиган будет.</ta>
            <ta e="T16" id="Seg_1040" s="T12">Дождик лей, дождик лей.</ta>
            <ta e="T28" id="Seg_1041" s="T16">На меня и на людей, на моего милого (ни капли ни единой)?.</ta>
            <ta e="T32" id="Seg_1042" s="T28">Дождь будет, дождь будет.</ta>
            <ta e="T38" id="Seg_1043" s="T32">Сказка намокла, на лесочек залезла, посохла.</ta>
            <ta e="T44" id="Seg_1044" s="T38">Опять намокла, на лесочек залезла, посохла.</ta>
            <ta e="T53" id="Seg_1045" s="T44">Эту сказку я раньше рассказывала своим ребятишкам.</ta>
            <ta e="T56" id="Seg_1046" s="T53">Они сказки любили.</ta>
            <ta e="T60" id="Seg_1047" s="T56">Меня плохо не досаждали(?).</ta>
            <ta e="T68" id="Seg_1048" s="T60">А я им все время эту сказку рассказывала.</ta>
            <ta e="T71" id="Seg_1049" s="T68">Эта сказка придумана.</ta>
            <ta e="T76" id="Seg_1050" s="T71">Я им нарочно рассказывала.</ta>
            <ta e="T80" id="Seg_1051" s="T76">А ребятишки слушали.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_1052" s="T1">дождик будет и мороз будет</ta>
            <ta e="T9" id="Seg_1053" s="T6">мать сына принесет</ta>
            <ta e="T12" id="Seg_1054" s="T9">хулиган будет</ta>
            <ta e="T16" id="Seg_1055" s="T12">дождик лей дождик лей</ta>
            <ta e="T28" id="Seg_1056" s="T16">на меня и на людей на моего милого ни капли ни единого</ta>
            <ta e="T32" id="Seg_1057" s="T28">дождь будет</ta>
            <ta e="T38" id="Seg_1058" s="T32">сказка намокла на лесочек залезла посохла</ta>
            <ta e="T44" id="Seg_1059" s="T38">опять намокла на лесочек залезла посохла</ta>
            <ta e="T53" id="Seg_1060" s="T44">эту сказку я раньше рассказывала своим ребятишкам</ta>
            <ta e="T56" id="Seg_1061" s="T53">они сказки любили</ta>
            <ta e="T60" id="Seg_1062" s="T56">меня досаж(д?)али</ta>
            <ta e="T68" id="Seg_1063" s="T60">а я им все время эту сказку рассказывала (нарочно сочиняла)</ta>
            <ta e="T71" id="Seg_1064" s="T68">эта сказка придумана</ta>
            <ta e="T76" id="Seg_1065" s="T71">я им нарочно рассказывала</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_1066" s="T9">[BrM:] Tentative analysis of 'ennanǯ'.</ta>
            <ta e="T28" id="Seg_1067" s="T16">[KuAI:] Variant: 'oloɣeŋ'.</ta>
            <ta e="T60" id="Seg_1068" s="T56">[BrM:] Unclear translation, tentative analysis.</ta>
            <ta e="T71" id="Seg_1069" s="T68">[BrM:] Tentative analysis of 'sidʼäkaj'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
