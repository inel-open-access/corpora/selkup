<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_ObsceneSong_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_ObsceneSong_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">16</ud-information>
            <ud-information attribute-name="# HIAT:w">12</ud-information>
            <ud-information attribute-name="# e">12</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T13" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Pajagannan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">pet</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">čaːǯə</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">Äragannan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">mandə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">iːbəgaj</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Okkɨr</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">äragannan</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">qɨgajlat</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">iːbɨgaj</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Täbnan</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">krɨʒat</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T13" id="Seg_49" n="sc" s="T1">
               <ts e="T2" id="Seg_51" n="e" s="T1">Pajagannan </ts>
               <ts e="T3" id="Seg_53" n="e" s="T2">pet </ts>
               <ts e="T4" id="Seg_55" n="e" s="T3">čaːǯə. </ts>
               <ts e="T5" id="Seg_57" n="e" s="T4">Äragannan </ts>
               <ts e="T6" id="Seg_59" n="e" s="T5">mandə </ts>
               <ts e="T7" id="Seg_61" n="e" s="T6">iːbəgaj. </ts>
               <ts e="T8" id="Seg_63" n="e" s="T7">Okkɨr </ts>
               <ts e="T9" id="Seg_65" n="e" s="T8">äragannan </ts>
               <ts e="T10" id="Seg_67" n="e" s="T9">qɨgajlat </ts>
               <ts e="T11" id="Seg_69" n="e" s="T10">iːbɨgaj. </ts>
               <ts e="T12" id="Seg_71" n="e" s="T11">Täbnan </ts>
               <ts e="T13" id="Seg_73" n="e" s="T12">krɨʒat. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_74" s="T1">PVD_1964_ObsceneSong_song.001 (001.001)</ta>
            <ta e="T7" id="Seg_75" s="T4">PVD_1964_ObsceneSong_song.002 (001.002)</ta>
            <ta e="T11" id="Seg_76" s="T7">PVD_1964_ObsceneSong_song.003 (001.003)</ta>
            <ta e="T13" id="Seg_77" s="T11">PVD_1964_ObsceneSong_song.004 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_78" s="T1">′паjа‵ганнан пет ′тша̄джъ.</ta>
            <ta e="T7" id="Seg_79" s="T4">′ӓраганнан ′мандъ ′ӣбъгай.</ta>
            <ta e="T11" id="Seg_80" s="T7">ок′кыр ӓра′ганнан kы′гайлат ′ӣбыгай.</ta>
            <ta e="T13" id="Seg_81" s="T11">тӓб′нан ′крыжат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_82" s="T1">pajagannan pet tšaːǯə.</ta>
            <ta e="T7" id="Seg_83" s="T4">äragannan mandə iːbəgaj.</ta>
            <ta e="T11" id="Seg_84" s="T7">okkɨr äragannan qɨgajlat iːbɨgaj.</ta>
            <ta e="T13" id="Seg_85" s="T11">täbnan krɨʒat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_86" s="T1">Pajagannan pet čaːǯə. </ta>
            <ta e="T7" id="Seg_87" s="T4">Äragannan mandə iːbəgaj. </ta>
            <ta e="T11" id="Seg_88" s="T7">Okkɨr äragannan qɨgajlat iːbɨgaj. </ta>
            <ta e="T13" id="Seg_89" s="T11">Täbnan krɨʒat. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_90" s="T1">paja-ga-nnan</ta>
            <ta e="T3" id="Seg_91" s="T2">pet</ta>
            <ta e="T4" id="Seg_92" s="T3">čaːǯə</ta>
            <ta e="T5" id="Seg_93" s="T4">ära-ga-nnan</ta>
            <ta e="T6" id="Seg_94" s="T5">man-də</ta>
            <ta e="T7" id="Seg_95" s="T6">iːbəgaj</ta>
            <ta e="T8" id="Seg_96" s="T7">okkɨr</ta>
            <ta e="T9" id="Seg_97" s="T8">ära-ga-nnan</ta>
            <ta e="T10" id="Seg_98" s="T9">qɨgaj-la-t</ta>
            <ta e="T11" id="Seg_99" s="T10">iːbɨgaj</ta>
            <ta e="T12" id="Seg_100" s="T11">täb-nan</ta>
            <ta e="T13" id="Seg_101" s="T12">krɨʒa-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_102" s="T1">paja-ka-nan</ta>
            <ta e="T3" id="Seg_103" s="T2">ped</ta>
            <ta e="T4" id="Seg_104" s="T3">čaːǯə</ta>
            <ta e="T5" id="Seg_105" s="T4">era-ka-nan</ta>
            <ta e="T6" id="Seg_106" s="T5">man-tə</ta>
            <ta e="T7" id="Seg_107" s="T6">iːbəgaj</ta>
            <ta e="T8" id="Seg_108" s="T7">okkɨr</ta>
            <ta e="T9" id="Seg_109" s="T8">era-ka-nan</ta>
            <ta e="T10" id="Seg_110" s="T9">qɨgaj-la-tə</ta>
            <ta e="T11" id="Seg_111" s="T10">iːbəgaj</ta>
            <ta e="T12" id="Seg_112" s="T11">täp-nan</ta>
            <ta e="T13" id="Seg_113" s="T12">krɨʒa-tə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_114" s="T1">old.woman-DIM-ADES</ta>
            <ta e="T3" id="Seg_115" s="T2">cunt.[NOM]</ta>
            <ta e="T4" id="Seg_116" s="T3">%wide</ta>
            <ta e="T5" id="Seg_117" s="T4">old.man-DIM-ADES</ta>
            <ta e="T6" id="Seg_118" s="T5">penis.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_119" s="T6">big</ta>
            <ta e="T8" id="Seg_120" s="T7">one</ta>
            <ta e="T9" id="Seg_121" s="T8">old.man-DIM-ADES</ta>
            <ta e="T10" id="Seg_122" s="T9">testicle-PL.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_123" s="T10">big</ta>
            <ta e="T12" id="Seg_124" s="T11">(s)he-ADES</ta>
            <ta e="T13" id="Seg_125" s="T12">hernia.[NOM]-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_126" s="T1">старуха-DIM-ADES</ta>
            <ta e="T3" id="Seg_127" s="T2">влагалище.[NOM]</ta>
            <ta e="T4" id="Seg_128" s="T3">%широкий</ta>
            <ta e="T5" id="Seg_129" s="T4">старик-DIM-ADES</ta>
            <ta e="T6" id="Seg_130" s="T5">penis.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_131" s="T6">большой</ta>
            <ta e="T8" id="Seg_132" s="T7">один</ta>
            <ta e="T9" id="Seg_133" s="T8">старик-DIM-ADES</ta>
            <ta e="T10" id="Seg_134" s="T9">яичко-PL.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_135" s="T10">большой</ta>
            <ta e="T12" id="Seg_136" s="T11">он(а)-ADES</ta>
            <ta e="T13" id="Seg_137" s="T12">грыжа.[NOM]-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_138" s="T1">n-n&gt;n-n:case</ta>
            <ta e="T3" id="Seg_139" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_140" s="T3">adj</ta>
            <ta e="T5" id="Seg_141" s="T4">n-n&gt;n-n:case</ta>
            <ta e="T6" id="Seg_142" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_143" s="T6">adj</ta>
            <ta e="T8" id="Seg_144" s="T7">num</ta>
            <ta e="T9" id="Seg_145" s="T8">n-n&gt;n-n:case</ta>
            <ta e="T10" id="Seg_146" s="T9">n-n:num.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_147" s="T10">adj</ta>
            <ta e="T12" id="Seg_148" s="T11">pers-n:case</ta>
            <ta e="T13" id="Seg_149" s="T12">n.[n:case]-n:poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_150" s="T1">n</ta>
            <ta e="T3" id="Seg_151" s="T2">n</ta>
            <ta e="T4" id="Seg_152" s="T3">adj</ta>
            <ta e="T5" id="Seg_153" s="T4">n</ta>
            <ta e="T6" id="Seg_154" s="T5">n</ta>
            <ta e="T7" id="Seg_155" s="T6">adj</ta>
            <ta e="T8" id="Seg_156" s="T7">num</ta>
            <ta e="T9" id="Seg_157" s="T8">n</ta>
            <ta e="T10" id="Seg_158" s="T9">n</ta>
            <ta e="T11" id="Seg_159" s="T10">adj</ta>
            <ta e="T12" id="Seg_160" s="T11">pers</ta>
            <ta e="T13" id="Seg_161" s="T12">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_162" s="T1">np.h:Poss</ta>
            <ta e="T3" id="Seg_163" s="T2">np:Th</ta>
            <ta e="T5" id="Seg_164" s="T4">np.h:Poss</ta>
            <ta e="T6" id="Seg_165" s="T5">np:Th</ta>
            <ta e="T9" id="Seg_166" s="T8">np.h:Poss</ta>
            <ta e="T10" id="Seg_167" s="T9">np:Th</ta>
            <ta e="T12" id="Seg_168" s="T11">pro.h:Poss</ta>
            <ta e="T13" id="Seg_169" s="T12">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_170" s="T2">np:S</ta>
            <ta e="T6" id="Seg_171" s="T5">np:S</ta>
            <ta e="T10" id="Seg_172" s="T9">np:S</ta>
            <ta e="T13" id="Seg_173" s="T12">np:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_174" s="T12">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_175" s="T1">An old woman has got a wide(?) cunt.</ta>
            <ta e="T7" id="Seg_176" s="T4">An old man has got a big penis.</ta>
            <ta e="T11" id="Seg_177" s="T7">One old man has got big testicles.</ta>
            <ta e="T13" id="Seg_178" s="T11">He has a hernia.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_179" s="T1">Eine alte Frau hat eine große(?) Muschi.</ta>
            <ta e="T7" id="Seg_180" s="T4">Ein alter Mann hat einen großen Penis.</ta>
            <ta e="T11" id="Seg_181" s="T7">Ein alter Mann hat große Hoden.</ta>
            <ta e="T13" id="Seg_182" s="T11">Er hat einen Bruch (eine Hernie).</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_183" s="T1">У старухи влагалище широкое(?).</ta>
            <ta e="T7" id="Seg_184" s="T4">У старика пенис большой.</ta>
            <ta e="T11" id="Seg_185" s="T7">У одного старика яйца большие.</ta>
            <ta e="T13" id="Seg_186" s="T11">У него грыжа.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_187" s="T1">у старухи Täschchen широкая</ta>
            <ta e="T7" id="Seg_188" s="T4">у старика Hahn большой</ta>
            <ta e="T11" id="Seg_189" s="T7">у одного старика яйца большие</ta>
            <ta e="T13" id="Seg_190" s="T11">у него грыжа</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
