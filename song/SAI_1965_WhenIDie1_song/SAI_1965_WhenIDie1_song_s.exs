<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SAI_1965_WhenIDie_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SAI_1965_WhenIDie1_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">198</ud-information>
            <ud-information attribute-name="# HIAT:w">162</ud-information>
            <ud-information attribute-name="# e">162</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAI">
            <abbreviation>SAI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T527" id="Seg_0" n="sc" s="T365">
               <ts e="T375" id="Seg_2" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_4" n="HIAT:w" s="T365">Apat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_7" n="HIAT:w" s="T366">čʼɔːpst</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_10" n="HIAT:w" s="T367">okoːt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_13" n="HIAT:w" s="T368">nık</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_16" n="HIAT:w" s="T369">keː</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_19" n="HIAT:w" s="T370">tomkɨŋɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_22" n="HIAT:w" s="T371">ilɨptäːqɨntɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">:</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_26" n="HIAT:w" s="T372">Somak</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_29" n="HIAT:w" s="T373">ilɨŋɨlɨt</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_33" n="HIAT:w" s="T374">iːjat</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_37" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_39" n="HIAT:w" s="T375">Imap</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_42" n="HIAT:w" s="T376">qontɔːlɨt</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_46" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_48" n="HIAT:w" s="T377">Somak</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_51" n="HIAT:w" s="T378">ilɨŋɨlɨt</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_55" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_57" n="HIAT:w" s="T379">Nɨːnä</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_60" n="HIAT:w" s="T380">oqqa</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_63" n="HIAT:w" s="T381">nɔːkɨr</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_66" n="HIAT:w" s="T382">ämnätɨ</ts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_70" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_72" n="HIAT:w" s="T383">Šitɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_75" n="HIAT:w" s="T384">ijantɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_78" n="HIAT:w" s="T385">mɨqılʼ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_81" n="HIAT:w" s="T386">amnäiːmtɨ</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_85" n="HIAT:w" s="T387">muntɨk</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_88" n="HIAT:w" s="T388">qoši</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_92" n="HIAT:w" s="T389">qɨːltɨmpatɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_96" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_98" n="HIAT:w" s="T390">A</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_101" n="HIAT:w" s="T391">poːs</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_104" n="HIAT:w" s="T392">warqɨ</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_107" n="HIAT:w" s="T393">ämnämtɨ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_110" n="HIAT:w" s="T394">orsä</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_113" n="HIAT:w" s="T395">kɨkap</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_117" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_119" n="HIAT:w" s="T396">Keː</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_122" n="HIAT:w" s="T397">mɨta</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_125" n="HIAT:w" s="T398">wärqɨ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_128" n="HIAT:w" s="T399">ija</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_132" n="HIAT:w" s="T400">warqä</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_135" n="HIAT:w" s="T401">nälʼa</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_139" n="HIAT:w" s="T402">somak</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_142" n="HIAT:w" s="T403">ilʼiŋɨlɨt</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_146" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_148" n="HIAT:w" s="T404">Nɨːnɨ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_151" n="HIAT:w" s="T405">nıː</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_154" n="HIAT:w" s="T406">keː</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_157" n="HIAT:w" s="T407">suːmpɨka</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_161" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_163" n="HIAT:w" s="T408">Somak</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_166" n="HIAT:w" s="T409">mɨta</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_169" n="HIAT:w" s="T410">mollə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_172" n="HIAT:w" s="T411">seːlʼčʼi</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_175" n="HIAT:w" s="T412">čʼürɨp</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_178" n="HIAT:w" s="T413">mompa</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_181" n="HIAT:w" s="T414">orqɨlpɨŋɨmtɨjä</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_184" n="HIAT:w" s="T415">wärqɨ</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_187" n="HIAT:w" s="T416">nʼälʼa</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_190" n="HIAT:w" s="T417">mɨtta</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_194" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_196" n="HIAT:w" s="T418">Mannaj</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_199" n="HIAT:w" s="T419">moqɨlʼ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_202" n="HIAT:w" s="T420">čʼeːloːqak</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_205" n="HIAT:w" s="T421">somak</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_208" n="HIAT:w" s="T422">mɛntɨk</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_211" n="HIAT:w" s="T423">mompa</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_214" n="HIAT:w" s="T424">sä</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_217" n="HIAT:w" s="T425">kʼe</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_220" n="HIAT:w" s="T426">ilɨlɨlɨt</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_224" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_226" n="HIAT:w" s="T427">Mannaj</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_229" n="HIAT:w" s="T428">moqoːqäŋ</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_232" n="HIAT:w" s="T429">aj</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_235" n="HIAT:w" s="T430">tap</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_238" n="HIAT:w" s="T431">kʼeː</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_240" n="HIAT:ip">(</nts>
                  <ts e="T433" id="Seg_242" n="HIAT:w" s="T432">qaj</ts>
                  <nts id="Seg_243" n="HIAT:ip">)</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_246" n="HIAT:w" s="T433">mompa</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_249" n="HIAT:w" s="T434">sä</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_252" n="HIAT:w" s="T435">kʼe</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_255" n="HIAT:w" s="T436">orqɨllɨlɨt</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_258" n="HIAT:w" s="T437">man</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_261" n="HIAT:w" s="T438">mɨmpa</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_265" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_267" n="HIAT:w" s="T439">Man</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_270" n="HIAT:w" s="T440">aj</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_273" n="HIAT:w" s="T441">quptäːlʼ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_276" n="HIAT:w" s="T442">moqoːqak</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_279" n="HIAT:w" s="T443">quptäːlʼ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_282" n="HIAT:w" s="T444">moqoːqäŋ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_285" n="HIAT:w" s="T445">aj</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_288" n="HIAT:w" s="T446">wärqɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_291" n="HIAT:w" s="T447">iːja</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_294" n="HIAT:w" s="T448">mompa</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_297" n="HIAT:w" s="T449">mašıp</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_300" n="HIAT:w" s="T450">aj</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_303" n="HIAT:w" s="T451">na</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_306" n="HIAT:w" s="T452">tɛnäptɛnta</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_310" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_312" n="HIAT:w" s="T453">Üt</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_315" n="HIAT:w" s="T454">kočʼik</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_318" n="HIAT:w" s="T455">ɨkɨ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_321" n="HIAT:w" s="T456">ütɨmpätɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_325" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_327" n="HIAT:w" s="T457">Čʼontɨkak</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_330" n="HIAT:w" s="T458">sä</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_333" n="HIAT:w" s="T459">mompa</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_336" n="HIAT:w" s="T460">ütɨrkuläntɨ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_339" n="HIAT:w" s="T461">naj</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_342" n="HIAT:w" s="T462">amɨrkulʼantɨ</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_345" n="HIAT:w" s="T463">naj</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_349" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_351" n="HIAT:w" s="T464">Tɛː</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_354" n="HIAT:w" s="T465">ütɨmpɔːtɨt</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_358" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_360" n="HIAT:w" s="T466">Man</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_363" n="HIAT:w" s="T467">aj</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_366" n="HIAT:w" s="T468">šıntɨ</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_369" n="HIAT:w" s="T469">kəːtɨptäːqäk</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_372" n="HIAT:w" s="T470">ütɨp</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_375" n="HIAT:w" s="T471">aj</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_378" n="HIAT:w" s="T472">amkap</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_382" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_384" n="HIAT:w" s="T473">Šentɨ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_387" n="HIAT:w" s="T474">rušit</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_390" n="HIAT:w" s="T475">nılʼčʼik</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_393" n="HIAT:w" s="T476">tomkɔːtɨt</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_396" n="HIAT:w" s="T477">mompa</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_400" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_402" n="HIAT:w" s="T478">Čʼontɨka</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_405" n="HIAT:w" s="T479">ütɨräš</ts>
                  <nts id="Seg_406" n="HIAT:ip">,</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_409" n="HIAT:w" s="T480">nılʼčʼik</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_412" n="HIAT:w" s="T481">sä</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_415" n="HIAT:w" s="T482">qä</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_418" n="HIAT:w" s="T483">ilɨläntɨ</ts>
                  <nts id="Seg_419" n="HIAT:ip">,</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_422" n="HIAT:w" s="T484">kočʼi</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_425" n="HIAT:w" s="T485">ɨkɨ</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_428" n="HIAT:w" s="T486">ütɨmpäš</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_432" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_434" n="HIAT:w" s="T487">Man</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_437" n="HIAT:w" s="T488">aj</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_440" n="HIAT:w" s="T489">quptäːlʼ</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_443" n="HIAT:w" s="T490">moqoːqak</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_446" n="HIAT:w" s="T491">latarɨlʼ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_449" n="HIAT:w" s="T492">apamtɨ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_452" n="HIAT:w" s="T493">sa</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_455" n="HIAT:w" s="T494">kkʼe</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_458" n="HIAT:w" s="T495">tɛnaptukɨläl</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_461" n="HIAT:w" s="T496">kutɨlʼ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_464" n="HIAT:w" s="T497">ɛːma</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_467" n="HIAT:w" s="T498">čʼeːlɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_471" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_473" n="HIAT:w" s="T499">A</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_476" n="HIAT:w" s="T500">wärqə</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_479" n="HIAT:w" s="T501">nälʼamɨ</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_482" n="HIAT:w" s="T502">naj</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_485" n="HIAT:w" s="T503">ja</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_488" n="HIAT:w" s="T504">qaip</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_491" n="HIAT:w" s="T505">kətɛntɨtɨ</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_494" n="HIAT:w" s="T506">sä</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_497" n="HIAT:w" s="T507">kʼen</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_500" n="HIAT:w" s="T508">üŋkɨltukɨläl</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_504" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_506" n="HIAT:w" s="T509">Nɔːtɨ</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_509" n="HIAT:w" s="T510">niːmto</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_512" n="HIAT:w" s="T511">qaširelʼa</ts>
                  <nts id="Seg_513" n="HIAT:ip">:</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_516" n="HIAT:w" s="T512">Kətsaniːmɨ</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_519" n="HIAT:w" s="T513">nʼe</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_522" n="HIAT:w" s="T514">obižajte</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_526" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_528" n="HIAT:w" s="T515">Ukkur</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_531" n="HIAT:w" s="T516">qumtɨlʼ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_534" n="HIAT:w" s="T517">kətsanmɨ</ts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_538" n="HIAT:w" s="T518">ukkur</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_541" n="HIAT:w" s="T519">nätalʼ</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_544" n="HIAT:w" s="T520">kətsanmɨ</ts>
                  <nts id="Seg_545" n="HIAT:ip">,</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_548" n="HIAT:w" s="T521">kočʼi</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_551" n="HIAT:w" s="T522">kətsanmɨ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_554" n="HIAT:w" s="T523">čʼäːŋka</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_558" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_560" n="HIAT:w" s="T524">Onak</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_563" n="HIAT:w" s="T525">qollä</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_566" n="HIAT:w" s="T526">taːtɨsam</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T527" id="Seg_569" n="sc" s="T365">
               <ts e="T366" id="Seg_571" n="e" s="T365">Apat </ts>
               <ts e="T367" id="Seg_573" n="e" s="T366">čʼɔːpst </ts>
               <ts e="T368" id="Seg_575" n="e" s="T367">okoːt </ts>
               <ts e="T369" id="Seg_577" n="e" s="T368">nık </ts>
               <ts e="T370" id="Seg_579" n="e" s="T369">keː </ts>
               <ts e="T371" id="Seg_581" n="e" s="T370">tomkɨŋɨt </ts>
               <ts e="T372" id="Seg_583" n="e" s="T371">ilɨptäːqɨntɨ: </ts>
               <ts e="T373" id="Seg_585" n="e" s="T372">Somak </ts>
               <ts e="T374" id="Seg_587" n="e" s="T373">ilɨŋɨlɨt, </ts>
               <ts e="T375" id="Seg_589" n="e" s="T374">iːjat. </ts>
               <ts e="T376" id="Seg_591" n="e" s="T375">Imap </ts>
               <ts e="T377" id="Seg_593" n="e" s="T376">qontɔːlɨt. </ts>
               <ts e="T378" id="Seg_595" n="e" s="T377">Somak </ts>
               <ts e="T379" id="Seg_597" n="e" s="T378">ilɨŋɨlɨt. </ts>
               <ts e="T380" id="Seg_599" n="e" s="T379">Nɨːnä </ts>
               <ts e="T381" id="Seg_601" n="e" s="T380">oqqa </ts>
               <ts e="T382" id="Seg_603" n="e" s="T381">nɔːkɨr </ts>
               <ts e="T383" id="Seg_605" n="e" s="T382">ämnätɨ. </ts>
               <ts e="T384" id="Seg_607" n="e" s="T383">Šitɨ </ts>
               <ts e="T385" id="Seg_609" n="e" s="T384">ijantɨ </ts>
               <ts e="T386" id="Seg_611" n="e" s="T385">mɨqılʼ </ts>
               <ts e="T387" id="Seg_613" n="e" s="T386">amnäiːmtɨ, </ts>
               <ts e="T388" id="Seg_615" n="e" s="T387">muntɨk </ts>
               <ts e="T389" id="Seg_617" n="e" s="T388">qoši, </ts>
               <ts e="T390" id="Seg_619" n="e" s="T389">qɨːltɨmpatɨ. </ts>
               <ts e="T391" id="Seg_621" n="e" s="T390">A </ts>
               <ts e="T392" id="Seg_623" n="e" s="T391">poːs </ts>
               <ts e="T393" id="Seg_625" n="e" s="T392">warqɨ </ts>
               <ts e="T394" id="Seg_627" n="e" s="T393">ämnämtɨ </ts>
               <ts e="T395" id="Seg_629" n="e" s="T394">orsä </ts>
               <ts e="T396" id="Seg_631" n="e" s="T395">kɨkap. </ts>
               <ts e="T397" id="Seg_633" n="e" s="T396">Keː </ts>
               <ts e="T398" id="Seg_635" n="e" s="T397">mɨta </ts>
               <ts e="T399" id="Seg_637" n="e" s="T398">wärqɨ </ts>
               <ts e="T400" id="Seg_639" n="e" s="T399">ija, </ts>
               <ts e="T401" id="Seg_641" n="e" s="T400">warqä </ts>
               <ts e="T402" id="Seg_643" n="e" s="T401">nälʼa, </ts>
               <ts e="T403" id="Seg_645" n="e" s="T402">somak </ts>
               <ts e="T404" id="Seg_647" n="e" s="T403">ilʼiŋɨlɨt. </ts>
               <ts e="T405" id="Seg_649" n="e" s="T404">Nɨːnɨ </ts>
               <ts e="T406" id="Seg_651" n="e" s="T405">nıː </ts>
               <ts e="T407" id="Seg_653" n="e" s="T406">keː </ts>
               <ts e="T408" id="Seg_655" n="e" s="T407">suːmpɨka. </ts>
               <ts e="T409" id="Seg_657" n="e" s="T408">Somak </ts>
               <ts e="T410" id="Seg_659" n="e" s="T409">mɨta </ts>
               <ts e="T411" id="Seg_661" n="e" s="T410">mollə </ts>
               <ts e="T412" id="Seg_663" n="e" s="T411">seːlʼčʼi </ts>
               <ts e="T413" id="Seg_665" n="e" s="T412">čʼürɨp </ts>
               <ts e="T414" id="Seg_667" n="e" s="T413">mompa </ts>
               <ts e="T415" id="Seg_669" n="e" s="T414">orqɨlpɨŋɨmtɨjä </ts>
               <ts e="T416" id="Seg_671" n="e" s="T415">wärqɨ </ts>
               <ts e="T417" id="Seg_673" n="e" s="T416">nʼälʼa </ts>
               <ts e="T418" id="Seg_675" n="e" s="T417">mɨtta. </ts>
               <ts e="T419" id="Seg_677" n="e" s="T418">Mannaj </ts>
               <ts e="T420" id="Seg_679" n="e" s="T419">moqɨlʼ </ts>
               <ts e="T421" id="Seg_681" n="e" s="T420">čʼeːloːqak </ts>
               <ts e="T422" id="Seg_683" n="e" s="T421">somak </ts>
               <ts e="T423" id="Seg_685" n="e" s="T422">mɛntɨk </ts>
               <ts e="T424" id="Seg_687" n="e" s="T423">mompa </ts>
               <ts e="T425" id="Seg_689" n="e" s="T424">sä </ts>
               <ts e="T426" id="Seg_691" n="e" s="T425">kʼe </ts>
               <ts e="T427" id="Seg_693" n="e" s="T426">ilɨlɨlɨt. </ts>
               <ts e="T428" id="Seg_695" n="e" s="T427">Mannaj </ts>
               <ts e="T429" id="Seg_697" n="e" s="T428">moqoːqäŋ </ts>
               <ts e="T430" id="Seg_699" n="e" s="T429">aj </ts>
               <ts e="T431" id="Seg_701" n="e" s="T430">tap </ts>
               <ts e="T432" id="Seg_703" n="e" s="T431">kʼeː </ts>
               <ts e="T433" id="Seg_705" n="e" s="T432">(qaj) </ts>
               <ts e="T434" id="Seg_707" n="e" s="T433">mompa </ts>
               <ts e="T435" id="Seg_709" n="e" s="T434">sä </ts>
               <ts e="T436" id="Seg_711" n="e" s="T435">kʼe </ts>
               <ts e="T437" id="Seg_713" n="e" s="T436">orqɨllɨlɨt </ts>
               <ts e="T438" id="Seg_715" n="e" s="T437">man </ts>
               <ts e="T439" id="Seg_717" n="e" s="T438">mɨmpa. </ts>
               <ts e="T440" id="Seg_719" n="e" s="T439">Man </ts>
               <ts e="T441" id="Seg_721" n="e" s="T440">aj </ts>
               <ts e="T442" id="Seg_723" n="e" s="T441">quptäːlʼ </ts>
               <ts e="T443" id="Seg_725" n="e" s="T442">moqoːqak </ts>
               <ts e="T444" id="Seg_727" n="e" s="T443">quptäːlʼ </ts>
               <ts e="T445" id="Seg_729" n="e" s="T444">moqoːqäŋ </ts>
               <ts e="T446" id="Seg_731" n="e" s="T445">aj </ts>
               <ts e="T447" id="Seg_733" n="e" s="T446">wärqɨ </ts>
               <ts e="T448" id="Seg_735" n="e" s="T447">iːja </ts>
               <ts e="T449" id="Seg_737" n="e" s="T448">mompa </ts>
               <ts e="T450" id="Seg_739" n="e" s="T449">mašıp </ts>
               <ts e="T451" id="Seg_741" n="e" s="T450">aj </ts>
               <ts e="T452" id="Seg_743" n="e" s="T451">na </ts>
               <ts e="T453" id="Seg_745" n="e" s="T452">tɛnäptɛnta. </ts>
               <ts e="T454" id="Seg_747" n="e" s="T453">Üt </ts>
               <ts e="T455" id="Seg_749" n="e" s="T454">kočʼik </ts>
               <ts e="T456" id="Seg_751" n="e" s="T455">ɨkɨ </ts>
               <ts e="T457" id="Seg_753" n="e" s="T456">ütɨmpätɨ. </ts>
               <ts e="T458" id="Seg_755" n="e" s="T457">Čʼontɨkak </ts>
               <ts e="T459" id="Seg_757" n="e" s="T458">sä </ts>
               <ts e="T460" id="Seg_759" n="e" s="T459">mompa </ts>
               <ts e="T461" id="Seg_761" n="e" s="T460">ütɨrkuläntɨ </ts>
               <ts e="T462" id="Seg_763" n="e" s="T461">naj </ts>
               <ts e="T463" id="Seg_765" n="e" s="T462">amɨrkulʼantɨ </ts>
               <ts e="T464" id="Seg_767" n="e" s="T463">naj. </ts>
               <ts e="T465" id="Seg_769" n="e" s="T464">Tɛː </ts>
               <ts e="T466" id="Seg_771" n="e" s="T465">ütɨmpɔːtɨt. </ts>
               <ts e="T467" id="Seg_773" n="e" s="T466">Man </ts>
               <ts e="T468" id="Seg_775" n="e" s="T467">aj </ts>
               <ts e="T469" id="Seg_777" n="e" s="T468">šıntɨ </ts>
               <ts e="T470" id="Seg_779" n="e" s="T469">kəːtɨptäːqäk </ts>
               <ts e="T471" id="Seg_781" n="e" s="T470">ütɨp </ts>
               <ts e="T472" id="Seg_783" n="e" s="T471">aj </ts>
               <ts e="T473" id="Seg_785" n="e" s="T472">amkap. </ts>
               <ts e="T474" id="Seg_787" n="e" s="T473">Šentɨ </ts>
               <ts e="T475" id="Seg_789" n="e" s="T474">rušit </ts>
               <ts e="T476" id="Seg_791" n="e" s="T475">nılʼčʼik </ts>
               <ts e="T477" id="Seg_793" n="e" s="T476">tomkɔːtɨt </ts>
               <ts e="T478" id="Seg_795" n="e" s="T477">mompa. </ts>
               <ts e="T479" id="Seg_797" n="e" s="T478">Čʼontɨka </ts>
               <ts e="T480" id="Seg_799" n="e" s="T479">ütɨräš, </ts>
               <ts e="T481" id="Seg_801" n="e" s="T480">nılʼčʼik </ts>
               <ts e="T482" id="Seg_803" n="e" s="T481">sä </ts>
               <ts e="T483" id="Seg_805" n="e" s="T482">qä </ts>
               <ts e="T484" id="Seg_807" n="e" s="T483">ilɨläntɨ, </ts>
               <ts e="T485" id="Seg_809" n="e" s="T484">kočʼi </ts>
               <ts e="T486" id="Seg_811" n="e" s="T485">ɨkɨ </ts>
               <ts e="T487" id="Seg_813" n="e" s="T486">ütɨmpäš. </ts>
               <ts e="T488" id="Seg_815" n="e" s="T487">Man </ts>
               <ts e="T489" id="Seg_817" n="e" s="T488">aj </ts>
               <ts e="T490" id="Seg_819" n="e" s="T489">quptäːlʼ </ts>
               <ts e="T491" id="Seg_821" n="e" s="T490">moqoːqak </ts>
               <ts e="T492" id="Seg_823" n="e" s="T491">latarɨlʼ </ts>
               <ts e="T493" id="Seg_825" n="e" s="T492">apamtɨ </ts>
               <ts e="T494" id="Seg_827" n="e" s="T493">sa </ts>
               <ts e="T495" id="Seg_829" n="e" s="T494">kkʼe </ts>
               <ts e="T496" id="Seg_831" n="e" s="T495">tɛnaptukɨläl </ts>
               <ts e="T497" id="Seg_833" n="e" s="T496">kutɨlʼ </ts>
               <ts e="T498" id="Seg_835" n="e" s="T497">ɛːma </ts>
               <ts e="T499" id="Seg_837" n="e" s="T498">čʼeːlɨ. </ts>
               <ts e="T500" id="Seg_839" n="e" s="T499">A </ts>
               <ts e="T501" id="Seg_841" n="e" s="T500">wärqə </ts>
               <ts e="T502" id="Seg_843" n="e" s="T501">nälʼamɨ </ts>
               <ts e="T503" id="Seg_845" n="e" s="T502">naj </ts>
               <ts e="T504" id="Seg_847" n="e" s="T503">ja </ts>
               <ts e="T505" id="Seg_849" n="e" s="T504">qaip </ts>
               <ts e="T506" id="Seg_851" n="e" s="T505">kətɛntɨtɨ </ts>
               <ts e="T507" id="Seg_853" n="e" s="T506">sä </ts>
               <ts e="T508" id="Seg_855" n="e" s="T507">kʼen </ts>
               <ts e="T509" id="Seg_857" n="e" s="T508">üŋkɨltukɨläl. </ts>
               <ts e="T510" id="Seg_859" n="e" s="T509">Nɔːtɨ </ts>
               <ts e="T511" id="Seg_861" n="e" s="T510">niːmto </ts>
               <ts e="T512" id="Seg_863" n="e" s="T511">qaširelʼa: </ts>
               <ts e="T513" id="Seg_865" n="e" s="T512">Kətsaniːmɨ </ts>
               <ts e="T514" id="Seg_867" n="e" s="T513">nʼe </ts>
               <ts e="T515" id="Seg_869" n="e" s="T514">obižajte. </ts>
               <ts e="T516" id="Seg_871" n="e" s="T515">Ukkur </ts>
               <ts e="T517" id="Seg_873" n="e" s="T516">qumtɨlʼ </ts>
               <ts e="T518" id="Seg_875" n="e" s="T517">kətsanmɨ, </ts>
               <ts e="T519" id="Seg_877" n="e" s="T518">ukkur </ts>
               <ts e="T520" id="Seg_879" n="e" s="T519">nätalʼ </ts>
               <ts e="T521" id="Seg_881" n="e" s="T520">kətsanmɨ, </ts>
               <ts e="T522" id="Seg_883" n="e" s="T521">kočʼi </ts>
               <ts e="T523" id="Seg_885" n="e" s="T522">kətsanmɨ </ts>
               <ts e="T524" id="Seg_887" n="e" s="T523">čʼäːŋka. </ts>
               <ts e="T525" id="Seg_889" n="e" s="T524">Onak </ts>
               <ts e="T526" id="Seg_891" n="e" s="T525">qollä </ts>
               <ts e="T527" id="Seg_893" n="e" s="T526">taːtɨsam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T375" id="Seg_894" s="T365">SAI_1965_WhenIDie1_song.001 (001.001)</ta>
            <ta e="T377" id="Seg_895" s="T375">SAI_1965_WhenIDie1_song.002 (001.002)</ta>
            <ta e="T379" id="Seg_896" s="T377">SAI_1965_WhenIDie1_song.003 (001.003)</ta>
            <ta e="T383" id="Seg_897" s="T379">SAI_1965_WhenIDie1_song.004 (001.004)</ta>
            <ta e="T390" id="Seg_898" s="T383">SAI_1965_WhenIDie1_song.005 (001.005)</ta>
            <ta e="T396" id="Seg_899" s="T390">SAI_1965_WhenIDie1_song.006 (001.006)</ta>
            <ta e="T404" id="Seg_900" s="T396">SAI_1965_WhenIDie1_song.007 (001.007)</ta>
            <ta e="T408" id="Seg_901" s="T404">SAI_1965_WhenIDie1_song.008 (001.008)</ta>
            <ta e="T418" id="Seg_902" s="T408">SAI_1965_WhenIDie1_song.009 (001.009)</ta>
            <ta e="T427" id="Seg_903" s="T418">SAI_1965_WhenIDie1_song.010 (001.010)</ta>
            <ta e="T439" id="Seg_904" s="T427">SAI_1965_WhenIDie1_song.011 (001.011)</ta>
            <ta e="T453" id="Seg_905" s="T439">SAI_1965_WhenIDie1_song.012 (001.012)</ta>
            <ta e="T457" id="Seg_906" s="T453">SAI_1965_WhenIDie1_song.013 (001.013)</ta>
            <ta e="T464" id="Seg_907" s="T457">SAI_1965_WhenIDie1_song.014 (001.014)</ta>
            <ta e="T466" id="Seg_908" s="T464">SAI_1965_WhenIDie1_song.015 (001.015)</ta>
            <ta e="T473" id="Seg_909" s="T466">SAI_1965_WhenIDie1_song.016 (001.016)</ta>
            <ta e="T478" id="Seg_910" s="T473">SAI_1965_WhenIDie1_song.017 (001.017)</ta>
            <ta e="T487" id="Seg_911" s="T478">SAI_1965_WhenIDie1_song.018 (001.018)</ta>
            <ta e="T499" id="Seg_912" s="T487">SAI_1965_WhenIDie1_song.019 (001.019)</ta>
            <ta e="T509" id="Seg_913" s="T499">SAI_1965_WhenIDie1_song.020 (001.020)</ta>
            <ta e="T515" id="Seg_914" s="T509">SAI_1965_WhenIDie1_song.021 (001.021)</ta>
            <ta e="T524" id="Seg_915" s="T515">SAI_1965_WhenIDie1_song.022 (001.022)</ta>
            <ta e="T527" id="Seg_916" s="T524">SAI_1965_WhenIDie1_song.023 (001.023)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T375" id="Seg_917" s="T365">апат ′чопст о′кот нӣк ке̄ томkыңыт илʼи[ы]птӓ̄ɣынты. ′сомак ′илыңылыт ӣ′jат.</ta>
            <ta e="T377" id="Seg_918" s="T375">и′мап kон′тоlыт.</ta>
            <ta e="T379" id="Seg_919" s="T377">′сомак ′илыңылыт.</ta>
            <ta e="T383" id="Seg_920" s="T379">нынӓ окка ′но̨kыр ӓм′нӓты.</ta>
            <ta e="T390" id="Seg_921" s="T383">шʼиты и′jантымыkылʼ ′амнеимты, мунтык ′коши kыlтым′паты.</ta>
            <ta e="T396" id="Seg_922" s="T390">а по̄с ′варkы ӓ̄м′нӓмты орсӓ кы′кап.</ta>
            <ta e="T404" id="Seg_923" s="T396">ке̄ ′мыта вӓрɣы ′иjа, варkӓ ′нӓлʼа со′мак ′ӣлʼи[е]ңылыт.</ta>
            <ta e="T408" id="Seg_924" s="T404">ныны нӣке̄ ′сумпы‵ка.</ta>
            <ta e="T418" id="Seg_925" s="T408">′сомак ′мыта моll′ъ̊′ъ̊ъ̊ ′селʼчи тʼӱрып мом′па̄ ‵орkыл′пы̄ңымтыjе ′вӓрkы ′нʼӓлʼа мы′тта.</ta>
            <ta e="T427" id="Seg_926" s="T418">маннай ′моkылʼ ′че̄ло‵kак ′ сомак ′ме̨нтык ′момпааа ′се̨[ӓ]кʼе ′илылы‵лыт.</ta>
            <ta e="T439" id="Seg_927" s="T427">ман ай ′моkо ‵kӓң′ай тап кʼе̄ [kай] ′момпа ′сӓкʼкʼе ′орkылылыт ′манмымпа.</ta>
            <ta e="T453" id="Seg_928" s="T439">ман ай ′kуптеl ‵моkо′как ′kуптеl ′моkо ′kӓңай вӓрkы ӣ′jа ′момпа ′машип айна тӓ′нӓп ′тента.</ta>
            <ta e="T457" id="Seg_929" s="T453">ӱт кочик ′ыкы ‵ӱтым′пӓты.</ta>
            <ta e="T464" id="Seg_930" s="T457">чонтыкаксӓ̄ момпа ′ӱтыр ′кӯлʼӓн тыннай а′мырку‵лʼантынай.</ta>
            <ta e="T466" id="Seg_931" s="T464">тӓ ӱ̄тымпотыт.</ta>
            <ta e="T473" id="Seg_932" s="T466">ман ай шʼинты kъ̊тып ′тӓ̄kӓк ′ӱтып ай ′ам′кап[м].</ta>
            <ta e="T478" id="Seg_933" s="T473">′шʼенты рӯшит ′ниlчик ′томkотыт ′момпа.</ta>
            <ta e="T487" id="Seg_934" s="T478">′чонтыка ӱ̄ты′реш, ниlчик сӓkӓ ‵илы′лӓнты кочи ′ыкы ӱ̄тым′пе̨ш.</ta>
            <ta e="T499" id="Seg_935" s="T487">ман ай ′kуптеl ‵моkо′как латарылʼ ′апамты ′саккʼе тӓ′нап ‵тукы′lеl ′кутыl е̄ма ′че̄лы.</ta>
            <ta e="T509" id="Seg_936" s="T499">а вӓрkъ нӓлʼа‵мы найjа kаип kъ̊′тӓнтыты сӓкʼен ‵ӱңкылʼ‵тукы′lеl.</ta>
            <ta e="T515" id="Seg_937" s="T509">ноты нӣмто ′kаширелʼа: ′къ̊тса ′нӣмы нʼе обижайте.</ta>
            <ta e="T524" id="Seg_938" s="T515">уккур ′кумтыl ′къ̊тсанмы уккур нӓтаl къ̊тсанмы кочи kъ̊тсанмы ′че̄ңка.</ta>
            <ta e="T527" id="Seg_939" s="T524">′онак колʼлʼе ′татысам.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T375" id="Seg_940" s="T365">apat čopst okot niːk keː tomqɨŋɨt ilʼi[ɨ]ptäːqɨntɨ. somak ilɨŋɨlɨt iːjat.</ta>
            <ta e="T377" id="Seg_941" s="T375">imap qontolʼɨt.</ta>
            <ta e="T379" id="Seg_942" s="T377">somak ilɨŋɨlɨt.</ta>
            <ta e="T383" id="Seg_943" s="T379">nɨnä okka noqɨr ämnätɨ.</ta>
            <ta e="T390" id="Seg_944" s="T383">šitɨ ijantɨmɨqɨlʼ amneimtɨ, muntɨk koši qɨlʼtɨmpatɨ.</ta>
            <ta e="T396" id="Seg_945" s="T390">a poːs varqɨ äːmnämtɨ orsä kɨkap.</ta>
            <ta e="T404" id="Seg_946" s="T396">keː mɨta värqɨ ija, varqä nälʼa somak iːlʼi[e]ŋɨlɨt.</ta>
            <ta e="T408" id="Seg_947" s="T404">nɨnɨ niːkeː sumpɨka.</ta>
            <ta e="T418" id="Seg_948" s="T408">somak mɨta molʼlʼəəə selʼči čürɨp mompaː orqɨlpɨːŋɨmtɨje värqɨ nʼälʼa mɨtta.</ta>
            <ta e="T427" id="Seg_949" s="T418">mannaj moqɨlʼ čeːloqak somak mentɨk mompaaa se[ä]kʼe ilɨlɨlɨt.</ta>
            <ta e="T439" id="Seg_950" s="T427">man aj moqo qäŋaj tap kʼeː [qaj] mompa säkʼkʼe orqɨlɨlɨt manmɨmpa.</ta>
            <ta e="T453" id="Seg_951" s="T439">man aj quptelʼ moqokak quptelʼ moqo qäŋaj värqɨ iːja mompa mašip ajna tänäp tenta.</ta>
            <ta e="T457" id="Seg_952" s="T453">üt kočik ɨkɨ ütɨmpätɨ.</ta>
            <ta e="T464" id="Seg_953" s="T457">čontɨkaksäː mompa ütɨr kuːlʼän tɨnnaj amɨrkulʼantɨnaj.</ta>
            <ta e="T466" id="Seg_954" s="T464">tä üːtɨmpotɨt.</ta>
            <ta e="T473" id="Seg_955" s="T466">man aj šintɨ qətɨp täːqäk ütɨp aj amkap[m].</ta>
            <ta e="T478" id="Seg_956" s="T473">šentɨ ruːšit nilʼčik tomqotɨt mompa.</ta>
            <ta e="T487" id="Seg_957" s="T478">čontɨka üːtɨreš, nilʼčik säqä ilɨläntɨ koči ɨkɨ üːtɨmpeš.</ta>
            <ta e="T499" id="Seg_958" s="T487">man aj quptelʼ moqokak latarɨlʼ apamtɨ sakkʼe tänap tukɨlʼelʼ kutɨlʼ eːma čeːlɨ.</ta>
            <ta e="T509" id="Seg_959" s="T499">a värqə nälʼamɨ najja qaip qətäntɨtɨ säkʼen üŋkɨlʼtukɨlʼelʼ.</ta>
            <ta e="T515" id="Seg_960" s="T509">notɨ niːmto qaširelʼa: kətsa niːmɨ nʼe opižajte.</ta>
            <ta e="T524" id="Seg_961" s="T515">ukkur kumtɨlʼ kətsanmɨ ukkur nätalʼ kətsanmɨ koči qətsanmɨ čeːŋka.</ta>
            <ta e="T527" id="Seg_962" s="T524">onak kolʼlʼe tatɨsam.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T375" id="Seg_963" s="T365">Apat čʼɔːpst okoːt nık keː tomkɨŋɨt ilɨptäːqɨntɨ: Somak ilɨŋɨlɨt, iːjat. </ta>
            <ta e="T377" id="Seg_964" s="T375">Imap qontɔːlɨt. </ta>
            <ta e="T379" id="Seg_965" s="T377">Somak ilɨŋɨlɨt. </ta>
            <ta e="T383" id="Seg_966" s="T379">Nɨːnä oqqa nɔːkɨr ämnätɨ. </ta>
            <ta e="T390" id="Seg_967" s="T383">Šitɨ ijantɨ mɨqılʼ amnäiːmtɨ, muntɨk qoši, qɨːltɨmpatɨ. </ta>
            <ta e="T396" id="Seg_968" s="T390">A poːs warqɨ ämnämtɨ orsä kɨkap. </ta>
            <ta e="T404" id="Seg_969" s="T396">Keː mɨta wärqɨ ija, warqä nälʼa, somak ilʼiŋɨlɨt. </ta>
            <ta e="T408" id="Seg_970" s="T404">Nɨːnɨ nıː keː suːmpɨka. </ta>
            <ta e="T418" id="Seg_971" s="T408">Somak mɨta mollə seːlʼčʼi čʼürɨp mompa orqɨlpɨŋɨmtɨjä wärqɨ nʼälʼa mɨtta. </ta>
            <ta e="T427" id="Seg_972" s="T418">Mannaj moqɨlʼ čʼeːloːqak somak mɛntɨk mompa sä kʼe ilɨlɨlɨt. </ta>
            <ta e="T439" id="Seg_973" s="T427">Mannaj moqoːqäŋ aj tap kʼeː (qaj) mompa sä kʼe orqɨllɨlɨt man mɨmpa. </ta>
            <ta e="T453" id="Seg_974" s="T439">Man aj quptäːlʼ moqoːqak quptäːlʼ moqoːqäŋ aj wärqɨ iːja mompa mašıp aj na tɛnäptɛnta. </ta>
            <ta e="T457" id="Seg_975" s="T453">Üt kočʼik ɨkɨ ütɨmpätɨ. </ta>
            <ta e="T464" id="Seg_976" s="T457">Čʼontɨkak sä mompa ütɨrkuläntɨ naj amɨrkulʼantɨ naj. </ta>
            <ta e="T466" id="Seg_977" s="T464">Tɛː ütɨmpɔːtɨt. </ta>
            <ta e="T473" id="Seg_978" s="T466">Man aj šıntɨ kəːtɨptäːqäk ütɨp aj amkap. </ta>
            <ta e="T478" id="Seg_979" s="T473">Šentɨ rušit nılʼčʼik tomkɔːtɨt mompa. </ta>
            <ta e="T487" id="Seg_980" s="T478">Čʼontɨka ütɨräš, nılʼčʼik sä qä ilɨläntɨ, kočʼi ɨkɨ ütɨmpäš. </ta>
            <ta e="T499" id="Seg_981" s="T487">Man aj quptäːlʼ moqoːqak latarɨlʼ apamtɨ sa kkʼe tɛnaptukɨläl kutɨlʼ ɛːma čʼeːlɨ. </ta>
            <ta e="T509" id="Seg_982" s="T499">A wärqə nälʼamɨ naj ja qaip kətɛntɨtɨ sä kʼen üŋkɨltukɨläl. </ta>
            <ta e="T515" id="Seg_983" s="T509">Nɔːtɨ niːmto qaširelʼa: Nɔːtɨ niːmto qaširelʼa. </ta>
            <ta e="T524" id="Seg_984" s="T515">Ukkur qumtɨlʼ kətsanmɨ, ukkur nätalʼ kətsanmɨ, kočʼi kətsanmɨ čʼäːŋka. </ta>
            <ta e="T527" id="Seg_985" s="T524">Onak qollä taːtɨsam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T366" id="Seg_986" s="T365">apa-t</ta>
            <ta e="T367" id="Seg_987" s="T366">čʼɔːps-t</ta>
            <ta e="T368" id="Seg_988" s="T367">okoːt</ta>
            <ta e="T369" id="Seg_989" s="T368">nık</ta>
            <ta e="T370" id="Seg_990" s="T369">keː</ta>
            <ta e="T371" id="Seg_991" s="T370">tom-kɨ-ŋɨ-t</ta>
            <ta e="T372" id="Seg_992" s="T371">ilɨ-ptäː-qɨn-tɨ</ta>
            <ta e="T373" id="Seg_993" s="T372">soma-k</ta>
            <ta e="T374" id="Seg_994" s="T373">ilɨ-ŋɨlɨt</ta>
            <ta e="T375" id="Seg_995" s="T374">iːja-t</ta>
            <ta e="T376" id="Seg_996" s="T375">ima-p</ta>
            <ta e="T377" id="Seg_997" s="T376">qo-ntɔː-lɨt</ta>
            <ta e="T378" id="Seg_998" s="T377">soma-k</ta>
            <ta e="T379" id="Seg_999" s="T378">ilɨ-ŋɨlɨt</ta>
            <ta e="T380" id="Seg_1000" s="T379">nɨːnä</ta>
            <ta e="T381" id="Seg_1001" s="T380">oqqa</ta>
            <ta e="T382" id="Seg_1002" s="T381">nɔːkɨr</ta>
            <ta e="T383" id="Seg_1003" s="T382">ämnä-tɨ</ta>
            <ta e="T384" id="Seg_1004" s="T383">šitɨ</ta>
            <ta e="T385" id="Seg_1005" s="T384">ija-n-tɨ</ta>
            <ta e="T386" id="Seg_1006" s="T385">mɨ-qı-lʼ</ta>
            <ta e="T387" id="Seg_1007" s="T386">amnä-iː-m-tɨ</ta>
            <ta e="T388" id="Seg_1008" s="T387">muntɨk</ta>
            <ta e="T389" id="Seg_1009" s="T388">qoši</ta>
            <ta e="T390" id="Seg_1010" s="T389">qɨː-ltɨ-mpa-tɨ</ta>
            <ta e="T391" id="Seg_1011" s="T390">a</ta>
            <ta e="T392" id="Seg_1012" s="T391">poːs</ta>
            <ta e="T393" id="Seg_1013" s="T392">warqɨ</ta>
            <ta e="T394" id="Seg_1014" s="T393">ämnä-m-tɨ</ta>
            <ta e="T395" id="Seg_1015" s="T394">or-sä</ta>
            <ta e="T396" id="Seg_1016" s="T395">kɨka-p</ta>
            <ta e="T397" id="Seg_1017" s="T396">keː</ta>
            <ta e="T398" id="Seg_1018" s="T397">mɨta</ta>
            <ta e="T399" id="Seg_1019" s="T398">wärqɨ</ta>
            <ta e="T400" id="Seg_1020" s="T399">ija</ta>
            <ta e="T401" id="Seg_1021" s="T400">warqä</ta>
            <ta e="T402" id="Seg_1022" s="T401">nälʼa</ta>
            <ta e="T403" id="Seg_1023" s="T402">soma-k</ta>
            <ta e="T404" id="Seg_1024" s="T403">ilʼi-ŋɨlɨt</ta>
            <ta e="T405" id="Seg_1025" s="T404">nɨːnɨ</ta>
            <ta e="T406" id="Seg_1026" s="T405">nıː</ta>
            <ta e="T407" id="Seg_1027" s="T406">keː</ta>
            <ta e="T408" id="Seg_1028" s="T407">suːmpɨ-ka</ta>
            <ta e="T409" id="Seg_1029" s="T408">soma-k</ta>
            <ta e="T410" id="Seg_1030" s="T409">mɨta</ta>
            <ta e="T411" id="Seg_1031" s="T410">mollə</ta>
            <ta e="T412" id="Seg_1032" s="T411">seːlʼčʼi</ta>
            <ta e="T413" id="Seg_1033" s="T412">čʼürɨ-p</ta>
            <ta e="T414" id="Seg_1034" s="T413">mompa</ta>
            <ta e="T415" id="Seg_1035" s="T414">orqɨl-pɨ-ŋɨmtɨjä</ta>
            <ta e="T416" id="Seg_1036" s="T415">wärqɨ</ta>
            <ta e="T417" id="Seg_1037" s="T416">nʼälʼa</ta>
            <ta e="T418" id="Seg_1038" s="T417">mɨtta</ta>
            <ta e="T419" id="Seg_1039" s="T418">man-naj</ta>
            <ta e="T420" id="Seg_1040" s="T419">moqɨ-lʼ</ta>
            <ta e="T421" id="Seg_1041" s="T420">čʼeːloː-qak</ta>
            <ta e="T422" id="Seg_1042" s="T421">soma-k</ta>
            <ta e="T423" id="Seg_1043" s="T422">mɛntɨk</ta>
            <ta e="T424" id="Seg_1044" s="T423">mompa</ta>
            <ta e="T425" id="Seg_1045" s="T424">sä</ta>
            <ta e="T426" id="Seg_1046" s="T425">kʼe</ta>
            <ta e="T427" id="Seg_1047" s="T426">ilɨ-lɨ-lɨt</ta>
            <ta e="T428" id="Seg_1048" s="T427">man-naj</ta>
            <ta e="T429" id="Seg_1049" s="T428">moqoː-qäŋ</ta>
            <ta e="T430" id="Seg_1050" s="T429">aj</ta>
            <ta e="T431" id="Seg_1051" s="T430">tap</ta>
            <ta e="T432" id="Seg_1052" s="T431">kʼeː</ta>
            <ta e="T433" id="Seg_1053" s="T432">qaj</ta>
            <ta e="T434" id="Seg_1054" s="T433">mompa</ta>
            <ta e="T435" id="Seg_1055" s="T434">sä</ta>
            <ta e="T436" id="Seg_1056" s="T435">kʼe</ta>
            <ta e="T437" id="Seg_1057" s="T436">orqɨl-lɨ-lɨt</ta>
            <ta e="T438" id="Seg_1058" s="T437">man</ta>
            <ta e="T439" id="Seg_1059" s="T438">mɨmpa</ta>
            <ta e="T440" id="Seg_1060" s="T439">man</ta>
            <ta e="T441" id="Seg_1061" s="T440">aj</ta>
            <ta e="T442" id="Seg_1062" s="T441">qu-ptäː-lʼ</ta>
            <ta e="T443" id="Seg_1063" s="T442">moqoː-qak</ta>
            <ta e="T444" id="Seg_1064" s="T443">qu-ptäː-lʼ</ta>
            <ta e="T445" id="Seg_1065" s="T444">moqoː-qäŋ</ta>
            <ta e="T446" id="Seg_1066" s="T445">aj</ta>
            <ta e="T447" id="Seg_1067" s="T446">wärqɨ</ta>
            <ta e="T448" id="Seg_1068" s="T447">iːja</ta>
            <ta e="T449" id="Seg_1069" s="T448">mompa</ta>
            <ta e="T450" id="Seg_1070" s="T449">mašıp</ta>
            <ta e="T451" id="Seg_1071" s="T450">aj</ta>
            <ta e="T452" id="Seg_1072" s="T451">na</ta>
            <ta e="T453" id="Seg_1073" s="T452">tɛnä-pt-ɛnta</ta>
            <ta e="T454" id="Seg_1074" s="T453">üt</ta>
            <ta e="T455" id="Seg_1075" s="T454">kočʼi-k</ta>
            <ta e="T456" id="Seg_1076" s="T455">ɨkɨ</ta>
            <ta e="T457" id="Seg_1077" s="T456">ütɨ-mp-ätɨ</ta>
            <ta e="T458" id="Seg_1078" s="T457">čʼontɨ-ka-k</ta>
            <ta e="T459" id="Seg_1079" s="T458">sä</ta>
            <ta e="T460" id="Seg_1080" s="T459">mompa</ta>
            <ta e="T461" id="Seg_1081" s="T460">ütɨ-r-ku-lä-ntɨ</ta>
            <ta e="T462" id="Seg_1082" s="T461">naj</ta>
            <ta e="T463" id="Seg_1083" s="T462">am-ɨ-r-ku-lʼa-ntɨ</ta>
            <ta e="T464" id="Seg_1084" s="T463">naj</ta>
            <ta e="T465" id="Seg_1085" s="T464">tɛː</ta>
            <ta e="T466" id="Seg_1086" s="T465">ütɨ-mpɔː-tɨt</ta>
            <ta e="T467" id="Seg_1087" s="T466">man</ta>
            <ta e="T468" id="Seg_1088" s="T467">aj</ta>
            <ta e="T469" id="Seg_1089" s="T468">šıntɨ</ta>
            <ta e="T470" id="Seg_1090" s="T469">kəːtɨ-ptäː-qäk</ta>
            <ta e="T471" id="Seg_1091" s="T470">üt-ɨ-p</ta>
            <ta e="T472" id="Seg_1092" s="T471">aj</ta>
            <ta e="T473" id="Seg_1093" s="T472">am-ka-p</ta>
            <ta e="T474" id="Seg_1094" s="T473">šentɨ</ta>
            <ta e="T475" id="Seg_1095" s="T474">ruš-i-t</ta>
            <ta e="T476" id="Seg_1096" s="T475">nılʼčʼi-k</ta>
            <ta e="T477" id="Seg_1097" s="T476">tom-kɔː-tɨt</ta>
            <ta e="T478" id="Seg_1098" s="T477">mompa</ta>
            <ta e="T479" id="Seg_1099" s="T478">čʼontɨ-ka</ta>
            <ta e="T480" id="Seg_1100" s="T479">ütɨ-r-äš</ta>
            <ta e="T481" id="Seg_1101" s="T480">nılʼčʼi-k</ta>
            <ta e="T482" id="Seg_1102" s="T481">sä</ta>
            <ta e="T483" id="Seg_1103" s="T482">qä</ta>
            <ta e="T484" id="Seg_1104" s="T483">ilɨ-lä-ntɨ</ta>
            <ta e="T485" id="Seg_1105" s="T484">kočʼi</ta>
            <ta e="T486" id="Seg_1106" s="T485">ɨkɨ</ta>
            <ta e="T487" id="Seg_1107" s="T486">ütɨ-mp-äš</ta>
            <ta e="T488" id="Seg_1108" s="T487">man</ta>
            <ta e="T489" id="Seg_1109" s="T488">aj</ta>
            <ta e="T490" id="Seg_1110" s="T489">qu-ptäː-lʼ</ta>
            <ta e="T491" id="Seg_1111" s="T490">moqoː-qak</ta>
            <ta e="T492" id="Seg_1112" s="T491">latar-ɨ-lʼ</ta>
            <ta e="T493" id="Seg_1113" s="T492">apa-m-tɨ</ta>
            <ta e="T494" id="Seg_1114" s="T493">sa</ta>
            <ta e="T495" id="Seg_1115" s="T494">kkʼe</ta>
            <ta e="T496" id="Seg_1116" s="T495">tɛna-ptu-kɨ-lä-l</ta>
            <ta e="T497" id="Seg_1117" s="T496">kutɨlʼ</ta>
            <ta e="T498" id="Seg_1118" s="T497">ɛːma</ta>
            <ta e="T499" id="Seg_1119" s="T498">čʼeːlɨ</ta>
            <ta e="T500" id="Seg_1120" s="T499">a</ta>
            <ta e="T501" id="Seg_1121" s="T500">wärqə</ta>
            <ta e="T502" id="Seg_1122" s="T501">nälʼa-mɨ</ta>
            <ta e="T503" id="Seg_1123" s="T502">naj</ta>
            <ta e="T504" id="Seg_1124" s="T503">ja</ta>
            <ta e="T505" id="Seg_1125" s="T504">qa-i-p</ta>
            <ta e="T506" id="Seg_1126" s="T505">kət-ɛntɨ-tɨ</ta>
            <ta e="T507" id="Seg_1127" s="T506">sä</ta>
            <ta e="T508" id="Seg_1128" s="T507">kʼen</ta>
            <ta e="T509" id="Seg_1129" s="T508">üŋkɨl-tu-kɨ-lä-l</ta>
            <ta e="T510" id="Seg_1130" s="T509">nɔːtɨ</ta>
            <ta e="T511" id="Seg_1131" s="T510">niːmto</ta>
            <ta e="T512" id="Seg_1132" s="T511">qaši-r-e-lʼa</ta>
            <ta e="T513" id="Seg_1133" s="T512">kətsan-iː-mɨ</ta>
            <ta e="T514" id="Seg_1134" s="T513">nʼe</ta>
            <ta e="T515" id="Seg_1135" s="T514">obižajte</ta>
            <ta e="T516" id="Seg_1136" s="T515">ukkur</ta>
            <ta e="T517" id="Seg_1137" s="T516">qum-t-ɨ-lʼ</ta>
            <ta e="T518" id="Seg_1138" s="T517">kətsan-mɨ</ta>
            <ta e="T519" id="Seg_1139" s="T518">ukkur</ta>
            <ta e="T520" id="Seg_1140" s="T519">näta-lʼ</ta>
            <ta e="T521" id="Seg_1141" s="T520">kətsan-mɨ</ta>
            <ta e="T522" id="Seg_1142" s="T521">kočʼi</ta>
            <ta e="T523" id="Seg_1143" s="T522">kətsan-mɨ</ta>
            <ta e="T524" id="Seg_1144" s="T523">čʼäːŋka</ta>
            <ta e="T525" id="Seg_1145" s="T524">onak</ta>
            <ta e="T526" id="Seg_1146" s="T525">qol-lä</ta>
            <ta e="T527" id="Seg_1147" s="T526">taːtɨ-sa-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T366" id="Seg_1148" s="T365">apa-tɨ</ta>
            <ta e="T367" id="Seg_1149" s="T366">čʼɔːpsɨ-tɨ</ta>
            <ta e="T368" id="Seg_1150" s="T367">ukoːn</ta>
            <ta e="T369" id="Seg_1151" s="T368">nık</ta>
            <ta e="T370" id="Seg_1152" s="T369">keː</ta>
            <ta e="T371" id="Seg_1153" s="T370">tom-kkɨ-ŋɨ-tɨ</ta>
            <ta e="T372" id="Seg_1154" s="T371">ilɨ-ptäː-qɨn-ntɨ</ta>
            <ta e="T373" id="Seg_1155" s="T372">soma-k</ta>
            <ta e="T374" id="Seg_1156" s="T373">ilɨ-ŋɨlɨt</ta>
            <ta e="T375" id="Seg_1157" s="T374">iːja-t</ta>
            <ta e="T376" id="Seg_1158" s="T375">ima-m</ta>
            <ta e="T377" id="Seg_1159" s="T376">qo-ɛntɨ-lɨt</ta>
            <ta e="T378" id="Seg_1160" s="T377">soma-k</ta>
            <ta e="T379" id="Seg_1161" s="T378">ilɨ-ŋɨlɨt</ta>
            <ta e="T380" id="Seg_1162" s="T379">nɨːnɨ</ta>
            <ta e="T381" id="Seg_1163" s="T380">oqqä</ta>
            <ta e="T382" id="Seg_1164" s="T381">nɔːkɨr</ta>
            <ta e="T383" id="Seg_1165" s="T382">ämnä-tɨ</ta>
            <ta e="T384" id="Seg_1166" s="T383">šittɨ</ta>
            <ta e="T385" id="Seg_1167" s="T384">iːja-n-tɨ</ta>
            <ta e="T386" id="Seg_1168" s="T385">mɨ-qı-lʼ</ta>
            <ta e="T387" id="Seg_1169" s="T386">ämnä-iː-m-tɨ</ta>
            <ta e="T388" id="Seg_1170" s="T387">muntɨk</ta>
            <ta e="T389" id="Seg_1171" s="T388">*qoš</ta>
            <ta e="T390" id="Seg_1172" s="T389">qɨː-ltɨ-mpɨ-tɨ</ta>
            <ta e="T391" id="Seg_1173" s="T390">a</ta>
            <ta e="T392" id="Seg_1174" s="T391">poːsɨ</ta>
            <ta e="T393" id="Seg_1175" s="T392">wərqɨ</ta>
            <ta e="T394" id="Seg_1176" s="T393">ämnä-m-tɨ</ta>
            <ta e="T395" id="Seg_1177" s="T394">orɨ-sä</ta>
            <ta e="T396" id="Seg_1178" s="T395">kɨkɨ-m</ta>
            <ta e="T397" id="Seg_1179" s="T396">keː</ta>
            <ta e="T398" id="Seg_1180" s="T397">mɨta</ta>
            <ta e="T399" id="Seg_1181" s="T398">wərqɨ</ta>
            <ta e="T400" id="Seg_1182" s="T399">iːja</ta>
            <ta e="T401" id="Seg_1183" s="T400">wərqɨ</ta>
            <ta e="T402" id="Seg_1184" s="T401">nälʼa</ta>
            <ta e="T403" id="Seg_1185" s="T402">soma-k</ta>
            <ta e="T404" id="Seg_1186" s="T403">ilɨ-ŋɨlɨt</ta>
            <ta e="T405" id="Seg_1187" s="T404">nɨːnɨ</ta>
            <ta e="T406" id="Seg_1188" s="T405">nık</ta>
            <ta e="T407" id="Seg_1189" s="T406">keː</ta>
            <ta e="T408" id="Seg_1190" s="T407">suːmpɨ-kkɨ</ta>
            <ta e="T409" id="Seg_1191" s="T408">soma-k</ta>
            <ta e="T410" id="Seg_1192" s="T409">mɨta</ta>
            <ta e="T411" id="Seg_1193" s="T410">mol</ta>
            <ta e="T412" id="Seg_1194" s="T411">seːlʼčʼɨ</ta>
            <ta e="T413" id="Seg_1195" s="T412">türɨ-m</ta>
            <ta e="T414" id="Seg_1196" s="T413">mompa</ta>
            <ta e="T415" id="Seg_1197" s="T414">orqɨl-mpɨ-ŋɨmtɨjä</ta>
            <ta e="T416" id="Seg_1198" s="T415">wərqɨ</ta>
            <ta e="T417" id="Seg_1199" s="T416">nälʼa</ta>
            <ta e="T418" id="Seg_1200" s="T417">mɨta</ta>
            <ta e="T419" id="Seg_1201" s="T418">man-naj</ta>
            <ta e="T420" id="Seg_1202" s="T419">moqɨ-lʼ</ta>
            <ta e="T421" id="Seg_1203" s="T420">čʼeːlɨ-qäk</ta>
            <ta e="T422" id="Seg_1204" s="T421">soma-k</ta>
            <ta e="T423" id="Seg_1205" s="T422">muntɨk</ta>
            <ta e="T424" id="Seg_1206" s="T423">mompa</ta>
            <ta e="T425" id="Seg_1207" s="T424">sä</ta>
            <ta e="T426" id="Seg_1208" s="T425">keː</ta>
            <ta e="T427" id="Seg_1209" s="T426">ilɨ-lä-lɨt</ta>
            <ta e="T428" id="Seg_1210" s="T427">man-naj</ta>
            <ta e="T429" id="Seg_1211" s="T428">moqɨ-qäk</ta>
            <ta e="T430" id="Seg_1212" s="T429">aj</ta>
            <ta e="T431" id="Seg_1213" s="T430">tam</ta>
            <ta e="T432" id="Seg_1214" s="T431">keː</ta>
            <ta e="T433" id="Seg_1215" s="T432">qaj</ta>
            <ta e="T434" id="Seg_1216" s="T433">mompa</ta>
            <ta e="T435" id="Seg_1217" s="T434">sä</ta>
            <ta e="T436" id="Seg_1218" s="T435">keː</ta>
            <ta e="T437" id="Seg_1219" s="T436">orqɨl-lä-lɨt</ta>
            <ta e="T438" id="Seg_1220" s="T437">man</ta>
            <ta e="T439" id="Seg_1221" s="T438">mompa</ta>
            <ta e="T440" id="Seg_1222" s="T439">man</ta>
            <ta e="T441" id="Seg_1223" s="T440">aj</ta>
            <ta e="T442" id="Seg_1224" s="T441">qu-ptäː-lʼ</ta>
            <ta e="T443" id="Seg_1225" s="T442">moqɨ-qäk</ta>
            <ta e="T444" id="Seg_1226" s="T443">qu-ptäː-lʼ</ta>
            <ta e="T445" id="Seg_1227" s="T444">moqɨ-qäk</ta>
            <ta e="T446" id="Seg_1228" s="T445">aj</ta>
            <ta e="T447" id="Seg_1229" s="T446">wərqɨ</ta>
            <ta e="T448" id="Seg_1230" s="T447">iːja</ta>
            <ta e="T449" id="Seg_1231" s="T448">mompa</ta>
            <ta e="T450" id="Seg_1232" s="T449">mašım</ta>
            <ta e="T451" id="Seg_1233" s="T450">aj</ta>
            <ta e="T452" id="Seg_1234" s="T451">na</ta>
            <ta e="T453" id="Seg_1235" s="T452">tɛnɨ-ptɨ-ɛntɨ</ta>
            <ta e="T454" id="Seg_1236" s="T453">üt</ta>
            <ta e="T455" id="Seg_1237" s="T454">kočʼčʼɨ-k</ta>
            <ta e="T456" id="Seg_1238" s="T455">ɨkɨ</ta>
            <ta e="T457" id="Seg_1239" s="T456">ütɨ-mpɨ-ätɨ</ta>
            <ta e="T458" id="Seg_1240" s="T457">čʼontɨ-ka-k</ta>
            <ta e="T459" id="Seg_1241" s="T458">sä</ta>
            <ta e="T460" id="Seg_1242" s="T459">mompa</ta>
            <ta e="T461" id="Seg_1243" s="T460">ütɨ-r-kkɨ-lä-ntɨ</ta>
            <ta e="T462" id="Seg_1244" s="T461">naj</ta>
            <ta e="T463" id="Seg_1245" s="T462">am-ɨ-r-kkɨ-lä-ntɨ</ta>
            <ta e="T464" id="Seg_1246" s="T463">naj</ta>
            <ta e="T465" id="Seg_1247" s="T464">tɛː</ta>
            <ta e="T466" id="Seg_1248" s="T465">ütɨ-mpɨ-tɨt</ta>
            <ta e="T467" id="Seg_1249" s="T466">man</ta>
            <ta e="T468" id="Seg_1250" s="T467">aj</ta>
            <ta e="T469" id="Seg_1251" s="T468">tašıntɨ</ta>
            <ta e="T470" id="Seg_1252" s="T469">kəːtɨ-ptäː-qäk</ta>
            <ta e="T471" id="Seg_1253" s="T470">üt-ɨ-m</ta>
            <ta e="T472" id="Seg_1254" s="T471">aj</ta>
            <ta e="T473" id="Seg_1255" s="T472">am-kkɨ-m</ta>
            <ta e="T474" id="Seg_1256" s="T473">šentɨ</ta>
            <ta e="T475" id="Seg_1257" s="T474">ruš-ɨ-t</ta>
            <ta e="T476" id="Seg_1258" s="T475">nılʼčʼɨ-k</ta>
            <ta e="T477" id="Seg_1259" s="T476">tom-kkɨ-tɨt</ta>
            <ta e="T478" id="Seg_1260" s="T477">mompa</ta>
            <ta e="T479" id="Seg_1261" s="T478">čʼontɨ-ka</ta>
            <ta e="T480" id="Seg_1262" s="T479">ütɨ-r-äšɨk</ta>
            <ta e="T481" id="Seg_1263" s="T480">nılʼčʼɨ-k</ta>
            <ta e="T482" id="Seg_1264" s="T481">sä</ta>
            <ta e="T483" id="Seg_1265" s="T482">keː</ta>
            <ta e="T484" id="Seg_1266" s="T483">ilɨ-lä-ntɨ</ta>
            <ta e="T485" id="Seg_1267" s="T484">kočʼčʼɨ</ta>
            <ta e="T486" id="Seg_1268" s="T485">ɨkɨ</ta>
            <ta e="T487" id="Seg_1269" s="T486">ütɨ-mpɨ-äšɨk</ta>
            <ta e="T488" id="Seg_1270" s="T487">man</ta>
            <ta e="T489" id="Seg_1271" s="T488">aj</ta>
            <ta e="T490" id="Seg_1272" s="T489">qu-ptäː-lʼ</ta>
            <ta e="T491" id="Seg_1273" s="T490">moqɨ-qäk</ta>
            <ta e="T492" id="Seg_1274" s="T491">lattar-ɨ-lʼ</ta>
            <ta e="T493" id="Seg_1275" s="T492">apa-m-ntɨ</ta>
            <ta e="T494" id="Seg_1276" s="T493">sä</ta>
            <ta e="T495" id="Seg_1277" s="T494">keː</ta>
            <ta e="T496" id="Seg_1278" s="T495">tɛnɨ-ptɨ-kkɨ-lä-l</ta>
            <ta e="T497" id="Seg_1279" s="T496">kutɨlʼ</ta>
            <ta e="T498" id="Seg_1280" s="T497">ɛːmä</ta>
            <ta e="T499" id="Seg_1281" s="T498">čʼeːlɨ</ta>
            <ta e="T500" id="Seg_1282" s="T499">a</ta>
            <ta e="T501" id="Seg_1283" s="T500">wərqɨ</ta>
            <ta e="T502" id="Seg_1284" s="T501">nälʼa-mɨ</ta>
            <ta e="T503" id="Seg_1285" s="T502">naj</ta>
            <ta e="T504" id="Seg_1286" s="T503">ja</ta>
            <ta e="T505" id="Seg_1287" s="T504">qaj-ɨ-m</ta>
            <ta e="T506" id="Seg_1288" s="T505">kətɨ-ɛntɨ-tɨ</ta>
            <ta e="T507" id="Seg_1289" s="T506">sä</ta>
            <ta e="T508" id="Seg_1290" s="T507">keː</ta>
            <ta e="T509" id="Seg_1291" s="T508">üŋkɨl-tɨ-kkɨ-lä-l</ta>
            <ta e="T510" id="Seg_1292" s="T509">nɔːtɨ</ta>
            <ta e="T511" id="Seg_1293" s="T510">nɨmtɨ</ta>
            <ta e="T512" id="Seg_1294" s="T511">qašɨ-r-ɨ-lä</ta>
            <ta e="T513" id="Seg_1295" s="T512">kətsan-iː-mɨ</ta>
            <ta e="T514" id="Seg_1296" s="T513">nʼi</ta>
            <ta e="T515" id="Seg_1297" s="T514">obižajte</ta>
            <ta e="T516" id="Seg_1298" s="T515">ukkɨr</ta>
            <ta e="T517" id="Seg_1299" s="T516">qum-t-ɨ-lʼ</ta>
            <ta e="T518" id="Seg_1300" s="T517">kətsan-mɨ</ta>
            <ta e="T519" id="Seg_1301" s="T518">ukkɨr</ta>
            <ta e="T520" id="Seg_1302" s="T519">nätäk-lʼ</ta>
            <ta e="T521" id="Seg_1303" s="T520">kətsan-mɨ</ta>
            <ta e="T522" id="Seg_1304" s="T521">kočʼčʼɨ</ta>
            <ta e="T523" id="Seg_1305" s="T522">kətsan-mɨ</ta>
            <ta e="T524" id="Seg_1306" s="T523">čʼäːŋkɨ</ta>
            <ta e="T525" id="Seg_1307" s="T524">onäk</ta>
            <ta e="T526" id="Seg_1308" s="T525">qən-lä</ta>
            <ta e="T527" id="Seg_1309" s="T526">taːtɨ-sɨ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T366" id="Seg_1310" s="T365">daddy.[NOM]-3SG</ta>
            <ta e="T367" id="Seg_1311" s="T366">late.[NOM]-3SG</ta>
            <ta e="T368" id="Seg_1312" s="T367">earlier</ta>
            <ta e="T369" id="Seg_1313" s="T368">so</ta>
            <ta e="T370" id="Seg_1314" s="T369">%%</ta>
            <ta e="T371" id="Seg_1315" s="T370">speak-HAB-CO-3SG.O</ta>
            <ta e="T372" id="Seg_1316" s="T371">live-ACTN-LOC-OBL.3SG</ta>
            <ta e="T373" id="Seg_1317" s="T372">good-ADVZ</ta>
            <ta e="T374" id="Seg_1318" s="T373">live-IMP.2PL</ta>
            <ta e="T375" id="Seg_1319" s="T374">child-PL.[NOM]</ta>
            <ta e="T376" id="Seg_1320" s="T375">wife-ACC</ta>
            <ta e="T377" id="Seg_1321" s="T376">find-FUT-2PL</ta>
            <ta e="T378" id="Seg_1322" s="T377">good-ADVZ</ta>
            <ta e="T379" id="Seg_1323" s="T378">live-IMP.2PL</ta>
            <ta e="T380" id="Seg_1324" s="T379">then</ta>
            <ta e="T381" id="Seg_1325" s="T380">just</ta>
            <ta e="T382" id="Seg_1326" s="T381">three</ta>
            <ta e="T383" id="Seg_1327" s="T382">daughter_in_law.[NOM]-3SG</ta>
            <ta e="T384" id="Seg_1328" s="T383">two</ta>
            <ta e="T385" id="Seg_1329" s="T384">son-GEN-3SG</ta>
            <ta e="T386" id="Seg_1330" s="T385">something-DU-ADJZ</ta>
            <ta e="T387" id="Seg_1331" s="T386">daughter_in_law-PL-ACC-3SG</ta>
            <ta e="T388" id="Seg_1332" s="T387">all</ta>
            <ta e="T389" id="Seg_1333" s="T388">bad</ta>
            <ta e="T390" id="Seg_1334" s="T389">finish-TR-PST.NAR-3SG.O</ta>
            <ta e="T391" id="Seg_1335" s="T390">but</ta>
            <ta e="T392" id="Seg_1336" s="T391">most</ta>
            <ta e="T393" id="Seg_1337" s="T392">elder</ta>
            <ta e="T394" id="Seg_1338" s="T393">daughter_in_law-ACC-3SG</ta>
            <ta e="T395" id="Seg_1339" s="T394">force-INSTR</ta>
            <ta e="T396" id="Seg_1340" s="T395">like-1SG.O</ta>
            <ta e="T397" id="Seg_1341" s="T396">%%</ta>
            <ta e="T398" id="Seg_1342" s="T397">as.if</ta>
            <ta e="T399" id="Seg_1343" s="T398">elder</ta>
            <ta e="T400" id="Seg_1344" s="T399">son.[NOM]</ta>
            <ta e="T401" id="Seg_1345" s="T400">elder</ta>
            <ta e="T402" id="Seg_1346" s="T401">daughter.[NOM]</ta>
            <ta e="T403" id="Seg_1347" s="T402">good-ADVZ</ta>
            <ta e="T404" id="Seg_1348" s="T403">live-IMP.2PL</ta>
            <ta e="T405" id="Seg_1349" s="T404">then</ta>
            <ta e="T406" id="Seg_1350" s="T405">so</ta>
            <ta e="T407" id="Seg_1351" s="T406">%%</ta>
            <ta e="T408" id="Seg_1352" s="T407">sing-HAB.[3SG.S]</ta>
            <ta e="T409" id="Seg_1353" s="T408">good-ADVZ</ta>
            <ta e="T410" id="Seg_1354" s="T409">as.if</ta>
            <ta e="T411" id="Seg_1355" s="T410">jetty</ta>
            <ta e="T412" id="Seg_1356" s="T411">seven</ta>
            <ta e="T413" id="Seg_1357" s="T412">crook-ACC</ta>
            <ta e="T414" id="Seg_1358" s="T413">it.is.said</ta>
            <ta e="T415" id="Seg_1359" s="T414">catch-DUR-IMP.3SG.O</ta>
            <ta e="T416" id="Seg_1360" s="T415">elder</ta>
            <ta e="T417" id="Seg_1361" s="T416">daughter.[NOM]</ta>
            <ta e="T418" id="Seg_1362" s="T417">as.if</ta>
            <ta e="T419" id="Seg_1363" s="T418">I.GEN-EMPH</ta>
            <ta e="T420" id="Seg_1364" s="T419">after-ADJZ</ta>
            <ta e="T421" id="Seg_1365" s="T420">day-LOC.1SG</ta>
            <ta e="T422" id="Seg_1366" s="T421">good-ADVZ</ta>
            <ta e="T423" id="Seg_1367" s="T422">all</ta>
            <ta e="T424" id="Seg_1368" s="T423">it.is.said</ta>
            <ta e="T425" id="Seg_1369" s="T424">OPT</ta>
            <ta e="T426" id="Seg_1370" s="T425">%%</ta>
            <ta e="T427" id="Seg_1371" s="T426">live-OPT-2PL</ta>
            <ta e="T428" id="Seg_1372" s="T427">I.GEN-EMPH</ta>
            <ta e="T429" id="Seg_1373" s="T428">after-LOC.1SG</ta>
            <ta e="T430" id="Seg_1374" s="T429">also</ta>
            <ta e="T431" id="Seg_1375" s="T430">this.[NOM]</ta>
            <ta e="T432" id="Seg_1376" s="T431">%%</ta>
            <ta e="T433" id="Seg_1377" s="T432">what.[NOM]</ta>
            <ta e="T434" id="Seg_1378" s="T433">it.is.said</ta>
            <ta e="T435" id="Seg_1379" s="T434">OPT</ta>
            <ta e="T436" id="Seg_1380" s="T435">%%</ta>
            <ta e="T437" id="Seg_1381" s="T436">catch-OPT-2PL</ta>
            <ta e="T438" id="Seg_1382" s="T437">I.GEN</ta>
            <ta e="T439" id="Seg_1383" s="T438">it.is.said</ta>
            <ta e="T440" id="Seg_1384" s="T439">I.NOM</ta>
            <ta e="T441" id="Seg_1385" s="T440">also</ta>
            <ta e="T442" id="Seg_1386" s="T441">die-ACTN-ADJZ</ta>
            <ta e="T443" id="Seg_1387" s="T442">after-LOC.1SG</ta>
            <ta e="T444" id="Seg_1388" s="T443">die-ACTN-ADJZ</ta>
            <ta e="T445" id="Seg_1389" s="T444">after-LOC.1SG</ta>
            <ta e="T446" id="Seg_1390" s="T445">also</ta>
            <ta e="T447" id="Seg_1391" s="T446">elder</ta>
            <ta e="T448" id="Seg_1392" s="T447">son.[NOM]</ta>
            <ta e="T449" id="Seg_1393" s="T448">it.is.said</ta>
            <ta e="T450" id="Seg_1394" s="T449">I.ACC</ta>
            <ta e="T451" id="Seg_1395" s="T450">and</ta>
            <ta e="T452" id="Seg_1396" s="T451">this.[NOM]</ta>
            <ta e="T453" id="Seg_1397" s="T452">think-CAUS-FUT.[3SG.S]</ta>
            <ta e="T454" id="Seg_1398" s="T453">spirit.[NOM]</ta>
            <ta e="T455" id="Seg_1399" s="T454">much-ADVZ</ta>
            <ta e="T456" id="Seg_1400" s="T455">NEG.IMP</ta>
            <ta e="T457" id="Seg_1401" s="T456">drink-DUR-IMP.2SG.O</ta>
            <ta e="T458" id="Seg_1402" s="T457">middle-DIM-ADVZ</ta>
            <ta e="T459" id="Seg_1403" s="T458">OPT</ta>
            <ta e="T460" id="Seg_1404" s="T459">it.is.said</ta>
            <ta e="T461" id="Seg_1405" s="T460">drink-FRQ-HAB-OPT-2SG.S</ta>
            <ta e="T462" id="Seg_1406" s="T461">also</ta>
            <ta e="T463" id="Seg_1407" s="T462">eat-EP-FRQ-HAB-OPT-2SG.S</ta>
            <ta e="T464" id="Seg_1408" s="T463">also</ta>
            <ta e="T465" id="Seg_1409" s="T464">you.PL.NOM</ta>
            <ta e="T466" id="Seg_1410" s="T465">drink-DUR-3PL</ta>
            <ta e="T467" id="Seg_1411" s="T466">I.NOM</ta>
            <ta e="T468" id="Seg_1412" s="T467">also</ta>
            <ta e="T469" id="Seg_1413" s="T468">you.SG.ACC</ta>
            <ta e="T470" id="Seg_1414" s="T469">bring.up-ACTN-LOC.1SG</ta>
            <ta e="T471" id="Seg_1415" s="T470">spirit-EP-ACC</ta>
            <ta e="T472" id="Seg_1416" s="T471">also</ta>
            <ta e="T473" id="Seg_1417" s="T472">eat-HAB-1SG.O</ta>
            <ta e="T474" id="Seg_1418" s="T473">new</ta>
            <ta e="T475" id="Seg_1419" s="T474">Russian-EP-PL.[NOM]</ta>
            <ta e="T476" id="Seg_1420" s="T475">such-ADVZ</ta>
            <ta e="T477" id="Seg_1421" s="T476">speak-HAB-3PL</ta>
            <ta e="T478" id="Seg_1422" s="T477">it.is.said</ta>
            <ta e="T479" id="Seg_1423" s="T478">middle-DIM.[NOM]</ta>
            <ta e="T480" id="Seg_1424" s="T479">drink-FRQ-IMP.2SG.S</ta>
            <ta e="T481" id="Seg_1425" s="T480">such-ADVZ</ta>
            <ta e="T482" id="Seg_1426" s="T481">OPT</ta>
            <ta e="T483" id="Seg_1427" s="T482">%%</ta>
            <ta e="T484" id="Seg_1428" s="T483">live-OPT-2SG.S</ta>
            <ta e="T485" id="Seg_1429" s="T484">much</ta>
            <ta e="T486" id="Seg_1430" s="T485">NEG.IMP</ta>
            <ta e="T487" id="Seg_1431" s="T486">drink-DUR-IMP.2SG.S</ta>
            <ta e="T488" id="Seg_1432" s="T487">I.GEN</ta>
            <ta e="T489" id="Seg_1433" s="T488">also</ta>
            <ta e="T490" id="Seg_1434" s="T489">die-ACTN-ADJZ</ta>
            <ta e="T491" id="Seg_1435" s="T490">after-LOC.1SG</ta>
            <ta e="T492" id="Seg_1436" s="T491">the.departed-EP-ADJZ</ta>
            <ta e="T493" id="Seg_1437" s="T492">daddy-ACC-OBL.2SG</ta>
            <ta e="T494" id="Seg_1438" s="T493">OPT</ta>
            <ta e="T495" id="Seg_1439" s="T494">%%</ta>
            <ta e="T496" id="Seg_1440" s="T495">think-CAUS-HAB-OPT-2SG.O</ta>
            <ta e="T497" id="Seg_1441" s="T496">which</ta>
            <ta e="T498" id="Seg_1442" s="T497">INDEF</ta>
            <ta e="T499" id="Seg_1443" s="T498">day.[NOM]</ta>
            <ta e="T500" id="Seg_1444" s="T499">but</ta>
            <ta e="T501" id="Seg_1445" s="T500">elder</ta>
            <ta e="T502" id="Seg_1446" s="T501">daughter.[NOM]-1SG</ta>
            <ta e="T503" id="Seg_1447" s="T502">also</ta>
            <ta e="T504" id="Seg_1448" s="T503">now</ta>
            <ta e="T505" id="Seg_1449" s="T504">what-EP-ACC</ta>
            <ta e="T506" id="Seg_1450" s="T505">say-FUT-3SG.O</ta>
            <ta e="T507" id="Seg_1451" s="T506">OPT</ta>
            <ta e="T508" id="Seg_1452" s="T507">%%</ta>
            <ta e="T509" id="Seg_1453" s="T508">hear-TR-HAB-OPT-2SG.O</ta>
            <ta e="T510" id="Seg_1454" s="T509">then</ta>
            <ta e="T511" id="Seg_1455" s="T510">here</ta>
            <ta e="T512" id="Seg_1456" s="T511">stop-FRQ-EP-CVB</ta>
            <ta e="T513" id="Seg_1457" s="T512">grandson-PL.[NOM]-1SG</ta>
            <ta e="T514" id="Seg_1458" s="T513">NEG</ta>
            <ta e="T515" id="Seg_1459" s="T514">hurt</ta>
            <ta e="T516" id="Seg_1460" s="T515">one</ta>
            <ta e="T517" id="Seg_1461" s="T516">human.being-PL-EP-ADJZ</ta>
            <ta e="T518" id="Seg_1462" s="T517">grandson.[NOM]-1SG</ta>
            <ta e="T519" id="Seg_1463" s="T518">one</ta>
            <ta e="T520" id="Seg_1464" s="T519">girl-ADJZ</ta>
            <ta e="T521" id="Seg_1465" s="T520">grandson.[NOM]-1SG</ta>
            <ta e="T522" id="Seg_1466" s="T521">much</ta>
            <ta e="T523" id="Seg_1467" s="T522">grandson.[NOM]-1SG</ta>
            <ta e="T524" id="Seg_1468" s="T523">NEG.EX.[3SG.S]</ta>
            <ta e="T525" id="Seg_1469" s="T524">oneself.1SG</ta>
            <ta e="T526" id="Seg_1470" s="T525">go.away-CVB</ta>
            <ta e="T527" id="Seg_1471" s="T526">bring-PST-1SG.O</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T366" id="Seg_1472" s="T365">папа.[NOM]-3SG</ta>
            <ta e="T367" id="Seg_1473" s="T366">покойный.[NOM]-3SG</ta>
            <ta e="T368" id="Seg_1474" s="T367">раньше</ta>
            <ta e="T369" id="Seg_1475" s="T368">так</ta>
            <ta e="T370" id="Seg_1476" s="T369">%%</ta>
            <ta e="T371" id="Seg_1477" s="T370">сказать-HAB-CO-3SG.O</ta>
            <ta e="T372" id="Seg_1478" s="T371">жить-ACTN-LOC-OBL.3SG</ta>
            <ta e="T373" id="Seg_1479" s="T372">хороший-ADVZ</ta>
            <ta e="T374" id="Seg_1480" s="T373">жить-IMP.2PL</ta>
            <ta e="T375" id="Seg_1481" s="T374">ребенок-PL.[NOM]</ta>
            <ta e="T376" id="Seg_1482" s="T375">жена-ACC</ta>
            <ta e="T377" id="Seg_1483" s="T376">находить-FUT-2PL</ta>
            <ta e="T378" id="Seg_1484" s="T377">хороший-ADVZ</ta>
            <ta e="T379" id="Seg_1485" s="T378">жить-IMP.2PL</ta>
            <ta e="T380" id="Seg_1486" s="T379">потом</ta>
            <ta e="T381" id="Seg_1487" s="T380">только.что</ta>
            <ta e="T382" id="Seg_1488" s="T381">три</ta>
            <ta e="T383" id="Seg_1489" s="T382">жена.сына.[NOM]-3SG</ta>
            <ta e="T384" id="Seg_1490" s="T383">два</ta>
            <ta e="T385" id="Seg_1491" s="T384">сын-GEN-3SG</ta>
            <ta e="T386" id="Seg_1492" s="T385">нечто-DU-ADJZ</ta>
            <ta e="T387" id="Seg_1493" s="T386">жена.сына-PL-ACC-3SG</ta>
            <ta e="T388" id="Seg_1494" s="T387">всё</ta>
            <ta e="T389" id="Seg_1495" s="T388">плохой</ta>
            <ta e="T390" id="Seg_1496" s="T389">кончить-TR-PST.NAR-3SG.O</ta>
            <ta e="T391" id="Seg_1497" s="T390">а</ta>
            <ta e="T392" id="Seg_1498" s="T391">самый</ta>
            <ta e="T393" id="Seg_1499" s="T392">старший</ta>
            <ta e="T394" id="Seg_1500" s="T393">жена.сына-ACC-3SG</ta>
            <ta e="T395" id="Seg_1501" s="T394">сила-INSTR</ta>
            <ta e="T396" id="Seg_1502" s="T395">любить-1SG.O</ta>
            <ta e="T397" id="Seg_1503" s="T396">%%</ta>
            <ta e="T398" id="Seg_1504" s="T397">будто</ta>
            <ta e="T399" id="Seg_1505" s="T398">старший</ta>
            <ta e="T400" id="Seg_1506" s="T399">сын.[NOM]</ta>
            <ta e="T401" id="Seg_1507" s="T400">старший</ta>
            <ta e="T402" id="Seg_1508" s="T401">дочь.[NOM]</ta>
            <ta e="T403" id="Seg_1509" s="T402">хороший-ADVZ</ta>
            <ta e="T404" id="Seg_1510" s="T403">жить-IMP.2PL</ta>
            <ta e="T405" id="Seg_1511" s="T404">потом</ta>
            <ta e="T406" id="Seg_1512" s="T405">так</ta>
            <ta e="T407" id="Seg_1513" s="T406">%%</ta>
            <ta e="T408" id="Seg_1514" s="T407">петь-HAB.[3SG.S]</ta>
            <ta e="T409" id="Seg_1515" s="T408">хороший-ADVZ</ta>
            <ta e="T410" id="Seg_1516" s="T409">будто</ta>
            <ta e="T411" id="Seg_1517" s="T410">мол</ta>
            <ta e="T412" id="Seg_1518" s="T411">семь</ta>
            <ta e="T413" id="Seg_1519" s="T412">посох-ACC</ta>
            <ta e="T414" id="Seg_1520" s="T413">мол</ta>
            <ta e="T415" id="Seg_1521" s="T414">схватить-DUR-IMP.3SG.O</ta>
            <ta e="T416" id="Seg_1522" s="T415">старший</ta>
            <ta e="T417" id="Seg_1523" s="T416">дочь.[NOM]</ta>
            <ta e="T418" id="Seg_1524" s="T417">будто</ta>
            <ta e="T419" id="Seg_1525" s="T418">я.GEN-EMPH</ta>
            <ta e="T420" id="Seg_1526" s="T419">после-ADJZ</ta>
            <ta e="T421" id="Seg_1527" s="T420">день-LOC.1SG</ta>
            <ta e="T422" id="Seg_1528" s="T421">хороший-ADVZ</ta>
            <ta e="T423" id="Seg_1529" s="T422">всё</ta>
            <ta e="T424" id="Seg_1530" s="T423">мол</ta>
            <ta e="T425" id="Seg_1531" s="T424">OPT</ta>
            <ta e="T426" id="Seg_1532" s="T425">%%</ta>
            <ta e="T427" id="Seg_1533" s="T426">жить-OPT-2PL</ta>
            <ta e="T428" id="Seg_1534" s="T427">я.GEN-EMPH</ta>
            <ta e="T429" id="Seg_1535" s="T428">после-LOC.1SG</ta>
            <ta e="T430" id="Seg_1536" s="T429">тоже</ta>
            <ta e="T431" id="Seg_1537" s="T430">этот.[NOM]</ta>
            <ta e="T432" id="Seg_1538" s="T431">%%</ta>
            <ta e="T433" id="Seg_1539" s="T432">что.[NOM]</ta>
            <ta e="T434" id="Seg_1540" s="T433">мол</ta>
            <ta e="T435" id="Seg_1541" s="T434">OPT</ta>
            <ta e="T436" id="Seg_1542" s="T435">%%</ta>
            <ta e="T437" id="Seg_1543" s="T436">схватить-OPT-2PL</ta>
            <ta e="T438" id="Seg_1544" s="T437">я.GEN</ta>
            <ta e="T439" id="Seg_1545" s="T438">мол</ta>
            <ta e="T440" id="Seg_1546" s="T439">я.NOM</ta>
            <ta e="T441" id="Seg_1547" s="T440">тоже</ta>
            <ta e="T442" id="Seg_1548" s="T441">умереть-ACTN-ADJZ</ta>
            <ta e="T443" id="Seg_1549" s="T442">после-LOC.1SG</ta>
            <ta e="T444" id="Seg_1550" s="T443">умереть-ACTN-ADJZ</ta>
            <ta e="T445" id="Seg_1551" s="T444">после-LOC.1SG</ta>
            <ta e="T446" id="Seg_1552" s="T445">тоже</ta>
            <ta e="T447" id="Seg_1553" s="T446">старший</ta>
            <ta e="T448" id="Seg_1554" s="T447">сын.[NOM]</ta>
            <ta e="T449" id="Seg_1555" s="T448">мол</ta>
            <ta e="T450" id="Seg_1556" s="T449">я.ACC</ta>
            <ta e="T451" id="Seg_1557" s="T450">и</ta>
            <ta e="T452" id="Seg_1558" s="T451">это.[NOM]</ta>
            <ta e="T453" id="Seg_1559" s="T452">думать-CAUS-FUT.[3SG.S]</ta>
            <ta e="T454" id="Seg_1560" s="T453">алкоголь.[NOM]</ta>
            <ta e="T455" id="Seg_1561" s="T454">много-ADVZ</ta>
            <ta e="T456" id="Seg_1562" s="T455">NEG.IMP</ta>
            <ta e="T457" id="Seg_1563" s="T456">пить-DUR-IMP.2SG.O</ta>
            <ta e="T458" id="Seg_1564" s="T457">середина-DIM-ADVZ</ta>
            <ta e="T459" id="Seg_1565" s="T458">OPT</ta>
            <ta e="T460" id="Seg_1566" s="T459">мол</ta>
            <ta e="T461" id="Seg_1567" s="T460">пить-FRQ-HAB-OPT-2SG.S</ta>
            <ta e="T462" id="Seg_1568" s="T461">тоже</ta>
            <ta e="T463" id="Seg_1569" s="T462">съесть-EP-FRQ-HAB-OPT-2SG.S</ta>
            <ta e="T464" id="Seg_1570" s="T463">тоже</ta>
            <ta e="T465" id="Seg_1571" s="T464">вы.PL.NOM</ta>
            <ta e="T466" id="Seg_1572" s="T465">пить-DUR-3PL</ta>
            <ta e="T467" id="Seg_1573" s="T466">я.NOM</ta>
            <ta e="T468" id="Seg_1574" s="T467">тоже</ta>
            <ta e="T469" id="Seg_1575" s="T468">ты.ACC</ta>
            <ta e="T470" id="Seg_1576" s="T469">вырастить-ACTN-LOC.1SG</ta>
            <ta e="T471" id="Seg_1577" s="T470">алкоголь-EP-ACC</ta>
            <ta e="T472" id="Seg_1578" s="T471">тоже</ta>
            <ta e="T473" id="Seg_1579" s="T472">съесть-HAB-1SG.O</ta>
            <ta e="T474" id="Seg_1580" s="T473">новый</ta>
            <ta e="T475" id="Seg_1581" s="T474">русский-EP-PL.[NOM]</ta>
            <ta e="T476" id="Seg_1582" s="T475">такой-ADVZ</ta>
            <ta e="T477" id="Seg_1583" s="T476">сказать-HAB-3PL</ta>
            <ta e="T478" id="Seg_1584" s="T477">мол</ta>
            <ta e="T479" id="Seg_1585" s="T478">середина-DIM.[NOM]</ta>
            <ta e="T480" id="Seg_1586" s="T479">пить-FRQ-IMP.2SG.S</ta>
            <ta e="T481" id="Seg_1587" s="T480">такой-ADVZ</ta>
            <ta e="T482" id="Seg_1588" s="T481">OPT</ta>
            <ta e="T483" id="Seg_1589" s="T482">%%</ta>
            <ta e="T484" id="Seg_1590" s="T483">жить-OPT-2SG.S</ta>
            <ta e="T485" id="Seg_1591" s="T484">много</ta>
            <ta e="T486" id="Seg_1592" s="T485">NEG.IMP</ta>
            <ta e="T487" id="Seg_1593" s="T486">пить-DUR-IMP.2SG.S</ta>
            <ta e="T488" id="Seg_1594" s="T487">я.GEN</ta>
            <ta e="T489" id="Seg_1595" s="T488">тоже</ta>
            <ta e="T490" id="Seg_1596" s="T489">умереть-ACTN-ADJZ</ta>
            <ta e="T491" id="Seg_1597" s="T490">после-LOC.1SG</ta>
            <ta e="T492" id="Seg_1598" s="T491">покойник-EP-ADJZ</ta>
            <ta e="T493" id="Seg_1599" s="T492">папа-ACC-OBL.2SG</ta>
            <ta e="T494" id="Seg_1600" s="T493">OPT</ta>
            <ta e="T495" id="Seg_1601" s="T494">%%</ta>
            <ta e="T496" id="Seg_1602" s="T495">думать-CAUS-HAB-OPT-2SG.O</ta>
            <ta e="T497" id="Seg_1603" s="T496">какой</ta>
            <ta e="T498" id="Seg_1604" s="T497">INDEF</ta>
            <ta e="T499" id="Seg_1605" s="T498">день.[NOM]</ta>
            <ta e="T500" id="Seg_1606" s="T499">а</ta>
            <ta e="T501" id="Seg_1607" s="T500">старший</ta>
            <ta e="T502" id="Seg_1608" s="T501">дочь.[NOM]-1SG</ta>
            <ta e="T503" id="Seg_1609" s="T502">тоже</ta>
            <ta e="T504" id="Seg_1610" s="T503">ну</ta>
            <ta e="T505" id="Seg_1611" s="T504">что-EP-ACC</ta>
            <ta e="T506" id="Seg_1612" s="T505">сказать-FUT-3SG.O</ta>
            <ta e="T507" id="Seg_1613" s="T506">OPT</ta>
            <ta e="T508" id="Seg_1614" s="T507">%%</ta>
            <ta e="T509" id="Seg_1615" s="T508">слушать-TR-HAB-OPT-2SG.O</ta>
            <ta e="T510" id="Seg_1616" s="T509">затем</ta>
            <ta e="T511" id="Seg_1617" s="T510">здесь</ta>
            <ta e="T512" id="Seg_1618" s="T511">остановить-FRQ-EP-CVB</ta>
            <ta e="T513" id="Seg_1619" s="T512">внук-PL.[NOM]-1SG</ta>
            <ta e="T514" id="Seg_1620" s="T513">NEG</ta>
            <ta e="T515" id="Seg_1621" s="T514">обижайте</ta>
            <ta e="T516" id="Seg_1622" s="T515">один</ta>
            <ta e="T517" id="Seg_1623" s="T516">человек-PL-EP-ADJZ</ta>
            <ta e="T518" id="Seg_1624" s="T517">внук.[NOM]-1SG</ta>
            <ta e="T519" id="Seg_1625" s="T518">один</ta>
            <ta e="T520" id="Seg_1626" s="T519">девушка-ADJZ</ta>
            <ta e="T521" id="Seg_1627" s="T520">внук.[NOM]-1SG</ta>
            <ta e="T522" id="Seg_1628" s="T521">много</ta>
            <ta e="T523" id="Seg_1629" s="T522">внук.[NOM]-1SG</ta>
            <ta e="T524" id="Seg_1630" s="T523">NEG.EX.[3SG.S]</ta>
            <ta e="T525" id="Seg_1631" s="T524">сам.1SG</ta>
            <ta e="T526" id="Seg_1632" s="T525">уйти-CVB</ta>
            <ta e="T527" id="Seg_1633" s="T526">привезти-PST-1SG.O</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T366" id="Seg_1634" s="T365">n-n:case-n:poss</ta>
            <ta e="T367" id="Seg_1635" s="T366">n-n:case-n:poss</ta>
            <ta e="T368" id="Seg_1636" s="T367">adv</ta>
            <ta e="T369" id="Seg_1637" s="T368">adv</ta>
            <ta e="T370" id="Seg_1638" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_1639" s="T370">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T372" id="Seg_1640" s="T371">v-v&gt;n-n:case-n:obl.poss</ta>
            <ta e="T373" id="Seg_1641" s="T372">adj-adj&gt;adv</ta>
            <ta e="T374" id="Seg_1642" s="T373">v-v:mood.pn</ta>
            <ta e="T375" id="Seg_1643" s="T374">n-n:num-n:case</ta>
            <ta e="T376" id="Seg_1644" s="T375">n-n:case</ta>
            <ta e="T377" id="Seg_1645" s="T376">v-v:tense-v:pn</ta>
            <ta e="T378" id="Seg_1646" s="T377">adj-adj&gt;adv</ta>
            <ta e="T379" id="Seg_1647" s="T378">v-v:mood.pn</ta>
            <ta e="T380" id="Seg_1648" s="T379">adv</ta>
            <ta e="T381" id="Seg_1649" s="T380">adv</ta>
            <ta e="T382" id="Seg_1650" s="T381">num</ta>
            <ta e="T383" id="Seg_1651" s="T382">n-n:case-n:poss</ta>
            <ta e="T384" id="Seg_1652" s="T383">num</ta>
            <ta e="T385" id="Seg_1653" s="T384">n-n:case-n:poss</ta>
            <ta e="T386" id="Seg_1654" s="T385">n-n:num-n&gt;adj</ta>
            <ta e="T387" id="Seg_1655" s="T386">n-n:num-n:case-n:poss</ta>
            <ta e="T388" id="Seg_1656" s="T387">quant</ta>
            <ta e="T389" id="Seg_1657" s="T388">adj</ta>
            <ta e="T390" id="Seg_1658" s="T389">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T391" id="Seg_1659" s="T390">conj</ta>
            <ta e="T392" id="Seg_1660" s="T391">adv</ta>
            <ta e="T393" id="Seg_1661" s="T392">adj</ta>
            <ta e="T394" id="Seg_1662" s="T393">n-n:case-n:poss</ta>
            <ta e="T395" id="Seg_1663" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_1664" s="T395">v-v:pn</ta>
            <ta e="T397" id="Seg_1665" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_1666" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_1667" s="T398">adj</ta>
            <ta e="T400" id="Seg_1668" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_1669" s="T400">adj</ta>
            <ta e="T402" id="Seg_1670" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_1671" s="T402">adj-adj&gt;adv</ta>
            <ta e="T404" id="Seg_1672" s="T403">v-v:mood.pn</ta>
            <ta e="T405" id="Seg_1673" s="T404">adv</ta>
            <ta e="T406" id="Seg_1674" s="T405">adv</ta>
            <ta e="T407" id="Seg_1675" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_1676" s="T407">v-v&gt;v-v:pn</ta>
            <ta e="T409" id="Seg_1677" s="T408">adj-adj&gt;adv</ta>
            <ta e="T410" id="Seg_1678" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_1679" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_1680" s="T411">num</ta>
            <ta e="T413" id="Seg_1681" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_1682" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_1683" s="T414">v-v&gt;v-v:mood.pn</ta>
            <ta e="T416" id="Seg_1684" s="T415">adj</ta>
            <ta e="T417" id="Seg_1685" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_1686" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_1687" s="T418">pers-clit</ta>
            <ta e="T420" id="Seg_1688" s="T419">pp-pp&gt;adj</ta>
            <ta e="T421" id="Seg_1689" s="T420">n-n:case.poss</ta>
            <ta e="T422" id="Seg_1690" s="T421">adj-adj&gt;adv</ta>
            <ta e="T423" id="Seg_1691" s="T422">quant</ta>
            <ta e="T424" id="Seg_1692" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_1693" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_1694" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_1695" s="T426">v-v:mood-v:pn</ta>
            <ta e="T428" id="Seg_1696" s="T427">pers-clit</ta>
            <ta e="T429" id="Seg_1697" s="T428">pp-n:case.poss</ta>
            <ta e="T430" id="Seg_1698" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_1699" s="T430">dem-n:case</ta>
            <ta e="T432" id="Seg_1700" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_1701" s="T432">interrog-n:case</ta>
            <ta e="T434" id="Seg_1702" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_1703" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_1704" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_1705" s="T436">v-v:mood-v:pn</ta>
            <ta e="T438" id="Seg_1706" s="T437">pers</ta>
            <ta e="T439" id="Seg_1707" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_1708" s="T439">pers</ta>
            <ta e="T441" id="Seg_1709" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_1710" s="T441">v-v&gt;n-n&gt;adj</ta>
            <ta e="T443" id="Seg_1711" s="T442">pp-n:case.poss</ta>
            <ta e="T444" id="Seg_1712" s="T443">v-v&gt;n-n&gt;adj</ta>
            <ta e="T445" id="Seg_1713" s="T444">pp-n:case.poss</ta>
            <ta e="T446" id="Seg_1714" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_1715" s="T446">adj</ta>
            <ta e="T448" id="Seg_1716" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_1717" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_1718" s="T449">pers</ta>
            <ta e="T451" id="Seg_1719" s="T450">conj</ta>
            <ta e="T452" id="Seg_1720" s="T451">pro-n:case</ta>
            <ta e="T453" id="Seg_1721" s="T452">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_1722" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_1723" s="T454">quant-adj&gt;adv</ta>
            <ta e="T456" id="Seg_1724" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_1725" s="T456">v-v&gt;v-v:mood.pn</ta>
            <ta e="T458" id="Seg_1726" s="T457">n-n&gt;n-n&gt;adv</ta>
            <ta e="T459" id="Seg_1727" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_1728" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_1729" s="T460">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T462" id="Seg_1730" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_1731" s="T462">v-v:ins-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T464" id="Seg_1732" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_1733" s="T464">pers</ta>
            <ta e="T466" id="Seg_1734" s="T465">v-v&gt;v-v:pn</ta>
            <ta e="T467" id="Seg_1735" s="T466">pers</ta>
            <ta e="T468" id="Seg_1736" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_1737" s="T468">pers</ta>
            <ta e="T470" id="Seg_1738" s="T469">v-v&gt;n-n:case.poss</ta>
            <ta e="T471" id="Seg_1739" s="T470">n-n:ins-n:case</ta>
            <ta e="T472" id="Seg_1740" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_1741" s="T472">v-v&gt;v-v:pn</ta>
            <ta e="T474" id="Seg_1742" s="T473">adj</ta>
            <ta e="T475" id="Seg_1743" s="T474">n-n:ins-n:num-n:case</ta>
            <ta e="T476" id="Seg_1744" s="T475">dem-adj&gt;adv</ta>
            <ta e="T477" id="Seg_1745" s="T476">v-v&gt;v-v:pn</ta>
            <ta e="T478" id="Seg_1746" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_1747" s="T478">n-n&gt;n-n:case</ta>
            <ta e="T480" id="Seg_1748" s="T479">v-v&gt;v-v:mood.pn</ta>
            <ta e="T481" id="Seg_1749" s="T480">dem-adj&gt;adv</ta>
            <ta e="T482" id="Seg_1750" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_1751" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_1752" s="T483">v-v:mood-v:pn</ta>
            <ta e="T485" id="Seg_1753" s="T484">quant</ta>
            <ta e="T486" id="Seg_1754" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_1755" s="T486">v-v&gt;v-v:mood.pn</ta>
            <ta e="T488" id="Seg_1756" s="T487">pers</ta>
            <ta e="T489" id="Seg_1757" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_1758" s="T489">v-v&gt;n-n&gt;adj</ta>
            <ta e="T491" id="Seg_1759" s="T490">pp-n:case.poss</ta>
            <ta e="T492" id="Seg_1760" s="T491">n-n:ins-n&gt;adj</ta>
            <ta e="T493" id="Seg_1761" s="T492">n-n:case-n:obl.poss</ta>
            <ta e="T494" id="Seg_1762" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_1763" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_1764" s="T495">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T497" id="Seg_1765" s="T496">interrog</ta>
            <ta e="T498" id="Seg_1766" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_1767" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_1768" s="T499">conj</ta>
            <ta e="T501" id="Seg_1769" s="T500">adj</ta>
            <ta e="T502" id="Seg_1770" s="T501">n-n:case-n:poss</ta>
            <ta e="T503" id="Seg_1771" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_1772" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_1773" s="T504">interrog-n:ins-n:case</ta>
            <ta e="T506" id="Seg_1774" s="T505">v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_1775" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_1776" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_1777" s="T508">v-v&gt;v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T510" id="Seg_1778" s="T509">adv</ta>
            <ta e="T511" id="Seg_1779" s="T510">adv</ta>
            <ta e="T512" id="Seg_1780" s="T511">v-v&gt;v-v:ins-v&gt;adv</ta>
            <ta e="T513" id="Seg_1781" s="T512">n-n:num-n:case-n:poss</ta>
            <ta e="T514" id="Seg_1782" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_1783" s="T514">v</ta>
            <ta e="T516" id="Seg_1784" s="T515">num</ta>
            <ta e="T517" id="Seg_1785" s="T516">n-n:num-n:ins-n&gt;adj</ta>
            <ta e="T518" id="Seg_1786" s="T517">n-n:case-n:poss</ta>
            <ta e="T519" id="Seg_1787" s="T518">num</ta>
            <ta e="T520" id="Seg_1788" s="T519">n-n&gt;adj</ta>
            <ta e="T521" id="Seg_1789" s="T520">n-n:case-n:poss</ta>
            <ta e="T522" id="Seg_1790" s="T521">quant</ta>
            <ta e="T523" id="Seg_1791" s="T522">n-n:case-n:poss</ta>
            <ta e="T524" id="Seg_1792" s="T523">v-v:pn</ta>
            <ta e="T525" id="Seg_1793" s="T524">emphpro</ta>
            <ta e="T526" id="Seg_1794" s="T525">v-v&gt;adv</ta>
            <ta e="T527" id="Seg_1795" s="T526">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T366" id="Seg_1796" s="T365">n</ta>
            <ta e="T367" id="Seg_1797" s="T366">n</ta>
            <ta e="T368" id="Seg_1798" s="T367">adv</ta>
            <ta e="T369" id="Seg_1799" s="T368">adv</ta>
            <ta e="T370" id="Seg_1800" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_1801" s="T370">v</ta>
            <ta e="T372" id="Seg_1802" s="T371">n</ta>
            <ta e="T373" id="Seg_1803" s="T372">adv</ta>
            <ta e="T374" id="Seg_1804" s="T373">v</ta>
            <ta e="T375" id="Seg_1805" s="T374">n</ta>
            <ta e="T376" id="Seg_1806" s="T375">n</ta>
            <ta e="T377" id="Seg_1807" s="T376">v</ta>
            <ta e="T378" id="Seg_1808" s="T377">adv</ta>
            <ta e="T379" id="Seg_1809" s="T378">v</ta>
            <ta e="T380" id="Seg_1810" s="T379">adv</ta>
            <ta e="T381" id="Seg_1811" s="T380">adv</ta>
            <ta e="T382" id="Seg_1812" s="T381">num</ta>
            <ta e="T383" id="Seg_1813" s="T382">n</ta>
            <ta e="T384" id="Seg_1814" s="T383">num</ta>
            <ta e="T385" id="Seg_1815" s="T384">n</ta>
            <ta e="T386" id="Seg_1816" s="T385">adj</ta>
            <ta e="T387" id="Seg_1817" s="T386">n</ta>
            <ta e="T388" id="Seg_1818" s="T387">quant</ta>
            <ta e="T389" id="Seg_1819" s="T388">adj</ta>
            <ta e="T390" id="Seg_1820" s="T389">v</ta>
            <ta e="T391" id="Seg_1821" s="T390">conj</ta>
            <ta e="T392" id="Seg_1822" s="T391">adv</ta>
            <ta e="T393" id="Seg_1823" s="T392">adj</ta>
            <ta e="T394" id="Seg_1824" s="T393">n</ta>
            <ta e="T395" id="Seg_1825" s="T394">n</ta>
            <ta e="T396" id="Seg_1826" s="T395">v</ta>
            <ta e="T397" id="Seg_1827" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_1828" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_1829" s="T398">adj</ta>
            <ta e="T400" id="Seg_1830" s="T399">n</ta>
            <ta e="T401" id="Seg_1831" s="T400">adj</ta>
            <ta e="T402" id="Seg_1832" s="T401">n</ta>
            <ta e="T403" id="Seg_1833" s="T402">adv</ta>
            <ta e="T404" id="Seg_1834" s="T403">v</ta>
            <ta e="T405" id="Seg_1835" s="T404">adv</ta>
            <ta e="T406" id="Seg_1836" s="T405">adv</ta>
            <ta e="T407" id="Seg_1837" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_1838" s="T407">v</ta>
            <ta e="T409" id="Seg_1839" s="T408">adv</ta>
            <ta e="T410" id="Seg_1840" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_1841" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_1842" s="T411">num</ta>
            <ta e="T413" id="Seg_1843" s="T412">n</ta>
            <ta e="T414" id="Seg_1844" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_1845" s="T414">v</ta>
            <ta e="T416" id="Seg_1846" s="T415">adj</ta>
            <ta e="T417" id="Seg_1847" s="T416">n</ta>
            <ta e="T418" id="Seg_1848" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_1849" s="T418">pers</ta>
            <ta e="T420" id="Seg_1850" s="T419">adj</ta>
            <ta e="T421" id="Seg_1851" s="T420">n</ta>
            <ta e="T422" id="Seg_1852" s="T421">adv</ta>
            <ta e="T423" id="Seg_1853" s="T422">quant</ta>
            <ta e="T424" id="Seg_1854" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_1855" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_1856" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_1857" s="T426">v</ta>
            <ta e="T428" id="Seg_1858" s="T427">pers</ta>
            <ta e="T429" id="Seg_1859" s="T428">pp</ta>
            <ta e="T430" id="Seg_1860" s="T429">adv</ta>
            <ta e="T431" id="Seg_1861" s="T430">pers</ta>
            <ta e="T432" id="Seg_1862" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_1863" s="T432">interrog</ta>
            <ta e="T434" id="Seg_1864" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_1865" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_1866" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_1867" s="T436">v</ta>
            <ta e="T438" id="Seg_1868" s="T437">pers</ta>
            <ta e="T439" id="Seg_1869" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_1870" s="T439">pers</ta>
            <ta e="T441" id="Seg_1871" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_1872" s="T441">adj</ta>
            <ta e="T443" id="Seg_1873" s="T442">pp</ta>
            <ta e="T444" id="Seg_1874" s="T443">adj</ta>
            <ta e="T445" id="Seg_1875" s="T444">pp</ta>
            <ta e="T446" id="Seg_1876" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_1877" s="T446">adj</ta>
            <ta e="T448" id="Seg_1878" s="T447">n</ta>
            <ta e="T449" id="Seg_1879" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_1880" s="T449">pers</ta>
            <ta e="T451" id="Seg_1881" s="T450">conj</ta>
            <ta e="T452" id="Seg_1882" s="T451">pro</ta>
            <ta e="T453" id="Seg_1883" s="T452">n</ta>
            <ta e="T454" id="Seg_1884" s="T453">n</ta>
            <ta e="T455" id="Seg_1885" s="T454">adv</ta>
            <ta e="T456" id="Seg_1886" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_1887" s="T456">v</ta>
            <ta e="T458" id="Seg_1888" s="T457">adv</ta>
            <ta e="T459" id="Seg_1889" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_1890" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_1891" s="T460">v</ta>
            <ta e="T462" id="Seg_1892" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_1893" s="T462">v</ta>
            <ta e="T464" id="Seg_1894" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_1895" s="T464">pers</ta>
            <ta e="T466" id="Seg_1896" s="T465">v</ta>
            <ta e="T467" id="Seg_1897" s="T466">pers</ta>
            <ta e="T468" id="Seg_1898" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_1899" s="T468">pers</ta>
            <ta e="T470" id="Seg_1900" s="T469">n</ta>
            <ta e="T471" id="Seg_1901" s="T470">n</ta>
            <ta e="T472" id="Seg_1902" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_1903" s="T472">v</ta>
            <ta e="T474" id="Seg_1904" s="T473">adj</ta>
            <ta e="T475" id="Seg_1905" s="T474">n</ta>
            <ta e="T476" id="Seg_1906" s="T475">adv</ta>
            <ta e="T477" id="Seg_1907" s="T476">v</ta>
            <ta e="T478" id="Seg_1908" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_1909" s="T478">n</ta>
            <ta e="T480" id="Seg_1910" s="T479">v</ta>
            <ta e="T481" id="Seg_1911" s="T480">adv</ta>
            <ta e="T482" id="Seg_1912" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_1913" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_1914" s="T483">v</ta>
            <ta e="T485" id="Seg_1915" s="T484">quant</ta>
            <ta e="T486" id="Seg_1916" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_1917" s="T486">v</ta>
            <ta e="T488" id="Seg_1918" s="T487">pers</ta>
            <ta e="T489" id="Seg_1919" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_1920" s="T489">adj</ta>
            <ta e="T491" id="Seg_1921" s="T490">pp</ta>
            <ta e="T492" id="Seg_1922" s="T491">adj</ta>
            <ta e="T493" id="Seg_1923" s="T492">n</ta>
            <ta e="T494" id="Seg_1924" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_1925" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_1926" s="T495">v</ta>
            <ta e="T497" id="Seg_1927" s="T496">interrog</ta>
            <ta e="T498" id="Seg_1928" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_1929" s="T498">n</ta>
            <ta e="T500" id="Seg_1930" s="T499">conj</ta>
            <ta e="T501" id="Seg_1931" s="T500">adj</ta>
            <ta e="T502" id="Seg_1932" s="T501">n</ta>
            <ta e="T503" id="Seg_1933" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_1934" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_1935" s="T504">interrog</ta>
            <ta e="T506" id="Seg_1936" s="T505">v</ta>
            <ta e="T507" id="Seg_1937" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_1938" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_1939" s="T508">v</ta>
            <ta e="T510" id="Seg_1940" s="T509">adv</ta>
            <ta e="T511" id="Seg_1941" s="T510">adv</ta>
            <ta e="T512" id="Seg_1942" s="T511">adv</ta>
            <ta e="T513" id="Seg_1943" s="T512">n</ta>
            <ta e="T514" id="Seg_1944" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_1945" s="T514">v</ta>
            <ta e="T516" id="Seg_1946" s="T515">num</ta>
            <ta e="T517" id="Seg_1947" s="T516">adj</ta>
            <ta e="T518" id="Seg_1948" s="T517">n</ta>
            <ta e="T519" id="Seg_1949" s="T518">num</ta>
            <ta e="T520" id="Seg_1950" s="T519">adj</ta>
            <ta e="T521" id="Seg_1951" s="T520">n</ta>
            <ta e="T522" id="Seg_1952" s="T521">quant</ta>
            <ta e="T523" id="Seg_1953" s="T522">n</ta>
            <ta e="T524" id="Seg_1954" s="T523">v</ta>
            <ta e="T525" id="Seg_1955" s="T524">emphpro</ta>
            <ta e="T526" id="Seg_1956" s="T525">adv</ta>
            <ta e="T527" id="Seg_1957" s="T526">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T366" id="Seg_1958" s="T365">np.h:A</ta>
            <ta e="T368" id="Seg_1959" s="T367">adv:Time</ta>
            <ta e="T372" id="Seg_1960" s="T371">0.3.h:Th</ta>
            <ta e="T374" id="Seg_1961" s="T373">0.2.h:Th</ta>
            <ta e="T376" id="Seg_1962" s="T375">np.h:Th</ta>
            <ta e="T377" id="Seg_1963" s="T376">0.2.h:B</ta>
            <ta e="T379" id="Seg_1964" s="T378">0.2.h:Th</ta>
            <ta e="T383" id="Seg_1965" s="T382">np.h:Th 0.3.h:Poss</ta>
            <ta e="T385" id="Seg_1966" s="T384">pp.h:Poss 0.3.h:Poss</ta>
            <ta e="T387" id="Seg_1967" s="T386">np:Th</ta>
            <ta e="T390" id="Seg_1968" s="T389">0.3.h:A</ta>
            <ta e="T394" id="Seg_1969" s="T393">np.h:Th</ta>
            <ta e="T396" id="Seg_1970" s="T395">0.1.h:E</ta>
            <ta e="T404" id="Seg_1971" s="T403">0.2.h:Th</ta>
            <ta e="T405" id="Seg_1972" s="T404">adv:Time</ta>
            <ta e="T408" id="Seg_1973" s="T407">0.3.h:A</ta>
            <ta e="T413" id="Seg_1974" s="T412">np:Th</ta>
            <ta e="T417" id="Seg_1975" s="T416">np.h:A</ta>
            <ta e="T421" id="Seg_1976" s="T420">np:Time</ta>
            <ta e="T427" id="Seg_1977" s="T426">0.2.h:Th</ta>
            <ta e="T428" id="Seg_1978" s="T427">pp:Time</ta>
            <ta e="T440" id="Seg_1979" s="T439">pro.h:P</ta>
            <ta e="T448" id="Seg_1980" s="T447">np.h:E</ta>
            <ta e="T450" id="Seg_1981" s="T449">pro.h:Th</ta>
            <ta e="T452" id="Seg_1982" s="T451">pro:Th</ta>
            <ta e="T454" id="Seg_1983" s="T453">np:P</ta>
            <ta e="T457" id="Seg_1984" s="T456">0.2.h:A</ta>
            <ta e="T461" id="Seg_1985" s="T460">0.2.h:A</ta>
            <ta e="T463" id="Seg_1986" s="T462">0.2.h:A</ta>
            <ta e="T465" id="Seg_1987" s="T464">pro.h:A</ta>
            <ta e="T467" id="Seg_1988" s="T466">pro.h:A</ta>
            <ta e="T469" id="Seg_1989" s="T468">pro.h:P</ta>
            <ta e="T471" id="Seg_1990" s="T470">np:P</ta>
            <ta e="T473" id="Seg_1991" s="T472">0.1.h:A</ta>
            <ta e="T475" id="Seg_1992" s="T474">np.h:A</ta>
            <ta e="T480" id="Seg_1993" s="T479">0.2.h:A</ta>
            <ta e="T484" id="Seg_1994" s="T483">0.2.h:Th</ta>
            <ta e="T487" id="Seg_1995" s="T486">0.2.h:A</ta>
            <ta e="T488" id="Seg_1996" s="T487">pro.h:P</ta>
            <ta e="T493" id="Seg_1997" s="T492">np.h:Th 0.2.h:Poss</ta>
            <ta e="T496" id="Seg_1998" s="T495">0.2.h:E</ta>
            <ta e="T499" id="Seg_1999" s="T498">np:Time</ta>
            <ta e="T502" id="Seg_2000" s="T501">np.h:A</ta>
            <ta e="T505" id="Seg_2001" s="T504">np:Th</ta>
            <ta e="T509" id="Seg_2002" s="T508">0.2.h:A</ta>
            <ta e="T510" id="Seg_2003" s="T509">adv:Time</ta>
            <ta e="T511" id="Seg_2004" s="T510">adv:L</ta>
            <ta e="T513" id="Seg_2005" s="T512">np.h:Th 0.1.h:Poss</ta>
            <ta e="T515" id="Seg_2006" s="T514">0.2.h:A</ta>
            <ta e="T518" id="Seg_2007" s="T517">np.h:Th 0.1.h:Poss</ta>
            <ta e="T521" id="Seg_2008" s="T520">np.h:Th 0.1.h:Poss</ta>
            <ta e="T523" id="Seg_2009" s="T522">np.h:Th 0.1.h:Poss</ta>
            <ta e="T525" id="Seg_2010" s="T524">pro.h:A</ta>
            <ta e="T527" id="Seg_2011" s="T526">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T366" id="Seg_2012" s="T365">np.h:S</ta>
            <ta e="T371" id="Seg_2013" s="T370">v:pred</ta>
            <ta e="T372" id="Seg_2014" s="T371">s:temp</ta>
            <ta e="T374" id="Seg_2015" s="T373">0.2.h:S v:pred</ta>
            <ta e="T376" id="Seg_2016" s="T375">np.h:O</ta>
            <ta e="T377" id="Seg_2017" s="T376">0.2.h:S v:pred</ta>
            <ta e="T379" id="Seg_2018" s="T378">0.2.h:S v:pred</ta>
            <ta e="T383" id="Seg_2019" s="T382">np.h:S</ta>
            <ta e="T387" id="Seg_2020" s="T386">np.h:S</ta>
            <ta e="T389" id="Seg_2021" s="T388">adj:pred</ta>
            <ta e="T390" id="Seg_2022" s="T389">0.3.h:S v:pred</ta>
            <ta e="T394" id="Seg_2023" s="T393">np.h:O</ta>
            <ta e="T396" id="Seg_2024" s="T395">0.1.h:S v:pred</ta>
            <ta e="T404" id="Seg_2025" s="T403">0.2.h:S v:pred</ta>
            <ta e="T408" id="Seg_2026" s="T407">0.3.h:S v:pred</ta>
            <ta e="T413" id="Seg_2027" s="T412">np:O</ta>
            <ta e="T415" id="Seg_2028" s="T414">v:pred</ta>
            <ta e="T417" id="Seg_2029" s="T416">np.h:S</ta>
            <ta e="T427" id="Seg_2030" s="T426">0.2.h:S v:pred</ta>
            <ta e="T443" id="Seg_2031" s="T439">s:temp</ta>
            <ta e="T445" id="Seg_2032" s="T443">s:temp</ta>
            <ta e="T448" id="Seg_2033" s="T447">np.h:S</ta>
            <ta e="T450" id="Seg_2034" s="T449">pro.h:O</ta>
            <ta e="T452" id="Seg_2035" s="T451">pro:O</ta>
            <ta e="T453" id="Seg_2036" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_2037" s="T453">np:O</ta>
            <ta e="T457" id="Seg_2038" s="T456">0.2.h:S v:pred</ta>
            <ta e="T461" id="Seg_2039" s="T460">0.2.h:S v:pred</ta>
            <ta e="T463" id="Seg_2040" s="T462">0.2.h:S v:pred</ta>
            <ta e="T465" id="Seg_2041" s="T464">pro.h:S</ta>
            <ta e="T466" id="Seg_2042" s="T465">v:pred</ta>
            <ta e="T470" id="Seg_2043" s="T466">s:temp</ta>
            <ta e="T471" id="Seg_2044" s="T470">np:O</ta>
            <ta e="T473" id="Seg_2045" s="T472">0.1.h:S v:pred</ta>
            <ta e="T475" id="Seg_2046" s="T474">np.h:S</ta>
            <ta e="T477" id="Seg_2047" s="T476">v:pred</ta>
            <ta e="T480" id="Seg_2048" s="T479">0.2.h:S v:pred</ta>
            <ta e="T484" id="Seg_2049" s="T483">0.2.h:S v:pred</ta>
            <ta e="T487" id="Seg_2050" s="T486">0.2.h:S v:pred</ta>
            <ta e="T491" id="Seg_2051" s="T487">s:temp</ta>
            <ta e="T493" id="Seg_2052" s="T492">np.h:O</ta>
            <ta e="T496" id="Seg_2053" s="T495">0.2.h:S v:pred</ta>
            <ta e="T506" id="Seg_2054" s="T500">s:compl</ta>
            <ta e="T509" id="Seg_2055" s="T508">0.2.h:S v:pred</ta>
            <ta e="T512" id="Seg_2056" s="T509">s:adv</ta>
            <ta e="T513" id="Seg_2057" s="T512">np.h:O</ta>
            <ta e="T515" id="Seg_2058" s="T514">0.2.h:S v:pred</ta>
            <ta e="T518" id="Seg_2059" s="T517">np.h:S</ta>
            <ta e="T521" id="Seg_2060" s="T520">np.h:S</ta>
            <ta e="T523" id="Seg_2061" s="T522">np.h:S</ta>
            <ta e="T524" id="Seg_2062" s="T523">v:pred</ta>
            <ta e="T525" id="Seg_2063" s="T524">pro.h:S</ta>
            <ta e="T526" id="Seg_2064" s="T525">s:adv</ta>
            <ta e="T527" id="Seg_2065" s="T526">0.3.h:O v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T366" id="Seg_2066" s="T365">new</ta>
            <ta e="T371" id="Seg_2067" s="T370">quot-sp</ta>
            <ta e="T375" id="Seg_2068" s="T374">new-Q</ta>
            <ta e="T376" id="Seg_2069" s="T375">new-Q</ta>
            <ta e="T377" id="Seg_2070" s="T376">0.giv-active-Q</ta>
            <ta e="T379" id="Seg_2071" s="T378">0.giv-active-Q</ta>
            <ta e="T383" id="Seg_2072" s="T382">new</ta>
            <ta e="T387" id="Seg_2073" s="T386">giv-active</ta>
            <ta e="T394" id="Seg_2074" s="T393">accs-sit-Q</ta>
            <ta e="T400" id="Seg_2075" s="T399">accs-sit</ta>
            <ta e="T402" id="Seg_2076" s="T401">giv-active</ta>
            <ta e="T408" id="Seg_2077" s="T407">0.giv-inactive quot-sp</ta>
            <ta e="T413" id="Seg_2078" s="T412">new-Q</ta>
            <ta e="T417" id="Seg_2079" s="T416">giv-inactive-Q</ta>
            <ta e="T419" id="Seg_2080" s="T418">giv-active-Q</ta>
            <ta e="T421" id="Seg_2081" s="T420">accs-gen-Q</ta>
            <ta e="T427" id="Seg_2082" s="T426">0.accs-aggr-Q</ta>
            <ta e="T440" id="Seg_2083" s="T439">giv-active-Q</ta>
            <ta e="T448" id="Seg_2084" s="T447">giv-inactive-Q</ta>
            <ta e="T450" id="Seg_2085" s="T449">giv-active-Q</ta>
            <ta e="T454" id="Seg_2086" s="T453">new-Q</ta>
            <ta e="T457" id="Seg_2087" s="T456">0.giv-inactive-Q</ta>
            <ta e="T461" id="Seg_2088" s="T460">0.giv-active-Q</ta>
            <ta e="T463" id="Seg_2089" s="T462">0.giv-active-Q</ta>
            <ta e="T467" id="Seg_2090" s="T466">giv-inactive</ta>
            <ta e="T469" id="Seg_2091" s="T468">accs-sit</ta>
            <ta e="T471" id="Seg_2092" s="T470">accs-gen</ta>
            <ta e="T475" id="Seg_2093" s="T474">new</ta>
            <ta e="T477" id="Seg_2094" s="T476">quot-sp</ta>
            <ta e="T480" id="Seg_2095" s="T479">0.giv-inactive-Q</ta>
            <ta e="T484" id="Seg_2096" s="T483">0.giv-inactive-Q</ta>
            <ta e="T487" id="Seg_2097" s="T486">0.giv-inactive-Q</ta>
            <ta e="T488" id="Seg_2098" s="T487">giv-active</ta>
            <ta e="T493" id="Seg_2099" s="T492">giv-active</ta>
            <ta e="T496" id="Seg_2100" s="T495">0.giv-inactive</ta>
            <ta e="T502" id="Seg_2101" s="T501">giv-inactive</ta>
            <ta e="T505" id="Seg_2102" s="T504">accs-sit</ta>
            <ta e="T509" id="Seg_2103" s="T508">0.giv-active</ta>
            <ta e="T513" id="Seg_2104" s="T512">new</ta>
            <ta e="T515" id="Seg_2105" s="T514">0.giv-active</ta>
            <ta e="T518" id="Seg_2106" s="T517">accs-sit</ta>
            <ta e="T521" id="Seg_2107" s="T520">accs-sit</ta>
            <ta e="T523" id="Seg_2108" s="T522">accs-gen</ta>
            <ta e="T525" id="Seg_2109" s="T524">giv-inactive</ta>
            <ta e="T527" id="Seg_2110" s="T526">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T391" id="Seg_2111" s="T390">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T515" id="Seg_2112" s="T513">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T375" id="Seg_2113" s="T365">Отец покойный раньше так говорил, когда жил: "Хорошо живите, ребята.</ta>
            <ta e="T377" id="Seg_2114" s="T375">Жену найдёте.</ta>
            <ta e="T379" id="Seg_2115" s="T377">Хорошо живите".</ta>
            <ta e="T383" id="Seg_2116" s="T379">Потом три невестки. </ta>
            <ta e="T390" id="Seg_2117" s="T383">Двух сыновей невесток: "Все они плохие", - [так] закончил.</ta>
            <ta e="T396" id="Seg_2118" s="T390">"Самую старшую невестку очень люблю".</ta>
            <ta e="T404" id="Seg_2119" s="T396">"Мол, старший сын, старшая дочь, хорошо живите."</ta>
            <ta e="T408" id="Seg_2120" s="T404">Потом так он поет:</ta>
            <ta e="T418" id="Seg_2121" s="T408">"Пусть будто хорошо, мол, семь посохов держит старшая дочь.</ta>
            <ta e="T427" id="Seg_2122" s="T418">После меня, мол, все хорошо живите.</ta>
            <ta e="T439" id="Seg_2123" s="T427">После меня, мол, (?).</ta>
            <ta e="T453" id="Seg_2124" s="T439">После того, как я умру, старший сын, мол, меня и это вспомнит.</ta>
            <ta e="T457" id="Seg_2125" s="T453">Водки много не пей.</ta>
            <ta e="T464" id="Seg_2126" s="T457">Понемногу, мол, пей и ешь.</ta>
            <ta e="T466" id="Seg_2127" s="T464">Вы пьете.</ta>
            <ta e="T473" id="Seg_2128" s="T466">Я, когда тебя воспитывал, тоже водку пил.</ta>
            <ta e="T478" id="Seg_2129" s="T473">Новые русские так, мол, говорили.</ta>
            <ta e="T487" id="Seg_2130" s="T478">Понемногу пей, так живи, много не пей.</ta>
            <ta e="T499" id="Seg_2131" s="T487">После того, как я умру, покойного отца вспоминай когда-нибудь.</ta>
            <ta e="T509" id="Seg_2132" s="T499">А что старшая дочь скажет, слушай".</ta>
            <ta e="T515" id="Seg_2133" s="T509">Тогда на этом заканчивая: Внуков не обижайте.</ta>
            <ta e="T524" id="Seg_2134" s="T515">Один внук, одна внучка, много внуков нет.</ta>
            <ta e="T527" id="Seg_2135" s="T524">Я сам, съездив, привез.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T375" id="Seg_2136" s="T365">My late father used to say, when he was still alive: "Live well, children.</ta>
            <ta e="T377" id="Seg_2137" s="T375">You'll find a wife.</ta>
            <ta e="T379" id="Seg_2138" s="T377">Live well".</ta>
            <ta e="T383" id="Seg_2139" s="T379">Then there were three daughters-in-law.</ta>
            <ta e="T390" id="Seg_2140" s="T383">About the wives of his two sons: "they are both bad", - [so] he finished.</ta>
            <ta e="T396" id="Seg_2141" s="T390">"I like the eldest daughter-in-law very much".</ta>
            <ta e="T404" id="Seg_2142" s="T396">"The eldest son, the eldest daughter-in-law should live well."</ta>
            <ta e="T408" id="Seg_2143" s="T404">Then he was singing so:</ta>
            <ta e="T418" id="Seg_2144" s="T408">"The elder daughter should keep seven crooks well.</ta>
            <ta e="T427" id="Seg_2145" s="T418">After me you should live all well.</ta>
            <ta e="T439" id="Seg_2146" s="T427">After me (?).</ta>
            <ta e="T453" id="Seg_2147" s="T439">And after I die, my elder son will remember me and this.</ta>
            <ta e="T457" id="Seg_2148" s="T453">Do not drink a lot of alcohol.</ta>
            <ta e="T464" id="Seg_2149" s="T457">You should eat and drink moderately.</ta>
            <ta e="T466" id="Seg_2150" s="T464">You drink.</ta>
            <ta e="T473" id="Seg_2151" s="T466">When I was bringing you up, I also used to drink spirit. </ta>
            <ta e="T478" id="Seg_2152" s="T473">New Russians used to say the following:</ta>
            <ta e="T487" id="Seg_2153" s="T478">Drink moderately, live so, do not drink much.</ta>
            <ta e="T499" id="Seg_2154" s="T487">After my death think some day of your passed father.</ta>
            <ta e="T509" id="Seg_2155" s="T499">And listen to what the elder daughter will say.</ta>
            <ta e="T515" id="Seg_2156" s="T509">Then finishing with it: Do not hurt my grandchildren.</ta>
            <ta e="T524" id="Seg_2157" s="T515">One grandson, one granddaughter, [I don't have] many grandchildren."</ta>
            <ta e="T527" id="Seg_2158" s="T524">I went away myself, brought.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T375" id="Seg_2159" s="T365">Mein seliger Vater sagte früher, als er noch lebte: "Lebt gut, Kinder.</ta>
            <ta e="T377" id="Seg_2160" s="T375">Ihr werdet eine Frau finden.</ta>
            <ta e="T379" id="Seg_2161" s="T377">Lebt gut."</ta>
            <ta e="T383" id="Seg_2162" s="T379">Dann [hatte er] drei Schwiegertöchter.</ta>
            <ta e="T390" id="Seg_2163" s="T383">Über die Frauen von zwei seiner Söhne: "Sie sind ganz schlecht", [so] beendete er.</ta>
            <ta e="T396" id="Seg_2164" s="T390">"Die älteste Schwiegertochter mag ich sehr."</ta>
            <ta e="T404" id="Seg_2165" s="T396">"Ältester Sohn, älteste Tochter, lebt gut."</ta>
            <ta e="T408" id="Seg_2166" s="T404">Dann singt er so:</ta>
            <ta e="T418" id="Seg_2167" s="T408">"Es wäre gut, sagt man, die ältere Tochter wird sieben Hirtenstäbe halten.</ta>
            <ta e="T427" id="Seg_2168" s="T418">Nach mir sollt ihr gut leben.</ta>
            <ta e="T439" id="Seg_2169" s="T427">Nach mir (?).</ta>
            <ta e="T453" id="Seg_2170" s="T439">Nachdem ich gestorben bin, wird sich mein ältester Sohn, an mich und daran erinnern.</ta>
            <ta e="T457" id="Seg_2171" s="T453">Trink nicht viel Alkohol.</ta>
            <ta e="T464" id="Seg_2172" s="T457">Du sollst mäßig essen und trinken.</ta>
            <ta e="T466" id="Seg_2173" s="T464">Ihr trinkt.</ta>
            <ta e="T473" id="Seg_2174" s="T466">Als ich dich großzog, trank ich auch Alkohol.</ta>
            <ta e="T478" id="Seg_2175" s="T473">Die neuen Russen sagten dazu:</ta>
            <ta e="T487" id="Seg_2176" s="T478">"Trink mäßig, leb so, trink nicht viel."</ta>
            <ta e="T499" id="Seg_2177" s="T487">Nachdem ich gestorben bin, denk irgendwann an deinen seligen Vater.</ta>
            <ta e="T509" id="Seg_2178" s="T499">Und hör auf das, was die älteste Tochter sagt."</ta>
            <ta e="T515" id="Seg_2179" s="T509">Dann damit aufhörend: "Kränkt meine Enkel nicht.</ta>
            <ta e="T524" id="Seg_2180" s="T515">Einen Enkel, eine Enkelin, viele Enkelkinder [habe ich] nicht."</ta>
            <ta e="T527" id="Seg_2181" s="T524">Ich bin selber weggegangen und habe [sie] geholt.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T375" id="Seg_2182" s="T365">отец покойный раньше так говорил когда жил хорошо живите ребята</ta>
            <ta e="T377" id="Seg_2183" s="T375">жену найдете</ta>
            <ta e="T379" id="Seg_2184" s="T377">хорошо живите</ta>
            <ta e="T383" id="Seg_2185" s="T379">потом трое невесток </ta>
            <ta e="T390" id="Seg_2186" s="T383">двое сыновей невесток всех плохие они</ta>
            <ta e="T396" id="Seg_2187" s="T390">самый старш. невесту очень люблю</ta>
            <ta e="T404" id="Seg_2188" s="T396">большой сын большая дочь хорошо живите</ta>
            <ta e="T408" id="Seg_2189" s="T404">потом так он песни поет</ta>
            <ta e="T418" id="Seg_2190" s="T408">хорошо будто бы 7 посохов будто бы пусть держит старшая дочь</ta>
            <ta e="T427" id="Seg_2191" s="T418">я мое задний днях [когда я умру] хорошо все будто бы живите</ta>
            <ta e="T439" id="Seg_2192" s="T427">моей задней (жизни) когда умру после -его он что-то будто подерживал</ta>
            <ta e="T453" id="Seg_2193" s="T439">я когда умру после после когда помру старший сын будто меня вспомнишь</ta>
            <ta e="T457" id="Seg_2194" s="T453">вина много не пей</ta>
            <ta e="T464" id="Seg_2195" s="T457">тихонько будто выпивай ты тихонько покушай</ta>
            <ta e="T466" id="Seg_2196" s="T464">вы пьете</ta>
            <ta e="T473" id="Seg_2197" s="T466">я когда тебя воспитал вино тоже выпивал</ta>
            <ta e="T478" id="Seg_2198" s="T473">новый русский так рассказывали будто</ta>
            <ta e="T487" id="Seg_2199" s="T478">тихонько [медленно] выпивай так поживай много не выпивай</ta>
            <ta e="T499" id="Seg_2200" s="T487">я когда умру после покойного отца вспоминай когда-нибудь [в какой-ниб. день]</ta>
            <ta e="T509" id="Seg_2201" s="T499">а старшую дочь что скажет подчиняйся</ta>
            <ta e="T515" id="Seg_2202" s="T509">но на этом кончаю внучат внучат</ta>
            <ta e="T524" id="Seg_2203" s="T515">один мужчина внук одна женская внучка много внуков нет</ta>
            <ta e="T527" id="Seg_2204" s="T524">сам съездив привез</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T383" id="Seg_2205" s="T379">[OSV:] "oqqa" has here probably another meaning.</ta>
            <ta e="T390" id="Seg_2206" s="T383">[OSV:] unclear syntactic structure of the sentence. </ta>
            <ta e="T439" id="Seg_2207" s="T427">[OSV:] unclear translation of the sentence.</ta>
            <ta e="T466" id="Seg_2208" s="T464">[OSV:] here the form "ütɨmpɔːlɨt" (drink-HAB-2PL) would be grammatically correct.</ta>
            <ta e="T524" id="Seg_2209" s="T515">[OSV:] not sure in glossing of the form "qumtɨlʼ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T457" id="Seg_2210" s="T453">отец ему завещает</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
