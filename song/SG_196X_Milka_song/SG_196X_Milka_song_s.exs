<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SG_196X_Milka_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SG_196X_Milka_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">15</ud-information>
            <ud-information attribute-name="# HIAT:w">12</ud-information>
            <ud-information attribute-name="# e">12</ud-information>
            <ud-information attribute-name="# HIAT:u">2</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SG">
            <abbreviation>SG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T12" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">mat</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">wečorkan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">čaǯik</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">čаʒik</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">čaǯɨɣak</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">čigetɨhak</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">meka</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">kadɨhat</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">Milka</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">nʼet</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">čwese</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">paradičʼihak</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T12" id="Seg_44" n="sc" s="T0">
               <ts e="T1" id="Seg_46" n="e" s="T0">mat </ts>
               <ts e="T2" id="Seg_48" n="e" s="T1">wečorkan </ts>
               <ts e="T3" id="Seg_50" n="e" s="T2">čaǯik </ts>
               <ts e="T4" id="Seg_52" n="e" s="T3">čаʒik </ts>
               <ts e="T5" id="Seg_54" n="e" s="T4">čaǯɨɣak </ts>
               <ts e="T6" id="Seg_56" n="e" s="T5">čigetɨhak. </ts>
               <ts e="T7" id="Seg_58" n="e" s="T6">meka </ts>
               <ts e="T8" id="Seg_60" n="e" s="T7">kadɨhat </ts>
               <ts e="T9" id="Seg_62" n="e" s="T8">Milka </ts>
               <ts e="T10" id="Seg_64" n="e" s="T9">nʼet, </ts>
               <ts e="T11" id="Seg_66" n="e" s="T10">čwese </ts>
               <ts e="T12" id="Seg_68" n="e" s="T11">paradičʼihak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_69" s="T0">SG_196X_Milka_song.001 (001.001)</ta>
            <ta e="T12" id="Seg_70" s="T6">SG_196X_Milka_song.002 (001.002)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_71" s="T0">мат ве′чоркан ′чаджик чажик / чаджыɣак чигетыhак.</ta>
            <ta e="T12" id="Seg_72" s="T6">мека кадыhат милка нет / ′чвесе парадичʼиhак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl" />
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_73" s="T0">mat wečorkan čaǯik čаʒik/ čaǯɨɣak čigetɨhak. </ta>
            <ta e="T12" id="Seg_74" s="T6">meka kadɨhat Milka nʼet, čwese paradičʼihak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_75" s="T0">mat</ta>
            <ta e="T2" id="Seg_76" s="T1">wečorka-n</ta>
            <ta e="T3" id="Seg_77" s="T2">čaǯi-k</ta>
            <ta e="T4" id="Seg_78" s="T3">čаʒi-k</ta>
            <ta e="T5" id="Seg_79" s="T4">čaǯɨ-ɣa-k</ta>
            <ta e="T6" id="Seg_80" s="T5">čigetɨ-ha-k</ta>
            <ta e="T7" id="Seg_81" s="T6">meka</ta>
            <ta e="T8" id="Seg_82" s="T7">kadɨ-ha-t</ta>
            <ta e="T9" id="Seg_83" s="T8">Milka</ta>
            <ta e="T10" id="Seg_84" s="T9">nʼet</ta>
            <ta e="T11" id="Seg_85" s="T10">čwese</ta>
            <ta e="T12" id="Seg_86" s="T11">para-di-čʼi-ha-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_87" s="T0">man</ta>
            <ta e="T2" id="Seg_88" s="T1">wečʼorka-n</ta>
            <ta e="T3" id="Seg_89" s="T2">čaːǯɨ-k</ta>
            <ta e="T4" id="Seg_90" s="T3">čaːǯɨ-k</ta>
            <ta e="T5" id="Seg_91" s="T4">čaːǯɨ-ŋɨ-k</ta>
            <ta e="T6" id="Seg_92" s="T5">čigetɨ-ŋɨ-k</ta>
            <ta e="T7" id="Seg_93" s="T6">mäkkä</ta>
            <ta e="T8" id="Seg_94" s="T7">kadɨ-ŋɨ-dət</ta>
            <ta e="T9" id="Seg_95" s="T8">Milka</ta>
            <ta e="T10" id="Seg_96" s="T9">nʼet</ta>
            <ta e="T11" id="Seg_97" s="T10">čwesse</ta>
            <ta e="T12" id="Seg_98" s="T11">para-dʼi-če-ŋɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_99" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_100" s="T1">evening.meeting-ILL</ta>
            <ta e="T3" id="Seg_101" s="T2">go-1SG.S</ta>
            <ta e="T4" id="Seg_102" s="T3">go-1SG.S</ta>
            <ta e="T5" id="Seg_103" s="T4">go-CO-1SG.S</ta>
            <ta e="T6" id="Seg_104" s="T5">hurry.up-CO-1SG.S</ta>
            <ta e="T7" id="Seg_105" s="T6">I.ALL</ta>
            <ta e="T8" id="Seg_106" s="T7">say-CO-3PL</ta>
            <ta e="T9" id="Seg_107" s="T8">Milka.[NOM]</ta>
            <ta e="T10" id="Seg_108" s="T9">NEG</ta>
            <ta e="T11" id="Seg_109" s="T10">backward</ta>
            <ta e="T12" id="Seg_110" s="T11">return-DRV-DRV-CO-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_111" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_112" s="T1">вечёрка-ILL</ta>
            <ta e="T3" id="Seg_113" s="T2">идти-1SG.S</ta>
            <ta e="T4" id="Seg_114" s="T3">идти-1SG.S</ta>
            <ta e="T5" id="Seg_115" s="T4">идти-CO-1SG.S</ta>
            <ta e="T6" id="Seg_116" s="T5">торопиться-CO-1SG.S</ta>
            <ta e="T7" id="Seg_117" s="T6">я.ALL</ta>
            <ta e="T8" id="Seg_118" s="T7">сказать-CO-3PL</ta>
            <ta e="T9" id="Seg_119" s="T8">Милка.[NOM]</ta>
            <ta e="T10" id="Seg_120" s="T9">NEG</ta>
            <ta e="T11" id="Seg_121" s="T10">назад</ta>
            <ta e="T12" id="Seg_122" s="T11">вернуться-DRV-DRV-CO-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_123" s="T0">pers-n:case</ta>
            <ta e="T2" id="Seg_124" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_125" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_126" s="T3">v-v:pn</ta>
            <ta e="T5" id="Seg_127" s="T4">v-v:ins-v:pn</ta>
            <ta e="T6" id="Seg_128" s="T5">v-v:ins-v:pn</ta>
            <ta e="T7" id="Seg_129" s="T6">pers</ta>
            <ta e="T8" id="Seg_130" s="T7">v-v:ins-v:pn</ta>
            <ta e="T9" id="Seg_131" s="T8">nprop-n:case</ta>
            <ta e="T10" id="Seg_132" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_133" s="T10">adv</ta>
            <ta e="T12" id="Seg_134" s="T11">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_135" s="T0">pers</ta>
            <ta e="T2" id="Seg_136" s="T1">n</ta>
            <ta e="T3" id="Seg_137" s="T2">v</ta>
            <ta e="T4" id="Seg_138" s="T3">v</ta>
            <ta e="T5" id="Seg_139" s="T4">v</ta>
            <ta e="T6" id="Seg_140" s="T5">v</ta>
            <ta e="T7" id="Seg_141" s="T6">pers</ta>
            <ta e="T8" id="Seg_142" s="T7">v</ta>
            <ta e="T9" id="Seg_143" s="T8">nprop</ta>
            <ta e="T10" id="Seg_144" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_145" s="T10">adv</ta>
            <ta e="T12" id="Seg_146" s="T11">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_147" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_148" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_149" s="T3">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_150" s="T4">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_151" s="T5">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_152" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_153" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_154" s="T9">ptcl:pred</ta>
            <ta e="T12" id="Seg_155" s="T11">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_156" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_157" s="T1">np:G</ta>
            <ta e="T4" id="Seg_158" s="T3">0.1.h:A</ta>
            <ta e="T5" id="Seg_159" s="T4">0.1.h:A</ta>
            <ta e="T6" id="Seg_160" s="T5">0.1.h:A</ta>
            <ta e="T7" id="Seg_161" s="T6">pro.h:R</ta>
            <ta e="T8" id="Seg_162" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_163" s="T8">np.h:Th</ta>
            <ta e="T11" id="Seg_164" s="T10">adv:G</ta>
            <ta e="T12" id="Seg_165" s="T11">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_166" s="T9">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_167" s="T0">Я шел на вечёрку, торопился.</ta>
            <ta e="T12" id="Seg_168" s="T6">Мне сказали, что Милки нет, она назад вернулась.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_169" s="T0">I went to the evening meeting, I went, I went, I was in a hurry.</ta>
            <ta e="T12" id="Seg_170" s="T6">I was told Milka was not there, she went back.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_171" s="T0">Ich ging, ging zu dem Abendtreffen, ich ging, ich ging, ich eilte.</ta>
            <ta e="T12" id="Seg_172" s="T6">Sie sagten mir, Milka ist nicht da, sie ging zurück.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_173" s="T0">я шел я шел идя торопился.</ta>
            <ta e="T12" id="Seg_174" s="T6">мне сказали милки нет назад воротился</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
