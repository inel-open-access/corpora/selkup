<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>TAN_1963_ILostMyCap_song</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">TAN_1963_ILostMyCap_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">20</ud-information>
            <ud-information attribute-name="# HIAT:w">10</ud-information>
            <ud-information attribute-name="# e">10</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="TAN">
            <abbreviation>TAN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="TAN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T11" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">qänɨŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">barɣɨn</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">palʼdʼüzaŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip">/</nts>
                  <nts id="Seg_12" n="HIAT:ip">/</nts>
                  <nts id="Seg_13" n="HIAT:ip">,</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">üːkugaw</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ürčuzau</ts>
                  <nts id="Seg_20" n="HIAT:ip">/</nts>
                  <nts id="Seg_21" n="HIAT:ip">/</nts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_25" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">tʼepʼer</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">qaim</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">mekunǯaŋ</ts>
                  <nts id="Seg_34" n="HIAT:ip">/</nts>
                  <nts id="Seg_35" n="HIAT:ip">/</nts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_39" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">üqugalɨq</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">polʼdʼuǯaŋ</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T11" id="Seg_47" n="sc" s="T1">
               <ts e="T2" id="Seg_49" n="e" s="T1">qänɨŋ </ts>
               <ts e="T3" id="Seg_51" n="e" s="T2">barɣɨn </ts>
               <ts e="T4" id="Seg_53" n="e" s="T3">palʼdʼüzaŋ//, </ts>
               <ts e="T5" id="Seg_55" n="e" s="T4">üːkugaw </ts>
               <ts e="T6" id="Seg_57" n="e" s="T5">ürčuzau//. </ts>
               <ts e="T7" id="Seg_59" n="e" s="T6">tʼepʼer </ts>
               <ts e="T8" id="Seg_61" n="e" s="T7">qaim </ts>
               <ts e="T9" id="Seg_63" n="e" s="T8">mekunǯaŋ//. </ts>
               <ts e="T10" id="Seg_65" n="e" s="T9">üqugalɨq </ts>
               <ts e="T11" id="Seg_67" n="e" s="T10">polʼdʼuǯaŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_68" s="T1">TAN_1963_ILostMyCap_song.001 (001.001)</ta>
            <ta e="T9" id="Seg_69" s="T6">TAN_1963_ILostMyCap_song.002 (001.003)</ta>
            <ta e="T11" id="Seg_70" s="T9">TAN_1963_ILostMyCap_song.003 (001.004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_71" s="T1">′kӓның ′барɣын палʼдʼӱ′заң //,ӱ̄ку′гаw(у) ӱртшу′зау(ф) //.</ta>
            <ta e="T9" id="Seg_72" s="T6">тʼепʼер ′kаим ′мекунджаң //.</ta>
            <ta e="T11" id="Seg_73" s="T9">ӱку′галык полʼдʼу′джаң.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_74" s="T1">qänɨŋ barɣɨn palʼdʼüzaŋ //,üːkugaw(u) ürtšuzau(f) //.</ta>
            <ta e="T9" id="Seg_75" s="T6">tʼepʼer qaim mekunǯaŋ //.</ta>
            <ta e="T11" id="Seg_76" s="T9">üqugalɨq polʼdʼuǯaŋ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_77" s="T1">qänɨŋ barɣɨn palʼdʼüzaŋ//, üːkugaw ürčuzau//. </ta>
            <ta e="T9" id="Seg_78" s="T6">tʼepʼer qaim mekunǯaŋ//. </ta>
            <ta e="T11" id="Seg_79" s="T9">üqugalɨq polʼdʼuǯaŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_80" s="T1">qä-nɨŋ</ta>
            <ta e="T3" id="Seg_81" s="T2">bar-ɣɨn</ta>
            <ta e="T4" id="Seg_82" s="T3">palʼdʼü-za-ŋ</ta>
            <ta e="T5" id="Seg_83" s="T4">üːku-ga-w</ta>
            <ta e="T6" id="Seg_84" s="T5">ür-ču-za-u</ta>
            <ta e="T7" id="Seg_85" s="T6">tʼepʼer</ta>
            <ta e="T8" id="Seg_86" s="T7">qai-m</ta>
            <ta e="T9" id="Seg_87" s="T8">me-ku-nǯa-ŋ</ta>
            <ta e="T10" id="Seg_88" s="T9">üqu-galɨ-q</ta>
            <ta e="T11" id="Seg_89" s="T10">polʼdʼu-ǯa-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_90" s="T1">qəː-%%</ta>
            <ta e="T3" id="Seg_91" s="T2">par-qən</ta>
            <ta e="T4" id="Seg_92" s="T3">paldʼu-sɨ-ŋ</ta>
            <ta e="T5" id="Seg_93" s="T4">üku-ka-m</ta>
            <ta e="T6" id="Seg_94" s="T5">ür-ču-sɨ-m</ta>
            <ta e="T7" id="Seg_95" s="T6">tʼipeːrʼ</ta>
            <ta e="T8" id="Seg_96" s="T7">qaj-m</ta>
            <ta e="T9" id="Seg_97" s="T8">meː-ku-nǯɨ-ŋ</ta>
            <ta e="T10" id="Seg_98" s="T9">üku-gaːlɨ-k</ta>
            <ta e="T11" id="Seg_99" s="T10">paldʼu-nǯɨ-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_100" s="T1">steep.bank-%%</ta>
            <ta e="T3" id="Seg_101" s="T2">top-LOC</ta>
            <ta e="T4" id="Seg_102" s="T3">go-PST-1SG.S</ta>
            <ta e="T5" id="Seg_103" s="T4">cap-DIM-ACC</ta>
            <ta e="T6" id="Seg_104" s="T5">lost-DRV-PST-1SG.O</ta>
            <ta e="T7" id="Seg_105" s="T6">now</ta>
            <ta e="T8" id="Seg_106" s="T7">what-ACC</ta>
            <ta e="T9" id="Seg_107" s="T8">do-HAB-FUT-1SG.S</ta>
            <ta e="T10" id="Seg_108" s="T9">cap-CAR-ADVZ</ta>
            <ta e="T11" id="Seg_109" s="T10">go-FUT-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_110" s="T1">крутой.берег-%%</ta>
            <ta e="T3" id="Seg_111" s="T2">верх-LOC</ta>
            <ta e="T4" id="Seg_112" s="T3">ходить-PST-1SG.S</ta>
            <ta e="T5" id="Seg_113" s="T4">шапка-DIM-ACC</ta>
            <ta e="T6" id="Seg_114" s="T5">потерять-DRV-PST-1SG.O</ta>
            <ta e="T7" id="Seg_115" s="T6">теперь</ta>
            <ta e="T8" id="Seg_116" s="T7">что-ACC</ta>
            <ta e="T9" id="Seg_117" s="T8">делать-HAB-FUT-1SG.S</ta>
            <ta e="T10" id="Seg_118" s="T9">шапка-CAR-ADVZ</ta>
            <ta e="T11" id="Seg_119" s="T10">ходить-FUT-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_120" s="T1">n</ta>
            <ta e="T3" id="Seg_121" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_122" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_123" s="T4">n-n&gt;n-n:case</ta>
            <ta e="T6" id="Seg_124" s="T5">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_125" s="T6">adv</ta>
            <ta e="T8" id="Seg_126" s="T7">interrog-n:case</ta>
            <ta e="T9" id="Seg_127" s="T8">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_128" s="T9">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T11" id="Seg_129" s="T10">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T3" id="Seg_130" s="T2">n</ta>
            <ta e="T4" id="Seg_131" s="T3">v</ta>
            <ta e="T5" id="Seg_132" s="T4">n</ta>
            <ta e="T6" id="Seg_133" s="T5">v</ta>
            <ta e="T7" id="Seg_134" s="T6">adv</ta>
            <ta e="T8" id="Seg_135" s="T7">pro</ta>
            <ta e="T9" id="Seg_136" s="T8">v</ta>
            <ta e="T10" id="Seg_137" s="T9">adv</ta>
            <ta e="T11" id="Seg_138" s="T10">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_139" s="T2">np:L</ta>
            <ta e="T4" id="Seg_140" s="T3">0.1.h:A</ta>
            <ta e="T5" id="Seg_141" s="T4">np:Th</ta>
            <ta e="T6" id="Seg_142" s="T5">0.1.h:P</ta>
            <ta e="T7" id="Seg_143" s="T6">adv:Time</ta>
            <ta e="T8" id="Seg_144" s="T7">pro:Th</ta>
            <ta e="T9" id="Seg_145" s="T8">0.1.h:A</ta>
            <ta e="T11" id="Seg_146" s="T10">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_147" s="T3">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_148" s="T4">np:O</ta>
            <ta e="T6" id="Seg_149" s="T5">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_150" s="T7">pro:O</ta>
            <ta e="T9" id="Seg_151" s="T8">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_152" s="T10">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_153" s="T6">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_154" s="T1">I was walking on the hill, I lost my cap.</ta>
            <ta e="T9" id="Seg_155" s="T6">What am I going to do now?</ta>
            <ta e="T11" id="Seg_156" s="T9">I'll be walking without a cap.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_157" s="T1">Ich lief auf dem Hügel,Ich verlor meine Mütze.</ta>
            <ta e="T9" id="Seg_158" s="T6">Was mache ich jetzt?</ta>
            <ta e="T11" id="Seg_159" s="T9">Ich werde ohne Mütze laufen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_160" s="T1">По горе ходила,шапку потеряла.</ta>
            <ta e="T9" id="Seg_161" s="T6">Теперь что я буду делать.</ta>
            <ta e="T11" id="Seg_162" s="T9">Без шапки буду ходить.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_163" s="T1">на горе ходилашапку потеряла</ta>
            <ta e="T9" id="Seg_164" s="T6">теперь что я буду делать</ta>
            <ta e="T11" id="Seg_165" s="T9">без шапки буду ходить</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
