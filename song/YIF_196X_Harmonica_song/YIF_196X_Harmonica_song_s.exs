<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_196X_Harmonica_song</transcription-name>
         <referenced-file url="YIF_196X_Harmonica_song.wav" />
         <referenced-file url="YIF_196X_Harmonica_song.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_Harmonica_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">38</ud-information>
            <ud-information attribute-name="# HIAT:w">13</ud-information>
            <ud-information attribute-name="# e">17</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">1</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.672" type="appl" />
         <tli id="T2" time="1.1856" type="appl" />
         <tli id="T3" time="1.6992000000000003" type="appl" />
         <tli id="T4" time="2.2128" type="appl" />
         <tli id="T5" time="2.7264000000000004" type="appl" />
         <tli id="T6" time="3.24" type="appl" />
         <tli id="T8" time="3.26" type="appl" />
         <tli id="T9" time="3.9073333333333333" type="appl" />
         <tli id="T10" time="4.554666666666667" type="appl" />
         <tli id="T12" time="5.336" type="appl" />
         <tli id="T13" time="5.762833333333334" type="appl" />
         <tli id="T14" time="6.189666666666667" type="appl" />
         <tli id="T15" time="6.6165" type="appl" />
         <tli id="T16" time="7.043333333333334" type="appl" />
         <tli id="T17" time="7.4701666666666675" type="appl" />
         <tli id="T19" time="7.928" type="appl" />
         <tli id="T20" time="8.657333333333334" type="appl" />
         <tli id="T21" time="9.386666666666667" type="appl" />
         <tli id="T22" time="10.1" />
         <tli id="T0" time="10.116" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T22" id="Seg_0" n="sc" s="T1">
               <ts e="T22" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <nts id="Seg_4" n="HIAT:ip">(</nts>
                  <ats e="T2" id="Seg_5" n="HIAT:non-pho" s="T1">YIF:</ats>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip">)</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Mat</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">karmonep</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">nakram</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_19" n="HIAT:w" s="T5">ota</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <nts id="Seg_22" n="HIAT:ip">(</nts>
                  <ats e="T9" id="Seg_23" n="HIAT:non-pho" s="T8">YIF:</ats>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip">)</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">Karmonistom</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_31" n="HIAT:w" s="T10">urowo</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_33" n="HIAT:ip">(</nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <ats e="T13" id="Seg_35" n="HIAT:non-pho" s="T12">NP:</ats>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_40" n="HIAT:w" s="T13">Mat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_43" n="HIAT:w" s="T14">karmoškap</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_45" n="HIAT:ip">—</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_48" n="HIAT:w" s="T15">kopte</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_51" n="HIAT:w" s="T16">nu</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_54" n="HIAT:w" s="T17">ʒe</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_56" n="HIAT:ip">(</nts>
                  <nts id="Seg_57" n="HIAT:ip">(</nts>
                  <ats e="T20" id="Seg_58" n="HIAT:non-pho" s="T19">NP:</ats>
                  <nts id="Seg_59" n="HIAT:ip">)</nts>
                  <nts id="Seg_60" n="HIAT:ip">)</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_63" n="HIAT:w" s="T20">Karmonistam</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_67" n="HIAT:w" s="T21">nadernak</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T22" id="Seg_71" n="sc" s="T1">
               <ts e="T2" id="Seg_73" n="e" s="T1">((YIF:)) </ts>
               <ts e="T3" id="Seg_75" n="e" s="T2">Mat </ts>
               <ts e="T4" id="Seg_77" n="e" s="T3">karmonep </ts>
               <ts e="T5" id="Seg_79" n="e" s="T4">nakram </ts>
               <ts e="T8" id="Seg_81" n="e" s="T5">ota </ts>
               <ts e="T9" id="Seg_83" n="e" s="T8">((YIF:)) </ts>
               <ts e="T10" id="Seg_85" n="e" s="T9">Karmonistom </ts>
               <ts e="T12" id="Seg_87" n="e" s="T10">urowo </ts>
               <ts e="T13" id="Seg_89" n="e" s="T12">((NP:)) </ts>
               <ts e="T14" id="Seg_91" n="e" s="T13">Mat </ts>
               <ts e="T15" id="Seg_93" n="e" s="T14">karmoškap — </ts>
               <ts e="T16" id="Seg_95" n="e" s="T15">kopte </ts>
               <ts e="T17" id="Seg_97" n="e" s="T16">nu </ts>
               <ts e="T19" id="Seg_99" n="e" s="T17">ʒe </ts>
               <ts e="T20" id="Seg_101" n="e" s="T19">((NP:)) </ts>
               <ts e="T21" id="Seg_103" n="e" s="T20">Karmonistam </ts>
               <ts e="T22" id="Seg_105" n="e" s="T21">(nadernak). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T22" id="Seg_106" s="T1">YIF_196X_Harmonica_song.001 (001)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_107" s="T1">Мат кармо́нем накромо́та</ta>
            <ta e="T12" id="Seg_108" s="T8">Кармони́стом у́рово</ta>
            <ta e="T19" id="Seg_109" s="T12">Мат кармо́шкам — ко́птэ ну́жэн</ta>
            <ta e="T22" id="Seg_110" s="T19">Кармони́стам на́дэрнак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T8" id="Seg_111" s="T1">Mat karmonem nakromota</ta>
            <ta e="T12" id="Seg_112" s="T8">Karmonistom urowo</ta>
            <ta e="T19" id="Seg_113" s="T12">Mat karmoškam — kopte nuʒen</ta>
            <ta e="T22" id="Seg_114" s="T19">Karmonistam nadernak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_115" s="T1">((YIF:)) Mat karmonep nakram ota </ta>
            <ta e="T12" id="Seg_116" s="T8">((YIF:)) Karmonistom urowo </ta>
            <ta e="T19" id="Seg_117" s="T12">((NP:)) Mat karmoškap — kopte nu ʒe </ta>
            <ta e="T22" id="Seg_118" s="T19">((NP:)) Karmonistam (nadernak). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T3" id="Seg_119" s="T2">mat</ta>
            <ta e="T4" id="Seg_120" s="T3">karmon-e-p</ta>
            <ta e="T5" id="Seg_121" s="T4">nakra-m</ta>
            <ta e="T8" id="Seg_122" s="T5">ota</ta>
            <ta e="T10" id="Seg_123" s="T9">karmonistom</ta>
            <ta e="T12" id="Seg_124" s="T10">urowo</ta>
            <ta e="T14" id="Seg_125" s="T13">mat</ta>
            <ta e="T15" id="Seg_126" s="T14">karmoška-p</ta>
            <ta e="T16" id="Seg_127" s="T15">kopte</ta>
            <ta e="T17" id="Seg_128" s="T16">nu</ta>
            <ta e="T19" id="Seg_129" s="T17">ʒe</ta>
            <ta e="T21" id="Seg_130" s="T20">karmonist-a-m</ta>
            <ta e="T22" id="Seg_131" s="T21">nader-na-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T3" id="Seg_132" s="T2">man</ta>
            <ta e="T4" id="Seg_133" s="T3">karmon-ɨ-p</ta>
            <ta e="T5" id="Seg_134" s="T4">nakra-m</ta>
            <ta e="T8" id="Seg_135" s="T5">udɨ</ta>
            <ta e="T10" id="Seg_136" s="T9">karmonistom</ta>
            <ta e="T12" id="Seg_137" s="T10">urowo</ta>
            <ta e="T14" id="Seg_138" s="T13">man</ta>
            <ta e="T15" id="Seg_139" s="T14">karmoška-p</ta>
            <ta e="T16" id="Seg_140" s="T15">kopto</ta>
            <ta e="T17" id="Seg_141" s="T16">nu</ta>
            <ta e="T19" id="Seg_142" s="T17">ʒе</ta>
            <ta e="T21" id="Seg_143" s="T20">karmonist-ɨ-m</ta>
            <ta e="T22" id="Seg_144" s="T21">nadɨr-ŋɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T3" id="Seg_145" s="T2">I.NOM</ta>
            <ta e="T4" id="Seg_146" s="T3">concertina-EP-ACC</ta>
            <ta e="T5" id="Seg_147" s="T4">%%-1SG.O</ta>
            <ta e="T8" id="Seg_148" s="T5">hand.[NOM]</ta>
            <ta e="T10" id="Seg_149" s="T9">accordionist.INSTR</ta>
            <ta e="T12" id="Seg_150" s="T10">round.dance.[NOM]</ta>
            <ta e="T14" id="Seg_151" s="T13">I.NOM</ta>
            <ta e="T15" id="Seg_152" s="T14">concertina.DIM-ACC</ta>
            <ta e="T16" id="Seg_153" s="T15">bed</ta>
            <ta e="T17" id="Seg_154" s="T16">well</ta>
            <ta e="T19" id="Seg_155" s="T17">just</ta>
            <ta e="T21" id="Seg_156" s="T20">accordionist-EP-ACC</ta>
            <ta e="T22" id="Seg_157" s="T21">love-CO-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T3" id="Seg_158" s="T2">я.NOM</ta>
            <ta e="T4" id="Seg_159" s="T3">гармонь-EP-ACC</ta>
            <ta e="T5" id="Seg_160" s="T4">%%-1SG.O</ta>
            <ta e="T8" id="Seg_161" s="T5">рука.[NOM]</ta>
            <ta e="T10" id="Seg_162" s="T9">гармонист.INSTR</ta>
            <ta e="T12" id="Seg_163" s="T10">хоровод.[NOM]</ta>
            <ta e="T14" id="Seg_164" s="T13">я.NOM</ta>
            <ta e="T15" id="Seg_165" s="T14">гармошка.DIM-ACC</ta>
            <ta e="T16" id="Seg_166" s="T15">кровать</ta>
            <ta e="T17" id="Seg_167" s="T16">ну</ta>
            <ta e="T19" id="Seg_168" s="T17">же</ta>
            <ta e="T21" id="Seg_169" s="T20">гармонист-EP-ACC</ta>
            <ta e="T22" id="Seg_170" s="T21">любит-CO-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T3" id="Seg_171" s="T2">pers</ta>
            <ta e="T4" id="Seg_172" s="T3">n-n:ins-n:case</ta>
            <ta e="T5" id="Seg_173" s="T4">v-v:pn</ta>
            <ta e="T8" id="Seg_174" s="T5">n.[n:case]</ta>
            <ta e="T10" id="Seg_175" s="T9">n</ta>
            <ta e="T12" id="Seg_176" s="T10">n.[n:case]</ta>
            <ta e="T14" id="Seg_177" s="T13">pers</ta>
            <ta e="T15" id="Seg_178" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_179" s="T15">n</ta>
            <ta e="T17" id="Seg_180" s="T16">ptcl</ta>
            <ta e="T19" id="Seg_181" s="T17">ptcl</ta>
            <ta e="T21" id="Seg_182" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_183" s="T21">v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T3" id="Seg_184" s="T2">pers</ta>
            <ta e="T4" id="Seg_185" s="T3">n</ta>
            <ta e="T5" id="Seg_186" s="T4">v</ta>
            <ta e="T8" id="Seg_187" s="T5">n</ta>
            <ta e="T10" id="Seg_188" s="T9">n</ta>
            <ta e="T12" id="Seg_189" s="T10">n</ta>
            <ta e="T14" id="Seg_190" s="T13">pers</ta>
            <ta e="T15" id="Seg_191" s="T14">n</ta>
            <ta e="T16" id="Seg_192" s="T15">n</ta>
            <ta e="T17" id="Seg_193" s="T16">ptcl</ta>
            <ta e="T19" id="Seg_194" s="T17">ptcl</ta>
            <ta e="T21" id="Seg_195" s="T20">n</ta>
            <ta e="T22" id="Seg_196" s="T21">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_197" s="T2">pro.h:S</ta>
            <ta e="T4" id="Seg_198" s="T3">np:O</ta>
            <ta e="T5" id="Seg_199" s="T4">v:pred</ta>
            <ta e="T14" id="Seg_200" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_201" s="T14">np:O</ta>
            <ta e="T21" id="Seg_202" s="T20">np.h:O</ta>
            <ta e="T22" id="Seg_203" s="T21">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_204" s="T2">pro.h:A</ta>
            <ta e="T4" id="Seg_205" s="T3">np:Th</ta>
            <ta e="T14" id="Seg_206" s="T13">pro.h:A</ta>
            <ta e="T15" id="Seg_207" s="T14">np:Th</ta>
            <ta e="T21" id="Seg_208" s="T20">np.h:Th</ta>
            <ta e="T22" id="Seg_209" s="T21">0.1.h:E</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T15" id="Seg_210" s="T14">RUS:cult</ta>
            <ta e="T17" id="Seg_211" s="T16">RUS:disc</ta>
            <ta e="T19" id="Seg_212" s="T17">RUS:disc</ta>
            <ta e="T21" id="Seg_213" s="T20">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_214" s="T1">I took the concertina in my hands,</ta>
            <ta e="T12" id="Seg_215" s="T8">As a concertina player in the dance,</ta>
            <ta e="T19" id="Seg_216" s="T12">I put the concertina on the bed,</ta>
            <ta e="T22" id="Seg_217" s="T19">I will love the concertina player.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_218" s="T1">Ich nahm die Ziehharmonika in meinen Händen</ta>
            <ta e="T12" id="Seg_219" s="T8">Als Ziehharmonika-Spieler im Reigen,</ta>
            <ta e="T19" id="Seg_220" s="T12">Ich legte die Ziehharmonika auf das Bett,</ta>
            <ta e="T22" id="Seg_221" s="T19">Ich werde den Ziehharmonika-Spieler lieben.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_222" s="T1">Я гармонь в руки взяла,</ta>
            <ta e="T12" id="Seg_223" s="T8">Гармонистом в хоровод,</ta>
            <ta e="T19" id="Seg_224" s="T12">Я гармошку — на кровать,</ta>
            <ta e="T22" id="Seg_225" s="T19">Гармониста полюблю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_226" s="T1">[AAV:] This may be not YIF as well as the following verse.</ta>
            <ta e="T12" id="Seg_227" s="T8">[AAV:] urolo ?</ta>
            <ta e="T19" id="Seg_228" s="T12">[AAV:] nuʒe ? ‎‎[AAV:] This is likely not YIF singing, since her voice seems to be overlapping in the end.</ta>
            <ta e="T22" id="Seg_229" s="T19">[AAV:] "[YIF:] (Nadezhda?) Petrovna, mat…" (see YIF_NP_196X_Drinking_conv)</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
