<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_196X_Milka_song</transcription-name>
         <referenced-file url="YIF_196X_Milka_song.wav" />
         <referenced-file url="YIF_196X_Milka_song.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_Milka_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">19</ud-information>
            <ud-information attribute-name="# HIAT:w">11</ud-information>
            <ud-information attribute-name="# e">11</ud-information>
            <ud-information attribute-name="# HIAT:u">3</ud-information>
            <ud-information attribute-name="# sc">6</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="2.89" type="appl" />
         <tli id="T2" time="3.77" type="appl" />
         <tli id="T3" time="4.65" type="appl" />
         <tli id="T4" time="5.53" type="appl" />
         <tli id="T5" time="6.57" type="appl" />
         <tli id="T6" time="7.61" type="appl" />
         <tli id="T7" time="7.63" type="appl" />
         <tli id="T8" time="8.2575" type="appl" />
         <tli id="T9" time="8.885" type="appl" />
         <tli id="T10" time="9.512500000000001" type="appl" />
         <tli id="T11" time="10.14" type="appl" />
         <tli id="T12" time="10.157" type="appl" />
         <tli id="T13" time="11.4285" type="appl" />
         <tli id="T14" time="12.7" type="appl" />
         <tli id="T0" time="13.003" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Čaǯak</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">čaǯak</ts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">wečʼorkand</ts>
                  <nts id="Seg_12" n="HIAT:ip">,</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">Čaǯak</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_17" n="HIAT:ip">—</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">čagetɨmbak</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T11" id="Seg_23" n="sc" s="T7">
               <ts e="T11" id="Seg_25" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">Meka</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">kadat</ts>
                  <nts id="Seg_31" n="HIAT:ip">:</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Milkam</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">nʼetu</ts>
                  <nts id="Seg_38" n="HIAT:ip">!</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_40" n="sc" s="T12">
               <ts e="T14" id="Seg_42" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">Čʼwesse</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">paralelʼčaxak</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T6" id="Seg_50" n="sc" s="T1">
               <ts e="T2" id="Seg_52" n="e" s="T1">Čaǯak, </ts>
               <ts e="T3" id="Seg_54" n="e" s="T2">čaǯak </ts>
               <ts e="T4" id="Seg_56" n="e" s="T3">wečʼorkand, </ts>
               <ts e="T5" id="Seg_58" n="e" s="T4">Čaǯak — </ts>
               <ts e="T6" id="Seg_60" n="e" s="T5">čagetɨmbak. </ts>
            </ts>
            <ts e="T11" id="Seg_61" n="sc" s="T7">
               <ts e="T8" id="Seg_63" n="e" s="T7">Meka </ts>
               <ts e="T9" id="Seg_65" n="e" s="T8">kadat: </ts>
               <ts e="T10" id="Seg_67" n="e" s="T9">Milkam </ts>
               <ts e="T11" id="Seg_69" n="e" s="T10">nʼetu! </ts>
            </ts>
            <ts e="T14" id="Seg_70" n="sc" s="T12">
               <ts e="T13" id="Seg_72" n="e" s="T12">Čʼwesse </ts>
               <ts e="T14" id="Seg_74" n="e" s="T13">paralelʼčaxak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_75" s="T1">YIF_196X_Milka_song.001 (001)</ta>
            <ta e="T11" id="Seg_76" s="T7">YIF_196X_Milka_song.002 (003)</ta>
            <ta e="T14" id="Seg_77" s="T12">YIF_196X_Milka_song.003 (004)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_78" s="T1">Ҷа́джак, ҷа́джак вечорка́нд,Ҷа́джак — чагетымбак.</ta>
            <ta e="T11" id="Seg_79" s="T7">Ме́ка ка́дат: «Ми́лкам не́ту!»</ta>
            <ta e="T14" id="Seg_80" s="T12">Чвэ́ссе парале́льчахак.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_81" s="T1">Čaǯak, čaǯak wečʼorkand,Čaǯak — čʼagetɨmbak.</ta>
            <ta e="T11" id="Seg_82" s="T7">Meka kadat: «Milkam nʼetu!»</ta>
            <ta e="T14" id="Seg_83" s="T12">Čʼwesse paralelʼčʼaxak.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_84" s="T1">Čaǯak, čaǯak wečʼorkand, Čaǯak — čagetɨmbak. </ta>
            <ta e="T11" id="Seg_85" s="T7">Meka kadat: Milkam netu! </ta>
            <ta e="T14" id="Seg_86" s="T12">Čʼwesse paralelʼčaxak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_87" s="T1">čaǯa-k</ta>
            <ta e="T3" id="Seg_88" s="T2">čaǯa-k</ta>
            <ta e="T4" id="Seg_89" s="T3">wečʼorka-nd</ta>
            <ta e="T5" id="Seg_90" s="T4">čaǯa-k</ta>
            <ta e="T6" id="Seg_91" s="T5">čagetɨ-mba-k</ta>
            <ta e="T8" id="Seg_92" s="T7">meka</ta>
            <ta e="T9" id="Seg_93" s="T8">kada-t</ta>
            <ta e="T10" id="Seg_94" s="T9">Milka-m</ta>
            <ta e="T11" id="Seg_95" s="T10">nʼetu</ta>
            <ta e="T13" id="Seg_96" s="T12">čʼwesse</ta>
            <ta e="T14" id="Seg_97" s="T13">para-le-lʼča-xa-k</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_98" s="T1">čaːǯɨ-k</ta>
            <ta e="T3" id="Seg_99" s="T2">čaːǯɨ-k</ta>
            <ta e="T4" id="Seg_100" s="T3">wečʼorka-nde</ta>
            <ta e="T5" id="Seg_101" s="T4">čaːǯɨ-k</ta>
            <ta e="T6" id="Seg_102" s="T5">čageːtɨ-mbɨ-k</ta>
            <ta e="T8" id="Seg_103" s="T7">mäkkä</ta>
            <ta e="T9" id="Seg_104" s="T8">kadɨ-dət</ta>
            <ta e="T10" id="Seg_105" s="T9">Milka-mɨ</ta>
            <ta e="T11" id="Seg_106" s="T10">nʼetu</ta>
            <ta e="T13" id="Seg_107" s="T12">čwesse</ta>
            <ta e="T14" id="Seg_108" s="T13">para-lɨ-lʼčǝ-ŋɨ-k</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_109" s="T1">travel-1SG.S</ta>
            <ta e="T3" id="Seg_110" s="T2">travel-1SG.S</ta>
            <ta e="T4" id="Seg_111" s="T3">evening.meeting-ILL</ta>
            <ta e="T5" id="Seg_112" s="T4">travel-1SG.S</ta>
            <ta e="T6" id="Seg_113" s="T5">hurry-PST.NAR-1SG.S</ta>
            <ta e="T8" id="Seg_114" s="T7">I.ALL</ta>
            <ta e="T9" id="Seg_115" s="T8">say-3PL</ta>
            <ta e="T10" id="Seg_116" s="T9">Milka-1SG</ta>
            <ta e="T11" id="Seg_117" s="T10">NEG.EX.[3SG.S]</ta>
            <ta e="T13" id="Seg_118" s="T12">backward</ta>
            <ta e="T14" id="Seg_119" s="T13">return-RES-PFV-CO-1SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_120" s="T1">ехать-1SG.S</ta>
            <ta e="T3" id="Seg_121" s="T2">ехать-1SG.S</ta>
            <ta e="T4" id="Seg_122" s="T3">вечёрка-ILL</ta>
            <ta e="T5" id="Seg_123" s="T4">ехать-1SG.S</ta>
            <ta e="T6" id="Seg_124" s="T5">спешить-PST.NAR-1SG.S</ta>
            <ta e="T8" id="Seg_125" s="T7">я.ALL</ta>
            <ta e="T9" id="Seg_126" s="T8">сказать-3PL</ta>
            <ta e="T10" id="Seg_127" s="T9">Милка-1SG</ta>
            <ta e="T11" id="Seg_128" s="T10">NEG.EX.[3SG.S]</ta>
            <ta e="T13" id="Seg_129" s="T12">назад</ta>
            <ta e="T14" id="Seg_130" s="T13">вернуться-RES-PFV-CO-1SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_131" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_132" s="T2">v-v:pn</ta>
            <ta e="T4" id="Seg_133" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_134" s="T4">v-v:pn</ta>
            <ta e="T6" id="Seg_135" s="T5">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_136" s="T7">pers</ta>
            <ta e="T9" id="Seg_137" s="T8">v-v:pn</ta>
            <ta e="T10" id="Seg_138" s="T9">nprop-n:poss</ta>
            <ta e="T11" id="Seg_139" s="T10">v.[v:pn]</ta>
            <ta e="T13" id="Seg_140" s="T12">adv</ta>
            <ta e="T14" id="Seg_141" s="T13">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_142" s="T1">v</ta>
            <ta e="T3" id="Seg_143" s="T2">v</ta>
            <ta e="T4" id="Seg_144" s="T3">n</ta>
            <ta e="T5" id="Seg_145" s="T4">v</ta>
            <ta e="T6" id="Seg_146" s="T5">v</ta>
            <ta e="T8" id="Seg_147" s="T7">pers</ta>
            <ta e="T9" id="Seg_148" s="T8">v</ta>
            <ta e="T10" id="Seg_149" s="T9">nprop</ta>
            <ta e="T11" id="Seg_150" s="T10">v</ta>
            <ta e="T13" id="Seg_151" s="T12">adv</ta>
            <ta e="T14" id="Seg_152" s="T13">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_153" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_154" s="T2">0.1.h:A</ta>
            <ta e="T4" id="Seg_155" s="T3">np:G</ta>
            <ta e="T5" id="Seg_156" s="T4">0.1.h:A</ta>
            <ta e="T6" id="Seg_157" s="T5">0.1.h:A</ta>
            <ta e="T8" id="Seg_158" s="T7">pro.h:R</ta>
            <ta e="T9" id="Seg_159" s="T8">0.3.h:A</ta>
            <ta e="T10" id="Seg_160" s="T9">np.h:Th</ta>
            <ta e="T13" id="Seg_161" s="T12">adv:G</ta>
            <ta e="T14" id="Seg_162" s="T13">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_163" s="T1">0.1.h:S v:pred</ta>
            <ta e="T3" id="Seg_164" s="T2">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_165" s="T4">0.1.h:S v:pred</ta>
            <ta e="T6" id="Seg_166" s="T5">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_167" s="T8">0.3.h:S v:pred</ta>
            <ta e="T10" id="Seg_168" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_169" s="T10">v:pred</ta>
            <ta e="T14" id="Seg_170" s="T13">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_171" s="T3">RUS:cult</ta>
            <ta e="T11" id="Seg_172" s="T10">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_173" s="T1">I went, went to the evening meeting,I went – hurrying.</ta>
            <ta e="T11" id="Seg_174" s="T7">I was told: “Milka is not here!”</ta>
            <ta e="T14" id="Seg_175" s="T12">So I returned.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_176" s="T1">Ich ging, ging zu dem Abendtreffen, Ich ging – in Eile.</ta>
            <ta e="T11" id="Seg_177" s="T7">Mir wurde gesagt: „Milka ist nicht hier!“</ta>
            <ta e="T14" id="Seg_178" s="T12">Also kehrte ich zurück.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_179" s="T1">Шла, шла на вечёрку,Шла — торопилася.</ta>
            <ta e="T11" id="Seg_180" s="T7">Мне сказали: «Милки нет!»</ta>
            <ta e="T14" id="Seg_181" s="T12">Назад воротилася.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
