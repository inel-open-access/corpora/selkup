<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>YIF_196X_WePickBerries_song</transcription-name>
         <referenced-file url="YIF_196X_WePickBerries_song.wav" />
         <referenced-file url="YIF_196X_WePickBerries_song.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">YIF_196X_WePickBerries_song.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">11</ud-information>
            <ud-information attribute-name="# HIAT:w">8</ud-information>
            <ud-information attribute-name="# e">8</ud-information>
            <ud-information attribute-name="# HIAT:u">1</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YIF">
            <abbreviation>YIF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="2.057" type="appl" />
         <tli id="T3" time="5.337763736004647" />
         <tli id="T4" time="5.622" type="appl" />
         <tli id="T6" time="7.492" type="appl" />
         <tli id="T7" time="8.4815" type="appl" />
         <tli id="T8" time="11.27527620639184" />
         <tli id="T9" time="11.6355" type="appl" />
         <tli id="T10" time="13.8" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YIF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Čoborla</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">wateǯeklaj</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">Weškem</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_14" n="HIAT:w" s="T4">melʼčeklaj</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_18" n="HIAT:w" s="T6">Potom</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_21" n="HIAT:w" s="T7">üdep</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_24" n="HIAT:w" s="T8">Üteǯekwelaj</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_27" n="HIAT:w" s="T9">naj</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_30" n="sc" s="T1">
               <ts e="T2" id="Seg_32" n="e" s="T1">Čoborla </ts>
               <ts e="T3" id="Seg_34" n="e" s="T2">wateǯeklaj, </ts>
               <ts e="T4" id="Seg_36" n="e" s="T3">Weškem </ts>
               <ts e="T6" id="Seg_38" n="e" s="T4">melʼčeklaj, </ts>
               <ts e="T7" id="Seg_40" n="e" s="T6">Potom </ts>
               <ts e="T8" id="Seg_42" n="e" s="T7">üdep </ts>
               <ts e="T9" id="Seg_44" n="e" s="T8">Üteǯekwelaj </ts>
               <ts e="T10" id="Seg_46" n="e" s="T9">naj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_47" s="T1">YIF_196X_WePickBerries_song.001 (001)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_48" s="T1">Ҷо́борла ватэ́джеклай, Фэ́шкэм ме́льчеклай, Пото́м ӱ́дэпӰтэ́джеквэлай най.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T10" id="Seg_49" s="T1">Čoborla wateǯeklaj, Weškem melʼčʼeklaj, Potom üdepÜteǯekwelaj naj.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_50" s="T1">Čoborla wateǯeklaj, Weškem melʼčeklaj, Potom üdep Üteǯekwelaj naj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_51" s="T1">čobor-la</ta>
            <ta e="T3" id="Seg_52" s="T2">wate-ǯe-k-la-j</ta>
            <ta e="T4" id="Seg_53" s="T3">wešk-e-m</ta>
            <ta e="T6" id="Seg_54" s="T4">me-lʼče-k-la-j</ta>
            <ta e="T7" id="Seg_55" s="T6">potom</ta>
            <ta e="T8" id="Seg_56" s="T7">üd-e-p</ta>
            <ta e="T9" id="Seg_57" s="T8">üte-ǯe-k-we-la-j</ta>
            <ta e="T10" id="Seg_58" s="T9">naj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_59" s="T1">čoːbɛr-la</ta>
            <ta e="T3" id="Seg_60" s="T2">wate-nǯe-ku-la-j</ta>
            <ta e="T4" id="Seg_61" s="T3">wešk-ɨ-m</ta>
            <ta e="T6" id="Seg_62" s="T4">me-lʼčǝ-ku-la-j</ta>
            <ta e="T7" id="Seg_63" s="T6">patom</ta>
            <ta e="T8" id="Seg_64" s="T7">üt-ɨ-p</ta>
            <ta e="T9" id="Seg_65" s="T8">üdɨ-nǯe-ku-we-la-j</ta>
            <ta e="T10" id="Seg_66" s="T9">naj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_67" s="T1">berry-PL.[NOM]</ta>
            <ta e="T3" id="Seg_68" s="T2">gather-IPFV3-HAB-FUT-1DU</ta>
            <ta e="T4" id="Seg_69" s="T3">cedarwood.cone-EP-ACC</ta>
            <ta e="T6" id="Seg_70" s="T4">do-PFV-HAB-FUT-1DU</ta>
            <ta e="T7" id="Seg_71" s="T6">then</ta>
            <ta e="T8" id="Seg_72" s="T7">water-EP-ACC</ta>
            <ta e="T9" id="Seg_73" s="T8">drink-IPFV3-HAB-DRV-FUT-1DU</ta>
            <ta e="T10" id="Seg_74" s="T9">also</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_75" s="T1">ягода-PL.[NOM]</ta>
            <ta e="T3" id="Seg_76" s="T2">набрать-IPFV3-HAB-FUT-1DU</ta>
            <ta e="T4" id="Seg_77" s="T3">кедровая.шишка-EP-ACC</ta>
            <ta e="T6" id="Seg_78" s="T4">делать-PFV-HAB-FUT-1DU</ta>
            <ta e="T7" id="Seg_79" s="T6">потом</ta>
            <ta e="T8" id="Seg_80" s="T7">вода-EP-ACC</ta>
            <ta e="T9" id="Seg_81" s="T8">пить-IPFV3-HAB-DRV-FUT-1DU</ta>
            <ta e="T10" id="Seg_82" s="T9">тоже</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_83" s="T1">n-n:num.[n:case]</ta>
            <ta e="T3" id="Seg_84" s="T2">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_85" s="T3">n-n:ins-n:case</ta>
            <ta e="T6" id="Seg_86" s="T4">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_87" s="T6">adv</ta>
            <ta e="T8" id="Seg_88" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_89" s="T8">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_90" s="T9">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_91" s="T1">n</ta>
            <ta e="T3" id="Seg_92" s="T2">v</ta>
            <ta e="T4" id="Seg_93" s="T3">n</ta>
            <ta e="T6" id="Seg_94" s="T4">v</ta>
            <ta e="T7" id="Seg_95" s="T6">adv</ta>
            <ta e="T8" id="Seg_96" s="T7">n</ta>
            <ta e="T9" id="Seg_97" s="T8">v</ta>
            <ta e="T10" id="Seg_98" s="T9">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_99" s="T1">np:O</ta>
            <ta e="T3" id="Seg_100" s="T2">0.1.h:S v:pred</ta>
            <ta e="T4" id="Seg_101" s="T3">np:O</ta>
            <ta e="T6" id="Seg_102" s="T4">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_103" s="T7">np:O</ta>
            <ta e="T9" id="Seg_104" s="T8">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_105" s="T1">np:Th</ta>
            <ta e="T3" id="Seg_106" s="T2">0.1.h:A</ta>
            <ta e="T4" id="Seg_107" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_108" s="T4">0.1.h:A</ta>
            <ta e="T8" id="Seg_109" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_110" s="T8">0.1.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_111" s="T6">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_112" s="T1">We’ll pick berries, We’ll get nuts, Then water (vodka) Is what we’ll drink.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_113" s="T1">Wir werden Beeren pflücken, Wir werden Nüsse holen, Dann wird es Wasser (Vodka) Das wir trinken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T10" id="Seg_114" s="T1">Ягоды наберём, Орехов наделаем, Потом воду (водку) Выпивать будем тоже.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt">
            <ta e="T10" id="Seg_115" s="T1">[AAV:] Čoborla &gt; Čobor ?</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
