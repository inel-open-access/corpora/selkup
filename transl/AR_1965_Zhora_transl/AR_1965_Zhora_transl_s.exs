<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>AR_1965_Zhora_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">AR_1965_Zhora_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">37</ud-information>
            <ud-information attribute-name="# HIAT:w">31</ud-information>
            <ud-information attribute-name="# e">31</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AR">
            <abbreviation>AR</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AR"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T62" id="Seg_0" n="sc" s="T31">
               <ts e="T36" id="Seg_2" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_4" n="HIAT:w" s="T31">Nʼi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_7" n="HIAT:w" s="T32">kutɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_10" n="HIAT:w" s="T33">qajem</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_13" n="HIAT:w" s="T34">čʼäːŋka</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_16" n="HIAT:w" s="T35">qontɨrɛːtɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_20" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_22" n="HIAT:w" s="T36">Žora</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_25" n="HIAT:w" s="T37">qənpa</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_28" n="HIAT:w" s="T38">mɔːttɨ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_31" n="HIAT:w" s="T39">nɔːnɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_34" n="HIAT:w" s="T40">tɛːttɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_37" n="HIAT:w" s="T41">čʼasɨqɨn</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_40" n="HIAT:w" s="T42">qarɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_44" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_46" n="HIAT:w" s="T43">Mašina</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_49" n="HIAT:w" s="T44">tüntɨsa</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_52" n="HIAT:w" s="T45">püj</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_55" n="HIAT:w" s="T46">kikajantɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_59" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_61" n="HIAT:w" s="T47">Pɔːpɨ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_64" n="HIAT:w" s="T48">Šora</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_67" n="HIAT:w" s="T49">üːntəsesit</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_70" n="HIAT:w" s="T50">qos</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_73" n="HIAT:w" s="T51">qajim</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_77" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_79" n="HIAT:w" s="T52">Täp</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_82" n="HIAT:w" s="T53">tantɨs</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_85" n="HIAT:w" s="T54">ponä</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_88" n="HIAT:w" s="T55">mašina</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_91" n="HIAT:w" s="T56">nɔːnɨ</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_95" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_97" n="HIAT:w" s="T57">Nɨːnɨ</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_100" n="HIAT:w" s="T58">me</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_103" n="HIAT:w" s="T59">nɔːtɨ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_106" n="HIAT:w" s="T60">aša</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_109" n="HIAT:w" s="T61">qəntɔːmɨn</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T62" id="Seg_112" n="sc" s="T31">
               <ts e="T32" id="Seg_114" n="e" s="T31">Nʼi </ts>
               <ts e="T33" id="Seg_116" n="e" s="T32">kutɨ </ts>
               <ts e="T34" id="Seg_118" n="e" s="T33">qajem </ts>
               <ts e="T35" id="Seg_120" n="e" s="T34">čʼäːŋka </ts>
               <ts e="T36" id="Seg_122" n="e" s="T35">qontɨrɛːtɨ. </ts>
               <ts e="T37" id="Seg_124" n="e" s="T36">Žora </ts>
               <ts e="T38" id="Seg_126" n="e" s="T37">qənpa </ts>
               <ts e="T39" id="Seg_128" n="e" s="T38">mɔːttɨ </ts>
               <ts e="T40" id="Seg_130" n="e" s="T39">nɔːnɨ </ts>
               <ts e="T41" id="Seg_132" n="e" s="T40">tɛːttɨ </ts>
               <ts e="T42" id="Seg_134" n="e" s="T41">čʼasɨqɨn </ts>
               <ts e="T43" id="Seg_136" n="e" s="T42">qarɨn. </ts>
               <ts e="T44" id="Seg_138" n="e" s="T43">Mašina </ts>
               <ts e="T45" id="Seg_140" n="e" s="T44">tüntɨsa </ts>
               <ts e="T46" id="Seg_142" n="e" s="T45">püj </ts>
               <ts e="T47" id="Seg_144" n="e" s="T46">kikajantɨ. </ts>
               <ts e="T48" id="Seg_146" n="e" s="T47">Pɔːpɨ </ts>
               <ts e="T49" id="Seg_148" n="e" s="T48">Šora </ts>
               <ts e="T50" id="Seg_150" n="e" s="T49">üːntəsesit </ts>
               <ts e="T51" id="Seg_152" n="e" s="T50">qos </ts>
               <ts e="T52" id="Seg_154" n="e" s="T51">qajim. </ts>
               <ts e="T53" id="Seg_156" n="e" s="T52">Täp </ts>
               <ts e="T54" id="Seg_158" n="e" s="T53">tantɨs </ts>
               <ts e="T55" id="Seg_160" n="e" s="T54">ponä </ts>
               <ts e="T56" id="Seg_162" n="e" s="T55">mašina </ts>
               <ts e="T57" id="Seg_164" n="e" s="T56">nɔːnɨ. </ts>
               <ts e="T58" id="Seg_166" n="e" s="T57">Nɨːnɨ </ts>
               <ts e="T59" id="Seg_168" n="e" s="T58">me </ts>
               <ts e="T60" id="Seg_170" n="e" s="T59">nɔːtɨ </ts>
               <ts e="T61" id="Seg_172" n="e" s="T60">aša </ts>
               <ts e="T62" id="Seg_174" n="e" s="T61">qəntɔːmɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T36" id="Seg_175" s="T31">AR_1965_Zhora_transl.001 (001.001)</ta>
            <ta e="T43" id="Seg_176" s="T36">AR_1965_Zhora_transl.002 (001.002)</ta>
            <ta e="T47" id="Seg_177" s="T43">AR_1965_Zhora_transl.003 (001.003)</ta>
            <ta e="T52" id="Seg_178" s="T47">AR_1965_Zhora_transl.004 (001.004)</ta>
            <ta e="T57" id="Seg_179" s="T52">AR_1965_Zhora_transl.005 (001.005)</ta>
            <ta e="T62" id="Seg_180" s="T57">AR_1965_Zhora_transl.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T36" id="Seg_181" s="T31">ни′кута ′kаjем ′чен̃ка ′kонтырʼӓты.</ta>
            <ta e="T43" id="Seg_182" s="T36">′жора ′kӓнпа ′мо̄тынонъ ′тӓтты ча′саɣын kа̄рын.</ta>
            <ta e="T47" id="Seg_183" s="T43">ма′шина ′тӱнтысӓ ′пӱй кӣка′jанты.</ta>
            <ta e="T52" id="Seg_184" s="T47">′по̄пы ′шора ′ӱ̄нтъсе̨сит kос ′kайjим.</ta>
            <ta e="T57" id="Seg_185" s="T52">тӓп ′тандыс понӓ ′машинаноны.</ta>
            <ta e="T62" id="Seg_186" s="T57">′ныны ме ′ноты ′аша kӓ̄н′томын.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T36" id="Seg_187" s="T31">nikuta qajem čʼenka qontɨrʼätɨ.</ta>
            <ta e="T43" id="Seg_188" s="T36">жora qänpa moːtɨnonə tättɨ čʼasaqɨn qaːrɨn.</ta>
            <ta e="T47" id="Seg_189" s="T43">mašina tüntɨsä püj kiːkajantɨ.</ta>
            <ta e="T52" id="Seg_190" s="T47">poːpɨ šora üːntəsesit qos qajjim.</ta>
            <ta e="T57" id="Seg_191" s="T52">täp tandɨs ponä mašinanonɨ.</ta>
            <ta e="T62" id="Seg_192" s="T57">nɨnɨ me notɨ aša qäːntomɨn.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T36" id="Seg_193" s="T31">Nʼi kutɨ qajem čʼäːŋka qontɨrɛːtɨ. </ta>
            <ta e="T43" id="Seg_194" s="T36">Žora qənpa mɔːttɨ nɔːnɨ tɛːttɨ čʼasɨqɨn qarɨn. </ta>
            <ta e="T47" id="Seg_195" s="T43">Mašina tüntɨsa püj kikajantɨ. </ta>
            <ta e="T52" id="Seg_196" s="T47">Pɔːpɨ Šora üːntəsesit qos qajim. </ta>
            <ta e="T57" id="Seg_197" s="T52">Täp tantɨs ponä mašina nɔːnɨ. </ta>
            <ta e="T62" id="Seg_198" s="T57">Nɨːnɨ me nɔːtɨ aša qəntɔːmɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T32" id="Seg_199" s="T31">nʼi</ta>
            <ta e="T33" id="Seg_200" s="T32">kutɨ</ta>
            <ta e="T34" id="Seg_201" s="T33">qaj-e-m</ta>
            <ta e="T35" id="Seg_202" s="T34">čʼäːŋka</ta>
            <ta e="T36" id="Seg_203" s="T35">qo-ntɨr-ɛː-tɨ</ta>
            <ta e="T37" id="Seg_204" s="T36">Žora</ta>
            <ta e="T38" id="Seg_205" s="T37">qən-pa</ta>
            <ta e="T39" id="Seg_206" s="T38">mɔːt-tɨ</ta>
            <ta e="T40" id="Seg_207" s="T39">nɔː-nɨ</ta>
            <ta e="T41" id="Seg_208" s="T40">tɛːttɨ</ta>
            <ta e="T42" id="Seg_209" s="T41">čʼas-ɨ-qɨn</ta>
            <ta e="T43" id="Seg_210" s="T42">qarɨ-n</ta>
            <ta e="T44" id="Seg_211" s="T43">mašina</ta>
            <ta e="T45" id="Seg_212" s="T44">tü-ntɨ-sa</ta>
            <ta e="T46" id="Seg_213" s="T45">pü-j</ta>
            <ta e="T47" id="Seg_214" s="T46">kika-ja-ntɨ</ta>
            <ta e="T48" id="Seg_215" s="T47">pɔːpɨ</ta>
            <ta e="T49" id="Seg_216" s="T48">Šora</ta>
            <ta e="T50" id="Seg_217" s="T49">üːntə-s-e-si-t</ta>
            <ta e="T51" id="Seg_218" s="T50">qos</ta>
            <ta e="T52" id="Seg_219" s="T51">qaj-i-m</ta>
            <ta e="T53" id="Seg_220" s="T52">täp</ta>
            <ta e="T54" id="Seg_221" s="T53">tantɨ-s</ta>
            <ta e="T55" id="Seg_222" s="T54">ponä</ta>
            <ta e="T56" id="Seg_223" s="T55">mašina</ta>
            <ta e="T57" id="Seg_224" s="T56">nɔː-nɨ</ta>
            <ta e="T58" id="Seg_225" s="T57">nɨːnɨ</ta>
            <ta e="T59" id="Seg_226" s="T58">me</ta>
            <ta e="T60" id="Seg_227" s="T59">nɔːtɨ</ta>
            <ta e="T61" id="Seg_228" s="T60">aša</ta>
            <ta e="T62" id="Seg_229" s="T61">qən-tɔː-mɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T32" id="Seg_230" s="T31">nʼi</ta>
            <ta e="T33" id="Seg_231" s="T32">kutɨ</ta>
            <ta e="T34" id="Seg_232" s="T33">qaj-ɨ-m</ta>
            <ta e="T35" id="Seg_233" s="T34">čʼäːŋka</ta>
            <ta e="T36" id="Seg_234" s="T35">qo-ntɨr-ɛː-tɨ</ta>
            <ta e="T37" id="Seg_235" s="T36">Žora</ta>
            <ta e="T38" id="Seg_236" s="T37">qən-mpɨ</ta>
            <ta e="T39" id="Seg_237" s="T38">mɔːt-tɨ</ta>
            <ta e="T40" id="Seg_238" s="T39">*nɔː-nɨ</ta>
            <ta e="T41" id="Seg_239" s="T40">tɛːttɨ</ta>
            <ta e="T42" id="Seg_240" s="T41">čʼas-ɨ-qɨn</ta>
            <ta e="T43" id="Seg_241" s="T42">qarɨ-n</ta>
            <ta e="T44" id="Seg_242" s="T43">mašina</ta>
            <ta e="T45" id="Seg_243" s="T44">tü-ntɨ-sɨ</ta>
            <ta e="T46" id="Seg_244" s="T45">pü-lʼ</ta>
            <ta e="T47" id="Seg_245" s="T46">kıkä-ja-ntɨ</ta>
            <ta e="T48" id="Seg_246" s="T47">pɔːpɨ</ta>
            <ta e="T49" id="Seg_247" s="T48">Žora</ta>
            <ta e="T50" id="Seg_248" s="T49">üntɨ-š-ɨ-sɨ-tɨ</ta>
            <ta e="T51" id="Seg_249" s="T50">kos</ta>
            <ta e="T52" id="Seg_250" s="T51">qaj-ɨ-m</ta>
            <ta e="T53" id="Seg_251" s="T52">təp</ta>
            <ta e="T54" id="Seg_252" s="T53">tantɨ-sɨ</ta>
            <ta e="T55" id="Seg_253" s="T54">ponä</ta>
            <ta e="T56" id="Seg_254" s="T55">mašina</ta>
            <ta e="T57" id="Seg_255" s="T56">*nɔː-nɨ</ta>
            <ta e="T58" id="Seg_256" s="T57">nɨːnɨ</ta>
            <ta e="T59" id="Seg_257" s="T58">meː</ta>
            <ta e="T60" id="Seg_258" s="T59">nɔːtɨ</ta>
            <ta e="T61" id="Seg_259" s="T60">ašša</ta>
            <ta e="T62" id="Seg_260" s="T61">qən-ɛntɨ-mɨt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T32" id="Seg_261" s="T31">NEG</ta>
            <ta e="T33" id="Seg_262" s="T32">who.[NOM]</ta>
            <ta e="T34" id="Seg_263" s="T33">what-EP-ACC</ta>
            <ta e="T35" id="Seg_264" s="T34">NEG</ta>
            <ta e="T36" id="Seg_265" s="T35">see-DRV-PFV-3SG.O</ta>
            <ta e="T37" id="Seg_266" s="T36">Zhora.[NOM]</ta>
            <ta e="T38" id="Seg_267" s="T37">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_268" s="T38">tent.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_269" s="T39">out-ADV.EL</ta>
            <ta e="T41" id="Seg_270" s="T40">four</ta>
            <ta e="T42" id="Seg_271" s="T41">hour-EP-LOC</ta>
            <ta e="T43" id="Seg_272" s="T42">morning-ADV.LOC</ta>
            <ta e="T44" id="Seg_273" s="T43">car.[NOM]</ta>
            <ta e="T45" id="Seg_274" s="T44">come-IPFV-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_275" s="T45">stone-ADJZ</ta>
            <ta e="T47" id="Seg_276" s="T46">small.river-DIM-ILL</ta>
            <ta e="T48" id="Seg_277" s="T47">suddenly</ta>
            <ta e="T49" id="Seg_278" s="T48">Zhora.[NOM]</ta>
            <ta e="T50" id="Seg_279" s="T49">hear-US-EP-PST-3SG.O</ta>
            <ta e="T51" id="Seg_280" s="T50">INDEF3</ta>
            <ta e="T52" id="Seg_281" s="T51">what-EP-ACC</ta>
            <ta e="T53" id="Seg_282" s="T52">(s)he.[NOM]</ta>
            <ta e="T54" id="Seg_283" s="T53">go.out-PST.[3SG.S]</ta>
            <ta e="T55" id="Seg_284" s="T54">outwards</ta>
            <ta e="T56" id="Seg_285" s="T55">car.[NOM]</ta>
            <ta e="T57" id="Seg_286" s="T56">out-ADV.EL</ta>
            <ta e="T58" id="Seg_287" s="T57">then</ta>
            <ta e="T59" id="Seg_288" s="T58">we.PL.NOM</ta>
            <ta e="T60" id="Seg_289" s="T59">then</ta>
            <ta e="T61" id="Seg_290" s="T60">NEG</ta>
            <ta e="T62" id="Seg_291" s="T61">go.away-FUT-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T32" id="Seg_292" s="T31">NEG</ta>
            <ta e="T33" id="Seg_293" s="T32">кто.[NOM]</ta>
            <ta e="T34" id="Seg_294" s="T33">что-EP-ACC</ta>
            <ta e="T35" id="Seg_295" s="T34">NEG</ta>
            <ta e="T36" id="Seg_296" s="T35">увидеть-DRV-PFV-3SG.O</ta>
            <ta e="T37" id="Seg_297" s="T36">Жора.[NOM]</ta>
            <ta e="T38" id="Seg_298" s="T37">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T39" id="Seg_299" s="T38">чум.[NOM]-3SG</ta>
            <ta e="T40" id="Seg_300" s="T39">из-ADV.EL</ta>
            <ta e="T41" id="Seg_301" s="T40">четыре</ta>
            <ta e="T42" id="Seg_302" s="T41">час-EP-LOC</ta>
            <ta e="T43" id="Seg_303" s="T42">утро-ADV.LOC</ta>
            <ta e="T44" id="Seg_304" s="T43">машина.[NOM]</ta>
            <ta e="T45" id="Seg_305" s="T44">прийти-IPFV-PST.[3SG.S]</ta>
            <ta e="T46" id="Seg_306" s="T45">камень-ADJZ</ta>
            <ta e="T47" id="Seg_307" s="T46">речка-DIM-ILL</ta>
            <ta e="T48" id="Seg_308" s="T47">вдруг</ta>
            <ta e="T49" id="Seg_309" s="T48">Жора.[NOM]</ta>
            <ta e="T50" id="Seg_310" s="T49">слышать-US-EP-PST-3SG.O</ta>
            <ta e="T51" id="Seg_311" s="T50">INDEF3</ta>
            <ta e="T52" id="Seg_312" s="T51">что-EP-ACC</ta>
            <ta e="T53" id="Seg_313" s="T52">он(а).[NOM]</ta>
            <ta e="T54" id="Seg_314" s="T53">выйти-PST.[3SG.S]</ta>
            <ta e="T55" id="Seg_315" s="T54">наружу</ta>
            <ta e="T56" id="Seg_316" s="T55">машина.[NOM]</ta>
            <ta e="T57" id="Seg_317" s="T56">из-ADV.EL</ta>
            <ta e="T58" id="Seg_318" s="T57">потом</ta>
            <ta e="T59" id="Seg_319" s="T58">мы.PL.NOM</ta>
            <ta e="T60" id="Seg_320" s="T59">затем</ta>
            <ta e="T61" id="Seg_321" s="T60">NEG</ta>
            <ta e="T62" id="Seg_322" s="T61">уйти-FUT-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T32" id="Seg_323" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_324" s="T32">interrog-n:case</ta>
            <ta e="T34" id="Seg_325" s="T33">interrog-n:ins-n:case</ta>
            <ta e="T35" id="Seg_326" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_327" s="T35">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_328" s="T36">nprop-n:case</ta>
            <ta e="T38" id="Seg_329" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_330" s="T38">n-n:case-n:poss</ta>
            <ta e="T40" id="Seg_331" s="T39">pp-adv:case</ta>
            <ta e="T41" id="Seg_332" s="T40">num</ta>
            <ta e="T42" id="Seg_333" s="T41">n-n:ins-n:case</ta>
            <ta e="T43" id="Seg_334" s="T42">n-n&gt;adv</ta>
            <ta e="T44" id="Seg_335" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_336" s="T44">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_337" s="T45">n-n&gt;adj</ta>
            <ta e="T47" id="Seg_338" s="T46">n-n&gt;n-n:case</ta>
            <ta e="T48" id="Seg_339" s="T47">adv</ta>
            <ta e="T49" id="Seg_340" s="T48">nprop-n:case</ta>
            <ta e="T50" id="Seg_341" s="T49">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_342" s="T50">clit</ta>
            <ta e="T52" id="Seg_343" s="T51">interrog-n:ins-n:case</ta>
            <ta e="T53" id="Seg_344" s="T52">pers-n:case</ta>
            <ta e="T54" id="Seg_345" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_346" s="T54">adv</ta>
            <ta e="T56" id="Seg_347" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_348" s="T56">pp-adv:case</ta>
            <ta e="T58" id="Seg_349" s="T57">adv</ta>
            <ta e="T59" id="Seg_350" s="T58">pers</ta>
            <ta e="T60" id="Seg_351" s="T59">adv</ta>
            <ta e="T61" id="Seg_352" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_353" s="T61">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T32" id="Seg_354" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_355" s="T32">interrog</ta>
            <ta e="T34" id="Seg_356" s="T33">interrog</ta>
            <ta e="T35" id="Seg_357" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_358" s="T35">v</ta>
            <ta e="T37" id="Seg_359" s="T36">nprop</ta>
            <ta e="T38" id="Seg_360" s="T37">v</ta>
            <ta e="T39" id="Seg_361" s="T38">n</ta>
            <ta e="T40" id="Seg_362" s="T39">pp</ta>
            <ta e="T41" id="Seg_363" s="T40">num</ta>
            <ta e="T42" id="Seg_364" s="T41">n</ta>
            <ta e="T43" id="Seg_365" s="T42">adv</ta>
            <ta e="T44" id="Seg_366" s="T43">n</ta>
            <ta e="T45" id="Seg_367" s="T44">v</ta>
            <ta e="T46" id="Seg_368" s="T45">adj</ta>
            <ta e="T47" id="Seg_369" s="T46">n</ta>
            <ta e="T48" id="Seg_370" s="T47">adv</ta>
            <ta e="T49" id="Seg_371" s="T48">nprop</ta>
            <ta e="T50" id="Seg_372" s="T49">v</ta>
            <ta e="T51" id="Seg_373" s="T50">clit</ta>
            <ta e="T52" id="Seg_374" s="T51">interrog</ta>
            <ta e="T53" id="Seg_375" s="T52">pers</ta>
            <ta e="T54" id="Seg_376" s="T53">v</ta>
            <ta e="T55" id="Seg_377" s="T54">adv</ta>
            <ta e="T56" id="Seg_378" s="T55">n</ta>
            <ta e="T57" id="Seg_379" s="T56">pp</ta>
            <ta e="T58" id="Seg_380" s="T57">adv</ta>
            <ta e="T59" id="Seg_381" s="T58">pers</ta>
            <ta e="T60" id="Seg_382" s="T59">adv</ta>
            <ta e="T61" id="Seg_383" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_384" s="T61">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T33" id="Seg_385" s="T32">pro.h:E</ta>
            <ta e="T34" id="Seg_386" s="T33">pro:Th</ta>
            <ta e="T37" id="Seg_387" s="T36">np.h:A</ta>
            <ta e="T39" id="Seg_388" s="T38">pp:So</ta>
            <ta e="T42" id="Seg_389" s="T41">n:Time</ta>
            <ta e="T43" id="Seg_390" s="T42">adv:Time</ta>
            <ta e="T44" id="Seg_391" s="T43">np:Th</ta>
            <ta e="T47" id="Seg_392" s="T46">np:G</ta>
            <ta e="T48" id="Seg_393" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_394" s="T48">np.h:E</ta>
            <ta e="T52" id="Seg_395" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_396" s="T52">pro.h:A</ta>
            <ta e="T55" id="Seg_397" s="T54">adv:G</ta>
            <ta e="T56" id="Seg_398" s="T55">pp:So</ta>
            <ta e="T58" id="Seg_399" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_400" s="T58">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T33" id="Seg_401" s="T32">pro.h:S</ta>
            <ta e="T34" id="Seg_402" s="T33">pro:O</ta>
            <ta e="T36" id="Seg_403" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_404" s="T36">np.h:S</ta>
            <ta e="T38" id="Seg_405" s="T37">v:pred</ta>
            <ta e="T44" id="Seg_406" s="T43">np:S</ta>
            <ta e="T45" id="Seg_407" s="T44">v:pred</ta>
            <ta e="T49" id="Seg_408" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_409" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_410" s="T51">np:O</ta>
            <ta e="T53" id="Seg_411" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_412" s="T53">v:pred</ta>
            <ta e="T59" id="Seg_413" s="T58">pro.h:S</ta>
            <ta e="T62" id="Seg_414" s="T61">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T33" id="Seg_415" s="T32">new</ta>
            <ta e="T34" id="Seg_416" s="T33">new</ta>
            <ta e="T37" id="Seg_417" s="T36">new</ta>
            <ta e="T39" id="Seg_418" s="T38">new</ta>
            <ta e="T44" id="Seg_419" s="T43">new</ta>
            <ta e="T47" id="Seg_420" s="T46">accs-gen</ta>
            <ta e="T49" id="Seg_421" s="T48">giv-inactive</ta>
            <ta e="T52" id="Seg_422" s="T51">new</ta>
            <ta e="T53" id="Seg_423" s="T52">giv-active</ta>
            <ta e="T56" id="Seg_424" s="T55">giv-inactive</ta>
            <ta e="T59" id="Seg_425" s="T58">accs-aggr-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T37" id="Seg_426" s="T36">RUS:cult</ta>
            <ta e="T49" id="Seg_427" s="T48">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T36" id="Seg_428" s="T31">Никто ничего не видел.</ta>
            <ta e="T43" id="Seg_429" s="T36">Жора выехал из дома в 4 часа утра.</ta>
            <ta e="T47" id="Seg_430" s="T43">Машина приближалась к горной речушке.</ta>
            <ta e="T52" id="Seg_431" s="T47">Вдруг Жора услышал что-то.</ta>
            <ta e="T57" id="Seg_432" s="T52">Он вышел наружу из машины.</ta>
            <ta e="T62" id="Seg_433" s="T57">"Дальше мы не поедем".</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T36" id="Seg_434" s="T31">No one saw anything.</ta>
            <ta e="T43" id="Seg_435" s="T36">‎Zhora left the house at four in the morning.</ta>
            <ta e="T47" id="Seg_436" s="T43">The car was coming close to a small rocky river.</ta>
            <ta e="T52" id="Seg_437" s="T47">Suddenly Zhora heard something.</ta>
            <ta e="T57" id="Seg_438" s="T52">He went out of the car.</ta>
            <ta e="T62" id="Seg_439" s="T57">"We will not go any further".</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T36" id="Seg_440" s="T31">Keiner sah etwas.</ta>
            <ta e="T43" id="Seg_441" s="T36">Zhora verließ das Haus um vier Uhr morgens.</ta>
            <ta e="T47" id="Seg_442" s="T43">Das Auto näherte sich einem kleinen steinigen Fluss.</ta>
            <ta e="T52" id="Seg_443" s="T47">Plötzlich hörte Zhora etwas.</ta>
            <ta e="T57" id="Seg_444" s="T52">Er stieg aus dem Auto aus.</ta>
            <ta e="T62" id="Seg_445" s="T57">"Weiter fahren wir nicht."</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T36" id="Seg_446" s="T31">никто ничего не видел.</ta>
            <ta e="T43" id="Seg_447" s="T36">Жора выехал из дома в 4 часа утра.</ta>
            <ta e="T47" id="Seg_448" s="T43">машина приближалась к горной речушке.</ta>
            <ta e="T52" id="Seg_449" s="T47">вдруг жора услышал что-то.</ta>
            <ta e="T57" id="Seg_450" s="T52">он вышел наружу из машины.</ta>
            <ta e="T62" id="Seg_451" s="T57">дальше мы больше не поедем.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
