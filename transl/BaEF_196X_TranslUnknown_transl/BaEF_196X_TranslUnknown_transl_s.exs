<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>BaEF_196X_TranslUnknown_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">BaEF_196X_TranslUnknown_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">32</ud-information>
            <ud-information attribute-name="# HIAT:w">21</ud-information>
            <ud-information attribute-name="# e">21</ud-information>
            <ud-information attribute-name="# HIAT:u">6</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaEF">
            <abbreviation>BaEF</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaEF"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T22" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tebeɣum</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ippɨs</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">innäj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">polkaɣɨn</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Sanqam</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">penbat</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">udoɣɨtdə</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">akoškaon</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">mannɨbɨs</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_36" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">Täp</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">qaimness</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">qotǯirbat</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_48" n="HIAT:u" s="T13">
                  <nts id="Seg_49" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">Sejda</ts>
                  <nts id="Seg_52" n="HIAT:ip">)</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">Seim</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">čaɣəǯimbat</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Seimda</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">nʼüot</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_71" n="HIAT:u" s="T18">
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Wattoɣɨn</ts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">Matqɨn</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">tʼömnan</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">jen</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T22" id="Seg_87" n="sc" s="T1">
               <ts e="T2" id="Seg_89" n="e" s="T1">Tebeɣum </ts>
               <ts e="T3" id="Seg_91" n="e" s="T2">ippɨs </ts>
               <ts e="T4" id="Seg_93" n="e" s="T3">innäj </ts>
               <ts e="T5" id="Seg_95" n="e" s="T4">polkaɣɨn. </ts>
               <ts e="T6" id="Seg_97" n="e" s="T5">Sanqam </ts>
               <ts e="T7" id="Seg_99" n="e" s="T6">penbat </ts>
               <ts e="T8" id="Seg_101" n="e" s="T7">udoɣɨtdə, </ts>
               <ts e="T9" id="Seg_103" n="e" s="T8">akoškaon </ts>
               <ts e="T10" id="Seg_105" n="e" s="T9">mannɨbɨs. </ts>
               <ts e="T11" id="Seg_107" n="e" s="T10">Täp </ts>
               <ts e="T12" id="Seg_109" n="e" s="T11">qaimness </ts>
               <ts e="T13" id="Seg_111" n="e" s="T12">qotǯirbat. </ts>
               <ts e="T14" id="Seg_113" n="e" s="T13">(Sejda) </ts>
               <ts e="T15" id="Seg_115" n="e" s="T14">Seim </ts>
               <ts e="T16" id="Seg_117" n="e" s="T15">čaɣəǯimbat. </ts>
               <ts e="T17" id="Seg_119" n="e" s="T16">Seimda </ts>
               <ts e="T18" id="Seg_121" n="e" s="T17">nʼüot. </ts>
               <ts e="T19" id="Seg_123" n="e" s="T18">(Wattoɣɨn) </ts>
               <ts e="T20" id="Seg_125" n="e" s="T19">Matqɨn </ts>
               <ts e="T21" id="Seg_127" n="e" s="T20">tʼömnan </ts>
               <ts e="T22" id="Seg_129" n="e" s="T21">jen. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_130" s="T1">BaEF_196X_TranslUnknown_transl.001 (001.001)</ta>
            <ta e="T10" id="Seg_131" s="T5">BaEF_196X_TranslUnknown_transl.002 (001.002)</ta>
            <ta e="T13" id="Seg_132" s="T10">BaEF_196X_TranslUnknown_transl.003 (001.003)</ta>
            <ta e="T16" id="Seg_133" s="T13">BaEF_196X_TranslUnknown_transl.004 (001.004)</ta>
            <ta e="T18" id="Seg_134" s="T16">BaEF_196X_TranslUnknown_transl.005 (001.005)</ta>
            <ta e="T22" id="Seg_135" s="T18">BaEF_196X_TranslUnknown_transl.006 (001.006)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_136" s="T1">тебе′ɣум и′ппыс и′ннӓй полкаɣын.</ta>
            <ta e="T10" id="Seg_137" s="T5">′санkам пен′бат у′доɣытдъ, а′кошкаон манны′быс.</ta>
            <ta e="T13" id="Seg_138" s="T10">тӓп kаим′несс kотджир′бат.</ta>
            <ta e="T16" id="Seg_139" s="T13">(сейда) ′сеим ′тшаɣъджим′бат.</ta>
            <ta e="T18" id="Seg_140" s="T16">′сеимда ′нʼӱот.</ta>
            <ta e="T22" id="Seg_141" s="T18">(wа′ттоɣын) ′матkын ′тʼӧмнан ′jен.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T5" id="Seg_142" s="T1">tebeɣum ippɨs innäj polkaɣɨn.</ta>
            <ta e="T10" id="Seg_143" s="T5">sanqam penbat udoɣɨtdə, akoškaon mannɨbɨs.</ta>
            <ta e="T13" id="Seg_144" s="T10">täp qaimness qotǯirbat.</ta>
            <ta e="T16" id="Seg_145" s="T13">(sejda) seim čaɣəǯimbat.</ta>
            <ta e="T18" id="Seg_146" s="T16">seimda nʼüot.</ta>
            <ta e="T22" id="Seg_147" s="T18">(wattoɣɨn) matqɨn tʼömnan jen.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_148" s="T1">Tebeɣum ippɨs innäj polkaɣɨn. </ta>
            <ta e="T10" id="Seg_149" s="T5">Sanqam penbat udoɣɨtdə, akoškaon mannɨbɨs. </ta>
            <ta e="T13" id="Seg_150" s="T10">Täp qaimness qotǯirbat. </ta>
            <ta e="T16" id="Seg_151" s="T13">(Sejda) Seim čaɣəǯimbat. </ta>
            <ta e="T18" id="Seg_152" s="T16">Seimda nʼüot. </ta>
            <ta e="T22" id="Seg_153" s="T18">(Wattoɣɨn) Matqɨn tʼömnan jen. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_154" s="T1">tebe-ɣum</ta>
            <ta e="T3" id="Seg_155" s="T2">ippɨ-s</ta>
            <ta e="T4" id="Seg_156" s="T3">innä-j</ta>
            <ta e="T5" id="Seg_157" s="T4">polka-ɣɨn</ta>
            <ta e="T6" id="Seg_158" s="T5">sanqa-m</ta>
            <ta e="T7" id="Seg_159" s="T6">pen-ba-t</ta>
            <ta e="T8" id="Seg_160" s="T7">ud-o-ɣɨtdə</ta>
            <ta e="T9" id="Seg_161" s="T8">akoška-on</ta>
            <ta e="T10" id="Seg_162" s="T9">mannɨ-bɨ-s</ta>
            <ta e="T11" id="Seg_163" s="T10">täp</ta>
            <ta e="T12" id="Seg_164" s="T11">qai-m-ne-ss</ta>
            <ta e="T13" id="Seg_165" s="T12">qo-tǯir-ba-t</ta>
            <ta e="T14" id="Seg_166" s="T13">sej-da</ta>
            <ta e="T15" id="Seg_167" s="T14">sei-m</ta>
            <ta e="T16" id="Seg_168" s="T15">čaɣəǯi-mba-t</ta>
            <ta e="T17" id="Seg_169" s="T16">sei-m-da</ta>
            <ta e="T18" id="Seg_170" s="T17">nʼü-o-t</ta>
            <ta e="T19" id="Seg_171" s="T18">watto-ɣɨn</ta>
            <ta e="T20" id="Seg_172" s="T19">mat-qɨn</ta>
            <ta e="T21" id="Seg_173" s="T20">tʼömna-n</ta>
            <ta e="T22" id="Seg_174" s="T21">je-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_175" s="T1">täbe-qum</ta>
            <ta e="T3" id="Seg_176" s="T2">ippɨ-sɨ</ta>
            <ta e="T4" id="Seg_177" s="T3">innä-lʼ</ta>
            <ta e="T5" id="Seg_178" s="T4">polka-qɨn</ta>
            <ta e="T6" id="Seg_179" s="T5">sanka-m</ta>
            <ta e="T7" id="Seg_180" s="T6">pen-mbɨ-t</ta>
            <ta e="T8" id="Seg_181" s="T7">ut-ɨ-qɨntɨ</ta>
            <ta e="T9" id="Seg_182" s="T8">akoška-un</ta>
            <ta e="T10" id="Seg_183" s="T9">*mantɨ-mbɨ-sɨ</ta>
            <ta e="T11" id="Seg_184" s="T10">täp</ta>
            <ta e="T12" id="Seg_185" s="T11">qaj-m-näj-s</ta>
            <ta e="T13" id="Seg_186" s="T12">qo-nǯir-mbɨ-t</ta>
            <ta e="T14" id="Seg_187" s="T13">sej-ta</ta>
            <ta e="T15" id="Seg_188" s="T14">sej-m</ta>
            <ta e="T16" id="Seg_189" s="T15">čaɣaǯu-mbɨ-t</ta>
            <ta e="T17" id="Seg_190" s="T16">sej-m-ta</ta>
            <ta e="T18" id="Seg_191" s="T17">nʼuː-nɨ-t</ta>
            <ta e="T19" id="Seg_192" s="T18">watt-qɨn</ta>
            <ta e="T20" id="Seg_193" s="T19">maːt-qɨn</ta>
            <ta e="T21" id="Seg_194" s="T20">tʼömna-ŋ</ta>
            <ta e="T22" id="Seg_195" s="T21">eː-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_196" s="T1">man-human.being.[NOM]</ta>
            <ta e="T3" id="Seg_197" s="T2">lie-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_198" s="T3">up-ADJZ</ta>
            <ta e="T5" id="Seg_199" s="T4">berth-LOC</ta>
            <ta e="T6" id="Seg_200" s="T5">chin-ACC</ta>
            <ta e="T7" id="Seg_201" s="T6">put-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_202" s="T7">hand-EP-ILL.3SG</ta>
            <ta e="T9" id="Seg_203" s="T8">window-PROL</ta>
            <ta e="T10" id="Seg_204" s="T9">look-DUR-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_205" s="T10">(s)he.[NOM]</ta>
            <ta e="T12" id="Seg_206" s="T11">what-ACC-EMPH-NEG</ta>
            <ta e="T13" id="Seg_207" s="T12">see-DRV-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_208" s="T13">eye.[NOM]-EMPH2</ta>
            <ta e="T15" id="Seg_209" s="T14">eye-ACC</ta>
            <ta e="T16" id="Seg_210" s="T15">stopper-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_211" s="T16">eye-ACC-EMPH2</ta>
            <ta e="T18" id="Seg_212" s="T17">open-CO-3SG.O</ta>
            <ta e="T19" id="Seg_213" s="T18">road-LOC</ta>
            <ta e="T20" id="Seg_214" s="T19">house-LOC</ta>
            <ta e="T21" id="Seg_215" s="T20">dark-ADVZ</ta>
            <ta e="T22" id="Seg_216" s="T21">be-3SG.S</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_217" s="T1">мужчина-человек.[NOM]</ta>
            <ta e="T3" id="Seg_218" s="T2">лежать-PST.[3SG.S]</ta>
            <ta e="T4" id="Seg_219" s="T3">наверх-ADJZ</ta>
            <ta e="T5" id="Seg_220" s="T4">полка-LOC</ta>
            <ta e="T6" id="Seg_221" s="T5">подбородок-ACC</ta>
            <ta e="T7" id="Seg_222" s="T6">положить-PST.NAR-3SG.O</ta>
            <ta e="T8" id="Seg_223" s="T7">рука-EP-ILL.3SG</ta>
            <ta e="T9" id="Seg_224" s="T8">окно-PROL</ta>
            <ta e="T10" id="Seg_225" s="T9">посмотреть-DUR-PST.[3SG.S]</ta>
            <ta e="T11" id="Seg_226" s="T10">он(а).[NOM]</ta>
            <ta e="T12" id="Seg_227" s="T11">что-ACC-EMPH-NEG</ta>
            <ta e="T13" id="Seg_228" s="T12">увидеть-DRV-PST.NAR-3SG.O</ta>
            <ta e="T14" id="Seg_229" s="T13">глаз.[NOM]-EMPH2</ta>
            <ta e="T15" id="Seg_230" s="T14">глаз-ACC</ta>
            <ta e="T16" id="Seg_231" s="T15">заткнуть-PST.NAR-3SG.O</ta>
            <ta e="T17" id="Seg_232" s="T16">глаз-ACC-EMPH2</ta>
            <ta e="T18" id="Seg_233" s="T17">открыть-CO-3SG.O</ta>
            <ta e="T19" id="Seg_234" s="T18">дорога-LOC</ta>
            <ta e="T20" id="Seg_235" s="T19">дом-LOC</ta>
            <ta e="T21" id="Seg_236" s="T20">темный-ADVZ</ta>
            <ta e="T22" id="Seg_237" s="T21">быть-3SG.S</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_238" s="T1">n-n.[n:case]</ta>
            <ta e="T3" id="Seg_239" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_240" s="T3">adv-adv&gt;adj</ta>
            <ta e="T5" id="Seg_241" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_242" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_243" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_244" s="T7">n-n:ins-n:case.poss</ta>
            <ta e="T9" id="Seg_245" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_246" s="T9">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T11" id="Seg_247" s="T10">pers.[n:case]</ta>
            <ta e="T12" id="Seg_248" s="T11">interrog-n:case-clit-clit</ta>
            <ta e="T13" id="Seg_249" s="T12">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_250" s="T13">n.[n:case]-clit</ta>
            <ta e="T15" id="Seg_251" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_252" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_253" s="T16">n-n:case-clit</ta>
            <ta e="T18" id="Seg_254" s="T17">v-v:ins-v:pn</ta>
            <ta e="T19" id="Seg_255" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_256" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_257" s="T20">adj-adj&gt;adv</ta>
            <ta e="T22" id="Seg_258" s="T21">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_259" s="T1">n</ta>
            <ta e="T3" id="Seg_260" s="T2">v</ta>
            <ta e="T4" id="Seg_261" s="T3">adj</ta>
            <ta e="T5" id="Seg_262" s="T4">n</ta>
            <ta e="T6" id="Seg_263" s="T5">n</ta>
            <ta e="T7" id="Seg_264" s="T6">v</ta>
            <ta e="T8" id="Seg_265" s="T7">n</ta>
            <ta e="T9" id="Seg_266" s="T8">n</ta>
            <ta e="T10" id="Seg_267" s="T9">v</ta>
            <ta e="T11" id="Seg_268" s="T10">pers</ta>
            <ta e="T12" id="Seg_269" s="T11">pro</ta>
            <ta e="T13" id="Seg_270" s="T12">v</ta>
            <ta e="T14" id="Seg_271" s="T13">n</ta>
            <ta e="T15" id="Seg_272" s="T14">n</ta>
            <ta e="T16" id="Seg_273" s="T15">v</ta>
            <ta e="T17" id="Seg_274" s="T16">n</ta>
            <ta e="T18" id="Seg_275" s="T17">v</ta>
            <ta e="T19" id="Seg_276" s="T18">n</ta>
            <ta e="T20" id="Seg_277" s="T19">n</ta>
            <ta e="T21" id="Seg_278" s="T20">adj</ta>
            <ta e="T22" id="Seg_279" s="T21">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_280" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_281" s="T5">RUS:core?</ta>
            <ta e="T9" id="Seg_282" s="T8">RUS:cult</ta>
            <ta e="T14" id="Seg_283" s="T13">RUS:disc</ta>
            <ta e="T17" id="Seg_284" s="T16">RUS:gram</ta>
            <ta e="T21" id="Seg_285" s="T20">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_286" s="T1">A man was lying on an upper berth.</ta>
            <ta e="T10" id="Seg_287" s="T5">He put his chin onto his hands and was looking through the window.</ta>
            <ta e="T13" id="Seg_288" s="T10">He didn't see anything.</ta>
            <ta e="T16" id="Seg_289" s="T13">He closed his eyes.</ta>
            <ta e="T18" id="Seg_290" s="T16">He opened his eyes.</ta>
            <ta e="T22" id="Seg_291" s="T18">It was dark at home (on the road).</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_292" s="T1">Ein Mann lag in der oberen Koje.</ta>
            <ta e="T10" id="Seg_293" s="T5">Er stützte sein Kinn auf seine Hände und sah durchs Fenster.</ta>
            <ta e="T13" id="Seg_294" s="T10">Er sah nichts.</ta>
            <ta e="T16" id="Seg_295" s="T13">Er schloss seine Augen.</ta>
            <ta e="T18" id="Seg_296" s="T16">Er öffnete seine Augen.</ta>
            <ta e="T22" id="Seg_297" s="T18">Es war dunkel zu Hause (auf der Straße).</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_298" s="T1">Мужик лежал на верхней полке.</ta>
            <ta e="T10" id="Seg_299" s="T5">Положил подбородок на руки и смотрел в окно.</ta>
            <ta e="T13" id="Seg_300" s="T10">Он ничего не видел.</ta>
            <ta e="T16" id="Seg_301" s="T13">Глаза закрыл.</ta>
            <ta e="T18" id="Seg_302" s="T16">Глаза открыл.</ta>
            <ta e="T22" id="Seg_303" s="T18">(На дороге) Дома темно.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_304" s="T1">мужик</ta>
            <ta e="T10" id="Seg_305" s="T5">подбородок положил на руки и смотрел в окно</ta>
            <ta e="T13" id="Seg_306" s="T10">но он ничего не видел</ta>
            <ta e="T16" id="Seg_307" s="T13">глаза закрыл</ta>
            <ta e="T18" id="Seg_308" s="T16">глаза открыл</ta>
            <ta e="T22" id="Seg_309" s="T18">дома темно</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
