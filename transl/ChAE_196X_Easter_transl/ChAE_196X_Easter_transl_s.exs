<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_Easter_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_Easter_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">252</ud-information>
            <ud-information attribute-name="# HIAT:w">190</ud-information>
            <ud-information attribute-name="# e">190</ud-information>
            <ud-information attribute-name="# HIAT:u">38</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T191" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Tamdel</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">paska</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">perwɨj</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">del</ts>
                  <nts id="Seg_15" n="HIAT:ip">,</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">paskaj</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">del</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_25" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">Alʼena</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">wazɨmpan</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">ukon</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">qulanando</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_40" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">Alʼena</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">mannɨmbat</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">kutʼen</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">qaj</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">eŋ</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Kɨbačʼelam</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">üːdəmbat</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">nunmaːt</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">nädʼelam</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">üːdembat</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">nuːnmaːt</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_82" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">Ondə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">qwanba</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">sadom</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_93" n="HIAT:w" s="T26">kojalǯiku</ts>
                  <nts id="Seg_94" n="HIAT:ip">.</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_97" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">Tʼeːl</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">qonǯdimban</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">poːn</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">baːroɣondo</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_112" n="HIAT:u" s="T31">
                  <nts id="Seg_113" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">Üːdə</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">qwäntə</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">qari</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">qwäntə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">tʼäŋgu</ts>
                  <nts id="Seg_129" n="HIAT:ip">)</nts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_133" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">Iːri</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">nunmatkunto</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">nunmaːtɨn</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">qwäzɨn</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">ündus</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_151" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">Iːri</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">nunmaːtɨm</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">qwäsɨn</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">qwoːssat</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_166" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">Wes</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">küːɣangeːs</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_175" n="HIAT:u" s="T47">
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">Man</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">moɣolgɨlɨmnan</ts>
                  <nts id="Seg_182" n="HIAT:ip">)</nts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_186" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">Täban</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">sidʼit</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">Alʼönan</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_198" n="HIAT:w" s="T52">sit</ts>
                  <nts id="Seg_199" n="HIAT:ip">)</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_202" n="HIAT:w" s="T53">laɣolɨmba</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">kakbudta</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">pan</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">maʒimbat</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_215" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">Qajno</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_220" n="HIAT:w" s="T58">täbnä</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">aːndalbɨgu</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_227" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_229" n="HIAT:w" s="T60">Täp</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_232" n="HIAT:w" s="T61">qusakanaj</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">azə</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_238" n="HIAT:w" s="T63">andalba</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_242" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">Nano</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">ilɨmban</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">štobə</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">ik</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">qugu</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_260" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_262" n="HIAT:w" s="T69">Ass</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_265" n="HIAT:w" s="T70">kɨgɨmba</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_268" n="HIAT:w" s="T71">qugu</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_272" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">Kɨgɨmba</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">kundə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">ilɨgu</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_284" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_286" n="HIAT:w" s="T75">Täbɨnä</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_289" n="HIAT:w" s="T76">teːrbɨmban</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_292" n="HIAT:w" s="T77">wes</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_295" n="HIAT:w" s="T78">qumba</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_299" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_301" n="HIAT:w" s="T79">Uːdɨmba</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_304" n="HIAT:w" s="T80">čaːnǯaɣatqanda</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_307" n="HIAT:w" s="T81">oralbat</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_310" n="HIAT:w" s="T82">nuːnudimbɨdi</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_314" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_316" n="HIAT:w" s="T83">Iːril</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_319" n="HIAT:w" s="T84">dʼelɨt</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_322" n="HIAT:w" s="T85">tʼeːrbɨmɨndɨt</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_326" n="HIAT:w" s="T86">kundär</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_329" n="HIAT:w" s="T87">iːrə</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">warkɨmbattə</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_336" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_338" n="HIAT:w" s="T89">A</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_341" n="HIAT:w" s="T90">teper</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_344" n="HIAT:w" s="T91">teːrban</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_348" n="HIAT:w" s="T92">kundär</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_351" n="HIAT:w" s="T93">teper</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_354" n="HIAT:w" s="T94">warkugu</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_358" n="HIAT:w" s="T95">qaim</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">meːkugu</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_365" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_367" n="HIAT:w" s="T97">Sadon</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_370" n="HIAT:w" s="T98">qöɣɨn</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_373" n="HIAT:w" s="T99">čaǯimban</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_376" n="HIAT:w" s="T100">pajaga</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_380" n="HIAT:w" s="T101">espe</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_383" n="HIAT:w" s="T102">čenǯindɨ</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_387" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_389" n="HIAT:w" s="T103">Manaj</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_392" n="HIAT:w" s="T104">ɣundari</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_395" n="HIAT:w" s="T105">kuːn</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_398" n="HIAT:w" s="T106">dʼät</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_401" n="HIAT:w" s="T107">čeːnčin</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_405" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_407" n="HIAT:w" s="T108">Pajaga</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_410" n="HIAT:w" s="T109">uːtərɨŋ</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_414" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_416" n="HIAT:w" s="T110">Pajaga</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_419" n="HIAT:w" s="T111">čaːǯilʼe</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_422" n="HIAT:w" s="T112">nɨlʼedimba</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_426" n="HIAT:u" s="T113">
                  <nts id="Seg_427" n="HIAT:ip">“</nts>
                  <ts e="T114" id="Seg_429" n="HIAT:w" s="T113">Nunmaːttə</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_432" n="HIAT:w" s="T114">čaːǯandə</ts>
                  <nts id="Seg_433" n="HIAT:ip">,</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_436" n="HIAT:w" s="T115">tan</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_439" n="HIAT:w" s="T116">näden</ts>
                  <nts id="Seg_440" n="HIAT:ip">?</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_443" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_445" n="HIAT:w" s="T117">Pogodʼälaq</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_448" n="HIAT:w" s="T118">qwalakse</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_451" n="HIAT:w" s="T119">nunmat</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_455" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_457" n="HIAT:w" s="T120">Sejčʼas</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_460" n="HIAT:w" s="T121">ertä</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_463" n="HIAT:w" s="T122">eŋ</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_466" n="HIAT:w" s="T123">qwaŋgu</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_469" n="HIAT:w" s="T124">nunmattə</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_473" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_475" n="HIAT:w" s="T125">Telʼdʼen</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_478" n="HIAT:w" s="T126">seːrkuzaŋ</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_481" n="HIAT:w" s="T127">okɨr</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_484" n="HIAT:w" s="T128">näjɣunä</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_487" n="HIAT:w" s="T129">aːmdedʼigu</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_491" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_493" n="HIAT:w" s="T130">Nanä</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_496" n="HIAT:w" s="T131">näjɣum</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_499" n="HIAT:w" s="T132">äːʒulguŋ</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_502" n="HIAT:w" s="T133">mekka</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_506" n="HIAT:w" s="T134">štob</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_509" n="HIAT:w" s="T135">nuːnmat</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_512" n="HIAT:w" s="T136">köːskugu</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">nunmattə</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_519" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_521" n="HIAT:w" s="T138">Nunmatqɨn</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_524" n="HIAT:w" s="T139">tekka</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_527" n="HIAT:w" s="T140">kɨgattə</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_530" n="HIAT:w" s="T141">qaimdə</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_533" n="HIAT:w" s="T142">ketku</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_536" n="HIAT:w" s="T143">soːdʼiga</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_540" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_542" n="HIAT:w" s="T144">Täbla</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_545" n="HIAT:w" s="T145">nasaŋ</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_548" n="HIAT:w" s="T146">potpat</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_551" n="HIAT:w" s="T147">apsodimɨm</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_555" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_557" n="HIAT:w" s="T148">Aptiŋ</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_560" n="HIAT:w" s="T149">kundokt</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_563" n="HIAT:w" s="T150">apsodimə</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_567" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_569" n="HIAT:w" s="T151">Müzurumbattə</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_572" n="HIAT:w" s="T152">wadʼi</ts>
                  <nts id="Seg_573" n="HIAT:ip">,</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_576" n="HIAT:w" s="T153">qwäːl</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_580" n="HIAT:w" s="T154">čagarɨmbɨdi</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_583" n="HIAT:w" s="T155">qwäl</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_587" n="HIAT:w" s="T156">nʼäj</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_590" n="HIAT:w" s="T157">memɨndat</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_593" n="HIAT:w" s="T158">soːdiga</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_596" n="HIAT:w" s="T159">müzurumbattə</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_600" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_602" n="HIAT:w" s="T160">Kut</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_605" n="HIAT:w" s="T161">tüːmandan</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_608" n="HIAT:w" s="T162">täɣɨndältə</ts>
                  <nts id="Seg_609" n="HIAT:ip">?</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_612" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_614" n="HIAT:w" s="T163">Teblanmandə</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_617" n="HIAT:w" s="T164">tüːmban</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_620" n="HIAT:w" s="T165">qajda</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_623" n="HIAT:w" s="T166">qum</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_627" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_629" n="HIAT:w" s="T167">Nʼänʼät</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_632" n="HIAT:w" s="T168">tüːmand</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_635" n="HIAT:w" s="T169">quːbi</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_638" n="HIAT:w" s="T170">aːbant</ts>
                  <nts id="Seg_639" n="HIAT:ip">,</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_642" n="HIAT:w" s="T171">ass</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_645" n="HIAT:w" s="T172">nädəmbidi</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_648" n="HIAT:w" s="T173">üčʼide</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_651" n="HIAT:w" s="T174">tibeɣum</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_654" n="HIAT:w" s="T175">tüːmɨndan</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_657" n="HIAT:w" s="T176">kündɨze</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_660" n="HIAT:w" s="T177">künnɨn</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_663" n="HIAT:w" s="T178">paroɣɨn</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_666" n="HIAT:w" s="T179">aːmdɨlʼe</ts>
                  <nts id="Seg_667" n="HIAT:ip">,</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_670" n="HIAT:w" s="T180">soːgɨndɨze</ts>
                  <nts id="Seg_671" n="HIAT:ip">,</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_674" n="HIAT:w" s="T181">a</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_677" n="HIAT:w" s="T182">üːgɨt</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_680" n="HIAT:w" s="T183">seːraj</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_684" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_686" n="HIAT:w" s="T184">Pajamban</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_689" n="HIAT:w" s="T185">warɣɨn</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_692" n="HIAT:w" s="T186">i</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_695" n="HIAT:w" s="T187">aulčukow</ts>
                  <nts id="Seg_696" n="HIAT:ip">,</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_699" n="HIAT:w" s="T188">kundär</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_702" n="HIAT:w" s="T189">täbɨm</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_705" n="HIAT:w" s="T190">qwärat</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip">”</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T191" id="Seg_709" n="sc" s="T1">
               <ts e="T2" id="Seg_711" n="e" s="T1">Tamdel </ts>
               <ts e="T3" id="Seg_713" n="e" s="T2">paska, </ts>
               <ts e="T4" id="Seg_715" n="e" s="T3">perwɨj </ts>
               <ts e="T5" id="Seg_717" n="e" s="T4">del, </ts>
               <ts e="T6" id="Seg_719" n="e" s="T5">paskaj </ts>
               <ts e="T7" id="Seg_721" n="e" s="T6">del. </ts>
               <ts e="T8" id="Seg_723" n="e" s="T7">Alʼena </ts>
               <ts e="T9" id="Seg_725" n="e" s="T8">wazɨmpan </ts>
               <ts e="T10" id="Seg_727" n="e" s="T9">ukon </ts>
               <ts e="T11" id="Seg_729" n="e" s="T10">qulanando. </ts>
               <ts e="T12" id="Seg_731" n="e" s="T11">Alʼena </ts>
               <ts e="T13" id="Seg_733" n="e" s="T12">mannɨmbat </ts>
               <ts e="T14" id="Seg_735" n="e" s="T13">kutʼen </ts>
               <ts e="T15" id="Seg_737" n="e" s="T14">qaj </ts>
               <ts e="T16" id="Seg_739" n="e" s="T15">eŋ. </ts>
               <ts e="T17" id="Seg_741" n="e" s="T16">Kɨbačʼelam </ts>
               <ts e="T18" id="Seg_743" n="e" s="T17">üːdəmbat </ts>
               <ts e="T19" id="Seg_745" n="e" s="T18">nunmaːt </ts>
               <ts e="T20" id="Seg_747" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_749" n="e" s="T20">nädʼelam </ts>
               <ts e="T22" id="Seg_751" n="e" s="T21">üːdembat </ts>
               <ts e="T23" id="Seg_753" n="e" s="T22">nuːnmaːt. </ts>
               <ts e="T24" id="Seg_755" n="e" s="T23">Ondə </ts>
               <ts e="T25" id="Seg_757" n="e" s="T24">qwanba </ts>
               <ts e="T26" id="Seg_759" n="e" s="T25">sadom </ts>
               <ts e="T27" id="Seg_761" n="e" s="T26">kojalǯiku. </ts>
               <ts e="T28" id="Seg_763" n="e" s="T27">Tʼeːl </ts>
               <ts e="T29" id="Seg_765" n="e" s="T28">qonǯdimban </ts>
               <ts e="T30" id="Seg_767" n="e" s="T29">poːn </ts>
               <ts e="T31" id="Seg_769" n="e" s="T30">baːroɣondo. </ts>
               <ts e="T32" id="Seg_771" n="e" s="T31">(Üːdə </ts>
               <ts e="T33" id="Seg_773" n="e" s="T32">qwäntə, </ts>
               <ts e="T34" id="Seg_775" n="e" s="T33">qari </ts>
               <ts e="T35" id="Seg_777" n="e" s="T34">qwäntə </ts>
               <ts e="T36" id="Seg_779" n="e" s="T35">tʼäŋgu). </ts>
               <ts e="T37" id="Seg_781" n="e" s="T36">Iːri </ts>
               <ts e="T38" id="Seg_783" n="e" s="T37">nunmatkunto </ts>
               <ts e="T39" id="Seg_785" n="e" s="T38">nunmaːtɨn </ts>
               <ts e="T40" id="Seg_787" n="e" s="T39">qwäzɨn </ts>
               <ts e="T41" id="Seg_789" n="e" s="T40">ündus. </ts>
               <ts e="T42" id="Seg_791" n="e" s="T41">Iːri </ts>
               <ts e="T43" id="Seg_793" n="e" s="T42">nunmaːtɨm </ts>
               <ts e="T44" id="Seg_795" n="e" s="T43">qwäsɨn </ts>
               <ts e="T45" id="Seg_797" n="e" s="T44">qwoːssat. </ts>
               <ts e="T46" id="Seg_799" n="e" s="T45">Wes </ts>
               <ts e="T47" id="Seg_801" n="e" s="T46">küːɣangeːs. </ts>
               <ts e="T48" id="Seg_803" n="e" s="T47">(Man </ts>
               <ts e="T49" id="Seg_805" n="e" s="T48">moɣolgɨlɨmnan). </ts>
               <ts e="T50" id="Seg_807" n="e" s="T49">Täban </ts>
               <ts e="T51" id="Seg_809" n="e" s="T50">sidʼit </ts>
               <ts e="T52" id="Seg_811" n="e" s="T51">(Alʼönan </ts>
               <ts e="T53" id="Seg_813" n="e" s="T52">sit) </ts>
               <ts e="T54" id="Seg_815" n="e" s="T53">laɣolɨmba </ts>
               <ts e="T55" id="Seg_817" n="e" s="T54">kakbudta </ts>
               <ts e="T56" id="Seg_819" n="e" s="T55">pan </ts>
               <ts e="T57" id="Seg_821" n="e" s="T56">maʒimbat. </ts>
               <ts e="T58" id="Seg_823" n="e" s="T57">Qajno </ts>
               <ts e="T59" id="Seg_825" n="e" s="T58">täbnä </ts>
               <ts e="T60" id="Seg_827" n="e" s="T59">aːndalbɨgu. </ts>
               <ts e="T61" id="Seg_829" n="e" s="T60">Täp </ts>
               <ts e="T62" id="Seg_831" n="e" s="T61">qusakanaj </ts>
               <ts e="T63" id="Seg_833" n="e" s="T62">azə </ts>
               <ts e="T64" id="Seg_835" n="e" s="T63">andalba. </ts>
               <ts e="T65" id="Seg_837" n="e" s="T64">Nano </ts>
               <ts e="T66" id="Seg_839" n="e" s="T65">ilɨmban </ts>
               <ts e="T67" id="Seg_841" n="e" s="T66">štobə </ts>
               <ts e="T68" id="Seg_843" n="e" s="T67">ik </ts>
               <ts e="T69" id="Seg_845" n="e" s="T68">qugu. </ts>
               <ts e="T70" id="Seg_847" n="e" s="T69">Ass </ts>
               <ts e="T71" id="Seg_849" n="e" s="T70">kɨgɨmba </ts>
               <ts e="T72" id="Seg_851" n="e" s="T71">qugu. </ts>
               <ts e="T73" id="Seg_853" n="e" s="T72">Kɨgɨmba </ts>
               <ts e="T74" id="Seg_855" n="e" s="T73">kundə </ts>
               <ts e="T75" id="Seg_857" n="e" s="T74">ilɨgu. </ts>
               <ts e="T76" id="Seg_859" n="e" s="T75">Täbɨnä </ts>
               <ts e="T77" id="Seg_861" n="e" s="T76">teːrbɨmban </ts>
               <ts e="T78" id="Seg_863" n="e" s="T77">wes </ts>
               <ts e="T79" id="Seg_865" n="e" s="T78">qumba. </ts>
               <ts e="T80" id="Seg_867" n="e" s="T79">Uːdɨmba </ts>
               <ts e="T81" id="Seg_869" n="e" s="T80">čaːnǯaɣatqanda </ts>
               <ts e="T82" id="Seg_871" n="e" s="T81">oralbat </ts>
               <ts e="T83" id="Seg_873" n="e" s="T82">nuːnudimbɨdi. </ts>
               <ts e="T84" id="Seg_875" n="e" s="T83">Iːril </ts>
               <ts e="T85" id="Seg_877" n="e" s="T84">dʼelɨt </ts>
               <ts e="T86" id="Seg_879" n="e" s="T85">tʼeːrbɨmɨndɨt, </ts>
               <ts e="T87" id="Seg_881" n="e" s="T86">kundär </ts>
               <ts e="T88" id="Seg_883" n="e" s="T87">iːrə </ts>
               <ts e="T89" id="Seg_885" n="e" s="T88">warkɨmbattə. </ts>
               <ts e="T90" id="Seg_887" n="e" s="T89">A </ts>
               <ts e="T91" id="Seg_889" n="e" s="T90">teper </ts>
               <ts e="T92" id="Seg_891" n="e" s="T91">teːrban, </ts>
               <ts e="T93" id="Seg_893" n="e" s="T92">kundär </ts>
               <ts e="T94" id="Seg_895" n="e" s="T93">teper </ts>
               <ts e="T95" id="Seg_897" n="e" s="T94">warkugu, </ts>
               <ts e="T96" id="Seg_899" n="e" s="T95">qaim </ts>
               <ts e="T97" id="Seg_901" n="e" s="T96">meːkugu. </ts>
               <ts e="T98" id="Seg_903" n="e" s="T97">Sadon </ts>
               <ts e="T99" id="Seg_905" n="e" s="T98">qöɣɨn </ts>
               <ts e="T100" id="Seg_907" n="e" s="T99">čaǯimban </ts>
               <ts e="T101" id="Seg_909" n="e" s="T100">pajaga, </ts>
               <ts e="T102" id="Seg_911" n="e" s="T101">espe </ts>
               <ts e="T103" id="Seg_913" n="e" s="T102">čenǯindɨ. </ts>
               <ts e="T104" id="Seg_915" n="e" s="T103">Manaj </ts>
               <ts e="T105" id="Seg_917" n="e" s="T104">ɣundari </ts>
               <ts e="T106" id="Seg_919" n="e" s="T105">kuːn </ts>
               <ts e="T107" id="Seg_921" n="e" s="T106">dʼät </ts>
               <ts e="T108" id="Seg_923" n="e" s="T107">čeːnčin. </ts>
               <ts e="T109" id="Seg_925" n="e" s="T108">Pajaga </ts>
               <ts e="T110" id="Seg_927" n="e" s="T109">uːtərɨŋ. </ts>
               <ts e="T111" id="Seg_929" n="e" s="T110">Pajaga </ts>
               <ts e="T112" id="Seg_931" n="e" s="T111">čaːǯilʼe </ts>
               <ts e="T113" id="Seg_933" n="e" s="T112">nɨlʼedimba. </ts>
               <ts e="T114" id="Seg_935" n="e" s="T113">“Nunmaːttə </ts>
               <ts e="T115" id="Seg_937" n="e" s="T114">čaːǯandə, </ts>
               <ts e="T116" id="Seg_939" n="e" s="T115">tan </ts>
               <ts e="T117" id="Seg_941" n="e" s="T116">näden? </ts>
               <ts e="T118" id="Seg_943" n="e" s="T117">Pogodʼälaq </ts>
               <ts e="T119" id="Seg_945" n="e" s="T118">qwalakse </ts>
               <ts e="T120" id="Seg_947" n="e" s="T119">nunmat. </ts>
               <ts e="T121" id="Seg_949" n="e" s="T120">Sejčʼas </ts>
               <ts e="T122" id="Seg_951" n="e" s="T121">ertä </ts>
               <ts e="T123" id="Seg_953" n="e" s="T122">eŋ </ts>
               <ts e="T124" id="Seg_955" n="e" s="T123">qwaŋgu </ts>
               <ts e="T125" id="Seg_957" n="e" s="T124">nunmattə. </ts>
               <ts e="T126" id="Seg_959" n="e" s="T125">Telʼdʼen </ts>
               <ts e="T127" id="Seg_961" n="e" s="T126">seːrkuzaŋ </ts>
               <ts e="T128" id="Seg_963" n="e" s="T127">okɨr </ts>
               <ts e="T129" id="Seg_965" n="e" s="T128">näjɣunä </ts>
               <ts e="T130" id="Seg_967" n="e" s="T129">aːmdedʼigu. </ts>
               <ts e="T131" id="Seg_969" n="e" s="T130">Nanä </ts>
               <ts e="T132" id="Seg_971" n="e" s="T131">näjɣum </ts>
               <ts e="T133" id="Seg_973" n="e" s="T132">äːʒulguŋ </ts>
               <ts e="T134" id="Seg_975" n="e" s="T133">mekka, </ts>
               <ts e="T135" id="Seg_977" n="e" s="T134">štob </ts>
               <ts e="T136" id="Seg_979" n="e" s="T135">nuːnmat </ts>
               <ts e="T137" id="Seg_981" n="e" s="T136">köːskugu </ts>
               <ts e="T138" id="Seg_983" n="e" s="T137">nunmattə. </ts>
               <ts e="T139" id="Seg_985" n="e" s="T138">Nunmatqɨn </ts>
               <ts e="T140" id="Seg_987" n="e" s="T139">tekka </ts>
               <ts e="T141" id="Seg_989" n="e" s="T140">kɨgattə </ts>
               <ts e="T142" id="Seg_991" n="e" s="T141">qaimdə </ts>
               <ts e="T143" id="Seg_993" n="e" s="T142">ketku </ts>
               <ts e="T144" id="Seg_995" n="e" s="T143">soːdʼiga. </ts>
               <ts e="T145" id="Seg_997" n="e" s="T144">Täbla </ts>
               <ts e="T146" id="Seg_999" n="e" s="T145">nasaŋ </ts>
               <ts e="T147" id="Seg_1001" n="e" s="T146">potpat </ts>
               <ts e="T148" id="Seg_1003" n="e" s="T147">apsodimɨm. </ts>
               <ts e="T149" id="Seg_1005" n="e" s="T148">Aptiŋ </ts>
               <ts e="T150" id="Seg_1007" n="e" s="T149">kundokt </ts>
               <ts e="T151" id="Seg_1009" n="e" s="T150">apsodimə. </ts>
               <ts e="T152" id="Seg_1011" n="e" s="T151">Müzurumbattə </ts>
               <ts e="T153" id="Seg_1013" n="e" s="T152">wadʼi, </ts>
               <ts e="T154" id="Seg_1015" n="e" s="T153">qwäːl, </ts>
               <ts e="T155" id="Seg_1017" n="e" s="T154">čagarɨmbɨdi </ts>
               <ts e="T156" id="Seg_1019" n="e" s="T155">qwäl, </ts>
               <ts e="T157" id="Seg_1021" n="e" s="T156">nʼäj </ts>
               <ts e="T158" id="Seg_1023" n="e" s="T157">memɨndat </ts>
               <ts e="T159" id="Seg_1025" n="e" s="T158">soːdiga </ts>
               <ts e="T160" id="Seg_1027" n="e" s="T159">müzurumbattə. </ts>
               <ts e="T161" id="Seg_1029" n="e" s="T160">Kut </ts>
               <ts e="T162" id="Seg_1031" n="e" s="T161">tüːmandan </ts>
               <ts e="T163" id="Seg_1033" n="e" s="T162">täɣɨndältə? </ts>
               <ts e="T164" id="Seg_1035" n="e" s="T163">Teblanmandə </ts>
               <ts e="T165" id="Seg_1037" n="e" s="T164">tüːmban </ts>
               <ts e="T166" id="Seg_1039" n="e" s="T165">qajda </ts>
               <ts e="T167" id="Seg_1041" n="e" s="T166">qum. </ts>
               <ts e="T168" id="Seg_1043" n="e" s="T167">Nʼänʼät </ts>
               <ts e="T169" id="Seg_1045" n="e" s="T168">tüːmand </ts>
               <ts e="T170" id="Seg_1047" n="e" s="T169">quːbi </ts>
               <ts e="T171" id="Seg_1049" n="e" s="T170">aːbant, </ts>
               <ts e="T172" id="Seg_1051" n="e" s="T171">ass </ts>
               <ts e="T173" id="Seg_1053" n="e" s="T172">nädəmbidi </ts>
               <ts e="T174" id="Seg_1055" n="e" s="T173">üčʼide </ts>
               <ts e="T175" id="Seg_1057" n="e" s="T174">tibeɣum </ts>
               <ts e="T176" id="Seg_1059" n="e" s="T175">tüːmɨndan </ts>
               <ts e="T177" id="Seg_1061" n="e" s="T176">kündɨze </ts>
               <ts e="T178" id="Seg_1063" n="e" s="T177">künnɨn </ts>
               <ts e="T179" id="Seg_1065" n="e" s="T178">paroɣɨn </ts>
               <ts e="T180" id="Seg_1067" n="e" s="T179">aːmdɨlʼe, </ts>
               <ts e="T181" id="Seg_1069" n="e" s="T180">soːgɨndɨze, </ts>
               <ts e="T182" id="Seg_1071" n="e" s="T181">a </ts>
               <ts e="T183" id="Seg_1073" n="e" s="T182">üːgɨt </ts>
               <ts e="T184" id="Seg_1075" n="e" s="T183">seːraj. </ts>
               <ts e="T185" id="Seg_1077" n="e" s="T184">Pajamban </ts>
               <ts e="T186" id="Seg_1079" n="e" s="T185">warɣɨn </ts>
               <ts e="T187" id="Seg_1081" n="e" s="T186">i </ts>
               <ts e="T188" id="Seg_1083" n="e" s="T187">aulčukow, </ts>
               <ts e="T189" id="Seg_1085" n="e" s="T188">kundär </ts>
               <ts e="T190" id="Seg_1087" n="e" s="T189">täbɨm </ts>
               <ts e="T191" id="Seg_1089" n="e" s="T190">qwärat.” </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_1090" s="T1">ChAE_196X_Easter_transl.001 (001.001)</ta>
            <ta e="T11" id="Seg_1091" s="T7">ChAE_196X_Easter_transl.002 (001.002)</ta>
            <ta e="T16" id="Seg_1092" s="T11">ChAE_196X_Easter_transl.003 (001.003)</ta>
            <ta e="T23" id="Seg_1093" s="T16">ChAE_196X_Easter_transl.004 (001.004)</ta>
            <ta e="T27" id="Seg_1094" s="T23">ChAE_196X_Easter_transl.005 (001.005)</ta>
            <ta e="T31" id="Seg_1095" s="T27">ChAE_196X_Easter_transl.006 (001.006)</ta>
            <ta e="T36" id="Seg_1096" s="T31">ChAE_196X_Easter_transl.007 (001.007)</ta>
            <ta e="T41" id="Seg_1097" s="T36">ChAE_196X_Easter_transl.008 (001.008)</ta>
            <ta e="T45" id="Seg_1098" s="T41">ChAE_196X_Easter_transl.009 (001.009)</ta>
            <ta e="T47" id="Seg_1099" s="T45">ChAE_196X_Easter_transl.010 (001.010)</ta>
            <ta e="T49" id="Seg_1100" s="T47">ChAE_196X_Easter_transl.011 (001.011)</ta>
            <ta e="T57" id="Seg_1101" s="T49">ChAE_196X_Easter_transl.012 (001.012)</ta>
            <ta e="T60" id="Seg_1102" s="T57">ChAE_196X_Easter_transl.013 (001.013)</ta>
            <ta e="T64" id="Seg_1103" s="T60">ChAE_196X_Easter_transl.014 (001.014)</ta>
            <ta e="T69" id="Seg_1104" s="T64">ChAE_196X_Easter_transl.015 (001.015)</ta>
            <ta e="T72" id="Seg_1105" s="T69">ChAE_196X_Easter_transl.016 (001.016)</ta>
            <ta e="T75" id="Seg_1106" s="T72">ChAE_196X_Easter_transl.017 (001.017)</ta>
            <ta e="T79" id="Seg_1107" s="T75">ChAE_196X_Easter_transl.018 (001.018)</ta>
            <ta e="T83" id="Seg_1108" s="T79">ChAE_196X_Easter_transl.019 (001.019)</ta>
            <ta e="T89" id="Seg_1109" s="T83">ChAE_196X_Easter_transl.020 (001.020)</ta>
            <ta e="T97" id="Seg_1110" s="T89">ChAE_196X_Easter_transl.021 (001.021)</ta>
            <ta e="T103" id="Seg_1111" s="T97">ChAE_196X_Easter_transl.022 (001.022)</ta>
            <ta e="T108" id="Seg_1112" s="T103">ChAE_196X_Easter_transl.023 (001.023)</ta>
            <ta e="T110" id="Seg_1113" s="T108">ChAE_196X_Easter_transl.024 (001.024)</ta>
            <ta e="T113" id="Seg_1114" s="T110">ChAE_196X_Easter_transl.025 (001.025)</ta>
            <ta e="T117" id="Seg_1115" s="T113">ChAE_196X_Easter_transl.026 (001.026)</ta>
            <ta e="T120" id="Seg_1116" s="T117">ChAE_196X_Easter_transl.027 (001.027)</ta>
            <ta e="T125" id="Seg_1117" s="T120">ChAE_196X_Easter_transl.028 (001.028)</ta>
            <ta e="T130" id="Seg_1118" s="T125">ChAE_196X_Easter_transl.029 (001.029)</ta>
            <ta e="T138" id="Seg_1119" s="T130">ChAE_196X_Easter_transl.030 (001.030)</ta>
            <ta e="T144" id="Seg_1120" s="T138">ChAE_196X_Easter_transl.031 (001.031)</ta>
            <ta e="T148" id="Seg_1121" s="T144">ChAE_196X_Easter_transl.032 (001.032)</ta>
            <ta e="T151" id="Seg_1122" s="T148">ChAE_196X_Easter_transl.033 (001.033)</ta>
            <ta e="T160" id="Seg_1123" s="T151">ChAE_196X_Easter_transl.034 (001.034)</ta>
            <ta e="T163" id="Seg_1124" s="T160">ChAE_196X_Easter_transl.035 (001.035)</ta>
            <ta e="T167" id="Seg_1125" s="T163">ChAE_196X_Easter_transl.036 (001.036)</ta>
            <ta e="T184" id="Seg_1126" s="T167">ChAE_196X_Easter_transl.037 (001.037)</ta>
            <ta e="T191" id="Seg_1127" s="T184">ChAE_196X_Easter_transl.038 (001.038)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_1128" s="T1">тамдел па′ска, первый дел, ′паскай дел.</ta>
            <ta e="T11" id="Seg_1129" s="T7">А′лʼена вазым′пан у′кон kу′ланандо.</ta>
            <ta e="T16" id="Seg_1130" s="T11">А′лʼена манным′б̂ат ку′тʼен kай ′ең.</ta>
            <ta e="T23" id="Seg_1131" s="T16">кы′ба ′челам ӱ̄дъм′бат ′нунма̄т и нӓ′дʼелам ӱ̄дембат ‵нӯн′ма̄т.</ta>
            <ta e="T27" id="Seg_1132" s="T23">′ондъ kwан′ба ′садом ко′jалджику.</ta>
            <ta e="T31" id="Seg_1133" s="T27">тʼе̄л kондж(тш)дим′б̂ан ′по̄н ′ба̄роɣондо.</ta>
            <ta e="T36" id="Seg_1134" s="T31">(′ӱ̄дъ kwӓнтъ, ′kари ′kwӓнтъ тʼӓңгу.)</ta>
            <ta e="T41" id="Seg_1135" s="T36">ӣри нун′маткунто нун′ма̄тын ′kwӓзын ′ӱндус.</ta>
            <ta e="T45" id="Seg_1136" s="T41">′ӣри нун′ма̄тым kwӓсын ′kwо̄с(с)ат.</ta>
            <ta e="T47" id="Seg_1137" s="T45">вес ′кӱ̄ɣан′ге̄с.</ta>
            <ta e="T49" id="Seg_1138" s="T47">(ман мо′ɣолгылымнан.)</ta>
            <ta e="T57" id="Seg_1139" s="T49">′тӓбан ′си′дʼит (А′лʼӧнан сит) ла′ɣолымба как будта ′пан мажим′бат.</ta>
            <ta e="T60" id="Seg_1140" s="T57">kай′но тӓб′нӓ ′а̄ндалбыгу.</ta>
            <ta e="T64" id="Seg_1141" s="T60">тӓп kу′саканай азъ ′андалба.</ta>
            <ta e="T69" id="Seg_1142" s="T64">на′но илым′бан ′штобъ ′ик ′kугу.</ta>
            <ta e="T72" id="Seg_1143" s="T69">асс кыгым′ба ′kугу.</ta>
            <ta e="T75" id="Seg_1144" s="T72">кыгым′ба ′кундъ ′илыгу.</ta>
            <ta e="T79" id="Seg_1145" s="T75">тӓбы′нӓ ′те̄рбымбан вес ′kумба.</ta>
            <ta e="T83" id="Seg_1146" s="T79">′ӯдымба ′тша̄нджаɣатkанда о′ралбат ′нӯнудимбыди.</ta>
            <ta e="T89" id="Seg_1147" s="T83">′ӣрил ′д̂ʼелыт ′тʼе̄рбымындыт, кун′дӓр ′ӣръ варкым′баттъ.</ta>
            <ta e="T97" id="Seg_1148" s="T89">а те′пер ′те̄рбан, кун′дӓр те′пер варку′гу, kа′им ′ме̄кугу.</ta>
            <ta e="T103" id="Seg_1149" s="T97">′садон ′kӧɣын ′тшаджимбан па′jага, ес′пе тшенджинды.</ta>
            <ta e="T108" id="Seg_1150" s="T103">′манай ′ɣундари кӯн′дʼӓт ′тше̄нтшин.</ta>
            <ta e="T110" id="Seg_1151" s="T108">па′jага ӯтъ′рың.</ta>
            <ta e="T113" id="Seg_1152" s="T110">па′jага ′тша̄джилʼе ны′лʼедимба.</ta>
            <ta e="T117" id="Seg_1153" s="T113">′нунма̄ттъ ′тша̄джандъ, тан нӓ′де̨н?</ta>
            <ta e="T120" id="Seg_1154" s="T117">пого′дʼӓ лаk kwа′лаксе нунмат.</ta>
            <ta e="T125" id="Seg_1155" s="T120">сей′час ер′тӓ ең kwаңгу нун′маттъ.</ta>
            <ta e="T130" id="Seg_1156" s="T125">′телʼдʼен ′се̄ркузаң окыр ′нӓйɣунӓ а̄мдедʼигу.</ta>
            <ta e="T138" id="Seg_1157" s="T130">на′нӓ нӓй′ɣум ′ӓ̄жулгуң мекка, штоб ′нӯнмат ′кӧ̄ску′гу ′нунматтъ.</ta>
            <ta e="T144" id="Seg_1158" s="T138">′нунматkын ′текка кы′гаттъ kаимдъ кет′ку ′со̄дʼига.</ta>
            <ta e="T148" id="Seg_1159" s="T144">тӓб′ла на′саң ′потпат ап′содимым.</ta>
            <ta e="T151" id="Seg_1160" s="T148">ап′тиң кун′докт ап′содимъ.</ta>
            <ta e="T160" id="Seg_1161" s="T151">‵мӱзурум′баттъ ва′дʼи, kwӓ̄л, тшага′рымбыди ′kwӓл, ′нʼӓй мемындат ′со̄дига ‵мӱзурум′баттъ.</ta>
            <ta e="T163" id="Seg_1162" s="T160">кут тӱ̄мандан ‵тӓɣын′дӓлтъ?</ta>
            <ta e="T167" id="Seg_1163" s="T163">теб′лан‵мандъ ′тӱ̄мбан kай′да kум.</ta>
            <ta e="T184" id="Seg_1164" s="T167">нʼӓ′нʼӓт тӱ̄манд kӯби а̄′бант, асс ′нӓдъмбиди ӱчи′де ти′беɣум ′тӱ̄мындан кӱнды′зе ′кӱннын ′пароɣын ′а̄мдылʼе, со̄гындызе, а ′ӱ̄гыт ′се̄рай.</ta>
            <ta e="T191" id="Seg_1165" s="T184">па′jамбан вар′ɣын и ауlтшу′коw, кун′дӓр ′тӓбым ′kwӓрат.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_1166" s="T1">tamdel paska, perwɨj del, paskaj del.</ta>
            <ta e="T11" id="Seg_1167" s="T7">Аlʼena wazɨmpan ukon qulanando.</ta>
            <ta e="T16" id="Seg_1168" s="T11">Аlʼena mannɨmb̂at kutʼen qaj eŋ.</ta>
            <ta e="T23" id="Seg_1169" s="T16">kɨba čʼelam üːdəmbat nunmaːt i nädʼelam üːdembat nuːnmaːt.</ta>
            <ta e="T27" id="Seg_1170" s="T23">ondə qwanba sadom kojalǯiku.</ta>
            <ta e="T31" id="Seg_1171" s="T27">tʼeːl qonǯ(č)dimb̂an poːn baːroɣondo.</ta>
            <ta e="T36" id="Seg_1172" s="T31">(üːdə qwäntə, qari qwäntə tʼäŋgu.)</ta>
            <ta e="T41" id="Seg_1173" s="T36">iːri nunmatkunto nunmaːtɨn qwäzɨn ündus.</ta>
            <ta e="T45" id="Seg_1174" s="T41">iːri nunmaːtɨm qwäsɨn qwoːs(s)at.</ta>
            <ta e="T47" id="Seg_1175" s="T45">wes küːɣangeːs.</ta>
            <ta e="T49" id="Seg_1176" s="T47">(man moɣolgɨlɨmnan.)</ta>
            <ta e="T57" id="Seg_1177" s="T49">täban sidʼit (Аlʼönan sit) laɣolɨmba kak budta pan maʒimbat.</ta>
            <ta e="T60" id="Seg_1178" s="T57">qajno täbnä aːndalbɨgu.</ta>
            <ta e="T64" id="Seg_1179" s="T60">täp qusakanaj azə andalba.</ta>
            <ta e="T69" id="Seg_1180" s="T64">nano ilɨmban štobə ik qugu.</ta>
            <ta e="T72" id="Seg_1181" s="T69">ass kɨgɨmba qugu.</ta>
            <ta e="T75" id="Seg_1182" s="T72">kɨgɨmba kundə ilɨgu.</ta>
            <ta e="T79" id="Seg_1183" s="T75">täbɨnä teːrbɨmban wes qumba.</ta>
            <ta e="T83" id="Seg_1184" s="T79">uːdɨmba čaːnǯaɣatqanda oralbat nuːnudimbɨdi.</ta>
            <ta e="T89" id="Seg_1185" s="T83">iːril d̂ʼelɨt tʼeːrbɨmɨndɨt, kundär iːrə warkɨmbattə.</ta>
            <ta e="T97" id="Seg_1186" s="T89">a teper teːrban, kundär teper warkugu, qaim meːkugu.</ta>
            <ta e="T103" id="Seg_1187" s="T97">sadon qöɣɨn čaǯimban pajaga, espe čenǯindɨ.</ta>
            <ta e="T108" id="Seg_1188" s="T103">manaj ɣundari kuːndʼät čeːnčin.</ta>
            <ta e="T110" id="Seg_1189" s="T108">pajaga uːtərɨŋ.</ta>
            <ta e="T113" id="Seg_1190" s="T110">pajaga čaːǯilʼe nɨlʼedimba.</ta>
            <ta e="T117" id="Seg_1191" s="T113">nunmaːttə čaːǯandə, tan näden?</ta>
            <ta e="T120" id="Seg_1192" s="T117">pogodʼä laq qwalakse nunmat.</ta>
            <ta e="T125" id="Seg_1193" s="T120">sejčʼas ertä eŋ qwaŋgu nunmattə.</ta>
            <ta e="T130" id="Seg_1194" s="T125">telʼdʼen seːrkuzaŋ okɨr näjɣunä aːmdedʼigu.</ta>
            <ta e="T138" id="Seg_1195" s="T130">nanä näjɣum äːʒulguŋ mekka, štob nuːnmat köːskugu nunmattə.</ta>
            <ta e="T144" id="Seg_1196" s="T138">nunmatqɨn tekka kɨgattə qaimdə ketku soːdʼiga.</ta>
            <ta e="T148" id="Seg_1197" s="T144">täbla nasaŋ potpat apsodimɨm.</ta>
            <ta e="T151" id="Seg_1198" s="T148">aptiŋ kundokt apsodimə.</ta>
            <ta e="T160" id="Seg_1199" s="T151">müzurumbattə wadʼi, qwäːl, čagarɨmbɨdi qwäl, nʼäj memɨndat soːdiga müzurumbattə.</ta>
            <ta e="T163" id="Seg_1200" s="T160">kut tüːmandan täɣɨndältə?</ta>
            <ta e="T167" id="Seg_1201" s="T163">teblanmandə tüːmban qajda qum.</ta>
            <ta e="T184" id="Seg_1202" s="T167">nʼänʼät tüːmand quːbi aːbant, ass nädəmbidi üčʼide tibeɣum tüːmɨndan kündɨze künnɨn paroɣɨn aːmdɨlʼe, soːgɨndɨze, a üːgɨt seːraj.</ta>
            <ta e="T191" id="Seg_1203" s="T184">pajamban warɣɨn i aulčukow, kundär täbɨm qwärat.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_1204" s="T1">Tamdel paska, perwɨj del, paskaj del. </ta>
            <ta e="T11" id="Seg_1205" s="T7">Alʼena wazɨmpan ukon qulanando. </ta>
            <ta e="T16" id="Seg_1206" s="T11">Alʼena mannɨmbat kutʼen qaj eŋ. </ta>
            <ta e="T23" id="Seg_1207" s="T16">Kɨbačʼelam üːdəmbat nunmaːt i nädʼelam üːdembat nuːnmaːt. </ta>
            <ta e="T27" id="Seg_1208" s="T23">Ondə qwanba sadom kojalǯiku. </ta>
            <ta e="T31" id="Seg_1209" s="T27">Tʼeːl qonǯdimban poːn baːroɣondo. </ta>
            <ta e="T36" id="Seg_1210" s="T31">(Üːdə qwäntə, qari qwäntə tʼäŋgu). </ta>
            <ta e="T41" id="Seg_1211" s="T36">Iːri nunmatkunto nunmaːtɨn qwäzɨn ündus. </ta>
            <ta e="T45" id="Seg_1212" s="T41">Iːri nunmaːtɨm qwäsɨn qwoːssat. </ta>
            <ta e="T47" id="Seg_1213" s="T45">Wes küːɣangeːs. </ta>
            <ta e="T49" id="Seg_1214" s="T47">(Man moɣolgɨlɨmnan). </ta>
            <ta e="T57" id="Seg_1215" s="T49">Täban sidʼit (Alʼönan sit) laɣolɨmba kakbudta pan maʒimbat. </ta>
            <ta e="T60" id="Seg_1216" s="T57">Qajno täbnä aːndalbɨgu. </ta>
            <ta e="T64" id="Seg_1217" s="T60">Täp qusakanaj azə andalba. </ta>
            <ta e="T69" id="Seg_1218" s="T64">Nano ilɨmban štobə ik qugu. </ta>
            <ta e="T72" id="Seg_1219" s="T69">Ass kɨgɨmba qugu. </ta>
            <ta e="T75" id="Seg_1220" s="T72">Kɨgɨmba kundə ilɨgu. </ta>
            <ta e="T79" id="Seg_1221" s="T75">Täbɨnä teːrbɨmban wes qumba. </ta>
            <ta e="T83" id="Seg_1222" s="T79">Uːdɨmba čaːnǯaɣatqanda oralbat nuːnudimbɨdi. </ta>
            <ta e="T89" id="Seg_1223" s="T83">Iːril dʼelɨt tʼeːrbɨmɨndɨt, kundär iːrə warkɨmbattə. </ta>
            <ta e="T97" id="Seg_1224" s="T89">A teper teːrban, kundär teper warkugu, qaim meːkugu. </ta>
            <ta e="T103" id="Seg_1225" s="T97">Sadon qöɣɨn čaǯimban pajaga, espe čenǯindɨ. </ta>
            <ta e="T108" id="Seg_1226" s="T103">Manaj ɣundari kuːn dʼät čeːnčin. </ta>
            <ta e="T110" id="Seg_1227" s="T108">Pajaga uːtərɨŋ. </ta>
            <ta e="T113" id="Seg_1228" s="T110">Pajaga čaːǯilʼe nɨlʼedimba. </ta>
            <ta e="T117" id="Seg_1229" s="T113">“Nunmaːttə čaːǯandə, tan näden? </ta>
            <ta e="T120" id="Seg_1230" s="T117">Pogodʼälaq qwalakse nunmat. </ta>
            <ta e="T125" id="Seg_1231" s="T120">Sejčʼas ertä eŋ qwaŋgu nunmattə. </ta>
            <ta e="T130" id="Seg_1232" s="T125">Telʼdʼen seːrkuzaŋ okɨr näjɣunä aːmdedʼigu. </ta>
            <ta e="T138" id="Seg_1233" s="T130">Nanä näjɣum äːʒulguŋ mekka, štob nuːnmat köːskugu nunmattə. </ta>
            <ta e="T144" id="Seg_1234" s="T138">Nunmatqɨn tekka kɨgattə qaimdə ketku soːdʼiga. </ta>
            <ta e="T148" id="Seg_1235" s="T144">Täbla nasaŋ potpat apsodimɨm. </ta>
            <ta e="T151" id="Seg_1236" s="T148">Aptiŋ kundokt apsodimə. </ta>
            <ta e="T160" id="Seg_1237" s="T151">Müzurumbattə wadʼi, qwäːl, čagarɨmbɨdi qwäl, nʼäj memɨndat soːdiga müzurumbattə. </ta>
            <ta e="T163" id="Seg_1238" s="T160">Kut tüːmandan täɣɨndältə? </ta>
            <ta e="T167" id="Seg_1239" s="T163">Teblanmandə tüːmban qajda qum. </ta>
            <ta e="T184" id="Seg_1240" s="T167">Nʼänʼät tüːmand quːbi aːbant, ass nädəmbidi üčʼide tibeɣum tüːmɨndan kündɨze künnɨn paroɣɨn aːmdɨlʼe, soːgɨndɨze, a üːgɨt seːraj. </ta>
            <ta e="T191" id="Seg_1241" s="T184">Pajamban warɣɨn i aulčukow, kundär täbɨm qwärat.” </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1242" s="T1">tam-del</ta>
            <ta e="T3" id="Seg_1243" s="T2">paska</ta>
            <ta e="T4" id="Seg_1244" s="T3">perwɨj</ta>
            <ta e="T5" id="Seg_1245" s="T4">del</ta>
            <ta e="T6" id="Seg_1246" s="T5">paska-j</ta>
            <ta e="T7" id="Seg_1247" s="T6">del</ta>
            <ta e="T8" id="Seg_1248" s="T7">Alʼena</ta>
            <ta e="T9" id="Seg_1249" s="T8">wazɨ-mpa-n</ta>
            <ta e="T10" id="Seg_1250" s="T9">ukon</ta>
            <ta e="T11" id="Seg_1251" s="T10">qu-la-nando</ta>
            <ta e="T12" id="Seg_1252" s="T11">Alʼena</ta>
            <ta e="T13" id="Seg_1253" s="T12">mannɨ-mba-t</ta>
            <ta e="T14" id="Seg_1254" s="T13">kutʼen</ta>
            <ta e="T15" id="Seg_1255" s="T14">qaj</ta>
            <ta e="T16" id="Seg_1256" s="T15">e-ŋ</ta>
            <ta e="T17" id="Seg_1257" s="T16">kɨbačʼe-la-m</ta>
            <ta e="T18" id="Seg_1258" s="T17">üːdə-mba-t</ta>
            <ta e="T19" id="Seg_1259" s="T18">nu-n-maː-t</ta>
            <ta e="T20" id="Seg_1260" s="T19">i</ta>
            <ta e="T21" id="Seg_1261" s="T20">nädʼe-la-m</ta>
            <ta e="T22" id="Seg_1262" s="T21">üːde-mba-t</ta>
            <ta e="T23" id="Seg_1263" s="T22">nuː-n-maː-t</ta>
            <ta e="T24" id="Seg_1264" s="T23">ondə</ta>
            <ta e="T25" id="Seg_1265" s="T24">qwan-ba</ta>
            <ta e="T26" id="Seg_1266" s="T25">sad-o-m</ta>
            <ta e="T27" id="Seg_1267" s="T26">koja-lǯi-ku</ta>
            <ta e="T28" id="Seg_1268" s="T27">tʼeːl</ta>
            <ta e="T29" id="Seg_1269" s="T28">qonǯ-di-mba-n</ta>
            <ta e="T30" id="Seg_1270" s="T29">poː-n</ta>
            <ta e="T31" id="Seg_1271" s="T30">baːr-o-ɣondo</ta>
            <ta e="T32" id="Seg_1272" s="T31">üːdə</ta>
            <ta e="T33" id="Seg_1273" s="T32">qwäntə</ta>
            <ta e="T34" id="Seg_1274" s="T33">qar-i</ta>
            <ta e="T35" id="Seg_1275" s="T34">qwäntə</ta>
            <ta e="T36" id="Seg_1276" s="T35">tʼäŋgu</ta>
            <ta e="T37" id="Seg_1277" s="T36">iːr-i</ta>
            <ta e="T38" id="Seg_1278" s="T37">nu-n-mat-kunto</ta>
            <ta e="T39" id="Seg_1279" s="T38">nu-n-maːt-ɨ-n</ta>
            <ta e="T40" id="Seg_1280" s="T39">qwäzɨ-n</ta>
            <ta e="T41" id="Seg_1281" s="T40">ündu-s</ta>
            <ta e="T42" id="Seg_1282" s="T41">iːr-i</ta>
            <ta e="T43" id="Seg_1283" s="T42">nu-n-maːt-ɨ-m</ta>
            <ta e="T44" id="Seg_1284" s="T43">qwäsɨ-n</ta>
            <ta e="T45" id="Seg_1285" s="T44">qwoːs-sa-t</ta>
            <ta e="T46" id="Seg_1286" s="T45">wes</ta>
            <ta e="T47" id="Seg_1287" s="T46">küːɣangeː-s</ta>
            <ta e="T48" id="Seg_1288" s="T47">Man</ta>
            <ta e="T49" id="Seg_1289" s="T48">moɣol-gɨlɨ-m-na-n</ta>
            <ta e="T50" id="Seg_1290" s="T49">täb-a-n</ta>
            <ta e="T51" id="Seg_1291" s="T50">sidʼi-t</ta>
            <ta e="T52" id="Seg_1292" s="T51">Alʼöna-n</ta>
            <ta e="T53" id="Seg_1293" s="T52">sit</ta>
            <ta e="T54" id="Seg_1294" s="T53">laɣ-ol-ɨ-mba</ta>
            <ta e="T55" id="Seg_1295" s="T54">kakbudta</ta>
            <ta e="T56" id="Seg_1296" s="T55">pa-n</ta>
            <ta e="T57" id="Seg_1297" s="T56">maʒi-mba-t</ta>
            <ta e="T58" id="Seg_1298" s="T57">qaj-no</ta>
            <ta e="T59" id="Seg_1299" s="T58">täb-nä</ta>
            <ta e="T60" id="Seg_1300" s="T59">aːndal-bɨ-gu</ta>
            <ta e="T61" id="Seg_1301" s="T60">täp</ta>
            <ta e="T62" id="Seg_1302" s="T61">qusaka-naj</ta>
            <ta e="T63" id="Seg_1303" s="T62">azə</ta>
            <ta e="T64" id="Seg_1304" s="T63">andal-ba</ta>
            <ta e="T65" id="Seg_1305" s="T64">nano</ta>
            <ta e="T66" id="Seg_1306" s="T65">ilɨ-mba-n</ta>
            <ta e="T67" id="Seg_1307" s="T66">štobə</ta>
            <ta e="T68" id="Seg_1308" s="T67">ik</ta>
            <ta e="T69" id="Seg_1309" s="T68">qu-gu</ta>
            <ta e="T70" id="Seg_1310" s="T69">ass</ta>
            <ta e="T71" id="Seg_1311" s="T70">kɨgɨ-mba</ta>
            <ta e="T72" id="Seg_1312" s="T71">qu-gu</ta>
            <ta e="T73" id="Seg_1313" s="T72">kɨgɨ-mba</ta>
            <ta e="T74" id="Seg_1314" s="T73">kundə</ta>
            <ta e="T75" id="Seg_1315" s="T74">ilɨ-gu</ta>
            <ta e="T76" id="Seg_1316" s="T75">täb-ɨ-nä</ta>
            <ta e="T77" id="Seg_1317" s="T76">teːrbɨ-mba-n</ta>
            <ta e="T78" id="Seg_1318" s="T77">wes</ta>
            <ta e="T79" id="Seg_1319" s="T78">qu-mba</ta>
            <ta e="T80" id="Seg_1320" s="T79">uːd-ɨ-m-ba</ta>
            <ta e="T81" id="Seg_1321" s="T80">čaːnǯaɣat-qanda</ta>
            <ta e="T82" id="Seg_1322" s="T81">oral-ba-t</ta>
            <ta e="T83" id="Seg_1323" s="T82">nuːnu-di-mbɨdi</ta>
            <ta e="T84" id="Seg_1324" s="T83">iːr-i-l</ta>
            <ta e="T85" id="Seg_1325" s="T84">dʼel-ɨ-t</ta>
            <ta e="T86" id="Seg_1326" s="T85">tʼeːrbɨ-mɨ-ndɨ-t</ta>
            <ta e="T87" id="Seg_1327" s="T86">kundär</ta>
            <ta e="T88" id="Seg_1328" s="T87">iːrə</ta>
            <ta e="T89" id="Seg_1329" s="T88">warkɨ-mba-ttə</ta>
            <ta e="T90" id="Seg_1330" s="T89">a</ta>
            <ta e="T91" id="Seg_1331" s="T90">teper</ta>
            <ta e="T92" id="Seg_1332" s="T91">teːrba-n</ta>
            <ta e="T93" id="Seg_1333" s="T92">kundär</ta>
            <ta e="T94" id="Seg_1334" s="T93">teper</ta>
            <ta e="T95" id="Seg_1335" s="T94">warku-gu</ta>
            <ta e="T96" id="Seg_1336" s="T95">qai-m</ta>
            <ta e="T97" id="Seg_1337" s="T96">meː-ku-gu</ta>
            <ta e="T98" id="Seg_1338" s="T97">sad-o-n</ta>
            <ta e="T99" id="Seg_1339" s="T98">qö-ɣɨn</ta>
            <ta e="T100" id="Seg_1340" s="T99">čaǯi-mba-n</ta>
            <ta e="T101" id="Seg_1341" s="T100">paja-ga</ta>
            <ta e="T102" id="Seg_1342" s="T101">espe</ta>
            <ta e="T103" id="Seg_1343" s="T102">čenǯi-ndɨ</ta>
            <ta e="T104" id="Seg_1344" s="T103">mana-j</ta>
            <ta e="T105" id="Seg_1345" s="T104">ɣundar-i</ta>
            <ta e="T106" id="Seg_1346" s="T105">kuː-n</ta>
            <ta e="T107" id="Seg_1347" s="T106">dʼät</ta>
            <ta e="T108" id="Seg_1348" s="T107">čeːnči-n</ta>
            <ta e="T109" id="Seg_1349" s="T108">paja-ga</ta>
            <ta e="T110" id="Seg_1350" s="T109">uːtər-ɨ-ŋ</ta>
            <ta e="T111" id="Seg_1351" s="T110">paja-ga</ta>
            <ta e="T112" id="Seg_1352" s="T111">čaːǯi-lʼe</ta>
            <ta e="T113" id="Seg_1353" s="T112">nɨ-lʼ-edi-mba</ta>
            <ta e="T114" id="Seg_1354" s="T113">nu-n-maːt-tə</ta>
            <ta e="T115" id="Seg_1355" s="T114">čaːǯa-ndə</ta>
            <ta e="T116" id="Seg_1356" s="T115">tat</ta>
            <ta e="T117" id="Seg_1357" s="T116">näden</ta>
            <ta e="T118" id="Seg_1358" s="T117">pogodʼä-laq</ta>
            <ta e="T119" id="Seg_1359" s="T118">qwa-la-k-se</ta>
            <ta e="T120" id="Seg_1360" s="T119">nu-n-mat</ta>
            <ta e="T121" id="Seg_1361" s="T120">sejčʼas</ta>
            <ta e="T122" id="Seg_1362" s="T121">ertä</ta>
            <ta e="T123" id="Seg_1363" s="T122">e-ŋ</ta>
            <ta e="T124" id="Seg_1364" s="T123">qwaŋ-gu</ta>
            <ta e="T125" id="Seg_1365" s="T124">nu-n-mat-tə</ta>
            <ta e="T126" id="Seg_1366" s="T125">telʼdʼen</ta>
            <ta e="T127" id="Seg_1367" s="T126">seːr-ku-za-ŋ</ta>
            <ta e="T128" id="Seg_1368" s="T127">okɨr</ta>
            <ta e="T129" id="Seg_1369" s="T128">nä-j-ɣu-nä</ta>
            <ta e="T130" id="Seg_1370" s="T129">aːmd-edʼi-gu</ta>
            <ta e="T131" id="Seg_1371" s="T130">na-nä</ta>
            <ta e="T132" id="Seg_1372" s="T131">nä-j-ɣum</ta>
            <ta e="T133" id="Seg_1373" s="T132">äːʒu-l-gu-ŋ</ta>
            <ta e="T134" id="Seg_1374" s="T133">mekka</ta>
            <ta e="T135" id="Seg_1375" s="T134">štob</ta>
            <ta e="T136" id="Seg_1376" s="T135">nuː-n-mat</ta>
            <ta e="T137" id="Seg_1377" s="T136">köːs-ku-gu</ta>
            <ta e="T138" id="Seg_1378" s="T137">nu-n-mat-tə</ta>
            <ta e="T139" id="Seg_1379" s="T138">nu-n-mat-qɨn</ta>
            <ta e="T140" id="Seg_1380" s="T139">tekka</ta>
            <ta e="T141" id="Seg_1381" s="T140">kɨga-ttə</ta>
            <ta e="T142" id="Seg_1382" s="T141">qai-m-də</ta>
            <ta e="T143" id="Seg_1383" s="T142">ket-ku</ta>
            <ta e="T144" id="Seg_1384" s="T143">soːdʼiga</ta>
            <ta e="T145" id="Seg_1385" s="T144">täb-la</ta>
            <ta e="T146" id="Seg_1386" s="T145">na-saŋ</ta>
            <ta e="T147" id="Seg_1387" s="T146">pot-pa-t</ta>
            <ta e="T148" id="Seg_1388" s="T147">ap-sodi-mɨ-m</ta>
            <ta e="T149" id="Seg_1389" s="T148">apti-ŋ</ta>
            <ta e="T150" id="Seg_1390" s="T149">kundokt</ta>
            <ta e="T151" id="Seg_1391" s="T150">ap-sodi-mə</ta>
            <ta e="T152" id="Seg_1392" s="T151">müzu-ru-mba-ttə</ta>
            <ta e="T153" id="Seg_1393" s="T152">wadʼi</ta>
            <ta e="T154" id="Seg_1394" s="T153">qwäːl</ta>
            <ta e="T155" id="Seg_1395" s="T154">čaga-rɨ-mbɨdi</ta>
            <ta e="T156" id="Seg_1396" s="T155">qwäl</ta>
            <ta e="T157" id="Seg_1397" s="T156">nʼäj</ta>
            <ta e="T158" id="Seg_1398" s="T157">me-mɨ-nda-t</ta>
            <ta e="T159" id="Seg_1399" s="T158">soːdiga</ta>
            <ta e="T160" id="Seg_1400" s="T159">müzu-ru-mba-ttə</ta>
            <ta e="T161" id="Seg_1401" s="T160">kut</ta>
            <ta e="T162" id="Seg_1402" s="T161">tüː-ma-nda-n</ta>
            <ta e="T163" id="Seg_1403" s="T162">tä-ɣɨndä-ltə</ta>
            <ta e="T164" id="Seg_1404" s="T163">teb-la-n-ma-ndə</ta>
            <ta e="T165" id="Seg_1405" s="T164">tüː-mba-n</ta>
            <ta e="T166" id="Seg_1406" s="T165">qaj-da</ta>
            <ta e="T167" id="Seg_1407" s="T166">qum</ta>
            <ta e="T168" id="Seg_1408" s="T167">nʼänʼä-t</ta>
            <ta e="T169" id="Seg_1409" s="T168">tüː-ma-nd</ta>
            <ta e="T170" id="Seg_1410" s="T169">quː-bi</ta>
            <ta e="T171" id="Seg_1411" s="T170">aːba-n-t</ta>
            <ta e="T172" id="Seg_1412" s="T171">ass</ta>
            <ta e="T173" id="Seg_1413" s="T172">nädə-mbidi</ta>
            <ta e="T174" id="Seg_1414" s="T173">üčʼide</ta>
            <ta e="T175" id="Seg_1415" s="T174">tibe-ɣum</ta>
            <ta e="T176" id="Seg_1416" s="T175">tüː-mɨ-nda-n</ta>
            <ta e="T177" id="Seg_1417" s="T176">kündɨ-ze</ta>
            <ta e="T178" id="Seg_1418" s="T177">künnɨ-n</ta>
            <ta e="T179" id="Seg_1419" s="T178">par-o-ɣɨn</ta>
            <ta e="T180" id="Seg_1420" s="T179">aːmdɨ-lʼe</ta>
            <ta e="T181" id="Seg_1421" s="T180">soː-gɨndɨ-ze</ta>
            <ta e="T182" id="Seg_1422" s="T181">a</ta>
            <ta e="T183" id="Seg_1423" s="T182">üːgɨ-t</ta>
            <ta e="T184" id="Seg_1424" s="T183">seːraj</ta>
            <ta e="T185" id="Seg_1425" s="T184">paja-m-ba-n</ta>
            <ta e="T186" id="Seg_1426" s="T185">warɣɨ-n</ta>
            <ta e="T187" id="Seg_1427" s="T186">i</ta>
            <ta e="T188" id="Seg_1428" s="T187">aulču-ko-w</ta>
            <ta e="T189" id="Seg_1429" s="T188">kundär</ta>
            <ta e="T190" id="Seg_1430" s="T189">täb-ɨ-m</ta>
            <ta e="T191" id="Seg_1431" s="T190">qwära-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1432" s="T1">taw-dʼel</ta>
            <ta e="T3" id="Seg_1433" s="T2">paska</ta>
            <ta e="T4" id="Seg_1434" s="T3">perwɨj</ta>
            <ta e="T5" id="Seg_1435" s="T4">dʼel</ta>
            <ta e="T6" id="Seg_1436" s="T5">paska-lʼ</ta>
            <ta e="T7" id="Seg_1437" s="T6">dʼel</ta>
            <ta e="T8" id="Seg_1438" s="T7">Alʼena</ta>
            <ta e="T9" id="Seg_1439" s="T8">wešɨ-mbɨ-n</ta>
            <ta e="T10" id="Seg_1440" s="T9">ugon</ta>
            <ta e="T11" id="Seg_1441" s="T10">qum-la-nanto</ta>
            <ta e="T12" id="Seg_1442" s="T11">Alʼena</ta>
            <ta e="T13" id="Seg_1443" s="T12">*mantɨ-mbɨ-t</ta>
            <ta e="T14" id="Seg_1444" s="T13">qutʼet</ta>
            <ta e="T15" id="Seg_1445" s="T14">qaj</ta>
            <ta e="T16" id="Seg_1446" s="T15">eː-n</ta>
            <ta e="T17" id="Seg_1447" s="T16">kɨbajče-la-m</ta>
            <ta e="T18" id="Seg_1448" s="T17">üdə-mbɨ-t</ta>
            <ta e="T19" id="Seg_1449" s="T18">nom-n-maːt-ntə</ta>
            <ta e="T20" id="Seg_1450" s="T19">i</ta>
            <ta e="T21" id="Seg_1451" s="T20">nädek-la-m</ta>
            <ta e="T22" id="Seg_1452" s="T21">üdə-mbɨ-t</ta>
            <ta e="T23" id="Seg_1453" s="T22">nom-n-maːt-ntə</ta>
            <ta e="T24" id="Seg_1454" s="T23">ondə</ta>
            <ta e="T25" id="Seg_1455" s="T24">qwan-mbɨ</ta>
            <ta e="T26" id="Seg_1456" s="T25">sad-ɨ-m</ta>
            <ta e="T27" id="Seg_1457" s="T26">qoja-lǯi-gu</ta>
            <ta e="T28" id="Seg_1458" s="T27">dʼel</ta>
            <ta e="T29" id="Seg_1459" s="T28">qonču-dʼi-mbɨ-n</ta>
            <ta e="T30" id="Seg_1460" s="T29">po-n</ta>
            <ta e="T31" id="Seg_1461" s="T30">par-ɨ-qɨntɨ</ta>
            <ta e="T32" id="Seg_1462" s="T31">üdə</ta>
            <ta e="T33" id="Seg_1463" s="T32">qwäntə</ta>
            <ta e="T34" id="Seg_1464" s="T33">qare-lʼ</ta>
            <ta e="T35" id="Seg_1465" s="T34">qwäntə</ta>
            <ta e="T36" id="Seg_1466" s="T35">tʼäkku</ta>
            <ta e="T37" id="Seg_1467" s="T36">iːr-lʼ</ta>
            <ta e="T38" id="Seg_1468" s="T37">nom-n-maːt-qɨntɨ</ta>
            <ta e="T39" id="Seg_1469" s="T38">nom-n-maːt-ɨ-n</ta>
            <ta e="T40" id="Seg_1470" s="T39">qwəzi-n</ta>
            <ta e="T41" id="Seg_1471" s="T40">ündɨ-sɨ</ta>
            <ta e="T42" id="Seg_1472" s="T41">iːr-lʼ</ta>
            <ta e="T43" id="Seg_1473" s="T42">nom-n-maːt-ɨ-m</ta>
            <ta e="T44" id="Seg_1474" s="T43">qwəzi-n</ta>
            <ta e="T45" id="Seg_1475" s="T44">qwat-sɨ-tɨn</ta>
            <ta e="T46" id="Seg_1476" s="T45">wesʼ</ta>
            <ta e="T47" id="Seg_1477" s="T46">küːɣangeː-sɨ</ta>
            <ta e="T48" id="Seg_1478" s="T47">man</ta>
            <ta e="T49" id="Seg_1479" s="T48">mogol-galɨ-m-nɨ-ŋ</ta>
            <ta e="T50" id="Seg_1480" s="T49">täp-ɨ-n</ta>
            <ta e="T51" id="Seg_1481" s="T50">sidʼe-tə</ta>
            <ta e="T52" id="Seg_1482" s="T51">Alʼena-n</ta>
            <ta e="T53" id="Seg_1483" s="T52">sidʼe</ta>
            <ta e="T54" id="Seg_1484" s="T53">laqǝ-ol-ɨ-mbɨ</ta>
            <ta e="T55" id="Seg_1485" s="T54">kakbudta</ta>
            <ta e="T56" id="Seg_1486" s="T55">paː-ŋ</ta>
            <ta e="T57" id="Seg_1487" s="T56">maǯə-mbɨ-tɨn</ta>
            <ta e="T58" id="Seg_1488" s="T57">qaj-no</ta>
            <ta e="T59" id="Seg_1489" s="T58">täp-nä</ta>
            <ta e="T60" id="Seg_1490" s="T59">aːndal-mbɨ-gu</ta>
            <ta e="T61" id="Seg_1491" s="T60">täp</ta>
            <ta e="T62" id="Seg_1492" s="T61">qussaqɨn-näj</ta>
            <ta e="T63" id="Seg_1493" s="T62">asa</ta>
            <ta e="T64" id="Seg_1494" s="T63">aːndal-mbɨ</ta>
            <ta e="T65" id="Seg_1495" s="T64">nanoː</ta>
            <ta e="T66" id="Seg_1496" s="T65">elɨ-mbɨ-n</ta>
            <ta e="T67" id="Seg_1497" s="T66">štobɨ</ta>
            <ta e="T68" id="Seg_1498" s="T67">igə</ta>
            <ta e="T69" id="Seg_1499" s="T68">quː-gu</ta>
            <ta e="T70" id="Seg_1500" s="T69">asa</ta>
            <ta e="T71" id="Seg_1501" s="T70">kɨgɨ-mbɨ</ta>
            <ta e="T72" id="Seg_1502" s="T71">quː-gu</ta>
            <ta e="T73" id="Seg_1503" s="T72">kɨgɨ-mbɨ</ta>
            <ta e="T74" id="Seg_1504" s="T73">kundɨ</ta>
            <ta e="T75" id="Seg_1505" s="T74">elɨ-gu</ta>
            <ta e="T76" id="Seg_1506" s="T75">täp-ɨ-näj</ta>
            <ta e="T77" id="Seg_1507" s="T76">tärba-mbɨ-n</ta>
            <ta e="T78" id="Seg_1508" s="T77">wesʼ</ta>
            <ta e="T79" id="Seg_1509" s="T78">quː-mbɨ</ta>
            <ta e="T80" id="Seg_1510" s="T79">ut-ɨ-m-ba</ta>
            <ta e="T81" id="Seg_1511" s="T80">čantəkat-qɨntɨ</ta>
            <ta e="T82" id="Seg_1512" s="T81">oral-mbɨ-t</ta>
            <ta e="T83" id="Seg_1513" s="T82">nunɨ-dʼi-mbɨdi</ta>
            <ta e="T84" id="Seg_1514" s="T83">iːr-ɨ-lʼ</ta>
            <ta e="T85" id="Seg_1515" s="T84">dʼel-ɨ-tə</ta>
            <ta e="T86" id="Seg_1516" s="T85">tärba-mbɨ-ntɨ-t</ta>
            <ta e="T87" id="Seg_1517" s="T86">qundar</ta>
            <ta e="T88" id="Seg_1518" s="T87">iːr</ta>
            <ta e="T89" id="Seg_1519" s="T88">warkɨ-mbɨ-tɨn</ta>
            <ta e="T90" id="Seg_1520" s="T89">a</ta>
            <ta e="T91" id="Seg_1521" s="T90">teper</ta>
            <ta e="T92" id="Seg_1522" s="T91">tärba-n</ta>
            <ta e="T93" id="Seg_1523" s="T92">qundar</ta>
            <ta e="T94" id="Seg_1524" s="T93">teper</ta>
            <ta e="T95" id="Seg_1525" s="T94">warkɨ-gu</ta>
            <ta e="T96" id="Seg_1526" s="T95">qaj-m</ta>
            <ta e="T97" id="Seg_1527" s="T96">meː-ku-gu</ta>
            <ta e="T98" id="Seg_1528" s="T97">sad-ɨ-n</ta>
            <ta e="T99" id="Seg_1529" s="T98">kö-qɨn</ta>
            <ta e="T100" id="Seg_1530" s="T99">čaǯɨ-mbɨ-n</ta>
            <ta e="T101" id="Seg_1531" s="T100">paja-ka</ta>
            <ta e="T102" id="Seg_1532" s="T101">espe</ta>
            <ta e="T103" id="Seg_1533" s="T102">čenču-ntɨ</ta>
            <ta e="T104" id="Seg_1534" s="T103">mana-lʼ</ta>
            <ta e="T105" id="Seg_1535" s="T104">qundar-lʼ</ta>
            <ta e="T106" id="Seg_1536" s="T105">qum-n</ta>
            <ta e="T107" id="Seg_1537" s="T106">tʼаːt</ta>
            <ta e="T108" id="Seg_1538" s="T107">čenču-n</ta>
            <ta e="T109" id="Seg_1539" s="T108">paja-ka</ta>
            <ta e="T110" id="Seg_1540" s="T109">udɨr-ɨ-n</ta>
            <ta e="T111" id="Seg_1541" s="T110">paja-ka</ta>
            <ta e="T112" id="Seg_1542" s="T111">čaǯɨ-le</ta>
            <ta e="T113" id="Seg_1543" s="T112">nɨ-l-edi-mbɨ</ta>
            <ta e="T114" id="Seg_1544" s="T113">nom-n-maːt-ntə</ta>
            <ta e="T115" id="Seg_1545" s="T114">čaǯɨ-ntə</ta>
            <ta e="T116" id="Seg_1546" s="T115">tan</ta>
            <ta e="T117" id="Seg_1547" s="T116">nädek</ta>
            <ta e="T118" id="Seg_1548" s="T117">pogodʼä-laq</ta>
            <ta e="T119" id="Seg_1549" s="T118">qwan-lä-ŋ-s</ta>
            <ta e="T120" id="Seg_1550" s="T119">nom-n-maːt</ta>
            <ta e="T121" id="Seg_1551" s="T120">sičas</ta>
            <ta e="T122" id="Seg_1552" s="T121">erte</ta>
            <ta e="T123" id="Seg_1553" s="T122">eː-n</ta>
            <ta e="T124" id="Seg_1554" s="T123">qwan-gu</ta>
            <ta e="T125" id="Seg_1555" s="T124">nom-n-maːt-ntə</ta>
            <ta e="T126" id="Seg_1556" s="T125">telʼdʼan</ta>
            <ta e="T127" id="Seg_1557" s="T126">ser-ku-sɨ-ŋ</ta>
            <ta e="T128" id="Seg_1558" s="T127">okkɨr</ta>
            <ta e="T129" id="Seg_1559" s="T128">ne-lʼ-qum-nä</ta>
            <ta e="T130" id="Seg_1560" s="T129">amdɨ-edi-gu</ta>
            <ta e="T131" id="Seg_1561" s="T130">na-näj</ta>
            <ta e="T132" id="Seg_1562" s="T131">ne-lʼ-qum</ta>
            <ta e="T133" id="Seg_1563" s="T132">əǯu-lɨ-ku-n</ta>
            <ta e="T134" id="Seg_1564" s="T133">mekka</ta>
            <ta e="T135" id="Seg_1565" s="T134">štobɨ</ta>
            <ta e="T136" id="Seg_1566" s="T135">nom-n-maːt</ta>
            <ta e="T137" id="Seg_1567" s="T136">*qöːš-ku-gu</ta>
            <ta e="T138" id="Seg_1568" s="T137">nom-n-maːt-ntə</ta>
            <ta e="T139" id="Seg_1569" s="T138">nom-n-maːt-qɨn</ta>
            <ta e="T140" id="Seg_1570" s="T139">tekka</ta>
            <ta e="T141" id="Seg_1571" s="T140">kɨgɨ-tɨn</ta>
            <ta e="T142" id="Seg_1572" s="T141">qaj-m-də</ta>
            <ta e="T143" id="Seg_1573" s="T142">ket-gu</ta>
            <ta e="T144" id="Seg_1574" s="T143">soːdʼiga</ta>
            <ta e="T145" id="Seg_1575" s="T144">täp-la</ta>
            <ta e="T146" id="Seg_1576" s="T145">na-sak</ta>
            <ta e="T147" id="Seg_1577" s="T146">poːt-mbɨ-tɨn</ta>
            <ta e="T148" id="Seg_1578" s="T147">am-sodə-mɨ-m</ta>
            <ta e="T149" id="Seg_1579" s="T148">apti-n</ta>
            <ta e="T150" id="Seg_1580" s="T149">qundaqɨn</ta>
            <ta e="T151" id="Seg_1581" s="T150">am-sodə-mɨ</ta>
            <ta e="T152" id="Seg_1582" s="T151">mözu-rɨ-mbɨ-tɨn</ta>
            <ta e="T153" id="Seg_1583" s="T152">wadʼi</ta>
            <ta e="T154" id="Seg_1584" s="T153">qwɛl</ta>
            <ta e="T155" id="Seg_1585" s="T154">čagɨ-rɨ-mbɨdi</ta>
            <ta e="T156" id="Seg_1586" s="T155">qwɛl</ta>
            <ta e="T157" id="Seg_1587" s="T156">nʼäj</ta>
            <ta e="T158" id="Seg_1588" s="T157">meː-mbɨ-ntɨ-tɨn</ta>
            <ta e="T159" id="Seg_1589" s="T158">soːdʼiga</ta>
            <ta e="T160" id="Seg_1590" s="T159">mözu-rɨ-mbɨ-tɨn</ta>
            <ta e="T161" id="Seg_1591" s="T160">kud</ta>
            <ta e="T162" id="Seg_1592" s="T161">tüː-mbɨ-ntɨ-n</ta>
            <ta e="T163" id="Seg_1593" s="T162">te-qɨntɨ-lɨt</ta>
            <ta e="T164" id="Seg_1594" s="T163">täp-la-n-ma-ntə</ta>
            <ta e="T165" id="Seg_1595" s="T164">tüː-mbɨ-n</ta>
            <ta e="T166" id="Seg_1596" s="T165">qaj-ta</ta>
            <ta e="T167" id="Seg_1597" s="T166">qum</ta>
            <ta e="T168" id="Seg_1598" s="T167">nanʼa-tə</ta>
            <ta e="T169" id="Seg_1599" s="T168">tüː-mbɨ-ntɨ</ta>
            <ta e="T170" id="Seg_1600" s="T169">quː-mbɨdi</ta>
            <ta e="T171" id="Seg_1601" s="T170">aba-n-tə</ta>
            <ta e="T172" id="Seg_1602" s="T171">asa</ta>
            <ta e="T173" id="Seg_1603" s="T172">nʼädə-mbɨdi</ta>
            <ta e="T174" id="Seg_1604" s="T173">üčede</ta>
            <ta e="T175" id="Seg_1605" s="T174">täbe-qum</ta>
            <ta e="T176" id="Seg_1606" s="T175">tüː-mbɨ-ntɨ-n</ta>
            <ta e="T177" id="Seg_1607" s="T176">kütdə-se</ta>
            <ta e="T178" id="Seg_1608" s="T177">kütdə-n</ta>
            <ta e="T179" id="Seg_1609" s="T178">par-ɨ-qɨn</ta>
            <ta e="T180" id="Seg_1610" s="T179">amdɨ-le</ta>
            <ta e="T181" id="Seg_1611" s="T180">soː-qɨntɨ-se</ta>
            <ta e="T182" id="Seg_1612" s="T181">a</ta>
            <ta e="T183" id="Seg_1613" s="T182">ügu-tə</ta>
            <ta e="T184" id="Seg_1614" s="T183">seːraj</ta>
            <ta e="T185" id="Seg_1615" s="T184">paja-m-mbɨ-ŋ</ta>
            <ta e="T186" id="Seg_1616" s="T185">wargɨ-ŋ</ta>
            <ta e="T187" id="Seg_1617" s="T186">i</ta>
            <ta e="T188" id="Seg_1618" s="T187">aulǯu-ku-w</ta>
            <ta e="T189" id="Seg_1619" s="T188">qundar</ta>
            <ta e="T190" id="Seg_1620" s="T189">täp-ɨ-m</ta>
            <ta e="T191" id="Seg_1621" s="T190">qwɨrɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1622" s="T1">this-day.[NOM]</ta>
            <ta e="T3" id="Seg_1623" s="T2">Easter.[NOM]</ta>
            <ta e="T4" id="Seg_1624" s="T3">first</ta>
            <ta e="T5" id="Seg_1625" s="T4">day.[NOM]</ta>
            <ta e="T6" id="Seg_1626" s="T5">Easter-ADJZ</ta>
            <ta e="T7" id="Seg_1627" s="T6">day.[NOM]</ta>
            <ta e="T8" id="Seg_1628" s="T7">Alyona.[NOM]</ta>
            <ta e="T9" id="Seg_1629" s="T8">get.up-PST.NAR-3SG.S</ta>
            <ta e="T10" id="Seg_1630" s="T9">earlier</ta>
            <ta e="T11" id="Seg_1631" s="T10">human.being-PL-ABL</ta>
            <ta e="T12" id="Seg_1632" s="T11">Alyona.[NOM]</ta>
            <ta e="T13" id="Seg_1633" s="T12">look-DUR-3SG.O</ta>
            <ta e="T14" id="Seg_1634" s="T13">where</ta>
            <ta e="T15" id="Seg_1635" s="T14">what.[NOM]</ta>
            <ta e="T16" id="Seg_1636" s="T15">be-3SG.S</ta>
            <ta e="T17" id="Seg_1637" s="T16">boy-PL-ACC</ta>
            <ta e="T18" id="Seg_1638" s="T17">let.go-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_1639" s="T18">god-GEN-tent-ILL</ta>
            <ta e="T20" id="Seg_1640" s="T19">and</ta>
            <ta e="T21" id="Seg_1641" s="T20">girl-PL-ACC</ta>
            <ta e="T22" id="Seg_1642" s="T21">let.go-PST.NAR-3SG.O</ta>
            <ta e="T23" id="Seg_1643" s="T22">god-GEN-tent-ILL</ta>
            <ta e="T24" id="Seg_1644" s="T23">oneself.3SG</ta>
            <ta e="T25" id="Seg_1645" s="T24">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_1646" s="T25">garden-EP-ACC</ta>
            <ta e="T27" id="Seg_1647" s="T26">surround-TR-INF</ta>
            <ta e="T28" id="Seg_1648" s="T27">sun.[NOM]</ta>
            <ta e="T29" id="Seg_1649" s="T28">appear-RFL-PST.NAR-3SG.S</ta>
            <ta e="T30" id="Seg_1650" s="T29">tree-GEN</ta>
            <ta e="T31" id="Seg_1651" s="T30">top-EP-EL.3SG</ta>
            <ta e="T32" id="Seg_1652" s="T31">evening.[NOM]</ta>
            <ta e="T33" id="Seg_1653" s="T32">dawn.[NOM]</ta>
            <ta e="T34" id="Seg_1654" s="T33">morning-ADJZ</ta>
            <ta e="T35" id="Seg_1655" s="T34">dawn.[NOM]</ta>
            <ta e="T36" id="Seg_1656" s="T35">NEG.EX.[3SG.S]</ta>
            <ta e="T37" id="Seg_1657" s="T36">earlier-ADJZ</ta>
            <ta e="T38" id="Seg_1658" s="T37">god-GEN-house-EL.3SG</ta>
            <ta e="T39" id="Seg_1659" s="T38">god-GEN-house-EP-GEN</ta>
            <ta e="T40" id="Seg_1660" s="T39">iron-%%</ta>
            <ta e="T41" id="Seg_1661" s="T40">be.heard-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_1662" s="T41">earlier-ADJZ</ta>
            <ta e="T43" id="Seg_1663" s="T42">god-GEN-house-EP-ACC</ta>
            <ta e="T44" id="Seg_1664" s="T43">iron-%%</ta>
            <ta e="T45" id="Seg_1665" s="T44">beat-PST-3PL</ta>
            <ta e="T46" id="Seg_1666" s="T45">all</ta>
            <ta e="T47" id="Seg_1667" s="T46">%%-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_1668" s="T47">I.NOM</ta>
            <ta e="T49" id="Seg_1669" s="T48">back-CAR-TRL-CO-1SG.S</ta>
            <ta e="T50" id="Seg_1670" s="T49">(s)he-EP-GEN</ta>
            <ta e="T51" id="Seg_1671" s="T50">heart.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_1672" s="T51">Alyona-GEN</ta>
            <ta e="T53" id="Seg_1673" s="T52">heart.[NOM]</ta>
            <ta e="T54" id="Seg_1674" s="T53">move-MOM-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T55" id="Seg_1675" s="T54">as.if</ta>
            <ta e="T56" id="Seg_1676" s="T55">knife-ADVZ</ta>
            <ta e="T57" id="Seg_1677" s="T56">cut-PST.NAR-3PL</ta>
            <ta e="T58" id="Seg_1678" s="T57">what-TRL</ta>
            <ta e="T59" id="Seg_1679" s="T58">(s)he-ALL</ta>
            <ta e="T60" id="Seg_1680" s="T59">become.glad-DUR-INF</ta>
            <ta e="T61" id="Seg_1681" s="T60">(s)he.[NOM]</ta>
            <ta e="T62" id="Seg_1682" s="T61">when-EMPH</ta>
            <ta e="T63" id="Seg_1683" s="T62">NEG</ta>
            <ta e="T64" id="Seg_1684" s="T63">become.glad-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1685" s="T64">that.is.why</ta>
            <ta e="T66" id="Seg_1686" s="T65">live-PST.NAR-3SG.S</ta>
            <ta e="T67" id="Seg_1687" s="T66">so.that</ta>
            <ta e="T68" id="Seg_1688" s="T67">NEG.IMP</ta>
            <ta e="T69" id="Seg_1689" s="T68">die-INF</ta>
            <ta e="T70" id="Seg_1690" s="T69">NEG</ta>
            <ta e="T71" id="Seg_1691" s="T70">want-PST.NAR.[3SG.S]</ta>
            <ta e="T72" id="Seg_1692" s="T71">die-INF</ta>
            <ta e="T73" id="Seg_1693" s="T72">want-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1694" s="T73">long</ta>
            <ta e="T75" id="Seg_1695" s="T74">live-INF</ta>
            <ta e="T76" id="Seg_1696" s="T75">(s)he.[NOM]-EP-EMPH</ta>
            <ta e="T77" id="Seg_1697" s="T76">think-PST.NAR-3SG.S</ta>
            <ta e="T78" id="Seg_1698" s="T77">all</ta>
            <ta e="T79" id="Seg_1699" s="T78">die-RES.[3SG.S]</ta>
            <ta e="T80" id="Seg_1700" s="T79">hand-EP-ACC-%%</ta>
            <ta e="T81" id="Seg_1701" s="T80">forehead-ILL.3SG</ta>
            <ta e="T82" id="Seg_1702" s="T81">take-PST.NAR-3SG.O</ta>
            <ta e="T83" id="Seg_1703" s="T82">get.tired-RFL-PTCP.PST</ta>
            <ta e="T84" id="Seg_1704" s="T83">earlier-EP-ADJZ</ta>
            <ta e="T85" id="Seg_1705" s="T84">day.[NOM]-EP-3SG</ta>
            <ta e="T86" id="Seg_1706" s="T85">think-PST.NAR-INFER-3SG.O</ta>
            <ta e="T87" id="Seg_1707" s="T86">how</ta>
            <ta e="T88" id="Seg_1708" s="T87">earlier</ta>
            <ta e="T89" id="Seg_1709" s="T88">live-PST.NAR-3PL</ta>
            <ta e="T90" id="Seg_1710" s="T89">and</ta>
            <ta e="T91" id="Seg_1711" s="T90">now</ta>
            <ta e="T92" id="Seg_1712" s="T91">think-3SG.S</ta>
            <ta e="T93" id="Seg_1713" s="T92">how</ta>
            <ta e="T94" id="Seg_1714" s="T93">now</ta>
            <ta e="T95" id="Seg_1715" s="T94">live-INF</ta>
            <ta e="T96" id="Seg_1716" s="T95">what-ACC</ta>
            <ta e="T97" id="Seg_1717" s="T96">do-HAB-INF</ta>
            <ta e="T98" id="Seg_1718" s="T97">garden-EP-GEN</ta>
            <ta e="T99" id="Seg_1719" s="T98">side-LOC</ta>
            <ta e="T100" id="Seg_1720" s="T99">go-PST.NAR-3SG.S</ta>
            <ta e="T101" id="Seg_1721" s="T100">old.woman-DIM.[NOM]</ta>
            <ta e="T102" id="Seg_1722" s="T101">%%</ta>
            <ta e="T103" id="Seg_1723" s="T102">speak-INFER.[3SG.S]</ta>
            <ta e="T104" id="Seg_1724" s="T103">fool-ADJZ</ta>
            <ta e="T105" id="Seg_1725" s="T104">how-ADJZ</ta>
            <ta e="T106" id="Seg_1726" s="T105">human.being-GEN</ta>
            <ta e="T107" id="Seg_1727" s="T106">about</ta>
            <ta e="T108" id="Seg_1728" s="T107">speak-3SG.S</ta>
            <ta e="T109" id="Seg_1729" s="T108">old.woman-DIM.[NOM]</ta>
            <ta e="T110" id="Seg_1730" s="T109">stop-EP-3SG.S</ta>
            <ta e="T111" id="Seg_1731" s="T110">old.woman-DIM.[NOM]</ta>
            <ta e="T112" id="Seg_1732" s="T111">go-CVB</ta>
            <ta e="T113" id="Seg_1733" s="T112">stand-INCH-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T114" id="Seg_1734" s="T113">god-GEN-house-ILL</ta>
            <ta e="T115" id="Seg_1735" s="T114">go-2SG.S</ta>
            <ta e="T116" id="Seg_1736" s="T115">you.SG.NOM</ta>
            <ta e="T117" id="Seg_1737" s="T116">girl.[NOM]</ta>
            <ta e="T118" id="Seg_1738" s="T117">a.little.later-ATTEN</ta>
            <ta e="T119" id="Seg_1739" s="T118">leave-FUT-1SG.S-FUT</ta>
            <ta e="T120" id="Seg_1740" s="T119">god-GEN-house.[NOM]</ta>
            <ta e="T121" id="Seg_1741" s="T120">now</ta>
            <ta e="T122" id="Seg_1742" s="T121">early</ta>
            <ta e="T123" id="Seg_1743" s="T122">be-3SG.S</ta>
            <ta e="T124" id="Seg_1744" s="T123">leave-INF</ta>
            <ta e="T125" id="Seg_1745" s="T124">god-GEN-house-ILL</ta>
            <ta e="T126" id="Seg_1746" s="T125">yesterday</ta>
            <ta e="T127" id="Seg_1747" s="T126">come.in-HAB-PST-1SG.S</ta>
            <ta e="T128" id="Seg_1748" s="T127">one</ta>
            <ta e="T129" id="Seg_1749" s="T128">woman-ADJZ-human.being-ALL</ta>
            <ta e="T130" id="Seg_1750" s="T129">sit-DRV-INF</ta>
            <ta e="T131" id="Seg_1751" s="T130">this-EMPH</ta>
            <ta e="T132" id="Seg_1752" s="T131">woman-ADJZ-human.being.[NOM]</ta>
            <ta e="T133" id="Seg_1753" s="T132">say-RES-HAB-3SG.S</ta>
            <ta e="T134" id="Seg_1754" s="T133">I.ALL</ta>
            <ta e="T135" id="Seg_1755" s="T134">so.that</ta>
            <ta e="T136" id="Seg_1756" s="T135">god-GEN-house.[NOM]</ta>
            <ta e="T137" id="Seg_1757" s="T136">go-HAB-INF</ta>
            <ta e="T138" id="Seg_1758" s="T137">god-GEN-house-ILL</ta>
            <ta e="T139" id="Seg_1759" s="T138">god-GEN-house-LOC</ta>
            <ta e="T140" id="Seg_1760" s="T139">you.ALL</ta>
            <ta e="T141" id="Seg_1761" s="T140">want-3PL</ta>
            <ta e="T142" id="Seg_1762" s="T141">what-ACC-EMPH</ta>
            <ta e="T143" id="Seg_1763" s="T142">say-INF</ta>
            <ta e="T144" id="Seg_1764" s="T143">good</ta>
            <ta e="T145" id="Seg_1765" s="T144">(s)he-PL.[NOM]</ta>
            <ta e="T146" id="Seg_1766" s="T145">this-COR</ta>
            <ta e="T147" id="Seg_1767" s="T146">cook-RES-3PL</ta>
            <ta e="T148" id="Seg_1768" s="T147">eat-PTCP.NEC-something-ACC</ta>
            <ta e="T149" id="Seg_1769" s="T148">smell-3SG.S</ta>
            <ta e="T150" id="Seg_1770" s="T149">far</ta>
            <ta e="T151" id="Seg_1771" s="T150">eat-PTCP.NEC-something.[NOM]</ta>
            <ta e="T152" id="Seg_1772" s="T151">cook-CAUS-PST.NAR-3PL</ta>
            <ta e="T153" id="Seg_1773" s="T152">meat.[NOM]</ta>
            <ta e="T154" id="Seg_1774" s="T153">fish.[NOM]</ta>
            <ta e="T155" id="Seg_1775" s="T154">dry-CAUS-PTCP.PST</ta>
            <ta e="T156" id="Seg_1776" s="T155">fish.[NOM]</ta>
            <ta e="T157" id="Seg_1777" s="T156">bread.[NOM]</ta>
            <ta e="T158" id="Seg_1778" s="T157">do-PST.NAR-INFER-3PL</ta>
            <ta e="T159" id="Seg_1779" s="T158">good</ta>
            <ta e="T160" id="Seg_1780" s="T159">cook-CAUS-PST.NAR-3PL</ta>
            <ta e="T161" id="Seg_1781" s="T160">who.[NOM]</ta>
            <ta e="T162" id="Seg_1782" s="T161">come-PST.NAR-INFER-3SG.S</ta>
            <ta e="T163" id="Seg_1783" s="T162">you.PL-ILL.3SG-2PL</ta>
            <ta e="T164" id="Seg_1784" s="T163">(s)he-PL-GEN-%%-ILL</ta>
            <ta e="T165" id="Seg_1785" s="T164">come-PST.NAR-3SG.S</ta>
            <ta e="T166" id="Seg_1786" s="T165">what.[NOM]-INDEF</ta>
            <ta e="T167" id="Seg_1787" s="T166">human.being.[NOM]</ta>
            <ta e="T168" id="Seg_1788" s="T167">sister.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_1789" s="T168">come-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T170" id="Seg_1790" s="T169">die-PTCP.PST</ta>
            <ta e="T171" id="Seg_1791" s="T170">older.sister-GEN-3SG</ta>
            <ta e="T172" id="Seg_1792" s="T171">NEG</ta>
            <ta e="T173" id="Seg_1793" s="T172">get.married-PTCP.PST</ta>
            <ta e="T174" id="Seg_1794" s="T173">child.[NOM]</ta>
            <ta e="T175" id="Seg_1795" s="T174">man-human.being.[NOM]</ta>
            <ta e="T176" id="Seg_1796" s="T175">come-PST.NAR-INFER-3SG.S</ta>
            <ta e="T177" id="Seg_1797" s="T176">horse-INSTR</ta>
            <ta e="T178" id="Seg_1798" s="T177">horse-GEN</ta>
            <ta e="T179" id="Seg_1799" s="T178">top-EP-LOC</ta>
            <ta e="T180" id="Seg_1800" s="T179">sit-CVB</ta>
            <ta e="T181" id="Seg_1801" s="T180">good-LOC.3SG-ADJZ</ta>
            <ta e="T182" id="Seg_1802" s="T181">and</ta>
            <ta e="T183" id="Seg_1803" s="T182">cap.[NOM]-3SG</ta>
            <ta e="T184" id="Seg_1804" s="T183">grey.[3SG.S]</ta>
            <ta e="T185" id="Seg_1805" s="T184">old.woman-TRL-PST.NAR-1SG.S</ta>
            <ta e="T186" id="Seg_1806" s="T185">big-ADVZ</ta>
            <ta e="T187" id="Seg_1807" s="T186">and</ta>
            <ta e="T188" id="Seg_1808" s="T187">forget-HAB-1SG.O</ta>
            <ta e="T189" id="Seg_1809" s="T188">how</ta>
            <ta e="T190" id="Seg_1810" s="T189">(s)he-EP-ACC</ta>
            <ta e="T191" id="Seg_1811" s="T190">call-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1812" s="T1">этот-день.[NOM]</ta>
            <ta e="T3" id="Seg_1813" s="T2">Пасха.[NOM]</ta>
            <ta e="T4" id="Seg_1814" s="T3">первый</ta>
            <ta e="T5" id="Seg_1815" s="T4">день.[NOM]</ta>
            <ta e="T6" id="Seg_1816" s="T5">Пасха-ADJZ</ta>
            <ta e="T7" id="Seg_1817" s="T6">день.[NOM]</ta>
            <ta e="T8" id="Seg_1818" s="T7">Алёна.[NOM]</ta>
            <ta e="T9" id="Seg_1819" s="T8">встать-PST.NAR-3SG.S</ta>
            <ta e="T10" id="Seg_1820" s="T9">раньше</ta>
            <ta e="T11" id="Seg_1821" s="T10">человек-PL-ABL</ta>
            <ta e="T12" id="Seg_1822" s="T11">Алёна.[NOM]</ta>
            <ta e="T13" id="Seg_1823" s="T12">посмотреть-DUR-3SG.O</ta>
            <ta e="T14" id="Seg_1824" s="T13">где</ta>
            <ta e="T15" id="Seg_1825" s="T14">что.[NOM]</ta>
            <ta e="T16" id="Seg_1826" s="T15">быть-3SG.S</ta>
            <ta e="T17" id="Seg_1827" s="T16">мальчик-PL-ACC</ta>
            <ta e="T18" id="Seg_1828" s="T17">пустить-PST.NAR-3SG.O</ta>
            <ta e="T19" id="Seg_1829" s="T18">бог-GEN-чум-ILL</ta>
            <ta e="T20" id="Seg_1830" s="T19">и</ta>
            <ta e="T21" id="Seg_1831" s="T20">девушка-PL-ACC</ta>
            <ta e="T22" id="Seg_1832" s="T21">пустить-PST.NAR-3SG.O</ta>
            <ta e="T23" id="Seg_1833" s="T22">бог-GEN-чум-ILL</ta>
            <ta e="T24" id="Seg_1834" s="T23">сам.3SG</ta>
            <ta e="T25" id="Seg_1835" s="T24">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T26" id="Seg_1836" s="T25">сад-EP-ACC</ta>
            <ta e="T27" id="Seg_1837" s="T26">ходить.кругом-TR-INF</ta>
            <ta e="T28" id="Seg_1838" s="T27">солнце.[NOM]</ta>
            <ta e="T29" id="Seg_1839" s="T28">появиться-RFL-PST.NAR-3SG.S</ta>
            <ta e="T30" id="Seg_1840" s="T29">дерево-GEN</ta>
            <ta e="T31" id="Seg_1841" s="T30">верхняя.часть-EP-EL.3SG</ta>
            <ta e="T32" id="Seg_1842" s="T31">вечер.[NOM]</ta>
            <ta e="T33" id="Seg_1843" s="T32">рассвет.[NOM]</ta>
            <ta e="T34" id="Seg_1844" s="T33">утро-ADJZ</ta>
            <ta e="T35" id="Seg_1845" s="T34">рассвет.[NOM]</ta>
            <ta e="T36" id="Seg_1846" s="T35">NEG.EX.[3SG.S]</ta>
            <ta e="T37" id="Seg_1847" s="T36">давно-ADJZ</ta>
            <ta e="T38" id="Seg_1848" s="T37">бог-GEN-дом-EL.3SG</ta>
            <ta e="T39" id="Seg_1849" s="T38">бог-GEN-дом-EP-GEN</ta>
            <ta e="T40" id="Seg_1850" s="T39">железо-%%</ta>
            <ta e="T41" id="Seg_1851" s="T40">слышаться-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_1852" s="T41">давно-ADJZ</ta>
            <ta e="T43" id="Seg_1853" s="T42">бог-GEN-дом-EP-ACC</ta>
            <ta e="T44" id="Seg_1854" s="T43">железо-%%</ta>
            <ta e="T45" id="Seg_1855" s="T44">побить-PST-3PL</ta>
            <ta e="T46" id="Seg_1856" s="T45">весь</ta>
            <ta e="T47" id="Seg_1857" s="T46">%%-PST.[3SG.S]</ta>
            <ta e="T48" id="Seg_1858" s="T47">я.NOM</ta>
            <ta e="T49" id="Seg_1859" s="T48">спина-CAR-TRL-CO-1SG.S</ta>
            <ta e="T50" id="Seg_1860" s="T49">он(а)-EP-GEN</ta>
            <ta e="T51" id="Seg_1861" s="T50">сердце.[NOM]-3SG</ta>
            <ta e="T52" id="Seg_1862" s="T51">Алёна-GEN</ta>
            <ta e="T53" id="Seg_1863" s="T52">сердце.[NOM]</ta>
            <ta e="T54" id="Seg_1864" s="T53">шевелиться-MOM-EP-PST.NAR.[3SG.S]</ta>
            <ta e="T55" id="Seg_1865" s="T54">как.будто</ta>
            <ta e="T56" id="Seg_1866" s="T55">нож-ADVZ</ta>
            <ta e="T57" id="Seg_1867" s="T56">отрезать-PST.NAR-3PL</ta>
            <ta e="T58" id="Seg_1868" s="T57">что-TRL</ta>
            <ta e="T59" id="Seg_1869" s="T58">он(а)-ALL</ta>
            <ta e="T60" id="Seg_1870" s="T59">обрадоваться-DUR-INF</ta>
            <ta e="T61" id="Seg_1871" s="T60">он(а).[NOM]</ta>
            <ta e="T62" id="Seg_1872" s="T61">когда-EMPH</ta>
            <ta e="T63" id="Seg_1873" s="T62">NEG</ta>
            <ta e="T64" id="Seg_1874" s="T63">обрадоваться-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1875" s="T64">поэтому</ta>
            <ta e="T66" id="Seg_1876" s="T65">жить-PST.NAR-3SG.S</ta>
            <ta e="T67" id="Seg_1877" s="T66">чтобы</ta>
            <ta e="T68" id="Seg_1878" s="T67">NEG.IMP</ta>
            <ta e="T69" id="Seg_1879" s="T68">умереть-INF</ta>
            <ta e="T70" id="Seg_1880" s="T69">NEG</ta>
            <ta e="T71" id="Seg_1881" s="T70">хотеть-PST.NAR.[3SG.S]</ta>
            <ta e="T72" id="Seg_1882" s="T71">умереть-INF</ta>
            <ta e="T73" id="Seg_1883" s="T72">хотеть-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1884" s="T73">долго</ta>
            <ta e="T75" id="Seg_1885" s="T74">жить-INF</ta>
            <ta e="T76" id="Seg_1886" s="T75">он(а).[NOM]-EP-EMPH</ta>
            <ta e="T77" id="Seg_1887" s="T76">думать-PST.NAR-3SG.S</ta>
            <ta e="T78" id="Seg_1888" s="T77">все</ta>
            <ta e="T79" id="Seg_1889" s="T78">умереть-RES.[3SG.S]</ta>
            <ta e="T80" id="Seg_1890" s="T79">рука-EP-ACC-%%</ta>
            <ta e="T81" id="Seg_1891" s="T80">лоб-ILL.3SG</ta>
            <ta e="T82" id="Seg_1892" s="T81">взять-PST.NAR-3SG.O</ta>
            <ta e="T83" id="Seg_1893" s="T82">устать-RFL-PTCP.PST</ta>
            <ta e="T84" id="Seg_1894" s="T83">давно-EP-ADJZ</ta>
            <ta e="T85" id="Seg_1895" s="T84">день.[NOM]-EP-3SG</ta>
            <ta e="T86" id="Seg_1896" s="T85">думать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T87" id="Seg_1897" s="T86">как</ta>
            <ta e="T88" id="Seg_1898" s="T87">давно</ta>
            <ta e="T89" id="Seg_1899" s="T88">жить-PST.NAR-3PL</ta>
            <ta e="T90" id="Seg_1900" s="T89">а</ta>
            <ta e="T91" id="Seg_1901" s="T90">теперь</ta>
            <ta e="T92" id="Seg_1902" s="T91">думать-3SG.S</ta>
            <ta e="T93" id="Seg_1903" s="T92">как</ta>
            <ta e="T94" id="Seg_1904" s="T93">теперь</ta>
            <ta e="T95" id="Seg_1905" s="T94">жить-INF</ta>
            <ta e="T96" id="Seg_1906" s="T95">что-ACC</ta>
            <ta e="T97" id="Seg_1907" s="T96">сделать-HAB-INF</ta>
            <ta e="T98" id="Seg_1908" s="T97">сад-EP-GEN</ta>
            <ta e="T99" id="Seg_1909" s="T98">бок-LOC</ta>
            <ta e="T100" id="Seg_1910" s="T99">ходить-PST.NAR-3SG.S</ta>
            <ta e="T101" id="Seg_1911" s="T100">старуха-DIM.[NOM]</ta>
            <ta e="T102" id="Seg_1912" s="T101">%%</ta>
            <ta e="T103" id="Seg_1913" s="T102">говорить-INFER.[3SG.S]</ta>
            <ta e="T104" id="Seg_1914" s="T103">дурак-ADJZ</ta>
            <ta e="T105" id="Seg_1915" s="T104">как-ADJZ</ta>
            <ta e="T106" id="Seg_1916" s="T105">человек-GEN</ta>
            <ta e="T107" id="Seg_1917" s="T106">про</ta>
            <ta e="T108" id="Seg_1918" s="T107">говорить-3SG.S</ta>
            <ta e="T109" id="Seg_1919" s="T108">старуха-DIM.[NOM]</ta>
            <ta e="T110" id="Seg_1920" s="T109">остановиться-EP-3SG.S</ta>
            <ta e="T111" id="Seg_1921" s="T110">старуха-DIM.[NOM]</ta>
            <ta e="T112" id="Seg_1922" s="T111">ходить-CVB</ta>
            <ta e="T113" id="Seg_1923" s="T112">стоять-INCH-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T114" id="Seg_1924" s="T113">бог-GEN-дом-ILL</ta>
            <ta e="T115" id="Seg_1925" s="T114">ходить-2SG.S</ta>
            <ta e="T116" id="Seg_1926" s="T115">ты.NOM</ta>
            <ta e="T117" id="Seg_1927" s="T116">девушка.[NOM]</ta>
            <ta e="T118" id="Seg_1928" s="T117">погодя-ATTEN</ta>
            <ta e="T119" id="Seg_1929" s="T118">отправиться-FUT-1SG.S-FUT</ta>
            <ta e="T120" id="Seg_1930" s="T119">бог-GEN-дом.[NOM]</ta>
            <ta e="T121" id="Seg_1931" s="T120">сейчас</ta>
            <ta e="T122" id="Seg_1932" s="T121">рано</ta>
            <ta e="T123" id="Seg_1933" s="T122">быть-3SG.S</ta>
            <ta e="T124" id="Seg_1934" s="T123">отправиться-INF</ta>
            <ta e="T125" id="Seg_1935" s="T124">бог-GEN-дом-ILL</ta>
            <ta e="T126" id="Seg_1936" s="T125">вчера</ta>
            <ta e="T127" id="Seg_1937" s="T126">зайти-HAB-PST-1SG.S</ta>
            <ta e="T128" id="Seg_1938" s="T127">один</ta>
            <ta e="T129" id="Seg_1939" s="T128">женщина-ADJZ-человек-ALL</ta>
            <ta e="T130" id="Seg_1940" s="T129">сидеть-DRV-INF</ta>
            <ta e="T131" id="Seg_1941" s="T130">этот-EMPH</ta>
            <ta e="T132" id="Seg_1942" s="T131">женщина-ADJZ-человек.[NOM]</ta>
            <ta e="T133" id="Seg_1943" s="T132">сказать-RES-HAB-3SG.S</ta>
            <ta e="T134" id="Seg_1944" s="T133">я.ALL</ta>
            <ta e="T135" id="Seg_1945" s="T134">чтобы</ta>
            <ta e="T136" id="Seg_1946" s="T135">бог-GEN-дом.[NOM]</ta>
            <ta e="T137" id="Seg_1947" s="T136">сходить-HAB-INF</ta>
            <ta e="T138" id="Seg_1948" s="T137">бог-GEN-дом-ILL</ta>
            <ta e="T139" id="Seg_1949" s="T138">бог-GEN-дом-LOC</ta>
            <ta e="T140" id="Seg_1950" s="T139">ты.ALL</ta>
            <ta e="T141" id="Seg_1951" s="T140">хотеть-3PL</ta>
            <ta e="T142" id="Seg_1952" s="T141">что-ACC-то</ta>
            <ta e="T143" id="Seg_1953" s="T142">сказать-INF</ta>
            <ta e="T144" id="Seg_1954" s="T143">хороший</ta>
            <ta e="T145" id="Seg_1955" s="T144">он(а)-PL.[NOM]</ta>
            <ta e="T146" id="Seg_1956" s="T145">этот-COR</ta>
            <ta e="T147" id="Seg_1957" s="T146">сварить-RES-3PL</ta>
            <ta e="T148" id="Seg_1958" s="T147">съесть-PTCP.NEC-нечто-ACC</ta>
            <ta e="T149" id="Seg_1959" s="T148">пахнуть-3SG.S</ta>
            <ta e="T150" id="Seg_1960" s="T149">далеко</ta>
            <ta e="T151" id="Seg_1961" s="T150">съесть-PTCP.NEC-нечто.[NOM]</ta>
            <ta e="T152" id="Seg_1962" s="T151">сварить-CAUS-PST.NAR-3PL</ta>
            <ta e="T153" id="Seg_1963" s="T152">мясо.[NOM]</ta>
            <ta e="T154" id="Seg_1964" s="T153">рыба.[NOM]</ta>
            <ta e="T155" id="Seg_1965" s="T154">высохнуть-CAUS-PTCP.PST</ta>
            <ta e="T156" id="Seg_1966" s="T155">рыба.[NOM]</ta>
            <ta e="T157" id="Seg_1967" s="T156">хлеб.[NOM]</ta>
            <ta e="T158" id="Seg_1968" s="T157">сделать-PST.NAR-INFER-3PL</ta>
            <ta e="T159" id="Seg_1969" s="T158">хороший</ta>
            <ta e="T160" id="Seg_1970" s="T159">сварить-CAUS-PST.NAR-3PL</ta>
            <ta e="T161" id="Seg_1971" s="T160">кто.[NOM]</ta>
            <ta e="T162" id="Seg_1972" s="T161">приехать-PST.NAR-INFER-3SG.S</ta>
            <ta e="T163" id="Seg_1973" s="T162">вы.PL-ILL.3SG-2PL</ta>
            <ta e="T164" id="Seg_1974" s="T163">он(а)-PL-GEN-%%-ILL</ta>
            <ta e="T165" id="Seg_1975" s="T164">приехать-PST.NAR-3SG.S</ta>
            <ta e="T166" id="Seg_1976" s="T165">что.[NOM]-INDEF</ta>
            <ta e="T167" id="Seg_1977" s="T166">человек.[NOM]</ta>
            <ta e="T168" id="Seg_1978" s="T167">сестра.[NOM]-3SG</ta>
            <ta e="T169" id="Seg_1979" s="T168">приехать-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T170" id="Seg_1980" s="T169">умереть-PTCP.PST</ta>
            <ta e="T171" id="Seg_1981" s="T170">старшая.сестра-GEN-3SG</ta>
            <ta e="T172" id="Seg_1982" s="T171">NEG</ta>
            <ta e="T173" id="Seg_1983" s="T172">жениться-PTCP.PST</ta>
            <ta e="T174" id="Seg_1984" s="T173">ребенок.[NOM]</ta>
            <ta e="T175" id="Seg_1985" s="T174">мужчина-человек.[NOM]</ta>
            <ta e="T176" id="Seg_1986" s="T175">прийти-PST.NAR-INFER-3SG.S</ta>
            <ta e="T177" id="Seg_1987" s="T176">лошадь-INSTR</ta>
            <ta e="T178" id="Seg_1988" s="T177">лошадь-GEN</ta>
            <ta e="T179" id="Seg_1989" s="T178">верхняя.часть-EP-LOC</ta>
            <ta e="T180" id="Seg_1990" s="T179">сидеть-CVB</ta>
            <ta e="T181" id="Seg_1991" s="T180">хороший-LOC.3SG-ADJZ</ta>
            <ta e="T182" id="Seg_1992" s="T181">а</ta>
            <ta e="T183" id="Seg_1993" s="T182">шапка.[NOM]-3SG</ta>
            <ta e="T184" id="Seg_1994" s="T183">серый.[3SG.S]</ta>
            <ta e="T185" id="Seg_1995" s="T184">старуха-TRL-PST.NAR-1SG.S</ta>
            <ta e="T186" id="Seg_1996" s="T185">большой-ADVZ</ta>
            <ta e="T187" id="Seg_1997" s="T186">и</ta>
            <ta e="T188" id="Seg_1998" s="T187">забыть-HAB-1SG.O</ta>
            <ta e="T189" id="Seg_1999" s="T188">как</ta>
            <ta e="T190" id="Seg_2000" s="T189">он(а)-EP-ACC</ta>
            <ta e="T191" id="Seg_2001" s="T190">позвать-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2002" s="T1">dem-n.[n:case]</ta>
            <ta e="T3" id="Seg_2003" s="T2">nprop.[n:case]</ta>
            <ta e="T4" id="Seg_2004" s="T3">adj</ta>
            <ta e="T5" id="Seg_2005" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_2006" s="T5">nprop-n&gt;adj</ta>
            <ta e="T7" id="Seg_2007" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_2008" s="T7">nprop.[n:case]</ta>
            <ta e="T9" id="Seg_2009" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_2010" s="T9">adv</ta>
            <ta e="T11" id="Seg_2011" s="T10">n-n:num-n:case</ta>
            <ta e="T12" id="Seg_2012" s="T11">nprop.[n:case]</ta>
            <ta e="T13" id="Seg_2013" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_2014" s="T13">conj</ta>
            <ta e="T15" id="Seg_2015" s="T14">interrog.[n:case]</ta>
            <ta e="T16" id="Seg_2016" s="T15">v-v:pn</ta>
            <ta e="T17" id="Seg_2017" s="T16">n-n:num-n:case</ta>
            <ta e="T18" id="Seg_2018" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_2019" s="T18">n-n:case-n-n:case</ta>
            <ta e="T20" id="Seg_2020" s="T19">conj</ta>
            <ta e="T21" id="Seg_2021" s="T20">n-n:num-n:case</ta>
            <ta e="T22" id="Seg_2022" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_2023" s="T22">n-n:case-n-n:case</ta>
            <ta e="T24" id="Seg_2024" s="T23">emphpro</ta>
            <ta e="T25" id="Seg_2025" s="T24">v-v:tense.[v:pn]</ta>
            <ta e="T26" id="Seg_2026" s="T25">n-n:ins-n:case</ta>
            <ta e="T27" id="Seg_2027" s="T26">v-v&gt;v-v:inf</ta>
            <ta e="T28" id="Seg_2028" s="T27">n.[n:case]</ta>
            <ta e="T29" id="Seg_2029" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_2030" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_2031" s="T30">n-n:ins-n:case.poss</ta>
            <ta e="T32" id="Seg_2032" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_2033" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_2034" s="T33">n-n&gt;adj</ta>
            <ta e="T35" id="Seg_2035" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_2036" s="T35">v.[v:pn]</ta>
            <ta e="T37" id="Seg_2037" s="T36">adv-adv&gt;adj</ta>
            <ta e="T38" id="Seg_2038" s="T37">n-n:case-n-n:case.poss</ta>
            <ta e="T39" id="Seg_2039" s="T38">n-n:case-n-n:ins-n:case</ta>
            <ta e="T40" id="Seg_2040" s="T39">n-%%</ta>
            <ta e="T41" id="Seg_2041" s="T40">v-v:tense.[v:pn]</ta>
            <ta e="T42" id="Seg_2042" s="T41">adv-adv&gt;adj</ta>
            <ta e="T43" id="Seg_2043" s="T42">n-n:case-n-n:ins-n:case</ta>
            <ta e="T44" id="Seg_2044" s="T43">n-%%</ta>
            <ta e="T45" id="Seg_2045" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_2046" s="T45">quant</ta>
            <ta e="T47" id="Seg_2047" s="T46">v-v:tense.[v:pn]</ta>
            <ta e="T48" id="Seg_2048" s="T47">pers</ta>
            <ta e="T49" id="Seg_2049" s="T48">n-n&gt;adj-n&gt;v-v:ins-v:pn</ta>
            <ta e="T50" id="Seg_2050" s="T49">pers-n:ins-n:case</ta>
            <ta e="T51" id="Seg_2051" s="T50">n.[n:case]-n:poss</ta>
            <ta e="T52" id="Seg_2052" s="T51">nprop-n:case</ta>
            <ta e="T53" id="Seg_2053" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_2054" s="T53">v-v&gt;v-v:ins-v:tense.[v:pn]</ta>
            <ta e="T55" id="Seg_2055" s="T54">conj</ta>
            <ta e="T56" id="Seg_2056" s="T55">n-n&gt;adv</ta>
            <ta e="T57" id="Seg_2057" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_2058" s="T57">interrog-n:case</ta>
            <ta e="T59" id="Seg_2059" s="T58">pers-n:case</ta>
            <ta e="T60" id="Seg_2060" s="T59">v-v&gt;v-v:inf</ta>
            <ta e="T61" id="Seg_2061" s="T60">pers.[n:case]</ta>
            <ta e="T62" id="Seg_2062" s="T61">conj-clit</ta>
            <ta e="T63" id="Seg_2063" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_2064" s="T63">v-v&gt;v.[v:pn]</ta>
            <ta e="T65" id="Seg_2065" s="T64">conj</ta>
            <ta e="T66" id="Seg_2066" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2067" s="T66">conj</ta>
            <ta e="T68" id="Seg_2068" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_2069" s="T68">v-v:inf</ta>
            <ta e="T70" id="Seg_2070" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_2071" s="T70">v-v:tense.[v:pn]</ta>
            <ta e="T72" id="Seg_2072" s="T71">v-v:inf</ta>
            <ta e="T73" id="Seg_2073" s="T72">v-v:tense.[v:pn]</ta>
            <ta e="T74" id="Seg_2074" s="T73">adv</ta>
            <ta e="T75" id="Seg_2075" s="T74">v-v:inf</ta>
            <ta e="T76" id="Seg_2076" s="T75">pers.[n:case]-n:ins-clit</ta>
            <ta e="T77" id="Seg_2077" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2078" s="T77">quant</ta>
            <ta e="T79" id="Seg_2079" s="T78">v-v&gt;v.[v:pn]</ta>
            <ta e="T80" id="Seg_2080" s="T79">n-n:ins-n:case-%%</ta>
            <ta e="T81" id="Seg_2081" s="T80">n-n:case.poss</ta>
            <ta e="T82" id="Seg_2082" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_2083" s="T82">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T84" id="Seg_2084" s="T83">adv-n:ins-adv&gt;adj</ta>
            <ta e="T85" id="Seg_2085" s="T84">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T86" id="Seg_2086" s="T85">v-v:tense-v:mood-v:pn</ta>
            <ta e="T87" id="Seg_2087" s="T86">interrog</ta>
            <ta e="T88" id="Seg_2088" s="T87">adv</ta>
            <ta e="T89" id="Seg_2089" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2090" s="T89">conj</ta>
            <ta e="T91" id="Seg_2091" s="T90">adv</ta>
            <ta e="T92" id="Seg_2092" s="T91">v-v:pn</ta>
            <ta e="T93" id="Seg_2093" s="T92">interrog</ta>
            <ta e="T94" id="Seg_2094" s="T93">adv</ta>
            <ta e="T95" id="Seg_2095" s="T94">v-v:inf</ta>
            <ta e="T96" id="Seg_2096" s="T95">interrog-n:case</ta>
            <ta e="T97" id="Seg_2097" s="T96">v-v&gt;v-v:inf</ta>
            <ta e="T98" id="Seg_2098" s="T97">n-n:ins-n:case</ta>
            <ta e="T99" id="Seg_2099" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_2100" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_2101" s="T100">n-n&gt;n.[n:case]</ta>
            <ta e="T102" id="Seg_2102" s="T101">%%</ta>
            <ta e="T103" id="Seg_2103" s="T102">v-v:mood.[v:pn]</ta>
            <ta e="T104" id="Seg_2104" s="T103">n-n&gt;adj</ta>
            <ta e="T105" id="Seg_2105" s="T104">conj-adv&gt;adj</ta>
            <ta e="T106" id="Seg_2106" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2107" s="T106">pp</ta>
            <ta e="T108" id="Seg_2108" s="T107">v-v:pn</ta>
            <ta e="T109" id="Seg_2109" s="T108">n-n&gt;n.[n:case]</ta>
            <ta e="T110" id="Seg_2110" s="T109">v-v:ins-v:pn</ta>
            <ta e="T111" id="Seg_2111" s="T110">n-n&gt;n.[n:case]</ta>
            <ta e="T112" id="Seg_2112" s="T111">v-v&gt;adv</ta>
            <ta e="T113" id="Seg_2113" s="T112">v-v&gt;v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T114" id="Seg_2114" s="T113">n-n:case-n-n:case</ta>
            <ta e="T115" id="Seg_2115" s="T114">v-v:pn</ta>
            <ta e="T116" id="Seg_2116" s="T115">pers</ta>
            <ta e="T117" id="Seg_2117" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_2118" s="T117">adv-adj&gt;adj</ta>
            <ta e="T119" id="Seg_2119" s="T118">v-v:tense-v:pn-v:tense</ta>
            <ta e="T120" id="Seg_2120" s="T119">n-n:case-n.[n:case]</ta>
            <ta e="T121" id="Seg_2121" s="T120">adv</ta>
            <ta e="T122" id="Seg_2122" s="T121">adv</ta>
            <ta e="T123" id="Seg_2123" s="T122">v-v:pn</ta>
            <ta e="T124" id="Seg_2124" s="T123">v-v:inf</ta>
            <ta e="T125" id="Seg_2125" s="T124">n-n:case-n-n:case</ta>
            <ta e="T126" id="Seg_2126" s="T125">adv</ta>
            <ta e="T127" id="Seg_2127" s="T126">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2128" s="T127">num</ta>
            <ta e="T129" id="Seg_2129" s="T128">n-n&gt;adj-n-n:case</ta>
            <ta e="T130" id="Seg_2130" s="T129">v-v&gt;v-v:inf</ta>
            <ta e="T131" id="Seg_2131" s="T130">dem-clit</ta>
            <ta e="T132" id="Seg_2132" s="T131">n-n&gt;adj-n.[n:case]</ta>
            <ta e="T133" id="Seg_2133" s="T132">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T134" id="Seg_2134" s="T133">pers</ta>
            <ta e="T135" id="Seg_2135" s="T134">conj</ta>
            <ta e="T136" id="Seg_2136" s="T135">n-n:case-n.[n:case]</ta>
            <ta e="T137" id="Seg_2137" s="T136">v-v&gt;v-v:inf</ta>
            <ta e="T138" id="Seg_2138" s="T137">n-n:case-n-n:case</ta>
            <ta e="T139" id="Seg_2139" s="T138">n-n:case-n-n:case</ta>
            <ta e="T140" id="Seg_2140" s="T139">pers</ta>
            <ta e="T141" id="Seg_2141" s="T140">v-v:pn</ta>
            <ta e="T142" id="Seg_2142" s="T141">interrog-n:case-ptcl</ta>
            <ta e="T143" id="Seg_2143" s="T142">v-v:inf</ta>
            <ta e="T144" id="Seg_2144" s="T143">adj</ta>
            <ta e="T145" id="Seg_2145" s="T144">pers-n:num.[n:case]</ta>
            <ta e="T146" id="Seg_2146" s="T145">dem-n:case</ta>
            <ta e="T147" id="Seg_2147" s="T146">v-v&gt;v-v:pn</ta>
            <ta e="T148" id="Seg_2148" s="T147">v-v&gt;ptcp-n-n:case</ta>
            <ta e="T149" id="Seg_2149" s="T148">v-v:pn</ta>
            <ta e="T150" id="Seg_2150" s="T149">adv</ta>
            <ta e="T151" id="Seg_2151" s="T150">v-v&gt;ptcp-n.[n:case]</ta>
            <ta e="T152" id="Seg_2152" s="T151">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2153" s="T152">n.[n:case]</ta>
            <ta e="T154" id="Seg_2154" s="T153">n.[n:case]</ta>
            <ta e="T155" id="Seg_2155" s="T154">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T156" id="Seg_2156" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_2157" s="T156">n.[n:case]</ta>
            <ta e="T158" id="Seg_2158" s="T157">v-v:tense-v:mood-v:pn</ta>
            <ta e="T159" id="Seg_2159" s="T158">adj</ta>
            <ta e="T160" id="Seg_2160" s="T159">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2161" s="T160">interrog.[n:case]</ta>
            <ta e="T162" id="Seg_2162" s="T161">v-v:tense-v:mood-v:pn</ta>
            <ta e="T163" id="Seg_2163" s="T162">pers-n:case.poss-n:poss</ta>
            <ta e="T164" id="Seg_2164" s="T163">pers-n:num-n:case-%%-n:case</ta>
            <ta e="T165" id="Seg_2165" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_2166" s="T165">interrog.[n:case]-clit</ta>
            <ta e="T167" id="Seg_2167" s="T166">n.[n:case]</ta>
            <ta e="T168" id="Seg_2168" s="T167">n.[n:case]-n:poss</ta>
            <ta e="T169" id="Seg_2169" s="T168">v-v:tense-v:mood.[v:pn]</ta>
            <ta e="T170" id="Seg_2170" s="T169">v-v&gt;ptcp</ta>
            <ta e="T171" id="Seg_2171" s="T170">n-n:case-n:poss</ta>
            <ta e="T172" id="Seg_2172" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_2173" s="T172">v-v&gt;ptcp</ta>
            <ta e="T174" id="Seg_2174" s="T173">n.[n:case]</ta>
            <ta e="T175" id="Seg_2175" s="T174">n-n.[n:case]</ta>
            <ta e="T176" id="Seg_2176" s="T175">v-v:tense-v:mood-v:pn</ta>
            <ta e="T177" id="Seg_2177" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_2178" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2179" s="T178">n-n:ins-n:case</ta>
            <ta e="T180" id="Seg_2180" s="T179">v-v&gt;adv</ta>
            <ta e="T181" id="Seg_2181" s="T180">adj-n:case.poss-n&gt;adj</ta>
            <ta e="T182" id="Seg_2182" s="T181">conj</ta>
            <ta e="T183" id="Seg_2183" s="T182">n.[n:case]-n:poss</ta>
            <ta e="T184" id="Seg_2184" s="T183">adj.[v:pn]</ta>
            <ta e="T185" id="Seg_2185" s="T184">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_2186" s="T185">adj-adj&gt;adv</ta>
            <ta e="T187" id="Seg_2187" s="T186">conj</ta>
            <ta e="T188" id="Seg_2188" s="T187">v-v&gt;v-v:pn</ta>
            <ta e="T189" id="Seg_2189" s="T188">conj</ta>
            <ta e="T190" id="Seg_2190" s="T189">pers-n:ins-n:case</ta>
            <ta e="T191" id="Seg_2191" s="T190">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2192" s="T1">adv</ta>
            <ta e="T3" id="Seg_2193" s="T2">nprop</ta>
            <ta e="T4" id="Seg_2194" s="T3">adj</ta>
            <ta e="T5" id="Seg_2195" s="T4">n</ta>
            <ta e="T6" id="Seg_2196" s="T5">adj</ta>
            <ta e="T7" id="Seg_2197" s="T6">n</ta>
            <ta e="T8" id="Seg_2198" s="T7">nprop</ta>
            <ta e="T9" id="Seg_2199" s="T8">v</ta>
            <ta e="T10" id="Seg_2200" s="T9">adv</ta>
            <ta e="T11" id="Seg_2201" s="T10">n</ta>
            <ta e="T12" id="Seg_2202" s="T11">nprop</ta>
            <ta e="T13" id="Seg_2203" s="T12">v</ta>
            <ta e="T14" id="Seg_2204" s="T13">conj</ta>
            <ta e="T15" id="Seg_2205" s="T14">interrog</ta>
            <ta e="T16" id="Seg_2206" s="T15">v</ta>
            <ta e="T17" id="Seg_2207" s="T16">n</ta>
            <ta e="T18" id="Seg_2208" s="T17">v</ta>
            <ta e="T19" id="Seg_2209" s="T18">n</ta>
            <ta e="T20" id="Seg_2210" s="T19">conj</ta>
            <ta e="T21" id="Seg_2211" s="T20">n</ta>
            <ta e="T22" id="Seg_2212" s="T21">v</ta>
            <ta e="T23" id="Seg_2213" s="T22">n</ta>
            <ta e="T24" id="Seg_2214" s="T23">emphpro</ta>
            <ta e="T25" id="Seg_2215" s="T24">v</ta>
            <ta e="T26" id="Seg_2216" s="T25">n</ta>
            <ta e="T27" id="Seg_2217" s="T26">v</ta>
            <ta e="T28" id="Seg_2218" s="T27">n</ta>
            <ta e="T29" id="Seg_2219" s="T28">v</ta>
            <ta e="T30" id="Seg_2220" s="T29">n</ta>
            <ta e="T31" id="Seg_2221" s="T30">n</ta>
            <ta e="T32" id="Seg_2222" s="T31">n</ta>
            <ta e="T33" id="Seg_2223" s="T32">n</ta>
            <ta e="T34" id="Seg_2224" s="T33">n</ta>
            <ta e="T35" id="Seg_2225" s="T34">n</ta>
            <ta e="T36" id="Seg_2226" s="T35">v</ta>
            <ta e="T37" id="Seg_2227" s="T36">adj</ta>
            <ta e="T38" id="Seg_2228" s="T37">n</ta>
            <ta e="T39" id="Seg_2229" s="T38">n</ta>
            <ta e="T40" id="Seg_2230" s="T39">n</ta>
            <ta e="T41" id="Seg_2231" s="T40">v</ta>
            <ta e="T42" id="Seg_2232" s="T41">adj</ta>
            <ta e="T43" id="Seg_2233" s="T42">n</ta>
            <ta e="T44" id="Seg_2234" s="T43">n</ta>
            <ta e="T45" id="Seg_2235" s="T44">v</ta>
            <ta e="T46" id="Seg_2236" s="T45">quant</ta>
            <ta e="T47" id="Seg_2237" s="T46">v</ta>
            <ta e="T48" id="Seg_2238" s="T47">pers</ta>
            <ta e="T49" id="Seg_2239" s="T48">v</ta>
            <ta e="T50" id="Seg_2240" s="T49">pers</ta>
            <ta e="T51" id="Seg_2241" s="T50">n</ta>
            <ta e="T52" id="Seg_2242" s="T51">nprop</ta>
            <ta e="T53" id="Seg_2243" s="T52">n</ta>
            <ta e="T54" id="Seg_2244" s="T53">v</ta>
            <ta e="T55" id="Seg_2245" s="T54">conj</ta>
            <ta e="T56" id="Seg_2246" s="T55">adv</ta>
            <ta e="T57" id="Seg_2247" s="T56">v</ta>
            <ta e="T58" id="Seg_2248" s="T57">interrog</ta>
            <ta e="T59" id="Seg_2249" s="T58">pers</ta>
            <ta e="T60" id="Seg_2250" s="T59">v</ta>
            <ta e="T61" id="Seg_2251" s="T60">pers</ta>
            <ta e="T62" id="Seg_2252" s="T61">conj</ta>
            <ta e="T63" id="Seg_2253" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_2254" s="T63">v</ta>
            <ta e="T65" id="Seg_2255" s="T64">conj</ta>
            <ta e="T66" id="Seg_2256" s="T65">v</ta>
            <ta e="T67" id="Seg_2257" s="T66">conj</ta>
            <ta e="T68" id="Seg_2258" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_2259" s="T68">v</ta>
            <ta e="T70" id="Seg_2260" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_2261" s="T70">v</ta>
            <ta e="T72" id="Seg_2262" s="T71">v</ta>
            <ta e="T73" id="Seg_2263" s="T72">v</ta>
            <ta e="T74" id="Seg_2264" s="T73">adv</ta>
            <ta e="T75" id="Seg_2265" s="T74">v</ta>
            <ta e="T76" id="Seg_2266" s="T75">pers</ta>
            <ta e="T77" id="Seg_2267" s="T76">v</ta>
            <ta e="T78" id="Seg_2268" s="T77">quant</ta>
            <ta e="T79" id="Seg_2269" s="T78">v</ta>
            <ta e="T80" id="Seg_2270" s="T79">v</ta>
            <ta e="T81" id="Seg_2271" s="T80">n</ta>
            <ta e="T82" id="Seg_2272" s="T81">v</ta>
            <ta e="T83" id="Seg_2273" s="T82">ptcp</ta>
            <ta e="T84" id="Seg_2274" s="T83">adj</ta>
            <ta e="T85" id="Seg_2275" s="T84">n</ta>
            <ta e="T86" id="Seg_2276" s="T85">v</ta>
            <ta e="T87" id="Seg_2277" s="T86">interrog</ta>
            <ta e="T88" id="Seg_2278" s="T87">adv</ta>
            <ta e="T89" id="Seg_2279" s="T88">v</ta>
            <ta e="T90" id="Seg_2280" s="T89">conj</ta>
            <ta e="T91" id="Seg_2281" s="T90">adv</ta>
            <ta e="T92" id="Seg_2282" s="T91">v</ta>
            <ta e="T93" id="Seg_2283" s="T92">interrog</ta>
            <ta e="T94" id="Seg_2284" s="T93">adv</ta>
            <ta e="T95" id="Seg_2285" s="T94">v</ta>
            <ta e="T96" id="Seg_2286" s="T95">interrog</ta>
            <ta e="T97" id="Seg_2287" s="T96">v</ta>
            <ta e="T98" id="Seg_2288" s="T97">n</ta>
            <ta e="T99" id="Seg_2289" s="T98">n</ta>
            <ta e="T100" id="Seg_2290" s="T99">v</ta>
            <ta e="T101" id="Seg_2291" s="T100">n</ta>
            <ta e="T103" id="Seg_2292" s="T102">v</ta>
            <ta e="T104" id="Seg_2293" s="T103">adj</ta>
            <ta e="T105" id="Seg_2294" s="T104">conj</ta>
            <ta e="T106" id="Seg_2295" s="T105">n</ta>
            <ta e="T107" id="Seg_2296" s="T106">pp</ta>
            <ta e="T108" id="Seg_2297" s="T107">v</ta>
            <ta e="T109" id="Seg_2298" s="T108">n</ta>
            <ta e="T110" id="Seg_2299" s="T109">v</ta>
            <ta e="T111" id="Seg_2300" s="T110">n</ta>
            <ta e="T112" id="Seg_2301" s="T111">adv</ta>
            <ta e="T113" id="Seg_2302" s="T112">v</ta>
            <ta e="T114" id="Seg_2303" s="T113">n</ta>
            <ta e="T115" id="Seg_2304" s="T114">v</ta>
            <ta e="T116" id="Seg_2305" s="T115">pers</ta>
            <ta e="T117" id="Seg_2306" s="T116">n</ta>
            <ta e="T118" id="Seg_2307" s="T117">adj</ta>
            <ta e="T119" id="Seg_2308" s="T118">v</ta>
            <ta e="T120" id="Seg_2309" s="T119">adv</ta>
            <ta e="T121" id="Seg_2310" s="T120">adv</ta>
            <ta e="T122" id="Seg_2311" s="T121">adv</ta>
            <ta e="T123" id="Seg_2312" s="T122">v</ta>
            <ta e="T124" id="Seg_2313" s="T123">v</ta>
            <ta e="T125" id="Seg_2314" s="T124">n</ta>
            <ta e="T126" id="Seg_2315" s="T125">adv</ta>
            <ta e="T127" id="Seg_2316" s="T126">v</ta>
            <ta e="T128" id="Seg_2317" s="T127">num</ta>
            <ta e="T129" id="Seg_2318" s="T128">adj</ta>
            <ta e="T130" id="Seg_2319" s="T129">v</ta>
            <ta e="T131" id="Seg_2320" s="T130">dem</ta>
            <ta e="T132" id="Seg_2321" s="T131">n</ta>
            <ta e="T133" id="Seg_2322" s="T132">v</ta>
            <ta e="T134" id="Seg_2323" s="T133">pers</ta>
            <ta e="T135" id="Seg_2324" s="T134">conj</ta>
            <ta e="T136" id="Seg_2325" s="T135">adv</ta>
            <ta e="T137" id="Seg_2326" s="T136">v</ta>
            <ta e="T138" id="Seg_2327" s="T137">n</ta>
            <ta e="T139" id="Seg_2328" s="T138">n</ta>
            <ta e="T140" id="Seg_2329" s="T139">pers</ta>
            <ta e="T141" id="Seg_2330" s="T140">v</ta>
            <ta e="T142" id="Seg_2331" s="T141">pro</ta>
            <ta e="T143" id="Seg_2332" s="T142">v</ta>
            <ta e="T144" id="Seg_2333" s="T143">adj</ta>
            <ta e="T145" id="Seg_2334" s="T144">pers</ta>
            <ta e="T146" id="Seg_2335" s="T145">dem</ta>
            <ta e="T147" id="Seg_2336" s="T146">v</ta>
            <ta e="T148" id="Seg_2337" s="T147">n</ta>
            <ta e="T149" id="Seg_2338" s="T148">v</ta>
            <ta e="T150" id="Seg_2339" s="T149">adv</ta>
            <ta e="T151" id="Seg_2340" s="T150">n</ta>
            <ta e="T152" id="Seg_2341" s="T151">v</ta>
            <ta e="T153" id="Seg_2342" s="T152">n</ta>
            <ta e="T154" id="Seg_2343" s="T153">n</ta>
            <ta e="T155" id="Seg_2344" s="T154">ptcp</ta>
            <ta e="T156" id="Seg_2345" s="T155">n</ta>
            <ta e="T157" id="Seg_2346" s="T156">n</ta>
            <ta e="T158" id="Seg_2347" s="T157">v</ta>
            <ta e="T159" id="Seg_2348" s="T158">adj</ta>
            <ta e="T160" id="Seg_2349" s="T159">v</ta>
            <ta e="T161" id="Seg_2350" s="T160">interrog</ta>
            <ta e="T162" id="Seg_2351" s="T161">v</ta>
            <ta e="T163" id="Seg_2352" s="T162">pers</ta>
            <ta e="T164" id="Seg_2353" s="T163">pers</ta>
            <ta e="T165" id="Seg_2354" s="T164">v</ta>
            <ta e="T166" id="Seg_2355" s="T165">pro</ta>
            <ta e="T167" id="Seg_2356" s="T166">n</ta>
            <ta e="T168" id="Seg_2357" s="T167">n</ta>
            <ta e="T169" id="Seg_2358" s="T168">v</ta>
            <ta e="T170" id="Seg_2359" s="T169">v</ta>
            <ta e="T171" id="Seg_2360" s="T170">n</ta>
            <ta e="T172" id="Seg_2361" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_2362" s="T172">ptcp</ta>
            <ta e="T174" id="Seg_2363" s="T173">n</ta>
            <ta e="T175" id="Seg_2364" s="T174">n</ta>
            <ta e="T176" id="Seg_2365" s="T175">v</ta>
            <ta e="T177" id="Seg_2366" s="T176">n</ta>
            <ta e="T178" id="Seg_2367" s="T177">n</ta>
            <ta e="T179" id="Seg_2368" s="T178">n</ta>
            <ta e="T180" id="Seg_2369" s="T179">adv</ta>
            <ta e="T181" id="Seg_2370" s="T180">adj</ta>
            <ta e="T182" id="Seg_2371" s="T181">conj</ta>
            <ta e="T183" id="Seg_2372" s="T182">n</ta>
            <ta e="T184" id="Seg_2373" s="T183">adj</ta>
            <ta e="T185" id="Seg_2374" s="T184">v</ta>
            <ta e="T186" id="Seg_2375" s="T185">adv</ta>
            <ta e="T187" id="Seg_2376" s="T186">conj</ta>
            <ta e="T188" id="Seg_2377" s="T187">v</ta>
            <ta e="T189" id="Seg_2378" s="T188">conj</ta>
            <ta e="T190" id="Seg_2379" s="T189">pers</ta>
            <ta e="T191" id="Seg_2380" s="T190">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2381" s="T1">adv:Time</ta>
            <ta e="T3" id="Seg_2382" s="T2">np:Th</ta>
            <ta e="T8" id="Seg_2383" s="T7">np.h:A</ta>
            <ta e="T10" id="Seg_2384" s="T9">adv:Time</ta>
            <ta e="T12" id="Seg_2385" s="T11">np.h:A</ta>
            <ta e="T15" id="Seg_2386" s="T14">pro:Th</ta>
            <ta e="T17" id="Seg_2387" s="T16">np.h:Th</ta>
            <ta e="T18" id="Seg_2388" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_2389" s="T18">np:G</ta>
            <ta e="T21" id="Seg_2390" s="T20">np.h:Th</ta>
            <ta e="T22" id="Seg_2391" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_2392" s="T22">np:G</ta>
            <ta e="T25" id="Seg_2393" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_2394" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_2395" s="T27">np:Th</ta>
            <ta e="T30" id="Seg_2396" s="T29">np:Poss</ta>
            <ta e="T31" id="Seg_2397" s="T30">np:So</ta>
            <ta e="T33" id="Seg_2398" s="T32">np:Th</ta>
            <ta e="T35" id="Seg_2399" s="T34">np:Th</ta>
            <ta e="T38" id="Seg_2400" s="T37">np:So</ta>
            <ta e="T39" id="Seg_2401" s="T38">np:Poss</ta>
            <ta e="T40" id="Seg_2402" s="T39">np:Th</ta>
            <ta e="T44" id="Seg_2403" s="T43">np:P</ta>
            <ta e="T45" id="Seg_2404" s="T44">0.3.h:A</ta>
            <ta e="T48" id="Seg_2405" s="T47">pro.h:P</ta>
            <ta e="T50" id="Seg_2406" s="T49">pro.h:Poss</ta>
            <ta e="T51" id="Seg_2407" s="T50">np:Th 0.3.h:Poss</ta>
            <ta e="T57" id="Seg_2408" s="T56">0.3.h:A 0.3:P</ta>
            <ta e="T61" id="Seg_2409" s="T60">pro.h:E</ta>
            <ta e="T66" id="Seg_2410" s="T65">0.3.h:Th</ta>
            <ta e="T71" id="Seg_2411" s="T70">0.3.h:E</ta>
            <ta e="T72" id="Seg_2412" s="T71">v:Th</ta>
            <ta e="T73" id="Seg_2413" s="T72">0.3.h:E</ta>
            <ta e="T75" id="Seg_2414" s="T74">v:Th</ta>
            <ta e="T76" id="Seg_2415" s="T75">pro.h:E</ta>
            <ta e="T78" id="Seg_2416" s="T77">pro.h:P</ta>
            <ta e="T80" id="Seg_2417" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_2418" s="T80">np:G</ta>
            <ta e="T82" id="Seg_2419" s="T81">0.3.h:A</ta>
            <ta e="T86" id="Seg_2420" s="T85">0.3.h:E</ta>
            <ta e="T88" id="Seg_2421" s="T87">adv:Time</ta>
            <ta e="T89" id="Seg_2422" s="T88">0.3.h:Th</ta>
            <ta e="T91" id="Seg_2423" s="T90">adv:Time</ta>
            <ta e="T92" id="Seg_2424" s="T91">0.3.h:E</ta>
            <ta e="T94" id="Seg_2425" s="T93">adv:Time</ta>
            <ta e="T96" id="Seg_2426" s="T95">pro:Th</ta>
            <ta e="T98" id="Seg_2427" s="T97">np:Poss</ta>
            <ta e="T99" id="Seg_2428" s="T98">np:L</ta>
            <ta e="T101" id="Seg_2429" s="T100">np.h:A</ta>
            <ta e="T103" id="Seg_2430" s="T102">0.3.h:A</ta>
            <ta e="T108" id="Seg_2431" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_2432" s="T108">np.h:A</ta>
            <ta e="T111" id="Seg_2433" s="T110">np.h:Th</ta>
            <ta e="T114" id="Seg_2434" s="T113">np:G</ta>
            <ta e="T116" id="Seg_2435" s="T115">pro.h:A</ta>
            <ta e="T119" id="Seg_2436" s="T118">0.1.h:A</ta>
            <ta e="T121" id="Seg_2437" s="T120">adv:Time</ta>
            <ta e="T123" id="Seg_2438" s="T122">0.3:Th</ta>
            <ta e="T125" id="Seg_2439" s="T124">np:G</ta>
            <ta e="T126" id="Seg_2440" s="T125">adv:Time</ta>
            <ta e="T127" id="Seg_2441" s="T126">0.1.h:A</ta>
            <ta e="T129" id="Seg_2442" s="T128">np.h:G</ta>
            <ta e="T132" id="Seg_2443" s="T131">np.h:A</ta>
            <ta e="T134" id="Seg_2444" s="T133">pro.h:R</ta>
            <ta e="T138" id="Seg_2445" s="T137">np:G</ta>
            <ta e="T139" id="Seg_2446" s="T138">np:L</ta>
            <ta e="T140" id="Seg_2447" s="T139">pro.h:R</ta>
            <ta e="T141" id="Seg_2448" s="T140">0.3.h:E</ta>
            <ta e="T142" id="Seg_2449" s="T141">pro:Th</ta>
            <ta e="T143" id="Seg_2450" s="T142">v:Th</ta>
            <ta e="T145" id="Seg_2451" s="T144">pro.h:A</ta>
            <ta e="T148" id="Seg_2452" s="T147">np:P</ta>
            <ta e="T151" id="Seg_2453" s="T150">np:Th</ta>
            <ta e="T152" id="Seg_2454" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_2455" s="T152">np:P</ta>
            <ta e="T154" id="Seg_2456" s="T153">np:P</ta>
            <ta e="T156" id="Seg_2457" s="T155">np:P</ta>
            <ta e="T157" id="Seg_2458" s="T156">np:P</ta>
            <ta e="T158" id="Seg_2459" s="T157">0.3.h:A</ta>
            <ta e="T160" id="Seg_2460" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_2461" s="T160">pro.h:A</ta>
            <ta e="T163" id="Seg_2462" s="T162">pro.h:G</ta>
            <ta e="T164" id="Seg_2463" s="T163">pro.h:G</ta>
            <ta e="T167" id="Seg_2464" s="T166">np.h:A</ta>
            <ta e="T168" id="Seg_2465" s="T167">np.h:A</ta>
            <ta e="T171" id="Seg_2466" s="T170">np.h:Poss 0.3.h:Poss</ta>
            <ta e="T175" id="Seg_2467" s="T174">np.h:A</ta>
            <ta e="T178" id="Seg_2468" s="T177">np:Poss</ta>
            <ta e="T179" id="Seg_2469" s="T178">np:L</ta>
            <ta e="T183" id="Seg_2470" s="T182">np:Th 0.3.h:Poss</ta>
            <ta e="T185" id="Seg_2471" s="T184">0.1.h:P</ta>
            <ta e="T188" id="Seg_2472" s="T187">0.1.h:E</ta>
            <ta e="T190" id="Seg_2473" s="T189">pro.h:Th</ta>
            <ta e="T191" id="Seg_2474" s="T190">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_2475" s="T2">np:S</ta>
            <ta e="T8" id="Seg_2476" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_2477" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_2478" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_2479" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_2480" s="T13">s:rel</ta>
            <ta e="T17" id="Seg_2481" s="T16">np.h:O</ta>
            <ta e="T18" id="Seg_2482" s="T17">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_2483" s="T20">np.h:O</ta>
            <ta e="T22" id="Seg_2484" s="T21">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_2485" s="T24">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_2486" s="T25">s:purp</ta>
            <ta e="T28" id="Seg_2487" s="T27">np:S</ta>
            <ta e="T29" id="Seg_2488" s="T28">v:pred</ta>
            <ta e="T33" id="Seg_2489" s="T32">np:S</ta>
            <ta e="T35" id="Seg_2490" s="T34">np:S</ta>
            <ta e="T36" id="Seg_2491" s="T35">v:pred</ta>
            <ta e="T40" id="Seg_2492" s="T39">np:S</ta>
            <ta e="T41" id="Seg_2493" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_2494" s="T43">np:O</ta>
            <ta e="T45" id="Seg_2495" s="T44">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_2496" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_2497" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_2498" s="T50">np:S</ta>
            <ta e="T54" id="Seg_2499" s="T53">v:pred</ta>
            <ta e="T57" id="Seg_2500" s="T56">0.3.h:S v:pred 0.3:O</ta>
            <ta e="T61" id="Seg_2501" s="T60">pro.h:S</ta>
            <ta e="T64" id="Seg_2502" s="T63">v:pred</ta>
            <ta e="T66" id="Seg_2503" s="T65">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_2504" s="T66">s:purp</ta>
            <ta e="T71" id="Seg_2505" s="T70">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_2506" s="T71">v:O</ta>
            <ta e="T73" id="Seg_2507" s="T72">0.3.h:S v:pred</ta>
            <ta e="T75" id="Seg_2508" s="T74">v:O</ta>
            <ta e="T76" id="Seg_2509" s="T75">pro.h:S</ta>
            <ta e="T77" id="Seg_2510" s="T76">v:pred</ta>
            <ta e="T79" id="Seg_2511" s="T77">s:compl</ta>
            <ta e="T80" id="Seg_2512" s="T79">np:O</ta>
            <ta e="T82" id="Seg_2513" s="T81">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_2514" s="T85">0.3.h:S v:pred</ta>
            <ta e="T89" id="Seg_2515" s="T86">s:compl</ta>
            <ta e="T92" id="Seg_2516" s="T91">0.3.h:S v:pred</ta>
            <ta e="T95" id="Seg_2517" s="T92">s:compl</ta>
            <ta e="T97" id="Seg_2518" s="T95">s:compl</ta>
            <ta e="T100" id="Seg_2519" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_2520" s="T100">np.h:S</ta>
            <ta e="T103" id="Seg_2521" s="T102">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_2522" s="T107">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_2523" s="T108">np.h:S</ta>
            <ta e="T110" id="Seg_2524" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_2525" s="T110">np.h:S</ta>
            <ta e="T112" id="Seg_2526" s="T111">s:temp</ta>
            <ta e="T113" id="Seg_2527" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_2528" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_2529" s="T115">pro.h:S</ta>
            <ta e="T119" id="Seg_2530" s="T118">0.1.h:S v:pred</ta>
            <ta e="T122" id="Seg_2531" s="T121">adj:pred</ta>
            <ta e="T123" id="Seg_2532" s="T122">0.3:S cop</ta>
            <ta e="T127" id="Seg_2533" s="T126">0.1.h:S v:pred</ta>
            <ta e="T130" id="Seg_2534" s="T129">s:purp</ta>
            <ta e="T132" id="Seg_2535" s="T131">np.h:S</ta>
            <ta e="T133" id="Seg_2536" s="T132">v:pred</ta>
            <ta e="T138" id="Seg_2537" s="T134">s:compl</ta>
            <ta e="T141" id="Seg_2538" s="T140">0.3.h:S v:pred</ta>
            <ta e="T143" id="Seg_2539" s="T142">v:O</ta>
            <ta e="T145" id="Seg_2540" s="T144">pro.h:S</ta>
            <ta e="T147" id="Seg_2541" s="T146">v:pred</ta>
            <ta e="T148" id="Seg_2542" s="T147">np:O</ta>
            <ta e="T149" id="Seg_2543" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_2544" s="T150">np:S</ta>
            <ta e="T152" id="Seg_2545" s="T151">0.3.h:S v:pred</ta>
            <ta e="T153" id="Seg_2546" s="T152">np:O</ta>
            <ta e="T154" id="Seg_2547" s="T153">np:O</ta>
            <ta e="T156" id="Seg_2548" s="T155">np:O</ta>
            <ta e="T157" id="Seg_2549" s="T156">np:O</ta>
            <ta e="T158" id="Seg_2550" s="T157">0.3.h:S v:pred</ta>
            <ta e="T160" id="Seg_2551" s="T159">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_2552" s="T160">pro.h:S</ta>
            <ta e="T162" id="Seg_2553" s="T161">v:pred</ta>
            <ta e="T165" id="Seg_2554" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_2555" s="T166">np.h:S</ta>
            <ta e="T168" id="Seg_2556" s="T167">np.h:S</ta>
            <ta e="T169" id="Seg_2557" s="T168">v:pred</ta>
            <ta e="T175" id="Seg_2558" s="T174">np.h:S</ta>
            <ta e="T176" id="Seg_2559" s="T175">v:pred</ta>
            <ta e="T181" id="Seg_2560" s="T176">s:adv</ta>
            <ta e="T183" id="Seg_2561" s="T182">np:S</ta>
            <ta e="T184" id="Seg_2562" s="T183">adj:pred</ta>
            <ta e="T185" id="Seg_2563" s="T184">0.1.h:S v:pred</ta>
            <ta e="T188" id="Seg_2564" s="T187">0.1.h:S v:pred</ta>
            <ta e="T191" id="Seg_2565" s="T188">s:compl</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_2566" s="T2">RUS:cult</ta>
            <ta e="T4" id="Seg_2567" s="T3">RUS:cult</ta>
            <ta e="T6" id="Seg_2568" s="T5">RUS:cult</ta>
            <ta e="T20" id="Seg_2569" s="T19">RUS:gram</ta>
            <ta e="T26" id="Seg_2570" s="T25">RUS:cult</ta>
            <ta e="T46" id="Seg_2571" s="T45">RUS:core</ta>
            <ta e="T55" id="Seg_2572" s="T54">RUS:gram</ta>
            <ta e="T67" id="Seg_2573" s="T66">RUS:gram</ta>
            <ta e="T78" id="Seg_2574" s="T77">RUS:core</ta>
            <ta e="T90" id="Seg_2575" s="T89">RUS:gram</ta>
            <ta e="T91" id="Seg_2576" s="T90">RUS:core</ta>
            <ta e="T94" id="Seg_2577" s="T93">RUS:core</ta>
            <ta e="T98" id="Seg_2578" s="T97">RUS:cult</ta>
            <ta e="T118" id="Seg_2579" s="T117">RUS:cult</ta>
            <ta e="T121" id="Seg_2580" s="T120">RUS:core</ta>
            <ta e="T135" id="Seg_2581" s="T134">RUS:gram</ta>
            <ta e="T142" id="Seg_2582" s="T141">RUS:disc</ta>
            <ta e="T166" id="Seg_2583" s="T165">RUS:gram</ta>
            <ta e="T182" id="Seg_2584" s="T181">RUS:gram</ta>
            <ta e="T184" id="Seg_2585" s="T183">RUS:core</ta>
            <ta e="T187" id="Seg_2586" s="T186">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_2587" s="T1">Today is Easter, the first day, Easter day.</ta>
            <ta e="T11" id="Seg_2588" s="T7">Alyona got up earlier than other people.</ta>
            <ta e="T16" id="Seg_2589" s="T11">Alyona looked around, what where is.</ta>
            <ta e="T23" id="Seg_2590" s="T16">She let the boys and the girls [go] to the church.</ta>
            <ta e="T27" id="Seg_2591" s="T23">She went round the garden.</ta>
            <ta e="T31" id="Seg_2592" s="T27">The sun appeared from behind the forest.</ta>
            <ta e="T36" id="Seg_2593" s="T31">(There is no morning or evening dawn.)</ta>
            <ta e="T41" id="Seg_2594" s="T36">The bell was heard from the ancient church.</ta>
            <ta e="T45" id="Seg_2595" s="T41">The bell was ringing in the ancient church.</ta>
            <ta e="T47" id="Seg_2596" s="T45">Everything was quiet.</ta>
            <ta e="T49" id="Seg_2597" s="T47">My back began hurting.</ta>
            <ta e="T57" id="Seg_2598" s="T49">Her heart (Alyona's heart) shuddered, as if it was slashed with a knife.</ta>
            <ta e="T60" id="Seg_2599" s="T57">Why should she be glad.</ta>
            <ta e="T64" id="Seg_2600" s="T60">She never feels joy.</ta>
            <ta e="T69" id="Seg_2601" s="T64">She lived only not to die.</ta>
            <ta e="T72" id="Seg_2602" s="T69">She didn't want to die.</ta>
            <ta e="T75" id="Seg_2603" s="T72">She wanted to live further.</ta>
            <ta e="T79" id="Seg_2604" s="T75">She thought, everything was dead.</ta>
            <ta e="T83" id="Seg_2605" s="T79">She seized her forehead with her hands, she was tired.</ta>
            <ta e="T89" id="Seg_2606" s="T83">She remembered the life earlier, how they lived earlier.</ta>
            <ta e="T97" id="Seg_2607" s="T89">And now she thought how to live further, what to do.</ta>
            <ta e="T103" id="Seg_2608" s="T97">An old woman passed by the garden, she was speaking unclearly(?).</ta>
            <ta e="T108" id="Seg_2609" s="T103">She was talking about people as if she were mad.</ta>
            <ta e="T110" id="Seg_2610" s="T108">The old woman stopped.</ta>
            <ta e="T113" id="Seg_2611" s="T110">She was walking [and then she] stopped.</ta>
            <ta e="T117" id="Seg_2612" s="T113">“Aren't you going to the church, girl?</ta>
            <ta e="T120" id="Seg_2613" s="T117">I'll go to the church a little later.</ta>
            <ta e="T125" id="Seg_2614" s="T120">Now it's too early to go to the church.</ta>
            <ta e="T130" id="Seg_2615" s="T125">Yesterday I visited one woman to sit with her.</ta>
            <ta e="T138" id="Seg_2616" s="T130">This woman told me to go to the church.</ta>
            <ta e="T144" id="Seg_2617" s="T138">They want to say you something good in the church.</ta>
            <ta e="T148" id="Seg_2618" s="T144">They prepared lots of food.</ta>
            <ta e="T151" id="Seg_2619" s="T148">The food smells far.</ta>
            <ta e="T160" id="Seg_2620" s="T151">They cooked meat, fish, dried fish, they made bread, baked it well.</ta>
            <ta e="T163" id="Seg_2621" s="T160">Who came to you?</ta>
            <ta e="T167" id="Seg_2622" s="T163">Someone came to them.</ta>
            <ta e="T184" id="Seg_2623" s="T167">A sister came, an unmarried young man of the passed away elder sister came, he came on a horse, sitting on a good horse, and his cap was grey.</ta>
            <ta e="T191" id="Seg_2624" s="T184">I grew old and I forgot, what's his name.”</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_2625" s="T1">Heute ist Ostern, der erste Tag, Ostertag.</ta>
            <ta e="T11" id="Seg_2626" s="T7">Aljona stand früher als andere Leute auf.</ta>
            <ta e="T16" id="Seg_2627" s="T11">Aljona schaute, was wo ist.</ta>
            <ta e="T23" id="Seg_2628" s="T16">Sie ließ die Jungen und die Mädchen zur Kirche gehen.</ta>
            <ta e="T27" id="Seg_2629" s="T23">Sie ging im Garten herum.</ta>
            <ta e="T31" id="Seg_2630" s="T27">Die Sonne erschien hinter dem Wald.</ta>
            <ta e="T36" id="Seg_2631" s="T31">(Es gibt keine Morgen- oder Abenddämmerung.)</ta>
            <ta e="T41" id="Seg_2632" s="T36">Man hörte die Glocke der alten Kirche.</ta>
            <ta e="T45" id="Seg_2633" s="T41">Die Glocke in der alten Kirche läutete.</ta>
            <ta e="T47" id="Seg_2634" s="T45">Alles war still.</ta>
            <ta e="T49" id="Seg_2635" s="T47">Mein Rücken begann zu schmerzen.</ta>
            <ta e="T57" id="Seg_2636" s="T49">Ihr Herz (Aljonas Herz) schauderte, als ob es mit einem Messer zerschnitten worden wäre.</ta>
            <ta e="T60" id="Seg_2637" s="T57">Warum sollte sie froh sein.</ta>
            <ta e="T64" id="Seg_2638" s="T60">Sie ist niemals froh.</ta>
            <ta e="T69" id="Seg_2639" s="T64">Sie lebte nur um nicht zu sterben.</ta>
            <ta e="T72" id="Seg_2640" s="T69">Sie wollte nicht sterben.</ta>
            <ta e="T75" id="Seg_2641" s="T72">Sie wollte weiterleben.</ta>
            <ta e="T79" id="Seg_2642" s="T75">Sie dachte, alles wäre tot.</ta>
            <ta e="T83" id="Seg_2643" s="T79">Sie legte ihre Hände an ihre Stirn, sie war müde.</ta>
            <ta e="T89" id="Seg_2644" s="T83">Sie erinnerte sich an das Leben früher, wie sie früher gelebt hatten.</ta>
            <ta e="T97" id="Seg_2645" s="T89">Und jetzt dachte sie, wie weiterleben, was tun.</ta>
            <ta e="T103" id="Seg_2646" s="T97">Eine alte Frau lief am Garten vorbei, sie sprach undeutlich(?).</ta>
            <ta e="T108" id="Seg_2647" s="T103">Sie sprach über Leute, als wäre sie verrückt.</ta>
            <ta e="T110" id="Seg_2648" s="T108">Die alte Frau hielt an.</ta>
            <ta e="T113" id="Seg_2649" s="T110">Sie ging [und dann] hielt sie an.</ta>
            <ta e="T117" id="Seg_2650" s="T113">"Gehst du nicht in die Kirche, Mädchen?</ta>
            <ta e="T120" id="Seg_2651" s="T117">Ich gehe gleich in die Kirche.</ta>
            <ta e="T125" id="Seg_2652" s="T120">Jetzt ist es zu früh, um in die Kirche zu gehen.</ta>
            <ta e="T130" id="Seg_2653" s="T125">Gestern habe ich eine Frau besucht, um bei ihr zu sitzen.</ta>
            <ta e="T138" id="Seg_2654" s="T130">Diese Frau trug mir auf, in die Kirche zu gehen.</ta>
            <ta e="T144" id="Seg_2655" s="T138">In der Kirche wollen sie dir etwas Gutes sagen.</ta>
            <ta e="T148" id="Seg_2656" s="T144">Sie haben viel Essen vorbereitet.</ta>
            <ta e="T151" id="Seg_2657" s="T148">Man riecht das Essen von Weitem.</ta>
            <ta e="T160" id="Seg_2658" s="T151">Sie haben Fleisch gekocht, Fisch, getrockneten Fisch, sie haben Brot gemacht, haben es gut gebacken.</ta>
            <ta e="T163" id="Seg_2659" s="T160">Wer ist zu dir gekommen?</ta>
            <ta e="T167" id="Seg_2660" s="T163">Jemand ist zu ihnen gekommen.</ta>
            <ta e="T184" id="Seg_2661" s="T167">Eine Schwester kam, ein unverheirateter junger Mann der verstorbenen älteren Schwester kam, er kam auf einem Pferd, auf einem guten Pferd sitzend, und seine Mütze war grau.</ta>
            <ta e="T191" id="Seg_2662" s="T184">Ich bin alt geworden und habe vergessen, wie sein Name ist."</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_2663" s="T1">Сегодня Пасха, первый день, пасхальный день.</ta>
            <ta e="T11" id="Seg_2664" s="T7">Алена встала раньше людей.</ta>
            <ta e="T16" id="Seg_2665" s="T11">Алена посмотрела, где что есть.</ta>
            <ta e="T23" id="Seg_2666" s="T16">Парней отпустила в церковь и девушек отпустила в церковь.</ta>
            <ta e="T27" id="Seg_2667" s="T23">Сама пошла сад обойти.</ta>
            <ta e="T31" id="Seg_2668" s="T27">Солнце показалось из-за леса.</ta>
            <ta e="T36" id="Seg_2669" s="T31">Вечерней зари, утренней зари нет.</ta>
            <ta e="T41" id="Seg_2670" s="T36">От старинной церкви слышался колокол.</ta>
            <ta e="T45" id="Seg_2671" s="T41">В старинной церкви в колокол били.</ta>
            <ta e="T47" id="Seg_2672" s="T45">Всё тихо было.</ta>
            <ta e="T49" id="Seg_2673" s="T47">У меня спина заболела (=я без спины стала).</ta>
            <ta e="T57" id="Seg_2674" s="T49">Сердце ее (сердце Алены) дрогнуло, как будто его полоснули ножом.</ta>
            <ta e="T60" id="Seg_2675" s="T57">Чего ей радоваться.</ta>
            <ta e="T64" id="Seg_2676" s="T60">Она никогда не веселится.</ta>
            <ta e="T69" id="Seg_2677" s="T64">Затем и жила, чтобы не помереть.</ta>
            <ta e="T72" id="Seg_2678" s="T69">Не хотела умереть.</ta>
            <ta e="T75" id="Seg_2679" s="T72">Хотела дольше жить.</ta>
            <ta e="T79" id="Seg_2680" s="T75">Она думала, все мертвое.</ta>
            <ta e="T83" id="Seg_2681" s="T79">Руками за лоб схватилась, уставшая.</ta>
            <ta e="T89" id="Seg_2682" s="T83">Давнюю жизнь вспомнила, как раньше жили.</ta>
            <ta e="T97" id="Seg_2683" s="T89">А теперь думает, как теперь жить, что делать.</ta>
            <ta e="T103" id="Seg_2684" s="T97">Мимо сада прошла старуха, по-беспутному(?) разговаривает.</ta>
            <ta e="T108" id="Seg_2685" s="T103">Как сумасшедшая болтает про людей.</ta>
            <ta e="T110" id="Seg_2686" s="T108">Старуха остановилась.</ta>
            <ta e="T113" id="Seg_2687" s="T110">Старуха, идя, остановилась.</ta>
            <ta e="T117" id="Seg_2688" s="T113">“Ты, девчонка, не в церковь ли идешь?</ta>
            <ta e="T120" id="Seg_2689" s="T117">Попозже схожу в церковь.</ta>
            <ta e="T125" id="Seg_2690" s="T120">Сейчас рано идти в церковь.</ta>
            <ta e="T130" id="Seg_2691" s="T125">Вчера я заходила к одной женщине посидеть.</ta>
            <ta e="T138" id="Seg_2692" s="T130">Эта женщина сказала мне в церковь сходить, в церковь.</ta>
            <ta e="T144" id="Seg_2693" s="T138">В церкви тебе хотят что-то хорошее сказать.</ta>
            <ta e="T148" id="Seg_2694" s="T144">Они столько сварили еды.</ta>
            <ta e="T151" id="Seg_2695" s="T148">Пахнет далеко пищей.</ta>
            <ta e="T160" id="Seg_2696" s="T151">Наварили мяса, рыбы, сушеная рыба, хлеб сделали, хорошо испекли.</ta>
            <ta e="T163" id="Seg_2697" s="T160">Кто к вам приехал?</ta>
            <ta e="T167" id="Seg_2698" s="T163">К ним кто-то приехал.</ta>
            <ta e="T184" id="Seg_2699" s="T167">Сестра приехала, умершей старшей сестры неженатый молодой парень приехал, на коне, на коне верхом сидя, на хорошем, а шапка серая.</ta>
            <ta e="T191" id="Seg_2700" s="T184">Я сильно состарилась и забываю, как его зовут”.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_2701" s="T1">сегодня Пасха</ta>
            <ta e="T11" id="Seg_2702" s="T7">Алена встала раньше людей</ta>
            <ta e="T16" id="Seg_2703" s="T11">Алена посмотрела где что есть</ta>
            <ta e="T23" id="Seg_2704" s="T16">парней отпустила в церковь и девушек отпустила в церковь</ta>
            <ta e="T27" id="Seg_2705" s="T23">сама пошла сад обойти</ta>
            <ta e="T31" id="Seg_2706" s="T27">солнце показалось из-за леса</ta>
            <ta e="T36" id="Seg_2707" s="T31">вечерней зари нет утренней зари нет</ta>
            <ta e="T41" id="Seg_2708" s="T36">от старинной слышался колокол</ta>
            <ta e="T45" id="Seg_2709" s="T41">в колокол били</ta>
            <ta e="T47" id="Seg_2710" s="T45">тихо было</ta>
            <ta e="T49" id="Seg_2711" s="T47">я обеспинила (спина заболела) непонятно, относится ли это к тексту, или это отдельное предложение</ta>
            <ta e="T57" id="Seg_2712" s="T49">сердце ее (сердце Алены) дрогнуло как будто его полоснули ножом</ta>
            <ta e="T60" id="Seg_2713" s="T57">чего ей радоваться</ta>
            <ta e="T64" id="Seg_2714" s="T60">она никогда не веселится</ta>
            <ta e="T69" id="Seg_2715" s="T64">затем и жила чтоб не помереть</ta>
            <ta e="T72" id="Seg_2716" s="T69">не хотела умереть</ta>
            <ta e="T75" id="Seg_2717" s="T72">хотела дольше жить</ta>
            <ta e="T79" id="Seg_2718" s="T75">она думала все мертвое</ta>
            <ta e="T83" id="Seg_2719" s="T79">руками лоб (за лоб) схватила (поймалась) уставши</ta>
            <ta e="T89" id="Seg_2720" s="T83">ранешнюю жизнь вспомнила как раньше жили</ta>
            <ta e="T97" id="Seg_2721" s="T89">а теперь думает как теперь жить чего делать</ta>
            <ta e="T103" id="Seg_2722" s="T97">мимо сада прошла старуха по беспутному разговаривает</ta>
            <ta e="T108" id="Seg_2723" s="T103">как сумасшедшая болтает про людей без пути</ta>
            <ta e="T110" id="Seg_2724" s="T108">ста(руха)</ta>
            <ta e="T113" id="Seg_2725" s="T110">старуха идя остановилась</ta>
            <ta e="T117" id="Seg_2726" s="T113">ты девчонка не в церковь ли идешь</ta>
            <ta e="T120" id="Seg_2727" s="T117">попозже схожу в церковь</ta>
            <ta e="T125" id="Seg_2728" s="T120">сейчас рано идти в церковь</ta>
            <ta e="T130" id="Seg_2729" s="T125">вчера заходила к одной женщине посидеть</ta>
            <ta e="T138" id="Seg_2730" s="T130">эта женщина говорит мне что ты в церковь сходи</ta>
            <ta e="T144" id="Seg_2731" s="T138">в церкви тебе хотят что-то хорошее сказать</ta>
            <ta e="T148" id="Seg_2732" s="T144">они столько сварили есть</ta>
            <ta e="T151" id="Seg_2733" s="T148">пахнет далеко пищей</ta>
            <ta e="T160" id="Seg_2734" s="T151">наварили мяса рыбы сушеная рыба хлеб хорошо спекли</ta>
            <ta e="T163" id="Seg_2735" s="T160">кто к вам приехал</ta>
            <ta e="T167" id="Seg_2736" s="T163">к ним кто-то приехал</ta>
            <ta e="T184" id="Seg_2737" s="T167">сестра приехала умершей старшей сестры не женатый молодой (лет 15-16) парень приехал на коне верхом сидя на хорошем а шапка серая</ta>
            <ta e="T191" id="Seg_2738" s="T184">старуха шибко старая стала и позабываю как его звать</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T23" id="Seg_2739" s="T16">[BrM:] 'kɨba čʼelam' changed to 'kɨbačʼelam'.</ta>
            <ta e="T31" id="Seg_2740" s="T27">[KuAI:] Variant: 'qončdimban'.</ta>
            <ta e="T36" id="Seg_2741" s="T31">[BrM:] This sentence seems to be not a part of the text.</ta>
            <ta e="T45" id="Seg_2742" s="T41">[KuAI:] Variant: 'qwoːsat'.</ta>
            <ta e="T49" id="Seg_2743" s="T47">[BrM:] This sentence seems to be not a part of the text.</ta>
            <ta e="T57" id="Seg_2744" s="T49">[BrM:] Tentative analysis of 'pan'. [BrM:] 'kak budta' changed to 'kakbudta'.</ta>
            <ta e="T108" id="Seg_2745" s="T103">[BrM:] 'kuːndʼät' changed to 'kuːn dʼät'.</ta>
            <ta e="T120" id="Seg_2746" s="T117">[BrM:] 'pogodʼä laq' changed to 'pogodʼälaq'. [BrM:] It's unclear, who is saying this.</ta>
            <ta e="T138" id="Seg_2747" s="T130">[BrM:] Tentative analysis of 'äːʒulguŋ'.</ta>
            <ta e="T163" id="Seg_2748" s="T160">[BrM:] Tentative analysis of 'täɣɨndältə'.</ta>
            <ta e="T184" id="Seg_2749" s="T167">[BrM:] Tentative analysis of 'soːgɨndɨze'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
