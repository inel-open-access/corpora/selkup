<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>ChAE_196X_FirstRendezvous_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">ChAE_196X_FirstRendezvous_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">139</ud-information>
            <ud-information attribute-name="# HIAT:w">103</ud-information>
            <ud-information attribute-name="# e">103</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChAE">
            <abbreviation>ChAE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChAE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T104" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Zina</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qälʼdimba</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">sädidimba</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip">.</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_16" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">Qondɨlʼe</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">sädɨdespan</ts>
                  <nts id="Seg_22" n="HIAT:ip">,</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">a</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">uš</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">lakwatčan</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Qajno</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">täbnä</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">laqwačigu</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Irɨl</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">del</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">täːɣɨnt</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">patkɨlgunt</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Telʼden</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">tʼeːlat</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">eːppan</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">kak</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">iri</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">delat</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_83" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">Küːdəre</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">kudeptɨlʼdikuŋ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">ass</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">swa</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">küdəre</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">tʼäŋwa</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_104" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Küːderʼäɣɨn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">qonǯirbat</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">qwaim</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">üsse</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">əːtan</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_122" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Üt</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">qätɨmbat</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">wes</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">üsse</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">wes</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">nʼörom</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_143" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_145" n="HIAT:w" s="T39">Täp</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_148" n="HIAT:w" s="T40">soŋ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_150" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">swaŋ</ts>
                  <nts id="Seg_153" n="HIAT:ip">)</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">qondaŋ</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_160" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">Täp</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">ündedit</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">što</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">eut</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">sɨrə</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">pargɨlʼespɨndɨt</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_181" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">Sɨrlam</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">ass</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">kaniŋbatt</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">nʼört</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">apstämbɨgu</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_199" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">Tolʼkošto</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">qwändɨt</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">iːlädʼespa</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_211" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">Zina</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">wazɨmban</ts>
                  <nts id="Seg_217" n="HIAT:ip">,</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">qoptoɣɨndo</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">täː</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">paqtɨdʼemba</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">adejalam</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">täː</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">tʼäʒembat</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_240" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">Niːŋ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">swaŋ</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">aːmɨdimban</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_251" n="HIAT:w" s="T68">i</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_254" n="HIAT:w" s="T69">nʼäkɨldimban</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_258" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_260" n="HIAT:w" s="T70">Qolalgɨdi</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_263" n="HIAT:w" s="T71">qaborɣɨm</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_266" n="HIAT:w" s="T72">tʼedʼäčimbat</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_270" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_272" n="HIAT:w" s="T73">Uːdom</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">wandɨm</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_278" n="HIAT:w" s="T75">müzulʼǯugu</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_281" n="HIAT:w" s="T76">qwanba</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_285" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">Qaborɣɨmdə</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">niŋɨmbat</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_294" n="HIAT:w" s="T79">a</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_297" n="HIAT:w" s="T80">köl</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_300" n="HIAT:w" s="T81">pümmɨze</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_303" n="HIAT:w" s="T82">qwanban</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_306" n="HIAT:w" s="T83">wandɨm</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_309" n="HIAT:w" s="T84">müzulʼǯigu</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_313" n="HIAT:w" s="T85">wannɨmd</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_316" n="HIAT:w" s="T86">i</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_319" n="HIAT:w" s="T87">uːdomt</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_323" n="HIAT:u" s="T88">
                  <nts id="Seg_324" n="HIAT:ip">“</nts>
                  <ts e="T89" id="Seg_326" n="HIAT:w" s="T88">Qannämbədi</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_329" n="HIAT:w" s="T89">üt</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_332" n="HIAT:w" s="T90">wadʼöw</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_334" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_336" n="HIAT:w" s="T91">qobow</ts>
                  <nts id="Seg_337" n="HIAT:ip">)</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_340" n="HIAT:w" s="T92">kandeptit</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_344" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">Sidʼew</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">larɨmba</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_353" n="HIAT:w" s="T95">larɨtʼikuŋ</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_357" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_359" n="HIAT:w" s="T96">Sidʼeu</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_362" n="HIAT:w" s="T97">laɣolɨkkuŋ</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_366" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_368" n="HIAT:w" s="T98">Qobou</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_371" n="HIAT:w" s="T99">kandedʼikuŋ</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip">”</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_376" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_378" n="HIAT:w" s="T100">Üdom</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_381" n="HIAT:w" s="T101">qamǯelʼespest</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">uduze</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_387" n="HIAT:w" s="T103">moɣolɣɨnt</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T104" id="Seg_390" n="sc" s="T1">
               <ts e="T2" id="Seg_392" n="e" s="T1">Zina </ts>
               <ts e="T3" id="Seg_394" n="e" s="T2">qälʼdimba </ts>
               <ts e="T4" id="Seg_396" n="e" s="T3">(sädidimba). </ts>
               <ts e="T5" id="Seg_398" n="e" s="T4">Qondɨlʼe </ts>
               <ts e="T6" id="Seg_400" n="e" s="T5">sädɨdespan, </ts>
               <ts e="T7" id="Seg_402" n="e" s="T6">a </ts>
               <ts e="T8" id="Seg_404" n="e" s="T7">uš </ts>
               <ts e="T9" id="Seg_406" n="e" s="T8">lakwatčan. </ts>
               <ts e="T10" id="Seg_408" n="e" s="T9">Qajno </ts>
               <ts e="T11" id="Seg_410" n="e" s="T10">täbnä </ts>
               <ts e="T12" id="Seg_412" n="e" s="T11">laqwačigu. </ts>
               <ts e="T13" id="Seg_414" n="e" s="T12">Irɨl </ts>
               <ts e="T14" id="Seg_416" n="e" s="T13">del </ts>
               <ts e="T15" id="Seg_418" n="e" s="T14">täːɣɨnt </ts>
               <ts e="T16" id="Seg_420" n="e" s="T15">patkɨlgunt. </ts>
               <ts e="T17" id="Seg_422" n="e" s="T16">Telʼden </ts>
               <ts e="T18" id="Seg_424" n="e" s="T17">tʼeːlat </ts>
               <ts e="T19" id="Seg_426" n="e" s="T18">eːppan </ts>
               <ts e="T20" id="Seg_428" n="e" s="T19">kak </ts>
               <ts e="T21" id="Seg_430" n="e" s="T20">iri </ts>
               <ts e="T22" id="Seg_432" n="e" s="T21">delat. </ts>
               <ts e="T23" id="Seg_434" n="e" s="T22">Küːdəre </ts>
               <ts e="T24" id="Seg_436" n="e" s="T23">kudeptɨlʼdikuŋ </ts>
               <ts e="T25" id="Seg_438" n="e" s="T24">ass </ts>
               <ts e="T26" id="Seg_440" n="e" s="T25">swa </ts>
               <ts e="T27" id="Seg_442" n="e" s="T26">küdəre </ts>
               <ts e="T28" id="Seg_444" n="e" s="T27">tʼäŋwa. </ts>
               <ts e="T29" id="Seg_446" n="e" s="T28">Küːderʼäɣɨn </ts>
               <ts e="T30" id="Seg_448" n="e" s="T29">qonǯirbat </ts>
               <ts e="T31" id="Seg_450" n="e" s="T30">qwaim </ts>
               <ts e="T32" id="Seg_452" n="e" s="T31">üsse </ts>
               <ts e="T33" id="Seg_454" n="e" s="T32">əːtan. </ts>
               <ts e="T34" id="Seg_456" n="e" s="T33">Üt </ts>
               <ts e="T35" id="Seg_458" n="e" s="T34">qätɨmbat </ts>
               <ts e="T36" id="Seg_460" n="e" s="T35">wes </ts>
               <ts e="T37" id="Seg_462" n="e" s="T36">üsse </ts>
               <ts e="T38" id="Seg_464" n="e" s="T37">wes </ts>
               <ts e="T39" id="Seg_466" n="e" s="T38">nʼörom. </ts>
               <ts e="T40" id="Seg_468" n="e" s="T39">Täp </ts>
               <ts e="T41" id="Seg_470" n="e" s="T40">soŋ </ts>
               <ts e="T42" id="Seg_472" n="e" s="T41">(swaŋ) </ts>
               <ts e="T43" id="Seg_474" n="e" s="T42">qondaŋ. </ts>
               <ts e="T44" id="Seg_476" n="e" s="T43">Täp </ts>
               <ts e="T45" id="Seg_478" n="e" s="T44">ündedit </ts>
               <ts e="T46" id="Seg_480" n="e" s="T45">što </ts>
               <ts e="T47" id="Seg_482" n="e" s="T46">eut </ts>
               <ts e="T48" id="Seg_484" n="e" s="T47">sɨrə </ts>
               <ts e="T49" id="Seg_486" n="e" s="T48">pargɨlʼespɨndɨt. </ts>
               <ts e="T50" id="Seg_488" n="e" s="T49">Sɨrlam </ts>
               <ts e="T51" id="Seg_490" n="e" s="T50">ass </ts>
               <ts e="T52" id="Seg_492" n="e" s="T51">kaniŋbatt </ts>
               <ts e="T53" id="Seg_494" n="e" s="T52">nʼört </ts>
               <ts e="T54" id="Seg_496" n="e" s="T53">apstämbɨgu. </ts>
               <ts e="T55" id="Seg_498" n="e" s="T54">Tolʼkošto </ts>
               <ts e="T56" id="Seg_500" n="e" s="T55">qwändɨt </ts>
               <ts e="T57" id="Seg_502" n="e" s="T56">iːlädʼespa. </ts>
               <ts e="T58" id="Seg_504" n="e" s="T57">Zina </ts>
               <ts e="T59" id="Seg_506" n="e" s="T58">wazɨmban, </ts>
               <ts e="T60" id="Seg_508" n="e" s="T59">qoptoɣɨndo </ts>
               <ts e="T61" id="Seg_510" n="e" s="T60">täː </ts>
               <ts e="T62" id="Seg_512" n="e" s="T61">paqtɨdʼemba, </ts>
               <ts e="T63" id="Seg_514" n="e" s="T62">adejalam </ts>
               <ts e="T64" id="Seg_516" n="e" s="T63">täː </ts>
               <ts e="T65" id="Seg_518" n="e" s="T64">tʼäʒembat. </ts>
               <ts e="T66" id="Seg_520" n="e" s="T65">Niːŋ </ts>
               <ts e="T67" id="Seg_522" n="e" s="T66">swaŋ </ts>
               <ts e="T68" id="Seg_524" n="e" s="T67">aːmɨdimban </ts>
               <ts e="T69" id="Seg_526" n="e" s="T68">i </ts>
               <ts e="T70" id="Seg_528" n="e" s="T69">nʼäkɨldimban. </ts>
               <ts e="T71" id="Seg_530" n="e" s="T70">Qolalgɨdi </ts>
               <ts e="T72" id="Seg_532" n="e" s="T71">qaborɣɨm </ts>
               <ts e="T73" id="Seg_534" n="e" s="T72">tʼedʼäčimbat. </ts>
               <ts e="T74" id="Seg_536" n="e" s="T73">Uːdom </ts>
               <ts e="T75" id="Seg_538" n="e" s="T74">wandɨm </ts>
               <ts e="T76" id="Seg_540" n="e" s="T75">müzulʼǯugu </ts>
               <ts e="T77" id="Seg_542" n="e" s="T76">qwanba. </ts>
               <ts e="T78" id="Seg_544" n="e" s="T77">Qaborɣɨmdə </ts>
               <ts e="T79" id="Seg_546" n="e" s="T78">niŋɨmbat, </ts>
               <ts e="T80" id="Seg_548" n="e" s="T79">a </ts>
               <ts e="T81" id="Seg_550" n="e" s="T80">köl </ts>
               <ts e="T82" id="Seg_552" n="e" s="T81">pümmɨze </ts>
               <ts e="T83" id="Seg_554" n="e" s="T82">qwanban </ts>
               <ts e="T84" id="Seg_556" n="e" s="T83">wandɨm </ts>
               <ts e="T85" id="Seg_558" n="e" s="T84">müzulʼǯigu, </ts>
               <ts e="T86" id="Seg_560" n="e" s="T85">wannɨmd </ts>
               <ts e="T87" id="Seg_562" n="e" s="T86">i </ts>
               <ts e="T88" id="Seg_564" n="e" s="T87">uːdomt. </ts>
               <ts e="T89" id="Seg_566" n="e" s="T88">“Qannämbədi </ts>
               <ts e="T90" id="Seg_568" n="e" s="T89">üt </ts>
               <ts e="T91" id="Seg_570" n="e" s="T90">wadʼöw </ts>
               <ts e="T92" id="Seg_572" n="e" s="T91">(qobow) </ts>
               <ts e="T93" id="Seg_574" n="e" s="T92">kandeptit. </ts>
               <ts e="T94" id="Seg_576" n="e" s="T93">Sidʼew </ts>
               <ts e="T95" id="Seg_578" n="e" s="T94">larɨmba, </ts>
               <ts e="T96" id="Seg_580" n="e" s="T95">larɨtʼikuŋ. </ts>
               <ts e="T97" id="Seg_582" n="e" s="T96">Sidʼeu </ts>
               <ts e="T98" id="Seg_584" n="e" s="T97">laɣolɨkkuŋ. </ts>
               <ts e="T99" id="Seg_586" n="e" s="T98">Qobou </ts>
               <ts e="T100" id="Seg_588" n="e" s="T99">kandedʼikuŋ.” </ts>
               <ts e="T101" id="Seg_590" n="e" s="T100">Üdom </ts>
               <ts e="T102" id="Seg_592" n="e" s="T101">qamǯelʼespest </ts>
               <ts e="T103" id="Seg_594" n="e" s="T102">uduze </ts>
               <ts e="T104" id="Seg_596" n="e" s="T103">moɣolɣɨnt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_597" s="T1">ChAE_196X_FirstRendezvous_transl.001 (001.001)</ta>
            <ta e="T9" id="Seg_598" s="T4">ChAE_196X_FirstRendezvous_transl.002 (001.002)</ta>
            <ta e="T12" id="Seg_599" s="T9">ChAE_196X_FirstRendezvous_transl.003 (001.003)</ta>
            <ta e="T16" id="Seg_600" s="T12">ChAE_196X_FirstRendezvous_transl.004 (001.004)</ta>
            <ta e="T22" id="Seg_601" s="T16">ChAE_196X_FirstRendezvous_transl.005 (001.005)</ta>
            <ta e="T28" id="Seg_602" s="T22">ChAE_196X_FirstRendezvous_transl.006 (001.006)</ta>
            <ta e="T33" id="Seg_603" s="T28">ChAE_196X_FirstRendezvous_transl.007 (001.007)</ta>
            <ta e="T39" id="Seg_604" s="T33">ChAE_196X_FirstRendezvous_transl.008 (001.008)</ta>
            <ta e="T43" id="Seg_605" s="T39">ChAE_196X_FirstRendezvous_transl.009 (001.009)</ta>
            <ta e="T49" id="Seg_606" s="T43">ChAE_196X_FirstRendezvous_transl.010 (001.010)</ta>
            <ta e="T54" id="Seg_607" s="T49">ChAE_196X_FirstRendezvous_transl.011 (001.011)</ta>
            <ta e="T57" id="Seg_608" s="T54">ChAE_196X_FirstRendezvous_transl.012 (001.012)</ta>
            <ta e="T65" id="Seg_609" s="T57">ChAE_196X_FirstRendezvous_transl.013 (001.013)</ta>
            <ta e="T70" id="Seg_610" s="T65">ChAE_196X_FirstRendezvous_transl.014 (001.014)</ta>
            <ta e="T73" id="Seg_611" s="T70">ChAE_196X_FirstRendezvous_transl.015 (001.015)</ta>
            <ta e="T77" id="Seg_612" s="T73">ChAE_196X_FirstRendezvous_transl.016 (001.016)</ta>
            <ta e="T88" id="Seg_613" s="T77">ChAE_196X_FirstRendezvous_transl.017 (001.017)</ta>
            <ta e="T93" id="Seg_614" s="T88">ChAE_196X_FirstRendezvous_transl.018 (001.018)</ta>
            <ta e="T96" id="Seg_615" s="T93">ChAE_196X_FirstRendezvous_transl.019 (001.019)</ta>
            <ta e="T98" id="Seg_616" s="T96">ChAE_196X_FirstRendezvous_transl.020 (001.020)</ta>
            <ta e="T100" id="Seg_617" s="T98">ChAE_196X_FirstRendezvous_transl.021 (001.021)</ta>
            <ta e="T104" id="Seg_618" s="T100">ChAE_196X_FirstRendezvous_transl.022 (001.022)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_619" s="T1">′Зина ′kӓлʼд̂имба (′сӓдидим′ба).</ta>
            <ta e="T9" id="Seg_620" s="T4">′kондылʼе ‵сӓды′деспан, а уш ла′кваттшан.</ta>
            <ta e="T12" id="Seg_621" s="T9">kай′но тӓб′нӓ ла′kwатшигу.</ta>
            <ta e="T16" id="Seg_622" s="T12">′ирыл ′дел ′тӓ̄ɣынт(дъ) паткыл′гунт.</ta>
            <ta e="T22" id="Seg_623" s="T16">телʼ′ден ′тʼе̄лат ′е̄ппан как ′ири ′д̂елат.</ta>
            <ta e="T28" id="Seg_624" s="T22">′кӱ̄дъре ку′дептылʼдикуң асс сва ′кӱдъре ′тʼӓңва.</ta>
            <ta e="T33" id="Seg_625" s="T28">кӱ̄де′рʼӓɣын kонджирбат ′kwаим ӱс′се ъ̊̄′тан.</ta>
            <ta e="T39" id="Seg_626" s="T33">ӱт ′kӓтымбат вес ӱс′се вес нʼӧ′ром.</ta>
            <ta e="T43" id="Seg_627" s="T39">тӓп ′соң (сваң) kон′даң.</ta>
            <ta e="T49" id="Seg_628" s="T43">тӓп ӱнде′дит што ′еут ′сыръ паргы′лʼеспындыт.</ta>
            <ta e="T54" id="Seg_629" s="T49">′сырлам асс ка′ниңбатт ′нʼӧрт апстӓмбыгу.</ta>
            <ta e="T57" id="Seg_630" s="T54">толʼко што ′kwӓндыт ′ӣлӓдʼес′па.</ta>
            <ta e="T65" id="Seg_631" s="T57">Зина вазымбан, kоп′тоɣындо тӓ̄ паk‵тыдʼем′ба, аде′jалам тӓ̄ тʼӓжем′бат.</ta>
            <ta e="T70" id="Seg_632" s="T65">нӣң ′сваң ′а̄мыдимбан и ′нʼӓкыlдим′бан.</ta>
            <ta e="T73" id="Seg_633" s="T70">kо′лалгыди kа′борɣым ′тʼедʼӓтшим′бат.</ta>
            <ta e="T77" id="Seg_634" s="T73">′ӯдом ван′дым мӱзулʼджугу kwан′ба.</ta>
            <ta e="T88" id="Seg_635" s="T77">kа′борɣымдъ ниңымбат, а ′кӧл пӱммы′зе kwан′бан ван′дым мӱзулʼджиг̂у, wа′ннымд и ′ӯдомт.</ta>
            <ta e="T93" id="Seg_636" s="T88">kаннӓмбъ′ди ӱт ва′дʼӧw(у) (kо′боw(у̹) кан′дептит.</ta>
            <ta e="T96" id="Seg_637" s="T93">си′дʼеw ларым′ба, ларытʼикуң.</ta>
            <ta e="T98" id="Seg_638" s="T96">си′дʼеу̹ ла′ɣолыккуң.</ta>
            <ta e="T100" id="Seg_639" s="T98">kобоу кан′дед̂ʼикуң.</ta>
            <ta e="T104" id="Seg_640" s="T100">ӱ′дом kам′джелʼеспест уду′зе мо′ɣолɣынт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T4" id="Seg_641" s="T1">Зina qälʼd̂imba (sädidimba).</ta>
            <ta e="T9" id="Seg_642" s="T4">qondɨlʼe sädɨdespan, a uš lakwatčan.</ta>
            <ta e="T12" id="Seg_643" s="T9">qajno täbnä laqwačigu.</ta>
            <ta e="T16" id="Seg_644" s="T12">irɨl del täːɣɨnt(də) patkɨlgunt.</ta>
            <ta e="T22" id="Seg_645" s="T16">telʼden tʼeːlat eːppan kak iri d̂elat.</ta>
            <ta e="T28" id="Seg_646" s="T22">küːdəre kudeptɨlʼdikuŋ ass swa küdəre tʼäŋwa.</ta>
            <ta e="T33" id="Seg_647" s="T28">küːderʼäɣɨn qonǯirbat qwaim üsse əːtan.</ta>
            <ta e="T39" id="Seg_648" s="T33">üt qätɨmbat wes üsse wes nʼörom.</ta>
            <ta e="T43" id="Seg_649" s="T39">täp soŋ (swaŋ) qondaŋ.</ta>
            <ta e="T49" id="Seg_650" s="T43">täp ündedit što eut sɨrə pargɨlʼespɨndɨt.</ta>
            <ta e="T54" id="Seg_651" s="T49">sɨrlam ass kaniŋbatt nʼört apstämbɨgu.</ta>
            <ta e="T57" id="Seg_652" s="T54">tolʼko što qwändɨt iːlädʼespa.</ta>
            <ta e="T65" id="Seg_653" s="T57">Зina wazɨmban, qoptoɣɨndo täː paqtɨdʼemba, adejalam täː tʼäʒembat.</ta>
            <ta e="T70" id="Seg_654" s="T65">niːŋ swaŋ aːmɨdimban i nʼäkɨldimban.</ta>
            <ta e="T73" id="Seg_655" s="T70">qolalgɨdi qaborɣɨm tʼedʼäčimbat.</ta>
            <ta e="T77" id="Seg_656" s="T73">uːdom wandɨm müzulʼǯugu qwanba.</ta>
            <ta e="T88" id="Seg_657" s="T77">qaborɣɨmdə niŋɨmbat, a köl pümmɨze qwanban wandɨm müzulʼǯiĝu, wannɨmd i uːdomt.</ta>
            <ta e="T93" id="Seg_658" s="T88">qannämbədi üt wadʼöw(u) (qobow(u̹) kandeptit.</ta>
            <ta e="T96" id="Seg_659" s="T93">sidʼew larɨmba, larɨtʼikuŋ.</ta>
            <ta e="T98" id="Seg_660" s="T96">sidʼeu̹ laɣolɨkkuŋ.</ta>
            <ta e="T100" id="Seg_661" s="T98">qobou kanded̂ʼikuŋ.</ta>
            <ta e="T104" id="Seg_662" s="T100">üdom qamǯelʼespest uduze moɣolɣɨnt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_663" s="T1">Zina qälʼdimba (sädidimba). </ta>
            <ta e="T9" id="Seg_664" s="T4">Qondɨlʼe sädɨdespan, a uš lakwatčan. </ta>
            <ta e="T12" id="Seg_665" s="T9">Qajno täbnä laqwačigu. </ta>
            <ta e="T16" id="Seg_666" s="T12">Irɨl del täːɣɨnt patkɨlgunt. </ta>
            <ta e="T22" id="Seg_667" s="T16">Telʼden tʼeːlat eːppan kak iri delat. </ta>
            <ta e="T28" id="Seg_668" s="T22">Küːdəre kudeptɨlʼdikuŋ ass swa küdəre tʼäŋwa. </ta>
            <ta e="T33" id="Seg_669" s="T28">Küːderʼäɣɨn qonǯirbat qwaim üsse əːtan. </ta>
            <ta e="T39" id="Seg_670" s="T33">Üt qätɨmbat wes üsse wes nʼörom. </ta>
            <ta e="T43" id="Seg_671" s="T39">Täp soŋ (swaŋ) qondaŋ. </ta>
            <ta e="T49" id="Seg_672" s="T43">Täp ündedit što eut sɨrə pargɨlʼespɨndɨt. </ta>
            <ta e="T54" id="Seg_673" s="T49">Sɨrlam ass kaniŋbatt nʼört apstämbɨgu. </ta>
            <ta e="T57" id="Seg_674" s="T54">Tolʼkošto qwändɨt iːlädʼespa. </ta>
            <ta e="T65" id="Seg_675" s="T57">Zina wazɨmban, qoptoɣɨndo täː paqtɨdʼemba, adejalam täː tʼäʒembat. </ta>
            <ta e="T70" id="Seg_676" s="T65">Niːŋ swaŋ aːmɨdimban i nʼäkɨldimban. </ta>
            <ta e="T73" id="Seg_677" s="T70">Qolalgɨdi qaborɣɨm tʼedʼäčimbat. </ta>
            <ta e="T77" id="Seg_678" s="T73">Uːdom wandɨm müzulʼǯugu qwanba. </ta>
            <ta e="T88" id="Seg_679" s="T77">Qaborɣɨmdə niŋɨmbat, a köl pümmɨze qwanban wandɨm müzulʼǯigu, wannɨmd i uːdomt. </ta>
            <ta e="T93" id="Seg_680" s="T88">“Qannämbədi üt wadʼöw (qobow) kandeptit. </ta>
            <ta e="T96" id="Seg_681" s="T93">Sidʼew larɨmba, larɨtʼikuŋ. </ta>
            <ta e="T98" id="Seg_682" s="T96">Sidʼeu laɣolɨkkuŋ. </ta>
            <ta e="T100" id="Seg_683" s="T98">Qobou kandedʼikuŋ.” </ta>
            <ta e="T104" id="Seg_684" s="T100">Üdom qamǯelʼespest uduze moɣolɣɨnt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_685" s="T1">Zina</ta>
            <ta e="T3" id="Seg_686" s="T2">qälʼdi-mba</ta>
            <ta e="T4" id="Seg_687" s="T3">sädi-di-mba</ta>
            <ta e="T5" id="Seg_688" s="T4">qondɨ-lʼe</ta>
            <ta e="T6" id="Seg_689" s="T5">sädɨ-de-spa-n</ta>
            <ta e="T7" id="Seg_690" s="T6">a</ta>
            <ta e="T8" id="Seg_691" s="T7">uš</ta>
            <ta e="T9" id="Seg_692" s="T8">lakwat-ča-n</ta>
            <ta e="T10" id="Seg_693" s="T9">qaj-no</ta>
            <ta e="T11" id="Seg_694" s="T10">täb-nä</ta>
            <ta e="T12" id="Seg_695" s="T11">laqwa-či-gu</ta>
            <ta e="T13" id="Seg_696" s="T12">ir-ɨ-l</ta>
            <ta e="T14" id="Seg_697" s="T13">del</ta>
            <ta e="T15" id="Seg_698" s="T14">täː-ɣɨnt</ta>
            <ta e="T16" id="Seg_699" s="T15">pat-kɨl-gu-nt</ta>
            <ta e="T17" id="Seg_700" s="T16">telʼden</ta>
            <ta e="T18" id="Seg_701" s="T17">tʼeːl-a-t</ta>
            <ta e="T19" id="Seg_702" s="T18">eː-ppa-n</ta>
            <ta e="T20" id="Seg_703" s="T19">kak</ta>
            <ta e="T21" id="Seg_704" s="T20">ir-i</ta>
            <ta e="T22" id="Seg_705" s="T21">del-a-t</ta>
            <ta e="T23" id="Seg_706" s="T22">küːdə-r-e</ta>
            <ta e="T24" id="Seg_707" s="T23">kude-ptɨ-lʼdi-ku-ŋ</ta>
            <ta e="T25" id="Seg_708" s="T24">ass</ta>
            <ta e="T26" id="Seg_709" s="T25">swa</ta>
            <ta e="T27" id="Seg_710" s="T26">küdə-r-e</ta>
            <ta e="T28" id="Seg_711" s="T27">tʼäŋ-wa</ta>
            <ta e="T29" id="Seg_712" s="T28">küːde-rʼ-ä-ɣɨn</ta>
            <ta e="T30" id="Seg_713" s="T29">qo-nǯir-ba-t</ta>
            <ta e="T31" id="Seg_714" s="T30">qwai-m</ta>
            <ta e="T32" id="Seg_715" s="T31">üs-se</ta>
            <ta e="T33" id="Seg_716" s="T32">əːtan</ta>
            <ta e="T34" id="Seg_717" s="T33">üt</ta>
            <ta e="T35" id="Seg_718" s="T34">qätɨ-mba-t</ta>
            <ta e="T36" id="Seg_719" s="T35">wes</ta>
            <ta e="T37" id="Seg_720" s="T36">üs-se</ta>
            <ta e="T38" id="Seg_721" s="T37">wes</ta>
            <ta e="T39" id="Seg_722" s="T38">nʼör-o-m</ta>
            <ta e="T40" id="Seg_723" s="T39">täp</ta>
            <ta e="T41" id="Seg_724" s="T40">so-ŋ</ta>
            <ta e="T42" id="Seg_725" s="T41">swa-ŋ</ta>
            <ta e="T43" id="Seg_726" s="T42">qonda-ŋ</ta>
            <ta e="T44" id="Seg_727" s="T43">täp</ta>
            <ta e="T45" id="Seg_728" s="T44">ünde-di-t</ta>
            <ta e="T46" id="Seg_729" s="T45">što</ta>
            <ta e="T47" id="Seg_730" s="T46">eu-t</ta>
            <ta e="T48" id="Seg_731" s="T47">sɨr-ə</ta>
            <ta e="T49" id="Seg_732" s="T48">pargɨlʼ-e-spɨ-ndɨ-t</ta>
            <ta e="T50" id="Seg_733" s="T49">sɨr-la-m</ta>
            <ta e="T51" id="Seg_734" s="T50">ass</ta>
            <ta e="T52" id="Seg_735" s="T51">kaniŋ-ba-tt</ta>
            <ta e="T53" id="Seg_736" s="T52">nʼör-t</ta>
            <ta e="T54" id="Seg_737" s="T53">aps-tä-mbɨ-gu</ta>
            <ta e="T55" id="Seg_738" s="T54">tolʼkošto</ta>
            <ta e="T56" id="Seg_739" s="T55">qwändɨ-t</ta>
            <ta e="T57" id="Seg_740" s="T56">iːlä-dʼe-spa</ta>
            <ta e="T58" id="Seg_741" s="T57">Zina</ta>
            <ta e="T59" id="Seg_742" s="T58">wazɨ-mba-n</ta>
            <ta e="T60" id="Seg_743" s="T59">qopto-ɣɨndo</ta>
            <ta e="T61" id="Seg_744" s="T60">täː</ta>
            <ta e="T62" id="Seg_745" s="T61">paqtɨ-dʼe-mba</ta>
            <ta e="T63" id="Seg_746" s="T62">adejal-a-m</ta>
            <ta e="T64" id="Seg_747" s="T63">täː</ta>
            <ta e="T65" id="Seg_748" s="T64">tʼäʒe-mba-t</ta>
            <ta e="T66" id="Seg_749" s="T65">niːŋ</ta>
            <ta e="T67" id="Seg_750" s="T66">swa-ŋ</ta>
            <ta e="T68" id="Seg_751" s="T67">aːmɨ-di-mba-n</ta>
            <ta e="T69" id="Seg_752" s="T68">i</ta>
            <ta e="T70" id="Seg_753" s="T69">nʼäkɨl-di-mba-n</ta>
            <ta e="T71" id="Seg_754" s="T70">qolal-gɨdi</ta>
            <ta e="T72" id="Seg_755" s="T71">qaborɣ-ɨ-m</ta>
            <ta e="T73" id="Seg_756" s="T72">tʼedʼä-či-mba-t</ta>
            <ta e="T74" id="Seg_757" s="T73">uːd-o-m</ta>
            <ta e="T75" id="Seg_758" s="T74">wandɨ-m</ta>
            <ta e="T76" id="Seg_759" s="T75">müzulʼǯu-gu</ta>
            <ta e="T77" id="Seg_760" s="T76">qwan-ba</ta>
            <ta e="T78" id="Seg_761" s="T77">qaborɣ-ɨ-m-də</ta>
            <ta e="T79" id="Seg_762" s="T78">niŋɨ-mba-t</ta>
            <ta e="T80" id="Seg_763" s="T79">a</ta>
            <ta e="T81" id="Seg_764" s="T80">köl</ta>
            <ta e="T82" id="Seg_765" s="T81">pümmɨ-ze</ta>
            <ta e="T83" id="Seg_766" s="T82">qwan-ba-n</ta>
            <ta e="T84" id="Seg_767" s="T83">wandɨ-m</ta>
            <ta e="T85" id="Seg_768" s="T84">müzulʼǯi-gu</ta>
            <ta e="T86" id="Seg_769" s="T85">wannɨ-m-d</ta>
            <ta e="T87" id="Seg_770" s="T86">i</ta>
            <ta e="T88" id="Seg_771" s="T87">uːd-o-m-t</ta>
            <ta e="T89" id="Seg_772" s="T88">qannä-mbədi</ta>
            <ta e="T90" id="Seg_773" s="T89">üt</ta>
            <ta e="T91" id="Seg_774" s="T90">wadʼö-w</ta>
            <ta e="T92" id="Seg_775" s="T91">qob-o-w</ta>
            <ta e="T93" id="Seg_776" s="T92">kande-pti-t</ta>
            <ta e="T94" id="Seg_777" s="T93">sidʼe-w</ta>
            <ta e="T95" id="Seg_778" s="T94">larɨ-mba</ta>
            <ta e="T96" id="Seg_779" s="T95">larɨ-tʼi-ku-ŋ</ta>
            <ta e="T97" id="Seg_780" s="T96">sidʼe-u</ta>
            <ta e="T98" id="Seg_781" s="T97">laɣ-ol-ɨ-kku-ŋ</ta>
            <ta e="T99" id="Seg_782" s="T98">qobo-u</ta>
            <ta e="T100" id="Seg_783" s="T99">kande-dʼi-ku-ŋ</ta>
            <ta e="T101" id="Seg_784" s="T100">üd-o-m</ta>
            <ta e="T102" id="Seg_785" s="T101">qamǯe-lʼ-e-spe-s-t</ta>
            <ta e="T103" id="Seg_786" s="T102">ud-u-ze</ta>
            <ta e="T104" id="Seg_787" s="T103">moɣol-ɣɨnt</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_788" s="T1">Zina</ta>
            <ta e="T3" id="Seg_789" s="T2">qɨlʼdi-mbɨ</ta>
            <ta e="T4" id="Seg_790" s="T3">söde-dʼi-mbɨ</ta>
            <ta e="T5" id="Seg_791" s="T4">qondu-le</ta>
            <ta e="T6" id="Seg_792" s="T5">söde-dʼi-špə-n</ta>
            <ta e="T7" id="Seg_793" s="T6">a</ta>
            <ta e="T8" id="Seg_794" s="T7">uʒ</ta>
            <ta e="T9" id="Seg_795" s="T8">laːɣwat-ntɨ-n</ta>
            <ta e="T10" id="Seg_796" s="T9">qaj-no</ta>
            <ta e="T11" id="Seg_797" s="T10">täp-nä</ta>
            <ta e="T12" id="Seg_798" s="T11">laːɣwat-ntɨ-gu</ta>
            <ta e="T13" id="Seg_799" s="T12">iːr-ɨ-lʼ</ta>
            <ta e="T14" id="Seg_800" s="T13">dʼel</ta>
            <ta e="T15" id="Seg_801" s="T14">täj-qɨntɨ</ta>
            <ta e="T16" id="Seg_802" s="T15">pat-qɨl-ku-ntɨ</ta>
            <ta e="T17" id="Seg_803" s="T16">telʼdʼan</ta>
            <ta e="T18" id="Seg_804" s="T17">dʼel-ɨ-tə</ta>
            <ta e="T19" id="Seg_805" s="T18">eː-mbɨ-n</ta>
            <ta e="T20" id="Seg_806" s="T19">kak</ta>
            <ta e="T21" id="Seg_807" s="T20">iːr-lʼ</ta>
            <ta e="T22" id="Seg_808" s="T21">dʼel-ɨ-tə</ta>
            <ta e="T23" id="Seg_809" s="T22">ködɨ-r-ptä</ta>
            <ta e="T24" id="Seg_810" s="T23">ködɨ-ptɨ-lǯi-ku-n</ta>
            <ta e="T25" id="Seg_811" s="T24">asa</ta>
            <ta e="T26" id="Seg_812" s="T25">sawa</ta>
            <ta e="T27" id="Seg_813" s="T26">ködɨ-r-ptä</ta>
            <ta e="T28" id="Seg_814" s="T27">tʼäkku-nɨ</ta>
            <ta e="T29" id="Seg_815" s="T28">ködɨ-r-ptä-qɨn</ta>
            <ta e="T30" id="Seg_816" s="T29">qo-nǯir-mbɨ-t</ta>
            <ta e="T31" id="Seg_817" s="T30">kwaj-m</ta>
            <ta e="T32" id="Seg_818" s="T31">üt-se</ta>
            <ta e="T33" id="Seg_819" s="T32">üdɨn</ta>
            <ta e="T34" id="Seg_820" s="T33">üt</ta>
            <ta e="T35" id="Seg_821" s="T34">qättɨ-mbɨ-t</ta>
            <ta e="T36" id="Seg_822" s="T35">wesʼ</ta>
            <ta e="T37" id="Seg_823" s="T36">üt-se</ta>
            <ta e="T38" id="Seg_824" s="T37">wesʼ</ta>
            <ta e="T39" id="Seg_825" s="T38">nür-ɨ-m</ta>
            <ta e="T40" id="Seg_826" s="T39">täp</ta>
            <ta e="T41" id="Seg_827" s="T40">so-ŋ</ta>
            <ta e="T42" id="Seg_828" s="T41">sawa-ŋ</ta>
            <ta e="T43" id="Seg_829" s="T42">qondu-n</ta>
            <ta e="T44" id="Seg_830" s="T43">täp</ta>
            <ta e="T45" id="Seg_831" s="T44">ündɨ-dʼi-t</ta>
            <ta e="T46" id="Seg_832" s="T45">što</ta>
            <ta e="T47" id="Seg_833" s="T46">awa-tə</ta>
            <ta e="T48" id="Seg_834" s="T47">sɨr-ɨ</ta>
            <ta e="T49" id="Seg_835" s="T48">pargəl-ɨ-špə-ntɨ-t</ta>
            <ta e="T50" id="Seg_836" s="T49">sɨr-la-m</ta>
            <ta e="T51" id="Seg_837" s="T50">asa</ta>
            <ta e="T52" id="Seg_838" s="T51">kaniŋ-mbɨ-tɨn</ta>
            <ta e="T53" id="Seg_839" s="T52">nür-ntə</ta>
            <ta e="T54" id="Seg_840" s="T53">apsǝ-tɨ-mbɨ-gu</ta>
            <ta e="T55" id="Seg_841" s="T54">tolʼkošto</ta>
            <ta e="T56" id="Seg_842" s="T55">qwäntə-tə</ta>
            <ta e="T57" id="Seg_843" s="T56">elɨ-dʼi-špə</ta>
            <ta e="T58" id="Seg_844" s="T57">Zina</ta>
            <ta e="T59" id="Seg_845" s="T58">wazɨ-mbɨ-n</ta>
            <ta e="T60" id="Seg_846" s="T59">qoptə-qɨntɨ</ta>
            <ta e="T61" id="Seg_847" s="T60">to</ta>
            <ta e="T62" id="Seg_848" s="T61">paktɨ-dʼi-mbɨ</ta>
            <ta e="T63" id="Seg_849" s="T62">adʼejal-ɨ-m</ta>
            <ta e="T64" id="Seg_850" s="T63">to</ta>
            <ta e="T65" id="Seg_851" s="T64">tʼäʒə-mbɨ-t</ta>
            <ta e="T66" id="Seg_852" s="T65">niŋ</ta>
            <ta e="T67" id="Seg_853" s="T66">sawa-ŋ</ta>
            <ta e="T68" id="Seg_854" s="T67">aːmə-dʼi-mbɨ-n</ta>
            <ta e="T69" id="Seg_855" s="T68">i</ta>
            <ta e="T70" id="Seg_856" s="T69">nʼäqɨl-dʼi-mbɨ-n</ta>
            <ta e="T71" id="Seg_857" s="T70">kolal-gɨdi</ta>
            <ta e="T72" id="Seg_858" s="T71">qaborɣ-ɨ-m</ta>
            <ta e="T73" id="Seg_859" s="T72">tʼäʒə-ču-mbɨ-t</ta>
            <ta e="T74" id="Seg_860" s="T73">ut-ɨ-m</ta>
            <ta e="T75" id="Seg_861" s="T74">wandǝ-m</ta>
            <ta e="T76" id="Seg_862" s="T75">müzulǯu-gu</ta>
            <ta e="T77" id="Seg_863" s="T76">qwan-mbɨ</ta>
            <ta e="T78" id="Seg_864" s="T77">qaborɣ-ɨ-m-tə</ta>
            <ta e="T79" id="Seg_865" s="T78">niŋgɨ-mbɨ-t</ta>
            <ta e="T80" id="Seg_866" s="T79">a</ta>
            <ta e="T81" id="Seg_867" s="T80">köl</ta>
            <ta e="T82" id="Seg_868" s="T81">pümmə-se</ta>
            <ta e="T83" id="Seg_869" s="T82">qwan-mbɨ-n</ta>
            <ta e="T84" id="Seg_870" s="T83">wandǝ-m</ta>
            <ta e="T85" id="Seg_871" s="T84">müzulǯu-gu</ta>
            <ta e="T86" id="Seg_872" s="T85">wandǝ-m-tə</ta>
            <ta e="T87" id="Seg_873" s="T86">i</ta>
            <ta e="T88" id="Seg_874" s="T87">ut-ɨ-m-tə</ta>
            <ta e="T89" id="Seg_875" s="T88">qandɨ-mbɨdi</ta>
            <ta e="T90" id="Seg_876" s="T89">üt</ta>
            <ta e="T91" id="Seg_877" s="T90">wadʼi-w</ta>
            <ta e="T92" id="Seg_878" s="T91">qob-ɨ-w</ta>
            <ta e="T93" id="Seg_879" s="T92">qandɨ-ptɨ-t</ta>
            <ta e="T94" id="Seg_880" s="T93">sidʼe-w</ta>
            <ta e="T95" id="Seg_881" s="T94">*larɨ-mbɨ</ta>
            <ta e="T96" id="Seg_882" s="T95">*larɨ-ntɨ-ku-n</ta>
            <ta e="T97" id="Seg_883" s="T96">sidʼe-w</ta>
            <ta e="T98" id="Seg_884" s="T97">laqǝ-ol-ɨ-ku-n</ta>
            <ta e="T99" id="Seg_885" s="T98">qob-w</ta>
            <ta e="T100" id="Seg_886" s="T99">qandɨ-dʼi-ku-n</ta>
            <ta e="T101" id="Seg_887" s="T100">üt-ɨ-m</ta>
            <ta e="T102" id="Seg_888" s="T101">qamǯu-l-ɨ-špə-sɨ-t</ta>
            <ta e="T103" id="Seg_889" s="T102">ut-w-se</ta>
            <ta e="T104" id="Seg_890" s="T103">mogol-qɨntɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_891" s="T1">Zina.[NOM]</ta>
            <ta e="T3" id="Seg_892" s="T2">wake.up-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_893" s="T3">awake-RFL-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_894" s="T4">sleep-CVB</ta>
            <ta e="T6" id="Seg_895" s="T5">awake-RFL-IPFV2-3SG.S</ta>
            <ta e="T7" id="Seg_896" s="T6">and</ta>
            <ta e="T8" id="Seg_897" s="T7">already</ta>
            <ta e="T9" id="Seg_898" s="T8">begin.to.laugh-IPFV-3SG.S</ta>
            <ta e="T10" id="Seg_899" s="T9">what-TRL</ta>
            <ta e="T11" id="Seg_900" s="T10">(s)he-ALL</ta>
            <ta e="T12" id="Seg_901" s="T11">begin.to.laugh-IPFV-INF</ta>
            <ta e="T13" id="Seg_902" s="T12">earlier-EP-ADJZ</ta>
            <ta e="T14" id="Seg_903" s="T13">day.[NOM]</ta>
            <ta e="T15" id="Seg_904" s="T14">mind-ILL.3SG</ta>
            <ta e="T16" id="Seg_905" s="T15">go.down-DRV-HAB-INFER.[3SG.S]</ta>
            <ta e="T17" id="Seg_906" s="T16">yesterday</ta>
            <ta e="T18" id="Seg_907" s="T17">day.[NOM]-EP-3SG</ta>
            <ta e="T19" id="Seg_908" s="T18">be-PST.NAR-3SG.S</ta>
            <ta e="T20" id="Seg_909" s="T19">as</ta>
            <ta e="T21" id="Seg_910" s="T20">earlier-ADJZ</ta>
            <ta e="T22" id="Seg_911" s="T21">day.[NOM]-EP-3SG</ta>
            <ta e="T23" id="Seg_912" s="T22">dream-VBLZ-ACTN.[NOM]</ta>
            <ta e="T24" id="Seg_913" s="T23">dream-VBLZ-PFV-HAB-3SG.S</ta>
            <ta e="T25" id="Seg_914" s="T24">NEG</ta>
            <ta e="T26" id="Seg_915" s="T25">good</ta>
            <ta e="T27" id="Seg_916" s="T26">dream-VBLZ-ACTN.[NOM]</ta>
            <ta e="T28" id="Seg_917" s="T27">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_918" s="T28">dream-VBLZ-ACTN-LOC</ta>
            <ta e="T30" id="Seg_919" s="T29">see-DRV-PST.NAR-3SG.O</ta>
            <ta e="T31" id="Seg_920" s="T30">big.river-ACC</ta>
            <ta e="T32" id="Seg_921" s="T31">water-COM</ta>
            <ta e="T33" id="Seg_922" s="T32">in.spring</ta>
            <ta e="T34" id="Seg_923" s="T33">water.[NOM]</ta>
            <ta e="T35" id="Seg_924" s="T34">overflow-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_925" s="T35">all</ta>
            <ta e="T37" id="Seg_926" s="T36">water-INSTR</ta>
            <ta e="T38" id="Seg_927" s="T37">all</ta>
            <ta e="T39" id="Seg_928" s="T38">field-EP-ACC</ta>
            <ta e="T40" id="Seg_929" s="T39">(s)he.[NOM]</ta>
            <ta e="T41" id="Seg_930" s="T40">good-ADVZ</ta>
            <ta e="T42" id="Seg_931" s="T41">good-ADVZ</ta>
            <ta e="T43" id="Seg_932" s="T42">sleep-3SG.S</ta>
            <ta e="T44" id="Seg_933" s="T43">(s)he.[NOM]</ta>
            <ta e="T45" id="Seg_934" s="T44">hear-DRV-3SG.O</ta>
            <ta e="T46" id="Seg_935" s="T45">that</ta>
            <ta e="T47" id="Seg_936" s="T46">mother.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_937" s="T47">cow-EP.[NOM]</ta>
            <ta e="T49" id="Seg_938" s="T48">milk-EP-IPFV2-INFER-3SG.O</ta>
            <ta e="T50" id="Seg_939" s="T49">cow-PL-ACC</ta>
            <ta e="T51" id="Seg_940" s="T50">NEG</ta>
            <ta e="T52" id="Seg_941" s="T51">drive.out-PST.NAR-3PL</ta>
            <ta e="T53" id="Seg_942" s="T52">field-ILL</ta>
            <ta e="T54" id="Seg_943" s="T53">food-VBLZ-DUR-INF</ta>
            <ta e="T55" id="Seg_944" s="T54">just.now</ta>
            <ta e="T56" id="Seg_945" s="T55">dawn.[NOM]-3SG</ta>
            <ta e="T57" id="Seg_946" s="T56">live-RFL-IPFV2.[3SG.S]</ta>
            <ta e="T58" id="Seg_947" s="T57">Zina.[NOM]</ta>
            <ta e="T59" id="Seg_948" s="T58">get.up-PST.NAR-3SG.S</ta>
            <ta e="T60" id="Seg_949" s="T59">bed-EL.3SG</ta>
            <ta e="T61" id="Seg_950" s="T60">away</ta>
            <ta e="T62" id="Seg_951" s="T61">jump-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_952" s="T62">blanket-EP-ACC</ta>
            <ta e="T64" id="Seg_953" s="T63">away</ta>
            <ta e="T65" id="Seg_954" s="T64">throw-PST.NAR-3SG.O</ta>
            <ta e="T66" id="Seg_955" s="T65">so</ta>
            <ta e="T67" id="Seg_956" s="T66">good-ADVZ</ta>
            <ta e="T68" id="Seg_957" s="T67">yawn-DRV-PST.NAR-3SG.S</ta>
            <ta e="T69" id="Seg_958" s="T68">and</ta>
            <ta e="T70" id="Seg_959" s="T69">stretch-RFL-PST.NAR-3SG.S</ta>
            <ta e="T71" id="Seg_960" s="T70">sleeve-CAR</ta>
            <ta e="T72" id="Seg_961" s="T71">shirt-EP-ACC</ta>
            <ta e="T73" id="Seg_962" s="T72">throw-TR-PST.NAR-3SG.O</ta>
            <ta e="T74" id="Seg_963" s="T73">hand-EP-ACC</ta>
            <ta e="T75" id="Seg_964" s="T74">face-ACC</ta>
            <ta e="T76" id="Seg_965" s="T75">wash-INF</ta>
            <ta e="T77" id="Seg_966" s="T76">leave-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_967" s="T77">shirt-EP-ACC-3SG</ta>
            <ta e="T79" id="Seg_968" s="T78">take.off-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_969" s="T79">and</ta>
            <ta e="T81" id="Seg_970" s="T80">%%</ta>
            <ta e="T82" id="Seg_971" s="T81">trousers-INSTR</ta>
            <ta e="T83" id="Seg_972" s="T82">leave-PST.NAR-3SG.S</ta>
            <ta e="T84" id="Seg_973" s="T83">face-ACC</ta>
            <ta e="T85" id="Seg_974" s="T84">wash-INF</ta>
            <ta e="T86" id="Seg_975" s="T85">face-ACC-3SG</ta>
            <ta e="T87" id="Seg_976" s="T86">and</ta>
            <ta e="T88" id="Seg_977" s="T87">hand-EP-ACC-3SG</ta>
            <ta e="T89" id="Seg_978" s="T88">freeze-PTCP.PST</ta>
            <ta e="T90" id="Seg_979" s="T89">water.[NOM]</ta>
            <ta e="T91" id="Seg_980" s="T90">body.[NOM]-1SG</ta>
            <ta e="T92" id="Seg_981" s="T91">skin.[NOM]-EP-1SG</ta>
            <ta e="T93" id="Seg_982" s="T92">freeze-CAUS-3SG.O</ta>
            <ta e="T94" id="Seg_983" s="T93">heart.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_984" s="T94">get.afraid-DUR.[3SG.S]</ta>
            <ta e="T96" id="Seg_985" s="T95">get.afraid-IPFV-HAB-3SG.S</ta>
            <ta e="T97" id="Seg_986" s="T96">heart.[NOM]-1SG</ta>
            <ta e="T98" id="Seg_987" s="T97">move-MOM-EP-HAB-3SG.S</ta>
            <ta e="T99" id="Seg_988" s="T98">skin.[NOM]-1SG</ta>
            <ta e="T100" id="Seg_989" s="T99">freeze-DRV-HAB-3SG.S</ta>
            <ta e="T101" id="Seg_990" s="T100">water-EP-ACC</ta>
            <ta e="T102" id="Seg_991" s="T101">pour-DRV-EP-IPFV2-PST-3SG.O</ta>
            <ta e="T103" id="Seg_992" s="T102">hand-1SG-INSTR</ta>
            <ta e="T104" id="Seg_993" s="T103">back-ILL.3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_994" s="T1">Зина.[NOM]</ta>
            <ta e="T3" id="Seg_995" s="T2">проснуться-PST.NAR.[3SG.S]</ta>
            <ta e="T4" id="Seg_996" s="T3">разбудить-RFL-PST.NAR.[3SG.S]</ta>
            <ta e="T5" id="Seg_997" s="T4">спать-CVB</ta>
            <ta e="T6" id="Seg_998" s="T5">разбудить-RFL-IPFV2-3SG.S</ta>
            <ta e="T7" id="Seg_999" s="T6">а</ta>
            <ta e="T8" id="Seg_1000" s="T7">уже</ta>
            <ta e="T9" id="Seg_1001" s="T8">засмеяться-IPFV-3SG.S</ta>
            <ta e="T10" id="Seg_1002" s="T9">что-TRL</ta>
            <ta e="T11" id="Seg_1003" s="T10">он(а)-ALL</ta>
            <ta e="T12" id="Seg_1004" s="T11">засмеяться-IPFV-INF</ta>
            <ta e="T13" id="Seg_1005" s="T12">давно-EP-ADJZ</ta>
            <ta e="T14" id="Seg_1006" s="T13">день.[NOM]</ta>
            <ta e="T15" id="Seg_1007" s="T14">ум-ILL.3SG</ta>
            <ta e="T16" id="Seg_1008" s="T15">залезть-DRV-HAB-INFER.[3SG.S]</ta>
            <ta e="T17" id="Seg_1009" s="T16">вчера</ta>
            <ta e="T18" id="Seg_1010" s="T17">день.[NOM]-EP-3SG</ta>
            <ta e="T19" id="Seg_1011" s="T18">быть-PST.NAR-3SG.S</ta>
            <ta e="T20" id="Seg_1012" s="T19">как</ta>
            <ta e="T21" id="Seg_1013" s="T20">давно-ADJZ</ta>
            <ta e="T22" id="Seg_1014" s="T21">день.[NOM]-EP-3SG</ta>
            <ta e="T23" id="Seg_1015" s="T22">сон-VBLZ-ACTN.[NOM]</ta>
            <ta e="T24" id="Seg_1016" s="T23">сон-VBLZ-PFV-HAB-3SG.S</ta>
            <ta e="T25" id="Seg_1017" s="T24">NEG</ta>
            <ta e="T26" id="Seg_1018" s="T25">хороший</ta>
            <ta e="T27" id="Seg_1019" s="T26">сон-VBLZ-ACTN.[NOM]</ta>
            <ta e="T28" id="Seg_1020" s="T27">NEG.EX-CO.[3SG.S]</ta>
            <ta e="T29" id="Seg_1021" s="T28">сон-VBLZ-ACTN-LOC</ta>
            <ta e="T30" id="Seg_1022" s="T29">увидеть-DRV-PST.NAR-3SG.O</ta>
            <ta e="T31" id="Seg_1023" s="T30">большая.река-ACC</ta>
            <ta e="T32" id="Seg_1024" s="T31">вода-COM</ta>
            <ta e="T33" id="Seg_1025" s="T32">весной</ta>
            <ta e="T34" id="Seg_1026" s="T33">вода.[NOM]</ta>
            <ta e="T35" id="Seg_1027" s="T34">затопить-PST.NAR-3SG.O</ta>
            <ta e="T36" id="Seg_1028" s="T35">весь</ta>
            <ta e="T37" id="Seg_1029" s="T36">вода-INSTR</ta>
            <ta e="T38" id="Seg_1030" s="T37">весь</ta>
            <ta e="T39" id="Seg_1031" s="T38">поле-EP-ACC</ta>
            <ta e="T40" id="Seg_1032" s="T39">он(а).[NOM]</ta>
            <ta e="T41" id="Seg_1033" s="T40">хороший-ADVZ</ta>
            <ta e="T42" id="Seg_1034" s="T41">хороший-ADVZ</ta>
            <ta e="T43" id="Seg_1035" s="T42">спать-3SG.S</ta>
            <ta e="T44" id="Seg_1036" s="T43">он(а).[NOM]</ta>
            <ta e="T45" id="Seg_1037" s="T44">услышать-DRV-3SG.O</ta>
            <ta e="T46" id="Seg_1038" s="T45">что</ta>
            <ta e="T47" id="Seg_1039" s="T46">мать.[NOM]-3SG</ta>
            <ta e="T48" id="Seg_1040" s="T47">корова-EP.[NOM]</ta>
            <ta e="T49" id="Seg_1041" s="T48">подоить-EP-IPFV2-INFER-3SG.O</ta>
            <ta e="T50" id="Seg_1042" s="T49">корова-PL-ACC</ta>
            <ta e="T51" id="Seg_1043" s="T50">NEG</ta>
            <ta e="T52" id="Seg_1044" s="T51">выгнать-PST.NAR-3PL</ta>
            <ta e="T53" id="Seg_1045" s="T52">поле-ILL</ta>
            <ta e="T54" id="Seg_1046" s="T53">еда-VBLZ-DUR-INF</ta>
            <ta e="T55" id="Seg_1047" s="T54">только.что</ta>
            <ta e="T56" id="Seg_1048" s="T55">рассвет.[NOM]-3SG</ta>
            <ta e="T57" id="Seg_1049" s="T56">жить-RFL-IPFV2.[3SG.S]</ta>
            <ta e="T58" id="Seg_1050" s="T57">Зина.[NOM]</ta>
            <ta e="T59" id="Seg_1051" s="T58">встать-PST.NAR-3SG.S</ta>
            <ta e="T60" id="Seg_1052" s="T59">кровать-EL.3SG</ta>
            <ta e="T61" id="Seg_1053" s="T60">прочь</ta>
            <ta e="T62" id="Seg_1054" s="T61">прыгнуть-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T63" id="Seg_1055" s="T62">одеяло-EP-ACC</ta>
            <ta e="T64" id="Seg_1056" s="T63">прочь</ta>
            <ta e="T65" id="Seg_1057" s="T64">бросить-PST.NAR-3SG.O</ta>
            <ta e="T66" id="Seg_1058" s="T65">так</ta>
            <ta e="T67" id="Seg_1059" s="T66">хороший-ADVZ</ta>
            <ta e="T68" id="Seg_1060" s="T67">зевнуть-DRV-PST.NAR-3SG.S</ta>
            <ta e="T69" id="Seg_1061" s="T68">и</ta>
            <ta e="T70" id="Seg_1062" s="T69">натянуть-RFL-PST.NAR-3SG.S</ta>
            <ta e="T71" id="Seg_1063" s="T70">рукав-CAR</ta>
            <ta e="T72" id="Seg_1064" s="T71">рубашка-EP-ACC</ta>
            <ta e="T73" id="Seg_1065" s="T72">бросить-TR-PST.NAR-3SG.O</ta>
            <ta e="T74" id="Seg_1066" s="T73">рука-EP-ACC</ta>
            <ta e="T75" id="Seg_1067" s="T74">лицо-ACC</ta>
            <ta e="T76" id="Seg_1068" s="T75">вымыть-INF</ta>
            <ta e="T77" id="Seg_1069" s="T76">отправиться-PST.NAR.[3SG.S]</ta>
            <ta e="T78" id="Seg_1070" s="T77">рубашка-EP-ACC-3SG</ta>
            <ta e="T79" id="Seg_1071" s="T78">снять-PST.NAR-3SG.O</ta>
            <ta e="T80" id="Seg_1072" s="T79">а</ta>
            <ta e="T81" id="Seg_1073" s="T80">%%</ta>
            <ta e="T82" id="Seg_1074" s="T81">брюки-INSTR</ta>
            <ta e="T83" id="Seg_1075" s="T82">отправиться-PST.NAR-3SG.S</ta>
            <ta e="T84" id="Seg_1076" s="T83">лицо-ACC</ta>
            <ta e="T85" id="Seg_1077" s="T84">вымыть-INF</ta>
            <ta e="T86" id="Seg_1078" s="T85">лицо-ACC-3SG</ta>
            <ta e="T87" id="Seg_1079" s="T86">и</ta>
            <ta e="T88" id="Seg_1080" s="T87">рука-EP-ACC-3SG</ta>
            <ta e="T89" id="Seg_1081" s="T88">замерзнуть-PTCP.PST</ta>
            <ta e="T90" id="Seg_1082" s="T89">вода.[NOM]</ta>
            <ta e="T91" id="Seg_1083" s="T90">тело.[NOM]-1SG</ta>
            <ta e="T92" id="Seg_1084" s="T91">кожа.[NOM]-EP-1SG</ta>
            <ta e="T93" id="Seg_1085" s="T92">замерзнуть-CAUS-3SG.O</ta>
            <ta e="T94" id="Seg_1086" s="T93">сердце.[NOM]-1SG</ta>
            <ta e="T95" id="Seg_1087" s="T94">испугаться-DUR.[3SG.S]</ta>
            <ta e="T96" id="Seg_1088" s="T95">испугаться-IPFV-HAB-3SG.S</ta>
            <ta e="T97" id="Seg_1089" s="T96">сердце.[NOM]-1SG</ta>
            <ta e="T98" id="Seg_1090" s="T97">шевелиться-MOM-EP-HAB-3SG.S</ta>
            <ta e="T99" id="Seg_1091" s="T98">шкура.[NOM]-1SG</ta>
            <ta e="T100" id="Seg_1092" s="T99">замерзнуть-DRV-HAB-3SG.S</ta>
            <ta e="T101" id="Seg_1093" s="T100">вода-EP-ACC</ta>
            <ta e="T102" id="Seg_1094" s="T101">налить-DRV-EP-IPFV2-PST-3SG.O</ta>
            <ta e="T103" id="Seg_1095" s="T102">рука-1SG-INSTR</ta>
            <ta e="T104" id="Seg_1096" s="T103">спина-ILL.3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1097" s="T1">nprop.[n:case]</ta>
            <ta e="T3" id="Seg_1098" s="T2">v-v:tense.[v:pn]</ta>
            <ta e="T4" id="Seg_1099" s="T3">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_1100" s="T4">v-v&gt;adv</ta>
            <ta e="T6" id="Seg_1101" s="T5">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T7" id="Seg_1102" s="T6">conj</ta>
            <ta e="T8" id="Seg_1103" s="T7">adv</ta>
            <ta e="T9" id="Seg_1104" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_1105" s="T9">interrog-n:case</ta>
            <ta e="T11" id="Seg_1106" s="T10">pers-n:case</ta>
            <ta e="T12" id="Seg_1107" s="T11">v-v&gt;v-v:inf</ta>
            <ta e="T13" id="Seg_1108" s="T12">adv-n:ins-adv&gt;adj</ta>
            <ta e="T14" id="Seg_1109" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_1110" s="T14">n-n:case.poss</ta>
            <ta e="T16" id="Seg_1111" s="T15">v-v&gt;v-v&gt;v-v:mood.[v:pn]</ta>
            <ta e="T17" id="Seg_1112" s="T16">adv</ta>
            <ta e="T18" id="Seg_1113" s="T17">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T19" id="Seg_1114" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_1115" s="T19">conj</ta>
            <ta e="T21" id="Seg_1116" s="T20">adv-adv&gt;adj</ta>
            <ta e="T22" id="Seg_1117" s="T21">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T23" id="Seg_1118" s="T22">n-n&gt;v-v&gt;n.[n:case]</ta>
            <ta e="T24" id="Seg_1119" s="T23">n-n&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T25" id="Seg_1120" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1121" s="T25">adj</ta>
            <ta e="T27" id="Seg_1122" s="T26">n-n&gt;v-v&gt;n.[n:case]</ta>
            <ta e="T28" id="Seg_1123" s="T27">v-v:ins.[v:pn]</ta>
            <ta e="T29" id="Seg_1124" s="T28">n-n&gt;v-v&gt;n-n:case</ta>
            <ta e="T30" id="Seg_1125" s="T29">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1126" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1127" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_1128" s="T32">adv</ta>
            <ta e="T34" id="Seg_1129" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_1130" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1131" s="T35">quant</ta>
            <ta e="T37" id="Seg_1132" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1133" s="T37">quant</ta>
            <ta e="T39" id="Seg_1134" s="T38">n-n:ins-n:case</ta>
            <ta e="T40" id="Seg_1135" s="T39">pers.[n:case]</ta>
            <ta e="T41" id="Seg_1136" s="T40">adj-adj&gt;adv</ta>
            <ta e="T42" id="Seg_1137" s="T41">adj-adj&gt;adv</ta>
            <ta e="T43" id="Seg_1138" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_1139" s="T43">pers.[n:case]</ta>
            <ta e="T45" id="Seg_1140" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_1141" s="T45">conj</ta>
            <ta e="T47" id="Seg_1142" s="T46">n.[n:case]-n:poss</ta>
            <ta e="T48" id="Seg_1143" s="T47">n-n:ins.[n:case]</ta>
            <ta e="T49" id="Seg_1144" s="T48">v-v:ins-v&gt;v-v:mood-v:pn</ta>
            <ta e="T50" id="Seg_1145" s="T49">n-n:num-n:case</ta>
            <ta e="T51" id="Seg_1146" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1147" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1148" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_1149" s="T53">n-n&gt;v-v&gt;v-v:inf</ta>
            <ta e="T55" id="Seg_1150" s="T54">adv</ta>
            <ta e="T56" id="Seg_1151" s="T55">n.[n:case]-n:poss</ta>
            <ta e="T57" id="Seg_1152" s="T56">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T58" id="Seg_1153" s="T57">nprop.[n:case]</ta>
            <ta e="T59" id="Seg_1154" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_1155" s="T59">n-n:case.poss</ta>
            <ta e="T61" id="Seg_1156" s="T60">preverb</ta>
            <ta e="T62" id="Seg_1157" s="T61">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T63" id="Seg_1158" s="T62">n-n:ins-n:case</ta>
            <ta e="T64" id="Seg_1159" s="T63">preverb</ta>
            <ta e="T65" id="Seg_1160" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1161" s="T65">adv</ta>
            <ta e="T67" id="Seg_1162" s="T66">adj-adj&gt;adv</ta>
            <ta e="T68" id="Seg_1163" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1164" s="T68">conj</ta>
            <ta e="T70" id="Seg_1165" s="T69">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1166" s="T70">n-n&gt;adj</ta>
            <ta e="T72" id="Seg_1167" s="T71">n-n:ins-n:case</ta>
            <ta e="T73" id="Seg_1168" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1169" s="T73">n-n:ins-n:case</ta>
            <ta e="T75" id="Seg_1170" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1171" s="T75">v-v:inf</ta>
            <ta e="T77" id="Seg_1172" s="T76">v-v:tense.[v:pn]</ta>
            <ta e="T78" id="Seg_1173" s="T77">n-n:ins-n:case-n:poss</ta>
            <ta e="T79" id="Seg_1174" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_1175" s="T79">conj</ta>
            <ta e="T81" id="Seg_1176" s="T80">%%</ta>
            <ta e="T82" id="Seg_1177" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1178" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_1179" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1180" s="T84">v-v:inf</ta>
            <ta e="T86" id="Seg_1181" s="T85">n-n:case-n:poss</ta>
            <ta e="T87" id="Seg_1182" s="T86">conj</ta>
            <ta e="T88" id="Seg_1183" s="T87">n-n:ins-n:case-n:poss</ta>
            <ta e="T89" id="Seg_1184" s="T88">v-v&gt;ptcp</ta>
            <ta e="T90" id="Seg_1185" s="T89">n.[n:case]</ta>
            <ta e="T91" id="Seg_1186" s="T90">n.[n:case]-n:poss</ta>
            <ta e="T92" id="Seg_1187" s="T91">n.[n:case]-n:ins-n:poss</ta>
            <ta e="T93" id="Seg_1188" s="T92">v-v&gt;v-v:pn</ta>
            <ta e="T94" id="Seg_1189" s="T93">n.[n:case]-n:poss</ta>
            <ta e="T95" id="Seg_1190" s="T94">v-v&gt;v.[v:pn]</ta>
            <ta e="T96" id="Seg_1191" s="T95">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T97" id="Seg_1192" s="T96">n.[n:case]-n:poss</ta>
            <ta e="T98" id="Seg_1193" s="T97">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_1194" s="T98">n.[n:case]-n:poss</ta>
            <ta e="T100" id="Seg_1195" s="T99">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_1196" s="T100">n-n:ins-n:case</ta>
            <ta e="T102" id="Seg_1197" s="T101">v-v&gt;v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_1198" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_1199" s="T103">n-n:case.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1200" s="T1">nprop</ta>
            <ta e="T3" id="Seg_1201" s="T2">v</ta>
            <ta e="T4" id="Seg_1202" s="T3">v</ta>
            <ta e="T5" id="Seg_1203" s="T4">adv</ta>
            <ta e="T6" id="Seg_1204" s="T5">v</ta>
            <ta e="T7" id="Seg_1205" s="T6">conj</ta>
            <ta e="T8" id="Seg_1206" s="T7">adv</ta>
            <ta e="T9" id="Seg_1207" s="T8">v</ta>
            <ta e="T10" id="Seg_1208" s="T9">interrog</ta>
            <ta e="T11" id="Seg_1209" s="T10">pers</ta>
            <ta e="T12" id="Seg_1210" s="T11">v</ta>
            <ta e="T13" id="Seg_1211" s="T12">adj</ta>
            <ta e="T14" id="Seg_1212" s="T13">n</ta>
            <ta e="T15" id="Seg_1213" s="T14">n</ta>
            <ta e="T16" id="Seg_1214" s="T15">v</ta>
            <ta e="T17" id="Seg_1215" s="T16">adv</ta>
            <ta e="T18" id="Seg_1216" s="T17">n</ta>
            <ta e="T19" id="Seg_1217" s="T18">v</ta>
            <ta e="T20" id="Seg_1218" s="T19">conj</ta>
            <ta e="T21" id="Seg_1219" s="T20">adj</ta>
            <ta e="T22" id="Seg_1220" s="T21">n</ta>
            <ta e="T23" id="Seg_1221" s="T22">v</ta>
            <ta e="T24" id="Seg_1222" s="T23">v</ta>
            <ta e="T25" id="Seg_1223" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1224" s="T25">adj</ta>
            <ta e="T27" id="Seg_1225" s="T26">v</ta>
            <ta e="T28" id="Seg_1226" s="T27">v</ta>
            <ta e="T29" id="Seg_1227" s="T28">n</ta>
            <ta e="T30" id="Seg_1228" s="T29">v</ta>
            <ta e="T31" id="Seg_1229" s="T30">n</ta>
            <ta e="T32" id="Seg_1230" s="T31">n</ta>
            <ta e="T33" id="Seg_1231" s="T32">adv</ta>
            <ta e="T34" id="Seg_1232" s="T33">n</ta>
            <ta e="T35" id="Seg_1233" s="T34">v</ta>
            <ta e="T36" id="Seg_1234" s="T35">quant</ta>
            <ta e="T37" id="Seg_1235" s="T36">n</ta>
            <ta e="T38" id="Seg_1236" s="T37">quant</ta>
            <ta e="T39" id="Seg_1237" s="T38">n</ta>
            <ta e="T40" id="Seg_1238" s="T39">pers</ta>
            <ta e="T41" id="Seg_1239" s="T40">adv</ta>
            <ta e="T42" id="Seg_1240" s="T41">adv</ta>
            <ta e="T43" id="Seg_1241" s="T42">v</ta>
            <ta e="T44" id="Seg_1242" s="T43">pers</ta>
            <ta e="T45" id="Seg_1243" s="T44">v</ta>
            <ta e="T46" id="Seg_1244" s="T45">conj</ta>
            <ta e="T47" id="Seg_1245" s="T46">n</ta>
            <ta e="T48" id="Seg_1246" s="T47">n</ta>
            <ta e="T49" id="Seg_1247" s="T48">v</ta>
            <ta e="T50" id="Seg_1248" s="T49">n</ta>
            <ta e="T51" id="Seg_1249" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1250" s="T51">v</ta>
            <ta e="T53" id="Seg_1251" s="T52">n</ta>
            <ta e="T54" id="Seg_1252" s="T53">v</ta>
            <ta e="T55" id="Seg_1253" s="T54">adv</ta>
            <ta e="T56" id="Seg_1254" s="T55">n</ta>
            <ta e="T57" id="Seg_1255" s="T56">v</ta>
            <ta e="T58" id="Seg_1256" s="T57">nprop</ta>
            <ta e="T59" id="Seg_1257" s="T58">v</ta>
            <ta e="T60" id="Seg_1258" s="T59">n</ta>
            <ta e="T61" id="Seg_1259" s="T60">preverb</ta>
            <ta e="T62" id="Seg_1260" s="T61">v</ta>
            <ta e="T63" id="Seg_1261" s="T62">n</ta>
            <ta e="T64" id="Seg_1262" s="T63">preverb</ta>
            <ta e="T65" id="Seg_1263" s="T64">v</ta>
            <ta e="T66" id="Seg_1264" s="T65">adv</ta>
            <ta e="T67" id="Seg_1265" s="T66">adv</ta>
            <ta e="T68" id="Seg_1266" s="T67">v</ta>
            <ta e="T69" id="Seg_1267" s="T68">conj</ta>
            <ta e="T70" id="Seg_1268" s="T69">v</ta>
            <ta e="T71" id="Seg_1269" s="T70">adj</ta>
            <ta e="T72" id="Seg_1270" s="T71">n</ta>
            <ta e="T73" id="Seg_1271" s="T72">v</ta>
            <ta e="T74" id="Seg_1272" s="T73">n</ta>
            <ta e="T75" id="Seg_1273" s="T74">n</ta>
            <ta e="T76" id="Seg_1274" s="T75">v</ta>
            <ta e="T77" id="Seg_1275" s="T76">v</ta>
            <ta e="T78" id="Seg_1276" s="T77">n</ta>
            <ta e="T79" id="Seg_1277" s="T78">v</ta>
            <ta e="T80" id="Seg_1278" s="T79">conj</ta>
            <ta e="T82" id="Seg_1279" s="T81">n</ta>
            <ta e="T83" id="Seg_1280" s="T82">v</ta>
            <ta e="T84" id="Seg_1281" s="T83">n</ta>
            <ta e="T85" id="Seg_1282" s="T84">v</ta>
            <ta e="T86" id="Seg_1283" s="T85">n</ta>
            <ta e="T87" id="Seg_1284" s="T86">conj</ta>
            <ta e="T88" id="Seg_1285" s="T87">n</ta>
            <ta e="T89" id="Seg_1286" s="T88">ptcp</ta>
            <ta e="T90" id="Seg_1287" s="T89">n</ta>
            <ta e="T91" id="Seg_1288" s="T90">n</ta>
            <ta e="T92" id="Seg_1289" s="T91">n</ta>
            <ta e="T93" id="Seg_1290" s="T92">v</ta>
            <ta e="T94" id="Seg_1291" s="T93">n</ta>
            <ta e="T95" id="Seg_1292" s="T94">v</ta>
            <ta e="T96" id="Seg_1293" s="T95">v</ta>
            <ta e="T97" id="Seg_1294" s="T96">n</ta>
            <ta e="T98" id="Seg_1295" s="T97">v</ta>
            <ta e="T99" id="Seg_1296" s="T98">n</ta>
            <ta e="T100" id="Seg_1297" s="T99">v</ta>
            <ta e="T101" id="Seg_1298" s="T100">n</ta>
            <ta e="T102" id="Seg_1299" s="T101">v</ta>
            <ta e="T103" id="Seg_1300" s="T102">n</ta>
            <ta e="T104" id="Seg_1301" s="T103">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1302" s="T1">np.h:P</ta>
            <ta e="T6" id="Seg_1303" s="T5">0.3.h:P</ta>
            <ta e="T9" id="Seg_1304" s="T8">0.3.h:A</ta>
            <ta e="T14" id="Seg_1305" s="T13">np:A</ta>
            <ta e="T15" id="Seg_1306" s="T14">np:G</ta>
            <ta e="T18" id="Seg_1307" s="T17">np:Th</ta>
            <ta e="T23" id="Seg_1308" s="T22">np:Th</ta>
            <ta e="T24" id="Seg_1309" s="T23">0.3.h:E</ta>
            <ta e="T27" id="Seg_1310" s="T26">np:Th</ta>
            <ta e="T29" id="Seg_1311" s="T28">np:Time</ta>
            <ta e="T30" id="Seg_1312" s="T29">0.3.h:E</ta>
            <ta e="T31" id="Seg_1313" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1314" s="T31">np:Com</ta>
            <ta e="T33" id="Seg_1315" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_1316" s="T33">np:A</ta>
            <ta e="T37" id="Seg_1317" s="T36">np:Ins</ta>
            <ta e="T39" id="Seg_1318" s="T38">np:P</ta>
            <ta e="T40" id="Seg_1319" s="T39">pro.h:Th</ta>
            <ta e="T44" id="Seg_1320" s="T43">pro.h:E</ta>
            <ta e="T47" id="Seg_1321" s="T46">np.h:A 0.3.h:Poss</ta>
            <ta e="T48" id="Seg_1322" s="T47">np:Th</ta>
            <ta e="T50" id="Seg_1323" s="T49">np:Th</ta>
            <ta e="T52" id="Seg_1324" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_1325" s="T52">np:G</ta>
            <ta e="T56" id="Seg_1326" s="T55">np:Th</ta>
            <ta e="T58" id="Seg_1327" s="T57">np.h:A</ta>
            <ta e="T60" id="Seg_1328" s="T59">np:So</ta>
            <ta e="T62" id="Seg_1329" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_1330" s="T62">np:Th</ta>
            <ta e="T65" id="Seg_1331" s="T64">0.3.h:A</ta>
            <ta e="T68" id="Seg_1332" s="T67">0.3.h:A</ta>
            <ta e="T70" id="Seg_1333" s="T69">0.3.h:A</ta>
            <ta e="T72" id="Seg_1334" s="T71">np:Th</ta>
            <ta e="T73" id="Seg_1335" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_1336" s="T73">np:Th</ta>
            <ta e="T75" id="Seg_1337" s="T74">np:Th</ta>
            <ta e="T77" id="Seg_1338" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_1339" s="T77">np:Th 0.3.h:Poss</ta>
            <ta e="T79" id="Seg_1340" s="T78">0.3.h:A</ta>
            <ta e="T82" id="Seg_1341" s="T81">np:Ins</ta>
            <ta e="T83" id="Seg_1342" s="T82">0.3.h:A</ta>
            <ta e="T86" id="Seg_1343" s="T85">np:Th 0.3.h:Poss</ta>
            <ta e="T88" id="Seg_1344" s="T87">np:Th 0.3.h:Poss</ta>
            <ta e="T91" id="Seg_1345" s="T90">np:P 0.1.h:Poss</ta>
            <ta e="T94" id="Seg_1346" s="T93">np:E 0.1.h:Poss</ta>
            <ta e="T97" id="Seg_1347" s="T96">np:A 0.1.h:Poss</ta>
            <ta e="T99" id="Seg_1348" s="T98">np:P 0.1.h:Poss</ta>
            <ta e="T101" id="Seg_1349" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_1350" s="T101">0.3.h:A</ta>
            <ta e="T103" id="Seg_1351" s="T102">np:Ins</ta>
            <ta e="T104" id="Seg_1352" s="T103">np:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1353" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1354" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1355" s="T4">s:temp</ta>
            <ta e="T6" id="Seg_1356" s="T5">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_1357" s="T8">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1358" s="T13">np:S</ta>
            <ta e="T16" id="Seg_1359" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_1360" s="T17">np:S</ta>
            <ta e="T19" id="Seg_1361" s="T18">cop</ta>
            <ta e="T22" id="Seg_1362" s="T21">n:pred</ta>
            <ta e="T23" id="Seg_1363" s="T22">np:O</ta>
            <ta e="T24" id="Seg_1364" s="T23">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_1365" s="T26">np:S</ta>
            <ta e="T28" id="Seg_1366" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1367" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_1368" s="T30">np:O</ta>
            <ta e="T34" id="Seg_1369" s="T33">np:S</ta>
            <ta e="T35" id="Seg_1370" s="T34">v:pred</ta>
            <ta e="T40" id="Seg_1371" s="T39">pro.h:S</ta>
            <ta e="T43" id="Seg_1372" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_1373" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_1374" s="T44">v:pred</ta>
            <ta e="T49" id="Seg_1375" s="T45">s:compl</ta>
            <ta e="T50" id="Seg_1376" s="T49">np:O</ta>
            <ta e="T52" id="Seg_1377" s="T51">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_1378" s="T53">s:purp</ta>
            <ta e="T56" id="Seg_1379" s="T55">np:S</ta>
            <ta e="T57" id="Seg_1380" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_1381" s="T57">np.h:S</ta>
            <ta e="T59" id="Seg_1382" s="T58">v:pred</ta>
            <ta e="T62" id="Seg_1383" s="T61">0.3.h:S v:pred</ta>
            <ta e="T63" id="Seg_1384" s="T62">np:O</ta>
            <ta e="T65" id="Seg_1385" s="T64">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_1386" s="T67">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1387" s="T69">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_1388" s="T71">np:O</ta>
            <ta e="T73" id="Seg_1389" s="T72">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_1390" s="T73">s:purp</ta>
            <ta e="T77" id="Seg_1391" s="T76">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_1392" s="T77">np:O</ta>
            <ta e="T79" id="Seg_1393" s="T78">0.3.h:S v:pred</ta>
            <ta e="T83" id="Seg_1394" s="T82">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_1395" s="T83">s:purp</ta>
            <ta e="T91" id="Seg_1396" s="T90">np:S</ta>
            <ta e="T93" id="Seg_1397" s="T92">v:pred</ta>
            <ta e="T94" id="Seg_1398" s="T93">np:S</ta>
            <ta e="T95" id="Seg_1399" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_1400" s="T96">np:S</ta>
            <ta e="T98" id="Seg_1401" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_1402" s="T98">np:S</ta>
            <ta e="T100" id="Seg_1403" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_1404" s="T100">np:O</ta>
            <ta e="T102" id="Seg_1405" s="T101">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_1406" s="T6">RUS:gram</ta>
            <ta e="T8" id="Seg_1407" s="T7">RUS:disc</ta>
            <ta e="T20" id="Seg_1408" s="T19">RUS:gram</ta>
            <ta e="T36" id="Seg_1409" s="T35">RUS:core</ta>
            <ta e="T38" id="Seg_1410" s="T37">RUS:core</ta>
            <ta e="T46" id="Seg_1411" s="T45">RUS:gram</ta>
            <ta e="T55" id="Seg_1412" s="T54">RUS:cult</ta>
            <ta e="T63" id="Seg_1413" s="T62">RUS:cult</ta>
            <ta e="T69" id="Seg_1414" s="T68">RUS:gram</ta>
            <ta e="T80" id="Seg_1415" s="T79">RUS:gram</ta>
            <ta e="T87" id="Seg_1416" s="T86">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1417" s="T1">Zina woke up.</ta>
            <ta e="T9" id="Seg_1418" s="T4">She woke up laughing.</ta>
            <ta e="T12" id="Seg_1419" s="T9">Why would she laugh.</ta>
            <ta e="T16" id="Seg_1420" s="T12">The previous day comes to her mind.</ta>
            <ta e="T22" id="Seg_1421" s="T16">The day yesterday was like the days earlier.</ta>
            <ta e="T28" id="Seg_1422" s="T22">She didn't have a good dream.</ta>
            <ta e="T33" id="Seg_1423" s="T28">She dreamed of a river full of water in spring.</ta>
            <ta e="T39" id="Seg_1424" s="T33">The water overflowed everything, all fields.</ta>
            <ta e="T43" id="Seg_1425" s="T39">She slept well.</ta>
            <ta e="T49" id="Seg_1426" s="T43">She heard her mother milking a cow.</ta>
            <ta e="T54" id="Seg_1427" s="T49">The cows were not yet droven out to the fields.</ta>
            <ta e="T57" id="Seg_1428" s="T54">The day was just breaking.</ta>
            <ta e="T65" id="Seg_1429" s="T57">Zina got up, jumped up from her bed, threw away the blanket.</ta>
            <ta e="T70" id="Seg_1430" s="T65">She yawned well and stretched herself.</ta>
            <ta e="T73" id="Seg_1431" s="T70">She put off her sleeveless shirt.</ta>
            <ta e="T77" id="Seg_1432" s="T73">She went to wash her hands and her face.</ta>
            <ta e="T88" id="Seg_1433" s="T77">She put off her shirt, and went to wash her hands and her face wearing just trousers.</ta>
            <ta e="T93" id="Seg_1434" s="T88">“Cold water, my body (my skin) got cold.</ta>
            <ta e="T96" id="Seg_1435" s="T93">My heart is beating, frightening.</ta>
            <ta e="T98" id="Seg_1436" s="T96">My heart shudders.</ta>
            <ta e="T100" id="Seg_1437" s="T98">My skin is cold.”</ta>
            <ta e="T104" id="Seg_1438" s="T100">She poured water on her back with her hands.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1439" s="T1">Sina wachte auf.</ta>
            <ta e="T9" id="Seg_1440" s="T4">Sie wachte mit einem Lachen auf.</ta>
            <ta e="T12" id="Seg_1441" s="T9">Warum sollte sie lachen.</ta>
            <ta e="T16" id="Seg_1442" s="T12">Der gestrige Tag kam ihr in den Sinn.</ta>
            <ta e="T22" id="Seg_1443" s="T16">Der Tag gestern war wie die vorigen Tage.</ta>
            <ta e="T28" id="Seg_1444" s="T22">Sie hatte keinen guten Traum.</ta>
            <ta e="T33" id="Seg_1445" s="T28">Sie träumte von einem Fluss voller Wasser im Frühling.</ta>
            <ta e="T39" id="Seg_1446" s="T33">Das Wasser überflutete alles, alle Felder.</ta>
            <ta e="T43" id="Seg_1447" s="T39">Sie schlief gut.</ta>
            <ta e="T49" id="Seg_1448" s="T43">Sie hörte ihre Mutter eine Kuh melken.</ta>
            <ta e="T54" id="Seg_1449" s="T49">Man hatte die Kühe noch nicht auf die Felder getrieben.</ta>
            <ta e="T57" id="Seg_1450" s="T54">Der Tag brach gerade an.</ta>
            <ta e="T65" id="Seg_1451" s="T57">Sina stand auf, sprang aus ihrem Bett, warf die Decke beiseite.</ta>
            <ta e="T70" id="Seg_1452" s="T65">Sie gähnte ausgiebig und streckte sich.</ta>
            <ta e="T73" id="Seg_1453" s="T70">Sie zog ihr ärmelloses Hemd aus.</ta>
            <ta e="T77" id="Seg_1454" s="T73">Sie ging, um sich Hände und Gesicht zu waschen.</ta>
            <ta e="T88" id="Seg_1455" s="T77">Sie zog ihr Hemd aus und ging, um ihr Gesicht und ihre Hände nur mit Hose bekleidet zu waschen.</ta>
            <ta e="T93" id="Seg_1456" s="T88">"Kaltes Wasser, mein Körper (meine Haut) ist kalt geworden.</ta>
            <ta e="T96" id="Seg_1457" s="T93">Mein Herz schlägt ängstlich.</ta>
            <ta e="T98" id="Seg_1458" s="T96">Mein Herz zittert.</ta>
            <ta e="T100" id="Seg_1459" s="T98">Meine Haut ist kalt."</ta>
            <ta e="T104" id="Seg_1460" s="T100">Sie goss mit ihren Händen Wasser auf ihren Rücken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1461" s="T1">Зина проснулась.</ta>
            <ta e="T9" id="Seg_1462" s="T4">Только проснулась, а уже смеется.</ta>
            <ta e="T12" id="Seg_1463" s="T9">Чего бы ей смеяться.</ta>
            <ta e="T16" id="Seg_1464" s="T12">Прежний день на ум приходит.</ta>
            <ta e="T22" id="Seg_1465" s="T16">Вчерашний день был как прежние дни.</ta>
            <ta e="T28" id="Seg_1466" s="T22">Сон хороший не приснился.</ta>
            <ta e="T33" id="Seg_1467" s="T28">Во сне она видела реку с водой весной.</ta>
            <ta e="T39" id="Seg_1468" s="T33">Вода затопила всё водой, все луга.</ta>
            <ta e="T43" id="Seg_1469" s="T39">Она хорошо поспала.</ta>
            <ta e="T49" id="Seg_1470" s="T43">Она слышит, что мать корову доит.</ta>
            <ta e="T54" id="Seg_1471" s="T49">Коров не угнали еще на луга кормить.</ta>
            <ta e="T57" id="Seg_1472" s="T54">Только светать начинает (заря начинается).</ta>
            <ta e="T65" id="Seg_1473" s="T57">Зина встала, с кровати соскочила, одеяло сбросила.</ta>
            <ta e="T70" id="Seg_1474" s="T65">Хорошенько зевнула и потянулась.</ta>
            <ta e="T73" id="Seg_1475" s="T70">Рубашку-безрукавку сбросила.</ta>
            <ta e="T77" id="Seg_1476" s="T73">Руки, лицо умывать пошла.</ta>
            <ta e="T88" id="Seg_1477" s="T77">Рубашку скинула, а в одних штанах пошла умываться, руки и лицо.</ta>
            <ta e="T93" id="Seg_1478" s="T88">“Холодная вода, тело (кожу) застудила.</ta>
            <ta e="T96" id="Seg_1479" s="T93">Сердце боится, пугается.</ta>
            <ta e="T98" id="Seg_1480" s="T96">Сердце вздрагивает.</ta>
            <ta e="T100" id="Seg_1481" s="T98">Кожа мерзнет.”</ta>
            <ta e="T104" id="Seg_1482" s="T100">Воду плескала руками на спину.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_1483" s="T1">Зина проснулась</ta>
            <ta e="T9" id="Seg_1484" s="T4">только что разбужается смеется</ta>
            <ta e="T12" id="Seg_1485" s="T9">чего бы ей</ta>
            <ta e="T16" id="Seg_1486" s="T12">ранешняя жизнь на ум приходит</ta>
            <ta e="T22" id="Seg_1487" s="T16">вчерашний день был как старинные (прежние) дни</ta>
            <ta e="T28" id="Seg_1488" s="T22">сон хороший не приснился</ta>
            <ta e="T33" id="Seg_1489" s="T28">во сне видела реку с водой весной</ta>
            <ta e="T39" id="Seg_1490" s="T33">вода затопила все луга</ta>
            <ta e="T43" id="Seg_1491" s="T39">она хорошо поспала</ta>
            <ta e="T49" id="Seg_1492" s="T43">она слышит что мать корову доит</ta>
            <ta e="T54" id="Seg_1493" s="T49">коров не угнали еще на луга кормить</ta>
            <ta e="T57" id="Seg_1494" s="T54">только что светать начинает (заря начинает(ся)</ta>
            <ta e="T65" id="Seg_1495" s="T57">Зина встала с кровати соскочила одеяло сбросила</ta>
            <ta e="T70" id="Seg_1496" s="T65">сладко зевнула и потянулась</ta>
            <ta e="T73" id="Seg_1497" s="T70">безрукавку рубашку выбросила</ta>
            <ta e="T77" id="Seg_1498" s="T73">руки лицо умывать пошла</ta>
            <ta e="T88" id="Seg_1499" s="T77">рубашку скинула а в одних штанах пошла умываться руки и лицо</ta>
            <ta e="T93" id="Seg_1500" s="T88">холодная вода тело мясо (кожу) застудила</ta>
            <ta e="T96" id="Seg_1501" s="T93">сердце боится пугается</ta>
            <ta e="T98" id="Seg_1502" s="T96">сердце вздрагивает</ta>
            <ta e="T100" id="Seg_1503" s="T98">шкура (кожа) мерзнет</ta>
            <ta e="T104" id="Seg_1504" s="T100">воду обливала (плескала) руками на спину</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T16" id="Seg_1505" s="T12">[KuAI:] Variant: 'täːɣɨndə'.</ta>
            <ta e="T22" id="Seg_1506" s="T16">[BrM:] 'delat' – PL?</ta>
            <ta e="T28" id="Seg_1507" s="T22">[BrM:] Tentative analysis of 'küːdəre', 'kudeptɨlʼdikuŋ'.</ta>
            <ta e="T33" id="Seg_1508" s="T28">[BrM:] Tentative analysis of 'küːderʼäɣɨn'.</ta>
            <ta e="T57" id="Seg_1509" s="T54">[BrM:] 'tolʼko što' changed to 'tolʼkošto'. [BrM:] Tentative analysis of 'iːlädʼespa'.</ta>
            <ta e="T73" id="Seg_1510" s="T70">[BrM:] Tentative analysis of 'tʼedʼäčimbat'.</ta>
            <ta e="T93" id="Seg_1511" s="T88">[KuAI:] Variant: 'wadʼöu', 'qobou'. [BrM:] ACC instead of 1SG?</ta>
            <ta e="T98" id="Seg_1512" s="T96">[BrM:] Tentative analysis of 'laɣolɨkkuŋ'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
