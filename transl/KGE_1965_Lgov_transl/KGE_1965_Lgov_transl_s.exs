<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KGE_1965_Lgov_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KGE_1965_Lgov_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">211</ud-information>
            <ud-information attribute-name="# HIAT:w">160</ud-information>
            <ud-information attribute-name="# e">160</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KGE">
            <abbreviation>KGE</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KGE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T160" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Meː</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">qənqolapsäj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Jermolajn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">optı</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kıkʼät</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">qanɨŋmɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Šiːpa</ts>
                  <nts id="Seg_26" n="HIAT:ip">—</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">ɛnɨntɨlʼ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">suːrɨp</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Qanɨqqɨt</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">aša</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">orqɨlʼimpa</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Jesli</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">qaj</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">ɛːmij</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">ilʼmatɨj</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">pakʼäja</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">meː</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">čʼatɨkkɔːmɨt</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">meː</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">kanajiːmɨt</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">aša</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">tatɨkkɔːtɨt</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">naməšej</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">lotɨt</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">puːtoːqɨt</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_94" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">Kanat</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">aša</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">tɛnɨmɨsɔːtɨt</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">uːrqa</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <nts id="Seg_108" n="HIAT:ip">/</nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">uːrsɔːtɨt</ts>
                  <nts id="Seg_111" n="HIAT:ip">)</nts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_115" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">Kanaiːmɨt</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">nʼopɨlʼ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">təttoŋɨt</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">qənqa</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">tačʼalkɔːtɨt</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">täpɨt</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">ola</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">kukčʼaak</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">intälʼiːmtɨt</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">lotɨktɨ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">(</nts>
                  <nts id="Seg_148" n="HIAT:ip">/</nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">qɔːsɨlʼ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">nʼuːtantɨ</ts>
                  <nts id="Seg_154" n="HIAT:ip">)</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">maːtalsɔːtɨt</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_161" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_163" n="HIAT:w" s="T44">Aša</ts>
                  <nts id="Seg_164" n="HIAT:ip">—</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">kətɨsɨtɨ</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">čʼiːpoːqɨntɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">Jermolaj</ts>
                  <nts id="Seg_174" n="HIAT:ip">—</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">ürɨtɨ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">qošk</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">ɛːja</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_187" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_189" n="HIAT:w" s="T51">Nɔːtna</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_192" n="HIAT:w" s="T52">qoqo</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_195" n="HIAT:w" s="T53">alako</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_199" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">Qəllɔːmɨt</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">moqona</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">Lʼgovtɨ</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_211" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">Me</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">qənnɔːmɨt</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_220" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">Meː</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">ašena</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">aša</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">kočʼik</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">qəqɨp</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">meːntɨmɨt</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">meːqɨnit</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">čʼəssa</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">nʼarqɨj</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">markoːqɨnɨ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">qontšɛːjntɨna</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">qoštij</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">nılʼčʼij</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">kanalʼ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_265" n="HIAT:w" s="T73">lʼıːpijana</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_267" n="HIAT:ip">(</nts>
                  <nts id="Seg_268" n="HIAT:ip">/</nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">lʼıːpilʼa</ts>
                  <nts id="Seg_271" n="HIAT:ip">)</nts>
                  <nts id="Seg_272" n="HIAT:ip">,</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_275" n="HIAT:w" s="T75">täpɨn</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_278" n="HIAT:w" s="T76">moqalqɨt</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_281" n="HIAT:w" s="T77">atɔːlisɨ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_284" n="HIAT:w" s="T78">aša</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_287" n="HIAT:w" s="T79">pirqɨ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_290" n="HIAT:w" s="T80">qup</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">patɨj</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">porqɨjatɨ</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_300" n="HIAT:w" s="T83">orsä</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_303" n="HIAT:w" s="T84">nɨttɨmpa</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_307" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">Pimɨtɨ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">nʼärqɨlɔːqɨ</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_315" n="HIAT:w" s="T87">ɛːja</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">säqalpatɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">kutar</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_325" n="HIAT:w" s="T90">qos</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_328" n="HIAT:w" s="T91">küːtɨmtɨ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_331" n="HIAT:w" s="T92">nɨttɨpɨlʼ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_334" n="HIAT:w" s="T93">sapoqɨntɨ</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_338" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_340" n="HIAT:w" s="T94">Soːjqɨntɨ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_343" n="HIAT:w" s="T95">pajqɨlpatɨ</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_346" n="HIAT:w" s="T96">nʼärqɨ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_349" n="HIAT:w" s="T97">qampıjsa</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_353" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_355" n="HIAT:w" s="T98">Ukkur</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_358" n="HIAT:w" s="T99">šʼintʼilʼ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_361" n="HIAT:w" s="T100">puškatɨ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_364" n="HIAT:w" s="T101">moqalqäntɨ</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_367" n="HIAT:w" s="T102">täntalpatɨ</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_371" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_373" n="HIAT:w" s="T103">Meː</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_376" n="HIAT:w" s="T104">kanaiːmɨt</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_379" n="HIAT:w" s="T105">mänɨj</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_382" n="HIAT:w" s="T106">kanap</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_385" n="HIAT:w" s="T107">aptɨrqolʼapsɔːtɨt</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_389" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_391" n="HIAT:w" s="T108">Na</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_394" n="HIAT:w" s="T109">kanak</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_397" n="HIAT:w" s="T110">ɛnɨla</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_400" n="HIAT:w" s="T111">talʼčʼimtɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_403" n="HIAT:w" s="T112">ılla</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_406" n="HIAT:w" s="T113">ɨpasɨtɨ</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_410" n="HIAT:w" s="T114">üŋɨlsamtɨ</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_413" n="HIAT:w" s="T115">pɨrnʼaltɨmpɨsɨtɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_416" n="HIAT:w" s="T116">aj</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T117">čʼäkaptɨlʼa</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_422" n="HIAT:w" s="T118">paktɨrɨsɨ</ts>
                  <nts id="Seg_423" n="HIAT:ip">,</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_426" n="HIAT:w" s="T119">ɨrɨmpɨla</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_430" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_432" n="HIAT:w" s="T120">Mänɨj</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_435" n="HIAT:w" s="T121">qup</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_438" n="HIAT:w" s="T122">meːsʼa</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_441" n="HIAT:w" s="T123">qəntɨlʼa</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_444" n="HIAT:w" s="T124">toroattɨsɨ</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_448" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_450" n="HIAT:w" s="T125">Täpɨnɨk</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_453" n="HIAT:w" s="T126">kurasqɨntɨ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_456" n="HIAT:w" s="T127">ɛːnta</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_459" n="HIAT:w" s="T128">šitsar</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_462" n="HIAT:w" s="T129">sompɨla</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_465" n="HIAT:w" s="T130">poːtɨ</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_469" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_471" n="HIAT:w" s="T131">Täpɨn</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_474" n="HIAT:w" s="T132">čʼumpɨ</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_477" n="HIAT:w" s="T133">säːqɨ</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_480" n="HIAT:w" s="T134">oːptɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_483" n="HIAT:w" s="T135">čʼırɨmpa</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <nts id="Seg_486" n="HIAT:ip">/</nts>
                  <ts e="T137" id="Seg_488" n="HIAT:w" s="T136">čʼırɨmpɨsɔːtɨt</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_492" n="HIAT:w" s="T137">nʼelʼčʼɨnɨlʼa</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_496" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_498" n="HIAT:w" s="T138">Aša</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_501" n="HIAT:w" s="T139">märqa</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_504" n="HIAT:w" s="T140">säːqɨ</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_507" n="HIAT:w" s="T141">sajlʼaqıtɨ</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_509" n="HIAT:ip">(</nts>
                  <nts id="Seg_510" n="HIAT:ip">/</nts>
                  <ts e="T143" id="Seg_512" n="HIAT:w" s="T142">sajaiːtɨ</ts>
                  <nts id="Seg_513" n="HIAT:ip">)</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_516" n="HIAT:w" s="T143">ukkɨrtɨk</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_519" n="HIAT:w" s="T144">rɨpčʼimpɔːqıj</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_523" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_525" n="HIAT:w" s="T145">Muqɨlʼtɨːrij</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_528" n="HIAT:w" s="T146">mɨnnɨmtɨ</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_531" n="HIAT:w" s="T147">sɔːrɨmpatɨ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_534" n="HIAT:w" s="T148">säːqɨ</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_537" n="HIAT:w" s="T149">qampısä</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_540" n="HIAT:w" s="T150">mitə</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_543" n="HIAT:w" s="T151">mol</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_546" n="HIAT:w" s="T152">tiːmitɨ</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_549" n="HIAT:w" s="T153">čʼüšintɨ</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_551" n="HIAT:ip">(</nts>
                  <nts id="Seg_552" n="HIAT:ip">/</nts>
                  <ts e="T155" id="Seg_554" n="HIAT:w" s="T154">čʼüšimpa</ts>
                  <nts id="Seg_555" n="HIAT:ip">)</nts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_559" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_561" n="HIAT:w" s="T155">Ontɨ</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_564" n="HIAT:w" s="T156">meːqanɨt</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_567" n="HIAT:w" s="T157">nılʼčʼik</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_570" n="HIAT:w" s="T158">ɔːntalpɨlʼa</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_573" n="HIAT:w" s="T159">pisɨnʼnʼi</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T160" id="Seg_576" n="sc" s="T0">
               <ts e="T1" id="Seg_578" n="e" s="T0">Meː </ts>
               <ts e="T2" id="Seg_580" n="e" s="T1">qənqolapsäj </ts>
               <ts e="T3" id="Seg_582" n="e" s="T2">Jermolajn </ts>
               <ts e="T4" id="Seg_584" n="e" s="T3">optı </ts>
               <ts e="T5" id="Seg_586" n="e" s="T4">kıkʼät </ts>
               <ts e="T6" id="Seg_588" n="e" s="T5">qanɨŋmɨt. </ts>
               <ts e="T7" id="Seg_590" n="e" s="T6">Šiːpa— </ts>
               <ts e="T8" id="Seg_592" n="e" s="T7">ɛnɨntɨlʼ </ts>
               <ts e="T9" id="Seg_594" n="e" s="T8">suːrɨp. </ts>
               <ts e="T10" id="Seg_596" n="e" s="T9">Qanɨqqɨt </ts>
               <ts e="T11" id="Seg_598" n="e" s="T10">aša </ts>
               <ts e="T12" id="Seg_600" n="e" s="T11">orqɨlʼimpa. </ts>
               <ts e="T13" id="Seg_602" n="e" s="T12">Jesli </ts>
               <ts e="T14" id="Seg_604" n="e" s="T13">qaj </ts>
               <ts e="T15" id="Seg_606" n="e" s="T14">ɛːmij </ts>
               <ts e="T16" id="Seg_608" n="e" s="T15">ilʼmatɨj </ts>
               <ts e="T17" id="Seg_610" n="e" s="T16">pakʼäja </ts>
               <ts e="T18" id="Seg_612" n="e" s="T17">meː </ts>
               <ts e="T19" id="Seg_614" n="e" s="T18">čʼatɨkkɔːmɨt, </ts>
               <ts e="T20" id="Seg_616" n="e" s="T19">meː </ts>
               <ts e="T21" id="Seg_618" n="e" s="T20">kanajiːmɨt </ts>
               <ts e="T22" id="Seg_620" n="e" s="T21">aša </ts>
               <ts e="T23" id="Seg_622" n="e" s="T22">tatɨkkɔːtɨt </ts>
               <ts e="T24" id="Seg_624" n="e" s="T23">naməšej </ts>
               <ts e="T25" id="Seg_626" n="e" s="T24">lotɨt </ts>
               <ts e="T26" id="Seg_628" n="e" s="T25">puːtoːqɨt. </ts>
               <ts e="T27" id="Seg_630" n="e" s="T26">Kanat </ts>
               <ts e="T28" id="Seg_632" n="e" s="T27">aša </ts>
               <ts e="T29" id="Seg_634" n="e" s="T28">tɛnɨmɨsɔːtɨt </ts>
               <ts e="T30" id="Seg_636" n="e" s="T29">uːrqa </ts>
               <ts e="T31" id="Seg_638" n="e" s="T30">(/uːrsɔːtɨt). </ts>
               <ts e="T32" id="Seg_640" n="e" s="T31">Kanaiːmɨt </ts>
               <ts e="T33" id="Seg_642" n="e" s="T32">nʼopɨlʼ </ts>
               <ts e="T34" id="Seg_644" n="e" s="T33">təttoŋɨt </ts>
               <ts e="T35" id="Seg_646" n="e" s="T34">qənqa </ts>
               <ts e="T36" id="Seg_648" n="e" s="T35">tačʼalkɔːtɨt, </ts>
               <ts e="T37" id="Seg_650" n="e" s="T36">täpɨt </ts>
               <ts e="T38" id="Seg_652" n="e" s="T37">ola </ts>
               <ts e="T39" id="Seg_654" n="e" s="T38">kukčʼaak </ts>
               <ts e="T40" id="Seg_656" n="e" s="T39">intälʼiːmtɨt </ts>
               <ts e="T41" id="Seg_658" n="e" s="T40">lotɨktɨ </ts>
               <ts e="T42" id="Seg_660" n="e" s="T41">(/qɔːsɨlʼ </ts>
               <ts e="T43" id="Seg_662" n="e" s="T42">nʼuːtantɨ) </ts>
               <ts e="T44" id="Seg_664" n="e" s="T43">maːtalsɔːtɨt. </ts>
               <ts e="T45" id="Seg_666" n="e" s="T44">Aša— </ts>
               <ts e="T46" id="Seg_668" n="e" s="T45">kətɨsɨtɨ </ts>
               <ts e="T47" id="Seg_670" n="e" s="T46">čʼiːpoːqɨntɨ </ts>
               <ts e="T48" id="Seg_672" n="e" s="T47">Jermolaj— </ts>
               <ts e="T49" id="Seg_674" n="e" s="T48">ürɨtɨ </ts>
               <ts e="T50" id="Seg_676" n="e" s="T49">qošk </ts>
               <ts e="T51" id="Seg_678" n="e" s="T50">ɛːja. </ts>
               <ts e="T52" id="Seg_680" n="e" s="T51">Nɔːtna </ts>
               <ts e="T53" id="Seg_682" n="e" s="T52">qoqo </ts>
               <ts e="T54" id="Seg_684" n="e" s="T53">alako. </ts>
               <ts e="T55" id="Seg_686" n="e" s="T54">Qəllɔːmɨt </ts>
               <ts e="T56" id="Seg_688" n="e" s="T55">moqona </ts>
               <ts e="T57" id="Seg_690" n="e" s="T56">Lʼgovtɨ. </ts>
               <ts e="T58" id="Seg_692" n="e" s="T57">Me </ts>
               <ts e="T59" id="Seg_694" n="e" s="T58">qənnɔːmɨt. </ts>
               <ts e="T60" id="Seg_696" n="e" s="T59">Meː </ts>
               <ts e="T61" id="Seg_698" n="e" s="T60">ašena </ts>
               <ts e="T62" id="Seg_700" n="e" s="T61">aša </ts>
               <ts e="T63" id="Seg_702" n="e" s="T62">kočʼik </ts>
               <ts e="T64" id="Seg_704" n="e" s="T63">qəqɨp </ts>
               <ts e="T65" id="Seg_706" n="e" s="T64">meːntɨmɨt, </ts>
               <ts e="T66" id="Seg_708" n="e" s="T65">meːqɨnit </ts>
               <ts e="T67" id="Seg_710" n="e" s="T66">čʼəssa </ts>
               <ts e="T68" id="Seg_712" n="e" s="T67">nʼarqɨj </ts>
               <ts e="T69" id="Seg_714" n="e" s="T68">markoːqɨnɨ </ts>
               <ts e="T70" id="Seg_716" n="e" s="T69">qontšɛːjntɨna </ts>
               <ts e="T71" id="Seg_718" n="e" s="T70">qoštij </ts>
               <ts e="T72" id="Seg_720" n="e" s="T71">nılʼčʼij </ts>
               <ts e="T73" id="Seg_722" n="e" s="T72">kanalʼ </ts>
               <ts e="T74" id="Seg_724" n="e" s="T73">lʼıːpijana </ts>
               <ts e="T75" id="Seg_726" n="e" s="T74">(/lʼıːpilʼa), </ts>
               <ts e="T76" id="Seg_728" n="e" s="T75">täpɨn </ts>
               <ts e="T77" id="Seg_730" n="e" s="T76">moqalqɨt </ts>
               <ts e="T78" id="Seg_732" n="e" s="T77">atɔːlisɨ </ts>
               <ts e="T79" id="Seg_734" n="e" s="T78">aša </ts>
               <ts e="T80" id="Seg_736" n="e" s="T79">pirqɨ </ts>
               <ts e="T81" id="Seg_738" n="e" s="T80">qup, </ts>
               <ts e="T82" id="Seg_740" n="e" s="T81">patɨj </ts>
               <ts e="T83" id="Seg_742" n="e" s="T82">porqɨjatɨ </ts>
               <ts e="T84" id="Seg_744" n="e" s="T83">orsä </ts>
               <ts e="T85" id="Seg_746" n="e" s="T84">nɨttɨmpa. </ts>
               <ts e="T86" id="Seg_748" n="e" s="T85">Pimɨtɨ </ts>
               <ts e="T87" id="Seg_750" n="e" s="T86">nʼärqɨlɔːqɨ </ts>
               <ts e="T88" id="Seg_752" n="e" s="T87">ɛːja, </ts>
               <ts e="T89" id="Seg_754" n="e" s="T88">säqalpatɨ </ts>
               <ts e="T90" id="Seg_756" n="e" s="T89">kutar </ts>
               <ts e="T91" id="Seg_758" n="e" s="T90">qos </ts>
               <ts e="T92" id="Seg_760" n="e" s="T91">küːtɨmtɨ </ts>
               <ts e="T93" id="Seg_762" n="e" s="T92">nɨttɨpɨlʼ </ts>
               <ts e="T94" id="Seg_764" n="e" s="T93">sapoqɨntɨ. </ts>
               <ts e="T95" id="Seg_766" n="e" s="T94">Soːjqɨntɨ </ts>
               <ts e="T96" id="Seg_768" n="e" s="T95">pajqɨlpatɨ </ts>
               <ts e="T97" id="Seg_770" n="e" s="T96">nʼärqɨ </ts>
               <ts e="T98" id="Seg_772" n="e" s="T97">qampıjsa. </ts>
               <ts e="T99" id="Seg_774" n="e" s="T98">Ukkur </ts>
               <ts e="T100" id="Seg_776" n="e" s="T99">šʼintʼilʼ </ts>
               <ts e="T101" id="Seg_778" n="e" s="T100">puškatɨ </ts>
               <ts e="T102" id="Seg_780" n="e" s="T101">moqalqäntɨ </ts>
               <ts e="T103" id="Seg_782" n="e" s="T102">täntalpatɨ. </ts>
               <ts e="T104" id="Seg_784" n="e" s="T103">Meː </ts>
               <ts e="T105" id="Seg_786" n="e" s="T104">kanaiːmɨt </ts>
               <ts e="T106" id="Seg_788" n="e" s="T105">mänɨj </ts>
               <ts e="T107" id="Seg_790" n="e" s="T106">kanap </ts>
               <ts e="T108" id="Seg_792" n="e" s="T107">aptɨrqolʼapsɔːtɨt. </ts>
               <ts e="T109" id="Seg_794" n="e" s="T108">Na </ts>
               <ts e="T110" id="Seg_796" n="e" s="T109">kanak </ts>
               <ts e="T111" id="Seg_798" n="e" s="T110">ɛnɨla </ts>
               <ts e="T112" id="Seg_800" n="e" s="T111">talʼčʼimtɨ </ts>
               <ts e="T113" id="Seg_802" n="e" s="T112">ılla </ts>
               <ts e="T114" id="Seg_804" n="e" s="T113">ɨpasɨtɨ, </ts>
               <ts e="T115" id="Seg_806" n="e" s="T114">üŋɨlsamtɨ </ts>
               <ts e="T116" id="Seg_808" n="e" s="T115">pɨrnʼaltɨmpɨsɨtɨ </ts>
               <ts e="T117" id="Seg_810" n="e" s="T116">aj </ts>
               <ts e="T118" id="Seg_812" n="e" s="T117">čʼäkaptɨlʼa </ts>
               <ts e="T119" id="Seg_814" n="e" s="T118">paktɨrɨsɨ, </ts>
               <ts e="T120" id="Seg_816" n="e" s="T119">ɨrɨmpɨla. </ts>
               <ts e="T121" id="Seg_818" n="e" s="T120">Mänɨj </ts>
               <ts e="T122" id="Seg_820" n="e" s="T121">qup </ts>
               <ts e="T123" id="Seg_822" n="e" s="T122">meːsʼa </ts>
               <ts e="T124" id="Seg_824" n="e" s="T123">qəntɨlʼa </ts>
               <ts e="T125" id="Seg_826" n="e" s="T124">toroattɨsɨ. </ts>
               <ts e="T126" id="Seg_828" n="e" s="T125">Täpɨnɨk </ts>
               <ts e="T127" id="Seg_830" n="e" s="T126">kurasqɨntɨ </ts>
               <ts e="T128" id="Seg_832" n="e" s="T127">ɛːnta </ts>
               <ts e="T129" id="Seg_834" n="e" s="T128">šitsar </ts>
               <ts e="T130" id="Seg_836" n="e" s="T129">sompɨla </ts>
               <ts e="T131" id="Seg_838" n="e" s="T130">poːtɨ. </ts>
               <ts e="T132" id="Seg_840" n="e" s="T131">Täpɨn </ts>
               <ts e="T133" id="Seg_842" n="e" s="T132">čʼumpɨ </ts>
               <ts e="T134" id="Seg_844" n="e" s="T133">säːqɨ </ts>
               <ts e="T135" id="Seg_846" n="e" s="T134">oːptɨ </ts>
               <ts e="T136" id="Seg_848" n="e" s="T135">čʼırɨmpa </ts>
               <ts e="T137" id="Seg_850" n="e" s="T136">(/čʼırɨmpɨsɔːtɨt) </ts>
               <ts e="T138" id="Seg_852" n="e" s="T137">nʼelʼčʼɨnɨlʼa. </ts>
               <ts e="T139" id="Seg_854" n="e" s="T138">Aša </ts>
               <ts e="T140" id="Seg_856" n="e" s="T139">märqa </ts>
               <ts e="T141" id="Seg_858" n="e" s="T140">säːqɨ </ts>
               <ts e="T142" id="Seg_860" n="e" s="T141">sajlʼaqıtɨ </ts>
               <ts e="T143" id="Seg_862" n="e" s="T142">(/sajaiːtɨ) </ts>
               <ts e="T144" id="Seg_864" n="e" s="T143">ukkɨrtɨk </ts>
               <ts e="T145" id="Seg_866" n="e" s="T144">rɨpčʼimpɔːqıj. </ts>
               <ts e="T146" id="Seg_868" n="e" s="T145">Muqɨlʼtɨːrij </ts>
               <ts e="T147" id="Seg_870" n="e" s="T146">mɨnnɨmtɨ </ts>
               <ts e="T148" id="Seg_872" n="e" s="T147">sɔːrɨmpatɨ </ts>
               <ts e="T149" id="Seg_874" n="e" s="T148">säːqɨ </ts>
               <ts e="T150" id="Seg_876" n="e" s="T149">qampısä </ts>
               <ts e="T151" id="Seg_878" n="e" s="T150">mitə </ts>
               <ts e="T152" id="Seg_880" n="e" s="T151">mol </ts>
               <ts e="T153" id="Seg_882" n="e" s="T152">tiːmitɨ </ts>
               <ts e="T154" id="Seg_884" n="e" s="T153">čʼüšintɨ </ts>
               <ts e="T155" id="Seg_886" n="e" s="T154">(/čʼüšimpa). </ts>
               <ts e="T156" id="Seg_888" n="e" s="T155">Ontɨ </ts>
               <ts e="T157" id="Seg_890" n="e" s="T156">meːqanɨt </ts>
               <ts e="T158" id="Seg_892" n="e" s="T157">nılʼčʼik </ts>
               <ts e="T159" id="Seg_894" n="e" s="T158">ɔːntalpɨlʼa </ts>
               <ts e="T160" id="Seg_896" n="e" s="T159">pisɨnʼnʼi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_897" s="T0">KGE_1965_Lgov_transl.001 (001.001)</ta>
            <ta e="T9" id="Seg_898" s="T6">KGE_1965_Lgov_transl.002 (001.002)</ta>
            <ta e="T12" id="Seg_899" s="T9">KGE_1965_Lgov_transl.003 (001.003)</ta>
            <ta e="T26" id="Seg_900" s="T12">KGE_1965_Lgov_transl.004 (001.004)</ta>
            <ta e="T31" id="Seg_901" s="T26">KGE_1965_Lgov_transl.005 (001.005)</ta>
            <ta e="T44" id="Seg_902" s="T31">KGE_1965_Lgov_transl.006 (001.006)</ta>
            <ta e="T51" id="Seg_903" s="T44">KGE_1965_Lgov_transl.007 (001.007)</ta>
            <ta e="T54" id="Seg_904" s="T51">KGE_1965_Lgov_transl.008 (001.008)</ta>
            <ta e="T57" id="Seg_905" s="T54">KGE_1965_Lgov_transl.009 (001.009)</ta>
            <ta e="T59" id="Seg_906" s="T57">KGE_1965_Lgov_transl.010 (001.010)</ta>
            <ta e="T85" id="Seg_907" s="T59">KGE_1965_Lgov_transl.011 (001.011)</ta>
            <ta e="T94" id="Seg_908" s="T85">KGE_1965_Lgov_transl.012 (001.012)</ta>
            <ta e="T98" id="Seg_909" s="T94">KGE_1965_Lgov_transl.013 (001.013)</ta>
            <ta e="T103" id="Seg_910" s="T98">KGE_1965_Lgov_transl.014 (001.014)</ta>
            <ta e="T108" id="Seg_911" s="T103">KGE_1965_Lgov_transl.015 (002.001)</ta>
            <ta e="T120" id="Seg_912" s="T108">KGE_1965_Lgov_transl.016 (002.002)</ta>
            <ta e="T125" id="Seg_913" s="T120">KGE_1965_Lgov_transl.017 (002.003)</ta>
            <ta e="T131" id="Seg_914" s="T125">KGE_1965_Lgov_transl.018 (002.004)</ta>
            <ta e="T138" id="Seg_915" s="T131">KGE_1965_Lgov_transl.019 (002.005)</ta>
            <ta e="T145" id="Seg_916" s="T138">KGE_1965_Lgov_transl.020 (002.006)</ta>
            <ta e="T155" id="Seg_917" s="T145">KGE_1965_Lgov_transl.021 (002.007)</ta>
            <ta e="T160" id="Seg_918" s="T155">KGE_1965_Lgov_transl.022 (002.008)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_919" s="T0">ме ′kы[ъ̊]ңо‵лапсӓй Ермо′лайнопты ки′кʼӓт ′kаныңмыт.</ta>
            <ta e="T9" id="Seg_920" s="T6">′шʼипа ′енынтыl ′суры[у]п.</ta>
            <ta e="T12" id="Seg_921" s="T9">′kаныkыт ′ашʼа ′орkы‵лʼимпа [′орГыl′импа].</ta>
            <ta e="T26" id="Seg_922" s="T12">′jесли ′kай ′емий ′иlматый ′паkʼеjа ме ′чаты‵kомыт ме ′канай′имыт ′аша ′таты‵kотыт ′намъшей ′лотыт ′пӯтоkыт.</ta>
            <ta e="T31" id="Seg_923" s="T26">′канат ′ашʼа (′урсотыт) ′тӓнымысотыт ′урkа.</ta>
            <ta e="T44" id="Seg_924" s="T31">′кана‵имыт ′нʼӧпыl ′тӓ[ъ̊]ттоңыт ′kъ̊̄нkа ′тачалkо̄тыт ′тӓпыт о′lа ′кукчаак ′индӓlимдыт лотыкты [kозыl ′нʼӯтонты] ′ма̄таlсод̂[т]ыт.</ta>
            <ta e="T51" id="Seg_925" s="T44">′ашʼа – ′kъ̊тысыты ′чипоɣынты Ермолай – ′ӱрыты ′kош кеjа. </ta>
            <ta e="T54" id="Seg_926" s="T51">нотна ′kоkо ′аlаkо.</ta>
            <ta e="T57" id="Seg_927" s="T54">′kъ̊llомыт ′моkона лʼгофты.</ta>
            <ta e="T59" id="Seg_928" s="T57">ме kъ̊̄′нномыт.</ta>
            <ta e="T85" id="Seg_929" s="T59">ме ′ашʼена ′ашʼа ′кочик ′kъ̊̄kып ме̄ндымыт[′томыт], ′меk[ɣ]ынит ′чӧсса ′нʼарkый[l] ′марkоɣыны ′kонт′шʼе̄йнтына ′kоштий ′ниlчий ′kанаl ′лʼипиjана (′лʼӣпиlʼа) ′тӓпын ′моkалkыт а̄′толисы ашʼа ′пирkы kуп ′патый ′порɣы′jаты ′орсӓ ′ныттымпа.</ta>
            <ta e="T94" id="Seg_930" s="T85">′пимыты нʼӓрkылоkы ′еjа, ′сӓkалʼ[л]б̂[п]ати[ы] кутар kос ′кӱ̄тымты ′нытыпыl са′поkынт[д]ы.</ta>
            <ta e="T98" id="Seg_931" s="T94">′сойkынты ′пайkылпаты ′нʼӓрkы ′kампийса.</ta>
            <ta e="T103" id="Seg_932" s="T98">′уккур ′шʼиндʼий[l] пуш′каты мо′kаlkӓнты ′тӓнтаlпаты.</ta>
            <ta e="T108" id="Seg_933" s="T103">ме ′кана‵имыт ′мӓный ′канап аптырkоlʼап′сод̂ыт.</ta>
            <ta e="T120" id="Seg_934" s="T108">на ′канак ′ӓныlа ′таlчимты иllа ы′пасыты ′ӱңыlсамты пыр′нʼалтымб̂ысыты ай ′че̄каптылʼа ′паkтырысы, ырымпыла.</ta>
            <ta e="T125" id="Seg_935" s="T120">′мӓный kуп ′ме̄зʼа ′kъ̊ттылʼа тороаттызы.</ta>
            <ta e="T131" id="Seg_936" s="T125">′тӓпынык ку′расkынды ′е̄нта ′шʼитсар ′сомбыла ′поты.</ta>
            <ta e="T138" id="Seg_937" s="T131">′тӓпын ′чумбы ′сӓ̄kы ′опты ′чирымпа [чирымбысотыт] ′нӓлʼчинылʼа.</ta>
            <ta e="T145" id="Seg_938" s="T138">′ашʼа ′мӓрk[ɣ]а ′сӓɣ[k]ы (′саjа′ӣты) сайlаɣыты ′уккыртык ′рыпчʼим′б̂оɣий.</ta>
            <ta e="T155" id="Seg_939" s="T145">′муkылʼ‵тырий ′мыннымты ′со̄рымбаты сӓɣы ′kамписӓ ′митъ[ы]моl ′тӣмиты ′чушинты [чушимпа].</ta>
            <ta e="T160" id="Seg_940" s="T155">′онды ′ме̄kаны[е]т ′ниlчик ′ондаlбылʼа ′писынʼнʼи.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T6" id="Seg_941" s="T0">me qɨŋolapsäj Еrmolajnoptɨ kikʼät qanɨŋmɨt.</ta>
            <ta e="T9" id="Seg_942" s="T6">šʼipa enɨntɨlʼ surɨ[u]p.</ta>
            <ta e="T12" id="Seg_943" s="T9">qanɨqɨt ašʼa orqɨlʼimpa [orГɨlʼimpa].</ta>
            <ta e="T26" id="Seg_944" s="T12">jesli qaj emij ilʼmatɨj paqʼeja me čʼatɨqomɨt me kanajimɨt aša tatɨqotɨt naməšej lotɨt puːtoqɨt.</ta>
            <ta e="T31" id="Seg_945" s="T26">kanat ašʼa (ursotɨt) tänɨmɨsotɨt urqa.</ta>
            <ta e="T44" id="Seg_946" s="T31">kanaimɨt nʼöpɨlʼ tättoŋɨt qəːnqa tačʼalqoːtɨt täpɨt olʼa kukčʼaak indälʼimdɨt lotɨktɨ [qozɨlʼ nʼuːtontɨ] maːtalʼsod̂[t]ɨt.</ta>
            <ta e="T51" id="Seg_947" s="T44">ašʼa – qətɨsɨtɨ čʼipoqɨntɨ Еrmolaj – ürɨtɨ qoš keja.</ta>
            <ta e="T54" id="Seg_948" s="T51">notna qoqo alʼaqo.</ta>
            <ta e="T57" id="Seg_949" s="T54">qəlʼlʼomɨt moqona lʼgoftɨ.</ta>
            <ta e="T59" id="Seg_950" s="T57">me qəːnnomɨt.</ta>
            <ta e="T85" id="Seg_951" s="T59">me ašʼena ašʼa kočʼik qəːqɨp meːndɨmɨt[tomɨt], meq[q]ɨnit čʼössa nʼarqɨj[lʼ] marqoqɨnɨ qontšeːjntɨna qoštij nilʼčʼij qanalʼ lʼipijana (lʼiːpilʼʼa) täpɨn moqalqɨt aːtolisɨ ašʼa pirqɨ qup patɨj porqɨjatɨ orsä nɨttɨmpa.</ta>
            <ta e="T94" id="Seg_952" s="T85">pimɨtɨ nʼärqɨloqɨ eja, säqalʼ[l]p̂[p]ati[ɨ] kutar qos küːtɨmtɨ nɨtɨpɨlʼ sapoqɨnt[d]ɨ.</ta>
            <ta e="T98" id="Seg_953" s="T94">sojqɨntɨ pajqɨlpatɨ nʼärqɨ qampijsa.</ta>
            <ta e="T103" id="Seg_954" s="T98">ukkur šʼindʼij[lʼ] puškatɨ moqalʼqäntɨ täntalʼpatɨ.</ta>
            <ta e="T108" id="Seg_955" s="T103">me kanaimɨt mänɨj kanap aptɨrqolʼʼapsod̂ɨt.</ta>
            <ta e="T120" id="Seg_956" s="T108">na kanak änɨlʼa talʼčʼimtɨ ilʼlʼa ɨpasɨtɨ üŋɨlʼsamtɨ pɨrnʼaltɨmp̂ɨsɨtɨ aj čʼeːkaptɨlʼa paqtɨrɨsɨ, ɨrɨmpɨla.</ta>
            <ta e="T125" id="Seg_957" s="T120">mänɨj qup meːzʼa qəttɨlʼa toroattɨzɨ.</ta>
            <ta e="T131" id="Seg_958" s="T125">täpɨnɨk kurasqɨndɨ eːnta šʼitsar sompɨla potɨ.</ta>
            <ta e="T138" id="Seg_959" s="T131">täpɨn čʼumpɨ säːqɨ optɨ čʼirɨmpa [čʼirɨmpɨsotɨt] nälʼčʼinɨlʼa.</ta>
            <ta e="T145" id="Seg_960" s="T138">aša märq[q]a säq[q]ɨ (sajaiːtɨ) sajlʼaqɨtɨ ukkɨrtɨk rɨpčʼimp̂oqij.</ta>
            <ta e="T155" id="Seg_961" s="T145">muqɨlʼtɨrij mɨnnɨmtɨ soːrɨmpatɨ säqɨ qampisä mitə[ɨ]molʼ tiːmitɨ čʼušintɨ [čʼušimpa].</ta>
            <ta e="T160" id="Seg_962" s="T155">ondɨ meːqanɨ[e]t nilʼčʼik ondalʼpɨlʼa pisɨnʼnʼi.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_963" s="T0">Meː qənqolapsäj Jermolajn optı kıkʼät qanɨŋmɨt. </ta>
            <ta e="T9" id="Seg_964" s="T6">Šiːpa— ɛnɨntɨlʼ suːrɨp. </ta>
            <ta e="T12" id="Seg_965" s="T9">Qanɨqqɨt aša orqɨlʼimpa. </ta>
            <ta e="T26" id="Seg_966" s="T12">Jesli qaj ɛːmij ilʼmatɨj pakʼäja meː čʼatɨkkɔːmɨt, meː kanajiːmɨt aša tatɨkkɔːtɨt naməšej lotɨt puːtoːqɨt. </ta>
            <ta e="T31" id="Seg_967" s="T26">Kanat aša tɛnɨmɨsɔːtɨt uːrqa (/uːrsɔːtɨt). </ta>
            <ta e="T44" id="Seg_968" s="T31">Kanaiːmɨt nʼopɨlʼ təttoŋɨt qənqa tačʼalkɔːtɨt, täpɨt ola kukčʼaak intälʼiːmtɨt lotɨktɨ (/qɔːsɨlʼ nʼuːtantɨ) maːtalsɔːtɨt. </ta>
            <ta e="T51" id="Seg_969" s="T44">Aša— kətɨsɨtɨ čʼiːpoːqɨntɨ Jermolaj— ürɨtɨ qošk ɛːja. </ta>
            <ta e="T54" id="Seg_970" s="T51">Nɔːtna qoqo alako. </ta>
            <ta e="T57" id="Seg_971" s="T54">Qəllɔːmɨt moqona Lʼgovtɨ. </ta>
            <ta e="T59" id="Seg_972" s="T57">Me qənnɔːmɨt. </ta>
            <ta e="T85" id="Seg_973" s="T59">Meː ašena aša kočʼik qəqɨp meːntɨmɨt, meːqɨnit čʼəssa nʼarqɨj markoːqɨnɨ qontšɛːjntɨna qoštij nılʼčʼij kanalʼ lʼıːpijana (/lʼıːpilʼa), täpɨn moqalqɨt atɔːlisɨ aša pirqɨ qup, patɨj porqɨjatɨ orsä nɨttɨmpa. </ta>
            <ta e="T94" id="Seg_974" s="T85">Pimɨtɨ nʼärqɨlɔːqɨ ɛːja, säqalpatɨ kutar qos küːtɨmtɨ nɨttɨpɨlʼ sapoqɨntɨ. </ta>
            <ta e="T98" id="Seg_975" s="T94">Soːjqɨntɨ pajqɨlpatɨ nʼärqɨ qampıjsa. </ta>
            <ta e="T103" id="Seg_976" s="T98">Ukkur šʼintʼilʼ puškatɨ moqalqäntɨ täntalpatɨ. </ta>
            <ta e="T108" id="Seg_977" s="T103">Meː kanaiːmɨt mänɨj kanap aptɨrqolʼapsɔːtɨt. </ta>
            <ta e="T120" id="Seg_978" s="T108">Na kanak ɛnɨla talʼčʼimtɨ ılla ɨpasɨtɨ, üŋɨlsamtɨ pɨrnʼaltɨmpɨsɨtɨ aj čʼäkaptɨlʼa paktɨrɨsɨ, ɨrɨmpɨla. </ta>
            <ta e="T125" id="Seg_979" s="T120">Mänɨj qup meːsʼa qəntɨlʼa toroattɨsɨ. </ta>
            <ta e="T131" id="Seg_980" s="T125">Täpɨnɨk kurasqɨntɨ ɛːnta šitsar sompɨla poːtɨ. </ta>
            <ta e="T138" id="Seg_981" s="T131">Täpɨn čʼumpɨ säːqɨ oːptɨ čʼırɨmpa (/čʼırɨmpɨsɔːtɨt) nʼelʼčʼɨnɨlʼa. </ta>
            <ta e="T145" id="Seg_982" s="T138">Aša märqa säːqɨ sajlʼaqıtɨ (/sajaiːtɨ) ukkɨrtɨk rɨpčʼimpɔːqıj. </ta>
            <ta e="T155" id="Seg_983" s="T145">Muqɨlʼtɨːrij mɨnnɨmtɨ sɔːrɨmpatɨ säːqɨ qampısä mitə mol tiːmitɨ čʼüšintɨ (/čʼüšimpa). </ta>
            <ta e="T160" id="Seg_984" s="T155">Ontɨ meːqanɨt nılʼčʼik ɔːntalpɨlʼa pisɨnʼnʼi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_985" s="T0">meː</ta>
            <ta e="T2" id="Seg_986" s="T1">qən-q-olap-sä-j</ta>
            <ta e="T3" id="Seg_987" s="T2">Jermolaj-n</ta>
            <ta e="T4" id="Seg_988" s="T3">optı</ta>
            <ta e="T5" id="Seg_989" s="T4">kıkʼä-t</ta>
            <ta e="T6" id="Seg_990" s="T5">qanɨŋ-mɨt</ta>
            <ta e="T7" id="Seg_991" s="T6">šiːpa</ta>
            <ta e="T8" id="Seg_992" s="T7">ɛnɨ-ntɨlʼ</ta>
            <ta e="T9" id="Seg_993" s="T8">suːrɨp</ta>
            <ta e="T10" id="Seg_994" s="T9">qanɨq-qɨt</ta>
            <ta e="T11" id="Seg_995" s="T10">aša</ta>
            <ta e="T12" id="Seg_996" s="T11">orqɨlʼ-i-mpa</ta>
            <ta e="T13" id="Seg_997" s="T12">jesli</ta>
            <ta e="T14" id="Seg_998" s="T13">qaj</ta>
            <ta e="T15" id="Seg_999" s="T14">ɛːmi-j</ta>
            <ta e="T16" id="Seg_1000" s="T15">ilʼmatɨj</ta>
            <ta e="T17" id="Seg_1001" s="T16">pakʼä-ja</ta>
            <ta e="T18" id="Seg_1002" s="T17">meː</ta>
            <ta e="T19" id="Seg_1003" s="T18">čʼatɨ-kkɔː-mɨt</ta>
            <ta e="T20" id="Seg_1004" s="T19">meː</ta>
            <ta e="T21" id="Seg_1005" s="T20">kana-jiː-mɨt</ta>
            <ta e="T22" id="Seg_1006" s="T21">aša</ta>
            <ta e="T23" id="Seg_1007" s="T22">tatɨ-kkɔː-tɨt</ta>
            <ta e="T24" id="Seg_1008" s="T23">naməše-j</ta>
            <ta e="T25" id="Seg_1009" s="T24">lotɨ-t</ta>
            <ta e="T26" id="Seg_1010" s="T25">puːtoː-qɨt</ta>
            <ta e="T27" id="Seg_1011" s="T26">kana-t</ta>
            <ta e="T28" id="Seg_1012" s="T27">aša</ta>
            <ta e="T29" id="Seg_1013" s="T28">tɛnɨmɨ-sɔː-tɨt</ta>
            <ta e="T30" id="Seg_1014" s="T29">uː-r-qa</ta>
            <ta e="T31" id="Seg_1015" s="T30">uː-r-sɔː-tɨt</ta>
            <ta e="T32" id="Seg_1016" s="T31">kana-iː-mɨt</ta>
            <ta e="T33" id="Seg_1017" s="T32">nʼop-ɨ-lʼ</ta>
            <ta e="T34" id="Seg_1018" s="T33">tətto-ŋɨt</ta>
            <ta e="T35" id="Seg_1019" s="T34">qən-qa</ta>
            <ta e="T36" id="Seg_1020" s="T35">tačʼal-kɔː-tɨt</ta>
            <ta e="T37" id="Seg_1021" s="T36">täp-ɨ-t</ta>
            <ta e="T38" id="Seg_1022" s="T37">ola</ta>
            <ta e="T39" id="Seg_1023" s="T38">kukčʼaak</ta>
            <ta e="T40" id="Seg_1024" s="T39">intälʼ-iː-m-tɨt</ta>
            <ta e="T41" id="Seg_1025" s="T40">lotɨk-tɨ</ta>
            <ta e="T42" id="Seg_1026" s="T41">qɔːsɨ-lʼ</ta>
            <ta e="T43" id="Seg_1027" s="T42">nʼuːta-ntɨ</ta>
            <ta e="T44" id="Seg_1028" s="T43">maːt-al-sɔː-tɨt</ta>
            <ta e="T45" id="Seg_1029" s="T44">aša</ta>
            <ta e="T46" id="Seg_1030" s="T45">kətɨ-sɨ-tɨ</ta>
            <ta e="T47" id="Seg_1031" s="T46">čʼiːpoː-qɨn-tɨ</ta>
            <ta e="T48" id="Seg_1032" s="T47">Jermolaj</ta>
            <ta e="T49" id="Seg_1033" s="T48">ürɨ-tɨ</ta>
            <ta e="T50" id="Seg_1034" s="T49">qošk</ta>
            <ta e="T51" id="Seg_1035" s="T50">ɛː-ja</ta>
            <ta e="T52" id="Seg_1036" s="T51">nɔːtna</ta>
            <ta e="T53" id="Seg_1037" s="T52">qo-qo</ta>
            <ta e="T54" id="Seg_1038" s="T53">alako</ta>
            <ta e="T55" id="Seg_1039" s="T54">qəl-lɔː-mɨt</ta>
            <ta e="T56" id="Seg_1040" s="T55">moqona</ta>
            <ta e="T57" id="Seg_1041" s="T56">lʼgov-tɨ</ta>
            <ta e="T58" id="Seg_1042" s="T57">me</ta>
            <ta e="T59" id="Seg_1043" s="T58">qən-nɔː-mɨt</ta>
            <ta e="T60" id="Seg_1044" s="T59">meː</ta>
            <ta e="T62" id="Seg_1045" s="T61">aša</ta>
            <ta e="T63" id="Seg_1046" s="T62">kočʼi-k</ta>
            <ta e="T64" id="Seg_1047" s="T63">qəqɨ-p</ta>
            <ta e="T65" id="Seg_1048" s="T64">meː-ntɨ-mɨt</ta>
            <ta e="T66" id="Seg_1049" s="T65">meːqɨnit</ta>
            <ta e="T67" id="Seg_1050" s="T66">čʼəssa</ta>
            <ta e="T68" id="Seg_1051" s="T67">nʼarqɨ-j</ta>
            <ta e="T69" id="Seg_1052" s="T68">markoː-qɨnɨ</ta>
            <ta e="T70" id="Seg_1053" s="T69">qont-š-ɛːj-ntɨ-na</ta>
            <ta e="T71" id="Seg_1054" s="T70">*qošti-j</ta>
            <ta e="T72" id="Seg_1055" s="T71">nılʼčʼi-j</ta>
            <ta e="T73" id="Seg_1056" s="T72">kana-lʼ</ta>
            <ta e="T74" id="Seg_1057" s="T73">lʼıːpi-ja-na</ta>
            <ta e="T75" id="Seg_1058" s="T74">lʼıːpi-lʼa</ta>
            <ta e="T76" id="Seg_1059" s="T75">täp-ɨ-n</ta>
            <ta e="T77" id="Seg_1060" s="T76">moqal-qɨt</ta>
            <ta e="T78" id="Seg_1061" s="T77">at-ɔːl-i-sɨ</ta>
            <ta e="T79" id="Seg_1062" s="T78">aša</ta>
            <ta e="T80" id="Seg_1063" s="T79">pirqɨ</ta>
            <ta e="T81" id="Seg_1064" s="T80">qup</ta>
            <ta e="T82" id="Seg_1065" s="T81">patɨj</ta>
            <ta e="T83" id="Seg_1066" s="T82">porqɨ-ja-tɨ</ta>
            <ta e="T84" id="Seg_1067" s="T83">or-sä</ta>
            <ta e="T85" id="Seg_1068" s="T84">nɨt-tɨ-mpa</ta>
            <ta e="T86" id="Seg_1069" s="T85">pimɨ-tɨ</ta>
            <ta e="T87" id="Seg_1070" s="T86">nʼärqɨ-lɔːqɨ</ta>
            <ta e="T88" id="Seg_1071" s="T87">ɛː-ja</ta>
            <ta e="T89" id="Seg_1072" s="T88">säqal-pa-tɨ</ta>
            <ta e="T90" id="Seg_1073" s="T89">kutar</ta>
            <ta e="T91" id="Seg_1074" s="T90">qos</ta>
            <ta e="T92" id="Seg_1075" s="T91">küːtɨ-m-tɨ</ta>
            <ta e="T93" id="Seg_1076" s="T92">nɨt-tɨ-pɨlʼ</ta>
            <ta e="T94" id="Seg_1077" s="T93">sapoq-ɨ-n-tɨ</ta>
            <ta e="T95" id="Seg_1078" s="T94">soːj-qɨn-tɨ</ta>
            <ta e="T96" id="Seg_1079" s="T95">paj-qɨl-pa-tɨ</ta>
            <ta e="T97" id="Seg_1080" s="T96">nʼärqɨ</ta>
            <ta e="T98" id="Seg_1081" s="T97">qampıj-sa</ta>
            <ta e="T99" id="Seg_1082" s="T98">ukkur</ta>
            <ta e="T100" id="Seg_1083" s="T99">šʼintʼi-lʼ</ta>
            <ta e="T101" id="Seg_1084" s="T100">puška-tɨ</ta>
            <ta e="T102" id="Seg_1085" s="T101">moqal-qän-tɨ</ta>
            <ta e="T103" id="Seg_1086" s="T102">tänt-al-pa-tɨ</ta>
            <ta e="T104" id="Seg_1087" s="T103">meː</ta>
            <ta e="T105" id="Seg_1088" s="T104">kana-iː-mɨt</ta>
            <ta e="T106" id="Seg_1089" s="T105">mänɨj</ta>
            <ta e="T107" id="Seg_1090" s="T106">kana-p</ta>
            <ta e="T108" id="Seg_1091" s="T107">aptɨ-r-q-olʼap-sɔː-tɨt</ta>
            <ta e="T109" id="Seg_1092" s="T108">na</ta>
            <ta e="T110" id="Seg_1093" s="T109">kanak</ta>
            <ta e="T111" id="Seg_1094" s="T110">ɛnɨ-la</ta>
            <ta e="T112" id="Seg_1095" s="T111">talʼčʼi-m-tɨ</ta>
            <ta e="T113" id="Seg_1096" s="T112">ılla</ta>
            <ta e="T114" id="Seg_1097" s="T113">ɨpa-sɨ-tɨ</ta>
            <ta e="T115" id="Seg_1098" s="T114">üŋɨlsa-m-tɨ</ta>
            <ta e="T116" id="Seg_1099" s="T115">pɨrnʼ-altɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T117" id="Seg_1100" s="T116">aj</ta>
            <ta e="T118" id="Seg_1101" s="T117">čʼäk-aptɨ-lʼa</ta>
            <ta e="T119" id="Seg_1102" s="T118">paktɨ-r-ɨ-sɨ</ta>
            <ta e="T120" id="Seg_1103" s="T119">ɨrɨ-mpɨ-la</ta>
            <ta e="T121" id="Seg_1104" s="T120">mänɨj</ta>
            <ta e="T122" id="Seg_1105" s="T121">qup</ta>
            <ta e="T123" id="Seg_1106" s="T122">meːsʼa</ta>
            <ta e="T124" id="Seg_1107" s="T123">qən-tɨ-lʼa</ta>
            <ta e="T125" id="Seg_1108" s="T124">toroattɨ-sɨ</ta>
            <ta e="T126" id="Seg_1109" s="T125">täp-ɨ-nɨk</ta>
            <ta e="T127" id="Seg_1110" s="T126">kuras-qɨn-tɨ</ta>
            <ta e="T128" id="Seg_1111" s="T127">ɛː-nta</ta>
            <ta e="T129" id="Seg_1112" s="T128">šit-sar</ta>
            <ta e="T130" id="Seg_1113" s="T129">sompɨla</ta>
            <ta e="T131" id="Seg_1114" s="T130">poː-tɨ</ta>
            <ta e="T132" id="Seg_1115" s="T131">täp-ɨ-n</ta>
            <ta e="T133" id="Seg_1116" s="T132">čʼumpɨ</ta>
            <ta e="T134" id="Seg_1117" s="T133">säːqɨ</ta>
            <ta e="T135" id="Seg_1118" s="T134">oːptɨ</ta>
            <ta e="T136" id="Seg_1119" s="T135">čʼırɨ-mpa</ta>
            <ta e="T137" id="Seg_1120" s="T136">čʼırɨ-mpɨ-sɔː-tɨt</ta>
            <ta e="T138" id="Seg_1121" s="T137">nʼelʼčʼɨ-nɨ-lʼa</ta>
            <ta e="T139" id="Seg_1122" s="T138">aša</ta>
            <ta e="T140" id="Seg_1123" s="T139">märqa</ta>
            <ta e="T141" id="Seg_1124" s="T140">säːqɨ</ta>
            <ta e="T142" id="Seg_1125" s="T141">saj-lʼa-qı-tɨ</ta>
            <ta e="T143" id="Seg_1126" s="T142">saja-iː-tɨ</ta>
            <ta e="T144" id="Seg_1127" s="T143">ukkɨrtɨk</ta>
            <ta e="T145" id="Seg_1128" s="T144">rɨpčʼi-mpɔː-qıj</ta>
            <ta e="T146" id="Seg_1129" s="T145">muqɨlʼtɨːri-j</ta>
            <ta e="T147" id="Seg_1130" s="T146">mɨnnɨ-m-tɨ</ta>
            <ta e="T148" id="Seg_1131" s="T147">sɔːrɨ-mpa-tɨ</ta>
            <ta e="T149" id="Seg_1132" s="T148">säːqɨ</ta>
            <ta e="T150" id="Seg_1133" s="T149">qampı-sä</ta>
            <ta e="T151" id="Seg_1134" s="T150">mitə</ta>
            <ta e="T152" id="Seg_1135" s="T151">mol</ta>
            <ta e="T153" id="Seg_1136" s="T152">tiːmi-tɨ</ta>
            <ta e="T154" id="Seg_1137" s="T153">čʼüši-ntɨ</ta>
            <ta e="T155" id="Seg_1138" s="T154">čʼüši-mpa</ta>
            <ta e="T156" id="Seg_1139" s="T155">ontɨ</ta>
            <ta e="T157" id="Seg_1140" s="T156">meːqanɨt</ta>
            <ta e="T158" id="Seg_1141" s="T157">nılʼčʼi-k</ta>
            <ta e="T159" id="Seg_1142" s="T158">ɔːnt-al-pɨ-lʼa</ta>
            <ta e="T160" id="Seg_1143" s="T159">pisɨ-nʼ-nʼi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1144" s="T0">meː</ta>
            <ta e="T2" id="Seg_1145" s="T1">qən-qo-olam-sɨ-j</ta>
            <ta e="T3" id="Seg_1146" s="T2">Jermolaj-n</ta>
            <ta e="T4" id="Seg_1147" s="T3">əptı</ta>
            <ta e="T5" id="Seg_1148" s="T4">kıkä-n</ta>
            <ta e="T6" id="Seg_1149" s="T5">qanɨŋ-mɨn</ta>
            <ta e="T7" id="Seg_1150" s="T6">šiːpa</ta>
            <ta e="T8" id="Seg_1151" s="T7">ɛnɨ-ntɨlʼ</ta>
            <ta e="T9" id="Seg_1152" s="T8">suːrɨm</ta>
            <ta e="T10" id="Seg_1153" s="T9">qanɨŋ-qɨn</ta>
            <ta e="T11" id="Seg_1154" s="T10">ašša</ta>
            <ta e="T12" id="Seg_1155" s="T11">orqɨl-ɨ-mpɨ</ta>
            <ta e="T13" id="Seg_1156" s="T12">jesli</ta>
            <ta e="T14" id="Seg_1157" s="T13">qaj</ta>
            <ta e="T15" id="Seg_1158" s="T14">ɛːmä-lʼ</ta>
            <ta e="T16" id="Seg_1159" s="T15">ilʼmatɨlʼ</ta>
            <ta e="T17" id="Seg_1160" s="T16">pakä-ja</ta>
            <ta e="T18" id="Seg_1161" s="T17">meː</ta>
            <ta e="T19" id="Seg_1162" s="T18">čʼattɨ-kkɨ-mɨt</ta>
            <ta e="T20" id="Seg_1163" s="T19">meː</ta>
            <ta e="T21" id="Seg_1164" s="T20">kanaŋ-iː-mɨt</ta>
            <ta e="T22" id="Seg_1165" s="T21">ašša</ta>
            <ta e="T23" id="Seg_1166" s="T22">taːtɨ-kkɨ-tɨt</ta>
            <ta e="T24" id="Seg_1167" s="T23">namɨššak-lʼ</ta>
            <ta e="T25" id="Seg_1168" s="T24">lotɨŋ-n</ta>
            <ta e="T26" id="Seg_1169" s="T25">puːtɨ-qɨn</ta>
            <ta e="T27" id="Seg_1170" s="T26">kanaŋ-t</ta>
            <ta e="T28" id="Seg_1171" s="T27">ašša</ta>
            <ta e="T29" id="Seg_1172" s="T28">tɛnɨmɨ-sɨ-tɨt</ta>
            <ta e="T30" id="Seg_1173" s="T29">uː-r-qo</ta>
            <ta e="T31" id="Seg_1174" s="T30">uː-r-sɨ-tɨt</ta>
            <ta e="T32" id="Seg_1175" s="T31">kanaŋ-iː-mɨt</ta>
            <ta e="T33" id="Seg_1176" s="T32">nʼop-ɨ-lʼ</ta>
            <ta e="T34" id="Seg_1177" s="T33">təttɨ-mɨn</ta>
            <ta e="T35" id="Seg_1178" s="T34">qən-qo</ta>
            <ta e="T36" id="Seg_1179" s="T35">tačʼal-kkɨ-tɨt</ta>
            <ta e="T37" id="Seg_1180" s="T36">təp-ɨ-t</ta>
            <ta e="T38" id="Seg_1181" s="T37">olä</ta>
            <ta e="T39" id="Seg_1182" s="T38">kukčʼaŋ</ta>
            <ta e="T40" id="Seg_1183" s="T39">ɨntälʼ-iː-m-tɨt</ta>
            <ta e="T41" id="Seg_1184" s="T40">lotɨŋ-ntɨ</ta>
            <ta e="T42" id="Seg_1185" s="T41">qɔːsa-lʼ</ta>
            <ta e="T43" id="Seg_1186" s="T42">nʼuːtɨ-ntɨ</ta>
            <ta e="T44" id="Seg_1187" s="T43">maːtɨ-ätɔːl-sɨ-tɨt</ta>
            <ta e="T45" id="Seg_1188" s="T44">ašša</ta>
            <ta e="T46" id="Seg_1189" s="T45">kətɨ-sɨ-tɨ</ta>
            <ta e="T47" id="Seg_1190" s="T46">čʼiːpɨ-qɨn-ntɨ</ta>
            <ta e="T48" id="Seg_1191" s="T47">Jermolaj</ta>
            <ta e="T49" id="Seg_1192" s="T48">ürɨ-tɨ</ta>
            <ta e="T50" id="Seg_1193" s="T49">*qoš</ta>
            <ta e="T51" id="Seg_1194" s="T50">ɛː-ŋɨ</ta>
            <ta e="T52" id="Seg_1195" s="T51">nɔːtna</ta>
            <ta e="T53" id="Seg_1196" s="T52">qo-qo</ta>
            <ta e="T54" id="Seg_1197" s="T53">alako</ta>
            <ta e="T55" id="Seg_1198" s="T54">qən-lä-mɨt</ta>
            <ta e="T56" id="Seg_1199" s="T55">moqɨnä</ta>
            <ta e="T57" id="Seg_1200" s="T56">Lʼgov-ntɨ</ta>
            <ta e="T58" id="Seg_1201" s="T57">meː</ta>
            <ta e="T59" id="Seg_1202" s="T58">qən-ŋɨ-mɨt</ta>
            <ta e="T60" id="Seg_1203" s="T59">meː</ta>
            <ta e="T62" id="Seg_1204" s="T61">ašša</ta>
            <ta e="T63" id="Seg_1205" s="T62">kočʼčʼɨ-k</ta>
            <ta e="T64" id="Seg_1206" s="T63">qəqqɨ-m</ta>
            <ta e="T65" id="Seg_1207" s="T64">meː-ntɨ-mɨt</ta>
            <ta e="T66" id="Seg_1208" s="T65">meːqɨnʼɨn</ta>
            <ta e="T67" id="Seg_1209" s="T66">čʼəssä</ta>
            <ta e="T68" id="Seg_1210" s="T67">nʼarqɨ-lʼ</ta>
            <ta e="T69" id="Seg_1211" s="T68">markɨ-qɨnɨ</ta>
            <ta e="T70" id="Seg_1212" s="T69">qontɨ-š-ɛː-ntɨ-naj</ta>
            <ta e="T71" id="Seg_1213" s="T70">*qoš-lʼ</ta>
            <ta e="T72" id="Seg_1214" s="T71">nılʼčʼɨ-lʼ</ta>
            <ta e="T73" id="Seg_1215" s="T72">kanaŋ-lʼ</ta>
            <ta e="T74" id="Seg_1216" s="T73">lıːpɨ-ja-naj</ta>
            <ta e="T75" id="Seg_1217" s="T74">lıːpɨ-lʼa</ta>
            <ta e="T76" id="Seg_1218" s="T75">təp-ɨ-n</ta>
            <ta e="T77" id="Seg_1219" s="T76">moqal-qɨn</ta>
            <ta e="T78" id="Seg_1220" s="T77">atɨ-ɔːl-ɨ-sɨ</ta>
            <ta e="T79" id="Seg_1221" s="T78">ašša</ta>
            <ta e="T80" id="Seg_1222" s="T79">pirqɨ</ta>
            <ta e="T81" id="Seg_1223" s="T80">qum</ta>
            <ta e="T82" id="Seg_1224" s="T81">patɨlʼ</ta>
            <ta e="T83" id="Seg_1225" s="T82">porqɨ-ja-tɨ</ta>
            <ta e="T84" id="Seg_1226" s="T83">orɨ-sä</ta>
            <ta e="T85" id="Seg_1227" s="T84">nɨta-tɨ-mpɨ</ta>
            <ta e="T86" id="Seg_1228" s="T85">pimmɨ-tɨ</ta>
            <ta e="T87" id="Seg_1229" s="T86">nʼarqɨ-lɔːqɨ</ta>
            <ta e="T88" id="Seg_1230" s="T87">ɛː-ŋɨ</ta>
            <ta e="T89" id="Seg_1231" s="T88">säqäl-mpɨ-tɨ</ta>
            <ta e="T90" id="Seg_1232" s="T89">kuttar</ta>
            <ta e="T91" id="Seg_1233" s="T90">kos</ta>
            <ta e="T92" id="Seg_1234" s="T91">küːtɨ-m-tɨ</ta>
            <ta e="T93" id="Seg_1235" s="T92">nɨta-tɨ-mpɨlʼ</ta>
            <ta e="T94" id="Seg_1236" s="T93">sapoq-ɨ-n-tɨ</ta>
            <ta e="T95" id="Seg_1237" s="T94">soːj-qɨn-ntɨ</ta>
            <ta e="T96" id="Seg_1238" s="T95">palʼ-qɨl-mpɨ-tɨ</ta>
            <ta e="T97" id="Seg_1239" s="T96">nʼarqɨ</ta>
            <ta e="T98" id="Seg_1240" s="T97">qampı-sä</ta>
            <ta e="T99" id="Seg_1241" s="T98">ukkɨr</ta>
            <ta e="T100" id="Seg_1242" s="T99">šʼintʼi-lʼ</ta>
            <ta e="T101" id="Seg_1243" s="T100">puška-tɨ</ta>
            <ta e="T102" id="Seg_1244" s="T101">moqal-qɨn-ntɨ</ta>
            <ta e="T103" id="Seg_1245" s="T102">tänt-ätɔːl-mpɨ-tɨ</ta>
            <ta e="T104" id="Seg_1246" s="T103">meː</ta>
            <ta e="T105" id="Seg_1247" s="T104">kanaŋ-iː-mɨt</ta>
            <ta e="T106" id="Seg_1248" s="T105">mənɨlʼ</ta>
            <ta e="T107" id="Seg_1249" s="T106">kanaŋ-m</ta>
            <ta e="T108" id="Seg_1250" s="T107">aptɨ-r-qo-olam-sɨ-tɨt</ta>
            <ta e="T109" id="Seg_1251" s="T108">na</ta>
            <ta e="T110" id="Seg_1252" s="T109">kanaŋ</ta>
            <ta e="T111" id="Seg_1253" s="T110">ɛnɨ-lä</ta>
            <ta e="T112" id="Seg_1254" s="T111">talʼčʼɨ-m-tɨ</ta>
            <ta e="T113" id="Seg_1255" s="T112">ıllä</ta>
            <ta e="T114" id="Seg_1256" s="T113">ɨpa-sɨ-tɨ</ta>
            <ta e="T115" id="Seg_1257" s="T114">üŋkɨlsa-m-tɨ</ta>
            <ta e="T116" id="Seg_1258" s="T115">pɨrnʼ-altɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T117" id="Seg_1259" s="T116">aj</ta>
            <ta e="T118" id="Seg_1260" s="T117">*čʼək-äptɨ-lä</ta>
            <ta e="T119" id="Seg_1261" s="T118">paktɨ-r-ɨ-sɨ</ta>
            <ta e="T120" id="Seg_1262" s="T119">ɨrɨ-mpɨ-lä</ta>
            <ta e="T121" id="Seg_1263" s="T120">mənɨlʼ</ta>
            <ta e="T122" id="Seg_1264" s="T121">qum</ta>
            <ta e="T123" id="Seg_1265" s="T122">meːsä</ta>
            <ta e="T124" id="Seg_1266" s="T123">qən-ntɨ-lä</ta>
            <ta e="T125" id="Seg_1267" s="T124">torowajtɨ-sɨ</ta>
            <ta e="T126" id="Seg_1268" s="T125">təp-ɨ-nɨŋ</ta>
            <ta e="T127" id="Seg_1269" s="T126">kuras-qɨn-ntɨ</ta>
            <ta e="T128" id="Seg_1270" s="T127">ɛː-ɛntɨ</ta>
            <ta e="T129" id="Seg_1271" s="T128">šittɨ-sar</ta>
            <ta e="T130" id="Seg_1272" s="T129">sompɨla</ta>
            <ta e="T131" id="Seg_1273" s="T130">poː-tɨ</ta>
            <ta e="T132" id="Seg_1274" s="T131">təp-ɨ-n</ta>
            <ta e="T133" id="Seg_1275" s="T132">čʼumpɨ</ta>
            <ta e="T134" id="Seg_1276" s="T133">säːqɨ</ta>
            <ta e="T135" id="Seg_1277" s="T134">oːptɨ</ta>
            <ta e="T136" id="Seg_1278" s="T135">čʼırɨ-mpɨ</ta>
            <ta e="T137" id="Seg_1279" s="T136">čʼırɨ-mpɨ-sɨ-tɨt</ta>
            <ta e="T138" id="Seg_1280" s="T137">nʼelʼčʼɨ-nʼɨ-lä</ta>
            <ta e="T139" id="Seg_1281" s="T138">ašša</ta>
            <ta e="T140" id="Seg_1282" s="T139">wərqɨ</ta>
            <ta e="T141" id="Seg_1283" s="T140">säːqɨ</ta>
            <ta e="T142" id="Seg_1284" s="T141">sajɨ-lʼa-qı-tɨ</ta>
            <ta e="T143" id="Seg_1285" s="T142">sajɨ-iː-tɨ</ta>
            <ta e="T144" id="Seg_1286" s="T143">ukkɨrtɨk</ta>
            <ta e="T145" id="Seg_1287" s="T144">rɨpčʼɨ-mpɨ-qı</ta>
            <ta e="T146" id="Seg_1288" s="T145">*muqɨltıːrɨ-lʼ</ta>
            <ta e="T147" id="Seg_1289" s="T146">wəntɨ-m-tɨ</ta>
            <ta e="T148" id="Seg_1290" s="T147">sɔːrɨ-mpɨ-tɨ</ta>
            <ta e="T149" id="Seg_1291" s="T148">säːqɨ</ta>
            <ta e="T150" id="Seg_1292" s="T149">qampı-sä</ta>
            <ta e="T151" id="Seg_1293" s="T150">mitɨ</ta>
            <ta e="T152" id="Seg_1294" s="T151">mol</ta>
            <ta e="T153" id="Seg_1295" s="T152">tiːmɨ-tɨ</ta>
            <ta e="T154" id="Seg_1296" s="T153">čʼüšɨ-ntɨ</ta>
            <ta e="T155" id="Seg_1297" s="T154">čʼüšɨ-mpɨ</ta>
            <ta e="T156" id="Seg_1298" s="T155">ontɨ</ta>
            <ta e="T157" id="Seg_1299" s="T156">meːqɨnʼɨn</ta>
            <ta e="T158" id="Seg_1300" s="T157">nılʼčʼɨ-k</ta>
            <ta e="T159" id="Seg_1301" s="T158">ɔːntɨ-al-mpɨ-lä</ta>
            <ta e="T160" id="Seg_1302" s="T159">pisɨ-š-ŋɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1303" s="T0">we.PL.NOM</ta>
            <ta e="T2" id="Seg_1304" s="T1">leave-INF-begin-PST-1DU</ta>
            <ta e="T3" id="Seg_1305" s="T2">Jermolaj-GEN</ta>
            <ta e="T4" id="Seg_1306" s="T3">with</ta>
            <ta e="T5" id="Seg_1307" s="T4">small.river-GEN</ta>
            <ta e="T6" id="Seg_1308" s="T5">bank-PROL</ta>
            <ta e="T7" id="Seg_1309" s="T6">duck.[NOM]</ta>
            <ta e="T8" id="Seg_1310" s="T7">be.afraid-PTCP.PRS</ta>
            <ta e="T9" id="Seg_1311" s="T8">wild.animal.[NOM]</ta>
            <ta e="T10" id="Seg_1312" s="T9">bank-LOC</ta>
            <ta e="T11" id="Seg_1313" s="T10">NEG</ta>
            <ta e="T12" id="Seg_1314" s="T11">catch-EP-DUR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1315" s="T12">if</ta>
            <ta e="T14" id="Seg_1316" s="T13">what.[NOM]</ta>
            <ta e="T15" id="Seg_1317" s="T14">INDEF2-ADJZ</ta>
            <ta e="T16" id="Seg_1318" s="T15">young</ta>
            <ta e="T17" id="Seg_1319" s="T16">teal-DIM.[NOM]</ta>
            <ta e="T18" id="Seg_1320" s="T17">we.PL.NOM</ta>
            <ta e="T19" id="Seg_1321" s="T18">shoot-HAB-1PL</ta>
            <ta e="T20" id="Seg_1322" s="T19">we.PL.GEN</ta>
            <ta e="T21" id="Seg_1323" s="T20">dog-PL.[NOM]-1PL</ta>
            <ta e="T22" id="Seg_1324" s="T21">NEG</ta>
            <ta e="T23" id="Seg_1325" s="T22">bring-HAB-3PL</ta>
            <ta e="T24" id="Seg_1326" s="T23">thus.much-ADJZ</ta>
            <ta e="T25" id="Seg_1327" s="T24">equisetum-GEN</ta>
            <ta e="T26" id="Seg_1328" s="T25">inside-LOC</ta>
            <ta e="T27" id="Seg_1329" s="T26">dog-PL.[NOM]</ta>
            <ta e="T28" id="Seg_1330" s="T27">NEG</ta>
            <ta e="T29" id="Seg_1331" s="T28">can-PST-3PL</ta>
            <ta e="T30" id="Seg_1332" s="T29">swim-FRQ-INF</ta>
            <ta e="T31" id="Seg_1333" s="T30">swim-FRQ-PST-3PL</ta>
            <ta e="T32" id="Seg_1334" s="T31">dog-PL.[NOM]-1PL</ta>
            <ta e="T33" id="Seg_1335" s="T32">slough-EP-ADJZ</ta>
            <ta e="T34" id="Seg_1336" s="T33">earth-PROL</ta>
            <ta e="T35" id="Seg_1337" s="T34">go.away-INF</ta>
            <ta e="T36" id="Seg_1338" s="T35">not.can-HAB-3PL</ta>
            <ta e="T37" id="Seg_1339" s="T36">(s)he-EP-PL.[NOM]</ta>
            <ta e="T38" id="Seg_1340" s="T37">only</ta>
            <ta e="T39" id="Seg_1341" s="T38">vainly</ta>
            <ta e="T40" id="Seg_1342" s="T39">nose-PL-ACC-3PL</ta>
            <ta e="T41" id="Seg_1343" s="T40">equisetum-ILL</ta>
            <ta e="T42" id="Seg_1344" s="T41">perch-ADJZ</ta>
            <ta e="T43" id="Seg_1345" s="T42">grass-ILL</ta>
            <ta e="T44" id="Seg_1346" s="T43">cut-MOM-PST-3PL</ta>
            <ta e="T45" id="Seg_1347" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1348" s="T45">say-PST-3SG.O</ta>
            <ta e="T47" id="Seg_1349" s="T46">end-LOC-OBL.3SG</ta>
            <ta e="T48" id="Seg_1350" s="T47">Jermolaj.[NOM]</ta>
            <ta e="T49" id="Seg_1351" s="T48">business.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_1352" s="T49">bad</ta>
            <ta e="T51" id="Seg_1353" s="T50">be-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_1354" s="T51">one.needs</ta>
            <ta e="T53" id="Seg_1355" s="T52">find-INF</ta>
            <ta e="T54" id="Seg_1356" s="T53">boat.[NOM]</ta>
            <ta e="T55" id="Seg_1357" s="T54">leave-OPT-1PL</ta>
            <ta e="T56" id="Seg_1358" s="T55">back</ta>
            <ta e="T57" id="Seg_1359" s="T56">Lgov-ILL</ta>
            <ta e="T58" id="Seg_1360" s="T57">we.PL.NOM</ta>
            <ta e="T59" id="Seg_1361" s="T58">go.away-CO-1PL</ta>
            <ta e="T60" id="Seg_1362" s="T59">we.PL.NOM</ta>
            <ta e="T62" id="Seg_1363" s="T61">NEG</ta>
            <ta e="T63" id="Seg_1364" s="T62">much-ADVZ</ta>
            <ta e="T64" id="Seg_1365" s="T63">step-ACC</ta>
            <ta e="T65" id="Seg_1366" s="T64">make-INFER-1PL</ta>
            <ta e="T66" id="Seg_1367" s="T65">we.PL.ALL</ta>
            <ta e="T67" id="Seg_1368" s="T66">towards</ta>
            <ta e="T68" id="Seg_1369" s="T67">red-ADJZ</ta>
            <ta e="T69" id="Seg_1370" s="T68">island-EL</ta>
            <ta e="T70" id="Seg_1371" s="T69">appear-US-PFV-INFER.[3SG.S]-EMPH</ta>
            <ta e="T71" id="Seg_1372" s="T70">bad-ADJZ</ta>
            <ta e="T72" id="Seg_1373" s="T71">such-ADJZ</ta>
            <ta e="T73" id="Seg_1374" s="T72">dog-ADJZ</ta>
            <ta e="T74" id="Seg_1375" s="T73">piece-DIM.[NOM]-EMPH</ta>
            <ta e="T75" id="Seg_1376" s="T74">piece-DIM.[NOM]</ta>
            <ta e="T76" id="Seg_1377" s="T75">(s)he-EP-GEN</ta>
            <ta e="T77" id="Seg_1378" s="T76">behind-LOC</ta>
            <ta e="T78" id="Seg_1379" s="T77">be.visible-MOM-EP-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_1380" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1381" s="T79">high</ta>
            <ta e="T81" id="Seg_1382" s="T80">human.being.[NOM]</ta>
            <ta e="T82" id="Seg_1383" s="T81">blue</ta>
            <ta e="T83" id="Seg_1384" s="T82">clothing-DIM.[NOM]-3SG</ta>
            <ta e="T84" id="Seg_1385" s="T83">force-INSTR</ta>
            <ta e="T85" id="Seg_1386" s="T84">tear-TR-DUR.[3SG.S]</ta>
            <ta e="T86" id="Seg_1387" s="T85">trousers.[NOM]-3SG</ta>
            <ta e="T87" id="Seg_1388" s="T86">red-in.some.degree</ta>
            <ta e="T88" id="Seg_1389" s="T87">be-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_1390" s="T88">push.in-DUR-3SG.O</ta>
            <ta e="T90" id="Seg_1391" s="T89">how</ta>
            <ta e="T91" id="Seg_1392" s="T90">INDEF3</ta>
            <ta e="T92" id="Seg_1393" s="T91">bootleg-ACC-3SG</ta>
            <ta e="T93" id="Seg_1394" s="T92">tear-TR-PTCP.PST</ta>
            <ta e="T94" id="Seg_1395" s="T93">high.boot-EP-GEN-3SG</ta>
            <ta e="T95" id="Seg_1396" s="T94">neck-LOC-OBL.3SG</ta>
            <ta e="T96" id="Seg_1397" s="T95">wrap-MULO-DUR-3SG.O</ta>
            <ta e="T97" id="Seg_1398" s="T96">red</ta>
            <ta e="T98" id="Seg_1399" s="T97">headscarf-INSTR</ta>
            <ta e="T99" id="Seg_1400" s="T98">one</ta>
            <ta e="T100" id="Seg_1401" s="T99">%%-ADJZ</ta>
            <ta e="T101" id="Seg_1402" s="T100">rifle.[NOM]-3SG</ta>
            <ta e="T102" id="Seg_1403" s="T101">back-LOC-OBL.3SG</ta>
            <ta e="T103" id="Seg_1404" s="T102">%%-MOM-PST.NAR-3SG.O</ta>
            <ta e="T104" id="Seg_1405" s="T103">we.PL.GEN</ta>
            <ta e="T105" id="Seg_1406" s="T104">dog-PL.[NOM]-1PL</ta>
            <ta e="T106" id="Seg_1407" s="T105">foreign</ta>
            <ta e="T107" id="Seg_1408" s="T106">dog-ACC</ta>
            <ta e="T108" id="Seg_1409" s="T107">smell-FRQ-INF-begin-PST-3PL</ta>
            <ta e="T109" id="Seg_1410" s="T108">this</ta>
            <ta e="T110" id="Seg_1411" s="T109">dog.[NOM]</ta>
            <ta e="T111" id="Seg_1412" s="T110">be.afraid-CVB</ta>
            <ta e="T112" id="Seg_1413" s="T111">tail-ACC-3SG</ta>
            <ta e="T113" id="Seg_1414" s="T112">down</ta>
            <ta e="T114" id="Seg_1415" s="T113">push-PST-3SG.O</ta>
            <ta e="T115" id="Seg_1416" s="T114">ear-ACC-3SG</ta>
            <ta e="T116" id="Seg_1417" s="T115">bristle.up-TR-DUR-PST-3SG.O</ta>
            <ta e="T117" id="Seg_1418" s="T116">and</ta>
            <ta e="T118" id="Seg_1419" s="T117">hurry-ATTEN-CVB</ta>
            <ta e="T119" id="Seg_1420" s="T118">jump-FRQ-EP-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1421" s="T119">snarl-DUR-CVB</ta>
            <ta e="T121" id="Seg_1422" s="T120">foreign</ta>
            <ta e="T122" id="Seg_1423" s="T121">human.being.[NOM]</ta>
            <ta e="T123" id="Seg_1424" s="T122">we.PL.INSTR</ta>
            <ta e="T124" id="Seg_1425" s="T123">leave-IPFV-CVB</ta>
            <ta e="T125" id="Seg_1426" s="T124">greet-PST.[3SG.S]</ta>
            <ta e="T126" id="Seg_1427" s="T125">(s)he-EP-ALL</ta>
            <ta e="T127" id="Seg_1428" s="T126">appearance-ILL/LOC/EL-OBL.3SG</ta>
            <ta e="T128" id="Seg_1429" s="T127">be-FUT.[3SG.S]</ta>
            <ta e="T129" id="Seg_1430" s="T128">two-ten</ta>
            <ta e="T130" id="Seg_1431" s="T129">five</ta>
            <ta e="T131" id="Seg_1432" s="T130">year.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_1433" s="T131">(s)he-EP-GEN</ta>
            <ta e="T133" id="Seg_1434" s="T132">long</ta>
            <ta e="T134" id="Seg_1435" s="T133">black</ta>
            <ta e="T135" id="Seg_1436" s="T134">hair.[NOM]</ta>
            <ta e="T136" id="Seg_1437" s="T135">smell-PST.NAR.[3SG.S]</ta>
            <ta e="T137" id="Seg_1438" s="T136">smell-DUR-PST-3PL</ta>
            <ta e="T138" id="Seg_1439" s="T137">sweat-VBLZ-CVB</ta>
            <ta e="T139" id="Seg_1440" s="T138">NEG</ta>
            <ta e="T140" id="Seg_1441" s="T139">big</ta>
            <ta e="T141" id="Seg_1442" s="T140">black</ta>
            <ta e="T142" id="Seg_1443" s="T141">eye-DIM-DU.[NOM]-3SG</ta>
            <ta e="T143" id="Seg_1444" s="T142">eye-PL.[NOM]-3SG</ta>
            <ta e="T144" id="Seg_1445" s="T143">often</ta>
            <ta e="T145" id="Seg_1446" s="T144">blink-PST.NAR-3DU.S</ta>
            <ta e="T146" id="Seg_1447" s="T145">whole-ADJZ</ta>
            <ta e="T147" id="Seg_1448" s="T146">face-ACC-3SG</ta>
            <ta e="T148" id="Seg_1449" s="T147">bind-DUR-3SG.O</ta>
            <ta e="T149" id="Seg_1450" s="T148">black</ta>
            <ta e="T150" id="Seg_1451" s="T149">headscarf-INSTR</ta>
            <ta e="T151" id="Seg_1452" s="T150">as.if</ta>
            <ta e="T152" id="Seg_1453" s="T151">jetty</ta>
            <ta e="T153" id="Seg_1454" s="T152">tooth.[NOM]-3SG</ta>
            <ta e="T154" id="Seg_1455" s="T153">be.sick-INFER.[3SG.S]</ta>
            <ta e="T155" id="Seg_1456" s="T154">be.sick-PST.NAR.[3SG.S]</ta>
            <ta e="T156" id="Seg_1457" s="T155">oneself.3SG.[NOM]</ta>
            <ta e="T157" id="Seg_1458" s="T156">we.PL.ALL</ta>
            <ta e="T158" id="Seg_1459" s="T157">such-ADVZ</ta>
            <ta e="T159" id="Seg_1460" s="T158">happiness-TR-DUR-CVB</ta>
            <ta e="T160" id="Seg_1461" s="T159">laugh-US-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1462" s="T0">мы.PL.NOM</ta>
            <ta e="T2" id="Seg_1463" s="T1">отправиться-INF-начать-PST-1DU</ta>
            <ta e="T3" id="Seg_1464" s="T2">Ермолай-GEN</ta>
            <ta e="T4" id="Seg_1465" s="T3">с</ta>
            <ta e="T5" id="Seg_1466" s="T4">речка-GEN</ta>
            <ta e="T6" id="Seg_1467" s="T5">берег-PROL</ta>
            <ta e="T7" id="Seg_1468" s="T6">утка.[NOM]</ta>
            <ta e="T8" id="Seg_1469" s="T7">бояться-PTCP.PRS</ta>
            <ta e="T9" id="Seg_1470" s="T8">зверь.[NOM]</ta>
            <ta e="T10" id="Seg_1471" s="T9">берег-LOC</ta>
            <ta e="T11" id="Seg_1472" s="T10">NEG</ta>
            <ta e="T12" id="Seg_1473" s="T11">схватить-EP-DUR.[3SG.S]</ta>
            <ta e="T13" id="Seg_1474" s="T12">если</ta>
            <ta e="T14" id="Seg_1475" s="T13">что.[NOM]</ta>
            <ta e="T15" id="Seg_1476" s="T14">INDEF2-ADJZ</ta>
            <ta e="T16" id="Seg_1477" s="T15">молодой</ta>
            <ta e="T17" id="Seg_1478" s="T16">чирок-DIM.[NOM]</ta>
            <ta e="T18" id="Seg_1479" s="T17">мы.PL.NOM</ta>
            <ta e="T19" id="Seg_1480" s="T18">стрелять-HAB-1PL</ta>
            <ta e="T20" id="Seg_1481" s="T19">мы.PL.GEN</ta>
            <ta e="T21" id="Seg_1482" s="T20">собака-PL.[NOM]-1PL</ta>
            <ta e="T22" id="Seg_1483" s="T21">NEG</ta>
            <ta e="T23" id="Seg_1484" s="T22">принести-HAB-3PL</ta>
            <ta e="T24" id="Seg_1485" s="T23">настолько-ADJZ</ta>
            <ta e="T25" id="Seg_1486" s="T24">хвощ-GEN</ta>
            <ta e="T26" id="Seg_1487" s="T25">внутри-LOC</ta>
            <ta e="T27" id="Seg_1488" s="T26">собака-PL.[NOM]</ta>
            <ta e="T28" id="Seg_1489" s="T27">NEG</ta>
            <ta e="T29" id="Seg_1490" s="T28">уметь-PST-3PL</ta>
            <ta e="T30" id="Seg_1491" s="T29">плыть-FRQ-INF</ta>
            <ta e="T31" id="Seg_1492" s="T30">плыть-FRQ-PST-3PL</ta>
            <ta e="T32" id="Seg_1493" s="T31">собака-PL.[NOM]-1PL</ta>
            <ta e="T33" id="Seg_1494" s="T32">трясина-EP-ADJZ</ta>
            <ta e="T34" id="Seg_1495" s="T33">земля-PROL</ta>
            <ta e="T35" id="Seg_1496" s="T34">уйти-INF</ta>
            <ta e="T36" id="Seg_1497" s="T35">не.мочь-HAB-3PL</ta>
            <ta e="T37" id="Seg_1498" s="T36">он(а)-EP-PL.[NOM]</ta>
            <ta e="T38" id="Seg_1499" s="T37">только</ta>
            <ta e="T39" id="Seg_1500" s="T38">напрасно</ta>
            <ta e="T40" id="Seg_1501" s="T39">нос-PL-ACC-3PL</ta>
            <ta e="T41" id="Seg_1502" s="T40">хвощ-ILL</ta>
            <ta e="T42" id="Seg_1503" s="T41">окунь-ADJZ</ta>
            <ta e="T43" id="Seg_1504" s="T42">трава-ILL</ta>
            <ta e="T44" id="Seg_1505" s="T43">резать-MOM-PST-3PL</ta>
            <ta e="T45" id="Seg_1506" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1507" s="T45">сказать-PST-3SG.O</ta>
            <ta e="T47" id="Seg_1508" s="T46">конец-LOC-OBL.3SG</ta>
            <ta e="T48" id="Seg_1509" s="T47">Ермолай.[NOM]</ta>
            <ta e="T49" id="Seg_1510" s="T48">дело.[NOM]-3SG</ta>
            <ta e="T50" id="Seg_1511" s="T49">плохой</ta>
            <ta e="T51" id="Seg_1512" s="T50">быть-CO.[3SG.S]</ta>
            <ta e="T52" id="Seg_1513" s="T51">нужно</ta>
            <ta e="T53" id="Seg_1514" s="T52">находить-INF</ta>
            <ta e="T54" id="Seg_1515" s="T53">лодка.[NOM]</ta>
            <ta e="T55" id="Seg_1516" s="T54">отправиться-OPT-1PL</ta>
            <ta e="T56" id="Seg_1517" s="T55">назад</ta>
            <ta e="T57" id="Seg_1518" s="T56">Льгов-ILL</ta>
            <ta e="T58" id="Seg_1519" s="T57">мы.PL.NOM</ta>
            <ta e="T59" id="Seg_1520" s="T58">уйти-CO-1PL</ta>
            <ta e="T60" id="Seg_1521" s="T59">мы.PL.NOM</ta>
            <ta e="T62" id="Seg_1522" s="T61">NEG</ta>
            <ta e="T63" id="Seg_1523" s="T62">много-ADVZ</ta>
            <ta e="T64" id="Seg_1524" s="T63">шаг-ACC</ta>
            <ta e="T65" id="Seg_1525" s="T64">сделать-INFER-1PL</ta>
            <ta e="T66" id="Seg_1526" s="T65">мы.PL.ALL</ta>
            <ta e="T67" id="Seg_1527" s="T66">навстречу</ta>
            <ta e="T68" id="Seg_1528" s="T67">красный-ADJZ</ta>
            <ta e="T69" id="Seg_1529" s="T68">остров-EL</ta>
            <ta e="T70" id="Seg_1530" s="T69">показаться-US-PFV-INFER.[3SG.S]-EMPH</ta>
            <ta e="T71" id="Seg_1531" s="T70">плохой-ADJZ</ta>
            <ta e="T72" id="Seg_1532" s="T71">такой-ADJZ</ta>
            <ta e="T73" id="Seg_1533" s="T72">собака-ADJZ</ta>
            <ta e="T74" id="Seg_1534" s="T73">кусок-DIM.[NOM]-EMPH</ta>
            <ta e="T75" id="Seg_1535" s="T74">кусок-DIM.[NOM]</ta>
            <ta e="T76" id="Seg_1536" s="T75">он(а)-EP-GEN</ta>
            <ta e="T77" id="Seg_1537" s="T76">сзади-LOC</ta>
            <ta e="T78" id="Seg_1538" s="T77">виднеться-MOM-EP-PST.[3SG.S]</ta>
            <ta e="T79" id="Seg_1539" s="T78">NEG</ta>
            <ta e="T80" id="Seg_1540" s="T79">высокий</ta>
            <ta e="T81" id="Seg_1541" s="T80">человек.[NOM]</ta>
            <ta e="T82" id="Seg_1542" s="T81">синий</ta>
            <ta e="T83" id="Seg_1543" s="T82">одежда-DIM.[NOM]-3SG</ta>
            <ta e="T84" id="Seg_1544" s="T83">сила-INSTR</ta>
            <ta e="T85" id="Seg_1545" s="T84">рвать-TR-DUR.[3SG.S]</ta>
            <ta e="T86" id="Seg_1546" s="T85">штаны.[NOM]-3SG</ta>
            <ta e="T87" id="Seg_1547" s="T86">красный-в.некоторой.степени</ta>
            <ta e="T88" id="Seg_1548" s="T87">быть-CO.[3SG.S]</ta>
            <ta e="T89" id="Seg_1549" s="T88">засунуть-DUR-3SG.O</ta>
            <ta e="T90" id="Seg_1550" s="T89">как</ta>
            <ta e="T91" id="Seg_1551" s="T90">INDEF3</ta>
            <ta e="T92" id="Seg_1552" s="T91">голенище-ACC-3SG</ta>
            <ta e="T93" id="Seg_1553" s="T92">рвать-TR-PTCP.PST</ta>
            <ta e="T94" id="Seg_1554" s="T93">сапог-EP-GEN-3SG</ta>
            <ta e="T95" id="Seg_1555" s="T94">шея-LOC-OBL.3SG</ta>
            <ta e="T96" id="Seg_1556" s="T95">обмотать-MULO-DUR-3SG.O</ta>
            <ta e="T97" id="Seg_1557" s="T96">красный</ta>
            <ta e="T98" id="Seg_1558" s="T97">платок-INSTR</ta>
            <ta e="T99" id="Seg_1559" s="T98">один</ta>
            <ta e="T100" id="Seg_1560" s="T99">%%-ADJZ</ta>
            <ta e="T101" id="Seg_1561" s="T100">ружьё.[NOM]-3SG</ta>
            <ta e="T102" id="Seg_1562" s="T101">спина-LOC-OBL.3SG</ta>
            <ta e="T103" id="Seg_1563" s="T102">%%-MOM-PST.NAR-3SG.O</ta>
            <ta e="T104" id="Seg_1564" s="T103">мы.PL.GEN</ta>
            <ta e="T105" id="Seg_1565" s="T104">собака-PL.[NOM]-1PL</ta>
            <ta e="T106" id="Seg_1566" s="T105">чужой</ta>
            <ta e="T107" id="Seg_1567" s="T106">собака-ACC</ta>
            <ta e="T108" id="Seg_1568" s="T107">нюхать-FRQ-INF-начать-PST-3PL</ta>
            <ta e="T109" id="Seg_1569" s="T108">этот</ta>
            <ta e="T110" id="Seg_1570" s="T109">собака.[NOM]</ta>
            <ta e="T111" id="Seg_1571" s="T110">бояться-CVB</ta>
            <ta e="T112" id="Seg_1572" s="T111">хвост-ACC-3SG</ta>
            <ta e="T113" id="Seg_1573" s="T112">вниз</ta>
            <ta e="T114" id="Seg_1574" s="T113">жать-PST-3SG.O</ta>
            <ta e="T115" id="Seg_1575" s="T114">ухо-ACC-3SG</ta>
            <ta e="T116" id="Seg_1576" s="T115">топорщиться-TR-DUR-PST-3SG.O</ta>
            <ta e="T117" id="Seg_1577" s="T116">и</ta>
            <ta e="T118" id="Seg_1578" s="T117">торопиться-ATTEN-CVB</ta>
            <ta e="T119" id="Seg_1579" s="T118">прыгнуть-FRQ-EP-PST.[3SG.S]</ta>
            <ta e="T120" id="Seg_1580" s="T119">рычать-DUR-CVB</ta>
            <ta e="T121" id="Seg_1581" s="T120">чужой</ta>
            <ta e="T122" id="Seg_1582" s="T121">человек.[NOM]</ta>
            <ta e="T123" id="Seg_1583" s="T122">мы.PL.INSTR</ta>
            <ta e="T124" id="Seg_1584" s="T123">отправиться-IPFV-CVB</ta>
            <ta e="T125" id="Seg_1585" s="T124">здороваться-PST.[3SG.S]</ta>
            <ta e="T126" id="Seg_1586" s="T125">он(а)-EP-ALL</ta>
            <ta e="T127" id="Seg_1587" s="T126">вид-ILL/LOC/EL-OBL.3SG</ta>
            <ta e="T128" id="Seg_1588" s="T127">быть-FUT.[3SG.S]</ta>
            <ta e="T129" id="Seg_1589" s="T128">два-десять</ta>
            <ta e="T130" id="Seg_1590" s="T129">пять</ta>
            <ta e="T131" id="Seg_1591" s="T130">год.[NOM]-3SG</ta>
            <ta e="T132" id="Seg_1592" s="T131">он(а)-EP-GEN</ta>
            <ta e="T133" id="Seg_1593" s="T132">длинный</ta>
            <ta e="T134" id="Seg_1594" s="T133">чёрный</ta>
            <ta e="T135" id="Seg_1595" s="T134">волосы.[NOM]</ta>
            <ta e="T136" id="Seg_1596" s="T135">пахнуть-PST.NAR.[3SG.S]</ta>
            <ta e="T137" id="Seg_1597" s="T136">пахнуть-DUR-PST-3PL</ta>
            <ta e="T138" id="Seg_1598" s="T137">пот-VBLZ-CVB</ta>
            <ta e="T139" id="Seg_1599" s="T138">NEG</ta>
            <ta e="T140" id="Seg_1600" s="T139">большой</ta>
            <ta e="T141" id="Seg_1601" s="T140">чёрный</ta>
            <ta e="T142" id="Seg_1602" s="T141">глаз-DIM-DU.[NOM]-3SG</ta>
            <ta e="T143" id="Seg_1603" s="T142">глаз-PL.[NOM]-3SG</ta>
            <ta e="T144" id="Seg_1604" s="T143">часто</ta>
            <ta e="T145" id="Seg_1605" s="T144">мигать-PST.NAR-3DU.S</ta>
            <ta e="T146" id="Seg_1606" s="T145">целый-ADJZ</ta>
            <ta e="T147" id="Seg_1607" s="T146">лицо-ACC-3SG</ta>
            <ta e="T148" id="Seg_1608" s="T147">привязать-DUR-3SG.O</ta>
            <ta e="T149" id="Seg_1609" s="T148">чёрный</ta>
            <ta e="T150" id="Seg_1610" s="T149">платок-INSTR</ta>
            <ta e="T151" id="Seg_1611" s="T150">словно</ta>
            <ta e="T152" id="Seg_1612" s="T151">мол</ta>
            <ta e="T153" id="Seg_1613" s="T152">зуб.[NOM]-3SG</ta>
            <ta e="T154" id="Seg_1614" s="T153">болеть-INFER.[3SG.S]</ta>
            <ta e="T155" id="Seg_1615" s="T154">болеть-PST.NAR.[3SG.S]</ta>
            <ta e="T156" id="Seg_1616" s="T155">сам.3SG.[NOM]</ta>
            <ta e="T157" id="Seg_1617" s="T156">мы.PL.ALL</ta>
            <ta e="T158" id="Seg_1618" s="T157">такой-ADVZ</ta>
            <ta e="T159" id="Seg_1619" s="T158">радость-TR-DUR-CVB</ta>
            <ta e="T160" id="Seg_1620" s="T159">смеяться-US-CO.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1621" s="T0">pers</ta>
            <ta e="T2" id="Seg_1622" s="T1">v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1623" s="T2">nprop-n:case</ta>
            <ta e="T4" id="Seg_1624" s="T3">pp</ta>
            <ta e="T5" id="Seg_1625" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_1626" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1627" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1628" s="T7">v-v&gt;ptcp</ta>
            <ta e="T9" id="Seg_1629" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1630" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_1631" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_1632" s="T11">v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_1633" s="T12">conj</ta>
            <ta e="T14" id="Seg_1634" s="T13">interrog-n:case</ta>
            <ta e="T15" id="Seg_1635" s="T14">ptcl-n&gt;adj</ta>
            <ta e="T16" id="Seg_1636" s="T15">adj</ta>
            <ta e="T17" id="Seg_1637" s="T16">n-n&gt;n-n:case</ta>
            <ta e="T18" id="Seg_1638" s="T17">pers</ta>
            <ta e="T19" id="Seg_1639" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_1640" s="T19">pers</ta>
            <ta e="T21" id="Seg_1641" s="T20">n-n:num-n:case-n:poss</ta>
            <ta e="T22" id="Seg_1642" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1643" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_1644" s="T23">adv-adv&gt;adj</ta>
            <ta e="T25" id="Seg_1645" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1646" s="T25">pp-n:case</ta>
            <ta e="T27" id="Seg_1647" s="T26">n-n:num-n:case</ta>
            <ta e="T28" id="Seg_1648" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1649" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1650" s="T29">v-v&gt;v-v:inf</ta>
            <ta e="T31" id="Seg_1651" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1652" s="T31">n-n:num-n:case-n:poss</ta>
            <ta e="T33" id="Seg_1653" s="T32">n-n:ins-n&gt;adj</ta>
            <ta e="T34" id="Seg_1654" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1655" s="T34">v-v:inf</ta>
            <ta e="T36" id="Seg_1656" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_1657" s="T36">pers-n:ins-n:num-n:case</ta>
            <ta e="T38" id="Seg_1658" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1659" s="T38">adv</ta>
            <ta e="T40" id="Seg_1660" s="T39">n-n:num-n:case-n:poss</ta>
            <ta e="T41" id="Seg_1661" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1662" s="T41">n-n&gt;adj</ta>
            <ta e="T43" id="Seg_1663" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1664" s="T43">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1665" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1666" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1667" s="T46">n-n:case-n:obl.poss</ta>
            <ta e="T48" id="Seg_1668" s="T47">nprop-n:case</ta>
            <ta e="T49" id="Seg_1669" s="T48">n-n:case-n:poss</ta>
            <ta e="T50" id="Seg_1670" s="T49">adj</ta>
            <ta e="T51" id="Seg_1671" s="T50">v-v:ins-v:pn</ta>
            <ta e="T52" id="Seg_1672" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_1673" s="T52">v-v:inf</ta>
            <ta e="T54" id="Seg_1674" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1675" s="T54">v-v:mood-v:pn</ta>
            <ta e="T56" id="Seg_1676" s="T55">adv</ta>
            <ta e="T57" id="Seg_1677" s="T56">nprop-n:case</ta>
            <ta e="T58" id="Seg_1678" s="T57">pers</ta>
            <ta e="T59" id="Seg_1679" s="T58">v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_1680" s="T59">pers</ta>
            <ta e="T62" id="Seg_1681" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1682" s="T62">quant-adj&gt;adv</ta>
            <ta e="T64" id="Seg_1683" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_1684" s="T64">v-v:tense.mood-v:pn</ta>
            <ta e="T66" id="Seg_1685" s="T65">pers</ta>
            <ta e="T67" id="Seg_1686" s="T66">adv</ta>
            <ta e="T68" id="Seg_1687" s="T67">adj-n&gt;adj</ta>
            <ta e="T69" id="Seg_1688" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_1689" s="T69">v-v&gt;v-v&gt;v-v:tense.mood-v:pn-clit</ta>
            <ta e="T71" id="Seg_1690" s="T70">adj-adj&gt;adj</ta>
            <ta e="T72" id="Seg_1691" s="T71">dem-adj&gt;adj</ta>
            <ta e="T73" id="Seg_1692" s="T72">n-n&gt;adj</ta>
            <ta e="T74" id="Seg_1693" s="T73">n-n&gt;n-n:case-clit</ta>
            <ta e="T75" id="Seg_1694" s="T74">n-n&gt;n-n:case</ta>
            <ta e="T76" id="Seg_1695" s="T75">pers-n:ins-n:case</ta>
            <ta e="T77" id="Seg_1696" s="T76">pp-n:case</ta>
            <ta e="T78" id="Seg_1697" s="T77">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1698" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1699" s="T79">adj</ta>
            <ta e="T81" id="Seg_1700" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1701" s="T81">adj</ta>
            <ta e="T83" id="Seg_1702" s="T82">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T84" id="Seg_1703" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1704" s="T84">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T86" id="Seg_1705" s="T85">n-n:case-n:poss</ta>
            <ta e="T87" id="Seg_1706" s="T86">adj-adj&gt;adj</ta>
            <ta e="T88" id="Seg_1707" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1708" s="T88">v-v&gt;v-v:pn</ta>
            <ta e="T90" id="Seg_1709" s="T89">interrog</ta>
            <ta e="T91" id="Seg_1710" s="T90">clit</ta>
            <ta e="T92" id="Seg_1711" s="T91">n-n:case-n:poss</ta>
            <ta e="T93" id="Seg_1712" s="T92">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T94" id="Seg_1713" s="T93">n-n:ins-n:case-n:poss</ta>
            <ta e="T95" id="Seg_1714" s="T94">n-n:case-n:obl.poss</ta>
            <ta e="T96" id="Seg_1715" s="T95">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T97" id="Seg_1716" s="T96">adj</ta>
            <ta e="T98" id="Seg_1717" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_1718" s="T98">num</ta>
            <ta e="T100" id="Seg_1719" s="T99">n-n&gt;adj</ta>
            <ta e="T101" id="Seg_1720" s="T100">n-n:case-n:poss</ta>
            <ta e="T102" id="Seg_1721" s="T101">n-n:case-n:obl.poss</ta>
            <ta e="T103" id="Seg_1722" s="T102">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1723" s="T103">pers</ta>
            <ta e="T105" id="Seg_1724" s="T104">n-n:num-n:case-n:poss</ta>
            <ta e="T106" id="Seg_1725" s="T105">adj</ta>
            <ta e="T107" id="Seg_1726" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1727" s="T107">v-v&gt;v-v:inf-v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1728" s="T108">dem</ta>
            <ta e="T110" id="Seg_1729" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1730" s="T110">v-v&gt;adv</ta>
            <ta e="T112" id="Seg_1731" s="T111">n-n:case-n:poss</ta>
            <ta e="T113" id="Seg_1732" s="T112">preverb</ta>
            <ta e="T114" id="Seg_1733" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1734" s="T114">n-n:case-n:poss</ta>
            <ta e="T116" id="Seg_1735" s="T115">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_1736" s="T116">conj</ta>
            <ta e="T118" id="Seg_1737" s="T117">v-v&gt;v-v&gt;adv</ta>
            <ta e="T119" id="Seg_1738" s="T118">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1739" s="T119">v-v&gt;v-v&gt;adv</ta>
            <ta e="T121" id="Seg_1740" s="T120">adj</ta>
            <ta e="T122" id="Seg_1741" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_1742" s="T122">pro</ta>
            <ta e="T124" id="Seg_1743" s="T123">v-v&gt;v-v&gt;adv</ta>
            <ta e="T125" id="Seg_1744" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1745" s="T125">pers-n:ins-n:case</ta>
            <ta e="T127" id="Seg_1746" s="T126">n-n:case-n:obl.poss</ta>
            <ta e="T128" id="Seg_1747" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_1748" s="T128">num-num&gt;num</ta>
            <ta e="T130" id="Seg_1749" s="T129">num</ta>
            <ta e="T131" id="Seg_1750" s="T130">n-n:case-n:poss</ta>
            <ta e="T132" id="Seg_1751" s="T131">pers-n:ins-n:case</ta>
            <ta e="T133" id="Seg_1752" s="T132">adj</ta>
            <ta e="T134" id="Seg_1753" s="T133">adj</ta>
            <ta e="T135" id="Seg_1754" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1755" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1756" s="T136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1757" s="T137">n-n&gt;v-v&gt;adv</ta>
            <ta e="T139" id="Seg_1758" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1759" s="T139">adj</ta>
            <ta e="T141" id="Seg_1760" s="T140">adj</ta>
            <ta e="T142" id="Seg_1761" s="T141">n-n&gt;n-n:num-n:case-n:poss</ta>
            <ta e="T143" id="Seg_1762" s="T142">n-n:num-n:case-n:poss</ta>
            <ta e="T144" id="Seg_1763" s="T143">adv</ta>
            <ta e="T145" id="Seg_1764" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_1765" s="T145">adj-adj&gt;adj</ta>
            <ta e="T147" id="Seg_1766" s="T146">n-n:case-n:poss</ta>
            <ta e="T148" id="Seg_1767" s="T147">v-v&gt;v-v:pn</ta>
            <ta e="T149" id="Seg_1768" s="T148">adj</ta>
            <ta e="T150" id="Seg_1769" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_1770" s="T150">conj</ta>
            <ta e="T152" id="Seg_1771" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_1772" s="T152">n-n:case-n:poss</ta>
            <ta e="T154" id="Seg_1773" s="T153">v-v:tense.mood-v:pn</ta>
            <ta e="T155" id="Seg_1774" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_1775" s="T155">emphpro-n:case</ta>
            <ta e="T157" id="Seg_1776" s="T156">pers</ta>
            <ta e="T158" id="Seg_1777" s="T157">dem-adj&gt;adv</ta>
            <ta e="T159" id="Seg_1778" s="T158">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T160" id="Seg_1779" s="T159">v-v&gt;v-v:ins-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1780" s="T0">pers</ta>
            <ta e="T2" id="Seg_1781" s="T1">v</ta>
            <ta e="T3" id="Seg_1782" s="T2">nprop</ta>
            <ta e="T4" id="Seg_1783" s="T3">pp</ta>
            <ta e="T5" id="Seg_1784" s="T4">n</ta>
            <ta e="T6" id="Seg_1785" s="T5">n</ta>
            <ta e="T7" id="Seg_1786" s="T6">n</ta>
            <ta e="T8" id="Seg_1787" s="T7">ptcp</ta>
            <ta e="T9" id="Seg_1788" s="T8">n</ta>
            <ta e="T10" id="Seg_1789" s="T9">n</ta>
            <ta e="T11" id="Seg_1790" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_1791" s="T11">v</ta>
            <ta e="T13" id="Seg_1792" s="T12">conj</ta>
            <ta e="T14" id="Seg_1793" s="T13">interrog</ta>
            <ta e="T15" id="Seg_1794" s="T14">adj</ta>
            <ta e="T16" id="Seg_1795" s="T15">adj</ta>
            <ta e="T17" id="Seg_1796" s="T16">n</ta>
            <ta e="T18" id="Seg_1797" s="T17">pers</ta>
            <ta e="T19" id="Seg_1798" s="T18">v</ta>
            <ta e="T20" id="Seg_1799" s="T19">pers</ta>
            <ta e="T21" id="Seg_1800" s="T20">n</ta>
            <ta e="T22" id="Seg_1801" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1802" s="T22">v</ta>
            <ta e="T24" id="Seg_1803" s="T23">adj</ta>
            <ta e="T25" id="Seg_1804" s="T24">n</ta>
            <ta e="T26" id="Seg_1805" s="T25">pp</ta>
            <ta e="T27" id="Seg_1806" s="T26">n</ta>
            <ta e="T28" id="Seg_1807" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1808" s="T28">v</ta>
            <ta e="T30" id="Seg_1809" s="T29">v</ta>
            <ta e="T31" id="Seg_1810" s="T30">v</ta>
            <ta e="T32" id="Seg_1811" s="T31">n</ta>
            <ta e="T33" id="Seg_1812" s="T32">adj</ta>
            <ta e="T34" id="Seg_1813" s="T33">n</ta>
            <ta e="T35" id="Seg_1814" s="T34">v</ta>
            <ta e="T36" id="Seg_1815" s="T35">v</ta>
            <ta e="T37" id="Seg_1816" s="T36">pers</ta>
            <ta e="T38" id="Seg_1817" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1818" s="T38">adv</ta>
            <ta e="T40" id="Seg_1819" s="T39">n</ta>
            <ta e="T41" id="Seg_1820" s="T40">n</ta>
            <ta e="T42" id="Seg_1821" s="T41">adj</ta>
            <ta e="T43" id="Seg_1822" s="T42">n</ta>
            <ta e="T44" id="Seg_1823" s="T43">v</ta>
            <ta e="T45" id="Seg_1824" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1825" s="T45">v</ta>
            <ta e="T47" id="Seg_1826" s="T46">n</ta>
            <ta e="T48" id="Seg_1827" s="T47">nprop</ta>
            <ta e="T49" id="Seg_1828" s="T48">n</ta>
            <ta e="T50" id="Seg_1829" s="T49">adj</ta>
            <ta e="T51" id="Seg_1830" s="T50">v</ta>
            <ta e="T52" id="Seg_1831" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_1832" s="T52">v</ta>
            <ta e="T54" id="Seg_1833" s="T53">n</ta>
            <ta e="T55" id="Seg_1834" s="T54">v</ta>
            <ta e="T56" id="Seg_1835" s="T55">adv</ta>
            <ta e="T57" id="Seg_1836" s="T56">nprop</ta>
            <ta e="T58" id="Seg_1837" s="T57">pers</ta>
            <ta e="T59" id="Seg_1838" s="T58">v</ta>
            <ta e="T60" id="Seg_1839" s="T59">pers</ta>
            <ta e="T62" id="Seg_1840" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1841" s="T62">adv</ta>
            <ta e="T64" id="Seg_1842" s="T63">n</ta>
            <ta e="T65" id="Seg_1843" s="T64">v</ta>
            <ta e="T66" id="Seg_1844" s="T65">pers</ta>
            <ta e="T67" id="Seg_1845" s="T66">adv</ta>
            <ta e="T68" id="Seg_1846" s="T67">adj</ta>
            <ta e="T69" id="Seg_1847" s="T68">n</ta>
            <ta e="T70" id="Seg_1848" s="T69">v</ta>
            <ta e="T71" id="Seg_1849" s="T70">adj</ta>
            <ta e="T72" id="Seg_1850" s="T71">adj</ta>
            <ta e="T73" id="Seg_1851" s="T72">adj</ta>
            <ta e="T74" id="Seg_1852" s="T73">n</ta>
            <ta e="T75" id="Seg_1853" s="T74">n</ta>
            <ta e="T76" id="Seg_1854" s="T75">pers</ta>
            <ta e="T77" id="Seg_1855" s="T76">pp</ta>
            <ta e="T78" id="Seg_1856" s="T77">v</ta>
            <ta e="T79" id="Seg_1857" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1858" s="T79">adj</ta>
            <ta e="T81" id="Seg_1859" s="T80">n</ta>
            <ta e="T82" id="Seg_1860" s="T81">adj</ta>
            <ta e="T83" id="Seg_1861" s="T82">n</ta>
            <ta e="T84" id="Seg_1862" s="T83">n</ta>
            <ta e="T85" id="Seg_1863" s="T84">v</ta>
            <ta e="T86" id="Seg_1864" s="T85">n</ta>
            <ta e="T87" id="Seg_1865" s="T86">adj</ta>
            <ta e="T88" id="Seg_1866" s="T87">v</ta>
            <ta e="T89" id="Seg_1867" s="T88">v</ta>
            <ta e="T90" id="Seg_1868" s="T89">interrog</ta>
            <ta e="T91" id="Seg_1869" s="T90">clit</ta>
            <ta e="T92" id="Seg_1870" s="T91">n</ta>
            <ta e="T93" id="Seg_1871" s="T92">v</ta>
            <ta e="T94" id="Seg_1872" s="T93">n</ta>
            <ta e="T95" id="Seg_1873" s="T94">n</ta>
            <ta e="T96" id="Seg_1874" s="T95">v</ta>
            <ta e="T97" id="Seg_1875" s="T96">adj</ta>
            <ta e="T98" id="Seg_1876" s="T97">n</ta>
            <ta e="T99" id="Seg_1877" s="T98">num</ta>
            <ta e="T100" id="Seg_1878" s="T99">adj</ta>
            <ta e="T101" id="Seg_1879" s="T100">n</ta>
            <ta e="T102" id="Seg_1880" s="T101">n</ta>
            <ta e="T103" id="Seg_1881" s="T102">v</ta>
            <ta e="T104" id="Seg_1882" s="T103">pers</ta>
            <ta e="T105" id="Seg_1883" s="T104">n</ta>
            <ta e="T106" id="Seg_1884" s="T105">adj</ta>
            <ta e="T107" id="Seg_1885" s="T106">n</ta>
            <ta e="T108" id="Seg_1886" s="T107">v</ta>
            <ta e="T109" id="Seg_1887" s="T108">dem</ta>
            <ta e="T110" id="Seg_1888" s="T109">n</ta>
            <ta e="T111" id="Seg_1889" s="T110">adv</ta>
            <ta e="T112" id="Seg_1890" s="T111">n</ta>
            <ta e="T113" id="Seg_1891" s="T112">preverb</ta>
            <ta e="T114" id="Seg_1892" s="T113">v</ta>
            <ta e="T115" id="Seg_1893" s="T114">n</ta>
            <ta e="T116" id="Seg_1894" s="T115">v</ta>
            <ta e="T117" id="Seg_1895" s="T116">conj</ta>
            <ta e="T118" id="Seg_1896" s="T117">adv</ta>
            <ta e="T119" id="Seg_1897" s="T118">v</ta>
            <ta e="T120" id="Seg_1898" s="T119">adv</ta>
            <ta e="T121" id="Seg_1899" s="T120">adj</ta>
            <ta e="T122" id="Seg_1900" s="T121">n</ta>
            <ta e="T123" id="Seg_1901" s="T122">pers</ta>
            <ta e="T124" id="Seg_1902" s="T123">adv</ta>
            <ta e="T125" id="Seg_1903" s="T124">v</ta>
            <ta e="T126" id="Seg_1904" s="T125">pers</ta>
            <ta e="T127" id="Seg_1905" s="T126">n</ta>
            <ta e="T128" id="Seg_1906" s="T127">v</ta>
            <ta e="T129" id="Seg_1907" s="T128">num</ta>
            <ta e="T130" id="Seg_1908" s="T129">num</ta>
            <ta e="T131" id="Seg_1909" s="T130">n</ta>
            <ta e="T132" id="Seg_1910" s="T131">pers</ta>
            <ta e="T133" id="Seg_1911" s="T132">adj</ta>
            <ta e="T134" id="Seg_1912" s="T133">adj</ta>
            <ta e="T135" id="Seg_1913" s="T134">n</ta>
            <ta e="T136" id="Seg_1914" s="T135">v</ta>
            <ta e="T137" id="Seg_1915" s="T136">v</ta>
            <ta e="T138" id="Seg_1916" s="T137">adv</ta>
            <ta e="T139" id="Seg_1917" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1918" s="T139">adj</ta>
            <ta e="T141" id="Seg_1919" s="T140">adj</ta>
            <ta e="T142" id="Seg_1920" s="T141">n</ta>
            <ta e="T143" id="Seg_1921" s="T142">n</ta>
            <ta e="T144" id="Seg_1922" s="T143">adv</ta>
            <ta e="T145" id="Seg_1923" s="T144">v</ta>
            <ta e="T146" id="Seg_1924" s="T145">adj</ta>
            <ta e="T147" id="Seg_1925" s="T146">n</ta>
            <ta e="T148" id="Seg_1926" s="T147">v</ta>
            <ta e="T149" id="Seg_1927" s="T148">adj</ta>
            <ta e="T150" id="Seg_1928" s="T149">n</ta>
            <ta e="T151" id="Seg_1929" s="T150">conj</ta>
            <ta e="T152" id="Seg_1930" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_1931" s="T152">n</ta>
            <ta e="T154" id="Seg_1932" s="T153">v</ta>
            <ta e="T155" id="Seg_1933" s="T154">v</ta>
            <ta e="T156" id="Seg_1934" s="T155">emphpro</ta>
            <ta e="T157" id="Seg_1935" s="T156">pers</ta>
            <ta e="T158" id="Seg_1936" s="T157">adv</ta>
            <ta e="T159" id="Seg_1937" s="T158">v</ta>
            <ta e="T160" id="Seg_1938" s="T159">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1939" s="T0">pro.h:A</ta>
            <ta e="T3" id="Seg_1940" s="T2">pp:Com</ta>
            <ta e="T5" id="Seg_1941" s="T4">np:Poss</ta>
            <ta e="T6" id="Seg_1942" s="T5">np:Path</ta>
            <ta e="T7" id="Seg_1943" s="T6">np:Th</ta>
            <ta e="T10" id="Seg_1944" s="T9">np:L</ta>
            <ta e="T12" id="Seg_1945" s="T11">0.3:A</ta>
            <ta e="T17" id="Seg_1946" s="T16">np:P</ta>
            <ta e="T18" id="Seg_1947" s="T17">pro.h:A</ta>
            <ta e="T20" id="Seg_1948" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_1949" s="T20">np:A</ta>
            <ta e="T23" id="Seg_1950" s="T22">0.3:Th</ta>
            <ta e="T25" id="Seg_1951" s="T24">pp:So</ta>
            <ta e="T27" id="Seg_1952" s="T26">np:A</ta>
            <ta e="T30" id="Seg_1953" s="T29">v:Th</ta>
            <ta e="T32" id="Seg_1954" s="T31">np:A</ta>
            <ta e="T34" id="Seg_1955" s="T33">np:Path</ta>
            <ta e="T35" id="Seg_1956" s="T34">v:Th</ta>
            <ta e="T37" id="Seg_1957" s="T36">pro.h:A </ta>
            <ta e="T40" id="Seg_1958" s="T39">np:P 0.3.h:Poss</ta>
            <ta e="T41" id="Seg_1959" s="T40">np:Ins</ta>
            <ta e="T43" id="Seg_1960" s="T42">np:Ins</ta>
            <ta e="T48" id="Seg_1961" s="T47">np.h:A</ta>
            <ta e="T49" id="Seg_1962" s="T48">np:Th</ta>
            <ta e="T53" id="Seg_1963" s="T52">v:Th</ta>
            <ta e="T54" id="Seg_1964" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_1965" s="T54">0.1.h:A</ta>
            <ta e="T56" id="Seg_1966" s="T55">adv:G</ta>
            <ta e="T57" id="Seg_1967" s="T56">np:G</ta>
            <ta e="T58" id="Seg_1968" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_1969" s="T59">pro.h:A</ta>
            <ta e="T64" id="Seg_1970" s="T63">np:Th</ta>
            <ta e="T67" id="Seg_1971" s="T66">adv:G</ta>
            <ta e="T69" id="Seg_1972" s="T68">np:So</ta>
            <ta e="T74" id="Seg_1973" s="T73">np:Th</ta>
            <ta e="T76" id="Seg_1974" s="T75">pp:L</ta>
            <ta e="T81" id="Seg_1975" s="T80">np.h:Th</ta>
            <ta e="T83" id="Seg_1976" s="T82">np:P 0.3.h:Poss</ta>
            <ta e="T86" id="Seg_1977" s="T85">np:Th 0.3.h:Poss</ta>
            <ta e="T89" id="Seg_1978" s="T88">0.3.h:A 0.3:Th</ta>
            <ta e="T94" id="Seg_1979" s="T93">np:Poss 0.3.h:Poss</ta>
            <ta e="T95" id="Seg_1980" s="T94">np:L 0.3.h:Poss</ta>
            <ta e="T96" id="Seg_1981" s="T95">0.3.h:A 0.3:Th</ta>
            <ta e="T98" id="Seg_1982" s="T97">np:Ins</ta>
            <ta e="T101" id="Seg_1983" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_1984" s="T101">np:L 0.3.h:Poss</ta>
            <ta e="T103" id="Seg_1985" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_1986" s="T103">pro.h:Poss</ta>
            <ta e="T105" id="Seg_1987" s="T104">np:A</ta>
            <ta e="T107" id="Seg_1988" s="T106">np:Th</ta>
            <ta e="T110" id="Seg_1989" s="T109">np:A</ta>
            <ta e="T112" id="Seg_1990" s="T111">np:Th 0.3:Poss</ta>
            <ta e="T115" id="Seg_1991" s="T114">np:Th</ta>
            <ta e="T116" id="Seg_1992" s="T115">0.3:A</ta>
            <ta e="T119" id="Seg_1993" s="T118">0.3:A</ta>
            <ta e="T122" id="Seg_1994" s="T121">np.h:A</ta>
            <ta e="T123" id="Seg_1995" s="T122">pro:Com</ta>
            <ta e="T132" id="Seg_1996" s="T131">pro.h:Poss</ta>
            <ta e="T135" id="Seg_1997" s="T134">np:Th</ta>
            <ta e="T142" id="Seg_1998" s="T141">np:Th 0.3.h:Poss</ta>
            <ta e="T147" id="Seg_1999" s="T146">np:P</ta>
            <ta e="T148" id="Seg_2000" s="T147">0.3.h:A</ta>
            <ta e="T150" id="Seg_2001" s="T149">np:Ins</ta>
            <ta e="T153" id="Seg_2002" s="T152">np:P</ta>
            <ta e="T156" id="Seg_2003" s="T155">pro.h:A</ta>
            <ta e="T157" id="Seg_2004" s="T156">pro.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2005" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_2006" s="T1">v:pred</ta>
            <ta e="T7" id="Seg_2007" s="T6">np:S</ta>
            <ta e="T9" id="Seg_2008" s="T8">n:pred</ta>
            <ta e="T12" id="Seg_2009" s="T11">0.3:S v:pred</ta>
            <ta e="T19" id="Seg_2010" s="T12">s:cond</ta>
            <ta e="T21" id="Seg_2011" s="T20">np:S</ta>
            <ta e="T23" id="Seg_2012" s="T22">0.3:O v:pred</ta>
            <ta e="T27" id="Seg_2013" s="T26">np:S</ta>
            <ta e="T29" id="Seg_2014" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_2015" s="T29">v:O</ta>
            <ta e="T32" id="Seg_2016" s="T31">np:S</ta>
            <ta e="T35" id="Seg_2017" s="T34">v:O</ta>
            <ta e="T36" id="Seg_2018" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_2019" s="T36">pro.h:S</ta>
            <ta e="T40" id="Seg_2020" s="T39">np:O</ta>
            <ta e="T44" id="Seg_2021" s="T43">v:pred</ta>
            <ta e="T46" id="Seg_2022" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_2023" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_2024" s="T48">np:S</ta>
            <ta e="T50" id="Seg_2025" s="T49">adj:pred</ta>
            <ta e="T51" id="Seg_2026" s="T50">cop</ta>
            <ta e="T52" id="Seg_2027" s="T51">ptcl:pred</ta>
            <ta e="T53" id="Seg_2028" s="T52">v:O</ta>
            <ta e="T55" id="Seg_2029" s="T54">0.1.h:S v:pred</ta>
            <ta e="T58" id="Seg_2030" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_2031" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_2032" s="T59">pro.h:S</ta>
            <ta e="T64" id="Seg_2033" s="T63">np:O</ta>
            <ta e="T65" id="Seg_2034" s="T64">v:pred</ta>
            <ta e="T70" id="Seg_2035" s="T69">v:pred</ta>
            <ta e="T74" id="Seg_2036" s="T73">np:S</ta>
            <ta e="T78" id="Seg_2037" s="T77">v:pred</ta>
            <ta e="T81" id="Seg_2038" s="T80">np.h:S</ta>
            <ta e="T83" id="Seg_2039" s="T82">np:S</ta>
            <ta e="T85" id="Seg_2040" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2041" s="T85">np:S</ta>
            <ta e="T87" id="Seg_2042" s="T86">adj:pred</ta>
            <ta e="T88" id="Seg_2043" s="T87">cop</ta>
            <ta e="T89" id="Seg_2044" s="T88">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T96" id="Seg_2045" s="T95">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T101" id="Seg_2046" s="T100">np:O</ta>
            <ta e="T103" id="Seg_2047" s="T102">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_2048" s="T104">np:S</ta>
            <ta e="T107" id="Seg_2049" s="T106">np:O</ta>
            <ta e="T108" id="Seg_2050" s="T107">v:pred</ta>
            <ta e="T110" id="Seg_2051" s="T109">np:S</ta>
            <ta e="T111" id="Seg_2052" s="T110">s:adv</ta>
            <ta e="T112" id="Seg_2053" s="T111">np:O</ta>
            <ta e="T114" id="Seg_2054" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_2055" s="T114">np:O</ta>
            <ta e="T116" id="Seg_2056" s="T115">0.3:S v:pred</ta>
            <ta e="T118" id="Seg_2057" s="T117">s:adv</ta>
            <ta e="T119" id="Seg_2058" s="T118">0.3:S v:pred</ta>
            <ta e="T120" id="Seg_2059" s="T119">s:adv</ta>
            <ta e="T122" id="Seg_2060" s="T121">np.h:S</ta>
            <ta e="T124" id="Seg_2061" s="T123">s:adv</ta>
            <ta e="T125" id="Seg_2062" s="T124">v:pred</ta>
            <ta e="T128" id="Seg_2063" s="T127">v:pred</ta>
            <ta e="T131" id="Seg_2064" s="T130">np:S</ta>
            <ta e="T135" id="Seg_2065" s="T134">np:S</ta>
            <ta e="T136" id="Seg_2066" s="T135">v:pred</ta>
            <ta e="T138" id="Seg_2067" s="T137">s:adv</ta>
            <ta e="T142" id="Seg_2068" s="T141">np:S</ta>
            <ta e="T145" id="Seg_2069" s="T144">v:pred</ta>
            <ta e="T147" id="Seg_2070" s="T146">np:O</ta>
            <ta e="T148" id="Seg_2071" s="T147">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_2072" s="T150">s:adv</ta>
            <ta e="T156" id="Seg_2073" s="T155">pro.h:S</ta>
            <ta e="T159" id="Seg_2074" s="T158">s:adv</ta>
            <ta e="T160" id="Seg_2075" s="T159">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_2076" s="T2">RUS:cult</ta>
            <ta e="T13" id="Seg_2077" s="T12">RUS:gram</ta>
            <ta e="T48" id="Seg_2078" s="T47">RUS:cult</ta>
            <ta e="T52" id="Seg_2079" s="T51">RUS:mod</ta>
            <ta e="T57" id="Seg_2080" s="T56">RUS:cult</ta>
            <ta e="T94" id="Seg_2081" s="T93">RUS:cult</ta>
            <ta e="T101" id="Seg_2082" s="T100">RUS:cult</ta>
            <ta e="T125" id="Seg_2083" s="T124">RUS:cult</ta>
            <ta e="T152" id="Seg_2084" s="T151">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T131" id="Seg_2085" s="T125">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_2086" s="T0">Мы пошли было с Ермолаем вдоль берега реки.</ta>
            <ta e="T9" id="Seg_2087" s="T6">Утка — боязливая птица.</ta>
            <ta e="T12" id="Seg_2088" s="T9">На берегу не держится.</ta>
            <ta e="T26" id="Seg_2089" s="T12">Если какого-нибудь молодого чирочка мы и убивали, наши собаки не приносили его из таких зарослей.</ta>
            <ta e="T31" id="Seg_2090" s="T26">Собаки не умели плавать (/плавали) .</ta>
            <ta e="T44" id="Seg_2091" s="T31">Собаки не могли пройти по илистому месту, они только зря резали носы об осоку.</ta>
            <ta e="T51" id="Seg_2092" s="T44">"Нет", — сказал наконец Ермолай, — "дело-то плохо.</ta>
            <ta e="T54" id="Seg_2093" s="T51">Надо найти лодку.</ta>
            <ta e="T57" id="Seg_2094" s="T54">Пойдёмте обратно во Льгов".</ta>
            <ta e="T59" id="Seg_2095" s="T57">Мы пошли.</ta>
            <ta e="T85" id="Seg_2096" s="T59">Мы сделали несколько шагов, нам навстречу из тальника выбежала такая плохая собака, вслед за ней появился невысокого роста человек в синем, изорванном пальтишке.</ta>
            <ta e="T94" id="Seg_2097" s="T85">Штаны красноватые, засунутые кое-как в голенища порванных сапог.</ta>
            <ta e="T98" id="Seg_2098" s="T94">На шее повязан платок.</ta>
            <ta e="T103" id="Seg_2099" s="T98">Одноствольное ружьё он тащит за спиной. </ta>
            <ta e="T108" id="Seg_2100" s="T103">Наши собаки обнюхивали чужую собаку (/начали обнюхивать).</ta>
            <ta e="T120" id="Seg_2101" s="T108">Эта собака трусливо поднимала хвост, шевелила ушами и быстро прыгала, рыча.</ta>
            <ta e="T125" id="Seg_2102" s="T120">Чужой человек, подойдя, с нами поздоровался.</ta>
            <ta e="T131" id="Seg_2103" s="T125">Ему на вид было лет двадцать пять (/будет).</ta>
            <ta e="T138" id="Seg_2104" s="T131">Его длинные черные волосы пахли потом.</ta>
            <ta e="T145" id="Seg_2105" s="T138">Небольшие чёрные глазки часто моргали.</ta>
            <ta e="T155" id="Seg_2106" s="T145">Всё лицо, повязанное черным платком, как будто у него болели зубы.</ta>
            <ta e="T160" id="Seg_2107" s="T155">Он нам радостно улыбался.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_2108" s="T0">Yermolay and I went together along the riverbank.</ta>
            <ta e="T9" id="Seg_2109" s="T6">The duck is a timid bird.</ta>
            <ta e="T12" id="Seg_2110" s="T9">It doesn't stay on the bank.</ta>
            <ta e="T26" id="Seg_2111" s="T12">When we shot a young teal, our dogs couldn't bring it out of the dense overgrowth.</ta>
            <ta e="T31" id="Seg_2112" s="T26">The dogs couldn't swim.</ta>
            <ta e="T44" id="Seg_2113" s="T31">The dogs couldn't go through the mud, they vainly cut their noses at the sedge.</ta>
            <ta e="T51" id="Seg_2114" s="T44">"No", — Yermolay said finally, — "things look bad.</ta>
            <ta e="T54" id="Seg_2115" s="T51">We have to find the boat.</ta>
            <ta e="T57" id="Seg_2116" s="T54">Let's go back to Lʼgov".</ta>
            <ta e="T59" id="Seg_2117" s="T57">We went.</ta>
            <ta e="T85" id="Seg_2118" s="T59">We've made several steps, when a bad dog ran out of the willow towards us, behind it there appeared a little man in a small and ragged blue coat.</ta>
            <ta e="T94" id="Seg_2119" s="T85">His trousers were reddish, they were somehow stuck into his bootlegs.</ta>
            <ta e="T98" id="Seg_2120" s="T94">A red headscarf was tied around his neck.</ta>
            <ta e="T103" id="Seg_2121" s="T98">He was carrying his single-barrel gun on his back.</ta>
            <ta e="T108" id="Seg_2122" s="T103">Our dogs were sniffing at the strange dog (/began to sniffle).</ta>
            <ta e="T120" id="Seg_2123" s="T108">That dog anxiously put its tail between its legs, moved its ears and jumped quickly and growling.</ta>
            <ta e="T125" id="Seg_2124" s="T120">A foreign human came there and greeted us.</ta>
            <ta e="T131" id="Seg_2125" s="T125">He looked like a twenty-five-year-old.</ta>
            <ta e="T138" id="Seg_2126" s="T131">His long black hair smelled of sweat.</ta>
            <ta e="T145" id="Seg_2127" s="T138">His small black eyes often were blinking.</ta>
            <ta e="T155" id="Seg_2128" s="T145">His whole face was tied with a black headscarf, as if he had a toothache.</ta>
            <ta e="T160" id="Seg_2129" s="T155">He was smiling to us happily.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_2130" s="T0">Jermolaj und ich sind entlang des Flussufers gegangen.</ta>
            <ta e="T9" id="Seg_2131" s="T6">Die Ente ist ein ängstlicher Vogel.</ta>
            <ta e="T12" id="Seg_2132" s="T9">Sie bleibt nicht am Ufer.</ta>
            <ta e="T26" id="Seg_2133" s="T12">Als wir eine junge Krickente schossen, konnten unsere Hunde die nicht aus dem Gestrüpp holen.</ta>
            <ta e="T31" id="Seg_2134" s="T26">Die Hunde konnten nicht schwimmen.</ta>
            <ta e="T44" id="Seg_2135" s="T31">Die Hunde konnten nicht durch den Morast, sie schnitten sich ihre Schnauzen vergeblich am Riedgras.</ta>
            <ta e="T51" id="Seg_2136" s="T44">"Nein", — sagte Jermolaj schließlich, — "es sieht schlecht aus.</ta>
            <ta e="T54" id="Seg_2137" s="T51">Wir müssen das Boot finden.</ta>
            <ta e="T57" id="Seg_2138" s="T54">Lass uns zurück nach Lʼgov gehen."</ta>
            <ta e="T59" id="Seg_2139" s="T57">Wir gingen.</ta>
            <ta e="T85" id="Seg_2140" s="T59">Wir hatten ein paar Schritte gemacht, als ein böser Hund aus einer Weide herauskam, hinterher erschien ein kleiner Mann in einem zerrissenen blauen Mäntelchen.</ta>
            <ta e="T94" id="Seg_2141" s="T85">Seine Hose war rötlich, sie war in den Stiefelschaft gesteckt.</ta>
            <ta e="T98" id="Seg_2142" s="T94">Ein rotes Tuch war um seinen Hals gebunden.</ta>
            <ta e="T103" id="Seg_2143" s="T98">Er trug sein einläufiges Gewehr auf dem Rücken.</ta>
            <ta e="T108" id="Seg_2144" s="T103">Unsere Hunde beschnupperten den fremden Hund (/fingen an zu beschnuppern).</ta>
            <ta e="T120" id="Seg_2145" s="T108">Dieser Hund zog ängstlich seinen Schwanz ein, bewegte seine Ohren und sprang schnell und knurrend.</ta>
            <ta e="T125" id="Seg_2146" s="T120">Ein fremder Mann kam und begrüßte uns.</ta>
            <ta e="T131" id="Seg_2147" s="T125">Er sah aus wie ein Fünfundzwanzigjähriger.</ta>
            <ta e="T138" id="Seg_2148" s="T131">Seine langen schwarzen Haare rochen nach Schweiß.</ta>
            <ta e="T145" id="Seg_2149" s="T138">Seine kleinen schwarzen Augen blinzelten oft.</ta>
            <ta e="T155" id="Seg_2150" s="T145">Sein ganzes Gesicht war mit einem schwarzen Tuch umwickelt, als ob er Zahnschmerzen hätte.</ta>
            <ta e="T160" id="Seg_2151" s="T155">Er lächelte uns glücklich an.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_2152" s="T0">мы пошли было с Ер-ем вдоль (берега) речки</ta>
            <ta e="T9" id="Seg_2153" s="T6">утка боязливая птица</ta>
            <ta e="T12" id="Seg_2154" s="T9">на берегу не держится</ta>
            <ta e="T26" id="Seg_2155" s="T12">если какой нибудь молодой чирочек мы и стреляли наши собаки не приносили из столь таких зарослей.</ta>
            <ta e="T31" id="Seg_2156" s="T26">собаки не умели плавать.</ta>
            <ta e="T44" id="Seg_2157" s="T31">собаки по илистому месту пройти не могли они только зря носы об заросли (осока болотная) резал</ta>
            <ta e="T51" id="Seg_2158" s="T44">нет сказал наконец Ермолай дело-то плохо</ta>
            <ta e="T54" id="Seg_2159" s="T51">надо найти лодку</ta>
            <ta e="T57" id="Seg_2160" s="T54">Пойдемте обратно во Льгов.</ta>
            <ta e="T59" id="Seg_2161" s="T57">мы пошли</ta>
            <ta e="T85" id="Seg_2162" s="T59">мы немного шагов сделали, нам навстречу из тальника кустарника выбежала плохая такая собачья уродина. вслед за ней появился не высокого роста человек синее пальтишко его сильно порватом</ta>
            <ta e="T94" id="Seg_2163" s="T85">штаны красноватые засунутые кое-как в голенища порванных сапог.</ta>
            <ta e="T98" id="Seg_2164" s="T94">на шее повязан [обвит] красным платком</ta>
            <ta e="T103" id="Seg_2165" s="T98">одноствольное ружье за спиной тащит.</ta>
            <ta e="T108" id="Seg_2166" s="T103">наши собаки чужую собаку начали нюхать [обнюхивать].</ta>
            <ta e="T120" id="Seg_2167" s="T108">эта собака трусливо хвост поджимала уши поднимала (шевелила) и быстро прыгала, рычавшись [рычала].</ta>
            <ta e="T125" id="Seg_2168" s="T120">чужой человек к нам подойдя поздоровался.</ta>
            <ta e="T131" id="Seg_2169" s="T125">ему на вид будет 20 пять лет.</ta>
            <ta e="T138" id="Seg_2170" s="T131">его длинные черные волосы пахнут [пахли] потом.</ta>
            <ta e="T145" id="Seg_2171" s="T138">не большие черные глазки часто моргали.</ta>
            <ta e="T155" id="Seg_2172" s="T145">все лицо повязано черным платком, как будто зубы болят [болели]</ta>
            <ta e="T160" id="Seg_2173" s="T155">он нам так радостно смеялся (улыбался)</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T26" id="Seg_2174" s="T12">[OSV:] The speaker alternates between Present and Past tenses.</ta>
            <ta e="T85" id="Seg_2175" s="T59">[OSV:] not sure in glossing of the form "ašʼena" (possible interpretation is "ašša naj" (not EMPH) but it is questionable if an emphatic clitic can be used with the negative particle.</ta>
            <ta e="T94" id="Seg_2176" s="T85">[OSV:] The participle form "nɨtɨpɨlʼ" has been edited into "nɨttɨpɨlʼ" .</ta>
            <ta e="T103" id="Seg_2177" s="T98">[OSV:] unclear form "šʼintʼij" (according to IES: "šʼittɨlʼ"), a possible interpretation could be "šünʼčʼɨ-lʼ" (inside-ADJ).</ta>
            <ta e="T125" id="Seg_2178" s="T120">[OSV:] The verbal form "qəttɨlʼa"has been edited into "qəntɨlʼa".</ta>
            <ta e="T131" id="Seg_2179" s="T125">[OSV:] The translation is not correct, in the original text it is: "He looked like he was twenty five years old".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
