<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KIA_1965_Petro_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KIA_1965_Petro_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">233</ud-information>
            <ud-information attribute-name="# HIAT:w">166</ud-information>
            <ud-information attribute-name="# e">166</ud-information>
            <ud-information attribute-name="# HIAT:u">29</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KIA">
            <abbreviation>KIA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KIA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T167" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Petro</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">omtɨttɛːmpa</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Nɨːnɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">mäčʼčʼimpat</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">poːjam</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">qənqɨntoːqa</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">šitɨmtʼälʼi</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">aj</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">moqona</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">pɔːralpa</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">mɔːtqɨntɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Täm</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">pit</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">čʼontoːqɨn</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">püqəltɨmpat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">nʼäŋɨčʼa</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">utɨsʼa</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">nɨttɨpij</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <nts id="Seg_65" n="HIAT:ip">/</nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">sʼäpalɨpij</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">mɔːtam</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">nɨːnɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">kɨpɨkä</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">tukaptɨmpat</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_85" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">Nɔːtɨ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">tıpinʼä</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">man</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">qəntɨl</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">orɨnʼnʼaŋ</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_104" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_106" n="HIAT:w" s="T28">Minut</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_109" n="HIAT:w" s="T29">kunte</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_112" n="HIAT:w" s="T30">üŋkɨltɨmɨmpat</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">kɨpäka</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_119" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_121" n="HIAT:w" s="T32">Nɨːn</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">aj</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">šentɨŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_130" n="HIAT:w" s="T35">püqɨltɨkkijoimpelɨmpat</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">na</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">nʼuːtə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">mɔːtam</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">qatalpɨtij</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">pötpɨqɨntoːqa</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Näkalʼa</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">qɨːqɨlʼlʼa</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">aj</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">sümɨkɔːlɨŋ</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">kätɨmpat</ts>
                  <nts id="Seg_164" n="HIAT:ip">:</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">Man</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">na</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">qəntaŋ</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_177" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_179" n="HIAT:w" s="T49">Nɨːnɨ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">kuralpa</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">pona</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_189" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">Täp</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">čʼäːŋkɨ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">üntɨjıːmpat</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_201" n="HIAT:w" s="T55">kuttar</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">mɔːta</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_207" n="HIAT:w" s="T57">tıːqɨmɔːtpa</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_211" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">Nɨːnɨ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">čʼäːŋkɨ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">qontɨrrɛːtɨ</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">kuttar</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_226" n="HIAT:w" s="T62">täpɨm</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_229" n="HIAT:w" s="T63">nʼoːmpat</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">nʼäŋɨčʼa</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_235" n="HIAT:w" s="T65">topɨsʼa</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_238" n="HIAT:w" s="T66">sɨroːmɨn</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_242" n="HIAT:w" s="T67">nʼoːmpat</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_245" n="HIAT:w" s="T68">mittə</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_248" n="HIAT:w" s="T69">nʼuːtoːqɨn</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_251" n="HIAT:w" s="T70">Ivan</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_255" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_257" n="HIAT:w" s="T71">Na</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_260" n="HIAT:w" s="T72">kuttar</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_263" n="HIAT:w" s="T73">ɛsimmantɨ</ts>
                  <nts id="Seg_264" n="HIAT:ip">?</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_267" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_269" n="HIAT:w" s="T74">Qaj</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_272" n="HIAT:w" s="T75">qɨntɨl</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_275" n="HIAT:w" s="T76">orɨnʼnʼantɨ</ts>
                  <nts id="Seg_276" n="HIAT:ip">?</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_279" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_281" n="HIAT:w" s="T77">Ukoːn</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_284" n="HIAT:w" s="T78">taŋaltɨŋ</ts>
                  <nts id="Seg_285" n="HIAT:ip">!</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_288" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_290" n="HIAT:w" s="T79">Tolʼ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_293" n="HIAT:w" s="T80">mɔːtqɨn</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <nts id="Seg_296" n="HIAT:ip">/</nts>
                  <ts e="T82" id="Seg_298" n="HIAT:w" s="T81">toj</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">mɔːtqɨn</ts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_305" n="HIAT:w" s="T83">qontɔːmɨn</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_308" n="HIAT:w" s="T84">nʼäim</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_312" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_314" n="HIAT:w" s="T85">Mättoːqɨn</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_317" n="HIAT:w" s="T86">amɨrq</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_320" n="HIAT:w" s="T87">ɛjsenantɨ</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_324" n="HIAT:u" s="T88">
                  <nts id="Seg_325" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_327" n="HIAT:w" s="T88">Tıpɨnʼäqɨntɨ</ts>
                  <nts id="Seg_328" n="HIAT:ip">)</nts>
                  <nts id="Seg_329" n="HIAT:ip">:</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_332" n="HIAT:w" s="T89">Tıpɨnʼa</ts>
                  <nts id="Seg_333" n="HIAT:ip">,</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">man</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_339" n="HIAT:w" s="T91">aša</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_342" n="HIAT:w" s="T92">kɨkam</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_346" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_348" n="HIAT:w" s="T93">Onäntɨ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">qumɨiːqantɨ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">qəːčʼat</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_358" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_360" n="HIAT:w" s="T96">Meː</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_363" n="HIAT:w" s="T97">qumɨtɨt</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_366" n="HIAT:w" s="T98">puːtoːqɨn</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_369" n="HIAT:w" s="T99">aša</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_372" n="HIAT:w" s="T100">quntɔːmɨn</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip">—</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">kätɨmpat</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">Petro</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_384" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_386" n="HIAT:w" s="T103">Nɨːnɨ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">iːmpat</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">utoːqɨntɨ</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">muqɨlsimi</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_398" n="HIAT:w" s="T107">äːqaim</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_402" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_404" n="HIAT:w" s="T108">Ivan</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_407" n="HIAT:w" s="T109">qaj</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_410" n="HIAT:w" s="T110">lɨpɨk</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_413" n="HIAT:w" s="T111">ɛːptaːntoːqa</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_416" n="HIAT:w" s="T112">čʼekaptɨlʼa</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_419" n="HIAT:w" s="T113">nıːtɨmpat</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_422" n="HIAT:w" s="T114">tıpɨnʼämtɨ</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_425" n="HIAT:w" s="T115">kočʼčʼi</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_428" n="HIAT:w" s="T116">pɔːrə</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_431" n="HIAT:w" s="T117">puːtɨlmɨntɨ</ts>
                  <nts id="Seg_432" n="HIAT:ip">,</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_435" n="HIAT:w" s="T118">qɔːnmɨntɨ</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_439" n="HIAT:w" s="T119">inčʼajmɨntɨ</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_443" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_445" n="HIAT:w" s="T120">Tıːtam</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">qaj</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">ne</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_454" n="HIAT:w" s="T123">qontɨrtɛntɔːmɨn</ts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip">—</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_459" n="HIAT:w" s="T124">kätɨmpat</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_462" n="HIAT:w" s="T125">täm</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_466" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_468" n="HIAT:w" s="T126">Ivan</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_471" n="HIAT:w" s="T127">noqqɔːlpat</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_473" n="HIAT:ip">(</nts>
                  <nts id="Seg_474" n="HIAT:ip">/</nts>
                  <ts e="T129" id="Seg_476" n="HIAT:w" s="T128">noqqɔːlnɨt</ts>
                  <nts id="Seg_477" n="HIAT:ip">)</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_480" n="HIAT:w" s="T129">qaqlɨmtɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_484" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_486" n="HIAT:w" s="T130">Nɨːnɨ</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_489" n="HIAT:w" s="T131">čʼuntɨ</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_492" n="HIAT:w" s="T132">kurlarnɨ</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_496" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_498" n="HIAT:w" s="T133">Petro</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">qäjlɨmɨmpa</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_503" n="HIAT:ip">(</nts>
                  <nts id="Seg_504" n="HIAT:ip">/</nts>
                  <ts e="T136" id="Seg_506" n="HIAT:w" s="T135">qäjlɨmpa</ts>
                  <nts id="Seg_507" n="HIAT:ip">)</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_510" n="HIAT:w" s="T136">qaqlɨntɨ</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_513" n="HIAT:w" s="T137">qöqɨn</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_517" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_519" n="HIAT:w" s="T138">Ivan</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">namɨšaŋ</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_525" n="HIAT:w" s="T140">nɨŋɨmpa</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_527" n="HIAT:ip">[</nts>
                  <ts e="T142" id="Seg_529" n="HIAT:w" s="T141">nɨŋa</ts>
                  <nts id="Seg_530" n="HIAT:ip">]</nts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_534" n="HIAT:w" s="T142">kunte</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">nɨŋɨmpa</ts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_541" n="HIAT:w" s="T144">kuttar</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_544" n="HIAT:w" s="T145">qaqlɨntɨ</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_547" n="HIAT:w" s="T146">tıːqɨmpɨptäː</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_550" n="HIAT:w" s="T147">aša</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_553" n="HIAT:w" s="T148">ürrʼɛıːmpa</ts>
                  <nts id="Seg_554" n="HIAT:ip">.</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_557" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_559" n="HIAT:w" s="T150">Qaptičʼčʼikka</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_562" n="HIAT:w" s="T151">tar</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_565" n="HIAT:w" s="T152">aša</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_568" n="HIAT:w" s="T153">qaptämpa</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_572" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_574" n="HIAT:w" s="T154">Täp</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_577" n="HIAT:w" s="T155">očʼik</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_580" n="HIAT:w" s="T156">čʼɔːppɛːmpa</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_582" n="HIAT:ip">(</nts>
                  <nts id="Seg_583" n="HIAT:ip">/</nts>
                  <ts e="T158" id="Seg_585" n="HIAT:w" s="T157">čʼɔːttɛːmpat</ts>
                  <nts id="Seg_586" n="HIAT:ip">)</nts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_590" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_592" n="HIAT:w" s="T158">Ivan</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_595" n="HIAT:w" s="T159">mɔːt</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_598" n="HIAT:w" s="T160">seːrpa</ts>
                  <nts id="Seg_599" n="HIAT:ip">,</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_602" n="HIAT:w" s="T161">ılla</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_605" n="HIAT:w" s="T162">qaptoqontoːqɨn</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_609" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_611" n="HIAT:w" s="T163">Qaptičʼčʼika</ts>
                  <nts id="Seg_612" n="HIAT:ip">,</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_615" n="HIAT:w" s="T164">nan</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_618" n="HIAT:w" s="T165">ɛj</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_621" n="HIAT:w" s="T166">čʼɔːpa</ts>
                  <nts id="Seg_622" n="HIAT:ip">.</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T167" id="Seg_624" n="sc" s="T0">
               <ts e="T1" id="Seg_626" n="e" s="T0">Petro </ts>
               <ts e="T2" id="Seg_628" n="e" s="T1">omtɨttɛːmpa. </ts>
               <ts e="T3" id="Seg_630" n="e" s="T2">Nɨːnɨ </ts>
               <ts e="T4" id="Seg_632" n="e" s="T3">mäčʼčʼimpat </ts>
               <ts e="T5" id="Seg_634" n="e" s="T4">poːjam </ts>
               <ts e="T6" id="Seg_636" n="e" s="T5">qənqɨntoːqa, </ts>
               <ts e="T7" id="Seg_638" n="e" s="T6">šitɨmtʼälʼi </ts>
               <ts e="T8" id="Seg_640" n="e" s="T7">aj </ts>
               <ts e="T9" id="Seg_642" n="e" s="T8">moqona </ts>
               <ts e="T10" id="Seg_644" n="e" s="T9">pɔːralpa </ts>
               <ts e="T11" id="Seg_646" n="e" s="T10">mɔːtqɨntɨ. </ts>
               <ts e="T12" id="Seg_648" n="e" s="T11">Täm </ts>
               <ts e="T13" id="Seg_650" n="e" s="T12">pit </ts>
               <ts e="T14" id="Seg_652" n="e" s="T13">čʼontoːqɨn </ts>
               <ts e="T15" id="Seg_654" n="e" s="T14">püqəltɨmpat </ts>
               <ts e="T16" id="Seg_656" n="e" s="T15">nʼäŋɨčʼa </ts>
               <ts e="T17" id="Seg_658" n="e" s="T16">utɨsʼa </ts>
               <ts e="T18" id="Seg_660" n="e" s="T17">nɨttɨpij </ts>
               <ts e="T19" id="Seg_662" n="e" s="T18">(/sʼäpalɨpij) </ts>
               <ts e="T20" id="Seg_664" n="e" s="T19">mɔːtam, </ts>
               <ts e="T21" id="Seg_666" n="e" s="T20">nɨːnɨ </ts>
               <ts e="T22" id="Seg_668" n="e" s="T21">kɨpɨkä </ts>
               <ts e="T23" id="Seg_670" n="e" s="T22">tukaptɨmpat. </ts>
               <ts e="T24" id="Seg_672" n="e" s="T23">Nɔːtɨ </ts>
               <ts e="T25" id="Seg_674" n="e" s="T24">tıpinʼä, </ts>
               <ts e="T26" id="Seg_676" n="e" s="T25">man </ts>
               <ts e="T27" id="Seg_678" n="e" s="T26">qəntɨl </ts>
               <ts e="T28" id="Seg_680" n="e" s="T27">orɨnʼnʼaŋ. </ts>
               <ts e="T29" id="Seg_682" n="e" s="T28">Minut </ts>
               <ts e="T30" id="Seg_684" n="e" s="T29">kunte </ts>
               <ts e="T31" id="Seg_686" n="e" s="T30">üŋkɨltɨmɨmpat </ts>
               <ts e="T32" id="Seg_688" n="e" s="T31">kɨpäka. </ts>
               <ts e="T33" id="Seg_690" n="e" s="T32">Nɨːn </ts>
               <ts e="T34" id="Seg_692" n="e" s="T33">aj </ts>
               <ts e="T35" id="Seg_694" n="e" s="T34">šentɨŋ </ts>
               <ts e="T36" id="Seg_696" n="e" s="T35">püqɨltɨkkijoimpelɨmpat </ts>
               <ts e="T37" id="Seg_698" n="e" s="T36">na </ts>
               <ts e="T38" id="Seg_700" n="e" s="T37">nʼuːtə </ts>
               <ts e="T39" id="Seg_702" n="e" s="T38">mɔːtam </ts>
               <ts e="T40" id="Seg_704" n="e" s="T39">qatalpɨtij </ts>
               <ts e="T41" id="Seg_706" n="e" s="T40">pötpɨqɨntoːqa. </ts>
               <ts e="T42" id="Seg_708" n="e" s="T41">Näkalʼa </ts>
               <ts e="T43" id="Seg_710" n="e" s="T42">qɨːqɨlʼlʼa </ts>
               <ts e="T44" id="Seg_712" n="e" s="T43">aj </ts>
               <ts e="T45" id="Seg_714" n="e" s="T44">sümɨkɔːlɨŋ </ts>
               <ts e="T46" id="Seg_716" n="e" s="T45">kätɨmpat: </ts>
               <ts e="T47" id="Seg_718" n="e" s="T46">Man </ts>
               <ts e="T48" id="Seg_720" n="e" s="T47">na </ts>
               <ts e="T49" id="Seg_722" n="e" s="T48">qəntaŋ. </ts>
               <ts e="T50" id="Seg_724" n="e" s="T49">Nɨːnɨ </ts>
               <ts e="T51" id="Seg_726" n="e" s="T50">kuralpa </ts>
               <ts e="T52" id="Seg_728" n="e" s="T51">pona. </ts>
               <ts e="T53" id="Seg_730" n="e" s="T52">Täp </ts>
               <ts e="T54" id="Seg_732" n="e" s="T53">čʼäːŋkɨ </ts>
               <ts e="T55" id="Seg_734" n="e" s="T54">üntɨjıːmpat, </ts>
               <ts e="T56" id="Seg_736" n="e" s="T55">kuttar </ts>
               <ts e="T57" id="Seg_738" n="e" s="T56">mɔːta </ts>
               <ts e="T58" id="Seg_740" n="e" s="T57">tıːqɨmɔːtpa. </ts>
               <ts e="T59" id="Seg_742" n="e" s="T58">Nɨːnɨ </ts>
               <ts e="T60" id="Seg_744" n="e" s="T59">čʼäːŋkɨ </ts>
               <ts e="T61" id="Seg_746" n="e" s="T60">qontɨrrɛːtɨ, </ts>
               <ts e="T62" id="Seg_748" n="e" s="T61">kuttar </ts>
               <ts e="T63" id="Seg_750" n="e" s="T62">täpɨm </ts>
               <ts e="T64" id="Seg_752" n="e" s="T63">nʼoːmpat </ts>
               <ts e="T65" id="Seg_754" n="e" s="T64">nʼäŋɨčʼa </ts>
               <ts e="T66" id="Seg_756" n="e" s="T65">topɨsʼa </ts>
               <ts e="T67" id="Seg_758" n="e" s="T66">sɨroːmɨn, </ts>
               <ts e="T68" id="Seg_760" n="e" s="T67">nʼoːmpat </ts>
               <ts e="T69" id="Seg_762" n="e" s="T68">mittə </ts>
               <ts e="T70" id="Seg_764" n="e" s="T69">nʼuːtoːqɨn </ts>
               <ts e="T71" id="Seg_766" n="e" s="T70">Ivan. </ts>
               <ts e="T72" id="Seg_768" n="e" s="T71">Na </ts>
               <ts e="T73" id="Seg_770" n="e" s="T72">kuttar </ts>
               <ts e="T74" id="Seg_772" n="e" s="T73">ɛsimmantɨ? </ts>
               <ts e="T75" id="Seg_774" n="e" s="T74">Qaj </ts>
               <ts e="T76" id="Seg_776" n="e" s="T75">qɨntɨl </ts>
               <ts e="T77" id="Seg_778" n="e" s="T76">orɨnʼnʼantɨ? </ts>
               <ts e="T78" id="Seg_780" n="e" s="T77">Ukoːn </ts>
               <ts e="T79" id="Seg_782" n="e" s="T78">taŋaltɨŋ! </ts>
               <ts e="T80" id="Seg_784" n="e" s="T79">Tolʼ </ts>
               <ts e="T81" id="Seg_786" n="e" s="T80">mɔːtqɨn </ts>
               <ts e="T82" id="Seg_788" n="e" s="T81">(/toj </ts>
               <ts e="T83" id="Seg_790" n="e" s="T82">mɔːtqɨn) </ts>
               <ts e="T84" id="Seg_792" n="e" s="T83">qontɔːmɨn </ts>
               <ts e="T85" id="Seg_794" n="e" s="T84">nʼäim. </ts>
               <ts e="T86" id="Seg_796" n="e" s="T85">Mättoːqɨn </ts>
               <ts e="T87" id="Seg_798" n="e" s="T86">amɨrq </ts>
               <ts e="T88" id="Seg_800" n="e" s="T87">ɛjsenantɨ. </ts>
               <ts e="T89" id="Seg_802" n="e" s="T88">(Tıpɨnʼäqɨntɨ): </ts>
               <ts e="T90" id="Seg_804" n="e" s="T89">Tıpɨnʼa, </ts>
               <ts e="T91" id="Seg_806" n="e" s="T90">man </ts>
               <ts e="T92" id="Seg_808" n="e" s="T91">aša </ts>
               <ts e="T93" id="Seg_810" n="e" s="T92">kɨkam. </ts>
               <ts e="T94" id="Seg_812" n="e" s="T93">Onäntɨ </ts>
               <ts e="T95" id="Seg_814" n="e" s="T94">qumɨiːqantɨ </ts>
               <ts e="T96" id="Seg_816" n="e" s="T95">qəːčʼat. </ts>
               <ts e="T97" id="Seg_818" n="e" s="T96">Meː </ts>
               <ts e="T98" id="Seg_820" n="e" s="T97">qumɨtɨt </ts>
               <ts e="T99" id="Seg_822" n="e" s="T98">puːtoːqɨn </ts>
               <ts e="T100" id="Seg_824" n="e" s="T99">aša </ts>
               <ts e="T101" id="Seg_826" n="e" s="T100">quntɔːmɨn,— </ts>
               <ts e="T102" id="Seg_828" n="e" s="T101">kätɨmpat </ts>
               <ts e="T103" id="Seg_830" n="e" s="T102">Petro. </ts>
               <ts e="T104" id="Seg_832" n="e" s="T103">Nɨːnɨ </ts>
               <ts e="T105" id="Seg_834" n="e" s="T104">iːmpat </ts>
               <ts e="T106" id="Seg_836" n="e" s="T105">utoːqɨntɨ </ts>
               <ts e="T107" id="Seg_838" n="e" s="T106">muqɨlsimi </ts>
               <ts e="T108" id="Seg_840" n="e" s="T107">äːqaim. </ts>
               <ts e="T109" id="Seg_842" n="e" s="T108">Ivan </ts>
               <ts e="T110" id="Seg_844" n="e" s="T109">qaj </ts>
               <ts e="T111" id="Seg_846" n="e" s="T110">lɨpɨk </ts>
               <ts e="T112" id="Seg_848" n="e" s="T111">ɛːptaːntoːqa </ts>
               <ts e="T113" id="Seg_850" n="e" s="T112">čʼekaptɨlʼa </ts>
               <ts e="T114" id="Seg_852" n="e" s="T113">nıːtɨmpat </ts>
               <ts e="T115" id="Seg_854" n="e" s="T114">tıpɨnʼämtɨ </ts>
               <ts e="T116" id="Seg_856" n="e" s="T115">kočʼčʼi </ts>
               <ts e="T117" id="Seg_858" n="e" s="T116">pɔːrə </ts>
               <ts e="T118" id="Seg_860" n="e" s="T117">puːtɨlmɨntɨ, </ts>
               <ts e="T119" id="Seg_862" n="e" s="T118">qɔːnmɨntɨ, </ts>
               <ts e="T120" id="Seg_864" n="e" s="T119">inčʼajmɨntɨ. </ts>
               <ts e="T121" id="Seg_866" n="e" s="T120">Tıːtam </ts>
               <ts e="T122" id="Seg_868" n="e" s="T121">qaj </ts>
               <ts e="T123" id="Seg_870" n="e" s="T122">ne </ts>
               <ts e="T124" id="Seg_872" n="e" s="T123">qontɨrtɛntɔːmɨn,— </ts>
               <ts e="T125" id="Seg_874" n="e" s="T124">kätɨmpat </ts>
               <ts e="T126" id="Seg_876" n="e" s="T125">täm. </ts>
               <ts e="T127" id="Seg_878" n="e" s="T126">Ivan </ts>
               <ts e="T128" id="Seg_880" n="e" s="T127">noqqɔːlpat </ts>
               <ts e="T129" id="Seg_882" n="e" s="T128">(/noqqɔːlnɨt) </ts>
               <ts e="T130" id="Seg_884" n="e" s="T129">qaqlɨmtɨ. </ts>
               <ts e="T131" id="Seg_886" n="e" s="T130">Nɨːnɨ </ts>
               <ts e="T132" id="Seg_888" n="e" s="T131">čʼuntɨ </ts>
               <ts e="T133" id="Seg_890" n="e" s="T132">kurlarnɨ. </ts>
               <ts e="T134" id="Seg_892" n="e" s="T133">Petro </ts>
               <ts e="T135" id="Seg_894" n="e" s="T134">qäjlɨmɨmpa </ts>
               <ts e="T136" id="Seg_896" n="e" s="T135">(/qäjlɨmpa) </ts>
               <ts e="T137" id="Seg_898" n="e" s="T136">qaqlɨntɨ </ts>
               <ts e="T138" id="Seg_900" n="e" s="T137">qöqɨn. </ts>
               <ts e="T139" id="Seg_902" n="e" s="T138">Ivan </ts>
               <ts e="T140" id="Seg_904" n="e" s="T139">namɨšaŋ </ts>
               <ts e="T141" id="Seg_906" n="e" s="T140">nɨŋɨmpa </ts>
               <ts e="T142" id="Seg_908" n="e" s="T141">[nɨŋa], </ts>
               <ts e="T143" id="Seg_910" n="e" s="T142">kunte </ts>
               <ts e="T144" id="Seg_912" n="e" s="T143">nɨŋɨmpa, </ts>
               <ts e="T145" id="Seg_914" n="e" s="T144">kuttar </ts>
               <ts e="T146" id="Seg_916" n="e" s="T145">qaqlɨntɨ </ts>
               <ts e="T147" id="Seg_918" n="e" s="T146">tıːqɨmpɨptäː </ts>
               <ts e="T148" id="Seg_920" n="e" s="T147">aša </ts>
               <ts e="T150" id="Seg_922" n="e" s="T148">ürrʼɛıːmpa. </ts>
               <ts e="T151" id="Seg_924" n="e" s="T150">Qaptičʼčʼikka </ts>
               <ts e="T152" id="Seg_926" n="e" s="T151">tar </ts>
               <ts e="T153" id="Seg_928" n="e" s="T152">aša </ts>
               <ts e="T154" id="Seg_930" n="e" s="T153">qaptämpa. </ts>
               <ts e="T155" id="Seg_932" n="e" s="T154">Täp </ts>
               <ts e="T156" id="Seg_934" n="e" s="T155">očʼik </ts>
               <ts e="T157" id="Seg_936" n="e" s="T156">čʼɔːppɛːmpa </ts>
               <ts e="T158" id="Seg_938" n="e" s="T157">(/čʼɔːttɛːmpat). </ts>
               <ts e="T159" id="Seg_940" n="e" s="T158">Ivan </ts>
               <ts e="T160" id="Seg_942" n="e" s="T159">mɔːt </ts>
               <ts e="T161" id="Seg_944" n="e" s="T160">seːrpa, </ts>
               <ts e="T162" id="Seg_946" n="e" s="T161">ılla </ts>
               <ts e="T163" id="Seg_948" n="e" s="T162">qaptoqontoːqɨn. </ts>
               <ts e="T164" id="Seg_950" n="e" s="T163">Qaptičʼčʼika, </ts>
               <ts e="T165" id="Seg_952" n="e" s="T164">nan </ts>
               <ts e="T166" id="Seg_954" n="e" s="T165">ɛj </ts>
               <ts e="T167" id="Seg_956" n="e" s="T166">čʼɔːpa. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_957" s="T0">KIA_1965_Petro_transl.001 (001.001)</ta>
            <ta e="T11" id="Seg_958" s="T2">KIA_1965_Petro_transl.002 (001.002)</ta>
            <ta e="T23" id="Seg_959" s="T11">KIA_1965_Petro_transl.003 (001.003)</ta>
            <ta e="T28" id="Seg_960" s="T23">KIA_1965_Petro_transl.004 (001.004)</ta>
            <ta e="T32" id="Seg_961" s="T28">KIA_1965_Petro_transl.005 (001.005)</ta>
            <ta e="T41" id="Seg_962" s="T32">KIA_1965_Petro_transl.006 (001.006)</ta>
            <ta e="T49" id="Seg_963" s="T41">KIA_1965_Petro_transl.007 (001.007)</ta>
            <ta e="T52" id="Seg_964" s="T49">KIA_1965_Petro_transl.008 (001.008)</ta>
            <ta e="T58" id="Seg_965" s="T52">KIA_1965_Petro_transl.009 (001.009)</ta>
            <ta e="T71" id="Seg_966" s="T58">KIA_1965_Petro_transl.010 (001.010)</ta>
            <ta e="T74" id="Seg_967" s="T71">KIA_1965_Petro_transl.011 (001.011)</ta>
            <ta e="T77" id="Seg_968" s="T74">KIA_1965_Petro_transl.012 (001.012)</ta>
            <ta e="T79" id="Seg_969" s="T77">KIA_1965_Petro_transl.013 (001.013)</ta>
            <ta e="T85" id="Seg_970" s="T79">KIA_1965_Petro_transl.014 (001.014)</ta>
            <ta e="T88" id="Seg_971" s="T85">KIA_1965_Petro_transl.015 (001.015)</ta>
            <ta e="T93" id="Seg_972" s="T88">KIA_1965_Petro_transl.016 (001.016)</ta>
            <ta e="T96" id="Seg_973" s="T93">KIA_1965_Petro_transl.017 (001.017)</ta>
            <ta e="T103" id="Seg_974" s="T96">KIA_1965_Petro_transl.018 (001.018)</ta>
            <ta e="T108" id="Seg_975" s="T103">KIA_1965_Petro_transl.019 (001.019)</ta>
            <ta e="T120" id="Seg_976" s="T108">KIA_1965_Petro_transl.020 (001.020)</ta>
            <ta e="T126" id="Seg_977" s="T120">KIA_1965_Petro_transl.021 (001.021)</ta>
            <ta e="T130" id="Seg_978" s="T126">KIA_1965_Petro_transl.022 (001.022)</ta>
            <ta e="T133" id="Seg_979" s="T130">KIA_1965_Petro_transl.023 (001.023)</ta>
            <ta e="T138" id="Seg_980" s="T133">KIA_1965_Petro_transl.024 (001.024)</ta>
            <ta e="T150" id="Seg_981" s="T138">KIA_1965_Petro_transl.025 (001.025)</ta>
            <ta e="T154" id="Seg_982" s="T150">KIA_1965_Petro_transl.026 (001.026)</ta>
            <ta e="T158" id="Seg_983" s="T154">KIA_1965_Petro_transl.027 (001.027)</ta>
            <ta e="T163" id="Seg_984" s="T158">KIA_1965_Petro_transl.028 (001.028)</ta>
            <ta e="T167" id="Seg_985" s="T163">KIA_1965_Petro_transl.029 (001.029)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_986" s="T0">пет′ро ′омт̂ытэмб̂[п]а.</ta>
            <ta e="T11" id="Seg_987" s="T2">ныны ′мӓтчимпат поjам ′kъ̊̄нkынтоkа, ′шʼидымдʼелʼи ай ′моkона ′по̄ралб̂а мо̄тkынты.</ta>
            <ta e="T23" id="Seg_988" s="T11">тӓм ′питчонд̂оkын ′пӱ̄ɣо[ъ]лд̂ымпат ′нʼӓңыча ′ӯтысʼа ′ныттыпий [сʼӓ′палыпий] ′мо̄там ныны кы′пыкӓ ′тукаптымб̂ат.</ta>
            <ta e="T28" id="Seg_989" s="T23">“′ноты ′типинʼӓ, ман ′kъ̊̄нд̂ылорынʼнʼаң.”</ta>
            <ta e="T32" id="Seg_990" s="T28">ми′нуткунде ′ӱңкылтымымпат ′кыпӓка.</ta>
            <ta e="T41" id="Seg_991" s="T32">ны′най ′щʼентың[к] пӱ̄kылтыки′jоим′пелым‵пат. на нʼӱтъ ′мо̄там kа′талпытий ′пӧтпыkын‵тоɣ[k]а.</ta>
            <ta e="T49" id="Seg_992" s="T41">не̨′каllа ′kы̄ɣылʼлʼа ай ′сӱ̄мыkолың ′kӓтымб̂ат ман ′на kъ̊̄′нд̂аң.</ta>
            <ta e="T52" id="Seg_993" s="T49">ныны ку′раlб̂а ′пона.</ta>
            <ta e="T58" id="Seg_994" s="T52">тӓп ′чӓңгы ӱн′ты[j]импат ′куттар мо̄та ′тикkымо̄тпа.</ta>
            <ta e="T71" id="Seg_995" s="T58">ныны ′чӓңг̂[k]ы ′kонтыр′ре̄ты ′куттар ′тӓпым ′нʼомп[б̂]ат ′нʼӓңыча ′топысʼа съ[ы]′ромын ′нʼомп[б̂]ат ′миттъ ′нӱ̄д̂оɣын Иван.</ta>
            <ta e="T74" id="Seg_996" s="T71">на ′куттар ′е̨з̂иманты?</ta>
            <ta e="T77" id="Seg_997" s="T74">kай ′kындыlорыннʼанты. </ta>
            <ta e="T79" id="Seg_998" s="T77">у′кон та′ңаlтың!</ta>
            <ta e="T85" id="Seg_999" s="T79">′то̄lмотkын [′тоймотkын] kон′домын ′нʼӓим.</ta>
            <ta e="T88" id="Seg_1000" s="T85">′мӓт‵то̄kын ′амырkейсенанты.</ta>
            <ta e="T93" id="Seg_1001" s="T88">(типы′нʼӓkынты) ′типынʼа ман ашʼа кы′кам.</ta>
            <ta e="T96" id="Seg_1002" s="T93">о̄′нӓнды ′kумы′иkанты ′kы̄[ъ̊̄]чат.</ta>
            <ta e="T103" id="Seg_1003" s="T96">ме ′kумытыт ′пӯтоɣын ′аща ‵kун′домын ′kӓтымбат Петро.</ta>
            <ta e="T108" id="Seg_1004" s="T103">ныны ′ӣмпат у′тоkынты ′муkылсими ′ӓ̄kаим.</ta>
            <ta e="T120" id="Seg_1005" s="T108">Иван kай ′лыпы‵кʼептан′тоɣа че̄k[к]аптылʼа ′нид̂ымб̂ат ′тӣпи[ы]‵нʼӓмты ′коччипаръ ′пӯтылмынты, kонмынты, ′инчаймынты.</ta>
            <ta e="T126" id="Seg_1006" s="T120">′тӣд̂ам kай не ′kонд̂ыртенд̂амын. ′kӓтымбат тӓм.</ta>
            <ta e="T130" id="Seg_1007" s="T126">Иван но′kkолб̂ат [ноkkолныт] ′kаɣлымты.</ta>
            <ta e="T133" id="Seg_1008" s="T130">ныны ′чунты kур′lарны.</ta>
            <ta e="T138" id="Seg_1009" s="T133">петро ′kӓйлымымпа [′kӓйлымпа]. ′kаɣлынты ′kӧɣын.</ta>
            <ta e="T150" id="Seg_1010" s="T138">Иван ′намыщаң ′ныңымпа [ныңа] ′кунте ′ныңымпа, ′куттар ′kаɣлынты ′тӣɣымпыптӓ ′аща ′ӱ̄ррʼ′е̨̄имб̂а.</ta>
            <ta e="T154" id="Seg_1011" s="T150">′kаптиччикка тар ашʼа kап′тӓмпа.</ta>
            <ta e="T158" id="Seg_1012" s="T154">тӓп[м] ′очик чо̄′пӓмпа [чо̄тӓмпат].</ta>
            <ta e="T163" id="Seg_1013" s="T158">иван мо̄т серпа, ′илла ′kаптоɣонтоɣын.</ta>
            <ta e="T167" id="Seg_1014" s="T163">′kаптиччика на′ней ′чопа.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T2" id="Seg_1015" s="T0">petro omt̂ɨtɛmp̂[p]a.</ta>
            <ta e="T11" id="Seg_1016" s="T2">nɨnɨ mätčimpat pojam qəːnqɨntoqa, šidɨmdʼelʼi aj moqona poːralp̂a moːtqɨntɨ.</ta>
            <ta e="T23" id="Seg_1017" s="T11">täm pitčond̂oqɨn püːqo[ə]ld̂ɨmpat nʼäŋɨča uːtɨsʼa nɨttɨpij [sʼäpalɨpij] moːtam nɨnɨ kɨpɨkä tukaptɨmp̂at.</ta>
            <ta e="T28" id="Seg_1018" s="T23">“notɨ tipinʼä, man qəːnd̂ɨlorɨnʼnʼaŋ.”</ta>
            <ta e="T32" id="Seg_1019" s="T28">minutkunde üŋkɨltɨmɨmpat kɨpäka.</ta>
            <ta e="T41" id="Seg_1020" s="T32">nɨnaj šʼentɨŋ[k] püːqɨltɨkijoimpelɨmpat. na nʼütə moːtam qatalpɨtij pötpɨqɨntoq[q]a.</ta>
            <ta e="T49" id="Seg_1021" s="T41">nekalʼlʼa qɨːqɨlʼlʼa aj süːmɨqolɨŋ qätɨmp̂at man na qəːnd̂aŋ.</ta>
            <ta e="T52" id="Seg_1022" s="T49">nɨnɨ kuralʼp̂a pona.</ta>
            <ta e="T58" id="Seg_1023" s="T52">täp čäŋgɨ üntɨ[j]impat kuttar moːta tikqɨmoːtpa.</ta>
            <ta e="T71" id="Seg_1024" s="T58">nɨnɨ čäŋĝ[q]ɨ qontɨrreːtɨ kuttar täpɨm nʼomp[p̂]at nʼäŋɨča topɨsʼa sə[ɨ]romɨn nʼomp[p̂]at mittə nüːd̂oqɨn Иvan.</ta>
            <ta e="T74" id="Seg_1025" s="T71">na kuttar eẑimantɨ?</ta>
            <ta e="T77" id="Seg_1026" s="T74">qaj qɨndɨlʼorɨnnʼantɨ. </ta>
            <ta e="T79" id="Seg_1027" s="T77">ukon taŋalʼtɨŋ!</ta>
            <ta e="T85" id="Seg_1028" s="T79">toːlʼmotqɨn [tojmotqɨn] qondomɨn nʼäim.</ta>
            <ta e="T88" id="Seg_1029" s="T85">mättoːqɨn amɨrqejsenantɨ.</ta>
            <ta e="T93" id="Seg_1030" s="T88">(tipɨnʼäqɨntɨ) tipɨnʼa man aša kɨkam.</ta>
            <ta e="T96" id="Seg_1031" s="T93">oːnändɨ qumɨiqantɨ qɨː[əː]čat.</ta>
            <ta e="T103" id="Seg_1032" s="T96">me qumɨtɨt puːtoqɨn aša qundomɨn qätɨmpat Пetro.</ta>
            <ta e="T108" id="Seg_1033" s="T103">nɨnɨ iːmpat utoqɨntɨ muqɨlsimi äːqaim.</ta>
            <ta e="T120" id="Seg_1034" s="T108">Иvan qaj lɨpɨkʼeptantoqa čeːq[k]aptɨlʼa nid̂ɨmp̂at tiːpi[ɨ]nʼämtɨ koččiparə puːtɨlmɨntɨ, qonmɨntɨ, inčajmɨntɨ.</ta>
            <ta e="T126" id="Seg_1035" s="T120">tiːd̂am qaj ne qond̂ɨrtend̂amɨn. qätɨmpat täm.</ta>
            <ta e="T130" id="Seg_1036" s="T126">Иvan noqqolp̂at [noqqolnɨt] qaqlɨmtɨ.</ta>
            <ta e="T133" id="Seg_1037" s="T130">nɨnɨ čuntɨ qurlʼarnɨ.</ta>
            <ta e="T138" id="Seg_1038" s="T133">petro qäjlɨmɨmpa [qäjlɨmpa]. qaqlɨntɨ qöqɨn.</ta>
            <ta e="T150" id="Seg_1039" s="T138">Иvan namɨšaŋ nɨŋɨmpa [nɨŋa] kunte nɨŋɨmpa, kuttar qaqlɨntɨ tiːqɨmpɨptä aša üːrrʼeːimp̂a.</ta>
            <ta e="T154" id="Seg_1040" s="T150">qaptiččikka tar aša qaptämpa.</ta>
            <ta e="T158" id="Seg_1041" s="T154">täp[m] očik čoːpämpa [čoːtämpat].</ta>
            <ta e="T163" id="Seg_1042" s="T158">ivan moːt serpa, illa qaptoqontoqɨn.</ta>
            <ta e="T167" id="Seg_1043" s="T163">qaptiččika nanej čopa.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_1044" s="T0">Petro omtɨttɛːmpa. </ta>
            <ta e="T11" id="Seg_1045" s="T2">Nɨːnɨ mäčʼčʼimpat poːjam qənqɨntoːqa, šitɨmtʼälʼi aj moqona pɔːralpa mɔːtqɨntɨ. </ta>
            <ta e="T23" id="Seg_1046" s="T11">Täm pit čʼontoːqɨn püqəltɨmpat nʼäŋɨčʼa utɨsʼa nɨttɨpij (/sʼäpalɨpij) mɔːtam, nɨːnɨ kɨpɨkä tukaptɨmpat. </ta>
            <ta e="T28" id="Seg_1047" s="T23">Nɔːtɨ tıpinʼä, man qəntɨl orɨnʼnʼaŋ. </ta>
            <ta e="T32" id="Seg_1048" s="T28">Minut kunte üŋkɨltɨmɨmpat kɨpäka. </ta>
            <ta e="T41" id="Seg_1049" s="T32">Nɨːn aj šentɨŋ püqɨltɨkkijoimpelɨmpat na nʼuːtə mɔːtam qatalpɨtij pötpɨqɨntoːqa. </ta>
            <ta e="T49" id="Seg_1050" s="T41">Näkalʼa qɨːqɨlʼlʼa aj sümɨkɔːlɨŋ kätɨmpat: Man na qəntaŋ. </ta>
            <ta e="T52" id="Seg_1051" s="T49">Nɨːnɨ kuralpa pona. </ta>
            <ta e="T58" id="Seg_1052" s="T52">Täp čʼäːŋkɨ üntɨjıːmpat, kuttar mɔːta tıːqɨmɔːtpa. </ta>
            <ta e="T71" id="Seg_1053" s="T58">Nɨːnɨ čʼäːŋkɨ qontɨrrɛːtɨ, kuttar täpɨm nʼoːmpat nʼäŋɨčʼa topɨsʼa sɨroːmɨn, nʼoːmpat mittə nʼuːtoːqɨn Ivan. </ta>
            <ta e="T74" id="Seg_1054" s="T71">Na kuttar ɛsimmantɨ? </ta>
            <ta e="T77" id="Seg_1055" s="T74">Qaj qɨntɨl orɨnʼnʼantɨ? </ta>
            <ta e="T79" id="Seg_1056" s="T77">Ukoːn taŋaltɨŋ! </ta>
            <ta e="T85" id="Seg_1057" s="T79">Tolʼ mɔːtqɨn (/toj mɔːtqɨn) qontɔːmɨn nʼäim. </ta>
            <ta e="T88" id="Seg_1058" s="T85">Mättoːqɨn amɨrq ɛjsenantɨ. </ta>
            <ta e="T93" id="Seg_1059" s="T88">(Tıpɨnʼäqɨntɨ): Tıpɨnʼa, man aša kɨkam. </ta>
            <ta e="T96" id="Seg_1060" s="T93">Onäntɨ qumɨiːqantɨ qəːčʼat. </ta>
            <ta e="T103" id="Seg_1061" s="T96">Meː qumɨtɨt puːtoːqɨn aša quntɔːmɨn,— kätɨmpat Petro. </ta>
            <ta e="T108" id="Seg_1062" s="T103">Nɨːnɨ iːmpat utoːqɨntɨ muqɨlsimi äːqaim. </ta>
            <ta e="T120" id="Seg_1063" s="T108">Ivan qaj lɨpɨk ɛːptaːntoːqa čʼekaptɨlʼa nıːtɨmpat tıpɨnʼämtɨ kočʼčʼi pɔːrə puːtɨlmɨntɨ, qɔːnmɨntɨ, inčʼajmɨntɨ. </ta>
            <ta e="T126" id="Seg_1064" s="T120">Tıːtam qaj ne qontɨrtɛntɔːmɨn,— kätɨmpat täm. </ta>
            <ta e="T130" id="Seg_1065" s="T126">Ivan noqqɔːlpat (/noqqɔːlnɨt) qaqlɨmtɨ. </ta>
            <ta e="T133" id="Seg_1066" s="T130">Nɨːnɨ čʼuntɨ kurlarnɨ. </ta>
            <ta e="T138" id="Seg_1067" s="T133">Petro qäjlɨmɨmpa (/qäjlɨmpa) qaqlɨntɨ qöqɨn. </ta>
            <ta e="T150" id="Seg_1068" s="T138">Ivan namɨšaŋ nɨŋɨmpa [nɨŋa], kunte nɨŋɨmpa, kuttar qaqlɨntɨ tıːqɨmpɨptäː aša ürrʼɛıːmpa. </ta>
            <ta e="T154" id="Seg_1069" s="T150">Qaptičʼčʼikka tar aša qaptämpa. </ta>
            <ta e="T158" id="Seg_1070" s="T154">Täp očʼik čʼɔːppɛːmpa (/čʼɔːttɛːmpat). </ta>
            <ta e="T163" id="Seg_1071" s="T158">Ivan mɔːt seːrpa, ılla qaptoqontoːqɨn. </ta>
            <ta e="T167" id="Seg_1072" s="T163">Qaptičʼčʼika, nan ɛj čʼɔːpa. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1073" s="T0">petro</ta>
            <ta e="T2" id="Seg_1074" s="T1">omtɨ-tt-ɛː-mpa</ta>
            <ta e="T3" id="Seg_1075" s="T2">nɨːnɨ</ta>
            <ta e="T4" id="Seg_1076" s="T3">mäčʼčʼi-mpa-t</ta>
            <ta e="T5" id="Seg_1077" s="T4">poː-ja-m</ta>
            <ta e="T6" id="Seg_1078" s="T5">qən-qɨntoːqa</ta>
            <ta e="T7" id="Seg_1079" s="T6">šitɨ-mtʼälʼi</ta>
            <ta e="T8" id="Seg_1080" s="T7">aj</ta>
            <ta e="T9" id="Seg_1081" s="T8">moqona</ta>
            <ta e="T10" id="Seg_1082" s="T9">pɔːr-al-pa</ta>
            <ta e="T11" id="Seg_1083" s="T10">mɔːt-qɨn-tɨ</ta>
            <ta e="T12" id="Seg_1084" s="T11">täm</ta>
            <ta e="T13" id="Seg_1085" s="T12">pi-t</ta>
            <ta e="T14" id="Seg_1086" s="T13">čʼontoː-qɨn</ta>
            <ta e="T15" id="Seg_1087" s="T14">pü-qəl-tɨ-mpa-t</ta>
            <ta e="T16" id="Seg_1088" s="T15">nʼäŋɨčʼa</ta>
            <ta e="T17" id="Seg_1089" s="T16">utɨ-sʼa</ta>
            <ta e="T18" id="Seg_1090" s="T17">nɨt-tɨ-pij</ta>
            <ta e="T19" id="Seg_1091" s="T18">sʼäpa-lɨ-pij</ta>
            <ta e="T20" id="Seg_1092" s="T19">mɔːta-m</ta>
            <ta e="T21" id="Seg_1093" s="T20">nɨːnɨ</ta>
            <ta e="T22" id="Seg_1094" s="T21">kɨpɨkä</ta>
            <ta e="T23" id="Seg_1095" s="T22">tuk-aptɨ-mpa-t</ta>
            <ta e="T24" id="Seg_1096" s="T23">Nɔːtɨ</ta>
            <ta e="T25" id="Seg_1097" s="T24">tıpinʼä</ta>
            <ta e="T26" id="Seg_1098" s="T25">man</ta>
            <ta e="T27" id="Seg_1099" s="T26">qən-tɨ-l</ta>
            <ta e="T28" id="Seg_1100" s="T27">orɨ-nʼ-nʼa-ŋ</ta>
            <ta e="T29" id="Seg_1101" s="T28">minut</ta>
            <ta e="T30" id="Seg_1102" s="T29">kunte</ta>
            <ta e="T31" id="Seg_1103" s="T30">üŋkɨl-tɨ-mɨ-mpa-t</ta>
            <ta e="T32" id="Seg_1104" s="T31">kɨpäka</ta>
            <ta e="T33" id="Seg_1105" s="T32">nɨːn</ta>
            <ta e="T34" id="Seg_1106" s="T33">aj</ta>
            <ta e="T35" id="Seg_1107" s="T34">šentɨ-ŋ</ta>
            <ta e="T36" id="Seg_1108" s="T35">pü-qɨl-tɨ-kki-j-oim-pe-lɨ-mpa-t</ta>
            <ta e="T37" id="Seg_1109" s="T36">na</ta>
            <ta e="T38" id="Seg_1110" s="T37">nʼuːtə</ta>
            <ta e="T39" id="Seg_1111" s="T38">mɔːta-m</ta>
            <ta e="T40" id="Seg_1112" s="T39">qat-al-pɨ-tij</ta>
            <ta e="T41" id="Seg_1113" s="T40">pöt-pɨ-qɨntoːqa</ta>
            <ta e="T42" id="Seg_1114" s="T41">näka-lʼa</ta>
            <ta e="T43" id="Seg_1115" s="T42">qɨː-qɨlʼ-lʼa</ta>
            <ta e="T44" id="Seg_1116" s="T43">aj</ta>
            <ta e="T45" id="Seg_1117" s="T44">sümɨ-kɔːlɨ-ŋ</ta>
            <ta e="T46" id="Seg_1118" s="T45">kätɨ-mpa-t</ta>
            <ta e="T47" id="Seg_1119" s="T46">Man</ta>
            <ta e="T48" id="Seg_1120" s="T47">na</ta>
            <ta e="T49" id="Seg_1121" s="T48">qən-ta-ŋ</ta>
            <ta e="T50" id="Seg_1122" s="T49">nɨːnɨ</ta>
            <ta e="T51" id="Seg_1123" s="T50">kur-al-pa</ta>
            <ta e="T52" id="Seg_1124" s="T51">pona</ta>
            <ta e="T53" id="Seg_1125" s="T52">täp</ta>
            <ta e="T54" id="Seg_1126" s="T53">čʼäːŋkɨ</ta>
            <ta e="T55" id="Seg_1127" s="T54">üntɨ-jıː-mpa-t</ta>
            <ta e="T56" id="Seg_1128" s="T55">kuttar</ta>
            <ta e="T57" id="Seg_1129" s="T56">mɔːta</ta>
            <ta e="T58" id="Seg_1130" s="T57">tıːqɨ-mɔːt-pa</ta>
            <ta e="T59" id="Seg_1131" s="T58">nɨːnɨ</ta>
            <ta e="T60" id="Seg_1132" s="T59">čʼäːŋkɨ</ta>
            <ta e="T61" id="Seg_1133" s="T60">qo-ntɨ-rr-ɛː-tɨ</ta>
            <ta e="T62" id="Seg_1134" s="T61">kuttar</ta>
            <ta e="T63" id="Seg_1135" s="T62">täp-ɨ-m</ta>
            <ta e="T64" id="Seg_1136" s="T63">nʼoː-mpa-t</ta>
            <ta e="T65" id="Seg_1137" s="T64">nʼäŋɨčʼa</ta>
            <ta e="T66" id="Seg_1138" s="T65">topɨ-sʼa</ta>
            <ta e="T67" id="Seg_1139" s="T66">sɨroː-mɨn</ta>
            <ta e="T68" id="Seg_1140" s="T67">nʼoː-mpa-t</ta>
            <ta e="T69" id="Seg_1141" s="T68">mittə</ta>
            <ta e="T70" id="Seg_1142" s="T69">nʼuːtoː-qɨn</ta>
            <ta e="T71" id="Seg_1143" s="T70">ivan</ta>
            <ta e="T72" id="Seg_1144" s="T71">na</ta>
            <ta e="T73" id="Seg_1145" s="T72">kuttar</ta>
            <ta e="T74" id="Seg_1146" s="T73">ɛsi-mma-ntɨ</ta>
            <ta e="T75" id="Seg_1147" s="T74">qaj</ta>
            <ta e="T76" id="Seg_1148" s="T75">qɨn-tɨ-l</ta>
            <ta e="T77" id="Seg_1149" s="T76">orɨ-nʼ-nʼa-ntɨ</ta>
            <ta e="T78" id="Seg_1150" s="T77">ukoːn</ta>
            <ta e="T79" id="Seg_1151" s="T78">taŋ-alt-ɨŋ</ta>
            <ta e="T80" id="Seg_1152" s="T79">to-lʼ</ta>
            <ta e="T81" id="Seg_1153" s="T80">mɔːt-qɨn</ta>
            <ta e="T82" id="Seg_1154" s="T81">to-j</ta>
            <ta e="T83" id="Seg_1155" s="T82">mɔːt-qɨn</ta>
            <ta e="T84" id="Seg_1156" s="T83">qo-ntɔː-mɨn</ta>
            <ta e="T85" id="Seg_1157" s="T84">nʼäi-m</ta>
            <ta e="T86" id="Seg_1158" s="T85">mättoː-qɨn</ta>
            <ta e="T87" id="Seg_1159" s="T86">am-ɨ-r-q</ta>
            <ta e="T88" id="Seg_1160" s="T87">ɛjse-na-ntɨ</ta>
            <ta e="T89" id="Seg_1161" s="T88">Tıpɨnʼä-qɨn-tɨ</ta>
            <ta e="T90" id="Seg_1162" s="T89">Tıpɨnʼa</ta>
            <ta e="T91" id="Seg_1163" s="T90">man</ta>
            <ta e="T92" id="Seg_1164" s="T91">aša</ta>
            <ta e="T93" id="Seg_1165" s="T92">kɨka-m</ta>
            <ta e="T94" id="Seg_1166" s="T93">onäntɨ</ta>
            <ta e="T95" id="Seg_1167" s="T94">qum-ɨ-iː-qantɨ</ta>
            <ta e="T96" id="Seg_1168" s="T95">qəːčʼ-at</ta>
            <ta e="T97" id="Seg_1169" s="T96">meː</ta>
            <ta e="T98" id="Seg_1170" s="T97">qum-ɨ-t-ɨ-t</ta>
            <ta e="T99" id="Seg_1171" s="T98">puːtoː-qɨn</ta>
            <ta e="T100" id="Seg_1172" s="T99">aša</ta>
            <ta e="T101" id="Seg_1173" s="T100">qu-ntɔː-mɨn</ta>
            <ta e="T102" id="Seg_1174" s="T101">kätɨ-mpa-t</ta>
            <ta e="T103" id="Seg_1175" s="T102">petro</ta>
            <ta e="T104" id="Seg_1176" s="T103">nɨːnɨ</ta>
            <ta e="T105" id="Seg_1177" s="T104">iː-mpa-t</ta>
            <ta e="T106" id="Seg_1178" s="T105">utoː-qɨn-tɨ</ta>
            <ta e="T107" id="Seg_1179" s="T106">muqɨl-simi-lʼ</ta>
            <ta e="T108" id="Seg_1180" s="T107">äːqai-m</ta>
            <ta e="T109" id="Seg_1181" s="T108">ivan</ta>
            <ta e="T110" id="Seg_1182" s="T109">qaj</ta>
            <ta e="T111" id="Seg_1183" s="T110">lɨpɨ-k</ta>
            <ta e="T112" id="Seg_1184" s="T111">ɛː-ptaː-ntoːqa</ta>
            <ta e="T113" id="Seg_1185" s="T112">čʼek-aptɨ-lʼa</ta>
            <ta e="T114" id="Seg_1186" s="T113">nıːtɨ-mpa-t</ta>
            <ta e="T115" id="Seg_1187" s="T114">tıpɨnʼä-m-tɨ</ta>
            <ta e="T116" id="Seg_1188" s="T115">kočʼčʼi</ta>
            <ta e="T117" id="Seg_1189" s="T116">pɔːrə</ta>
            <ta e="T118" id="Seg_1190" s="T117">puːtɨl-mɨn-tɨ</ta>
            <ta e="T119" id="Seg_1191" s="T118">qɔːn-mɨn-tɨ</ta>
            <ta e="T120" id="Seg_1192" s="T119">inčʼaj-mɨn-tɨ</ta>
            <ta e="T121" id="Seg_1193" s="T120">tıːtam</ta>
            <ta e="T122" id="Seg_1194" s="T121">qaj</ta>
            <ta e="T123" id="Seg_1195" s="T122">ne</ta>
            <ta e="T124" id="Seg_1196" s="T123">qo-ntɨr-tɛntɔː-mɨn</ta>
            <ta e="T125" id="Seg_1197" s="T124">kätɨ-mpa-t</ta>
            <ta e="T126" id="Seg_1198" s="T125">täm</ta>
            <ta e="T127" id="Seg_1199" s="T126">ivan</ta>
            <ta e="T128" id="Seg_1200" s="T127">noqq-ɔːl-pa-t</ta>
            <ta e="T129" id="Seg_1201" s="T128">noqq-ɔːl-nɨ-t</ta>
            <ta e="T130" id="Seg_1202" s="T129">qaqlɨ-m-tɨ</ta>
            <ta e="T131" id="Seg_1203" s="T130">nɨːnɨ</ta>
            <ta e="T132" id="Seg_1204" s="T131">čʼuntɨ</ta>
            <ta e="T133" id="Seg_1205" s="T132">kur-la-r-nɨ</ta>
            <ta e="T134" id="Seg_1206" s="T133">petro</ta>
            <ta e="T135" id="Seg_1207" s="T134">qäjlɨ-mɨ-mpa</ta>
            <ta e="T136" id="Seg_1208" s="T135">qäjlɨ-mpa</ta>
            <ta e="T137" id="Seg_1209" s="T136">qaqlɨ-n-tɨ</ta>
            <ta e="T138" id="Seg_1210" s="T137">qö-qɨn</ta>
            <ta e="T139" id="Seg_1211" s="T138">ivan</ta>
            <ta e="T140" id="Seg_1212" s="T139">namɨšaŋ</ta>
            <ta e="T141" id="Seg_1213" s="T140">nɨŋɨ-mpa</ta>
            <ta e="T142" id="Seg_1214" s="T141">nɨŋ-a</ta>
            <ta e="T143" id="Seg_1215" s="T142">kunte</ta>
            <ta e="T144" id="Seg_1216" s="T143">nɨŋɨ-mpa</ta>
            <ta e="T145" id="Seg_1217" s="T144">kuttar</ta>
            <ta e="T146" id="Seg_1218" s="T145">qaqlɨ-n-tɨ</ta>
            <ta e="T147" id="Seg_1219" s="T146">tıːqɨ-mpɨ-ptäː</ta>
            <ta e="T148" id="Seg_1220" s="T147">aša</ta>
            <ta e="T150" id="Seg_1221" s="T148">ürrʼ-ɛıː-mpa</ta>
            <ta e="T151" id="Seg_1222" s="T150">qapti-čʼ-čʼi-kka</ta>
            <ta e="T152" id="Seg_1223" s="T151">tar</ta>
            <ta e="T153" id="Seg_1224" s="T152">aša</ta>
            <ta e="T154" id="Seg_1225" s="T153">qap-tä-mpa</ta>
            <ta e="T155" id="Seg_1226" s="T154">täp</ta>
            <ta e="T156" id="Seg_1227" s="T155">očʼik</ta>
            <ta e="T157" id="Seg_1228" s="T156">čʼɔːpp-ɛː-mpa</ta>
            <ta e="T158" id="Seg_1229" s="T157">čʼɔːtt-ɛː-mpa-t</ta>
            <ta e="T159" id="Seg_1230" s="T158">ivan</ta>
            <ta e="T160" id="Seg_1231" s="T159">mɔːt</ta>
            <ta e="T161" id="Seg_1232" s="T160">seːr-pa</ta>
            <ta e="T162" id="Seg_1233" s="T161">ılla</ta>
            <ta e="T163" id="Seg_1234" s="T162">qap-to-qontoːqɨ-n</ta>
            <ta e="T164" id="Seg_1235" s="T163">qapti-čʼ-čʼi-ka</ta>
            <ta e="T165" id="Seg_1236" s="T164">nan</ta>
            <ta e="T166" id="Seg_1237" s="T165">ɛj</ta>
            <ta e="T167" id="Seg_1238" s="T166">čʼɔːpa</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1239" s="T0">petro</ta>
            <ta e="T2" id="Seg_1240" s="T1">omtɨ-ttɨ-ɛː-mpɨ</ta>
            <ta e="T3" id="Seg_1241" s="T2">nɨːnɨ</ta>
            <ta e="T4" id="Seg_1242" s="T3">wəčʼčʼɨ-mpɨ-tɨ</ta>
            <ta e="T5" id="Seg_1243" s="T4">poː-ja-m</ta>
            <ta e="T6" id="Seg_1244" s="T5">qən-qɨntoːqo</ta>
            <ta e="T7" id="Seg_1245" s="T6">šittɨ-mtäl</ta>
            <ta e="T8" id="Seg_1246" s="T7">aj</ta>
            <ta e="T9" id="Seg_1247" s="T8">moqɨnä</ta>
            <ta e="T10" id="Seg_1248" s="T9">pɔːrɨ-ätɔːl-mpɨ</ta>
            <ta e="T11" id="Seg_1249" s="T10">mɔːt-qɨn-ntɨ</ta>
            <ta e="T12" id="Seg_1250" s="T11">təp</ta>
            <ta e="T13" id="Seg_1251" s="T12">pi-n</ta>
            <ta e="T14" id="Seg_1252" s="T13">čʼontɨ-qɨn</ta>
            <ta e="T15" id="Seg_1253" s="T14">pü-qɨl-tɨ-mpɨ-tɨ</ta>
            <ta e="T16" id="Seg_1254" s="T15">nʼaŋɨčʼa</ta>
            <ta e="T17" id="Seg_1255" s="T16">utɨ-sä</ta>
            <ta e="T18" id="Seg_1256" s="T17">nɨta-tɨ-mpɨlʼ</ta>
            <ta e="T19" id="Seg_1257" s="T18">səpɨ-lɨ-mpɨlʼ</ta>
            <ta e="T20" id="Seg_1258" s="T19">mɔːta-m</ta>
            <ta e="T21" id="Seg_1259" s="T20">nɨːnɨ</ta>
            <ta e="T22" id="Seg_1260" s="T21">kɨpaka</ta>
            <ta e="T23" id="Seg_1261" s="T22">tukɨ-äptɨ-mpɨ-tɨ</ta>
            <ta e="T24" id="Seg_1262" s="T23">nɔːtɨ</ta>
            <ta e="T25" id="Seg_1263" s="T24">tıpɨnʼa</ta>
            <ta e="T26" id="Seg_1264" s="T25">man</ta>
            <ta e="T27" id="Seg_1265" s="T26">qən-ntɨ-lä</ta>
            <ta e="T28" id="Seg_1266" s="T27">orɨ-š-ŋɨ-k</ta>
            <ta e="T29" id="Seg_1267" s="T28">minut</ta>
            <ta e="T30" id="Seg_1268" s="T29">kuntɨ</ta>
            <ta e="T31" id="Seg_1269" s="T30">üŋkɨl-ntɨ-mpɨ-mpɨ-tɨ</ta>
            <ta e="T32" id="Seg_1270" s="T31">kɨpaka</ta>
            <ta e="T33" id="Seg_1271" s="T32">nɨːnɨ</ta>
            <ta e="T34" id="Seg_1272" s="T33">aj</ta>
            <ta e="T35" id="Seg_1273" s="T34">šentɨ-k</ta>
            <ta e="T36" id="Seg_1274" s="T35">pü-qɨl-ntɨ-kkɨ-qo-olam-mpɨ-lɨ-mpɨ-tɨ</ta>
            <ta e="T37" id="Seg_1275" s="T36">na</ta>
            <ta e="T38" id="Seg_1276" s="T37">nʼuːtɨ</ta>
            <ta e="T39" id="Seg_1277" s="T38">mɔːta-m</ta>
            <ta e="T40" id="Seg_1278" s="T39">qättɨ-ätɔːl-mpɨ-ntɨlʼ</ta>
            <ta e="T41" id="Seg_1279" s="T40">pöt-mpɨ-qɨntoːqo</ta>
            <ta e="T42" id="Seg_1280" s="T41">näkä-lä</ta>
            <ta e="T43" id="Seg_1281" s="T42">qɨː-qɨl-lä</ta>
            <ta e="T44" id="Seg_1282" s="T43">aj</ta>
            <ta e="T45" id="Seg_1283" s="T44">sümɨ-kɔːlɨ-k</ta>
            <ta e="T46" id="Seg_1284" s="T45">kətɨ-mpɨ-tɨ</ta>
            <ta e="T47" id="Seg_1285" s="T46">man</ta>
            <ta e="T48" id="Seg_1286" s="T47">na</ta>
            <ta e="T49" id="Seg_1287" s="T48">qən-ɛntɨ-k</ta>
            <ta e="T50" id="Seg_1288" s="T49">nɨːnɨ</ta>
            <ta e="T51" id="Seg_1289" s="T50">*kurɨ-ätɔːl-mpɨ</ta>
            <ta e="T52" id="Seg_1290" s="T51">ponä</ta>
            <ta e="T53" id="Seg_1291" s="T52">təp</ta>
            <ta e="T54" id="Seg_1292" s="T53">čʼäːŋkɨ</ta>
            <ta e="T55" id="Seg_1293" s="T54">üntɨ-ıː-mpɨ-tɨ</ta>
            <ta e="T56" id="Seg_1294" s="T55">kuttar</ta>
            <ta e="T57" id="Seg_1295" s="T56">mɔːta</ta>
            <ta e="T58" id="Seg_1296" s="T57">tıːqɨ-mɔːt-mpɨ</ta>
            <ta e="T59" id="Seg_1297" s="T58">nɨːnɨ</ta>
            <ta e="T60" id="Seg_1298" s="T59">čʼäːŋkɨ</ta>
            <ta e="T61" id="Seg_1299" s="T60">qo-ntɨ-r-ɛː-tɨ</ta>
            <ta e="T62" id="Seg_1300" s="T61">kuttar</ta>
            <ta e="T63" id="Seg_1301" s="T62">təp-ɨ-m</ta>
            <ta e="T64" id="Seg_1302" s="T63">nʼoː-mpɨ-tɨ</ta>
            <ta e="T65" id="Seg_1303" s="T64">nʼaŋɨčʼa</ta>
            <ta e="T66" id="Seg_1304" s="T65">topɨ-sä</ta>
            <ta e="T67" id="Seg_1305" s="T66">sɨrɨ-mɨn</ta>
            <ta e="T68" id="Seg_1306" s="T67">nʼoː-mpɨ-tɨ</ta>
            <ta e="T69" id="Seg_1307" s="T68">mitɨ</ta>
            <ta e="T70" id="Seg_1308" s="T69">nʼuːtɨ-qɨn</ta>
            <ta e="T71" id="Seg_1309" s="T70">Ivan</ta>
            <ta e="T72" id="Seg_1310" s="T71">na</ta>
            <ta e="T73" id="Seg_1311" s="T72">kuttar</ta>
            <ta e="T74" id="Seg_1312" s="T73">ɛsɨ-mpɨ-ntɨ</ta>
            <ta e="T75" id="Seg_1313" s="T74">qaj</ta>
            <ta e="T76" id="Seg_1314" s="T75">qən-ntɨ-lä</ta>
            <ta e="T77" id="Seg_1315" s="T76">orɨ-š-ŋɨ-ntɨ</ta>
            <ta e="T78" id="Seg_1316" s="T77">ukoːn</ta>
            <ta e="T79" id="Seg_1317" s="T78">*taŋɨ-altɨ-äšɨk</ta>
            <ta e="T80" id="Seg_1318" s="T79">to-lʼ</ta>
            <ta e="T81" id="Seg_1319" s="T80">mɔːt-qɨn</ta>
            <ta e="T82" id="Seg_1320" s="T81">to-lʼ</ta>
            <ta e="T83" id="Seg_1321" s="T82">mɔːt-qɨn</ta>
            <ta e="T84" id="Seg_1322" s="T83">qo-ɛntɨ-mɨt</ta>
            <ta e="T85" id="Seg_1323" s="T84">nʼanʼ-m</ta>
            <ta e="T86" id="Seg_1324" s="T85">wəttɨ-qɨn</ta>
            <ta e="T87" id="Seg_1325" s="T86">am-ɨ-r-qo</ta>
            <ta e="T88" id="Seg_1326" s="T87">ɛsɨ-ŋɨ-ntɨ</ta>
            <ta e="T89" id="Seg_1327" s="T88">tıpɨnʼa-qɨn-ntɨ</ta>
            <ta e="T90" id="Seg_1328" s="T89">tıpɨnʼa</ta>
            <ta e="T91" id="Seg_1329" s="T90">man</ta>
            <ta e="T92" id="Seg_1330" s="T91">ašša</ta>
            <ta e="T93" id="Seg_1331" s="T92">kɨkɨ-m</ta>
            <ta e="T94" id="Seg_1332" s="T93">onäntɨ</ta>
            <ta e="T95" id="Seg_1333" s="T94">qum-ɨ-iː-qäntɨ</ta>
            <ta e="T96" id="Seg_1334" s="T95">qəːčʼɨ-ätɨ</ta>
            <ta e="T97" id="Seg_1335" s="T96">meː</ta>
            <ta e="T98" id="Seg_1336" s="T97">qum-ɨ-t-ɨ-n</ta>
            <ta e="T99" id="Seg_1337" s="T98">puːtɨ-qɨn</ta>
            <ta e="T100" id="Seg_1338" s="T99">ašša</ta>
            <ta e="T101" id="Seg_1339" s="T100">qu-ɛntɨ-mɨt</ta>
            <ta e="T102" id="Seg_1340" s="T101">kətɨ-mpɨ-tɨ</ta>
            <ta e="T103" id="Seg_1341" s="T102">petro</ta>
            <ta e="T104" id="Seg_1342" s="T103">nɨːnɨ</ta>
            <ta e="T105" id="Seg_1343" s="T104">iː-mpɨ-tɨ</ta>
            <ta e="T106" id="Seg_1344" s="T105">utɨ-qɨn-ntɨ</ta>
            <ta e="T107" id="Seg_1345" s="T106">muqqɨl-sɨma-lʼ</ta>
            <ta e="T108" id="Seg_1346" s="T107">ɔːqqalʼ-m</ta>
            <ta e="T109" id="Seg_1347" s="T108">Ivan</ta>
            <ta e="T110" id="Seg_1348" s="T109">qaj</ta>
            <ta e="T111" id="Seg_1349" s="T110">*lɨpɨ-k</ta>
            <ta e="T112" id="Seg_1350" s="T111">ɛː-ptäː-ntoːqo</ta>
            <ta e="T113" id="Seg_1351" s="T112">*čʼək-äptɨ-lä</ta>
            <ta e="T114" id="Seg_1352" s="T113">nıːtɨ-mpɨ-tɨ</ta>
            <ta e="T115" id="Seg_1353" s="T114">tıpɨnʼa-m-tɨ</ta>
            <ta e="T116" id="Seg_1354" s="T115">kočʼčʼɨ</ta>
            <ta e="T117" id="Seg_1355" s="T116">pɔːrɨ</ta>
            <ta e="T118" id="Seg_1356" s="T117">puːtɨl-mɨn-ntɨ</ta>
            <ta e="T119" id="Seg_1357" s="T118">qɔːt-mɨn-ntɨ</ta>
            <ta e="T120" id="Seg_1358" s="T119">ɨntälʼ-mɨn-tɨ</ta>
            <ta e="T121" id="Seg_1359" s="T120">tıːtap</ta>
            <ta e="T122" id="Seg_1360" s="T121">qaj</ta>
            <ta e="T123" id="Seg_1361" s="T122">nʼi</ta>
            <ta e="T124" id="Seg_1362" s="T123">qo-ntɨr-ɛntɨ-mɨt</ta>
            <ta e="T125" id="Seg_1363" s="T124">kətɨ-mpɨ-tɨ</ta>
            <ta e="T126" id="Seg_1364" s="T125">təp</ta>
            <ta e="T127" id="Seg_1365" s="T126">Ivan</ta>
            <ta e="T128" id="Seg_1366" s="T127">noqqo-ɔːl-mpɨ-tɨ</ta>
            <ta e="T129" id="Seg_1367" s="T128">noqqo-ɔːl-ŋɨ-tɨ</ta>
            <ta e="T130" id="Seg_1368" s="T129">qaqlɨ-m-tɨ</ta>
            <ta e="T131" id="Seg_1369" s="T130">nɨːnɨ</ta>
            <ta e="T132" id="Seg_1370" s="T131">čʼuntɨ</ta>
            <ta e="T133" id="Seg_1371" s="T132">*kurɨ-lɨ-r-ŋɨ</ta>
            <ta e="T134" id="Seg_1372" s="T133">petro</ta>
            <ta e="T135" id="Seg_1373" s="T134">qälɨ-mpɨ-mpɨ</ta>
            <ta e="T136" id="Seg_1374" s="T135">qälɨ-mpɨ</ta>
            <ta e="T137" id="Seg_1375" s="T136">qaqlɨ-n-tɨ</ta>
            <ta e="T138" id="Seg_1376" s="T137">qö-qɨn</ta>
            <ta e="T139" id="Seg_1377" s="T138">Ivan</ta>
            <ta e="T140" id="Seg_1378" s="T139">namɨššak</ta>
            <ta e="T141" id="Seg_1379" s="T140">nɨŋ-mpɨ</ta>
            <ta e="T142" id="Seg_1380" s="T141">nɨŋ-ŋɨ</ta>
            <ta e="T143" id="Seg_1381" s="T142">kuntɨ</ta>
            <ta e="T144" id="Seg_1382" s="T143">nɨŋ-mpɨ</ta>
            <ta e="T145" id="Seg_1383" s="T144">kuttar</ta>
            <ta e="T146" id="Seg_1384" s="T145">qaqlɨ-n-tɨ</ta>
            <ta e="T147" id="Seg_1385" s="T146">tıːqɨ-mpɨ-ptäː</ta>
            <ta e="T148" id="Seg_1386" s="T147">ašša</ta>
            <ta e="T150" id="Seg_1387" s="T148">ürɨ-ɛː-mpɨ</ta>
            <ta e="T151" id="Seg_1388" s="T150">qaptɨ-š-ntɨ-kkɨ</ta>
            <ta e="T152" id="Seg_1389" s="T151">tarɨ</ta>
            <ta e="T153" id="Seg_1390" s="T152">ašša</ta>
            <ta e="T154" id="Seg_1391" s="T153">qapɨ-tä-mpɨ</ta>
            <ta e="T155" id="Seg_1392" s="T154">təp</ta>
            <ta e="T156" id="Seg_1393" s="T155">očʼɨŋ</ta>
            <ta e="T157" id="Seg_1394" s="T156">čʼɔːpɨ-ɛː-mpɨ</ta>
            <ta e="T158" id="Seg_1395" s="T157">čʼɔːtɨ-ɛː-mpɨ-tɨ</ta>
            <ta e="T159" id="Seg_1396" s="T158">Ivan</ta>
            <ta e="T160" id="Seg_1397" s="T159">mɔːt</ta>
            <ta e="T161" id="Seg_1398" s="T160">šeːr-mpɨ</ta>
            <ta e="T162" id="Seg_1399" s="T161">ıllä</ta>
            <ta e="T163" id="Seg_1400" s="T162">qapɨ-tɨ-qɨntoːqo-naj</ta>
            <ta e="T164" id="Seg_1401" s="T163">qapti-š-ntɨ-kkɨ</ta>
            <ta e="T165" id="Seg_1402" s="T164">nanɨ</ta>
            <ta e="T166" id="Seg_1403" s="T165">aj</ta>
            <ta e="T167" id="Seg_1404" s="T166">čʼɔːpɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1405" s="T0">Petro.[NOM]</ta>
            <ta e="T2" id="Seg_1406" s="T1">pray-DETR-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_1407" s="T2">then</ta>
            <ta e="T4" id="Seg_1408" s="T3">lift-PST.NAR-3SG.O</ta>
            <ta e="T5" id="Seg_1409" s="T4">tree-DIM-ACC</ta>
            <ta e="T6" id="Seg_1410" s="T5">leave-SUP.2/3SG</ta>
            <ta e="T7" id="Seg_1411" s="T6">two-ORD</ta>
            <ta e="T8" id="Seg_1412" s="T7">and</ta>
            <ta e="T9" id="Seg_1413" s="T8">back</ta>
            <ta e="T10" id="Seg_1414" s="T9">go.back-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T11" id="Seg_1415" s="T10">house-ILL-OBL.3SG</ta>
            <ta e="T12" id="Seg_1416" s="T11">(s)he.[NOM]</ta>
            <ta e="T13" id="Seg_1417" s="T12">night-GEN</ta>
            <ta e="T14" id="Seg_1418" s="T13">middle-LOC</ta>
            <ta e="T15" id="Seg_1419" s="T14">touch-MULO-TR-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_1420" s="T15">nude</ta>
            <ta e="T17" id="Seg_1421" s="T16">hand-INSTR</ta>
            <ta e="T18" id="Seg_1422" s="T17">tear-TR-PTCP.PST</ta>
            <ta e="T19" id="Seg_1423" s="T18">break-RES-PTCP.PST</ta>
            <ta e="T20" id="Seg_1424" s="T19">door-ACC</ta>
            <ta e="T21" id="Seg_1425" s="T20">then</ta>
            <ta e="T22" id="Seg_1426" s="T21">slightly</ta>
            <ta e="T23" id="Seg_1427" s="T22">knock-ATTEN-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_1428" s="T23">then</ta>
            <ta e="T25" id="Seg_1429" s="T24">brother.[NOM]</ta>
            <ta e="T26" id="Seg_1430" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_1431" s="T26">leave-IPFV-CVB</ta>
            <ta e="T28" id="Seg_1432" s="T27">force-VBLZ-CO-1SG.S</ta>
            <ta e="T29" id="Seg_1433" s="T28">minute.[NOM]</ta>
            <ta e="T30" id="Seg_1434" s="T29">during</ta>
            <ta e="T31" id="Seg_1435" s="T30">hear-IPFV-DUR-PST.NAR-3SG.O</ta>
            <ta e="T32" id="Seg_1436" s="T31">a.little.bit</ta>
            <ta e="T33" id="Seg_1437" s="T32">then</ta>
            <ta e="T34" id="Seg_1438" s="T33">again</ta>
            <ta e="T35" id="Seg_1439" s="T34">new-ADVZ</ta>
            <ta e="T36" id="Seg_1440" s="T35">touch-MULO-IPFV-HAB-INF-be.going.to-DUR-INCH-PST.NAR-3SG.O</ta>
            <ta e="T37" id="Seg_1441" s="T36">this</ta>
            <ta e="T38" id="Seg_1442" s="T37">hay.[NOM]</ta>
            <ta e="T39" id="Seg_1443" s="T38">door-ACC</ta>
            <ta e="T40" id="Seg_1444" s="T39">hit-MOM-DUR-PTCP.PRS</ta>
            <ta e="T41" id="Seg_1445" s="T40">warm-DUR-SUP.2/3SG</ta>
            <ta e="T42" id="Seg_1446" s="T41">pull-CVB</ta>
            <ta e="T43" id="Seg_1447" s="T42">finish-MULO-CVB</ta>
            <ta e="T44" id="Seg_1448" s="T43">again</ta>
            <ta e="T45" id="Seg_1449" s="T44">noise-CAR-ADVZ</ta>
            <ta e="T46" id="Seg_1450" s="T45">say-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_1451" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_1452" s="T47">here</ta>
            <ta e="T49" id="Seg_1453" s="T48">go.away-FUT-1SG.S</ta>
            <ta e="T50" id="Seg_1454" s="T49">then</ta>
            <ta e="T51" id="Seg_1455" s="T50">go-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_1456" s="T51">outwards</ta>
            <ta e="T53" id="Seg_1457" s="T52">(s)he.[NOM]</ta>
            <ta e="T54" id="Seg_1458" s="T53">NEG</ta>
            <ta e="T55" id="Seg_1459" s="T54">be.heard-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_1460" s="T55">how</ta>
            <ta e="T57" id="Seg_1461" s="T56">door.[NOM]</ta>
            <ta e="T58" id="Seg_1462" s="T57">creak-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T59" id="Seg_1463" s="T58">then</ta>
            <ta e="T60" id="Seg_1464" s="T59">NEG</ta>
            <ta e="T61" id="Seg_1465" s="T60">see-IPFV-FRQ-PFV-3SG.O</ta>
            <ta e="T62" id="Seg_1466" s="T61">how</ta>
            <ta e="T63" id="Seg_1467" s="T62">(s)he-EP-ACC</ta>
            <ta e="T64" id="Seg_1468" s="T63">catch.up-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_1469" s="T64">nude</ta>
            <ta e="T66" id="Seg_1470" s="T65">leg-INSTR</ta>
            <ta e="T67" id="Seg_1471" s="T66">snow-PROL</ta>
            <ta e="T68" id="Seg_1472" s="T67">catch.up-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1473" s="T68">like</ta>
            <ta e="T70" id="Seg_1474" s="T69">grass-LOC</ta>
            <ta e="T71" id="Seg_1475" s="T70">Ivan.[NOM]</ta>
            <ta e="T72" id="Seg_1476" s="T71">here</ta>
            <ta e="T73" id="Seg_1477" s="T72">how</ta>
            <ta e="T74" id="Seg_1478" s="T73">say-PST.NAR-2SG.S</ta>
            <ta e="T75" id="Seg_1479" s="T74">whether</ta>
            <ta e="T76" id="Seg_1480" s="T75">leave-IPFV-CVB</ta>
            <ta e="T77" id="Seg_1481" s="T76">force-VBLZ-CO-2SG.S</ta>
            <ta e="T78" id="Seg_1482" s="T77">earlier</ta>
            <ta e="T79" id="Seg_1483" s="T78">go.quiet-TR-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_1484" s="T79">that-ADJZ</ta>
            <ta e="T81" id="Seg_1485" s="T80">house-LOC</ta>
            <ta e="T82" id="Seg_1486" s="T81">that-ADJZ</ta>
            <ta e="T83" id="Seg_1487" s="T82">house-LOC</ta>
            <ta e="T84" id="Seg_1488" s="T83">find-FUT-1PL</ta>
            <ta e="T85" id="Seg_1489" s="T84">bread-ACC</ta>
            <ta e="T86" id="Seg_1490" s="T85">road-LOC</ta>
            <ta e="T87" id="Seg_1491" s="T86">eat-EP-FRQ-INF</ta>
            <ta e="T88" id="Seg_1492" s="T87">become-CO-2SG.S</ta>
            <ta e="T89" id="Seg_1493" s="T88">brother-ILL-OBL.3SG</ta>
            <ta e="T90" id="Seg_1494" s="T89">brother.[NOM]</ta>
            <ta e="T91" id="Seg_1495" s="T90">I.NOM</ta>
            <ta e="T92" id="Seg_1496" s="T91">NEG</ta>
            <ta e="T93" id="Seg_1497" s="T92">want-1SG.O</ta>
            <ta e="T94" id="Seg_1498" s="T93">oneself.2SG.GEN</ta>
            <ta e="T95" id="Seg_1499" s="T94">human.being-EP-PL-ILL.2SG</ta>
            <ta e="T96" id="Seg_1500" s="T95">leave-IMP.2SG.O</ta>
            <ta e="T97" id="Seg_1501" s="T96">we.PL.NOM</ta>
            <ta e="T98" id="Seg_1502" s="T97">human.being-EP-PL-EP-GEN</ta>
            <ta e="T99" id="Seg_1503" s="T98">inside-LOC</ta>
            <ta e="T100" id="Seg_1504" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1505" s="T100">die-FUT-1PL</ta>
            <ta e="T102" id="Seg_1506" s="T101">say-PST.NAR-3SG.O</ta>
            <ta e="T103" id="Seg_1507" s="T102">Petro.[NOM]</ta>
            <ta e="T104" id="Seg_1508" s="T103">then</ta>
            <ta e="T105" id="Seg_1509" s="T104">take-PST.NAR-3SG.O</ta>
            <ta e="T106" id="Seg_1510" s="T105">hand-ILL-OBL.3SG</ta>
            <ta e="T107" id="Seg_1511" s="T106">knot-PROPR-ADJZ</ta>
            <ta e="T108" id="Seg_1512" s="T107">reins-ACC</ta>
            <ta e="T109" id="Seg_1513" s="T108">Ivan.[NOM]</ta>
            <ta e="T110" id="Seg_1514" s="T109">whether</ta>
            <ta e="T111" id="Seg_1515" s="T110">darkness-ADVZ</ta>
            <ta e="T112" id="Seg_1516" s="T111">be-ACTN-TRL.3SG</ta>
            <ta e="T113" id="Seg_1517" s="T112">hurry-ATTEN-CVB</ta>
            <ta e="T114" id="Seg_1518" s="T113">kiss-PST.NAR-3SG.O</ta>
            <ta e="T115" id="Seg_1519" s="T114">brother-ACC-3SG</ta>
            <ta e="T116" id="Seg_1520" s="T115">much</ta>
            <ta e="T117" id="Seg_1521" s="T116">time.[NOM]</ta>
            <ta e="T118" id="Seg_1522" s="T117">cheek-PROL-OBL.3SG</ta>
            <ta e="T119" id="Seg_1523" s="T118">forehead-PROL-OBL.3SG</ta>
            <ta e="T120" id="Seg_1524" s="T119">nose-PROL-3SG</ta>
            <ta e="T121" id="Seg_1525" s="T120">%%</ta>
            <ta e="T122" id="Seg_1526" s="T121">whether</ta>
            <ta e="T123" id="Seg_1527" s="T122">NEG</ta>
            <ta e="T124" id="Seg_1528" s="T123">see-DRV-FUT-1PL</ta>
            <ta e="T125" id="Seg_1529" s="T124">say-PST.NAR-3SG.O</ta>
            <ta e="T126" id="Seg_1530" s="T125">(s)he.[NOM]</ta>
            <ta e="T127" id="Seg_1531" s="T126">Ivan.[NOM]</ta>
            <ta e="T128" id="Seg_1532" s="T127">push-MOM-PST.NAR-3SG.O</ta>
            <ta e="T129" id="Seg_1533" s="T128">push-MOM-CO-3SG.O</ta>
            <ta e="T130" id="Seg_1534" s="T129">sledge-ACC-3SG</ta>
            <ta e="T131" id="Seg_1535" s="T130">then</ta>
            <ta e="T132" id="Seg_1536" s="T131">horse.[NOM]</ta>
            <ta e="T133" id="Seg_1537" s="T132">go-INCH-FRQ-CO.[3SG.S]</ta>
            <ta e="T134" id="Seg_1538" s="T133">Petro.[NOM]</ta>
            <ta e="T135" id="Seg_1539" s="T134">go-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T136" id="Seg_1540" s="T135">go-PST.NAR.[3SG.S]</ta>
            <ta e="T137" id="Seg_1541" s="T136">sledge-GEN-3SG</ta>
            <ta e="T138" id="Seg_1542" s="T137">near-LOC</ta>
            <ta e="T139" id="Seg_1543" s="T138">Ivan.[NOM]</ta>
            <ta e="T140" id="Seg_1544" s="T139">thus.much</ta>
            <ta e="T141" id="Seg_1545" s="T140">stand-PST.NAR.[3SG.S]</ta>
            <ta e="T142" id="Seg_1546" s="T141">stand-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_1547" s="T142">long</ta>
            <ta e="T144" id="Seg_1548" s="T143">stand-PST.NAR.[3SG.S]</ta>
            <ta e="T145" id="Seg_1549" s="T144">how</ta>
            <ta e="T146" id="Seg_1550" s="T145">sledge-GEN-3SG</ta>
            <ta e="T147" id="Seg_1551" s="T146">creak-DUR-ACTN.[NOM]</ta>
            <ta e="T148" id="Seg_1552" s="T147">NEG</ta>
            <ta e="T150" id="Seg_1553" s="T148">be.lost-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T151" id="Seg_1554" s="T150">extinguish-US-IPFV-HAB.[3SG.S]</ta>
            <ta e="T152" id="Seg_1555" s="T151">still</ta>
            <ta e="T153" id="Seg_1556" s="T152">NEG</ta>
            <ta e="T154" id="Seg_1557" s="T153">blow.out-%%-PST.NAR.[3SG.S]</ta>
            <ta e="T155" id="Seg_1558" s="T154">(s)he.[NOM]</ta>
            <ta e="T156" id="Seg_1559" s="T155">again</ta>
            <ta e="T157" id="Seg_1560" s="T156">burn-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T158" id="Seg_1561" s="T157">set.fire-PFV-PST.NAR-3SG.O</ta>
            <ta e="T159" id="Seg_1562" s="T158">Ivan.[NOM]</ta>
            <ta e="T160" id="Seg_1563" s="T159">house.[NOM]</ta>
            <ta e="T161" id="Seg_1564" s="T160">come.in-PST.NAR.[3SG.S]</ta>
            <ta e="T162" id="Seg_1565" s="T161">down</ta>
            <ta e="T163" id="Seg_1566" s="T162">blow.out-TR-SUP.2/3SG-EMPH</ta>
            <ta e="T164" id="Seg_1567" s="T163">extinguish-US-IPFV-HAB.[3SG.S]</ta>
            <ta e="T165" id="Seg_1568" s="T164">sometimes</ta>
            <ta e="T166" id="Seg_1569" s="T165">again</ta>
            <ta e="T167" id="Seg_1570" s="T166">burn.[3SG.S]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1571" s="T0">Петро.[NOM]</ta>
            <ta e="T2" id="Seg_1572" s="T1">молиться-DETR-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T3" id="Seg_1573" s="T2">потом</ta>
            <ta e="T4" id="Seg_1574" s="T3">поднять-PST.NAR-3SG.O</ta>
            <ta e="T5" id="Seg_1575" s="T4">дерево-DIM-ACC</ta>
            <ta e="T6" id="Seg_1576" s="T5">отправиться-SUP.2/3SG</ta>
            <ta e="T7" id="Seg_1577" s="T6">два-ORD</ta>
            <ta e="T8" id="Seg_1578" s="T7">и</ta>
            <ta e="T9" id="Seg_1579" s="T8">назад</ta>
            <ta e="T10" id="Seg_1580" s="T9">вернуться-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T11" id="Seg_1581" s="T10">дом-ILL-OBL.3SG</ta>
            <ta e="T12" id="Seg_1582" s="T11">он(а).[NOM]</ta>
            <ta e="T13" id="Seg_1583" s="T12">ночь-GEN</ta>
            <ta e="T14" id="Seg_1584" s="T13">середина-LOC</ta>
            <ta e="T15" id="Seg_1585" s="T14">щупать-MULO-TR-PST.NAR-3SG.O</ta>
            <ta e="T16" id="Seg_1586" s="T15">голый</ta>
            <ta e="T17" id="Seg_1587" s="T16">рука-INSTR</ta>
            <ta e="T18" id="Seg_1588" s="T17">рвать-TR-PTCP.PST</ta>
            <ta e="T19" id="Seg_1589" s="T18">ломать-RES-PTCP.PST</ta>
            <ta e="T20" id="Seg_1590" s="T19">дверь-ACC</ta>
            <ta e="T21" id="Seg_1591" s="T20">потом</ta>
            <ta e="T22" id="Seg_1592" s="T21">слегка</ta>
            <ta e="T23" id="Seg_1593" s="T22">стучать-ATTEN-PST.NAR-3SG.O</ta>
            <ta e="T24" id="Seg_1594" s="T23">затем</ta>
            <ta e="T25" id="Seg_1595" s="T24">брат.[NOM]</ta>
            <ta e="T26" id="Seg_1596" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_1597" s="T26">отправиться-IPFV-CVB</ta>
            <ta e="T28" id="Seg_1598" s="T27">сила-VBLZ-CO-1SG.S</ta>
            <ta e="T29" id="Seg_1599" s="T28">минута.[NOM]</ta>
            <ta e="T30" id="Seg_1600" s="T29">в.течение</ta>
            <ta e="T31" id="Seg_1601" s="T30">слушать-IPFV-DUR-PST.NAR-3SG.O</ta>
            <ta e="T32" id="Seg_1602" s="T31">немного</ta>
            <ta e="T33" id="Seg_1603" s="T32">потом</ta>
            <ta e="T34" id="Seg_1604" s="T33">опять</ta>
            <ta e="T35" id="Seg_1605" s="T34">новый-ADVZ</ta>
            <ta e="T36" id="Seg_1606" s="T35">щупать-MULO-IPFV-HAB-INF-собраться-DUR-INCH-PST.NAR-3SG.O</ta>
            <ta e="T37" id="Seg_1607" s="T36">этот</ta>
            <ta e="T38" id="Seg_1608" s="T37">сено.[NOM]</ta>
            <ta e="T39" id="Seg_1609" s="T38">дверь-ACC</ta>
            <ta e="T40" id="Seg_1610" s="T39">ударить-MOM-DUR-PTCP.PRS</ta>
            <ta e="T41" id="Seg_1611" s="T40">нагреться-DUR-SUP.2/3SG</ta>
            <ta e="T42" id="Seg_1612" s="T41">тянуть-CVB</ta>
            <ta e="T43" id="Seg_1613" s="T42">кончить-MULO-CVB</ta>
            <ta e="T44" id="Seg_1614" s="T43">опять</ta>
            <ta e="T45" id="Seg_1615" s="T44">шум-CAR-ADVZ</ta>
            <ta e="T46" id="Seg_1616" s="T45">сказать-PST.NAR-3SG.O</ta>
            <ta e="T47" id="Seg_1617" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_1618" s="T47">вот</ta>
            <ta e="T49" id="Seg_1619" s="T48">уйти-FUT-1SG.S</ta>
            <ta e="T50" id="Seg_1620" s="T49">потом</ta>
            <ta e="T51" id="Seg_1621" s="T50">идти-MOM-PST.NAR.[3SG.S]</ta>
            <ta e="T52" id="Seg_1622" s="T51">наружу</ta>
            <ta e="T53" id="Seg_1623" s="T52">он(а).[NOM]</ta>
            <ta e="T54" id="Seg_1624" s="T53">NEG</ta>
            <ta e="T55" id="Seg_1625" s="T54">слышаться-RFL.PFV-PST.NAR-3SG.O</ta>
            <ta e="T56" id="Seg_1626" s="T55">как</ta>
            <ta e="T57" id="Seg_1627" s="T56">дверь.[NOM]</ta>
            <ta e="T58" id="Seg_1628" s="T57">скрипеть-DRV-PST.NAR.[3SG.S]</ta>
            <ta e="T59" id="Seg_1629" s="T58">потом</ta>
            <ta e="T60" id="Seg_1630" s="T59">NEG</ta>
            <ta e="T61" id="Seg_1631" s="T60">видеть-IPFV-FRQ-PFV-3SG.O</ta>
            <ta e="T62" id="Seg_1632" s="T61">как</ta>
            <ta e="T63" id="Seg_1633" s="T62">он(а)-EP-ACC</ta>
            <ta e="T64" id="Seg_1634" s="T63">догонять-PST.NAR-3SG.O</ta>
            <ta e="T65" id="Seg_1635" s="T64">голый</ta>
            <ta e="T66" id="Seg_1636" s="T65">нога-INSTR</ta>
            <ta e="T67" id="Seg_1637" s="T66">снег-PROL</ta>
            <ta e="T68" id="Seg_1638" s="T67">догонять-PST.NAR-3SG.O</ta>
            <ta e="T69" id="Seg_1639" s="T68">словно</ta>
            <ta e="T70" id="Seg_1640" s="T69">трава-LOC</ta>
            <ta e="T71" id="Seg_1641" s="T70">Иван.[NOM]</ta>
            <ta e="T72" id="Seg_1642" s="T71">вот</ta>
            <ta e="T73" id="Seg_1643" s="T72">как</ta>
            <ta e="T74" id="Seg_1644" s="T73">сказать-PST.NAR-2SG.S</ta>
            <ta e="T75" id="Seg_1645" s="T74">что.ли</ta>
            <ta e="T76" id="Seg_1646" s="T75">отправиться-IPFV-CVB</ta>
            <ta e="T77" id="Seg_1647" s="T76">сила-VBLZ-CO-2SG.S</ta>
            <ta e="T78" id="Seg_1648" s="T77">раньше</ta>
            <ta e="T79" id="Seg_1649" s="T78">притихнуть-TR-IMP.2SG.S</ta>
            <ta e="T80" id="Seg_1650" s="T79">тот-ADJZ</ta>
            <ta e="T81" id="Seg_1651" s="T80">дом-LOC</ta>
            <ta e="T82" id="Seg_1652" s="T81">тот-ADJZ</ta>
            <ta e="T83" id="Seg_1653" s="T82">дом-LOC</ta>
            <ta e="T84" id="Seg_1654" s="T83">находить-FUT-1PL</ta>
            <ta e="T85" id="Seg_1655" s="T84">хлеб-ACC</ta>
            <ta e="T86" id="Seg_1656" s="T85">дорога-LOC</ta>
            <ta e="T87" id="Seg_1657" s="T86">съесть-EP-FRQ-INF</ta>
            <ta e="T88" id="Seg_1658" s="T87">стать-CO-2SG.S</ta>
            <ta e="T89" id="Seg_1659" s="T88">брат-ILL-OBL.3SG</ta>
            <ta e="T90" id="Seg_1660" s="T89">брат.[NOM]</ta>
            <ta e="T91" id="Seg_1661" s="T90">я.NOM</ta>
            <ta e="T92" id="Seg_1662" s="T91">NEG</ta>
            <ta e="T93" id="Seg_1663" s="T92">хотеть-1SG.O</ta>
            <ta e="T94" id="Seg_1664" s="T93">сам.2SG.GEN</ta>
            <ta e="T95" id="Seg_1665" s="T94">человек-EP-PL-ILL.2SG</ta>
            <ta e="T96" id="Seg_1666" s="T95">оставить-IMP.2SG.O</ta>
            <ta e="T97" id="Seg_1667" s="T96">мы.PL.NOM</ta>
            <ta e="T98" id="Seg_1668" s="T97">человек-EP-PL-EP-GEN</ta>
            <ta e="T99" id="Seg_1669" s="T98">внутри-LOC</ta>
            <ta e="T100" id="Seg_1670" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1671" s="T100">умереть-FUT-1PL</ta>
            <ta e="T102" id="Seg_1672" s="T101">сказать-PST.NAR-3SG.O</ta>
            <ta e="T103" id="Seg_1673" s="T102">Петро.[NOM]</ta>
            <ta e="T104" id="Seg_1674" s="T103">потом</ta>
            <ta e="T105" id="Seg_1675" s="T104">взять-PST.NAR-3SG.O</ta>
            <ta e="T106" id="Seg_1676" s="T105">рука-ILL-OBL.3SG</ta>
            <ta e="T107" id="Seg_1677" s="T106">узел-PROPR-ADJZ</ta>
            <ta e="T108" id="Seg_1678" s="T107">вожжи-ACC</ta>
            <ta e="T109" id="Seg_1679" s="T108">Иван.[NOM]</ta>
            <ta e="T110" id="Seg_1680" s="T109">что.ли</ta>
            <ta e="T111" id="Seg_1681" s="T110">темнота-ADVZ</ta>
            <ta e="T112" id="Seg_1682" s="T111">быть-ACTN-TRL.3SG</ta>
            <ta e="T113" id="Seg_1683" s="T112">торопиться-ATTEN-CVB</ta>
            <ta e="T114" id="Seg_1684" s="T113">целовать-PST.NAR-3SG.O</ta>
            <ta e="T115" id="Seg_1685" s="T114">брат-ACC-3SG</ta>
            <ta e="T116" id="Seg_1686" s="T115">много</ta>
            <ta e="T117" id="Seg_1687" s="T116">раз.[NOM]</ta>
            <ta e="T118" id="Seg_1688" s="T117">щека-PROL-OBL.3SG</ta>
            <ta e="T119" id="Seg_1689" s="T118">лоб-PROL-OBL.3SG</ta>
            <ta e="T120" id="Seg_1690" s="T119">нос-PROL-3SG</ta>
            <ta e="T121" id="Seg_1691" s="T120">%%</ta>
            <ta e="T122" id="Seg_1692" s="T121">что.ли</ta>
            <ta e="T123" id="Seg_1693" s="T122">NEG</ta>
            <ta e="T124" id="Seg_1694" s="T123">видеть-DRV-FUT-1PL</ta>
            <ta e="T125" id="Seg_1695" s="T124">сказать-PST.NAR-3SG.O</ta>
            <ta e="T126" id="Seg_1696" s="T125">он(а).[NOM]</ta>
            <ta e="T127" id="Seg_1697" s="T126">Иван.[NOM]</ta>
            <ta e="T128" id="Seg_1698" s="T127">толкать-MOM-PST.NAR-3SG.O</ta>
            <ta e="T129" id="Seg_1699" s="T128">толкать-MOM-CO-3SG.O</ta>
            <ta e="T130" id="Seg_1700" s="T129">нарты-ACC-3SG</ta>
            <ta e="T131" id="Seg_1701" s="T130">потом</ta>
            <ta e="T132" id="Seg_1702" s="T131">лошадь.[NOM]</ta>
            <ta e="T133" id="Seg_1703" s="T132">идти-INCH-FRQ-CO.[3SG.S]</ta>
            <ta e="T134" id="Seg_1704" s="T133">Петро.[NOM]</ta>
            <ta e="T135" id="Seg_1705" s="T134">ходить-DUR-PST.NAR.[3SG.S]</ta>
            <ta e="T136" id="Seg_1706" s="T135">ходить-PST.NAR.[3SG.S]</ta>
            <ta e="T137" id="Seg_1707" s="T136">нарты-GEN-3SG</ta>
            <ta e="T138" id="Seg_1708" s="T137">около-LOC</ta>
            <ta e="T139" id="Seg_1709" s="T138">Иван.[NOM]</ta>
            <ta e="T140" id="Seg_1710" s="T139">настолько</ta>
            <ta e="T141" id="Seg_1711" s="T140">стоять-PST.NAR.[3SG.S]</ta>
            <ta e="T142" id="Seg_1712" s="T141">стоять-CO.[3SG.S]</ta>
            <ta e="T143" id="Seg_1713" s="T142">долго</ta>
            <ta e="T144" id="Seg_1714" s="T143">стоять-PST.NAR.[3SG.S]</ta>
            <ta e="T145" id="Seg_1715" s="T144">как</ta>
            <ta e="T146" id="Seg_1716" s="T145">нарты-GEN-3SG</ta>
            <ta e="T147" id="Seg_1717" s="T146">скрипеть-DUR-ACTN.[NOM]</ta>
            <ta e="T148" id="Seg_1718" s="T147">NEG</ta>
            <ta e="T150" id="Seg_1719" s="T148">потеряться-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T151" id="Seg_1720" s="T150">погасить-US-IPFV-HAB.[3SG.S]</ta>
            <ta e="T152" id="Seg_1721" s="T151">пока</ta>
            <ta e="T153" id="Seg_1722" s="T152">NEG</ta>
            <ta e="T154" id="Seg_1723" s="T153">погаснуть-%%-PST.NAR.[3SG.S]</ta>
            <ta e="T155" id="Seg_1724" s="T154">он(а).[NOM]</ta>
            <ta e="T156" id="Seg_1725" s="T155">снова</ta>
            <ta e="T157" id="Seg_1726" s="T156">гореть-PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T158" id="Seg_1727" s="T157">зажечь-PFV-PST.NAR-3SG.O</ta>
            <ta e="T159" id="Seg_1728" s="T158">Иван.[NOM]</ta>
            <ta e="T160" id="Seg_1729" s="T159">дом.[NOM]</ta>
            <ta e="T161" id="Seg_1730" s="T160">войти-PST.NAR.[3SG.S]</ta>
            <ta e="T162" id="Seg_1731" s="T161">вниз</ta>
            <ta e="T163" id="Seg_1732" s="T162">погаснуть-TR-SUP.2/3SG-EMPH</ta>
            <ta e="T164" id="Seg_1733" s="T163">погасить-US-IPFV-HAB.[3SG.S]</ta>
            <ta e="T165" id="Seg_1734" s="T164">некоторое.время</ta>
            <ta e="T166" id="Seg_1735" s="T165">опять</ta>
            <ta e="T167" id="Seg_1736" s="T166">гореть.[3SG.S]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1737" s="T0">nprop-n:case</ta>
            <ta e="T2" id="Seg_1738" s="T1">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1739" s="T2">adv</ta>
            <ta e="T4" id="Seg_1740" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1741" s="T4">n-n&gt;n-n:case</ta>
            <ta e="T6" id="Seg_1742" s="T5">v-v:inf.poss</ta>
            <ta e="T7" id="Seg_1743" s="T6">num-num&gt;adv</ta>
            <ta e="T8" id="Seg_1744" s="T7">conj</ta>
            <ta e="T9" id="Seg_1745" s="T8">adv</ta>
            <ta e="T10" id="Seg_1746" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1747" s="T10">n-n:case-n:obl.poss</ta>
            <ta e="T12" id="Seg_1748" s="T11">pers-n:case</ta>
            <ta e="T13" id="Seg_1749" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1750" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1751" s="T14">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1752" s="T15">adj</ta>
            <ta e="T17" id="Seg_1753" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1754" s="T17">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T19" id="Seg_1755" s="T18">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T20" id="Seg_1756" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1757" s="T20">adv</ta>
            <ta e="T22" id="Seg_1758" s="T21">adv</ta>
            <ta e="T23" id="Seg_1759" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1760" s="T23">adv</ta>
            <ta e="T25" id="Seg_1761" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1762" s="T25">pers</ta>
            <ta e="T27" id="Seg_1763" s="T26">v-v&gt;v-v&gt;adv</ta>
            <ta e="T28" id="Seg_1764" s="T27">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T29" id="Seg_1765" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1766" s="T29">pp</ta>
            <ta e="T31" id="Seg_1767" s="T30">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1768" s="T31">adv</ta>
            <ta e="T33" id="Seg_1769" s="T32">adv</ta>
            <ta e="T34" id="Seg_1770" s="T33">adv</ta>
            <ta e="T35" id="Seg_1771" s="T34">adj-adj&gt;adv</ta>
            <ta e="T36" id="Seg_1772" s="T35">v-v&gt;v-v&gt;v-v&gt;v-v:inf-v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1773" s="T36">dem</ta>
            <ta e="T38" id="Seg_1774" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1775" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1776" s="T39">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T41" id="Seg_1777" s="T40">v-v&gt;v-v:inf.poss</ta>
            <ta e="T42" id="Seg_1778" s="T41">v-v&gt;adv</ta>
            <ta e="T43" id="Seg_1779" s="T42">v-v&gt;v-v&gt;adv</ta>
            <ta e="T44" id="Seg_1780" s="T43">adv</ta>
            <ta e="T45" id="Seg_1781" s="T44">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T46" id="Seg_1782" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1783" s="T46">pers</ta>
            <ta e="T48" id="Seg_1784" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1785" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1786" s="T49">adv</ta>
            <ta e="T51" id="Seg_1787" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1788" s="T51">adv</ta>
            <ta e="T53" id="Seg_1789" s="T52">pers-n:case</ta>
            <ta e="T54" id="Seg_1790" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1791" s="T54">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_1792" s="T55">conj</ta>
            <ta e="T57" id="Seg_1793" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1794" s="T57">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1795" s="T58">adv</ta>
            <ta e="T60" id="Seg_1796" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_1797" s="T60">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T62" id="Seg_1798" s="T61">conj</ta>
            <ta e="T63" id="Seg_1799" s="T62">pers-n:ins-n:case</ta>
            <ta e="T64" id="Seg_1800" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1801" s="T64">adj</ta>
            <ta e="T66" id="Seg_1802" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_1803" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1804" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1805" s="T68">conj</ta>
            <ta e="T70" id="Seg_1806" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_1807" s="T70">nprop-n:case</ta>
            <ta e="T72" id="Seg_1808" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1809" s="T72">interrog</ta>
            <ta e="T74" id="Seg_1810" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1811" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1812" s="T75">v-v&gt;v-v&gt;adv</ta>
            <ta e="T77" id="Seg_1813" s="T76">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T78" id="Seg_1814" s="T77">adv</ta>
            <ta e="T79" id="Seg_1815" s="T78">v-v&gt;v-v:mood.pn</ta>
            <ta e="T80" id="Seg_1816" s="T79">dem-n&gt;adj</ta>
            <ta e="T81" id="Seg_1817" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1818" s="T81">dem-n&gt;adj</ta>
            <ta e="T83" id="Seg_1819" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1820" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1821" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1822" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1823" s="T86">v-v:ins-v&gt;v-v:inf</ta>
            <ta e="T88" id="Seg_1824" s="T87">v-v:ins-v:pn</ta>
            <ta e="T89" id="Seg_1825" s="T88">n-n:case-n:obl.poss</ta>
            <ta e="T90" id="Seg_1826" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1827" s="T90">pers</ta>
            <ta e="T92" id="Seg_1828" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1829" s="T92">v-v:pn</ta>
            <ta e="T94" id="Seg_1830" s="T93">emphpro-n:case</ta>
            <ta e="T95" id="Seg_1831" s="T94">n-n:ins-n:num-n:case.poss</ta>
            <ta e="T96" id="Seg_1832" s="T95">v-v:mood.pn</ta>
            <ta e="T97" id="Seg_1833" s="T96">pers</ta>
            <ta e="T98" id="Seg_1834" s="T97">n-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T99" id="Seg_1835" s="T98">pp-n:case</ta>
            <ta e="T100" id="Seg_1836" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_1837" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1838" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_1839" s="T102">nprop-n:case</ta>
            <ta e="T104" id="Seg_1840" s="T103">adv</ta>
            <ta e="T105" id="Seg_1841" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1842" s="T105">n-n:case-n:obl.poss</ta>
            <ta e="T107" id="Seg_1843" s="T106">n-n&gt;n-n&gt;adj</ta>
            <ta e="T108" id="Seg_1844" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_1845" s="T108">nprop-n:case</ta>
            <ta e="T110" id="Seg_1846" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_1847" s="T110">n-n&gt;adv</ta>
            <ta e="T112" id="Seg_1848" s="T111">v-v&gt;n-n:case.poss</ta>
            <ta e="T113" id="Seg_1849" s="T112">v-v&gt;v-v&gt;adv</ta>
            <ta e="T114" id="Seg_1850" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1851" s="T114">n-n:case-n:poss</ta>
            <ta e="T116" id="Seg_1852" s="T115">quant</ta>
            <ta e="T117" id="Seg_1853" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1854" s="T117">n-n:case-n:obl.poss</ta>
            <ta e="T119" id="Seg_1855" s="T118">n-n:case-n:obl.poss</ta>
            <ta e="T120" id="Seg_1856" s="T119">n-n:case-n:poss</ta>
            <ta e="T121" id="Seg_1857" s="T120">adv</ta>
            <ta e="T122" id="Seg_1858" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1859" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1860" s="T123">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1861" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1862" s="T125">pers-n:case</ta>
            <ta e="T127" id="Seg_1863" s="T126">nprop-n:case</ta>
            <ta e="T128" id="Seg_1864" s="T127">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_1865" s="T128">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T130" id="Seg_1866" s="T129">n-n:case-n:poss</ta>
            <ta e="T131" id="Seg_1867" s="T130">adv</ta>
            <ta e="T132" id="Seg_1868" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_1869" s="T132">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T134" id="Seg_1870" s="T133">nprop-n:case</ta>
            <ta e="T135" id="Seg_1871" s="T134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_1872" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1873" s="T136">n-n:case-n:poss</ta>
            <ta e="T138" id="Seg_1874" s="T137">pp-n:case</ta>
            <ta e="T139" id="Seg_1875" s="T138">nprop-n:case</ta>
            <ta e="T140" id="Seg_1876" s="T139">adv</ta>
            <ta e="T141" id="Seg_1877" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_1878" s="T141">v--v:ins-v:pn</ta>
            <ta e="T143" id="Seg_1879" s="T142">adv</ta>
            <ta e="T144" id="Seg_1880" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1881" s="T144">conj</ta>
            <ta e="T146" id="Seg_1882" s="T145">n-n:case-n:poss</ta>
            <ta e="T147" id="Seg_1883" s="T146">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T148" id="Seg_1884" s="T147">ptcl</ta>
            <ta e="T150" id="Seg_1885" s="T148">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_1886" s="T150">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_1887" s="T151">adv</ta>
            <ta e="T153" id="Seg_1888" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_1889" s="T153">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T155" id="Seg_1890" s="T154">pers-n:case</ta>
            <ta e="T156" id="Seg_1891" s="T155">adv</ta>
            <ta e="T157" id="Seg_1892" s="T156">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_1893" s="T157">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_1894" s="T158">nprop-n:case</ta>
            <ta e="T160" id="Seg_1895" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_1896" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_1897" s="T161">preverb</ta>
            <ta e="T163" id="Seg_1898" s="T162">v-v&gt;v-v:inf.poss-clit</ta>
            <ta e="T164" id="Seg_1899" s="T163">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T165" id="Seg_1900" s="T164">adv</ta>
            <ta e="T166" id="Seg_1901" s="T165">adv</ta>
            <ta e="T167" id="Seg_1902" s="T166">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1903" s="T0">nprop</ta>
            <ta e="T2" id="Seg_1904" s="T1">v</ta>
            <ta e="T3" id="Seg_1905" s="T2">adv</ta>
            <ta e="T4" id="Seg_1906" s="T3">v</ta>
            <ta e="T5" id="Seg_1907" s="T4">n</ta>
            <ta e="T6" id="Seg_1908" s="T5">v</ta>
            <ta e="T7" id="Seg_1909" s="T6">adv</ta>
            <ta e="T8" id="Seg_1910" s="T7">conj</ta>
            <ta e="T9" id="Seg_1911" s="T8">adv</ta>
            <ta e="T10" id="Seg_1912" s="T9">v</ta>
            <ta e="T11" id="Seg_1913" s="T10">n</ta>
            <ta e="T12" id="Seg_1914" s="T11">pers</ta>
            <ta e="T13" id="Seg_1915" s="T12">n</ta>
            <ta e="T14" id="Seg_1916" s="T13">n</ta>
            <ta e="T15" id="Seg_1917" s="T14">v</ta>
            <ta e="T16" id="Seg_1918" s="T15">adj</ta>
            <ta e="T17" id="Seg_1919" s="T16">n</ta>
            <ta e="T18" id="Seg_1920" s="T17">v</ta>
            <ta e="T19" id="Seg_1921" s="T18">ptcp</ta>
            <ta e="T20" id="Seg_1922" s="T19">n</ta>
            <ta e="T21" id="Seg_1923" s="T20">adv</ta>
            <ta e="T22" id="Seg_1924" s="T21">adv</ta>
            <ta e="T23" id="Seg_1925" s="T22">v</ta>
            <ta e="T24" id="Seg_1926" s="T23">adv</ta>
            <ta e="T25" id="Seg_1927" s="T24">n</ta>
            <ta e="T26" id="Seg_1928" s="T25">pers</ta>
            <ta e="T27" id="Seg_1929" s="T26">adv</ta>
            <ta e="T28" id="Seg_1930" s="T27">v</ta>
            <ta e="T29" id="Seg_1931" s="T28">n</ta>
            <ta e="T30" id="Seg_1932" s="T29">pp</ta>
            <ta e="T31" id="Seg_1933" s="T30">v</ta>
            <ta e="T32" id="Seg_1934" s="T31">adv</ta>
            <ta e="T33" id="Seg_1935" s="T32">adv</ta>
            <ta e="T34" id="Seg_1936" s="T33">adv</ta>
            <ta e="T35" id="Seg_1937" s="T34">adv</ta>
            <ta e="T36" id="Seg_1938" s="T35">v</ta>
            <ta e="T37" id="Seg_1939" s="T36">dem</ta>
            <ta e="T38" id="Seg_1940" s="T37">n</ta>
            <ta e="T39" id="Seg_1941" s="T38">n</ta>
            <ta e="T40" id="Seg_1942" s="T39">ptcp</ta>
            <ta e="T41" id="Seg_1943" s="T40">v</ta>
            <ta e="T42" id="Seg_1944" s="T41">adv</ta>
            <ta e="T43" id="Seg_1945" s="T42">adv</ta>
            <ta e="T44" id="Seg_1946" s="T43">adv</ta>
            <ta e="T45" id="Seg_1947" s="T44">adv</ta>
            <ta e="T46" id="Seg_1948" s="T45">v</ta>
            <ta e="T47" id="Seg_1949" s="T46">pers</ta>
            <ta e="T48" id="Seg_1950" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1951" s="T48">v</ta>
            <ta e="T50" id="Seg_1952" s="T49">adv</ta>
            <ta e="T51" id="Seg_1953" s="T50">v</ta>
            <ta e="T52" id="Seg_1954" s="T51">adv</ta>
            <ta e="T53" id="Seg_1955" s="T52">pers</ta>
            <ta e="T54" id="Seg_1956" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1957" s="T54">v</ta>
            <ta e="T56" id="Seg_1958" s="T55">conj</ta>
            <ta e="T57" id="Seg_1959" s="T56">n</ta>
            <ta e="T58" id="Seg_1960" s="T57">v</ta>
            <ta e="T59" id="Seg_1961" s="T58">adv</ta>
            <ta e="T60" id="Seg_1962" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_1963" s="T60">v</ta>
            <ta e="T62" id="Seg_1964" s="T61">conj</ta>
            <ta e="T63" id="Seg_1965" s="T62">pers</ta>
            <ta e="T64" id="Seg_1966" s="T63">v</ta>
            <ta e="T65" id="Seg_1967" s="T64">adj</ta>
            <ta e="T66" id="Seg_1968" s="T65">n</ta>
            <ta e="T67" id="Seg_1969" s="T66">n</ta>
            <ta e="T68" id="Seg_1970" s="T67">v</ta>
            <ta e="T69" id="Seg_1971" s="T68">conj</ta>
            <ta e="T70" id="Seg_1972" s="T69">n</ta>
            <ta e="T71" id="Seg_1973" s="T70">nprop</ta>
            <ta e="T72" id="Seg_1974" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1975" s="T72">adv</ta>
            <ta e="T74" id="Seg_1976" s="T73">v</ta>
            <ta e="T75" id="Seg_1977" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1978" s="T75">adv</ta>
            <ta e="T77" id="Seg_1979" s="T76">v</ta>
            <ta e="T78" id="Seg_1980" s="T77">adv</ta>
            <ta e="T79" id="Seg_1981" s="T78">v</ta>
            <ta e="T80" id="Seg_1982" s="T79">adj</ta>
            <ta e="T81" id="Seg_1983" s="T80">n</ta>
            <ta e="T82" id="Seg_1984" s="T81">adj</ta>
            <ta e="T83" id="Seg_1985" s="T82">n</ta>
            <ta e="T84" id="Seg_1986" s="T83">v</ta>
            <ta e="T85" id="Seg_1987" s="T84">n</ta>
            <ta e="T86" id="Seg_1988" s="T85">n</ta>
            <ta e="T87" id="Seg_1989" s="T86">v</ta>
            <ta e="T88" id="Seg_1990" s="T87">v</ta>
            <ta e="T89" id="Seg_1991" s="T88">n</ta>
            <ta e="T90" id="Seg_1992" s="T89">n</ta>
            <ta e="T91" id="Seg_1993" s="T90">pers</ta>
            <ta e="T92" id="Seg_1994" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1995" s="T92">v</ta>
            <ta e="T94" id="Seg_1996" s="T93">emphpro</ta>
            <ta e="T95" id="Seg_1997" s="T94">n</ta>
            <ta e="T96" id="Seg_1998" s="T95">v</ta>
            <ta e="T97" id="Seg_1999" s="T96">pers</ta>
            <ta e="T98" id="Seg_2000" s="T97">n</ta>
            <ta e="T99" id="Seg_2001" s="T98">pp</ta>
            <ta e="T100" id="Seg_2002" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2003" s="T100">v</ta>
            <ta e="T102" id="Seg_2004" s="T101">v</ta>
            <ta e="T103" id="Seg_2005" s="T102">nprop</ta>
            <ta e="T104" id="Seg_2006" s="T103">adv</ta>
            <ta e="T105" id="Seg_2007" s="T104">v</ta>
            <ta e="T106" id="Seg_2008" s="T105">n</ta>
            <ta e="T107" id="Seg_2009" s="T106">adj</ta>
            <ta e="T108" id="Seg_2010" s="T107">n</ta>
            <ta e="T109" id="Seg_2011" s="T108">nprop</ta>
            <ta e="T110" id="Seg_2012" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2013" s="T110">adv</ta>
            <ta e="T112" id="Seg_2014" s="T111">n</ta>
            <ta e="T113" id="Seg_2015" s="T112">adv</ta>
            <ta e="T114" id="Seg_2016" s="T113">v</ta>
            <ta e="T115" id="Seg_2017" s="T114">n</ta>
            <ta e="T116" id="Seg_2018" s="T115">quant</ta>
            <ta e="T117" id="Seg_2019" s="T116">n</ta>
            <ta e="T118" id="Seg_2020" s="T117">n</ta>
            <ta e="T119" id="Seg_2021" s="T118">n</ta>
            <ta e="T120" id="Seg_2022" s="T119">n</ta>
            <ta e="T121" id="Seg_2023" s="T120">adv</ta>
            <ta e="T122" id="Seg_2024" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_2025" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_2026" s="T123">v</ta>
            <ta e="T125" id="Seg_2027" s="T124">v</ta>
            <ta e="T126" id="Seg_2028" s="T125">pers</ta>
            <ta e="T127" id="Seg_2029" s="T126">nprop</ta>
            <ta e="T128" id="Seg_2030" s="T127">v</ta>
            <ta e="T129" id="Seg_2031" s="T128">v</ta>
            <ta e="T130" id="Seg_2032" s="T129">n</ta>
            <ta e="T131" id="Seg_2033" s="T130">adv</ta>
            <ta e="T132" id="Seg_2034" s="T131">n</ta>
            <ta e="T133" id="Seg_2035" s="T132">v</ta>
            <ta e="T134" id="Seg_2036" s="T133">nprop</ta>
            <ta e="T135" id="Seg_2037" s="T134">v</ta>
            <ta e="T136" id="Seg_2038" s="T135">v</ta>
            <ta e="T137" id="Seg_2039" s="T136">n</ta>
            <ta e="T138" id="Seg_2040" s="T137">pp</ta>
            <ta e="T139" id="Seg_2041" s="T138">nprop</ta>
            <ta e="T140" id="Seg_2042" s="T139">adv</ta>
            <ta e="T141" id="Seg_2043" s="T140">v</ta>
            <ta e="T142" id="Seg_2044" s="T141">v</ta>
            <ta e="T143" id="Seg_2045" s="T142">adv</ta>
            <ta e="T144" id="Seg_2046" s="T143">v</ta>
            <ta e="T145" id="Seg_2047" s="T144">conj</ta>
            <ta e="T146" id="Seg_2048" s="T145">n</ta>
            <ta e="T147" id="Seg_2049" s="T146">n</ta>
            <ta e="T148" id="Seg_2050" s="T147">ptcl</ta>
            <ta e="T150" id="Seg_2051" s="T148">v</ta>
            <ta e="T151" id="Seg_2052" s="T150">v</ta>
            <ta e="T152" id="Seg_2053" s="T151">adv</ta>
            <ta e="T153" id="Seg_2054" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_2055" s="T153">v</ta>
            <ta e="T155" id="Seg_2056" s="T154">pers</ta>
            <ta e="T156" id="Seg_2057" s="T155">adv</ta>
            <ta e="T157" id="Seg_2058" s="T156">v</ta>
            <ta e="T158" id="Seg_2059" s="T157">v</ta>
            <ta e="T159" id="Seg_2060" s="T158">nprop</ta>
            <ta e="T160" id="Seg_2061" s="T159">n</ta>
            <ta e="T161" id="Seg_2062" s="T160">v</ta>
            <ta e="T162" id="Seg_2063" s="T161">preverb</ta>
            <ta e="T163" id="Seg_2064" s="T162">v</ta>
            <ta e="T164" id="Seg_2065" s="T163">v</ta>
            <ta e="T165" id="Seg_2066" s="T164">adv</ta>
            <ta e="T166" id="Seg_2067" s="T165">adv</ta>
            <ta e="T167" id="Seg_2068" s="T166">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2069" s="T0">np.h:A</ta>
            <ta e="T3" id="Seg_2070" s="T2">adv:Time</ta>
            <ta e="T4" id="Seg_2071" s="T3">0.3.h:A</ta>
            <ta e="T5" id="Seg_2072" s="T4">np:Th</ta>
            <ta e="T6" id="Seg_2073" s="T5">0.3.h:A</ta>
            <ta e="T9" id="Seg_2074" s="T8">adv:G</ta>
            <ta e="T10" id="Seg_2075" s="T9">0.3.h:A</ta>
            <ta e="T11" id="Seg_2076" s="T10">np:G</ta>
            <ta e="T12" id="Seg_2077" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_2078" s="T13">np:Time</ta>
            <ta e="T17" id="Seg_2079" s="T16">np:Ins</ta>
            <ta e="T20" id="Seg_2080" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_2081" s="T20">adv:Time</ta>
            <ta e="T23" id="Seg_2082" s="T22">0.3.h:A 0.3:Th</ta>
            <ta e="T26" id="Seg_2083" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_2084" s="T28">pp:Time</ta>
            <ta e="T31" id="Seg_2085" s="T30">0.3.h:A 0.3:Th</ta>
            <ta e="T33" id="Seg_2086" s="T32">adv:Time</ta>
            <ta e="T36" id="Seg_2087" s="T35">0.3.h:A</ta>
            <ta e="T39" id="Seg_2088" s="T38">np:Th</ta>
            <ta e="T46" id="Seg_2089" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_2090" s="T46">pro.h:A</ta>
            <ta e="T50" id="Seg_2091" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_2092" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_2093" s="T51">adv:G</ta>
            <ta e="T53" id="Seg_2094" s="T52">pro.h:E</ta>
            <ta e="T59" id="Seg_2095" s="T58">adv:Time</ta>
            <ta e="T61" id="Seg_2096" s="T60">0.3.h:E</ta>
            <ta e="T67" id="Seg_2097" s="T66">np:Path</ta>
            <ta e="T71" id="Seg_2098" s="T70">np.h:A</ta>
            <ta e="T74" id="Seg_2099" s="T73">0.2.h:A</ta>
            <ta e="T77" id="Seg_2100" s="T76">0.2.h:A</ta>
            <ta e="T78" id="Seg_2101" s="T77">adv:Time</ta>
            <ta e="T79" id="Seg_2102" s="T78">0.2.h:A</ta>
            <ta e="T81" id="Seg_2103" s="T80">np:L</ta>
            <ta e="T84" id="Seg_2104" s="T83">0.1.h:B</ta>
            <ta e="T85" id="Seg_2105" s="T84">np:Th</ta>
            <ta e="T86" id="Seg_2106" s="T85">np:L</ta>
            <ta e="T88" id="Seg_2107" s="T87">0.2.h:E</ta>
            <ta e="T91" id="Seg_2108" s="T90">pro.h:E</ta>
            <ta e="T93" id="Seg_2109" s="T92">0.3:Th</ta>
            <ta e="T94" id="Seg_2110" s="T93">pro.h:Poss</ta>
            <ta e="T95" id="Seg_2111" s="T94">np.h:B</ta>
            <ta e="T96" id="Seg_2112" s="T95">0.2.h:A 0.3:Th</ta>
            <ta e="T97" id="Seg_2113" s="T96">pro.h:P</ta>
            <ta e="T98" id="Seg_2114" s="T97">pp:L</ta>
            <ta e="T103" id="Seg_2115" s="T102">np.h:A</ta>
            <ta e="T104" id="Seg_2116" s="T103">adv:Time</ta>
            <ta e="T105" id="Seg_2117" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_2118" s="T105">np:G</ta>
            <ta e="T108" id="Seg_2119" s="T107">np:Th</ta>
            <ta e="T109" id="Seg_2120" s="T108">np.h:A</ta>
            <ta e="T115" id="Seg_2121" s="T114">np.h:Th 0.3.h:Poss</ta>
            <ta e="T118" id="Seg_2122" s="T117">np:G</ta>
            <ta e="T119" id="Seg_2123" s="T118">np:G</ta>
            <ta e="T120" id="Seg_2124" s="T119">np:G</ta>
            <ta e="T121" id="Seg_2125" s="T120">adv:Time</ta>
            <ta e="T124" id="Seg_2126" s="T123">0.1.h:E</ta>
            <ta e="T126" id="Seg_2127" s="T125">pro.h:A</ta>
            <ta e="T127" id="Seg_2128" s="T126">np.h:A</ta>
            <ta e="T130" id="Seg_2129" s="T129">np:Th</ta>
            <ta e="T131" id="Seg_2130" s="T130">adv:Time</ta>
            <ta e="T132" id="Seg_2131" s="T131">np:A</ta>
            <ta e="T134" id="Seg_2132" s="T133">np.h:A</ta>
            <ta e="T137" id="Seg_2133" s="T136">pp:L</ta>
            <ta e="T139" id="Seg_2134" s="T138">np.h:Th</ta>
            <ta e="T144" id="Seg_2135" s="T143">0.3.h:Th</ta>
            <ta e="T147" id="Seg_2136" s="T146">np:Th</ta>
            <ta e="T151" id="Seg_2137" s="T150">0.3.h:A</ta>
            <ta e="T155" id="Seg_2138" s="T154">pro.h:P</ta>
            <ta e="T159" id="Seg_2139" s="T158">np.h:A</ta>
            <ta e="T160" id="Seg_2140" s="T159">np:G</ta>
            <ta e="T163" id="Seg_2141" s="T162">0.3.h:A</ta>
            <ta e="T164" id="Seg_2142" s="T163">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2143" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_2144" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_2145" s="T3">0.3.h:S v:pred</ta>
            <ta e="T5" id="Seg_2146" s="T4">np:O</ta>
            <ta e="T6" id="Seg_2147" s="T5">s:purp</ta>
            <ta e="T10" id="Seg_2148" s="T9">0.3.h:S v:pred</ta>
            <ta e="T12" id="Seg_2149" s="T11">pro.h:S</ta>
            <ta e="T15" id="Seg_2150" s="T14">v:pred</ta>
            <ta e="T20" id="Seg_2151" s="T19">np:O</ta>
            <ta e="T23" id="Seg_2152" s="T22">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T26" id="Seg_2153" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_2154" s="T26">s:adv</ta>
            <ta e="T28" id="Seg_2155" s="T27">v:pred</ta>
            <ta e="T31" id="Seg_2156" s="T30">0.3.h:S 0.3:O v:pred</ta>
            <ta e="T36" id="Seg_2157" s="T35">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_2158" s="T38">np:O</ta>
            <ta e="T41" id="Seg_2159" s="T39">s:rel</ta>
            <ta e="T43" id="Seg_2160" s="T41">s:adv</ta>
            <ta e="T46" id="Seg_2161" s="T45">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_2162" s="T46">pro.h:S</ta>
            <ta e="T49" id="Seg_2163" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_2164" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_2165" s="T52">pro.h:S</ta>
            <ta e="T55" id="Seg_2166" s="T54">v:pred</ta>
            <ta e="T58" id="Seg_2167" s="T55">s:compl</ta>
            <ta e="T61" id="Seg_2168" s="T60">0.3.h:S v:pred</ta>
            <ta e="T71" id="Seg_2169" s="T61">s:compl</ta>
            <ta e="T74" id="Seg_2170" s="T73">0.2.h:S v:pred</ta>
            <ta e="T76" id="Seg_2171" s="T75">s:adv</ta>
            <ta e="T77" id="Seg_2172" s="T76">0.2.h:S v:pred</ta>
            <ta e="T79" id="Seg_2173" s="T78">0.2.h:S v:pred</ta>
            <ta e="T84" id="Seg_2174" s="T83">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_2175" s="T84">np:O</ta>
            <ta e="T87" id="Seg_2176" s="T86">s:purp</ta>
            <ta e="T88" id="Seg_2177" s="T87">0.2.h:S v:pred</ta>
            <ta e="T91" id="Seg_2178" s="T90">pro.h:S</ta>
            <ta e="T93" id="Seg_2179" s="T92">0.3:O v:pred</ta>
            <ta e="T96" id="Seg_2180" s="T95">0.2.h:S 0.3:O v:pred</ta>
            <ta e="T97" id="Seg_2181" s="T96">pro.h:S</ta>
            <ta e="T101" id="Seg_2182" s="T100">v:pred</ta>
            <ta e="T102" id="Seg_2183" s="T101">v:pred</ta>
            <ta e="T103" id="Seg_2184" s="T102">np.h:S</ta>
            <ta e="T105" id="Seg_2185" s="T104">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_2186" s="T107">np:O</ta>
            <ta e="T109" id="Seg_2187" s="T108">np.h:S</ta>
            <ta e="T112" id="Seg_2188" s="T109">s:temp</ta>
            <ta e="T113" id="Seg_2189" s="T112">s:adv</ta>
            <ta e="T114" id="Seg_2190" s="T113">v:pred</ta>
            <ta e="T115" id="Seg_2191" s="T114">np.h:O</ta>
            <ta e="T124" id="Seg_2192" s="T123">0.1.h:S v:pred</ta>
            <ta e="T125" id="Seg_2193" s="T124">v:pred</ta>
            <ta e="T126" id="Seg_2194" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_2195" s="T126">np.h:S</ta>
            <ta e="T129" id="Seg_2196" s="T128">v:pred</ta>
            <ta e="T130" id="Seg_2197" s="T129">np:O</ta>
            <ta e="T132" id="Seg_2198" s="T131">np:S</ta>
            <ta e="T133" id="Seg_2199" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_2200" s="T133">np.h:S</ta>
            <ta e="T135" id="Seg_2201" s="T134">v:pred</ta>
            <ta e="T139" id="Seg_2202" s="T138">np.h:S</ta>
            <ta e="T141" id="Seg_2203" s="T140">v:pred</ta>
            <ta e="T144" id="Seg_2204" s="T143">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_2205" s="T144">s:temp</ta>
            <ta e="T151" id="Seg_2206" s="T150">0.3.h:S v:pred</ta>
            <ta e="T154" id="Seg_2207" s="T153">v:pred</ta>
            <ta e="T155" id="Seg_2208" s="T154">pro.h:S</ta>
            <ta e="T157" id="Seg_2209" s="T156">v:pred</ta>
            <ta e="T159" id="Seg_2210" s="T158">np.h:S</ta>
            <ta e="T161" id="Seg_2211" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_2212" s="T161">s:purp</ta>
            <ta e="T164" id="Seg_2213" s="T163">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_2214" s="T166">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_2215" s="T0">RUS:cult</ta>
            <ta e="T29" id="Seg_2216" s="T28">RUS:cult</ta>
            <ta e="T71" id="Seg_2217" s="T70">RUS:cult</ta>
            <ta e="T103" id="Seg_2218" s="T102">RUS:cult</ta>
            <ta e="T109" id="Seg_2219" s="T108">RUS:cult</ta>
            <ta e="T127" id="Seg_2220" s="T126">RUS:cult</ta>
            <ta e="T134" id="Seg_2221" s="T133">RUS:cult</ta>
            <ta e="T139" id="Seg_2222" s="T138">RUS:cult</ta>
            <ta e="T159" id="Seg_2223" s="T158">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_2224" s="T0">Петро помолился [перекрестился?].</ta>
            <ta e="T11" id="Seg_2225" s="T2">Потом поднял [палку?], чтобы ехать, [поднял] во второй раз и повернул обратно домой.</ta>
            <ta e="T23" id="Seg_2226" s="T11">Он в полночь нащупал голыми руками порванную (/сломанную) дверь, потом слегка постучал.</ta>
            <ta e="T28" id="Seg_2227" s="T23">"[Пока?], брат, я еду!"</ta>
            <ta e="T32" id="Seg_2228" s="T28">Минуту прислушался немного.</ta>
            <ta e="T41" id="Seg_2229" s="T32">Потом он опять начал щупать дверь, обитую сеном для утепления.</ta>
            <ta e="T49" id="Seg_2230" s="T41">Подёргав, он снова тихо сказал: "Я поехал".</ta>
            <ta e="T52" id="Seg_2231" s="T49">Потом он пошёл на улицу.</ta>
            <ta e="T58" id="Seg_2232" s="T52">Он не услышал, как скрипнула дверь. </ta>
            <ta e="T71" id="Seg_2233" s="T58">Потом он не видел, как его догонял босиком по снегу, догонял будто по траве Иван.</ta>
            <ta e="T74" id="Seg_2234" s="T71">"Как ты сказал?</ta>
            <ta e="T77" id="Seg_2235" s="T74">Едешь? </ta>
            <ta e="T79" id="Seg_2236" s="T77">Сперва подожди.</ta>
            <ta e="T85" id="Seg_2237" s="T79">В том доме найдём хлеб.</ta>
            <ta e="T88" id="Seg_2238" s="T85">В дороге проголодаешься.</ta>
            <ta e="T93" id="Seg_2239" s="T88">(Брату): "Брат, я не хочу. </ta>
            <ta e="T96" id="Seg_2240" s="T93">Своим людям оставь. </ta>
            <ta e="T103" id="Seg_2241" s="T96">Мы среди людей не умрём", — сказал Петро.</ta>
            <ta e="T108" id="Seg_2242" s="T103">Потом он взял в руки узловатые вожжи.</ta>
            <ta e="T120" id="Seg_2243" s="T108">Иван в темноте торопливо поцеловал брата несколько раз в щёку, в лоб, в нос.</ta>
            <ta e="T126" id="Seg_2244" s="T120">[Потом?] увидимся ли мы ещё", — сказал он.</ta>
            <ta e="T130" id="Seg_2245" s="T126">Иван толкнул (/толкает) нарты. </ta>
            <ta e="T133" id="Seg_2246" s="T130">После этого лошадь пошла.</ta>
            <ta e="T138" id="Seg_2247" s="T133">Петро шёл (/пошёл) рядом с нартами. </ta>
            <ta e="T150" id="Seg_2248" s="T138">Иван до тех пор стоял (/стоит), долго стоял, пока скрип нарт не затих.</ta>
            <ta e="T154" id="Seg_2249" s="T150">Он притушил [лампу], пока она не погасла.</ta>
            <ta e="T158" id="Seg_2250" s="T154">Она снова разгорелась (/он снова зажёг [её]).</ta>
            <ta e="T163" id="Seg_2251" s="T158">Иван зашёл в дом, чтобы [её] погасить.</ta>
            <ta e="T167" id="Seg_2252" s="T163">Он притушил [лампу], некоторое время она снова горела.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_2253" s="T0">Petro prayed [crossed himself?].</ta>
            <ta e="T11" id="Seg_2254" s="T2">Then he lifted [the pole] to ride off, [lifted] it for the second time and went back home.</ta>
            <ta e="T23" id="Seg_2255" s="T11">At midnight he searched with his bare hands for the torn (/broken) door, then he knocked slightly.</ta>
            <ta e="T28" id="Seg_2256" s="T23">"[So long?], brother, I'm leaving!"</ta>
            <ta e="T32" id="Seg_2257" s="T28">He was listening for a while.</ta>
            <ta e="T41" id="Seg_2258" s="T32">Then he started to touch the door padded with hay for warmth isolation.</ta>
            <ta e="T49" id="Seg_2259" s="T41">Having pulled it aside, he said quietly: "I'm leaving".</ta>
            <ta e="T52" id="Seg_2260" s="T49">Then he went out.</ta>
            <ta e="T58" id="Seg_2261" s="T52">He didn't hear how the door creaked.</ta>
            <ta e="T71" id="Seg_2262" s="T58">Then he didn't see how Ivan was following him barefoot through the snow, as if it was grass.</ta>
            <ta e="T74" id="Seg_2263" s="T71">"What did you say?</ta>
            <ta e="T77" id="Seg_2264" s="T74">You're leaving?</ta>
            <ta e="T79" id="Seg_2265" s="T77">First wait.</ta>
            <ta e="T85" id="Seg_2266" s="T79">In that house we will find some bread.</ta>
            <ta e="T88" id="Seg_2267" s="T85">You will get hungry on your way."</ta>
            <ta e="T93" id="Seg_2268" s="T88">(To the brother): "Brother, I don't want.</ta>
            <ta e="T96" id="Seg_2269" s="T93">Leave it for your people.</ta>
            <ta e="T103" id="Seg_2270" s="T96">We won't die among people", — Petro said.</ta>
            <ta e="T108" id="Seg_2271" s="T103">Then he took the knotty reins into his hands.</ta>
            <ta e="T120" id="Seg_2272" s="T108">In the darkness Ivan kissed his brother on his cheek, his forehead and his nose.</ta>
            <ta e="T126" id="Seg_2273" s="T120">"Will we see each other again [then?]", — he said.</ta>
            <ta e="T130" id="Seg_2274" s="T126">Ivan pushed (/pushes) the sledge.</ta>
            <ta e="T133" id="Seg_2275" s="T130">Then the horse moved off.</ta>
            <ta e="T138" id="Seg_2276" s="T133">Petro went beside the sledge.</ta>
            <ta e="T150" id="Seg_2277" s="T138">Ivan was standing there until the creak of the sledge faded away.</ta>
            <ta e="T154" id="Seg_2278" s="T150">He extinguished [the lamp] till it has gone out.</ta>
            <ta e="T158" id="Seg_2279" s="T154">It burned again (/he lit it up again).</ta>
            <ta e="T163" id="Seg_2280" s="T158">Ivan went into the house to blow it out.</ta>
            <ta e="T167" id="Seg_2281" s="T163">He extinguished [the lamp], it was again burning for a while.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_2282" s="T0">Petro betete [bekreuzigte sich?].</ta>
            <ta e="T11" id="Seg_2283" s="T2">Dann hob er [den Stock] um loszufahren, [hob] ihn zum zweiten Mal und fuhr zurück nach Hause.</ta>
            <ta e="T23" id="Seg_2284" s="T11">Um Mitternacht tastete er mit seinen nackten Händen nach der zerrissenen (/kaputten) Tür, dann klopfte er leicht.</ta>
            <ta e="T28" id="Seg_2285" s="T23">"[Tschüss?], Bruder, ich fahre los!"</ta>
            <ta e="T32" id="Seg_2286" s="T28">Er lauschte eine Weile.</ta>
            <ta e="T41" id="Seg_2287" s="T32">Dann fing er an, die zur Isolierung mit Heu gepolsterte Tür abzutasten.</ta>
            <ta e="T49" id="Seg_2288" s="T41">Er schob sie zur Seite und sagte leise: "Ich fahre los."</ta>
            <ta e="T52" id="Seg_2289" s="T49">Dann ging er nach draußen.</ta>
            <ta e="T58" id="Seg_2290" s="T52">Er hörte nicht, wie die Tür quietschte.</ta>
            <ta e="T71" id="Seg_2291" s="T58">Dann merkte er nicht, wie Ivan ihm barfuß im Schnee hinterher lief, als ob es Gras wäre.</ta>
            <ta e="T74" id="Seg_2292" s="T71">"Was hast du gesagt?</ta>
            <ta e="T77" id="Seg_2293" s="T74">Fährst du los?</ta>
            <ta e="T79" id="Seg_2294" s="T77">Warte erstmal.</ta>
            <ta e="T85" id="Seg_2295" s="T79">In dem Haus da finden wir Brot.</ta>
            <ta e="T88" id="Seg_2296" s="T85">Du wirst unterwegs hungrig werden."</ta>
            <ta e="T93" id="Seg_2297" s="T88">(Zum Bruder): "Bruder, ich will nicht.</ta>
            <ta e="T96" id="Seg_2298" s="T93">Lass es für deine Leute.</ta>
            <ta e="T103" id="Seg_2299" s="T96">Unter Menschen werden wir nicht sterben", — sagte Petro.</ta>
            <ta e="T108" id="Seg_2300" s="T103">Dann nahm er die verknoteten Zügel in die Hand.</ta>
            <ta e="T120" id="Seg_2301" s="T108">In der Dunkelheit küsste Ivan seinem Bruder auf Wange, Stirn und Nase.</ta>
            <ta e="T126" id="Seg_2302" s="T120">"Ob wir uns [dann?] wiedersehen", — sagte er.</ta>
            <ta e="T130" id="Seg_2303" s="T126">Ivan stieß (/stößt) den Schlitten.</ta>
            <ta e="T133" id="Seg_2304" s="T130">Dann lief das Pferd los.</ta>
            <ta e="T138" id="Seg_2305" s="T133">Petro ging neben dem Schlitten.</ta>
            <ta e="T150" id="Seg_2306" s="T138">Ivan stand so lange da, bis das Quietschen des Schlittens verklungen war.</ta>
            <ta e="T154" id="Seg_2307" s="T150">Er löschte [die Lampe] bis sie ausgegangen ist.</ta>
            <ta e="T158" id="Seg_2308" s="T154">Sie brannte wieder (/er hat sie wieder angezündet).</ta>
            <ta e="T163" id="Seg_2309" s="T158">Ivan ging ins Haus hinein, um sie auszumachen.</ta>
            <ta e="T167" id="Seg_2310" s="T163">Er löschte [die Lampe], sie brannte noch eine Weile.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_2311" s="T0">перекрестился</ta>
            <ta e="T11" id="Seg_2312" s="T2">уже поднял палочку, чтобы ехать снова (второй раз) обратно повернул домой</ta>
            <ta e="T23" id="Seg_2313" s="T11">он в темноте нащупал голыми руками порванный (в чуме) [сломанную] дверь помаленьку постучал</ta>
            <ta e="T28" id="Seg_2314" s="T23">пока брат, я еду!</ta>
            <ta e="T32" id="Seg_2315" s="T28">минутку прислушался немного</ta>
            <ta e="T41" id="Seg_2316" s="T32">опять снова пошарил(ся) в сене (соломе) дверь прибитый для утепления</ta>
            <ta e="T49" id="Seg_2317" s="T41">подергав кончив снова тихо сказал я поехал</ta>
            <ta e="T52" id="Seg_2318" s="T49">потом пошел на улицу</ta>
            <ta e="T58" id="Seg_2319" s="T52">он не услышал, как дверь скрипнула</ta>
            <ta e="T71" id="Seg_2320" s="T58">потом не видел как его догонял [за ним шел] голой ногой [босиком] по снегу гонял как по траве (сену) Иван</ta>
            <ta e="T74" id="Seg_2321" s="T71">ну что ты делаешь?</ta>
            <ta e="T77" id="Seg_2322" s="T74">уже едешь?</ta>
            <ta e="T79" id="Seg_2323" s="T77"> сперва подожди</ta>
            <ta e="T85" id="Seg_2324" s="T79">у соседа [в Ратте] найдем хлеба</ta>
            <ta e="T88" id="Seg_2325" s="T85">дорогой есть захочешь</ta>
            <ta e="T93" id="Seg_2326" s="T88">брату брат я не хочу </ta>
            <ta e="T96" id="Seg_2327" s="T93">своим людям оставь </ta>
            <ta e="T103" id="Seg_2328" s="T96">мы людей среди не умрем сказал П.</ta>
            <ta e="T108" id="Seg_2329" s="T103">потом взял в руки узловатые вожжи</ta>
            <ta e="T120" id="Seg_2330" s="T108">Иван может быть темно быстренько поцеловал брата несколько раз по щеке [в щеку] по лбу [в лоб] в нос</ta>
            <ta e="T126" id="Seg_2331" s="T120">потом может быть не увидим ли друг друга сказал он</ta>
            <ta e="T130" id="Seg_2332" s="T126">Иван толкнул (когда-то давно, мы не видели) [сейчас подтолкнет (мы видим)] санки </ta>
            <ta e="T133" id="Seg_2333" s="T130">лошадь пошла</ta>
            <ta e="T138" id="Seg_2334" s="T133">(когда-то шел) [(сейчас шел)]</ta>
            <ta e="T150" id="Seg_2335" s="T138">Иван столько [до тех пор] давно стоял [сейчас стоял, его видели] долго стоял когда нарты скрип не потерялся</ta>
            <ta e="T154" id="Seg_2336" s="T150">лампа еще не погасла</ta>
            <ta e="T158" id="Seg_2337" s="T154">снова разгорелась</ta>
            <ta e="T163" id="Seg_2338" s="T158">Иван снова зашел [домой] чтобы погасить</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T88" id="Seg_2339" s="T85">[OSV:] "amɨrqo ɛsɨqo" - "to get hungry".</ta>
            <ta e="T108" id="Seg_2340" s="T103">[OSV:] the adjectival form has been edited into "muqɨlsimilʼ".</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
