<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>KMS_1963_Hunting_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">KMS_1963_Hunting_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">633</ud-information>
            <ud-information attribute-name="# HIAT:w">510</ud-information>
            <ud-information attribute-name="# e">510</ud-information>
            <ud-information attribute-name="# HIAT:u">85</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KMS">
            <abbreviation>KMS</abbreviation>
            <sex value="m" />
            <languages-used>
               <language lang="sel" />
            </languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KMS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T511" id="Seg_0" n="sc" s="T1">
               <ts e="T24" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">šɨtə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">saːrum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">šɨttə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">qwäjgʼöt</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">čisloɣondə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">aktʼäbərin</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">ireɣəndo</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">okkə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">tɨsʼäča</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_30" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_32" n="HIAT:w" s="T10">okkɨr</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_36" n="HIAT:w" s="T11">tʼädʼigʼöt</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_39" n="HIAT:w" s="T12">ton</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_42" n="HIAT:w" s="T13">muqtut</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_45" n="HIAT:w" s="T14">saːrum</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_48" n="HIAT:w" s="T15">pottə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_50" n="HIAT:ip">(</nts>
                  <ts e="T17" id="Seg_52" n="HIAT:w" s="T16">poɣändə</ts>
                  <nts id="Seg_53" n="HIAT:ip">)</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">mat</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">qwassaŋ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">suːrulʼgu</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">nʼärɨntaːj</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_68" n="HIAT:w" s="T21">suːrulʼdi</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_71" n="HIAT:w" s="T22">qun</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_74" n="HIAT:w" s="T23">maːttə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_78" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">nimdə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_83" n="HIAT:w" s="T25">maːdʼin</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_86" n="HIAT:w" s="T26">čwäčin</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_89" n="HIAT:w" s="T27">Мitrij</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_92" n="HIAT:w" s="T28">Sidərɨn</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_95" n="HIAT:w" s="T29">maːtʼtʼe</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_99" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_101" n="HIAT:w" s="T30">nadə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">äːsan</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_107" n="HIAT:w" s="T32">äran</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_111" n="HIAT:w" s="T33">tʼäk</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_114" n="HIAT:w" s="T34">äːdi</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_117" n="HIAT:w" s="T35">ärat</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_121" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">man</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_126" n="HIAT:w" s="T37">wättow</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_129" n="HIAT:w" s="T38">ässan</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_132" n="HIAT:w" s="T39">soː</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_136" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">naŋo</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_141" n="HIAT:w" s="T41">soː</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_144" n="HIAT:w" s="T42">äːss</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_147" n="HIAT:w" s="T43">qanǯiwättə</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_150" n="HIAT:w" s="T44">sunʼdʼeːwɨn</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_153" n="HIAT:w" s="T45">nʼärɨn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_156" n="HIAT:w" s="T46">taj</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_159" n="HIAT:w" s="T47">qajewɨn</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_162" n="HIAT:w" s="T48">naːdəmnes</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_165" n="HIAT:w" s="T49">čaːǯigu</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_169" n="HIAT:w" s="T50">qandɨmbäːs</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_173" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_175" n="HIAT:w" s="T51">i</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_178" n="HIAT:w" s="T52">naːr</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_181" n="HIAT:w" s="T53">lʼäɣa</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_184" n="HIAT:w" s="T54">qwänbɨzattə</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_187" n="HIAT:w" s="T55">man</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_190" n="HIAT:w" s="T56">nʼärnän</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_194" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_196" n="HIAT:w" s="T57">täppɨlan</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_199" n="HIAT:w" s="T58">nimdə</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_202" n="HIAT:w" s="T59">Vasilij</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_206" n="HIAT:w" s="T60">Мaksim</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_210" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_212" n="HIAT:w" s="T61">Мaksimɨn</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_215" n="HIAT:w" s="T62">kojandə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_218" n="HIAT:w" s="T63">nimdə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_221" n="HIAT:w" s="T64">man</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_224" n="HIAT:w" s="T65">äːwɨlǯɨmbaw</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_228" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_230" n="HIAT:w" s="T66">tappɨla</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_233" n="HIAT:w" s="T67">qwandɨmbattə</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_236" n="HIAT:w" s="T68">tolʼdʼi</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_239" n="HIAT:w" s="T69">wättɨm</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_243" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">täppɨlan</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">tolʼdʼiwättə</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">qandämba</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_255" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">taw</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">dʼeːl</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">kulʼdʼiŋ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_266" n="HIAT:w" s="T76">kɨkkɨzaŋ</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_269" n="HIAT:w" s="T77">qweːdulugu</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_272" n="HIAT:w" s="T78">nʼäjakazä</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_276" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_278" n="HIAT:w" s="T79">assə</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_281" n="HIAT:w" s="T80">qweːduluaŋ</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_285" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_287" n="HIAT:w" s="T81">okkɨr</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_290" n="HIAT:w" s="T82">nʼäjam</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_293" n="HIAT:w" s="T83">kanaŋmo</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_297" n="HIAT:w" s="T84">kanalaw</ts>
                  <nts id="Seg_298" n="HIAT:ip">)</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_301" n="HIAT:w" s="T85">muːdəjbɨzɨt</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_305" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_307" n="HIAT:w" s="T86">man</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_310" n="HIAT:w" s="T87">manǯibɨzaw</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_313" n="HIAT:w" s="T88">tɨttoɣɨn</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_317" n="HIAT:w" s="T89">assa</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_320" n="HIAT:w" s="T90">qolʼdizaw</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_324" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_326" n="HIAT:w" s="T91">kanaŋmə</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_329" n="HIAT:w" s="T92">qoːɨt</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_332" n="HIAT:w" s="T93">šɨdəmdälǯi</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_335" n="HIAT:w" s="T94">nʼäja</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_338" n="HIAT:w" s="T95">kwäːrgoɣon</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_342" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_344" n="HIAT:w" s="T96">man</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_347" n="HIAT:w" s="T97">qolʼdʼäw</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_351" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_353" n="HIAT:w" s="T98">täbɨm</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">tʼätčigu</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_359" n="HIAT:w" s="T100">assə</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_362" n="HIAT:w" s="T101">äːsaŋ</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_366" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_368" n="HIAT:w" s="T102">naŋo</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_371" n="HIAT:w" s="T103">täbɨm</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_374" n="HIAT:w" s="T104">assə</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_377" n="HIAT:w" s="T105">qwässaw</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_381" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_383" n="HIAT:w" s="T106">täp</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_386" n="HIAT:w" s="T107">nʼäːrɣeːs</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_390" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_392" n="HIAT:w" s="T108">nʼärɣa</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_395" n="HIAT:w" s="T109">nʼäjam</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_398" n="HIAT:w" s="T110">assə</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_401" n="HIAT:w" s="T111">tʼäǯiqwot</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_405" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_407" n="HIAT:w" s="T112">assä</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_410" n="HIAT:w" s="T113">mittälʼe</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_413" n="HIAT:w" s="T114">čwäčin</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_416" n="HIAT:w" s="T115">nimdə</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_419" n="HIAT:w" s="T116">Мitrij</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_422" n="HIAT:w" s="T117">Sidərɨn</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_425" n="HIAT:w" s="T118">i</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_428" n="HIAT:w" s="T119">Lukanʼtʼin</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_431" n="HIAT:w" s="T120">lʼewi</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_434" n="HIAT:w" s="T121">maːttə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_437" n="HIAT:w" s="T122">panaluŋ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_440" n="HIAT:w" s="T123">qanǯow</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_444" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_446" n="HIAT:w" s="T124">na</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_449" n="HIAT:w" s="T125">qanǯow</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_452" n="HIAT:w" s="T126">mimbedi</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_455" n="HIAT:w" s="T127">i</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_458" n="HIAT:w" s="T128">qombädi</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_461" n="HIAT:w" s="T129">man</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_464" n="HIAT:w" s="T130">pajannä</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_468" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_470" n="HIAT:w" s="T131">wättə</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_473" n="HIAT:w" s="T132">ässan</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_476" n="HIAT:w" s="T133">aː</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_479" n="HIAT:w" s="T134">soː</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_483" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_485" n="HIAT:w" s="T135">qwändi</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_488" n="HIAT:w" s="T136">qulan</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_491" n="HIAT:w" s="T137">muqqoɣonna</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_494" n="HIAT:w" s="T138">qanǯiwättə</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_497" n="HIAT:w" s="T139">assə</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_500" n="HIAT:w" s="T140">qandɨmbɨs</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_504" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_506" n="HIAT:w" s="T141">üːdəmlʼe</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_509" n="HIAT:w" s="T142">oldɨŋ</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_513" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_515" n="HIAT:w" s="T143">qanǯow</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_518" n="HIAT:w" s="T144">panaluŋ</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_522" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">qailaw</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_527" n="HIAT:w" s="T146">i</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_530" n="HIAT:w" s="T147">panalumbädi</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_533" n="HIAT:w" s="T148">qanǯongaw</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_536" n="HIAT:w" s="T149">i</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_539" n="HIAT:w" s="T150">qailaw</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_542" n="HIAT:w" s="T151">moqqalʼe</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_545" n="HIAT:w" s="T152">qwändaw</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_548" n="HIAT:w" s="T153">nʼärɨn</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_551" n="HIAT:w" s="T154">tobi</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_554" n="HIAT:w" s="T155">tɨttɨ</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_557" n="HIAT:w" s="T156">mɨsɨndə</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_561" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_563" n="HIAT:w" s="T157">tɨttɨ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_566" n="HIAT:w" s="T158">mɨsɨɣan</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_569" n="HIAT:w" s="T159">pennaw</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_572" n="HIAT:w" s="T160">tüːm</ts>
                  <nts id="Seg_573" n="HIAT:ip">,</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_576" n="HIAT:w" s="T161">lakčenaw</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_579" n="HIAT:w" s="T162">tɨdɨn</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_582" n="HIAT:w" s="T163">molam</ts>
                  <nts id="Seg_583" n="HIAT:ip">,</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_586" n="HIAT:w" s="T164">meːjaw</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_589" n="HIAT:w" s="T165">oːmdɨssa</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_592" n="HIAT:w" s="T166">kučasoti</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_595" n="HIAT:w" s="T167">qoptəm</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_599" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_601" n="HIAT:w" s="T168">Madʼö</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_604" n="HIAT:w" s="T169">sagɨŋ</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_607" n="HIAT:w" s="T170">uttərɨŋ</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_611" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_613" n="HIAT:w" s="T171">meŋnan</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_616" n="HIAT:w" s="T172">okkɨ</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_619" n="HIAT:w" s="T173">täːrba</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_622" n="HIAT:w" s="T174">kundar</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_625" n="HIAT:w" s="T175">laːdiŋu</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_628" n="HIAT:w" s="T176">panalumbodi</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_631" n="HIAT:w" s="T177">qaːnǯim</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_635" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_637" n="HIAT:w" s="T178">meŋnan</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_640" n="HIAT:w" s="T179">äːsattə</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_643" n="HIAT:w" s="T180">nʼön</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_646" n="HIAT:w" s="T181">kɨssɨnɨla</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_650" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_652" n="HIAT:w" s="T182">šɨttə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_655" n="HIAT:w" s="T183">saːrum</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_658" n="HIAT:w" s="T184">šɨttə</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_661" n="HIAT:w" s="T185">qwäjgʼöt</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_664" n="HIAT:w" s="T186">šɨttə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_667" n="HIAT:w" s="T187">saːrum</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_670" n="HIAT:w" s="T188">naːr</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_673" n="HIAT:w" s="T189">qwäjgʼöt</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_676" n="HIAT:w" s="T190">pin</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_679" n="HIAT:w" s="T191">man</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_682" n="HIAT:w" s="T192">laqqɨzaŋ</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_686" n="HIAT:w" s="T193">sagɨŋ</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_689" n="HIAT:w" s="T194">praːiksaw</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_692" n="HIAT:w" s="T195">onäŋ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_695" n="HIAT:w" s="T196">qanǯaw</ts>
                  <nts id="Seg_696" n="HIAT:ip">.</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_699" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_701" n="HIAT:w" s="T197">mindɨn</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_704" n="HIAT:w" s="T198">taːdərɨgu</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_707" n="HIAT:w" s="T199">qailam</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_709" n="HIAT:ip">(</nts>
                  <ts e="T201" id="Seg_711" n="HIAT:w" s="T200">katomkoj</ts>
                  <nts id="Seg_712" n="HIAT:ip">)</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_715" n="HIAT:w" s="T201">meŋga</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_718" n="HIAT:w" s="T202">sätčim</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_721" n="HIAT:w" s="T203">änǯiŋ</ts>
                  <nts id="Seg_722" n="HIAT:ip">.</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_725" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_727" n="HIAT:w" s="T204">naŋo</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_730" n="HIAT:w" s="T205">pin</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_733" n="HIAT:w" s="T206">soːŋ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_736" n="HIAT:w" s="T207">assə</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_739" n="HIAT:w" s="T208">qondəzaŋ</ts>
                  <nts id="Seg_740" n="HIAT:ip">.</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_743" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_745" n="HIAT:w" s="T209">qanǯim</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_748" n="HIAT:w" s="T210">meːkuzaw</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_751" n="HIAT:w" s="T211">pin</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_754" n="HIAT:w" s="T212">tʼönǯin</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_757" n="HIAT:w" s="T213">ǯenno</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_761" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_763" n="HIAT:w" s="T214">okkɨrɨŋ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_766" n="HIAT:w" s="T215">pin</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_769" n="HIAT:w" s="T216">kanaŋmɨ</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_772" n="HIAT:w" s="T217">muːdɨjgu</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_775" n="HIAT:w" s="T218">äzuŋ</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_779" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_781" n="HIAT:w" s="T219">šɨdɨŋ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_784" n="HIAT:w" s="T220">narɨŋ</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_787" n="HIAT:w" s="T221">wäqsä</ts>
                  <nts id="Seg_788" n="HIAT:ip">.</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_791" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_793" n="HIAT:w" s="T222">man</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_796" n="HIAT:w" s="T223">assä</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_799" n="HIAT:w" s="T224">lärumbɨzaŋ</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_803" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_805" n="HIAT:w" s="T225">man</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_808" n="HIAT:w" s="T226">oneŋ</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_811" n="HIAT:w" s="T227">siwwə</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_814" n="HIAT:w" s="T228">oralbɨzaw</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_817" n="HIAT:w" s="T229">kak</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_820" n="HIAT:w" s="T230">kozjäin</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_823" n="HIAT:w" s="T231">onäŋ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_826" n="HIAT:w" s="T232">tüːɣonäŋ</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_830" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_832" n="HIAT:w" s="T233">man</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_835" n="HIAT:w" s="T234">manǯibɨsaw</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_838" n="HIAT:w" s="T235">tɨtti</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_841" n="HIAT:w" s="T236">makandə</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_844" n="HIAT:w" s="T237">warɣə</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_847" n="HIAT:w" s="T238">nʼärɨn</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_850" n="HIAT:w" s="T239">tobi</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_854" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_856" n="HIAT:w" s="T240">man</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_859" n="HIAT:w" s="T241">täːrbɨzaŋ</ts>
                  <nts id="Seg_860" n="HIAT:ip">:</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_863" n="HIAT:w" s="T242">qardʼe</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_866" n="HIAT:w" s="T243">nʼärnä</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_869" n="HIAT:w" s="T244">čaːčenǯaŋ</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_872" n="HIAT:w" s="T245">na</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_875" n="HIAT:w" s="T246">qanǯi</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_878" n="HIAT:w" s="T247">wättɨ</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_881" n="HIAT:w" s="T248">sünʼdʼewɨn</ts>
                  <nts id="Seg_882" n="HIAT:ip">.</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_885" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_887" n="HIAT:w" s="T249">sɨsaːrum</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_890" n="HIAT:w" s="T250">narqwäj</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_893" n="HIAT:w" s="T251">qarʼimɨɣɨn</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_896" n="HIAT:w" s="T252">kundə</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_899" n="HIAT:w" s="T253">assə</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_902" n="HIAT:w" s="T254">taqqɨlbɨzaŋ</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_906" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_908" n="HIAT:w" s="T255">čaːǯigu</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_911" n="HIAT:w" s="T256">soː</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_914" n="HIAT:w" s="T257">jes</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_917" n="HIAT:w" s="T258">äːrraj</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_920" n="HIAT:w" s="T259">qanǯi</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_923" n="HIAT:w" s="T260">wättoɨn</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_926" n="HIAT:w" s="T261">qaj</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_929" n="HIAT:w" s="T262">qandəmbɨs</ts>
                  <nts id="Seg_930" n="HIAT:ip">.</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_933" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_935" n="HIAT:w" s="T263">okkɨr</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_938" n="HIAT:w" s="T264">qwäjgʼöt</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_941" n="HIAT:w" s="T265">časoɣondə</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_944" n="HIAT:w" s="T266">tʼeːlɨn</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_947" n="HIAT:w" s="T267">man</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_950" n="HIAT:w" s="T268">äːsaŋ</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_953" n="HIAT:w" s="T269">naːr</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_956" n="HIAT:w" s="T270">suːrulʼdʼi</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_959" n="HIAT:w" s="T271">lʼäɣalɨnnan</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_963" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_965" n="HIAT:w" s="T272">täppɨla</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_968" n="HIAT:w" s="T273">nʼäjam</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_971" n="HIAT:w" s="T274">assä</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_974" n="HIAT:w" s="T275">qwatkuzattə</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_978" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_980" n="HIAT:w" s="T276">tüːlʼewlʼe</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_983" n="HIAT:w" s="T277">surulʼdi</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_986" n="HIAT:w" s="T278">maːttə</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_989" n="HIAT:w" s="T279">man</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_992" n="HIAT:w" s="T280">qwässaŋ</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_995" n="HIAT:w" s="T281">peːmɨgu</ts>
                  <nts id="Seg_996" n="HIAT:ip">.</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_999" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1001" n="HIAT:w" s="T282">na</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1004" n="HIAT:w" s="T283">dʼel</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1007" n="HIAT:w" s="T284">man</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1010" n="HIAT:w" s="T285">qwätnaw</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1013" n="HIAT:w" s="T286">okkə</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1016" n="HIAT:w" s="T287">nʼäjam</ts>
                  <nts id="Seg_1017" n="HIAT:ip">,</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1020" n="HIAT:w" s="T288">naːt</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1023" n="HIAT:w" s="T289">na</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1026" n="HIAT:w" s="T290">säːɣääs</ts>
                  <nts id="Seg_1027" n="HIAT:ip">,</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1030" n="HIAT:w" s="T291">i</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1033" n="HIAT:w" s="T292">okkə</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1036" n="HIAT:w" s="T293">saŋgɨm</ts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1040" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1042" n="HIAT:w" s="T294">äːsan</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1045" n="HIAT:w" s="T295">vaskrʼösnɨj</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1048" n="HIAT:w" s="T296">tʼeːlɨt</ts>
                  <nts id="Seg_1049" n="HIAT:ip">.</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1052" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1054" n="HIAT:w" s="T297">šɨdəmdälǯi</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1057" n="HIAT:w" s="T298">dʼel</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1061" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1063" n="HIAT:w" s="T299">peːmɨzaŋ</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1066" n="HIAT:w" s="T300">qwässaw</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1069" n="HIAT:w" s="T301">šɨttə</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1072" n="HIAT:w" s="T302">nʼäjam</ts>
                  <nts id="Seg_1073" n="HIAT:ip">.</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1076" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1078" n="HIAT:w" s="T303">täpkiː</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1081" n="HIAT:w" s="T304">äːsadi</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1084" n="HIAT:w" s="T305">siːnʼe</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1087" n="HIAT:w" s="T306">udɨ</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1090" n="HIAT:w" s="T307">säːɣə</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1093" n="HIAT:w" s="T308">qoːbʼi</ts>
                  <nts id="Seg_1094" n="HIAT:ip">.</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1097" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1099" n="HIAT:w" s="T309">siːtʼew</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1102" n="HIAT:w" s="T310">assä</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1105" n="HIAT:w" s="T311">aːndalba</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1109" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1111" n="HIAT:w" s="T312">naːrɨndʼelǯe</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1114" n="HIAT:w" s="T313">dʼel</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1117" n="HIAT:w" s="T314">äːsan</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1120" n="HIAT:w" s="T315">soː</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1123" n="HIAT:w" s="T316">tʼeːlɨt</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1127" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1129" n="HIAT:w" s="T317">na</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1132" n="HIAT:w" s="T318">dʼel</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1135" n="HIAT:w" s="T319">qaimnassä</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1138" n="HIAT:w" s="T320">qwässaw</ts>
                  <nts id="Seg_1139" n="HIAT:ip">.</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1142" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1144" n="HIAT:w" s="T321">tʼelɨt</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1147" n="HIAT:w" s="T322">äːsan</ts>
                  <nts id="Seg_1148" n="HIAT:ip">,</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1151" n="HIAT:w" s="T323">tʼeːlɨt</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1154" n="HIAT:w" s="T324">tʼeːlɨmbədi</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1157" n="HIAT:w" s="T325">man</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1160" n="HIAT:w" s="T326">pajannä</ts>
                  <nts id="Seg_1161" n="HIAT:ip">.</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1164" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1166" n="HIAT:w" s="T327">na</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1169" n="HIAT:w" s="T328">dʼel</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1172" n="HIAT:w" s="T329">lʼäɣalaze</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1175" n="HIAT:w" s="T330">okkə</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1178" n="HIAT:w" s="T331">mɨɣɨn</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1181" n="HIAT:w" s="T332">palʼdʼüzot</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1184" n="HIAT:w" s="T333">nʼärɨn</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1187" n="HIAT:w" s="T334">taj</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1190" n="HIAT:w" s="T335">pojkɨndə</ts>
                  <nts id="Seg_1191" n="HIAT:ip">.</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1194" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1196" n="HIAT:w" s="T336">nɨtʼän</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1199" n="HIAT:w" s="T337">me</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1202" n="HIAT:w" s="T338">qwädʼizot</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1205" n="HIAT:w" s="T339">pondə</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1208" n="HIAT:w" s="T340">nägärlʼe</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1211" n="HIAT:w" s="T341">oːnɨt</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1214" n="HIAT:w" s="T342">niwlawɨt</ts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1218" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1220" n="HIAT:w" s="T343">a</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1223" n="HIAT:w" s="T344">me</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1226" n="HIAT:w" s="T345">nimwɨt</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1229" n="HIAT:w" s="T346">Мadʼö</ts>
                  <nts id="Seg_1230" n="HIAT:ip">,</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1233" n="HIAT:w" s="T347">Мaksa</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1236" n="HIAT:w" s="T348">i</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1239" n="HIAT:w" s="T349">Вaska</ts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1243" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1245" n="HIAT:w" s="T350">taw</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1248" n="HIAT:w" s="T351">dʼel</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1251" n="HIAT:w" s="T352">me</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1254" n="HIAT:w" s="T353">tassot</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1257" n="HIAT:w" s="T354">naːgur</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1260" n="HIAT:w" s="T355">tawut</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1263" n="HIAT:w" s="T356">qumbädi</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1266" n="HIAT:w" s="T357">äsäw</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1269" n="HIAT:w" s="T358">Sämonɨm</ts>
                  <nts id="Seg_1270" n="HIAT:ip">.</nts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1273" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1275" n="HIAT:w" s="T359">meɣanɨt</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1278" n="HIAT:w" s="T360">äːsan</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1281" n="HIAT:w" s="T361">opsä</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1284" n="HIAT:w" s="T362">assə</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1287" n="HIAT:w" s="T363">soŋ</ts>
                  <nts id="Seg_1288" n="HIAT:ip">.</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1291" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1293" n="HIAT:w" s="T364">tättɨmdelǯi</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1296" n="HIAT:w" s="T365">dʼel</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1299" n="HIAT:w" s="T366">qarimɨɣɨn</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1302" n="HIAT:w" s="T367">püm</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1305" n="HIAT:w" s="T368">äːs</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1309" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1311" n="HIAT:w" s="T369">üːdəmɨn</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1314" n="HIAT:w" s="T370">äːsan</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1317" n="HIAT:w" s="T371">särrot</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1321" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1323" n="HIAT:w" s="T372">tʼeːlɨt</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1326" n="HIAT:w" s="T373">sättšim</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1329" n="HIAT:w" s="T374">äːss</ts>
                  <nts id="Seg_1330" n="HIAT:ip">.</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1333" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1335" n="HIAT:w" s="T375">palʼdʼügu</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1338" n="HIAT:w" s="T376">toppɨla</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1341" n="HIAT:w" s="T377">nuːnɨmbɨzattə</ts>
                  <nts id="Seg_1342" n="HIAT:ip">.</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1345" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1347" n="HIAT:w" s="T378">na</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1350" n="HIAT:w" s="T379">dʼel</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1353" n="HIAT:w" s="T380">tannaw</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1356" n="HIAT:w" s="T381">naːr</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1359" n="HIAT:w" s="T382">nʼäjam</ts>
                  <nts id="Seg_1360" n="HIAT:ip">.</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1363" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1365" n="HIAT:w" s="T383">sombɨlʼemdälǯi</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1368" n="HIAT:w" s="T384">dʼel</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1371" n="HIAT:w" s="T385">okkɨr</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1374" n="HIAT:w" s="T386">lʼäɣa</ts>
                  <nts id="Seg_1375" n="HIAT:ip">,</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1378" n="HIAT:w" s="T387">nimdə</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1381" n="HIAT:w" s="T388">täbɨn</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1384" n="HIAT:w" s="T389">Wasilʼij</ts>
                  <nts id="Seg_1385" n="HIAT:ip">,</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1388" n="HIAT:w" s="T390">qwässɨ</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1391" n="HIAT:w" s="T391">maːtqɨndə</ts>
                  <nts id="Seg_1392" n="HIAT:ip">.</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1395" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1397" n="HIAT:w" s="T392">naː</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1400" n="HIAT:w" s="T393">dʼel</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1403" n="HIAT:w" s="T394">tassaw</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1406" n="HIAT:w" s="T395">tättə</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1409" n="HIAT:w" s="T396">nʼäjam</ts>
                  <nts id="Seg_1410" n="HIAT:ip">.</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1413" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1415" n="HIAT:w" s="T397">muqtumdälǯi</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1418" n="HIAT:w" s="T398">dʼel</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1421" n="HIAT:w" s="T399">palʼdʼüzaŋ</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1424" n="HIAT:w" s="T400">pʼüru</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1427" n="HIAT:w" s="T401">nʼäran</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1430" n="HIAT:w" s="T402">taj</ts>
                  <nts id="Seg_1431" n="HIAT:ip">.</nts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1434" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1436" n="HIAT:w" s="T403">äːrukkaŋ</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1439" n="HIAT:w" s="T404">čaːǯizaŋ</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1442" n="HIAT:w" s="T405">naŋo</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1445" n="HIAT:w" s="T406">üŋulʼǯumbulʼe</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1448" n="HIAT:w" s="T407">qaːɣə</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1451" n="HIAT:w" s="T408">muːtolʼdʼenǯiŋ</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1454" n="HIAT:w" s="T409">man</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1457" n="HIAT:w" s="T410">kanaŋmə</ts>
                  <nts id="Seg_1458" n="HIAT:ip">.</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1461" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1463" n="HIAT:w" s="T411">nimdə</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1466" n="HIAT:w" s="T412">täbɨn</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1469" n="HIAT:w" s="T413">Äːdu</ts>
                  <nts id="Seg_1470" n="HIAT:ip">.</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1473" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1475" n="HIAT:w" s="T414">ündədʼäw</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1477" n="HIAT:ip">(</nts>
                  <ts e="T416" id="Seg_1479" n="HIAT:w" s="T415">ündɨzäjaw</ts>
                  <nts id="Seg_1480" n="HIAT:ip">)</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1483" n="HIAT:w" s="T416">kanaŋmə</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1486" n="HIAT:w" s="T417">muːdɨŋ</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1489" n="HIAT:w" s="T418">nʼäjandə</ts>
                  <nts id="Seg_1490" n="HIAT:ip">.</nts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1493" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1495" n="HIAT:w" s="T419">mitänǯigu</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1498" n="HIAT:w" s="T420">äːduni</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1502" n="HIAT:w" s="T421">täp</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1505" n="HIAT:w" s="T422">soːŋ</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1508" n="HIAT:w" s="T423">muːdɨŋ</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1511" n="HIAT:w" s="T424">warɣə</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1514" n="HIAT:w" s="T425">pirəga</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1517" n="HIAT:w" s="T426">tɨttonda</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1521" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1523" n="HIAT:w" s="T427">manǯimbaŋ</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1526" n="HIAT:w" s="T428">innä</ts>
                  <nts id="Seg_1527" n="HIAT:ip">,</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1530" n="HIAT:w" s="T429">assə</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1533" n="HIAT:w" s="T430">qolʼdikwaw</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1537" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1539" n="HIAT:w" s="T431">nʼäjam</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1542" n="HIAT:w" s="T432">lärčigu</ts>
                  <nts id="Seg_1543" n="HIAT:ip">,</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1546" n="HIAT:w" s="T433">qonɨŋ</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1549" n="HIAT:w" s="T434">tʼätʼčizaŋ</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1552" n="HIAT:w" s="T435">toːzopkazä</ts>
                  <nts id="Seg_1553" n="HIAT:ip">,</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1556" n="HIAT:w" s="T436">a</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1559" n="HIAT:w" s="T437">nännä</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1562" n="HIAT:w" s="T438">assə</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1565" n="HIAT:w" s="T439">okkɨrɨŋ</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1568" n="HIAT:w" s="T440">tʼätʼtšizaŋ</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1571" n="HIAT:w" s="T441">šɨttə</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1574" n="HIAT:w" s="T442">puːǯi</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1577" n="HIAT:w" s="T443">tüːləsäzä</ts>
                  <nts id="Seg_1578" n="HIAT:ip">.</nts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1581" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1583" n="HIAT:w" s="T444">kɨkkɨzaŋ</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1586" n="HIAT:w" s="T445">man</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1589" n="HIAT:w" s="T446">täbɨm</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1592" n="HIAT:w" s="T447">qoːlʼdigu</ts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1596" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1598" n="HIAT:w" s="T448">man</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1601" n="HIAT:w" s="T449">täbɨm</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1604" n="HIAT:w" s="T450">qolʼdʼäw</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1607" n="HIAT:w" s="T451">tɨttən</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1610" n="HIAT:w" s="T452">oloɣɨn</ts>
                  <nts id="Seg_1611" n="HIAT:ip">.</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1614" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1616" n="HIAT:w" s="T453">assə</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1619" n="HIAT:w" s="T454">naːrɨŋ</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1622" n="HIAT:w" s="T455">tättɨŋ</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1625" n="HIAT:w" s="T456">tɨttɨn</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1628" n="HIAT:w" s="T457">olondə</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1631" n="HIAT:w" s="T458">tʼättšikuzaŋ</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1634" n="HIAT:w" s="T459">i</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1637" n="HIAT:w" s="T460">qwäːtʼäw</ts>
                  <nts id="Seg_1638" n="HIAT:ip">.</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1641" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1643" n="HIAT:w" s="T461">nʼegətšeːjaŋ</ts>
                  <nts id="Seg_1644" n="HIAT:ip">,</nts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1647" n="HIAT:w" s="T462">nɨŋejaŋ</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1651" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1653" n="HIAT:w" s="T463">assə</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1656" n="HIAT:w" s="T464">kɨkkɨzaŋ</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1659" n="HIAT:w" s="T465">qwädʼigu</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1662" n="HIAT:w" s="T466">nʼäjam</ts>
                  <nts id="Seg_1663" n="HIAT:ip">.</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1666" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1668" n="HIAT:w" s="T467">manǯimbaŋ</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1671" n="HIAT:w" s="T468">man</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1674" n="HIAT:w" s="T469">matʼtʼöɣi</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1677" n="HIAT:w" s="T470">lʼäɣaw</ts>
                  <nts id="Seg_1678" n="HIAT:ip">,</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1681" n="HIAT:w" s="T471">nimdə</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1684" n="HIAT:w" s="T472">täbɨn</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1687" n="HIAT:w" s="T473">äːdu</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1691" n="HIAT:w" s="T474">qaimdaka</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1694" n="HIAT:w" s="T475">meːkut</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1697" n="HIAT:w" s="T476">tʼäǯikutäj</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1700" n="HIAT:w" s="T477">tɨːdɨn</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1703" n="HIAT:w" s="T478">taːbaɣɨn</ts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1707" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1709" n="HIAT:w" s="T479">man</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1712" n="HIAT:w" s="T480">tolʼdʼin</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1715" n="HIAT:w" s="T481">nʼöɣɨnnaŋ</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1718" n="HIAT:w" s="T482">täbɨnni</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1722" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_1724" n="HIAT:w" s="T483">täp</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1727" n="HIAT:w" s="T484">pɨŋgəlɨmbɨdi</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1730" n="HIAT:w" s="T485">nʼäjam</ts>
                  <nts id="Seg_1731" n="HIAT:ip">,</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1734" n="HIAT:w" s="T486">kunnɨ</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1737" n="HIAT:w" s="T487">man</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1740" n="HIAT:w" s="T488">nɨŋɨzaŋ</ts>
                  <nts id="Seg_1741" n="HIAT:ip">,</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1744" n="HIAT:w" s="T489">nʼegɨtšizaŋ</ts>
                  <nts id="Seg_1745" n="HIAT:ip">,</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1748" n="HIAT:w" s="T490">täp</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1751" n="HIAT:w" s="T491">ambat</ts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1755" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1757" n="HIAT:w" s="T492">qwädimbat</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1760" n="HIAT:w" s="T493">meŋa</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1763" n="HIAT:w" s="T494">moqqoɣɨ</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1766" n="HIAT:w" s="T495">tobɨkalamdə</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1768" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_1770" n="HIAT:w" s="T496">ɨ</ts>
                  <nts id="Seg_1771" n="HIAT:ip">)</nts>
                  <nts id="Seg_1772" n="HIAT:ip">,</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1775" n="HIAT:w" s="T497">pušqaj</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1778" n="HIAT:w" s="T498">talʼdʼüm</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1782" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_1784" n="HIAT:w" s="T499">me</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1787" n="HIAT:w" s="T500">täpsä</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1790" n="HIAT:w" s="T501">qwäduzoː</ts>
                  <nts id="Seg_1791" n="HIAT:ip">,</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1794" n="HIAT:w" s="T502">tɨttäruzoː</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1797" n="HIAT:w" s="T503">aj</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1800" n="HIAT:w" s="T504">miriuzo</ts>
                  <nts id="Seg_1801" n="HIAT:ip">.</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1804" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1806" n="HIAT:w" s="T505">na</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1809" n="HIAT:w" s="T506">dʼel</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1812" n="HIAT:w" s="T507">me</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1815" n="HIAT:w" s="T508">tasso</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1818" n="HIAT:w" s="T509">šɨttə</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1821" n="HIAT:w" s="T510">nʼäjam</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T511" id="Seg_1824" n="sc" s="T1">
               <ts e="T2" id="Seg_1826" n="e" s="T1">šɨtə </ts>
               <ts e="T3" id="Seg_1828" n="e" s="T2">saːrum </ts>
               <ts e="T4" id="Seg_1830" n="e" s="T3">šɨttə </ts>
               <ts e="T5" id="Seg_1832" n="e" s="T4">qwäjgʼöt </ts>
               <ts e="T6" id="Seg_1834" n="e" s="T5">čisloɣondə </ts>
               <ts e="T7" id="Seg_1836" n="e" s="T6">aktʼäbərin </ts>
               <ts e="T8" id="Seg_1838" n="e" s="T7">ireɣəndo </ts>
               <ts e="T9" id="Seg_1840" n="e" s="T8">okkə </ts>
               <ts e="T10" id="Seg_1842" n="e" s="T9">tɨsʼäča </ts>
               <ts e="T11" id="Seg_1844" n="e" s="T10">(okkɨr) </ts>
               <ts e="T12" id="Seg_1846" n="e" s="T11">tʼädʼigʼöt </ts>
               <ts e="T13" id="Seg_1848" n="e" s="T12">ton </ts>
               <ts e="T14" id="Seg_1850" n="e" s="T13">muqtut </ts>
               <ts e="T15" id="Seg_1852" n="e" s="T14">saːrum </ts>
               <ts e="T16" id="Seg_1854" n="e" s="T15">pottə </ts>
               <ts e="T17" id="Seg_1856" n="e" s="T16">(poɣändə) </ts>
               <ts e="T18" id="Seg_1858" n="e" s="T17">mat </ts>
               <ts e="T19" id="Seg_1860" n="e" s="T18">qwassaŋ </ts>
               <ts e="T20" id="Seg_1862" n="e" s="T19">suːrulʼgu </ts>
               <ts e="T21" id="Seg_1864" n="e" s="T20">nʼärɨntaːj </ts>
               <ts e="T22" id="Seg_1866" n="e" s="T21">suːrulʼdi </ts>
               <ts e="T23" id="Seg_1868" n="e" s="T22">qun </ts>
               <ts e="T24" id="Seg_1870" n="e" s="T23">maːttə. </ts>
               <ts e="T25" id="Seg_1872" n="e" s="T24">nimdə </ts>
               <ts e="T26" id="Seg_1874" n="e" s="T25">maːdʼin </ts>
               <ts e="T27" id="Seg_1876" n="e" s="T26">čwäčin </ts>
               <ts e="T28" id="Seg_1878" n="e" s="T27">Мitrij </ts>
               <ts e="T29" id="Seg_1880" n="e" s="T28">Sidərɨn </ts>
               <ts e="T30" id="Seg_1882" n="e" s="T29">maːtʼtʼe. </ts>
               <ts e="T31" id="Seg_1884" n="e" s="T30">nadə </ts>
               <ts e="T32" id="Seg_1886" n="e" s="T31">äːsan </ts>
               <ts e="T33" id="Seg_1888" n="e" s="T32">äran, </ts>
               <ts e="T34" id="Seg_1890" n="e" s="T33">tʼäk </ts>
               <ts e="T35" id="Seg_1892" n="e" s="T34">äːdi </ts>
               <ts e="T36" id="Seg_1894" n="e" s="T35">ärat. </ts>
               <ts e="T37" id="Seg_1896" n="e" s="T36">man </ts>
               <ts e="T38" id="Seg_1898" n="e" s="T37">wättow </ts>
               <ts e="T39" id="Seg_1900" n="e" s="T38">ässan </ts>
               <ts e="T40" id="Seg_1902" n="e" s="T39">soː. </ts>
               <ts e="T41" id="Seg_1904" n="e" s="T40">naŋo </ts>
               <ts e="T42" id="Seg_1906" n="e" s="T41">soː </ts>
               <ts e="T43" id="Seg_1908" n="e" s="T42">äːss </ts>
               <ts e="T44" id="Seg_1910" n="e" s="T43">qanǯiwättə </ts>
               <ts e="T45" id="Seg_1912" n="e" s="T44">sunʼdʼeːwɨn </ts>
               <ts e="T46" id="Seg_1914" n="e" s="T45">nʼärɨn </ts>
               <ts e="T47" id="Seg_1916" n="e" s="T46">taj </ts>
               <ts e="T48" id="Seg_1918" n="e" s="T47">qajewɨn </ts>
               <ts e="T49" id="Seg_1920" n="e" s="T48">naːdəmnes </ts>
               <ts e="T50" id="Seg_1922" n="e" s="T49">čaːǯigu, </ts>
               <ts e="T51" id="Seg_1924" n="e" s="T50">qandɨmbäːs. </ts>
               <ts e="T52" id="Seg_1926" n="e" s="T51">i </ts>
               <ts e="T53" id="Seg_1928" n="e" s="T52">naːr </ts>
               <ts e="T54" id="Seg_1930" n="e" s="T53">lʼäɣa </ts>
               <ts e="T55" id="Seg_1932" n="e" s="T54">qwänbɨzattə </ts>
               <ts e="T56" id="Seg_1934" n="e" s="T55">man </ts>
               <ts e="T57" id="Seg_1936" n="e" s="T56">nʼärnän. </ts>
               <ts e="T58" id="Seg_1938" n="e" s="T57">täppɨlan </ts>
               <ts e="T59" id="Seg_1940" n="e" s="T58">nimdə </ts>
               <ts e="T60" id="Seg_1942" n="e" s="T59">Vasilij, </ts>
               <ts e="T61" id="Seg_1944" n="e" s="T60">Мaksim. </ts>
               <ts e="T62" id="Seg_1946" n="e" s="T61">Мaksimɨn </ts>
               <ts e="T63" id="Seg_1948" n="e" s="T62">kojandə </ts>
               <ts e="T64" id="Seg_1950" n="e" s="T63">nimdə </ts>
               <ts e="T65" id="Seg_1952" n="e" s="T64">man </ts>
               <ts e="T66" id="Seg_1954" n="e" s="T65">äːwɨlǯɨmbaw. </ts>
               <ts e="T67" id="Seg_1956" n="e" s="T66">tappɨla </ts>
               <ts e="T68" id="Seg_1958" n="e" s="T67">qwandɨmbattə </ts>
               <ts e="T69" id="Seg_1960" n="e" s="T68">tolʼdʼi </ts>
               <ts e="T70" id="Seg_1962" n="e" s="T69">wättɨm. </ts>
               <ts e="T71" id="Seg_1964" n="e" s="T70">täppɨlan </ts>
               <ts e="T72" id="Seg_1966" n="e" s="T71">tolʼdʼiwättə </ts>
               <ts e="T73" id="Seg_1968" n="e" s="T72">qandämba. </ts>
               <ts e="T74" id="Seg_1970" n="e" s="T73">taw </ts>
               <ts e="T75" id="Seg_1972" n="e" s="T74">dʼeːl </ts>
               <ts e="T76" id="Seg_1974" n="e" s="T75">kulʼdʼiŋ </ts>
               <ts e="T77" id="Seg_1976" n="e" s="T76">kɨkkɨzaŋ </ts>
               <ts e="T78" id="Seg_1978" n="e" s="T77">qweːdulugu </ts>
               <ts e="T79" id="Seg_1980" n="e" s="T78">nʼäjakazä. </ts>
               <ts e="T80" id="Seg_1982" n="e" s="T79">assə </ts>
               <ts e="T81" id="Seg_1984" n="e" s="T80">qweːduluaŋ. </ts>
               <ts e="T82" id="Seg_1986" n="e" s="T81">okkɨr </ts>
               <ts e="T83" id="Seg_1988" n="e" s="T82">nʼäjam </ts>
               <ts e="T84" id="Seg_1990" n="e" s="T83">kanaŋmo </ts>
               <ts e="T85" id="Seg_1992" n="e" s="T84">(kanalaw) </ts>
               <ts e="T86" id="Seg_1994" n="e" s="T85">muːdəjbɨzɨt. </ts>
               <ts e="T87" id="Seg_1996" n="e" s="T86">man </ts>
               <ts e="T88" id="Seg_1998" n="e" s="T87">manǯibɨzaw </ts>
               <ts e="T89" id="Seg_2000" n="e" s="T88">tɨttoɣɨn, </ts>
               <ts e="T90" id="Seg_2002" n="e" s="T89">assa </ts>
               <ts e="T91" id="Seg_2004" n="e" s="T90">qolʼdizaw. </ts>
               <ts e="T92" id="Seg_2006" n="e" s="T91">kanaŋmə </ts>
               <ts e="T93" id="Seg_2008" n="e" s="T92">qoːɨt </ts>
               <ts e="T94" id="Seg_2010" n="e" s="T93">šɨdəmdälǯi </ts>
               <ts e="T95" id="Seg_2012" n="e" s="T94">nʼäja </ts>
               <ts e="T96" id="Seg_2014" n="e" s="T95">kwäːrgoɣon. </ts>
               <ts e="T97" id="Seg_2016" n="e" s="T96">man </ts>
               <ts e="T98" id="Seg_2018" n="e" s="T97">qolʼdʼäw. </ts>
               <ts e="T99" id="Seg_2020" n="e" s="T98">täbɨm </ts>
               <ts e="T100" id="Seg_2022" n="e" s="T99">tʼätčigu </ts>
               <ts e="T101" id="Seg_2024" n="e" s="T100">assə </ts>
               <ts e="T102" id="Seg_2026" n="e" s="T101">äːsaŋ. </ts>
               <ts e="T103" id="Seg_2028" n="e" s="T102">naŋo </ts>
               <ts e="T104" id="Seg_2030" n="e" s="T103">täbɨm </ts>
               <ts e="T105" id="Seg_2032" n="e" s="T104">assə </ts>
               <ts e="T106" id="Seg_2034" n="e" s="T105">qwässaw. </ts>
               <ts e="T107" id="Seg_2036" n="e" s="T106">täp </ts>
               <ts e="T108" id="Seg_2038" n="e" s="T107">nʼäːrɣeːs. </ts>
               <ts e="T109" id="Seg_2040" n="e" s="T108">nʼärɣa </ts>
               <ts e="T110" id="Seg_2042" n="e" s="T109">nʼäjam </ts>
               <ts e="T111" id="Seg_2044" n="e" s="T110">assə </ts>
               <ts e="T112" id="Seg_2046" n="e" s="T111">tʼäǯiqwot. </ts>
               <ts e="T113" id="Seg_2048" n="e" s="T112">assä </ts>
               <ts e="T114" id="Seg_2050" n="e" s="T113">mittälʼe </ts>
               <ts e="T115" id="Seg_2052" n="e" s="T114">čwäčin </ts>
               <ts e="T116" id="Seg_2054" n="e" s="T115">nimdə </ts>
               <ts e="T117" id="Seg_2056" n="e" s="T116">Мitrij </ts>
               <ts e="T118" id="Seg_2058" n="e" s="T117">Sidərɨn </ts>
               <ts e="T119" id="Seg_2060" n="e" s="T118">i </ts>
               <ts e="T120" id="Seg_2062" n="e" s="T119">Lukanʼtʼin </ts>
               <ts e="T121" id="Seg_2064" n="e" s="T120">lʼewi </ts>
               <ts e="T122" id="Seg_2066" n="e" s="T121">maːttə </ts>
               <ts e="T123" id="Seg_2068" n="e" s="T122">panaluŋ </ts>
               <ts e="T124" id="Seg_2070" n="e" s="T123">qanǯow. </ts>
               <ts e="T125" id="Seg_2072" n="e" s="T124">na </ts>
               <ts e="T126" id="Seg_2074" n="e" s="T125">qanǯow </ts>
               <ts e="T127" id="Seg_2076" n="e" s="T126">mimbedi </ts>
               <ts e="T128" id="Seg_2078" n="e" s="T127">i </ts>
               <ts e="T129" id="Seg_2080" n="e" s="T128">qombädi </ts>
               <ts e="T130" id="Seg_2082" n="e" s="T129">man </ts>
               <ts e="T131" id="Seg_2084" n="e" s="T130">pajannä. </ts>
               <ts e="T132" id="Seg_2086" n="e" s="T131">wättə </ts>
               <ts e="T133" id="Seg_2088" n="e" s="T132">ässan </ts>
               <ts e="T134" id="Seg_2090" n="e" s="T133">aː </ts>
               <ts e="T135" id="Seg_2092" n="e" s="T134">soː. </ts>
               <ts e="T136" id="Seg_2094" n="e" s="T135">qwändi </ts>
               <ts e="T137" id="Seg_2096" n="e" s="T136">qulan </ts>
               <ts e="T138" id="Seg_2098" n="e" s="T137">muqqoɣonna </ts>
               <ts e="T139" id="Seg_2100" n="e" s="T138">qanǯiwättə </ts>
               <ts e="T140" id="Seg_2102" n="e" s="T139">assə </ts>
               <ts e="T141" id="Seg_2104" n="e" s="T140">qandɨmbɨs. </ts>
               <ts e="T142" id="Seg_2106" n="e" s="T141">üːdəmlʼe </ts>
               <ts e="T143" id="Seg_2108" n="e" s="T142">oldɨŋ. </ts>
               <ts e="T144" id="Seg_2110" n="e" s="T143">qanǯow </ts>
               <ts e="T145" id="Seg_2112" n="e" s="T144">panaluŋ. </ts>
               <ts e="T146" id="Seg_2114" n="e" s="T145">qailaw </ts>
               <ts e="T147" id="Seg_2116" n="e" s="T146">i </ts>
               <ts e="T148" id="Seg_2118" n="e" s="T147">panalumbädi </ts>
               <ts e="T149" id="Seg_2120" n="e" s="T148">qanǯongaw </ts>
               <ts e="T150" id="Seg_2122" n="e" s="T149">i </ts>
               <ts e="T151" id="Seg_2124" n="e" s="T150">qailaw </ts>
               <ts e="T152" id="Seg_2126" n="e" s="T151">moqqalʼe </ts>
               <ts e="T153" id="Seg_2128" n="e" s="T152">qwändaw </ts>
               <ts e="T154" id="Seg_2130" n="e" s="T153">nʼärɨn </ts>
               <ts e="T155" id="Seg_2132" n="e" s="T154">tobi </ts>
               <ts e="T156" id="Seg_2134" n="e" s="T155">tɨttɨ </ts>
               <ts e="T157" id="Seg_2136" n="e" s="T156">mɨsɨndə. </ts>
               <ts e="T158" id="Seg_2138" n="e" s="T157">tɨttɨ </ts>
               <ts e="T159" id="Seg_2140" n="e" s="T158">mɨsɨɣan </ts>
               <ts e="T160" id="Seg_2142" n="e" s="T159">pennaw </ts>
               <ts e="T161" id="Seg_2144" n="e" s="T160">tüːm, </ts>
               <ts e="T162" id="Seg_2146" n="e" s="T161">lakčenaw </ts>
               <ts e="T163" id="Seg_2148" n="e" s="T162">tɨdɨn </ts>
               <ts e="T164" id="Seg_2150" n="e" s="T163">molam, </ts>
               <ts e="T165" id="Seg_2152" n="e" s="T164">meːjaw </ts>
               <ts e="T166" id="Seg_2154" n="e" s="T165">oːmdɨssa </ts>
               <ts e="T167" id="Seg_2156" n="e" s="T166">kučasoti </ts>
               <ts e="T168" id="Seg_2158" n="e" s="T167">qoptəm. </ts>
               <ts e="T169" id="Seg_2160" n="e" s="T168">Madʼö </ts>
               <ts e="T170" id="Seg_2162" n="e" s="T169">sagɨŋ </ts>
               <ts e="T171" id="Seg_2164" n="e" s="T170">uttərɨŋ. </ts>
               <ts e="T172" id="Seg_2166" n="e" s="T171">meŋnan </ts>
               <ts e="T173" id="Seg_2168" n="e" s="T172">okkɨ </ts>
               <ts e="T174" id="Seg_2170" n="e" s="T173">täːrba </ts>
               <ts e="T175" id="Seg_2172" n="e" s="T174">kundar </ts>
               <ts e="T176" id="Seg_2174" n="e" s="T175">laːdiŋu </ts>
               <ts e="T177" id="Seg_2176" n="e" s="T176">panalumbodi </ts>
               <ts e="T178" id="Seg_2178" n="e" s="T177">qaːnǯim. </ts>
               <ts e="T179" id="Seg_2180" n="e" s="T178">meŋnan </ts>
               <ts e="T180" id="Seg_2182" n="e" s="T179">äːsattə </ts>
               <ts e="T181" id="Seg_2184" n="e" s="T180">nʼön </ts>
               <ts e="T182" id="Seg_2186" n="e" s="T181">kɨssɨnɨla. </ts>
               <ts e="T183" id="Seg_2188" n="e" s="T182">šɨttə </ts>
               <ts e="T184" id="Seg_2190" n="e" s="T183">saːrum </ts>
               <ts e="T185" id="Seg_2192" n="e" s="T184">šɨttə </ts>
               <ts e="T186" id="Seg_2194" n="e" s="T185">qwäjgʼöt </ts>
               <ts e="T187" id="Seg_2196" n="e" s="T186">šɨttə </ts>
               <ts e="T188" id="Seg_2198" n="e" s="T187">saːrum </ts>
               <ts e="T189" id="Seg_2200" n="e" s="T188">naːr </ts>
               <ts e="T190" id="Seg_2202" n="e" s="T189">qwäjgʼöt </ts>
               <ts e="T191" id="Seg_2204" n="e" s="T190">pin </ts>
               <ts e="T192" id="Seg_2206" n="e" s="T191">man </ts>
               <ts e="T193" id="Seg_2208" n="e" s="T192">laqqɨzaŋ, </ts>
               <ts e="T194" id="Seg_2210" n="e" s="T193">sagɨŋ </ts>
               <ts e="T195" id="Seg_2212" n="e" s="T194">praːiksaw </ts>
               <ts e="T196" id="Seg_2214" n="e" s="T195">onäŋ </ts>
               <ts e="T197" id="Seg_2216" n="e" s="T196">qanǯaw. </ts>
               <ts e="T198" id="Seg_2218" n="e" s="T197">mindɨn </ts>
               <ts e="T199" id="Seg_2220" n="e" s="T198">taːdərɨgu </ts>
               <ts e="T200" id="Seg_2222" n="e" s="T199">qailam </ts>
               <ts e="T201" id="Seg_2224" n="e" s="T200">(katomkoj) </ts>
               <ts e="T202" id="Seg_2226" n="e" s="T201">meŋga </ts>
               <ts e="T203" id="Seg_2228" n="e" s="T202">sätčim </ts>
               <ts e="T204" id="Seg_2230" n="e" s="T203">änǯiŋ. </ts>
               <ts e="T205" id="Seg_2232" n="e" s="T204">naŋo </ts>
               <ts e="T206" id="Seg_2234" n="e" s="T205">pin </ts>
               <ts e="T207" id="Seg_2236" n="e" s="T206">soːŋ </ts>
               <ts e="T208" id="Seg_2238" n="e" s="T207">assə </ts>
               <ts e="T209" id="Seg_2240" n="e" s="T208">qondəzaŋ. </ts>
               <ts e="T210" id="Seg_2242" n="e" s="T209">qanǯim </ts>
               <ts e="T211" id="Seg_2244" n="e" s="T210">meːkuzaw </ts>
               <ts e="T212" id="Seg_2246" n="e" s="T211">pin </ts>
               <ts e="T213" id="Seg_2248" n="e" s="T212">tʼönǯin </ts>
               <ts e="T214" id="Seg_2250" n="e" s="T213">ǯenno. </ts>
               <ts e="T215" id="Seg_2252" n="e" s="T214">okkɨrɨŋ </ts>
               <ts e="T216" id="Seg_2254" n="e" s="T215">pin </ts>
               <ts e="T217" id="Seg_2256" n="e" s="T216">kanaŋmɨ </ts>
               <ts e="T218" id="Seg_2258" n="e" s="T217">muːdɨjgu </ts>
               <ts e="T219" id="Seg_2260" n="e" s="T218">äzuŋ. </ts>
               <ts e="T220" id="Seg_2262" n="e" s="T219">šɨdɨŋ </ts>
               <ts e="T221" id="Seg_2264" n="e" s="T220">narɨŋ </ts>
               <ts e="T222" id="Seg_2266" n="e" s="T221">wäqsä. </ts>
               <ts e="T223" id="Seg_2268" n="e" s="T222">man </ts>
               <ts e="T224" id="Seg_2270" n="e" s="T223">assä </ts>
               <ts e="T225" id="Seg_2272" n="e" s="T224">lärumbɨzaŋ. </ts>
               <ts e="T226" id="Seg_2274" n="e" s="T225">man </ts>
               <ts e="T227" id="Seg_2276" n="e" s="T226">oneŋ </ts>
               <ts e="T228" id="Seg_2278" n="e" s="T227">siwwə </ts>
               <ts e="T229" id="Seg_2280" n="e" s="T228">oralbɨzaw </ts>
               <ts e="T230" id="Seg_2282" n="e" s="T229">kak </ts>
               <ts e="T231" id="Seg_2284" n="e" s="T230">kozjäin </ts>
               <ts e="T232" id="Seg_2286" n="e" s="T231">onäŋ </ts>
               <ts e="T233" id="Seg_2288" n="e" s="T232">tüːɣonäŋ. </ts>
               <ts e="T234" id="Seg_2290" n="e" s="T233">man </ts>
               <ts e="T235" id="Seg_2292" n="e" s="T234">manǯibɨsaw </ts>
               <ts e="T236" id="Seg_2294" n="e" s="T235">tɨtti </ts>
               <ts e="T237" id="Seg_2296" n="e" s="T236">makandə </ts>
               <ts e="T238" id="Seg_2298" n="e" s="T237">warɣə </ts>
               <ts e="T239" id="Seg_2300" n="e" s="T238">nʼärɨn </ts>
               <ts e="T240" id="Seg_2302" n="e" s="T239">tobi. </ts>
               <ts e="T241" id="Seg_2304" n="e" s="T240">man </ts>
               <ts e="T242" id="Seg_2306" n="e" s="T241">täːrbɨzaŋ: </ts>
               <ts e="T243" id="Seg_2308" n="e" s="T242">qardʼe </ts>
               <ts e="T244" id="Seg_2310" n="e" s="T243">nʼärnä </ts>
               <ts e="T245" id="Seg_2312" n="e" s="T244">čaːčenǯaŋ </ts>
               <ts e="T246" id="Seg_2314" n="e" s="T245">na </ts>
               <ts e="T247" id="Seg_2316" n="e" s="T246">qanǯi </ts>
               <ts e="T248" id="Seg_2318" n="e" s="T247">wättɨ </ts>
               <ts e="T249" id="Seg_2320" n="e" s="T248">sünʼdʼewɨn. </ts>
               <ts e="T250" id="Seg_2322" n="e" s="T249">sɨsaːrum </ts>
               <ts e="T251" id="Seg_2324" n="e" s="T250">narqwäj </ts>
               <ts e="T252" id="Seg_2326" n="e" s="T251">qarʼimɨɣɨn </ts>
               <ts e="T253" id="Seg_2328" n="e" s="T252">kundə </ts>
               <ts e="T254" id="Seg_2330" n="e" s="T253">assə </ts>
               <ts e="T255" id="Seg_2332" n="e" s="T254">taqqɨlbɨzaŋ. </ts>
               <ts e="T256" id="Seg_2334" n="e" s="T255">čaːǯigu </ts>
               <ts e="T257" id="Seg_2336" n="e" s="T256">soː </ts>
               <ts e="T258" id="Seg_2338" n="e" s="T257">jes </ts>
               <ts e="T259" id="Seg_2340" n="e" s="T258">äːrraj </ts>
               <ts e="T260" id="Seg_2342" n="e" s="T259">qanǯi </ts>
               <ts e="T261" id="Seg_2344" n="e" s="T260">wättoɨn </ts>
               <ts e="T262" id="Seg_2346" n="e" s="T261">qaj </ts>
               <ts e="T263" id="Seg_2348" n="e" s="T262">qandəmbɨs. </ts>
               <ts e="T264" id="Seg_2350" n="e" s="T263">okkɨr </ts>
               <ts e="T265" id="Seg_2352" n="e" s="T264">qwäjgʼöt </ts>
               <ts e="T266" id="Seg_2354" n="e" s="T265">časoɣondə </ts>
               <ts e="T267" id="Seg_2356" n="e" s="T266">tʼeːlɨn </ts>
               <ts e="T268" id="Seg_2358" n="e" s="T267">man </ts>
               <ts e="T269" id="Seg_2360" n="e" s="T268">äːsaŋ </ts>
               <ts e="T270" id="Seg_2362" n="e" s="T269">naːr </ts>
               <ts e="T271" id="Seg_2364" n="e" s="T270">suːrulʼdʼi </ts>
               <ts e="T272" id="Seg_2366" n="e" s="T271">lʼäɣalɨnnan. </ts>
               <ts e="T273" id="Seg_2368" n="e" s="T272">täppɨla </ts>
               <ts e="T274" id="Seg_2370" n="e" s="T273">nʼäjam </ts>
               <ts e="T275" id="Seg_2372" n="e" s="T274">assä </ts>
               <ts e="T276" id="Seg_2374" n="e" s="T275">qwatkuzattə. </ts>
               <ts e="T277" id="Seg_2376" n="e" s="T276">tüːlʼewlʼe </ts>
               <ts e="T278" id="Seg_2378" n="e" s="T277">surulʼdi </ts>
               <ts e="T279" id="Seg_2380" n="e" s="T278">maːttə </ts>
               <ts e="T280" id="Seg_2382" n="e" s="T279">man </ts>
               <ts e="T281" id="Seg_2384" n="e" s="T280">qwässaŋ </ts>
               <ts e="T282" id="Seg_2386" n="e" s="T281">peːmɨgu. </ts>
               <ts e="T283" id="Seg_2388" n="e" s="T282">na </ts>
               <ts e="T284" id="Seg_2390" n="e" s="T283">dʼel </ts>
               <ts e="T285" id="Seg_2392" n="e" s="T284">man </ts>
               <ts e="T286" id="Seg_2394" n="e" s="T285">qwätnaw </ts>
               <ts e="T287" id="Seg_2396" n="e" s="T286">okkə </ts>
               <ts e="T288" id="Seg_2398" n="e" s="T287">nʼäjam, </ts>
               <ts e="T289" id="Seg_2400" n="e" s="T288">naːt </ts>
               <ts e="T290" id="Seg_2402" n="e" s="T289">na </ts>
               <ts e="T291" id="Seg_2404" n="e" s="T290">säːɣääs, </ts>
               <ts e="T292" id="Seg_2406" n="e" s="T291">i </ts>
               <ts e="T293" id="Seg_2408" n="e" s="T292">okkə </ts>
               <ts e="T294" id="Seg_2410" n="e" s="T293">saŋgɨm. </ts>
               <ts e="T295" id="Seg_2412" n="e" s="T294">äːsan </ts>
               <ts e="T296" id="Seg_2414" n="e" s="T295">vaskrʼösnɨj </ts>
               <ts e="T297" id="Seg_2416" n="e" s="T296">tʼeːlɨt. </ts>
               <ts e="T298" id="Seg_2418" n="e" s="T297">šɨdəmdälǯi </ts>
               <ts e="T299" id="Seg_2420" n="e" s="T298">dʼel. </ts>
               <ts e="T300" id="Seg_2422" n="e" s="T299">peːmɨzaŋ </ts>
               <ts e="T301" id="Seg_2424" n="e" s="T300">qwässaw </ts>
               <ts e="T302" id="Seg_2426" n="e" s="T301">šɨttə </ts>
               <ts e="T303" id="Seg_2428" n="e" s="T302">nʼäjam. </ts>
               <ts e="T304" id="Seg_2430" n="e" s="T303">täpkiː </ts>
               <ts e="T305" id="Seg_2432" n="e" s="T304">äːsadi </ts>
               <ts e="T306" id="Seg_2434" n="e" s="T305">siːnʼe </ts>
               <ts e="T307" id="Seg_2436" n="e" s="T306">udɨ </ts>
               <ts e="T308" id="Seg_2438" n="e" s="T307">säːɣə </ts>
               <ts e="T309" id="Seg_2440" n="e" s="T308">qoːbʼi. </ts>
               <ts e="T310" id="Seg_2442" n="e" s="T309">siːtʼew </ts>
               <ts e="T311" id="Seg_2444" n="e" s="T310">assä </ts>
               <ts e="T312" id="Seg_2446" n="e" s="T311">aːndalba. </ts>
               <ts e="T313" id="Seg_2448" n="e" s="T312">naːrɨndʼelǯe </ts>
               <ts e="T314" id="Seg_2450" n="e" s="T313">dʼel </ts>
               <ts e="T315" id="Seg_2452" n="e" s="T314">äːsan </ts>
               <ts e="T316" id="Seg_2454" n="e" s="T315">soː </ts>
               <ts e="T317" id="Seg_2456" n="e" s="T316">tʼeːlɨt. </ts>
               <ts e="T318" id="Seg_2458" n="e" s="T317">na </ts>
               <ts e="T319" id="Seg_2460" n="e" s="T318">dʼel </ts>
               <ts e="T320" id="Seg_2462" n="e" s="T319">qaimnassä </ts>
               <ts e="T321" id="Seg_2464" n="e" s="T320">qwässaw. </ts>
               <ts e="T322" id="Seg_2466" n="e" s="T321">tʼelɨt </ts>
               <ts e="T323" id="Seg_2468" n="e" s="T322">äːsan, </ts>
               <ts e="T324" id="Seg_2470" n="e" s="T323">tʼeːlɨt </ts>
               <ts e="T325" id="Seg_2472" n="e" s="T324">tʼeːlɨmbədi </ts>
               <ts e="T326" id="Seg_2474" n="e" s="T325">man </ts>
               <ts e="T327" id="Seg_2476" n="e" s="T326">pajannä. </ts>
               <ts e="T328" id="Seg_2478" n="e" s="T327">na </ts>
               <ts e="T329" id="Seg_2480" n="e" s="T328">dʼel </ts>
               <ts e="T330" id="Seg_2482" n="e" s="T329">lʼäɣalaze </ts>
               <ts e="T331" id="Seg_2484" n="e" s="T330">okkə </ts>
               <ts e="T332" id="Seg_2486" n="e" s="T331">mɨɣɨn </ts>
               <ts e="T333" id="Seg_2488" n="e" s="T332">palʼdʼüzot </ts>
               <ts e="T334" id="Seg_2490" n="e" s="T333">nʼärɨn </ts>
               <ts e="T335" id="Seg_2492" n="e" s="T334">taj </ts>
               <ts e="T336" id="Seg_2494" n="e" s="T335">pojkɨndə. </ts>
               <ts e="T337" id="Seg_2496" n="e" s="T336">nɨtʼän </ts>
               <ts e="T338" id="Seg_2498" n="e" s="T337">me </ts>
               <ts e="T339" id="Seg_2500" n="e" s="T338">qwädʼizot </ts>
               <ts e="T340" id="Seg_2502" n="e" s="T339">pondə </ts>
               <ts e="T341" id="Seg_2504" n="e" s="T340">nägärlʼe </ts>
               <ts e="T342" id="Seg_2506" n="e" s="T341">oːnɨt </ts>
               <ts e="T343" id="Seg_2508" n="e" s="T342">niwlawɨt. </ts>
               <ts e="T344" id="Seg_2510" n="e" s="T343">a </ts>
               <ts e="T345" id="Seg_2512" n="e" s="T344">me </ts>
               <ts e="T346" id="Seg_2514" n="e" s="T345">nimwɨt </ts>
               <ts e="T347" id="Seg_2516" n="e" s="T346">Мadʼö, </ts>
               <ts e="T348" id="Seg_2518" n="e" s="T347">Мaksa </ts>
               <ts e="T349" id="Seg_2520" n="e" s="T348">i </ts>
               <ts e="T350" id="Seg_2522" n="e" s="T349">Вaska. </ts>
               <ts e="T351" id="Seg_2524" n="e" s="T350">taw </ts>
               <ts e="T352" id="Seg_2526" n="e" s="T351">dʼel </ts>
               <ts e="T353" id="Seg_2528" n="e" s="T352">me </ts>
               <ts e="T354" id="Seg_2530" n="e" s="T353">tassot </ts>
               <ts e="T355" id="Seg_2532" n="e" s="T354">naːgur </ts>
               <ts e="T356" id="Seg_2534" n="e" s="T355">tawut </ts>
               <ts e="T357" id="Seg_2536" n="e" s="T356">qumbädi </ts>
               <ts e="T358" id="Seg_2538" n="e" s="T357">äsäw </ts>
               <ts e="T359" id="Seg_2540" n="e" s="T358">Sämonɨm. </ts>
               <ts e="T360" id="Seg_2542" n="e" s="T359">meɣanɨt </ts>
               <ts e="T361" id="Seg_2544" n="e" s="T360">äːsan </ts>
               <ts e="T362" id="Seg_2546" n="e" s="T361">opsä </ts>
               <ts e="T363" id="Seg_2548" n="e" s="T362">assə </ts>
               <ts e="T364" id="Seg_2550" n="e" s="T363">soŋ. </ts>
               <ts e="T365" id="Seg_2552" n="e" s="T364">tättɨmdelǯi </ts>
               <ts e="T366" id="Seg_2554" n="e" s="T365">dʼel </ts>
               <ts e="T367" id="Seg_2556" n="e" s="T366">qarimɨɣɨn </ts>
               <ts e="T368" id="Seg_2558" n="e" s="T367">püm </ts>
               <ts e="T369" id="Seg_2560" n="e" s="T368">äːs. </ts>
               <ts e="T370" id="Seg_2562" n="e" s="T369">üːdəmɨn </ts>
               <ts e="T371" id="Seg_2564" n="e" s="T370">äːsan </ts>
               <ts e="T372" id="Seg_2566" n="e" s="T371">särrot. </ts>
               <ts e="T373" id="Seg_2568" n="e" s="T372">tʼeːlɨt </ts>
               <ts e="T374" id="Seg_2570" n="e" s="T373">sättšim </ts>
               <ts e="T375" id="Seg_2572" n="e" s="T374">äːss. </ts>
               <ts e="T376" id="Seg_2574" n="e" s="T375">palʼdʼügu </ts>
               <ts e="T377" id="Seg_2576" n="e" s="T376">toppɨla </ts>
               <ts e="T378" id="Seg_2578" n="e" s="T377">nuːnɨmbɨzattə. </ts>
               <ts e="T379" id="Seg_2580" n="e" s="T378">na </ts>
               <ts e="T380" id="Seg_2582" n="e" s="T379">dʼel </ts>
               <ts e="T381" id="Seg_2584" n="e" s="T380">tannaw </ts>
               <ts e="T382" id="Seg_2586" n="e" s="T381">naːr </ts>
               <ts e="T383" id="Seg_2588" n="e" s="T382">nʼäjam. </ts>
               <ts e="T384" id="Seg_2590" n="e" s="T383">sombɨlʼemdälǯi </ts>
               <ts e="T385" id="Seg_2592" n="e" s="T384">dʼel </ts>
               <ts e="T386" id="Seg_2594" n="e" s="T385">okkɨr </ts>
               <ts e="T387" id="Seg_2596" n="e" s="T386">lʼäɣa, </ts>
               <ts e="T388" id="Seg_2598" n="e" s="T387">nimdə </ts>
               <ts e="T389" id="Seg_2600" n="e" s="T388">täbɨn </ts>
               <ts e="T390" id="Seg_2602" n="e" s="T389">Wasilʼij, </ts>
               <ts e="T391" id="Seg_2604" n="e" s="T390">qwässɨ </ts>
               <ts e="T392" id="Seg_2606" n="e" s="T391">maːtqɨndə. </ts>
               <ts e="T393" id="Seg_2608" n="e" s="T392">naː </ts>
               <ts e="T394" id="Seg_2610" n="e" s="T393">dʼel </ts>
               <ts e="T395" id="Seg_2612" n="e" s="T394">tassaw </ts>
               <ts e="T396" id="Seg_2614" n="e" s="T395">tättə </ts>
               <ts e="T397" id="Seg_2616" n="e" s="T396">nʼäjam. </ts>
               <ts e="T398" id="Seg_2618" n="e" s="T397">muqtumdälǯi </ts>
               <ts e="T399" id="Seg_2620" n="e" s="T398">dʼel </ts>
               <ts e="T400" id="Seg_2622" n="e" s="T399">palʼdʼüzaŋ </ts>
               <ts e="T401" id="Seg_2624" n="e" s="T400">pʼüru </ts>
               <ts e="T402" id="Seg_2626" n="e" s="T401">nʼäran </ts>
               <ts e="T403" id="Seg_2628" n="e" s="T402">taj. </ts>
               <ts e="T404" id="Seg_2630" n="e" s="T403">äːrukkaŋ </ts>
               <ts e="T405" id="Seg_2632" n="e" s="T404">čaːǯizaŋ </ts>
               <ts e="T406" id="Seg_2634" n="e" s="T405">naŋo </ts>
               <ts e="T407" id="Seg_2636" n="e" s="T406">üŋulʼǯumbulʼe </ts>
               <ts e="T408" id="Seg_2638" n="e" s="T407">qaːɣə </ts>
               <ts e="T409" id="Seg_2640" n="e" s="T408">muːtolʼdʼenǯiŋ </ts>
               <ts e="T410" id="Seg_2642" n="e" s="T409">man </ts>
               <ts e="T411" id="Seg_2644" n="e" s="T410">kanaŋmə. </ts>
               <ts e="T412" id="Seg_2646" n="e" s="T411">nimdə </ts>
               <ts e="T413" id="Seg_2648" n="e" s="T412">täbɨn </ts>
               <ts e="T414" id="Seg_2650" n="e" s="T413">Äːdu. </ts>
               <ts e="T415" id="Seg_2652" n="e" s="T414">ündədʼäw </ts>
               <ts e="T416" id="Seg_2654" n="e" s="T415">(ündɨzäjaw) </ts>
               <ts e="T417" id="Seg_2656" n="e" s="T416">kanaŋmə </ts>
               <ts e="T418" id="Seg_2658" n="e" s="T417">muːdɨŋ </ts>
               <ts e="T419" id="Seg_2660" n="e" s="T418">nʼäjandə. </ts>
               <ts e="T420" id="Seg_2662" n="e" s="T419">mitänǯigu </ts>
               <ts e="T421" id="Seg_2664" n="e" s="T420">äːduni, </ts>
               <ts e="T422" id="Seg_2666" n="e" s="T421">täp </ts>
               <ts e="T423" id="Seg_2668" n="e" s="T422">soːŋ </ts>
               <ts e="T424" id="Seg_2670" n="e" s="T423">muːdɨŋ </ts>
               <ts e="T425" id="Seg_2672" n="e" s="T424">warɣə </ts>
               <ts e="T426" id="Seg_2674" n="e" s="T425">pirəga </ts>
               <ts e="T427" id="Seg_2676" n="e" s="T426">tɨttonda. </ts>
               <ts e="T428" id="Seg_2678" n="e" s="T427">manǯimbaŋ </ts>
               <ts e="T429" id="Seg_2680" n="e" s="T428">innä, </ts>
               <ts e="T430" id="Seg_2682" n="e" s="T429">assə </ts>
               <ts e="T431" id="Seg_2684" n="e" s="T430">qolʼdikwaw. </ts>
               <ts e="T432" id="Seg_2686" n="e" s="T431">nʼäjam </ts>
               <ts e="T433" id="Seg_2688" n="e" s="T432">lärčigu, </ts>
               <ts e="T434" id="Seg_2690" n="e" s="T433">qonɨŋ </ts>
               <ts e="T435" id="Seg_2692" n="e" s="T434">tʼätʼčizaŋ </ts>
               <ts e="T436" id="Seg_2694" n="e" s="T435">toːzopkazä, </ts>
               <ts e="T437" id="Seg_2696" n="e" s="T436">a </ts>
               <ts e="T438" id="Seg_2698" n="e" s="T437">nännä </ts>
               <ts e="T439" id="Seg_2700" n="e" s="T438">assə </ts>
               <ts e="T440" id="Seg_2702" n="e" s="T439">okkɨrɨŋ </ts>
               <ts e="T441" id="Seg_2704" n="e" s="T440">tʼätʼtšizaŋ </ts>
               <ts e="T442" id="Seg_2706" n="e" s="T441">šɨttə </ts>
               <ts e="T443" id="Seg_2708" n="e" s="T442">puːǯi </ts>
               <ts e="T444" id="Seg_2710" n="e" s="T443">tüːləsäzä. </ts>
               <ts e="T445" id="Seg_2712" n="e" s="T444">kɨkkɨzaŋ </ts>
               <ts e="T446" id="Seg_2714" n="e" s="T445">man </ts>
               <ts e="T447" id="Seg_2716" n="e" s="T446">täbɨm </ts>
               <ts e="T448" id="Seg_2718" n="e" s="T447">qoːlʼdigu. </ts>
               <ts e="T449" id="Seg_2720" n="e" s="T448">man </ts>
               <ts e="T450" id="Seg_2722" n="e" s="T449">täbɨm </ts>
               <ts e="T451" id="Seg_2724" n="e" s="T450">qolʼdʼäw </ts>
               <ts e="T452" id="Seg_2726" n="e" s="T451">tɨttən </ts>
               <ts e="T453" id="Seg_2728" n="e" s="T452">oloɣɨn. </ts>
               <ts e="T454" id="Seg_2730" n="e" s="T453">assə </ts>
               <ts e="T455" id="Seg_2732" n="e" s="T454">naːrɨŋ </ts>
               <ts e="T456" id="Seg_2734" n="e" s="T455">tättɨŋ </ts>
               <ts e="T457" id="Seg_2736" n="e" s="T456">tɨttɨn </ts>
               <ts e="T458" id="Seg_2738" n="e" s="T457">olondə </ts>
               <ts e="T459" id="Seg_2740" n="e" s="T458">tʼättšikuzaŋ </ts>
               <ts e="T460" id="Seg_2742" n="e" s="T459">i </ts>
               <ts e="T461" id="Seg_2744" n="e" s="T460">qwäːtʼäw. </ts>
               <ts e="T462" id="Seg_2746" n="e" s="T461">nʼegətšeːjaŋ, </ts>
               <ts e="T463" id="Seg_2748" n="e" s="T462">nɨŋejaŋ. </ts>
               <ts e="T464" id="Seg_2750" n="e" s="T463">assə </ts>
               <ts e="T465" id="Seg_2752" n="e" s="T464">kɨkkɨzaŋ </ts>
               <ts e="T466" id="Seg_2754" n="e" s="T465">qwädʼigu </ts>
               <ts e="T467" id="Seg_2756" n="e" s="T466">nʼäjam. </ts>
               <ts e="T468" id="Seg_2758" n="e" s="T467">manǯimbaŋ </ts>
               <ts e="T469" id="Seg_2760" n="e" s="T468">man </ts>
               <ts e="T470" id="Seg_2762" n="e" s="T469">matʼtʼöɣi </ts>
               <ts e="T471" id="Seg_2764" n="e" s="T470">lʼäɣaw, </ts>
               <ts e="T472" id="Seg_2766" n="e" s="T471">nimdə </ts>
               <ts e="T473" id="Seg_2768" n="e" s="T472">täbɨn </ts>
               <ts e="T474" id="Seg_2770" n="e" s="T473">äːdu, </ts>
               <ts e="T475" id="Seg_2772" n="e" s="T474">qaimdaka </ts>
               <ts e="T476" id="Seg_2774" n="e" s="T475">meːkut </ts>
               <ts e="T477" id="Seg_2776" n="e" s="T476">tʼäǯikutäj </ts>
               <ts e="T478" id="Seg_2778" n="e" s="T477">tɨːdɨn </ts>
               <ts e="T479" id="Seg_2780" n="e" s="T478">taːbaɣɨn. </ts>
               <ts e="T480" id="Seg_2782" n="e" s="T479">man </ts>
               <ts e="T481" id="Seg_2784" n="e" s="T480">tolʼdʼin </ts>
               <ts e="T482" id="Seg_2786" n="e" s="T481">nʼöɣɨnnaŋ </ts>
               <ts e="T483" id="Seg_2788" n="e" s="T482">täbɨnni. </ts>
               <ts e="T484" id="Seg_2790" n="e" s="T483">täp </ts>
               <ts e="T485" id="Seg_2792" n="e" s="T484">pɨŋgəlɨmbɨdi </ts>
               <ts e="T486" id="Seg_2794" n="e" s="T485">nʼäjam, </ts>
               <ts e="T487" id="Seg_2796" n="e" s="T486">kunnɨ </ts>
               <ts e="T488" id="Seg_2798" n="e" s="T487">man </ts>
               <ts e="T489" id="Seg_2800" n="e" s="T488">nɨŋɨzaŋ, </ts>
               <ts e="T490" id="Seg_2802" n="e" s="T489">nʼegɨtšizaŋ, </ts>
               <ts e="T491" id="Seg_2804" n="e" s="T490">täp </ts>
               <ts e="T492" id="Seg_2806" n="e" s="T491">ambat. </ts>
               <ts e="T493" id="Seg_2808" n="e" s="T492">qwädimbat </ts>
               <ts e="T494" id="Seg_2810" n="e" s="T493">meŋa </ts>
               <ts e="T495" id="Seg_2812" n="e" s="T494">moqqoɣɨ </ts>
               <ts e="T496" id="Seg_2814" n="e" s="T495">tobɨkalamdə </ts>
               <ts e="T497" id="Seg_2816" n="e" s="T496">(ɨ), </ts>
               <ts e="T498" id="Seg_2818" n="e" s="T497">pušqaj </ts>
               <ts e="T499" id="Seg_2820" n="e" s="T498">talʼdʼüm. </ts>
               <ts e="T500" id="Seg_2822" n="e" s="T499">me </ts>
               <ts e="T501" id="Seg_2824" n="e" s="T500">täpsä </ts>
               <ts e="T502" id="Seg_2826" n="e" s="T501">qwäduzoː, </ts>
               <ts e="T503" id="Seg_2828" n="e" s="T502">tɨttäruzoː </ts>
               <ts e="T504" id="Seg_2830" n="e" s="T503">aj </ts>
               <ts e="T505" id="Seg_2832" n="e" s="T504">miriuzo. </ts>
               <ts e="T506" id="Seg_2834" n="e" s="T505">na </ts>
               <ts e="T507" id="Seg_2836" n="e" s="T506">dʼel </ts>
               <ts e="T508" id="Seg_2838" n="e" s="T507">me </ts>
               <ts e="T509" id="Seg_2840" n="e" s="T508">tasso </ts>
               <ts e="T510" id="Seg_2842" n="e" s="T509">šɨttə </ts>
               <ts e="T511" id="Seg_2844" n="e" s="T510">nʼäjam. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T24" id="Seg_2845" s="T1">KMS_1963_Hunting_transl.001 (001.001)</ta>
            <ta e="T30" id="Seg_2846" s="T24">KMS_1963_Hunting_transl.002 (001.002)</ta>
            <ta e="T36" id="Seg_2847" s="T30">KMS_1963_Hunting_transl.003 (001.003)</ta>
            <ta e="T40" id="Seg_2848" s="T36">KMS_1963_Hunting_transl.004 (001.004)</ta>
            <ta e="T51" id="Seg_2849" s="T40">KMS_1963_Hunting_transl.005 (001.005)</ta>
            <ta e="T57" id="Seg_2850" s="T51">KMS_1963_Hunting_transl.006 (001.006)</ta>
            <ta e="T61" id="Seg_2851" s="T57">KMS_1963_Hunting_transl.007 (001.007)</ta>
            <ta e="T66" id="Seg_2852" s="T61">KMS_1963_Hunting_transl.008 (001.008)</ta>
            <ta e="T70" id="Seg_2853" s="T66">KMS_1963_Hunting_transl.009 (001.009)</ta>
            <ta e="T73" id="Seg_2854" s="T70">KMS_1963_Hunting_transl.010 (001.010)</ta>
            <ta e="T79" id="Seg_2855" s="T73">KMS_1963_Hunting_transl.011 (001.011)</ta>
            <ta e="T81" id="Seg_2856" s="T79">KMS_1963_Hunting_transl.012 (001.012)</ta>
            <ta e="T86" id="Seg_2857" s="T81">KMS_1963_Hunting_transl.013 (001.013)</ta>
            <ta e="T91" id="Seg_2858" s="T86">KMS_1963_Hunting_transl.014 (001.014)</ta>
            <ta e="T96" id="Seg_2859" s="T91">KMS_1963_Hunting_transl.015 (001.015)</ta>
            <ta e="T98" id="Seg_2860" s="T96">KMS_1963_Hunting_transl.016 (001.016)</ta>
            <ta e="T102" id="Seg_2861" s="T98">KMS_1963_Hunting_transl.017 (001.017)</ta>
            <ta e="T106" id="Seg_2862" s="T102">KMS_1963_Hunting_transl.018 (001.018)</ta>
            <ta e="T108" id="Seg_2863" s="T106">KMS_1963_Hunting_transl.019 (001.019)</ta>
            <ta e="T112" id="Seg_2864" s="T108">KMS_1963_Hunting_transl.020 (001.020)</ta>
            <ta e="T124" id="Seg_2865" s="T112">KMS_1963_Hunting_transl.021 (001.021)</ta>
            <ta e="T131" id="Seg_2866" s="T124">KMS_1963_Hunting_transl.022 (001.022)</ta>
            <ta e="T135" id="Seg_2867" s="T131">KMS_1963_Hunting_transl.023 (001.023)</ta>
            <ta e="T141" id="Seg_2868" s="T135">KMS_1963_Hunting_transl.024 (001.024)</ta>
            <ta e="T143" id="Seg_2869" s="T141">KMS_1963_Hunting_transl.025 (001.025)</ta>
            <ta e="T145" id="Seg_2870" s="T143">KMS_1963_Hunting_transl.026 (001.026)</ta>
            <ta e="T157" id="Seg_2871" s="T145">KMS_1963_Hunting_transl.027 (001.027)</ta>
            <ta e="T168" id="Seg_2872" s="T157">KMS_1963_Hunting_transl.028 (001.028)</ta>
            <ta e="T171" id="Seg_2873" s="T168">KMS_1963_Hunting_transl.029 (001.029)</ta>
            <ta e="T178" id="Seg_2874" s="T171">KMS_1963_Hunting_transl.030 (001.030)</ta>
            <ta e="T182" id="Seg_2875" s="T178">KMS_1963_Hunting_transl.031 (001.031)</ta>
            <ta e="T197" id="Seg_2876" s="T182">KMS_1963_Hunting_transl.032 (001.032)</ta>
            <ta e="T204" id="Seg_2877" s="T197">KMS_1963_Hunting_transl.033 (001.033)</ta>
            <ta e="T209" id="Seg_2878" s="T204">KMS_1963_Hunting_transl.034 (001.034)</ta>
            <ta e="T214" id="Seg_2879" s="T209">KMS_1963_Hunting_transl.035 (001.035)</ta>
            <ta e="T219" id="Seg_2880" s="T214">KMS_1963_Hunting_transl.036 (001.036)</ta>
            <ta e="T222" id="Seg_2881" s="T219">KMS_1963_Hunting_transl.037 (001.037)</ta>
            <ta e="T225" id="Seg_2882" s="T222">KMS_1963_Hunting_transl.038 (001.038)</ta>
            <ta e="T233" id="Seg_2883" s="T225">KMS_1963_Hunting_transl.039 (001.039)</ta>
            <ta e="T240" id="Seg_2884" s="T233">KMS_1963_Hunting_transl.040 (001.040)</ta>
            <ta e="T249" id="Seg_2885" s="T240">KMS_1963_Hunting_transl.041 (001.041)</ta>
            <ta e="T255" id="Seg_2886" s="T249">KMS_1963_Hunting_transl.042 (001.042)</ta>
            <ta e="T263" id="Seg_2887" s="T255">KMS_1963_Hunting_transl.043 (001.043)</ta>
            <ta e="T272" id="Seg_2888" s="T263">KMS_1963_Hunting_transl.044 (001.044)</ta>
            <ta e="T276" id="Seg_2889" s="T272">KMS_1963_Hunting_transl.045 (001.045)</ta>
            <ta e="T282" id="Seg_2890" s="T276">KMS_1963_Hunting_transl.046 (001.046)</ta>
            <ta e="T294" id="Seg_2891" s="T282">KMS_1963_Hunting_transl.047 (001.047)</ta>
            <ta e="T297" id="Seg_2892" s="T294">KMS_1963_Hunting_transl.048 (001.048)</ta>
            <ta e="T299" id="Seg_2893" s="T297">KMS_1963_Hunting_transl.049 (001.049)</ta>
            <ta e="T303" id="Seg_2894" s="T299">KMS_1963_Hunting_transl.050 (001.050)</ta>
            <ta e="T309" id="Seg_2895" s="T303">KMS_1963_Hunting_transl.051 (001.051)</ta>
            <ta e="T312" id="Seg_2896" s="T309">KMS_1963_Hunting_transl.052 (001.052)</ta>
            <ta e="T317" id="Seg_2897" s="T312">KMS_1963_Hunting_transl.053 (001.053)</ta>
            <ta e="T321" id="Seg_2898" s="T317">KMS_1963_Hunting_transl.054 (001.054)</ta>
            <ta e="T327" id="Seg_2899" s="T321">KMS_1963_Hunting_transl.055 (001.055)</ta>
            <ta e="T336" id="Seg_2900" s="T327">KMS_1963_Hunting_transl.056 (001.056)</ta>
            <ta e="T343" id="Seg_2901" s="T336">KMS_1963_Hunting_transl.057 (001.057)</ta>
            <ta e="T350" id="Seg_2902" s="T343">KMS_1963_Hunting_transl.058 (001.058)</ta>
            <ta e="T359" id="Seg_2903" s="T350">KMS_1963_Hunting_transl.059 (001.059)</ta>
            <ta e="T364" id="Seg_2904" s="T359">KMS_1963_Hunting_transl.060 (001.060)</ta>
            <ta e="T369" id="Seg_2905" s="T364">KMS_1963_Hunting_transl.061 (001.061)</ta>
            <ta e="T372" id="Seg_2906" s="T369">KMS_1963_Hunting_transl.062 (001.062)</ta>
            <ta e="T375" id="Seg_2907" s="T372">KMS_1963_Hunting_transl.063 (001.063)</ta>
            <ta e="T378" id="Seg_2908" s="T375">KMS_1963_Hunting_transl.064 (001.064)</ta>
            <ta e="T383" id="Seg_2909" s="T378">KMS_1963_Hunting_transl.065 (001.065)</ta>
            <ta e="T392" id="Seg_2910" s="T383">KMS_1963_Hunting_transl.066 (001.066)</ta>
            <ta e="T397" id="Seg_2911" s="T392">KMS_1963_Hunting_transl.067 (001.067)</ta>
            <ta e="T403" id="Seg_2912" s="T397">KMS_1963_Hunting_transl.068 (001.068)</ta>
            <ta e="T411" id="Seg_2913" s="T403">KMS_1963_Hunting_transl.069 (001.069)</ta>
            <ta e="T414" id="Seg_2914" s="T411">KMS_1963_Hunting_transl.070 (001.070)</ta>
            <ta e="T419" id="Seg_2915" s="T414">KMS_1963_Hunting_transl.071 (001.071)</ta>
            <ta e="T427" id="Seg_2916" s="T419">KMS_1963_Hunting_transl.072 (001.072)</ta>
            <ta e="T431" id="Seg_2917" s="T427">KMS_1963_Hunting_transl.073 (001.073)</ta>
            <ta e="T444" id="Seg_2918" s="T431">KMS_1963_Hunting_transl.074 (001.074)</ta>
            <ta e="T448" id="Seg_2919" s="T444">KMS_1963_Hunting_transl.075 (001.075)</ta>
            <ta e="T453" id="Seg_2920" s="T448">KMS_1963_Hunting_transl.076 (001.076)</ta>
            <ta e="T461" id="Seg_2921" s="T453">KMS_1963_Hunting_transl.077 (001.077)</ta>
            <ta e="T463" id="Seg_2922" s="T461">KMS_1963_Hunting_transl.078 (001.078)</ta>
            <ta e="T467" id="Seg_2923" s="T463">KMS_1963_Hunting_transl.079 (001.079)</ta>
            <ta e="T479" id="Seg_2924" s="T467">KMS_1963_Hunting_transl.080 (001.080)</ta>
            <ta e="T483" id="Seg_2925" s="T479">KMS_1963_Hunting_transl.081 (001.081)</ta>
            <ta e="T492" id="Seg_2926" s="T483">KMS_1963_Hunting_transl.082 (001.082)</ta>
            <ta e="T499" id="Seg_2927" s="T492">KMS_1963_Hunting_transl.083 (001.083)</ta>
            <ta e="T505" id="Seg_2928" s="T499">KMS_1963_Hunting_transl.084 (001.084)</ta>
            <ta e="T511" id="Seg_2929" s="T505">KMS_1963_Hunting_transl.085 (001.085)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="stl" tierref="stl">
            <ta e="T24" id="Seg_2930" s="T1">šɨtə saːrum šɨttə qwäjgʼöt čisloɣondə aktʼäbərin ireɣə(ɨ)ndo okkə tɨsʼäča (okkɨr) tʼädʼigʼöt ton muqtut saːrum pottə (poɣändə) mat qwassaŋ suːrulʼgu nʼärɨntaːj suːrulʼdi qun maːttə.</ta>
            <ta e="T30" id="Seg_2931" s="T24">nimdə maːdʼin tšvätšin Мitrij Сidərɨn maːtʼtʼe.</ta>
            <ta e="T36" id="Seg_2932" s="T30">nadə äːsan äran, tʼäk äːdi ärat.</ta>
            <ta e="T40" id="Seg_2933" s="T36">man vättow äs(s)an soː.</ta>
            <ta e="T51" id="Seg_2934" s="T40">naŋo soː äːss qanǯivättə sunʼdʼeːvɨn nʼärɨn taj qajevɨn naːdəmnes tšaːǯigu, qandɨmbäːs.</ta>
            <ta e="T57" id="Seg_2935" s="T51">i naːr lʼäɣa qwänbɨzattə man nʼärnän.</ta>
            <ta e="T61" id="Seg_2936" s="T57">täppɨlan nimdə Вasilij, Мaksim.</ta>
            <ta e="T66" id="Seg_2937" s="T61">Мaksimɨn kojandə nimdə man äːvɨlǯɨmbaw.</ta>
            <ta e="T70" id="Seg_2938" s="T66">tappɨla qwandɨmbattə tolʼdʼi vättɨm.</ta>
            <ta e="T73" id="Seg_2939" s="T70">täppɨlan tolʼdʼivättə qandämba.</ta>
            <ta e="T79" id="Seg_2940" s="T73">taw dʼeːl kulʼdʼiŋ kɨkkɨzaŋ qweːdulugu nʼäjakazä.</ta>
            <ta e="T81" id="Seg_2941" s="T79">assə qweːduluaŋ.</ta>
            <ta e="T86" id="Seg_2942" s="T81">okkɨr nʼäjam kanaŋmo (kanalaw) muːdə(ä)jbɨzɨt.</ta>
            <ta e="T91" id="Seg_2943" s="T86">man manǯibɨzaw tɨttoɣɨn, assa qolʼdizaw.</ta>
            <ta e="T96" id="Seg_2944" s="T91">kanaŋmə qoːɨt šɨdəmdälǯi nʼäja kväːrgoɣon.</ta>
            <ta e="T98" id="Seg_2945" s="T96">man qolʼdʼäw.</ta>
            <ta e="T102" id="Seg_2946" s="T98">täbɨm tʼättšigu assə äːsaŋ.</ta>
            <ta e="T106" id="Seg_2947" s="T102">naŋo täbɨm assə qwässaw.</ta>
            <ta e="T108" id="Seg_2948" s="T106">täp nʼäːrɣeːs.</ta>
            <ta e="T112" id="Seg_2949" s="T108">nʼärɣa nʼäjam assə tʼäǯiqwot.</ta>
            <ta e="T124" id="Seg_2950" s="T112">assä(ə) mittälʼe tšwätšin nimdə Мitrij Sidərɨn i Lukanʼtʼin lʼevi maːttə panaluŋ qanǯow.</ta>
            <ta e="T131" id="Seg_2951" s="T124">na qanǯow mimbedi i qombädi man pajannä.</ta>
            <ta e="T135" id="Seg_2952" s="T131">vättə ässan aː soː.</ta>
            <ta e="T141" id="Seg_2953" s="T135">qwändi qulan muqqoɣonna qanǯivättə assə qandɨmbɨs.</ta>
            <ta e="T143" id="Seg_2954" s="T141">üːdəmlʼe oldɨŋ.</ta>
            <ta e="T145" id="Seg_2955" s="T143">qanǯow panaluŋ.</ta>
            <ta e="T157" id="Seg_2956" s="T145">qailaw i panalumbädi qanǯongaw i qailaw moqqalʼe qwändaw nʼärɨn tobi tɨttɨ mɨsɨndə.</ta>
            <ta e="T168" id="Seg_2957" s="T157">tɨttɨ mɨsɨɣa(ɨ)n pennaw tüːm, laktšenaw tɨdɨn molam, meːjaw oːmdɨssa kutšasoti qoptəm.</ta>
            <ta e="T171" id="Seg_2958" s="T168">madʼö sagɨŋ uttərɨŋ.</ta>
            <ta e="T178" id="Seg_2959" s="T171">meŋnan okkɨ täːrba kundar laːdiŋu panalumbodi qaːnǯim.</ta>
            <ta e="T182" id="Seg_2960" s="T178">meŋnan äːsattə nʼön kɨssɨnɨla.</ta>
            <ta e="T197" id="Seg_2961" s="T182">šɨttə saːrum šɨttə qwäjgʼöt šɨttə saːrum naːr qwäjgʼöt pin man laqqɨzaŋ, sagɨŋ praːiksaw on(n)äŋ qanǯaw.</ta>
            <ta e="T204" id="Seg_2962" s="T197">mindɨn taːdərɨgu qailam (katomkoj) meŋga sättšim änǯiŋ.</ta>
            <ta e="T209" id="Seg_2963" s="T204">naŋo pin soːŋ assə qondəzaŋ.</ta>
            <ta e="T214" id="Seg_2964" s="T209">qanǯim meːkuzaw pin tʼönǯinǯe(ä)nno.</ta>
            <ta e="T219" id="Seg_2965" s="T214">okkɨrɨŋ pin kanaŋmɨ muːdɨjgu äzuŋ.</ta>
            <ta e="T222" id="Seg_2966" s="T219">šɨdɨŋ narɨŋ vä(a)qsä.</ta>
            <ta e="T225" id="Seg_2967" s="T222">man assä lärumbɨzaŋ.</ta>
            <ta e="T233" id="Seg_2968" s="T225">man one(ä)ŋ sivvə oralbɨzaw kak kozʼäin onäŋ tüːɣonäŋ.</ta>
            <ta e="T240" id="Seg_2969" s="T233">man manǯibɨsaw tɨtti makandə varɣə nʼärɨn tobi.</ta>
            <ta e="T249" id="Seg_2970" s="T240">man täːrbɨzaŋ qardʼe nʼärnä tšaːtšenǯaŋ na qanǯi vättɨ sünʼdʼevɨn.</ta>
            <ta e="T255" id="Seg_2971" s="T249">sɨsaːrum narqwäj qarʼimɨɣɨn kundə assə taqqɨlbɨzaŋ.</ta>
            <ta e="T263" id="Seg_2972" s="T255">tšaːǯigu soː jes äːrraj qanǯi vättoɨn qaj qandəmbɨs.</ta>
            <ta e="T272" id="Seg_2973" s="T263">okkɨr qwäjgʼöt časoɣondə tʼeːlɨn man äːsaŋ naːr suːrulʼdʼi lʼäɣalɨnnan.</ta>
            <ta e="T276" id="Seg_2974" s="T272">täppɨla nʼäjam assä qwatkuzattə.</ta>
            <ta e="T282" id="Seg_2975" s="T276">tüːlʼevlʼe surulʼdi maːttə man qwässaŋ peːmɨgu.</ta>
            <ta e="T294" id="Seg_2976" s="T282">na dʼel man qwätnaw okkə nʼäjam, naːt na säːɣäːs, i okkə saŋgɨm.</ta>
            <ta e="T297" id="Seg_2977" s="T294">äːsan vaskrʼösnɨj tʼeːlɨt.</ta>
            <ta e="T299" id="Seg_2978" s="T297">šɨdəmdälǯi dʼel.</ta>
            <ta e="T303" id="Seg_2979" s="T299">peːmɨzaŋ qwässaw šɨttə nʼäjam.</ta>
            <ta e="T309" id="Seg_2980" s="T303">täpkiː(ɨː) äːsadi siːnʼe udɨ säːɣə qoːbʼi.</ta>
            <ta e="T312" id="Seg_2981" s="T309">siːtʼew assä aːndalba.</ta>
            <ta e="T317" id="Seg_2982" s="T312">naːrɨndʼelǯe dʼel äːsan soː tʼeːlɨt.</ta>
            <ta e="T321" id="Seg_2983" s="T317">na dʼel qaimnassä qwässaw.</ta>
            <ta e="T327" id="Seg_2984" s="T321">tʼelɨt äːsan, tʼeːlɨt tʼeːlɨmbədi man pajannä.</ta>
            <ta e="T336" id="Seg_2985" s="T327">na dʼel lʼäɣalaze okkə mɨɣɨn palʼdʼüzot nʼärɨn taj pojkɨndə.</ta>
            <ta e="T343" id="Seg_2986" s="T336">nɨtʼän me qwädʼizot pondə nägärlʼe oːnɨt nivlawɨt.</ta>
            <ta e="T350" id="Seg_2987" s="T343">a me nimvɨ(ä)t Мadʼö, Мaksa i Вaska.</ta>
            <ta e="T359" id="Seg_2988" s="T350">taw dʼel me tassot naːgur tavut qumbädi äsäw sämonɨm.</ta>
            <ta e="T364" id="Seg_2989" s="T359">meɣanɨt äːsan opsä assə soŋ.</ta>
            <ta e="T369" id="Seg_2990" s="T364">tättɨm delǯi dʼel qarimɨ(ä)ɣɨn püm äːs.</ta>
            <ta e="T372" id="Seg_2991" s="T369">üːdəmɨn äːsan särrot.</ta>
            <ta e="T375" id="Seg_2992" s="T372">tʼeːlɨt sättšim äːss.</ta>
            <ta e="T378" id="Seg_2993" s="T375">palʼdʼügu toppɨla nuːnɨmbɨzattə(a).</ta>
            <ta e="T383" id="Seg_2994" s="T378">na dʼel tannaw naːr nʼäjam.</ta>
            <ta e="T392" id="Seg_2995" s="T383">sombɨlʼem dälǯi dʼel okkɨr lʼäɣa, nimdə täbɨn Vasilʼij, qwässɨ maːtqɨndə.</ta>
            <ta e="T397" id="Seg_2996" s="T392">naː dʼel tassaw tättə nʼäjam.</ta>
            <ta e="T403" id="Seg_2997" s="T397">muqtum dälǯi dʼel palʼdʼüzaŋ pʼüru nʼäran taj.</ta>
            <ta e="T411" id="Seg_2998" s="T403">äːrukkaŋ tšaːǯizaŋ naŋo üŋulʼǯumbulʼe qaːɣə muːtolʼdʼenǯiŋ man kanaŋmə.</ta>
            <ta e="T414" id="Seg_2999" s="T411">nimdə täbɨn äːdu.</ta>
            <ta e="T419" id="Seg_3000" s="T414">ündədʼ(d)äw (ündɨzäjaw) kanaŋmə muːdɨŋ nʼäjandə.</ta>
            <ta e="T427" id="Seg_3001" s="T419">mitänǯigu äːdun(n)i, täp soːŋ muːdɨŋ warɣə pirəga tɨttonda.</ta>
            <ta e="T431" id="Seg_3002" s="T427">manǯimbaŋ innä, assə qolʼdikwaw.</ta>
            <ta e="T444" id="Seg_3003" s="T431">nʼäjam lärtšigu, qonɨŋ tʼätʼtšizaŋ toːzopkazä, a nännä assə okkɨrɨŋ tʼätʼtšizaŋ šɨttə puːǯi tüːləsäzä.</ta>
            <ta e="T448" id="Seg_3004" s="T444">kɨkkɨzaŋ man täbɨm qoːlʼdigu.</ta>
            <ta e="T453" id="Seg_3005" s="T448">man täbɨm qolʼdʼäw tɨttən oloɣɨn.</ta>
            <ta e="T461" id="Seg_3006" s="T453">assə naːrɨŋ tättɨŋ tɨttɨn olondə tʼättšikuzaŋ i qwäːtʼäw.</ta>
            <ta e="T463" id="Seg_3007" s="T461">nʼegətšeːjaŋ, nɨŋejaŋ.</ta>
            <ta e="T467" id="Seg_3008" s="T463">assə kɨkkɨzaŋ qwädʼigu nʼäjam.</ta>
            <ta e="T479" id="Seg_3009" s="T467">manǯimbaŋ man matʼtʼöɣi lʼäɣaw, nimdə täbɨn äːdu, qaimdaka meːkut tʼäǯikutäj tɨːdɨn taːbaɣɨn.</ta>
            <ta e="T483" id="Seg_3010" s="T479">man tolʼdʼin nʼöɣɨnnaŋ täbɨnni.</ta>
            <ta e="T492" id="Seg_3011" s="T483">täp pɨŋgəlɨmbɨdi nʼäjam, kunnɨ man nɨŋɨzaŋ, nʼegɨtšizaŋ, täp ambat.</ta>
            <ta e="T499" id="Seg_3012" s="T492">qwädimbat meŋa moqqoɣɨ tobɨkalamdə(ɨ), pušqaj talʼdʼüm.</ta>
            <ta e="T505" id="Seg_3013" s="T499">me täpsä qwäduzoː, tɨttäruzoː aj miriuzo.</ta>
            <ta e="T511" id="Seg_3014" s="T505">na dʼel me tasso šɨttə nʼäjam.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T24" id="Seg_3015" s="T1">šɨtə saːrum šɨttə qwäjgʼöt čisloɣondə aktʼäbərin ireɣəndo okkə tɨsʼäča (okkɨr) tʼädʼigʼöt ton muqtut saːrum pottə (poɣändə) mat qwassaŋ suːrulʼgu nʼärɨntaːj suːrulʼdi qun maːttə. </ta>
            <ta e="T30" id="Seg_3016" s="T24">nimdə maːdʼin čwäčin Мitrij Sidərɨn maːtʼtʼe. </ta>
            <ta e="T36" id="Seg_3017" s="T30">nadə äːsan äran, tʼäk äːdi ärat. </ta>
            <ta e="T40" id="Seg_3018" s="T36">man wättow ässan soː. </ta>
            <ta e="T51" id="Seg_3019" s="T40">naŋo soː äːss qanǯiwättə sunʼdʼeːwɨn nʼärɨn taj qajewɨn naːdəmnes čaːǯigu, qandɨmbäːs. </ta>
            <ta e="T57" id="Seg_3020" s="T51">i naːr lʼäɣa qwänbɨzattə man nʼärnän. </ta>
            <ta e="T61" id="Seg_3021" s="T57">täppɨlan nimdə Vasilij, Мaksim. </ta>
            <ta e="T66" id="Seg_3022" s="T61">Мaksimɨn kojandə nimdə man äːwɨlǯɨmbaw. </ta>
            <ta e="T70" id="Seg_3023" s="T66">tappɨla qwandɨmbattə tolʼdʼi wättɨm. </ta>
            <ta e="T73" id="Seg_3024" s="T70">täppɨlan tolʼdʼiwättə qandämba. </ta>
            <ta e="T79" id="Seg_3025" s="T73">taw dʼeːl kulʼdʼiŋ kɨkkɨzaŋ qweːdulugu nʼäjakazä. </ta>
            <ta e="T81" id="Seg_3026" s="T79">assə qweːduluaŋ. </ta>
            <ta e="T86" id="Seg_3027" s="T81">okkɨr nʼäjam kanaŋmo (kanalaw) muːdəjbɨzɨt. </ta>
            <ta e="T91" id="Seg_3028" s="T86">man manǯibɨzaw tɨttoɣɨn, assa qolʼdizaw. </ta>
            <ta e="T96" id="Seg_3029" s="T91">kanaŋmə qoːɨt šɨdəmdälǯi nʼäja kwäːrgoɣon. </ta>
            <ta e="T98" id="Seg_3030" s="T96">man qolʼdʼäw. </ta>
            <ta e="T102" id="Seg_3031" s="T98">täbɨm tʼätčigu assə äːsaŋ. </ta>
            <ta e="T106" id="Seg_3032" s="T102">naŋo täbɨm assə qwässaw. </ta>
            <ta e="T108" id="Seg_3033" s="T106">täp nʼäːrɣeːs. </ta>
            <ta e="T112" id="Seg_3034" s="T108">nʼärɣa nʼäjam assə tʼäǯiqwot. </ta>
            <ta e="T124" id="Seg_3035" s="T112">assä mittälʼe čwäčin nimdə Мitrij Sidərɨn i Lukanʼtʼin lʼewi maːttə panaluŋ qanǯow. </ta>
            <ta e="T131" id="Seg_3036" s="T124">na qanǯow mimbedi i qombädi man pajannä. </ta>
            <ta e="T135" id="Seg_3037" s="T131">wättə ässan aː soː. </ta>
            <ta e="T141" id="Seg_3038" s="T135">qwändi qulan muqqoɣonna qanǯiwättə assə qandɨmbɨs. </ta>
            <ta e="T143" id="Seg_3039" s="T141">üːdəmlʼe oldɨŋ. </ta>
            <ta e="T145" id="Seg_3040" s="T143">qanǯow panaluŋ. </ta>
            <ta e="T157" id="Seg_3041" s="T145">qailaw i panalumbädi qanǯongaw i qailaw moqqalʼe qwändaw nʼärɨn tobi tɨttɨ mɨsɨndə. </ta>
            <ta e="T168" id="Seg_3042" s="T157">tɨttɨ mɨsɨɣan pennaw tüːm, lakčenaw tɨdɨn molam, meːjaw oːmdɨssa kučasoti qoptəm. </ta>
            <ta e="T171" id="Seg_3043" s="T168">Madʼö sagɨŋ uttərɨŋ. </ta>
            <ta e="T178" id="Seg_3044" s="T171">meŋnan okkɨ täːrba kundar laːdiŋu panalumbodi qaːnǯim. </ta>
            <ta e="T182" id="Seg_3045" s="T178">meŋnan äːsattə nʼön kɨssɨnɨla. </ta>
            <ta e="T197" id="Seg_3046" s="T182">šɨttə saːrum šɨttə qwäjgʼöt šɨttə saːrum naːr qwäjgʼöt pin man laqqɨzaŋ, sagɨŋ praːiksaw onäŋ qanǯaw. </ta>
            <ta e="T204" id="Seg_3047" s="T197">mindɨn taːdərɨgu qailam (katomkoj) meŋga sätčim änǯiŋ. </ta>
            <ta e="T209" id="Seg_3048" s="T204">naŋo pin soːŋ assə qondəzaŋ. </ta>
            <ta e="T214" id="Seg_3049" s="T209">qanǯim meːkuzaw pin tʼönǯin ǯenno. </ta>
            <ta e="T219" id="Seg_3050" s="T214">okkɨrɨŋ pin kanaŋmɨ muːdɨjgu äzuŋ. </ta>
            <ta e="T222" id="Seg_3051" s="T219">šɨdɨŋ narɨŋ wäqsä. </ta>
            <ta e="T225" id="Seg_3052" s="T222">man assä lärumbɨzaŋ. </ta>
            <ta e="T233" id="Seg_3053" s="T225">man oneŋ siwwə oralbɨzaw kak kozjäin onäŋ tüːɣonäŋ. </ta>
            <ta e="T240" id="Seg_3054" s="T233">man manǯibɨsaw tɨtti makandə warɣə nʼärɨn tobi. </ta>
            <ta e="T249" id="Seg_3055" s="T240">man täːrbɨzaŋ: qardʼe nʼärnä čaːčenǯaŋ na qanǯi wättɨ sünʼdʼewɨn. </ta>
            <ta e="T255" id="Seg_3056" s="T249">sɨsaːrum narqwäj qarʼimɨɣɨn kundə assə taqqɨlbɨzaŋ. </ta>
            <ta e="T263" id="Seg_3057" s="T255">čaːǯigu soː jes äːrraj qanǯi wättoɨn qaj qandəmbɨs. </ta>
            <ta e="T272" id="Seg_3058" s="T263">okkɨr qwäjgʼöt časoɣondə tʼeːlɨn man äːsaŋ naːr suːrulʼdʼi lʼäɣalɨnnan. </ta>
            <ta e="T276" id="Seg_3059" s="T272">täppɨla nʼäjam assä qwatkuzattə. </ta>
            <ta e="T282" id="Seg_3060" s="T276">tüːlʼewlʼe surulʼdi maːttə man qwässaŋ peːmɨgu. </ta>
            <ta e="T294" id="Seg_3061" s="T282">na dʼel man qwätnaw okkə nʼäjam, naːt na säːɣääs, i okkə saŋgɨm. </ta>
            <ta e="T297" id="Seg_3062" s="T294">äːsan vaskrʼösnɨj tʼeːlɨt. </ta>
            <ta e="T299" id="Seg_3063" s="T297">šɨdəmdälǯi dʼel. </ta>
            <ta e="T303" id="Seg_3064" s="T299">peːmɨzaŋ qwässaw šɨttə nʼäjam. </ta>
            <ta e="T309" id="Seg_3065" s="T303">täpkiː äːsadi siːnʼe udɨ säːɣə qoːbʼi. </ta>
            <ta e="T312" id="Seg_3066" s="T309">siːtʼew assä aːndalba. </ta>
            <ta e="T317" id="Seg_3067" s="T312">naːrɨndʼelǯe dʼel äːsan soː tʼeːlɨt. </ta>
            <ta e="T321" id="Seg_3068" s="T317">na dʼel qaimnassä qwässaw. </ta>
            <ta e="T327" id="Seg_3069" s="T321">tʼelɨt äːsan, tʼeːlɨt tʼeːlɨmbədi man pajannä. </ta>
            <ta e="T336" id="Seg_3070" s="T327">na dʼel lʼäɣalaze okkə mɨɣɨn palʼdʼüzot nʼärɨn taj pojkɨndə. </ta>
            <ta e="T343" id="Seg_3071" s="T336">nɨtʼän me qwädʼizot pondə nägärlʼe oːnɨt niwlawɨt. </ta>
            <ta e="T350" id="Seg_3072" s="T343">a me nimwɨt Мadʼö, Мaksa i Вaska. </ta>
            <ta e="T359" id="Seg_3073" s="T350">taw dʼel me tassot naːgur tawut qumbädi äsäw Sämonɨm. </ta>
            <ta e="T364" id="Seg_3074" s="T359">meɣanɨt äːsan opsä assə soŋ. </ta>
            <ta e="T369" id="Seg_3075" s="T364">tättɨmdelǯi dʼel qarimɨɣɨn püm äːs. </ta>
            <ta e="T372" id="Seg_3076" s="T369">üːdəmɨn äːsan särrot. </ta>
            <ta e="T375" id="Seg_3077" s="T372">tʼeːlɨt sättšim äːss. </ta>
            <ta e="T378" id="Seg_3078" s="T375">palʼdʼügu toppɨla nuːnɨmbɨzattə. </ta>
            <ta e="T383" id="Seg_3079" s="T378">na dʼel tannaw naːr nʼäjam. </ta>
            <ta e="T392" id="Seg_3080" s="T383">sombɨlʼemdälǯi dʼel okkɨr lʼäɣa, nimdə täbɨn Wasilʼij, qwässɨ maːtqɨndə. </ta>
            <ta e="T397" id="Seg_3081" s="T392">naː dʼel tassaw tättə nʼäjam. </ta>
            <ta e="T403" id="Seg_3082" s="T397">muqtumdälǯi dʼel palʼdʼüzaŋ pʼüru nʼäran taj. </ta>
            <ta e="T411" id="Seg_3083" s="T403">äːrukkaŋ čaːǯizaŋ naŋo üŋulʼǯumbulʼe qaːɣə muːtolʼdʼenǯiŋ man kanaŋmə. </ta>
            <ta e="T414" id="Seg_3084" s="T411">nimdə täbɨn Äːdu. </ta>
            <ta e="T419" id="Seg_3085" s="T414">ündədʼäw (ündɨzäjaw) kanaŋmə muːdɨŋ nʼäjandə. </ta>
            <ta e="T427" id="Seg_3086" s="T419">mitänǯigu äːduni, täp soːŋ muːdɨŋ warɣə pirəga tɨttonda. </ta>
            <ta e="T431" id="Seg_3087" s="T427">manǯimbaŋ innä, assə qolʼdikwaw. </ta>
            <ta e="T444" id="Seg_3088" s="T431">nʼäjam lärčigu, qonɨŋ tʼätʼčizaŋ toːzopkazä, a nännä assə okkɨrɨŋ tʼätʼtšizaŋ šɨttə puːǯi tüːləsäzä. </ta>
            <ta e="T448" id="Seg_3089" s="T444">kɨkkɨzaŋ man täbɨm qoːlʼdigu. </ta>
            <ta e="T453" id="Seg_3090" s="T448">man täbɨm qolʼdʼäw tɨttən oloɣɨn. </ta>
            <ta e="T461" id="Seg_3091" s="T453">assə naːrɨŋ tättɨŋ tɨttɨn olondə tʼättšikuzaŋ i qwäːtʼäw. </ta>
            <ta e="T463" id="Seg_3092" s="T461">nʼegətšeːjaŋ, nɨŋejaŋ. </ta>
            <ta e="T467" id="Seg_3093" s="T463">assə kɨkkɨzaŋ qwädʼigu nʼäjam. </ta>
            <ta e="T479" id="Seg_3094" s="T467">manǯimbaŋ man matʼtʼöɣi lʼäɣaw, nimdə täbɨn äːdu, qaimdaka meːkut tʼäǯikutäj tɨːdɨn taːbaɣɨn. </ta>
            <ta e="T483" id="Seg_3095" s="T479">man tolʼdʼin nʼöɣɨnnaŋ täbɨnni. </ta>
            <ta e="T492" id="Seg_3096" s="T483">täp pɨŋgəlɨmbɨdi nʼäjam, kunnɨ man nɨŋɨzaŋ, nʼegɨtšizaŋ, täp ambat. </ta>
            <ta e="T499" id="Seg_3097" s="T492">qwädimbat meŋa moqqoɣɨ tobɨkalamdə (ɨ), pušqaj talʼdʼüm. </ta>
            <ta e="T505" id="Seg_3098" s="T499">me täpsä qwäduzoː, tɨttäruzoː aj miriuzo. </ta>
            <ta e="T511" id="Seg_3099" s="T505">na dʼel me tasso šɨttə nʼäjam. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_3100" s="T1">šɨtə</ta>
            <ta e="T3" id="Seg_3101" s="T2">*saːrum</ta>
            <ta e="T4" id="Seg_3102" s="T3">šɨttə</ta>
            <ta e="T5" id="Seg_3103" s="T4">qwäj-köt</ta>
            <ta e="T6" id="Seg_3104" s="T5">čislo-ɣondə</ta>
            <ta e="T7" id="Seg_3105" s="T6">aktʼäbər-i-n</ta>
            <ta e="T8" id="Seg_3106" s="T7">ire-ɣəndo</ta>
            <ta e="T9" id="Seg_3107" s="T8">okkə</ta>
            <ta e="T10" id="Seg_3108" s="T9">tɨsʼäča</ta>
            <ta e="T11" id="Seg_3109" s="T10">okkɨr</ta>
            <ta e="T12" id="Seg_3110" s="T11">tʼädʼigʼöt</ta>
            <ta e="T13" id="Seg_3111" s="T12">ton</ta>
            <ta e="T14" id="Seg_3112" s="T13">muqtut</ta>
            <ta e="T15" id="Seg_3113" s="T14">*saːrum</ta>
            <ta e="T16" id="Seg_3114" s="T15">po-ttə</ta>
            <ta e="T17" id="Seg_3115" s="T16">po-ɣändə</ta>
            <ta e="T18" id="Seg_3116" s="T17">mat</ta>
            <ta e="T19" id="Seg_3117" s="T18">qwas-sa-ŋ</ta>
            <ta e="T20" id="Seg_3118" s="T19">suːru-lʼ-gu</ta>
            <ta e="T21" id="Seg_3119" s="T20">nʼärɨ-n-taːj</ta>
            <ta e="T22" id="Seg_3120" s="T21">suːru-lʼdi</ta>
            <ta e="T23" id="Seg_3121" s="T22">qu-n</ta>
            <ta e="T24" id="Seg_3122" s="T23">maːt-tə</ta>
            <ta e="T25" id="Seg_3123" s="T24">nim-də</ta>
            <ta e="T26" id="Seg_3124" s="T25">maːdʼi-n</ta>
            <ta e="T27" id="Seg_3125" s="T26">čwäč-i-n</ta>
            <ta e="T28" id="Seg_3126" s="T27">Мitrij</ta>
            <ta e="T29" id="Seg_3127" s="T28">Sidər-ɨ-n</ta>
            <ta e="T30" id="Seg_3128" s="T29">maːtʼtʼe</ta>
            <ta e="T31" id="Seg_3129" s="T30">na-də</ta>
            <ta e="T32" id="Seg_3130" s="T31">äː-sa-n</ta>
            <ta e="T33" id="Seg_3131" s="T32">ära-n</ta>
            <ta e="T34" id="Seg_3132" s="T33">tʼäk</ta>
            <ta e="T35" id="Seg_3133" s="T34">äːdi</ta>
            <ta e="T36" id="Seg_3134" s="T35">ära-t</ta>
            <ta e="T37" id="Seg_3135" s="T36">man</ta>
            <ta e="T38" id="Seg_3136" s="T37">wätto-w</ta>
            <ta e="T39" id="Seg_3137" s="T38">ä-ssa-ŋ</ta>
            <ta e="T40" id="Seg_3138" s="T39">soː</ta>
            <ta e="T41" id="Seg_3139" s="T40">naŋo</ta>
            <ta e="T42" id="Seg_3140" s="T41">soː</ta>
            <ta e="T43" id="Seg_3141" s="T42">äː-ss</ta>
            <ta e="T44" id="Seg_3142" s="T43">qanǯi-wättə</ta>
            <ta e="T45" id="Seg_3143" s="T44">sunʼdʼeː-wɨn</ta>
            <ta e="T46" id="Seg_3144" s="T45">nʼärɨ-n</ta>
            <ta e="T47" id="Seg_3145" s="T46">taj</ta>
            <ta e="T48" id="Seg_3146" s="T47">qaj-e-wɨn</ta>
            <ta e="T49" id="Seg_3147" s="T48">naːdə-m-ne-s</ta>
            <ta e="T50" id="Seg_3148" s="T49">čaːǯi-gu</ta>
            <ta e="T51" id="Seg_3149" s="T50">qandɨ-mbäː-s</ta>
            <ta e="T52" id="Seg_3150" s="T51">i</ta>
            <ta e="T53" id="Seg_3151" s="T52">naːr</ta>
            <ta e="T54" id="Seg_3152" s="T53">lʼäɣa</ta>
            <ta e="T55" id="Seg_3153" s="T54">qwän-bɨ-za-ttə</ta>
            <ta e="T56" id="Seg_3154" s="T55">man</ta>
            <ta e="T57" id="Seg_3155" s="T56">nʼärnä-n</ta>
            <ta e="T58" id="Seg_3156" s="T57">täpp-ɨ-la-n</ta>
            <ta e="T59" id="Seg_3157" s="T58">nim-də</ta>
            <ta e="T60" id="Seg_3158" s="T59">Vasilij</ta>
            <ta e="T61" id="Seg_3159" s="T60">Мaksim</ta>
            <ta e="T62" id="Seg_3160" s="T61">Мaksim-ɨ-n</ta>
            <ta e="T63" id="Seg_3161" s="T62">koja-n-də</ta>
            <ta e="T64" id="Seg_3162" s="T63">nim-də</ta>
            <ta e="T65" id="Seg_3163" s="T64">man</ta>
            <ta e="T66" id="Seg_3164" s="T65">äːwɨlǯɨ-mba-w</ta>
            <ta e="T67" id="Seg_3165" s="T66">tapp-ɨ-la</ta>
            <ta e="T68" id="Seg_3166" s="T67">qwan-dɨ-mba-ttə</ta>
            <ta e="T69" id="Seg_3167" s="T68">tolʼdʼi</ta>
            <ta e="T70" id="Seg_3168" s="T69">wättɨ-m</ta>
            <ta e="T71" id="Seg_3169" s="T70">täpp-ɨ-la-n</ta>
            <ta e="T72" id="Seg_3170" s="T71">tolʼdʼi-wättə</ta>
            <ta e="T73" id="Seg_3171" s="T72">qandä-mba</ta>
            <ta e="T74" id="Seg_3172" s="T73">taw</ta>
            <ta e="T75" id="Seg_3173" s="T74">dʼeːl</ta>
            <ta e="T76" id="Seg_3174" s="T75">kulʼdʼi-ŋ</ta>
            <ta e="T77" id="Seg_3175" s="T76">kɨkkɨ-za-ŋ</ta>
            <ta e="T78" id="Seg_3176" s="T77">qweːdu-lu-gu</ta>
            <ta e="T79" id="Seg_3177" s="T78">nʼäja-ka-zä</ta>
            <ta e="T80" id="Seg_3178" s="T79">assə</ta>
            <ta e="T81" id="Seg_3179" s="T80">qweːdu-lu-a-ŋ</ta>
            <ta e="T82" id="Seg_3180" s="T81">okkɨr</ta>
            <ta e="T83" id="Seg_3181" s="T82">nʼäja-m</ta>
            <ta e="T84" id="Seg_3182" s="T83">kanaŋ-mo</ta>
            <ta e="T85" id="Seg_3183" s="T84">kana-la-w</ta>
            <ta e="T86" id="Seg_3184" s="T85">muːdəj-bɨ-zɨ-t</ta>
            <ta e="T87" id="Seg_3185" s="T86">man</ta>
            <ta e="T88" id="Seg_3186" s="T87">manǯi-bɨ-za-w</ta>
            <ta e="T89" id="Seg_3187" s="T88">tɨtto-ɣɨn</ta>
            <ta e="T90" id="Seg_3188" s="T89">assa</ta>
            <ta e="T91" id="Seg_3189" s="T90">qolʼdi-za-w</ta>
            <ta e="T92" id="Seg_3190" s="T91">kanaŋ-mə</ta>
            <ta e="T93" id="Seg_3191" s="T92">qoː-ɨ-t</ta>
            <ta e="T94" id="Seg_3192" s="T93">šɨdə-mdälǯi</ta>
            <ta e="T95" id="Seg_3193" s="T94">nʼäja</ta>
            <ta e="T96" id="Seg_3194" s="T95">kwäːrgo-ɣon</ta>
            <ta e="T97" id="Seg_3195" s="T96">man</ta>
            <ta e="T98" id="Seg_3196" s="T97">qolʼdʼä-w</ta>
            <ta e="T99" id="Seg_3197" s="T98">täb-ɨ-m</ta>
            <ta e="T100" id="Seg_3198" s="T99">tʼätči-gu</ta>
            <ta e="T101" id="Seg_3199" s="T100">assə</ta>
            <ta e="T102" id="Seg_3200" s="T101">äː-sa-ŋ</ta>
            <ta e="T103" id="Seg_3201" s="T102">naŋo</ta>
            <ta e="T104" id="Seg_3202" s="T103">täb-ɨ-m</ta>
            <ta e="T105" id="Seg_3203" s="T104">assə</ta>
            <ta e="T106" id="Seg_3204" s="T105">qwäs-sa-w</ta>
            <ta e="T107" id="Seg_3205" s="T106">täp</ta>
            <ta e="T108" id="Seg_3206" s="T107">nʼäːrɣeː-s</ta>
            <ta e="T109" id="Seg_3207" s="T108">nʼärɣa</ta>
            <ta e="T110" id="Seg_3208" s="T109">nʼäja-m</ta>
            <ta e="T111" id="Seg_3209" s="T110">assə</ta>
            <ta e="T112" id="Seg_3210" s="T111">tʼäǯi-q-wot</ta>
            <ta e="T113" id="Seg_3211" s="T112">assä</ta>
            <ta e="T114" id="Seg_3212" s="T113">mittä-lʼe</ta>
            <ta e="T115" id="Seg_3213" s="T114">čwäč-i-n</ta>
            <ta e="T116" id="Seg_3214" s="T115">nim-də</ta>
            <ta e="T117" id="Seg_3215" s="T116">Мitrij</ta>
            <ta e="T118" id="Seg_3216" s="T117">Sidər-ɨ-n</ta>
            <ta e="T119" id="Seg_3217" s="T118">i</ta>
            <ta e="T120" id="Seg_3218" s="T119">Lukanʼtʼi-n</ta>
            <ta e="T121" id="Seg_3219" s="T120">lʼewi</ta>
            <ta e="T122" id="Seg_3220" s="T121">maːt-tə</ta>
            <ta e="T123" id="Seg_3221" s="T122">panalu-ŋ</ta>
            <ta e="T124" id="Seg_3222" s="T123">qanǯo-w</ta>
            <ta e="T125" id="Seg_3223" s="T124">na</ta>
            <ta e="T126" id="Seg_3224" s="T125">qanǯo-w</ta>
            <ta e="T127" id="Seg_3225" s="T126">mi-mbedi</ta>
            <ta e="T128" id="Seg_3226" s="T127">i</ta>
            <ta e="T129" id="Seg_3227" s="T128">qo-mbädi</ta>
            <ta e="T130" id="Seg_3228" s="T129">man</ta>
            <ta e="T131" id="Seg_3229" s="T130">paja-n-nä</ta>
            <ta e="T132" id="Seg_3230" s="T131">wättə</ta>
            <ta e="T133" id="Seg_3231" s="T132">ä-ssa-ŋ</ta>
            <ta e="T134" id="Seg_3232" s="T133">aː</ta>
            <ta e="T135" id="Seg_3233" s="T134">soː</ta>
            <ta e="T136" id="Seg_3234" s="T135">qwä-ndi</ta>
            <ta e="T137" id="Seg_3235" s="T136">qu-la-n</ta>
            <ta e="T138" id="Seg_3236" s="T137">muqqo-ɣonna</ta>
            <ta e="T139" id="Seg_3237" s="T138">qanǯi-wättə</ta>
            <ta e="T140" id="Seg_3238" s="T139">assə</ta>
            <ta e="T141" id="Seg_3239" s="T140">qandɨ-mbɨ-s</ta>
            <ta e="T142" id="Seg_3240" s="T141">üːdə-m-lʼe</ta>
            <ta e="T143" id="Seg_3241" s="T142">oldɨ-n</ta>
            <ta e="T144" id="Seg_3242" s="T143">qanǯo-w</ta>
            <ta e="T145" id="Seg_3243" s="T144">panalu-ŋ</ta>
            <ta e="T146" id="Seg_3244" s="T145">qai-la-w</ta>
            <ta e="T147" id="Seg_3245" s="T146">i</ta>
            <ta e="T148" id="Seg_3246" s="T147">panalu-mbädi</ta>
            <ta e="T149" id="Seg_3247" s="T148">qanǯo-nga-w</ta>
            <ta e="T150" id="Seg_3248" s="T149">i</ta>
            <ta e="T151" id="Seg_3249" s="T150">qai-la-w</ta>
            <ta e="T152" id="Seg_3250" s="T151">moqqa-lʼe</ta>
            <ta e="T153" id="Seg_3251" s="T152">qwän-da-w</ta>
            <ta e="T154" id="Seg_3252" s="T153">nʼärɨ-n</ta>
            <ta e="T155" id="Seg_3253" s="T154">tobi</ta>
            <ta e="T156" id="Seg_3254" s="T155">tɨttɨ</ta>
            <ta e="T157" id="Seg_3255" s="T156">mɨs-ɨ-ndə</ta>
            <ta e="T158" id="Seg_3256" s="T157">tɨttɨ</ta>
            <ta e="T159" id="Seg_3257" s="T158">mɨs-ɨ-ɣan</ta>
            <ta e="T160" id="Seg_3258" s="T159">pen-na-w</ta>
            <ta e="T161" id="Seg_3259" s="T160">tüː-m</ta>
            <ta e="T162" id="Seg_3260" s="T161">lakče-na-w</ta>
            <ta e="T163" id="Seg_3261" s="T162">tɨdɨ-n</ta>
            <ta e="T164" id="Seg_3262" s="T163">mo-la-m</ta>
            <ta e="T165" id="Seg_3263" s="T164">meː-ja-w</ta>
            <ta e="T166" id="Seg_3264" s="T165">oːmdɨ-ssa</ta>
            <ta e="T167" id="Seg_3265" s="T166">kuča-so-ti</ta>
            <ta e="T168" id="Seg_3266" s="T167">qoptə-m</ta>
            <ta e="T169" id="Seg_3267" s="T168">Madʼö</ta>
            <ta e="T170" id="Seg_3268" s="T169">sagɨ-ŋ</ta>
            <ta e="T171" id="Seg_3269" s="T170">uttə-r-ɨ-n</ta>
            <ta e="T172" id="Seg_3270" s="T171">meŋ-nan</ta>
            <ta e="T173" id="Seg_3271" s="T172">okkɨ</ta>
            <ta e="T174" id="Seg_3272" s="T173">täːrba</ta>
            <ta e="T175" id="Seg_3273" s="T174">kundar</ta>
            <ta e="T176" id="Seg_3274" s="T175">laːdi-ŋu</ta>
            <ta e="T177" id="Seg_3275" s="T176">panalu-mbodi</ta>
            <ta e="T178" id="Seg_3276" s="T177">qaːnǯi-m</ta>
            <ta e="T179" id="Seg_3277" s="T178">meŋ-nan</ta>
            <ta e="T180" id="Seg_3278" s="T179">äː-sa-ttə</ta>
            <ta e="T181" id="Seg_3279" s="T180">nʼö-n</ta>
            <ta e="T182" id="Seg_3280" s="T181">kɨssɨn-ɨ-la</ta>
            <ta e="T183" id="Seg_3281" s="T182">šɨttə</ta>
            <ta e="T184" id="Seg_3282" s="T183">*saːrum</ta>
            <ta e="T185" id="Seg_3283" s="T184">šɨttə</ta>
            <ta e="T186" id="Seg_3284" s="T185">qwäj-gʼöt</ta>
            <ta e="T187" id="Seg_3285" s="T186">šɨttə</ta>
            <ta e="T188" id="Seg_3286" s="T187">*saːrum</ta>
            <ta e="T189" id="Seg_3287" s="T188">naːr</ta>
            <ta e="T190" id="Seg_3288" s="T189">qwäj-gʼöt</ta>
            <ta e="T191" id="Seg_3289" s="T190">pi-n</ta>
            <ta e="T192" id="Seg_3290" s="T191">man</ta>
            <ta e="T193" id="Seg_3291" s="T192">laqqɨ-za-ŋ</ta>
            <ta e="T194" id="Seg_3292" s="T193">sagɨ-ŋ</ta>
            <ta e="T195" id="Seg_3293" s="T194">praːi-k-sa-w</ta>
            <ta e="T196" id="Seg_3294" s="T195">onäŋ</ta>
            <ta e="T197" id="Seg_3295" s="T196">qanǯa-w</ta>
            <ta e="T198" id="Seg_3296" s="T197">mindɨn</ta>
            <ta e="T199" id="Seg_3297" s="T198">taːdə-r-ɨ-gu</ta>
            <ta e="T200" id="Seg_3298" s="T199">qai-la-m</ta>
            <ta e="T201" id="Seg_3299" s="T200">katomko-j</ta>
            <ta e="T202" id="Seg_3300" s="T201">meŋga</ta>
            <ta e="T203" id="Seg_3301" s="T202">sätčim</ta>
            <ta e="T204" id="Seg_3302" s="T203">ä-nǯi-ŋ</ta>
            <ta e="T205" id="Seg_3303" s="T204">naŋo</ta>
            <ta e="T206" id="Seg_3304" s="T205">pi-n</ta>
            <ta e="T207" id="Seg_3305" s="T206">soː-ŋ</ta>
            <ta e="T208" id="Seg_3306" s="T207">assə</ta>
            <ta e="T209" id="Seg_3307" s="T208">qondə-za-n</ta>
            <ta e="T210" id="Seg_3308" s="T209">qanǯi-m</ta>
            <ta e="T211" id="Seg_3309" s="T210">meː-ku-za-w</ta>
            <ta e="T212" id="Seg_3310" s="T211">pi-n</ta>
            <ta e="T213" id="Seg_3311" s="T212">tʼönǯi-n</ta>
            <ta e="T214" id="Seg_3312" s="T213">ǯenno</ta>
            <ta e="T215" id="Seg_3313" s="T214">okkɨr-ɨ-ŋ</ta>
            <ta e="T216" id="Seg_3314" s="T215">pi-n</ta>
            <ta e="T217" id="Seg_3315" s="T216">kanaŋ-mɨ</ta>
            <ta e="T218" id="Seg_3316" s="T217">muːdɨj-gu</ta>
            <ta e="T219" id="Seg_3317" s="T218">ä-zu-ŋ</ta>
            <ta e="T220" id="Seg_3318" s="T219">šɨdɨ-ŋ</ta>
            <ta e="T221" id="Seg_3319" s="T220">narɨ-ŋ</ta>
            <ta e="T222" id="Seg_3320" s="T221">wäq-sä</ta>
            <ta e="T223" id="Seg_3321" s="T222">man</ta>
            <ta e="T224" id="Seg_3322" s="T223">assä</ta>
            <ta e="T225" id="Seg_3323" s="T224">läru-mbɨ-za-ŋ</ta>
            <ta e="T226" id="Seg_3324" s="T225">man</ta>
            <ta e="T227" id="Seg_3325" s="T226">oneŋ</ta>
            <ta e="T228" id="Seg_3326" s="T227">*siw-wə</ta>
            <ta e="T229" id="Seg_3327" s="T228">oral-bɨ-za-w</ta>
            <ta e="T230" id="Seg_3328" s="T229">kak</ta>
            <ta e="T231" id="Seg_3329" s="T230">kozjäin</ta>
            <ta e="T232" id="Seg_3330" s="T231">onäŋ</ta>
            <ta e="T233" id="Seg_3331" s="T232">tüː-ɣonäŋ</ta>
            <ta e="T234" id="Seg_3332" s="T233">man</ta>
            <ta e="T235" id="Seg_3333" s="T234">manǯi-bɨ-sa-w</ta>
            <ta e="T236" id="Seg_3334" s="T235">tɨtti</ta>
            <ta e="T237" id="Seg_3335" s="T236">maka-ndə</ta>
            <ta e="T238" id="Seg_3336" s="T237">warɣə</ta>
            <ta e="T239" id="Seg_3337" s="T238">nʼärɨ-n</ta>
            <ta e="T240" id="Seg_3338" s="T239">tobi</ta>
            <ta e="T241" id="Seg_3339" s="T240">man</ta>
            <ta e="T242" id="Seg_3340" s="T241">täːrbɨ-za-ŋ</ta>
            <ta e="T243" id="Seg_3341" s="T242">qardʼe</ta>
            <ta e="T244" id="Seg_3342" s="T243">nʼärnä</ta>
            <ta e="T245" id="Seg_3343" s="T244">čaːče-nǯa-ŋ</ta>
            <ta e="T246" id="Seg_3344" s="T245">na</ta>
            <ta e="T247" id="Seg_3345" s="T246">qanǯi</ta>
            <ta e="T248" id="Seg_3346" s="T247">wättɨ</ta>
            <ta e="T249" id="Seg_3347" s="T248">sünʼdʼe-wɨn</ta>
            <ta e="T250" id="Seg_3348" s="T249">sɨsaːrum</ta>
            <ta e="T251" id="Seg_3349" s="T250">narq-wäj</ta>
            <ta e="T252" id="Seg_3350" s="T251">qarʼi-mɨ-ɣɨn</ta>
            <ta e="T253" id="Seg_3351" s="T252">kundə</ta>
            <ta e="T254" id="Seg_3352" s="T253">assə</ta>
            <ta e="T255" id="Seg_3353" s="T254">taqqɨ-le-bɨ-za-ŋ</ta>
            <ta e="T256" id="Seg_3354" s="T255">čaːǯi-gu</ta>
            <ta e="T257" id="Seg_3355" s="T256">soː</ta>
            <ta e="T258" id="Seg_3356" s="T257">je-s</ta>
            <ta e="T259" id="Seg_3357" s="T258">äːrra-j</ta>
            <ta e="T260" id="Seg_3358" s="T259">qanǯi</ta>
            <ta e="T261" id="Seg_3359" s="T260">wätto-ɨn</ta>
            <ta e="T262" id="Seg_3360" s="T261">qaj</ta>
            <ta e="T263" id="Seg_3361" s="T262">qandə-mbɨ-s</ta>
            <ta e="T264" id="Seg_3362" s="T263">okkɨr</ta>
            <ta e="T265" id="Seg_3363" s="T264">qwäj-gʼöt</ta>
            <ta e="T266" id="Seg_3364" s="T265">časo-ɣondə</ta>
            <ta e="T267" id="Seg_3365" s="T266">tʼeːlɨ-n</ta>
            <ta e="T268" id="Seg_3366" s="T267">man</ta>
            <ta e="T269" id="Seg_3367" s="T268">äː-sa-ŋ</ta>
            <ta e="T270" id="Seg_3368" s="T269">naːr</ta>
            <ta e="T271" id="Seg_3369" s="T270">suːru-lʼdʼi</ta>
            <ta e="T272" id="Seg_3370" s="T271">lʼäɣa-lɨ-n-nan</ta>
            <ta e="T273" id="Seg_3371" s="T272">täpp-ɨ-la</ta>
            <ta e="T274" id="Seg_3372" s="T273">nʼäja-m</ta>
            <ta e="T275" id="Seg_3373" s="T274">assä</ta>
            <ta e="T276" id="Seg_3374" s="T275">qwat-ku-za-ttə</ta>
            <ta e="T277" id="Seg_3375" s="T276">tüː-lʼewlʼe</ta>
            <ta e="T278" id="Seg_3376" s="T277">suru-lʼdi</ta>
            <ta e="T279" id="Seg_3377" s="T278">maːt-tə</ta>
            <ta e="T280" id="Seg_3378" s="T279">man</ta>
            <ta e="T281" id="Seg_3379" s="T280">qwäs-sa-ŋ</ta>
            <ta e="T282" id="Seg_3380" s="T281">peːmɨ-gu</ta>
            <ta e="T283" id="Seg_3381" s="T282">na</ta>
            <ta e="T284" id="Seg_3382" s="T283">dʼel</ta>
            <ta e="T285" id="Seg_3383" s="T284">man</ta>
            <ta e="T286" id="Seg_3384" s="T285">qwät-na-w</ta>
            <ta e="T287" id="Seg_3385" s="T286">okkə</ta>
            <ta e="T288" id="Seg_3386" s="T287">nʼäja-m</ta>
            <ta e="T289" id="Seg_3387" s="T288">naːt</ta>
            <ta e="T290" id="Seg_3388" s="T289">na</ta>
            <ta e="T291" id="Seg_3389" s="T290">säːɣä-ä-s</ta>
            <ta e="T292" id="Seg_3390" s="T291">i</ta>
            <ta e="T293" id="Seg_3391" s="T292">okkə</ta>
            <ta e="T294" id="Seg_3392" s="T293">saŋgɨ-m</ta>
            <ta e="T295" id="Seg_3393" s="T294">äː-sa-ŋ</ta>
            <ta e="T296" id="Seg_3394" s="T295">vaskrʼösnɨj</ta>
            <ta e="T297" id="Seg_3395" s="T296">tʼeːlɨ-tɨ</ta>
            <ta e="T298" id="Seg_3396" s="T297">šɨdə-mdälǯi</ta>
            <ta e="T299" id="Seg_3397" s="T298">dʼel</ta>
            <ta e="T300" id="Seg_3398" s="T299">peːmɨ-za-ŋ</ta>
            <ta e="T301" id="Seg_3399" s="T300">qwäs-sa-w</ta>
            <ta e="T302" id="Seg_3400" s="T301">šɨttə</ta>
            <ta e="T303" id="Seg_3401" s="T302">nʼäja-m</ta>
            <ta e="T304" id="Seg_3402" s="T303">täp-kiː</ta>
            <ta e="T305" id="Seg_3403" s="T304">äː-sa-di</ta>
            <ta e="T306" id="Seg_3404" s="T305">siːnʼe</ta>
            <ta e="T307" id="Seg_3405" s="T306">udɨ</ta>
            <ta e="T308" id="Seg_3406" s="T307">säːɣə</ta>
            <ta e="T309" id="Seg_3407" s="T308">qoːbʼi</ta>
            <ta e="T310" id="Seg_3408" s="T309">siːtʼe-w</ta>
            <ta e="T311" id="Seg_3409" s="T310">assä</ta>
            <ta e="T312" id="Seg_3410" s="T311">aːnda-l-ba</ta>
            <ta e="T313" id="Seg_3411" s="T312">naːrɨ-ndʼelǯe</ta>
            <ta e="T314" id="Seg_3412" s="T313">dʼel</ta>
            <ta e="T315" id="Seg_3413" s="T314">äː-sa-ŋ</ta>
            <ta e="T316" id="Seg_3414" s="T315">soː</ta>
            <ta e="T317" id="Seg_3415" s="T316">tʼeːlɨ-tɨ</ta>
            <ta e="T318" id="Seg_3416" s="T317">na</ta>
            <ta e="T319" id="Seg_3417" s="T318">dʼel</ta>
            <ta e="T320" id="Seg_3418" s="T319">qai-m-na-ssä</ta>
            <ta e="T321" id="Seg_3419" s="T320">qwäs-sa-w</ta>
            <ta e="T322" id="Seg_3420" s="T321">tʼelɨ-tɨ</ta>
            <ta e="T323" id="Seg_3421" s="T322">äː-sa-ŋ</ta>
            <ta e="T324" id="Seg_3422" s="T323">tʼeːlɨ-tɨ</ta>
            <ta e="T325" id="Seg_3423" s="T324">tʼeːlɨ-m-bədi</ta>
            <ta e="T326" id="Seg_3424" s="T325">man</ta>
            <ta e="T327" id="Seg_3425" s="T326">paja-nnä</ta>
            <ta e="T328" id="Seg_3426" s="T327">na</ta>
            <ta e="T329" id="Seg_3427" s="T328">dʼel</ta>
            <ta e="T330" id="Seg_3428" s="T329">lʼäɣa-la-ze</ta>
            <ta e="T331" id="Seg_3429" s="T330">okkə</ta>
            <ta e="T332" id="Seg_3430" s="T331">mɨ-ɣɨn</ta>
            <ta e="T333" id="Seg_3431" s="T332">palʼdʼü-zo-t</ta>
            <ta e="T334" id="Seg_3432" s="T333">nʼärɨ-n</ta>
            <ta e="T335" id="Seg_3433" s="T334">taj</ta>
            <ta e="T336" id="Seg_3434" s="T335">pojkɨ-ndə</ta>
            <ta e="T337" id="Seg_3435" s="T336">nɨtʼä-n</ta>
            <ta e="T338" id="Seg_3436" s="T337">me</ta>
            <ta e="T339" id="Seg_3437" s="T338">qwädʼi-zo-t</ta>
            <ta e="T340" id="Seg_3438" s="T339">po-ndə</ta>
            <ta e="T341" id="Seg_3439" s="T340">nägä-r-lʼe</ta>
            <ta e="T342" id="Seg_3440" s="T341">oːnɨt</ta>
            <ta e="T343" id="Seg_3441" s="T342">niw-la-wɨt</ta>
            <ta e="T344" id="Seg_3442" s="T343">a</ta>
            <ta e="T345" id="Seg_3443" s="T344">me</ta>
            <ta e="T346" id="Seg_3444" s="T345">nim-wɨt</ta>
            <ta e="T347" id="Seg_3445" s="T346">Мadʼö</ta>
            <ta e="T348" id="Seg_3446" s="T347">Мaksa</ta>
            <ta e="T349" id="Seg_3447" s="T348">i</ta>
            <ta e="T350" id="Seg_3448" s="T349">Вaska</ta>
            <ta e="T351" id="Seg_3449" s="T350">taw</ta>
            <ta e="T352" id="Seg_3450" s="T351">dʼel</ta>
            <ta e="T353" id="Seg_3451" s="T352">me</ta>
            <ta e="T354" id="Seg_3452" s="T353">tas-so-t</ta>
            <ta e="T355" id="Seg_3453" s="T354">naːgur</ta>
            <ta e="T356" id="Seg_3454" s="T355">ta-wu-t</ta>
            <ta e="T357" id="Seg_3455" s="T356">qu-mbädi</ta>
            <ta e="T358" id="Seg_3456" s="T357">äsä-w</ta>
            <ta e="T359" id="Seg_3457" s="T358">Sämon-ɨ-m</ta>
            <ta e="T360" id="Seg_3458" s="T359">meɣanɨt</ta>
            <ta e="T361" id="Seg_3459" s="T360">äː-sa-ŋ</ta>
            <ta e="T362" id="Seg_3460" s="T361">opsä</ta>
            <ta e="T363" id="Seg_3461" s="T362">assə</ta>
            <ta e="T364" id="Seg_3462" s="T363">so-ŋ</ta>
            <ta e="T365" id="Seg_3463" s="T364">tättɨ-mdelǯi</ta>
            <ta e="T366" id="Seg_3464" s="T365">dʼel</ta>
            <ta e="T367" id="Seg_3465" s="T366">qari-mɨɣɨn</ta>
            <ta e="T368" id="Seg_3466" s="T367">püm</ta>
            <ta e="T369" id="Seg_3467" s="T368">äː-s</ta>
            <ta e="T370" id="Seg_3468" s="T369">üːdə-mɨn</ta>
            <ta e="T371" id="Seg_3469" s="T370">äː-sa-n</ta>
            <ta e="T372" id="Seg_3470" s="T371">särro-tɨ</ta>
            <ta e="T373" id="Seg_3471" s="T372">tʼeːlɨ-tɨ</ta>
            <ta e="T374" id="Seg_3472" s="T373">sätčim</ta>
            <ta e="T375" id="Seg_3473" s="T374">äː-ss</ta>
            <ta e="T376" id="Seg_3474" s="T375">palʼdʼü-gu</ta>
            <ta e="T377" id="Seg_3475" s="T376">toppɨ-la</ta>
            <ta e="T378" id="Seg_3476" s="T377">nuːnɨ-mbɨ-za-ttə</ta>
            <ta e="T379" id="Seg_3477" s="T378">na</ta>
            <ta e="T380" id="Seg_3478" s="T379">dʼel</ta>
            <ta e="T381" id="Seg_3479" s="T380">tan-na-w</ta>
            <ta e="T382" id="Seg_3480" s="T381">naːr</ta>
            <ta e="T383" id="Seg_3481" s="T382">nʼäja-m</ta>
            <ta e="T384" id="Seg_3482" s="T383">sombɨlʼe-mdälǯi</ta>
            <ta e="T385" id="Seg_3483" s="T384">dʼel</ta>
            <ta e="T386" id="Seg_3484" s="T385">okkɨr</ta>
            <ta e="T387" id="Seg_3485" s="T386">lʼäɣa</ta>
            <ta e="T388" id="Seg_3486" s="T387">nim-də</ta>
            <ta e="T389" id="Seg_3487" s="T388">täb-ɨ-n</ta>
            <ta e="T390" id="Seg_3488" s="T389">Wasilʼij</ta>
            <ta e="T391" id="Seg_3489" s="T390">qwäs-sɨ</ta>
            <ta e="T392" id="Seg_3490" s="T391">maːt-qɨndə</ta>
            <ta e="T393" id="Seg_3491" s="T392">naː</ta>
            <ta e="T394" id="Seg_3492" s="T393">dʼel</ta>
            <ta e="T395" id="Seg_3493" s="T394">tas-sa-w</ta>
            <ta e="T396" id="Seg_3494" s="T395">tättə</ta>
            <ta e="T397" id="Seg_3495" s="T396">nʼäja-m</ta>
            <ta e="T398" id="Seg_3496" s="T397">muqtu-mdälǯi</ta>
            <ta e="T399" id="Seg_3497" s="T398">dʼel</ta>
            <ta e="T400" id="Seg_3498" s="T399">palʼdʼü-za-ŋ</ta>
            <ta e="T401" id="Seg_3499" s="T400">pʼüru</ta>
            <ta e="T402" id="Seg_3500" s="T401">nʼära-n</ta>
            <ta e="T403" id="Seg_3501" s="T402">taj</ta>
            <ta e="T404" id="Seg_3502" s="T403">äːrukka-ŋ</ta>
            <ta e="T405" id="Seg_3503" s="T404">čaːǯi-za-ŋ</ta>
            <ta e="T406" id="Seg_3504" s="T405">naŋo</ta>
            <ta e="T407" id="Seg_3505" s="T406">üŋulʼǯu-mbu-lʼe</ta>
            <ta e="T408" id="Seg_3506" s="T407">qaːɣə</ta>
            <ta e="T409" id="Seg_3507" s="T408">muːt-o-lʼdʼe-nǯi-ŋ</ta>
            <ta e="T410" id="Seg_3508" s="T409">man</ta>
            <ta e="T411" id="Seg_3509" s="T410">kanaŋ-mə</ta>
            <ta e="T412" id="Seg_3510" s="T411">nim-də</ta>
            <ta e="T413" id="Seg_3511" s="T412">täb-ɨ-n</ta>
            <ta e="T414" id="Seg_3512" s="T413">äːdu</ta>
            <ta e="T415" id="Seg_3513" s="T414">ündə-dʼä-w</ta>
            <ta e="T416" id="Seg_3514" s="T415">ündɨ-zä-ja-w</ta>
            <ta e="T417" id="Seg_3515" s="T416">kanaŋ-mə</ta>
            <ta e="T418" id="Seg_3516" s="T417">muːd-ɨ-ŋ</ta>
            <ta e="T419" id="Seg_3517" s="T418">nʼäja-ndə</ta>
            <ta e="T420" id="Seg_3518" s="T419">mitä-nǯi-gu</ta>
            <ta e="T421" id="Seg_3519" s="T420">äːdu-ni</ta>
            <ta e="T422" id="Seg_3520" s="T421">täp</ta>
            <ta e="T423" id="Seg_3521" s="T422">soː-ŋ</ta>
            <ta e="T424" id="Seg_3522" s="T423">muːd-ɨ-ŋ</ta>
            <ta e="T425" id="Seg_3523" s="T424">warɣə</ta>
            <ta e="T426" id="Seg_3524" s="T425">pirəga</ta>
            <ta e="T427" id="Seg_3525" s="T426">tɨtto-nda</ta>
            <ta e="T428" id="Seg_3526" s="T427">manǯi-mba-ŋ</ta>
            <ta e="T429" id="Seg_3527" s="T428">innä</ta>
            <ta e="T430" id="Seg_3528" s="T429">assə</ta>
            <ta e="T431" id="Seg_3529" s="T430">qolʼdi-k-wa-w</ta>
            <ta e="T432" id="Seg_3530" s="T431">nʼäja-m</ta>
            <ta e="T433" id="Seg_3531" s="T432">lär-či-gu</ta>
            <ta e="T434" id="Seg_3532" s="T433">qonɨŋ</ta>
            <ta e="T435" id="Seg_3533" s="T434">tʼätʼči-za-ŋ</ta>
            <ta e="T436" id="Seg_3534" s="T435">toːzopka-zä</ta>
            <ta e="T437" id="Seg_3535" s="T436">a</ta>
            <ta e="T438" id="Seg_3536" s="T437">nännä</ta>
            <ta e="T439" id="Seg_3537" s="T438">assə</ta>
            <ta e="T440" id="Seg_3538" s="T439">okkɨr-ɨ-ŋ</ta>
            <ta e="T441" id="Seg_3539" s="T440">tʼätʼtši-za-ŋ</ta>
            <ta e="T442" id="Seg_3540" s="T441">šɨttə</ta>
            <ta e="T443" id="Seg_3541" s="T442">puːǯi</ta>
            <ta e="T444" id="Seg_3542" s="T443">tüːləsä-zä</ta>
            <ta e="T445" id="Seg_3543" s="T444">kɨkkɨ-za-ŋ</ta>
            <ta e="T446" id="Seg_3544" s="T445">man</ta>
            <ta e="T447" id="Seg_3545" s="T446">täb-ɨ-m</ta>
            <ta e="T448" id="Seg_3546" s="T447">qoː-lʼdi-gu</ta>
            <ta e="T449" id="Seg_3547" s="T448">man</ta>
            <ta e="T450" id="Seg_3548" s="T449">täb-ɨ-m</ta>
            <ta e="T451" id="Seg_3549" s="T450">qo-lʼdʼä-w</ta>
            <ta e="T452" id="Seg_3550" s="T451">tɨttə-n</ta>
            <ta e="T453" id="Seg_3551" s="T452">olo-ɣɨn</ta>
            <ta e="T454" id="Seg_3552" s="T453">assə</ta>
            <ta e="T455" id="Seg_3553" s="T454">naːr-ɨ-ŋ</ta>
            <ta e="T456" id="Seg_3554" s="T455">tättɨ-ŋ</ta>
            <ta e="T457" id="Seg_3555" s="T456">tɨttɨ-n</ta>
            <ta e="T458" id="Seg_3556" s="T457">olo-ndə</ta>
            <ta e="T459" id="Seg_3557" s="T458">tʼättši-ku-za-ŋ</ta>
            <ta e="T460" id="Seg_3558" s="T459">i</ta>
            <ta e="T461" id="Seg_3559" s="T460">qwäːtʼä-w</ta>
            <ta e="T462" id="Seg_3560" s="T461">nʼegətšeː-ja-ŋ</ta>
            <ta e="T463" id="Seg_3561" s="T462">nɨŋ-e-ja-ŋ</ta>
            <ta e="T464" id="Seg_3562" s="T463">assə</ta>
            <ta e="T465" id="Seg_3563" s="T464">kɨkkɨ-za-ŋ</ta>
            <ta e="T466" id="Seg_3564" s="T465">qwädʼi-gu</ta>
            <ta e="T467" id="Seg_3565" s="T466">nʼäja-m</ta>
            <ta e="T468" id="Seg_3566" s="T467">manǯi-mba-ŋ</ta>
            <ta e="T469" id="Seg_3567" s="T468">man</ta>
            <ta e="T470" id="Seg_3568" s="T469">matʼtʼö-ɣi</ta>
            <ta e="T471" id="Seg_3569" s="T470">lʼäɣa-w</ta>
            <ta e="T472" id="Seg_3570" s="T471">nim-də</ta>
            <ta e="T473" id="Seg_3571" s="T472">täb-ɨ-n</ta>
            <ta e="T474" id="Seg_3572" s="T473">äːdu</ta>
            <ta e="T475" id="Seg_3573" s="T474">qai-m-daka</ta>
            <ta e="T476" id="Seg_3574" s="T475">meː-ku-t</ta>
            <ta e="T477" id="Seg_3575" s="T476">tʼäǯi-ku-täj</ta>
            <ta e="T478" id="Seg_3576" s="T477">tɨːdɨ-n</ta>
            <ta e="T479" id="Seg_3577" s="T478">taːba-ɣɨn</ta>
            <ta e="T480" id="Seg_3578" s="T479">man</ta>
            <ta e="T481" id="Seg_3579" s="T480">tolʼdʼi-n</ta>
            <ta e="T482" id="Seg_3580" s="T481">nʼöɣɨn-na-ŋ</ta>
            <ta e="T483" id="Seg_3581" s="T482">täb-ɨ-nni</ta>
            <ta e="T484" id="Seg_3582" s="T483">täp</ta>
            <ta e="T485" id="Seg_3583" s="T484">pɨŋgə-lɨ-mbɨdi</ta>
            <ta e="T486" id="Seg_3584" s="T485">nʼäja-m</ta>
            <ta e="T487" id="Seg_3585" s="T486">kunnɨ</ta>
            <ta e="T488" id="Seg_3586" s="T487">man</ta>
            <ta e="T489" id="Seg_3587" s="T488">nɨŋ-ɨ-za-ŋ</ta>
            <ta e="T490" id="Seg_3588" s="T489">nʼegɨtši-za-ŋ</ta>
            <ta e="T491" id="Seg_3589" s="T490">täp</ta>
            <ta e="T492" id="Seg_3590" s="T491">am-ɨ-ba-t</ta>
            <ta e="T493" id="Seg_3591" s="T492">qwädi-mba-t</ta>
            <ta e="T494" id="Seg_3592" s="T493">meŋa</ta>
            <ta e="T495" id="Seg_3593" s="T494">moqqo-ɣɨ</ta>
            <ta e="T496" id="Seg_3594" s="T495">tobɨ-ka-la-m-də</ta>
            <ta e="T497" id="Seg_3595" s="T496">ɨ</ta>
            <ta e="T498" id="Seg_3596" s="T497">pušqaj</ta>
            <ta e="T499" id="Seg_3597" s="T498">talʼdʼü-m</ta>
            <ta e="T500" id="Seg_3598" s="T499">me</ta>
            <ta e="T501" id="Seg_3599" s="T500">täp-se</ta>
            <ta e="T502" id="Seg_3600" s="T501">qwädu-z-oː</ta>
            <ta e="T503" id="Seg_3601" s="T502">tɨttäru-z-oː</ta>
            <ta e="T504" id="Seg_3602" s="T503">aj</ta>
            <ta e="T505" id="Seg_3603" s="T504">miriu-z-o</ta>
            <ta e="T506" id="Seg_3604" s="T505">na</ta>
            <ta e="T507" id="Seg_3605" s="T506">dʼel</ta>
            <ta e="T508" id="Seg_3606" s="T507">me</ta>
            <ta e="T509" id="Seg_3607" s="T508">tas-s-o</ta>
            <ta e="T510" id="Seg_3608" s="T509">šɨttə</ta>
            <ta e="T511" id="Seg_3609" s="T510">nʼäja-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_3610" s="T1">šittə</ta>
            <ta e="T3" id="Seg_3611" s="T2">saːrum</ta>
            <ta e="T4" id="Seg_3612" s="T3">šittə</ta>
            <ta e="T5" id="Seg_3613" s="T4">qwäj-köt</ta>
            <ta e="T6" id="Seg_3614" s="T5">čislo-qəntɨ</ta>
            <ta e="T7" id="Seg_3615" s="T6">aktʼäbər-ɨ-n</ta>
            <ta e="T8" id="Seg_3616" s="T7">irä-qəntɨ</ta>
            <ta e="T9" id="Seg_3617" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_3618" s="T9">tɨsʼäča</ta>
            <ta e="T11" id="Seg_3619" s="T10">okkɨr</ta>
            <ta e="T12" id="Seg_3620" s="T11">tʼädʼigʼöt</ta>
            <ta e="T13" id="Seg_3621" s="T12">ton</ta>
            <ta e="T14" id="Seg_3622" s="T13">muktut</ta>
            <ta e="T15" id="Seg_3623" s="T14">saːrum</ta>
            <ta e="T16" id="Seg_3624" s="T15">po-tɨ</ta>
            <ta e="T17" id="Seg_3625" s="T16">po-qəntɨ</ta>
            <ta e="T18" id="Seg_3626" s="T17">man</ta>
            <ta e="T19" id="Seg_3627" s="T18">qwən-sɨ-ŋ</ta>
            <ta e="T20" id="Seg_3628" s="T19">suːrɨ-le-gu</ta>
            <ta e="T21" id="Seg_3629" s="T20">nʼarrɨ-n-taːj</ta>
            <ta e="T22" id="Seg_3630" s="T21">suːrɨ-lʼčǝ</ta>
            <ta e="T23" id="Seg_3631" s="T22">qum-n</ta>
            <ta e="T24" id="Seg_3632" s="T23">maːt-ndɨ</ta>
            <ta e="T25" id="Seg_3633" s="T24">nim-tɨ</ta>
            <ta e="T26" id="Seg_3634" s="T25">matʼtʼi-n</ta>
            <ta e="T27" id="Seg_3635" s="T26">tʼwät-ɨ-n</ta>
            <ta e="T28" id="Seg_3636" s="T27">Мitrij</ta>
            <ta e="T29" id="Seg_3637" s="T28">Sidər-ɨ-n</ta>
            <ta e="T30" id="Seg_3638" s="T29">matʼtʼi</ta>
            <ta e="T31" id="Seg_3639" s="T30">na-tɨ</ta>
            <ta e="T32" id="Seg_3640" s="T31">eː-sɨ-n</ta>
            <ta e="T33" id="Seg_3641" s="T32">ara-n</ta>
            <ta e="T34" id="Seg_3642" s="T33">tʼak</ta>
            <ta e="T35" id="Seg_3643" s="T34">attu</ta>
            <ta e="T36" id="Seg_3644" s="T35">ara-n</ta>
            <ta e="T37" id="Seg_3645" s="T36">man</ta>
            <ta e="T38" id="Seg_3646" s="T37">wattə-mɨ</ta>
            <ta e="T39" id="Seg_3647" s="T38">eː-sɨ-n</ta>
            <ta e="T40" id="Seg_3648" s="T39">soː</ta>
            <ta e="T41" id="Seg_3649" s="T40">natqo</ta>
            <ta e="T42" id="Seg_3650" s="T41">soː</ta>
            <ta e="T43" id="Seg_3651" s="T42">eː-sɨ</ta>
            <ta e="T44" id="Seg_3652" s="T43">qanǯe-mɨntɨ</ta>
            <ta e="T45" id="Seg_3653" s="T44">sündʼi-mɨn</ta>
            <ta e="T46" id="Seg_3654" s="T45">nʼarrɨ-n</ta>
            <ta e="T47" id="Seg_3655" s="T46">taːj</ta>
            <ta e="T48" id="Seg_3656" s="T47">qaj-ɨ-mɨn</ta>
            <ta e="T49" id="Seg_3657" s="T48">nadə-m-ne-sɨ</ta>
            <ta e="T50" id="Seg_3658" s="T49">čaːǯɨ-gu</ta>
            <ta e="T51" id="Seg_3659" s="T50">qantǝ-mbɨ-sɨ</ta>
            <ta e="T52" id="Seg_3660" s="T51">i</ta>
            <ta e="T53" id="Seg_3661" s="T52">nakkɨr</ta>
            <ta e="T54" id="Seg_3662" s="T53">lʼaqa</ta>
            <ta e="T55" id="Seg_3663" s="T54">qwən-mbɨ-sɨ-tɨt</ta>
            <ta e="T56" id="Seg_3664" s="T55">man</ta>
            <ta e="T57" id="Seg_3665" s="T56">nʼarne-n</ta>
            <ta e="T58" id="Seg_3666" s="T57">tap-ɨ-la-n</ta>
            <ta e="T59" id="Seg_3667" s="T58">nim-tɨ</ta>
            <ta e="T60" id="Seg_3668" s="T59">Vasilij</ta>
            <ta e="T61" id="Seg_3669" s="T60">Мaksim</ta>
            <ta e="T62" id="Seg_3670" s="T61">Мaksim-ɨ-n</ta>
            <ta e="T63" id="Seg_3671" s="T62">qoja-n-tɨ</ta>
            <ta e="T64" id="Seg_3672" s="T63">nim-tɨ</ta>
            <ta e="T65" id="Seg_3673" s="T64">man</ta>
            <ta e="T66" id="Seg_3674" s="T65">äwɨlǯi-mbɨ-m</ta>
            <ta e="T67" id="Seg_3675" s="T66">tap-ɨ-la</ta>
            <ta e="T68" id="Seg_3676" s="T67">qwən-ntɨ-mbɨ-tɨt</ta>
            <ta e="T69" id="Seg_3677" s="T68">tolʼdʼi</ta>
            <ta e="T70" id="Seg_3678" s="T69">wattə-m</ta>
            <ta e="T71" id="Seg_3679" s="T70">tap-ɨ-la-n</ta>
            <ta e="T72" id="Seg_3680" s="T71">tolʼdʼi-wattə</ta>
            <ta e="T73" id="Seg_3681" s="T72">qantǝ-mbɨ</ta>
            <ta e="T74" id="Seg_3682" s="T73">taw</ta>
            <ta e="T75" id="Seg_3683" s="T74">tʼeːlɨ</ta>
            <ta e="T76" id="Seg_3684" s="T75">kulʼdi-k</ta>
            <ta e="T77" id="Seg_3685" s="T76">kɨkkɨ-sɨ-ŋ</ta>
            <ta e="T78" id="Seg_3686" s="T77">qweːdu-lɨ-gu</ta>
            <ta e="T79" id="Seg_3687" s="T78">nʼaja-ka-se</ta>
            <ta e="T80" id="Seg_3688" s="T79">assɨ</ta>
            <ta e="T81" id="Seg_3689" s="T80">qweːdu-lɨ-ɨ-ŋ</ta>
            <ta e="T82" id="Seg_3690" s="T81">okkɨr</ta>
            <ta e="T83" id="Seg_3691" s="T82">nʼaja-m</ta>
            <ta e="T84" id="Seg_3692" s="T83">kanak-mɨ</ta>
            <ta e="T85" id="Seg_3693" s="T84">kanak-la-mɨ</ta>
            <ta e="T86" id="Seg_3694" s="T85">mudɨj-mbɨ-sɨ-tɨ</ta>
            <ta e="T87" id="Seg_3695" s="T86">man</ta>
            <ta e="T88" id="Seg_3696" s="T87">manǯɨ-mbɨ-sɨ-m</ta>
            <ta e="T89" id="Seg_3697" s="T88">tɨtɨŋ-qən</ta>
            <ta e="T90" id="Seg_3698" s="T89">assɨ</ta>
            <ta e="T91" id="Seg_3699" s="T90">qolʼdʼi-sɨ-m</ta>
            <ta e="T92" id="Seg_3700" s="T91">kanak-mɨ</ta>
            <ta e="T93" id="Seg_3701" s="T92">qo-ɨ-tɨ</ta>
            <ta e="T94" id="Seg_3702" s="T93">šittə-mtelǯij</ta>
            <ta e="T95" id="Seg_3703" s="T94">nʼaja</ta>
            <ta e="T96" id="Seg_3704" s="T95">kwärgə-qən</ta>
            <ta e="T97" id="Seg_3705" s="T96">man</ta>
            <ta e="T98" id="Seg_3706" s="T97">qolʼdʼi-m</ta>
            <ta e="T99" id="Seg_3707" s="T98">tap-ɨ-m</ta>
            <ta e="T100" id="Seg_3708" s="T99">tʼaččɨ-gu</ta>
            <ta e="T101" id="Seg_3709" s="T100">assɨ</ta>
            <ta e="T102" id="Seg_3710" s="T101">eː-sɨ-ŋ</ta>
            <ta e="T103" id="Seg_3711" s="T102">natqo</ta>
            <ta e="T104" id="Seg_3712" s="T103">tap-ɨ-m</ta>
            <ta e="T105" id="Seg_3713" s="T104">assɨ</ta>
            <ta e="T106" id="Seg_3714" s="T105">qwat-sɨ-m</ta>
            <ta e="T107" id="Seg_3715" s="T106">tap</ta>
            <ta e="T108" id="Seg_3716" s="T107">nʼarqə-sɨ</ta>
            <ta e="T109" id="Seg_3717" s="T108">nʼarqə</ta>
            <ta e="T110" id="Seg_3718" s="T109">nʼaja-m</ta>
            <ta e="T111" id="Seg_3719" s="T110">assɨ</ta>
            <ta e="T112" id="Seg_3720" s="T111">tʼätča-ku-ut</ta>
            <ta e="T113" id="Seg_3721" s="T112">assɨ</ta>
            <ta e="T114" id="Seg_3722" s="T113">mittɨ-le</ta>
            <ta e="T115" id="Seg_3723" s="T114">tʼwät-ɨ-n</ta>
            <ta e="T116" id="Seg_3724" s="T115">nim-tɨ</ta>
            <ta e="T117" id="Seg_3725" s="T116">Мitrij</ta>
            <ta e="T118" id="Seg_3726" s="T117">Sidər-ɨ-n</ta>
            <ta e="T119" id="Seg_3727" s="T118">i</ta>
            <ta e="T120" id="Seg_3728" s="T119">Lukanʼtʼi-n</ta>
            <ta e="T121" id="Seg_3729" s="T120">lewi</ta>
            <ta e="T122" id="Seg_3730" s="T121">maːt-ndɨ</ta>
            <ta e="T123" id="Seg_3731" s="T122">pannalu-n</ta>
            <ta e="T124" id="Seg_3732" s="T123">qanǯe-mɨ</ta>
            <ta e="T125" id="Seg_3733" s="T124">na</ta>
            <ta e="T126" id="Seg_3734" s="T125">qanǯe-mɨ</ta>
            <ta e="T127" id="Seg_3735" s="T126">mi-mbɨdi</ta>
            <ta e="T128" id="Seg_3736" s="T127">i</ta>
            <ta e="T129" id="Seg_3737" s="T128">qo-mbɨdi</ta>
            <ta e="T130" id="Seg_3738" s="T129">man</ta>
            <ta e="T131" id="Seg_3739" s="T130">paja-n-nɨ</ta>
            <ta e="T132" id="Seg_3740" s="T131">wattə</ta>
            <ta e="T133" id="Seg_3741" s="T132">eː-sɨ-n</ta>
            <ta e="T134" id="Seg_3742" s="T133">assɨ</ta>
            <ta e="T135" id="Seg_3743" s="T134">soː</ta>
            <ta e="T136" id="Seg_3744" s="T135">qwən-ndi</ta>
            <ta e="T137" id="Seg_3745" s="T136">qum-la-n</ta>
            <ta e="T138" id="Seg_3746" s="T137">moqə-qəntɨ</ta>
            <ta e="T139" id="Seg_3747" s="T138">qanǯe-mɨntɨ</ta>
            <ta e="T140" id="Seg_3748" s="T139">assɨ</ta>
            <ta e="T141" id="Seg_3749" s="T140">qantǝ-mbɨ-sɨ</ta>
            <ta e="T142" id="Seg_3750" s="T141">üːdɨ-m-le</ta>
            <ta e="T143" id="Seg_3751" s="T142">oldǝ-n</ta>
            <ta e="T144" id="Seg_3752" s="T143">qanǯe-mɨ</ta>
            <ta e="T145" id="Seg_3753" s="T144">pannalu-n</ta>
            <ta e="T146" id="Seg_3754" s="T145">qaj-la-mɨ</ta>
            <ta e="T147" id="Seg_3755" s="T146">i</ta>
            <ta e="T148" id="Seg_3756" s="T147">pannalu-mbɨdi</ta>
            <ta e="T149" id="Seg_3757" s="T148">qanǯe-nga-mɨ</ta>
            <ta e="T150" id="Seg_3758" s="T149">i</ta>
            <ta e="T151" id="Seg_3759" s="T150">qaj-la-mɨ</ta>
            <ta e="T152" id="Seg_3760" s="T151">moqə-le</ta>
            <ta e="T153" id="Seg_3761" s="T152">qwən-tɨ-m</ta>
            <ta e="T154" id="Seg_3762" s="T153">nʼarrɨ-n</ta>
            <ta e="T155" id="Seg_3763" s="T154">topǝ</ta>
            <ta e="T156" id="Seg_3764" s="T155">tɨtɨŋ</ta>
            <ta e="T157" id="Seg_3765" s="T156">mɨs-ɨ-ndɨ</ta>
            <ta e="T158" id="Seg_3766" s="T157">tɨtɨŋ</ta>
            <ta e="T159" id="Seg_3767" s="T158">mɨs-ɨ-qən</ta>
            <ta e="T160" id="Seg_3768" s="T159">pen-ŋɨ-m</ta>
            <ta e="T161" id="Seg_3769" s="T160">tüː-m</ta>
            <ta e="T162" id="Seg_3770" s="T161">laqčɨ-ŋɨ-m</ta>
            <ta e="T163" id="Seg_3771" s="T162">tɨtɨŋ-n</ta>
            <ta e="T164" id="Seg_3772" s="T163">moː-la-m</ta>
            <ta e="T165" id="Seg_3773" s="T164">meː-ŋɨ-m</ta>
            <ta e="T166" id="Seg_3774" s="T165">omdɨ-sɨ</ta>
            <ta e="T167" id="Seg_3775" s="T166">quča-sɨ-tɨ</ta>
            <ta e="T168" id="Seg_3776" s="T167">koːptɨ-m</ta>
            <ta e="T169" id="Seg_3777" s="T168">Madʼö</ta>
            <ta e="T170" id="Seg_3778" s="T169">sagi-k</ta>
            <ta e="T171" id="Seg_3779" s="T170">udə-r-ɨ-n</ta>
            <ta e="T172" id="Seg_3780" s="T171">meː-nan</ta>
            <ta e="T173" id="Seg_3781" s="T172">okkɨr</ta>
            <ta e="T174" id="Seg_3782" s="T173">tärba</ta>
            <ta e="T175" id="Seg_3783" s="T174">kundar</ta>
            <ta e="T176" id="Seg_3784" s="T175">laːdi-gu</ta>
            <ta e="T177" id="Seg_3785" s="T176">pannalu-mbɨdi</ta>
            <ta e="T178" id="Seg_3786" s="T177">qanǯe-m</ta>
            <ta e="T179" id="Seg_3787" s="T178">meː-nan</ta>
            <ta e="T180" id="Seg_3788" s="T179">eː-sɨ-tɨt</ta>
            <ta e="T181" id="Seg_3789" s="T180">nʼo-n</ta>
            <ta e="T182" id="Seg_3790" s="T181">kɨssɨn-ɨ-la</ta>
            <ta e="T183" id="Seg_3791" s="T182">šittə</ta>
            <ta e="T184" id="Seg_3792" s="T183">saːrum</ta>
            <ta e="T185" id="Seg_3793" s="T184">šittə</ta>
            <ta e="T186" id="Seg_3794" s="T185">qwäj-köt</ta>
            <ta e="T187" id="Seg_3795" s="T186">šittə</ta>
            <ta e="T188" id="Seg_3796" s="T187">saːrum</ta>
            <ta e="T189" id="Seg_3797" s="T188">nakkɨr</ta>
            <ta e="T190" id="Seg_3798" s="T189">qwäj-köt</ta>
            <ta e="T191" id="Seg_3799" s="T190">pi-n</ta>
            <ta e="T192" id="Seg_3800" s="T191">man</ta>
            <ta e="T193" id="Seg_3801" s="T192">laqqɨ-sɨ-ŋ</ta>
            <ta e="T194" id="Seg_3802" s="T193">sagi-k</ta>
            <ta e="T195" id="Seg_3803" s="T194">praːi-ku-sɨ-m</ta>
            <ta e="T196" id="Seg_3804" s="T195">onäk</ta>
            <ta e="T197" id="Seg_3805" s="T196">qanǯe-m</ta>
            <ta e="T198" id="Seg_3806" s="T197">mindɨn</ta>
            <ta e="T199" id="Seg_3807" s="T198">tadɨ-r-ɨ-gu</ta>
            <ta e="T200" id="Seg_3808" s="T199">qaj-la-m</ta>
            <ta e="T201" id="Seg_3809" s="T200">katomko-lʼ</ta>
            <ta e="T202" id="Seg_3810" s="T201">mäkkä</ta>
            <ta e="T203" id="Seg_3811" s="T202">sätčuŋ</ta>
            <ta e="T204" id="Seg_3812" s="T203">eː-nǯɨ-n</ta>
            <ta e="T205" id="Seg_3813" s="T204">natqo</ta>
            <ta e="T206" id="Seg_3814" s="T205">pi-n</ta>
            <ta e="T207" id="Seg_3815" s="T206">soː-k</ta>
            <ta e="T208" id="Seg_3816" s="T207">assɨ</ta>
            <ta e="T209" id="Seg_3817" s="T208">qontə-sɨ-n</ta>
            <ta e="T210" id="Seg_3818" s="T209">qanǯe-m</ta>
            <ta e="T211" id="Seg_3819" s="T210">meː-ku-sɨ-m</ta>
            <ta e="T212" id="Seg_3820" s="T211">pi-n</ta>
            <ta e="T213" id="Seg_3821" s="T212">tʼönʒi-n</ta>
            <ta e="T214" id="Seg_3822" s="T213">ǯenna</ta>
            <ta e="T215" id="Seg_3823" s="T214">okkɨr-ɨ-k</ta>
            <ta e="T216" id="Seg_3824" s="T215">pi-n</ta>
            <ta e="T217" id="Seg_3825" s="T216">kanak-mɨ</ta>
            <ta e="T218" id="Seg_3826" s="T217">mudɨj-gu</ta>
            <ta e="T219" id="Seg_3827" s="T218">eː-sɨ-n</ta>
            <ta e="T220" id="Seg_3828" s="T219">šittə-k</ta>
            <ta e="T221" id="Seg_3829" s="T220">nakkɨr-k</ta>
            <ta e="T222" id="Seg_3830" s="T221">wäq-sɨ</ta>
            <ta e="T223" id="Seg_3831" s="T222">man</ta>
            <ta e="T224" id="Seg_3832" s="T223">assɨ</ta>
            <ta e="T225" id="Seg_3833" s="T224">larɨ-mbɨ-sɨ-ŋ</ta>
            <ta e="T226" id="Seg_3834" s="T225">man</ta>
            <ta e="T227" id="Seg_3835" s="T226">onäk</ta>
            <ta e="T228" id="Seg_3836" s="T227">*siːm-mɨ</ta>
            <ta e="T229" id="Seg_3837" s="T228">oral-mbɨ-sɨ-m</ta>
            <ta e="T230" id="Seg_3838" s="T229">qaq</ta>
            <ta e="T231" id="Seg_3839" s="T230">hozjain</ta>
            <ta e="T232" id="Seg_3840" s="T231">onäk</ta>
            <ta e="T233" id="Seg_3841" s="T232">tüː-ɣonäŋ</ta>
            <ta e="T234" id="Seg_3842" s="T233">man</ta>
            <ta e="T235" id="Seg_3843" s="T234">manǯɨ-mbɨ-sɨ-m</ta>
            <ta e="T236" id="Seg_3844" s="T235">tɨtɨŋ</ta>
            <ta e="T237" id="Seg_3845" s="T236">maka-ndɨ</ta>
            <ta e="T238" id="Seg_3846" s="T237">wərkə</ta>
            <ta e="T239" id="Seg_3847" s="T238">nʼarrɨ-n</ta>
            <ta e="T240" id="Seg_3848" s="T239">topǝ</ta>
            <ta e="T241" id="Seg_3849" s="T240">man</ta>
            <ta e="T242" id="Seg_3850" s="T241">tärba-sɨ-ŋ</ta>
            <ta e="T243" id="Seg_3851" s="T242">qarɨ</ta>
            <ta e="T244" id="Seg_3852" s="T243">nʼarne</ta>
            <ta e="T245" id="Seg_3853" s="T244">čaːǯɨ-nǯɨ-ŋ</ta>
            <ta e="T246" id="Seg_3854" s="T245">na</ta>
            <ta e="T247" id="Seg_3855" s="T246">qanǯe</ta>
            <ta e="T248" id="Seg_3856" s="T247">wattə</ta>
            <ta e="T249" id="Seg_3857" s="T248">sündʼi-mɨn</ta>
            <ta e="T250" id="Seg_3858" s="T249">sɨsarum</ta>
            <ta e="T251" id="Seg_3859" s="T250">nakkɨr-mtelǯij</ta>
            <ta e="T252" id="Seg_3860" s="T251">qarɨ-mɨ-qən</ta>
            <ta e="T253" id="Seg_3861" s="T252">kundɨ</ta>
            <ta e="T254" id="Seg_3862" s="T253">assɨ</ta>
            <ta e="T255" id="Seg_3863" s="T254">takǝ-le-mbɨ-sɨ-ŋ</ta>
            <ta e="T256" id="Seg_3864" s="T255">čaːǯɨ-gu</ta>
            <ta e="T257" id="Seg_3865" s="T256">soː</ta>
            <ta e="T258" id="Seg_3866" s="T257">eː-sɨ</ta>
            <ta e="T259" id="Seg_3867" s="T258">ara-lʼ</ta>
            <ta e="T260" id="Seg_3868" s="T259">qanǯe</ta>
            <ta e="T261" id="Seg_3869" s="T260">wattə-mɨn</ta>
            <ta e="T262" id="Seg_3870" s="T261">qaj</ta>
            <ta e="T263" id="Seg_3871" s="T262">qantǝ-mbɨ-sɨ</ta>
            <ta e="T264" id="Seg_3872" s="T263">okkɨr</ta>
            <ta e="T265" id="Seg_3873" s="T264">qwäj-köt</ta>
            <ta e="T266" id="Seg_3874" s="T265">čas-qəntɨ</ta>
            <ta e="T267" id="Seg_3875" s="T266">tʼeːlɨ-n</ta>
            <ta e="T268" id="Seg_3876" s="T267">man</ta>
            <ta e="T269" id="Seg_3877" s="T268">eː-sɨ-ŋ</ta>
            <ta e="T270" id="Seg_3878" s="T269">nakkɨr</ta>
            <ta e="T271" id="Seg_3879" s="T270">suːrǝm-lʼdi</ta>
            <ta e="T272" id="Seg_3880" s="T271">lʼaqa-la-n-nan</ta>
            <ta e="T273" id="Seg_3881" s="T272">tap-ɨ-la</ta>
            <ta e="T274" id="Seg_3882" s="T273">nʼaja-m</ta>
            <ta e="T275" id="Seg_3883" s="T274">assɨ</ta>
            <ta e="T276" id="Seg_3884" s="T275">qwat-ku-sɨ-tɨt</ta>
            <ta e="T277" id="Seg_3885" s="T276">tüː-lewle</ta>
            <ta e="T278" id="Seg_3886" s="T277">suːrɨ-lʼdi</ta>
            <ta e="T279" id="Seg_3887" s="T278">maːt-ndɨ</ta>
            <ta e="T280" id="Seg_3888" s="T279">man</ta>
            <ta e="T281" id="Seg_3889" s="T280">qwən-sɨ-ŋ</ta>
            <ta e="T282" id="Seg_3890" s="T281">pemɨ-gu</ta>
            <ta e="T283" id="Seg_3891" s="T282">na</ta>
            <ta e="T284" id="Seg_3892" s="T283">tʼeːlɨ</ta>
            <ta e="T285" id="Seg_3893" s="T284">man</ta>
            <ta e="T286" id="Seg_3894" s="T285">qwat-ŋɨ-m</ta>
            <ta e="T287" id="Seg_3895" s="T286">okkɨr</ta>
            <ta e="T288" id="Seg_3896" s="T287">nʼaja-m</ta>
            <ta e="T289" id="Seg_3897" s="T288">naːdə</ta>
            <ta e="T290" id="Seg_3898" s="T289">na</ta>
            <ta e="T291" id="Seg_3899" s="T290">säɣə-eː-sɨ</ta>
            <ta e="T292" id="Seg_3900" s="T291">i</ta>
            <ta e="T293" id="Seg_3901" s="T292">okkɨr</ta>
            <ta e="T294" id="Seg_3902" s="T293">säŋɨ-m</ta>
            <ta e="T295" id="Seg_3903" s="T294">eː-sɨ-n</ta>
            <ta e="T296" id="Seg_3904" s="T295">vaskrʼösnɨj</ta>
            <ta e="T297" id="Seg_3905" s="T296">tʼeːlɨ-tɨ</ta>
            <ta e="T298" id="Seg_3906" s="T297">šittə-mtelǯij</ta>
            <ta e="T299" id="Seg_3907" s="T298">tʼeːlɨ</ta>
            <ta e="T300" id="Seg_3908" s="T299">pemɨ-sɨ-ŋ</ta>
            <ta e="T301" id="Seg_3909" s="T300">qwat-sɨ-m</ta>
            <ta e="T302" id="Seg_3910" s="T301">šittə</ta>
            <ta e="T303" id="Seg_3911" s="T302">nʼaja-m</ta>
            <ta e="T304" id="Seg_3912" s="T303">tap-qi</ta>
            <ta e="T305" id="Seg_3913" s="T304">eː-sɨ-di</ta>
            <ta e="T306" id="Seg_3914" s="T305">siːnʼe</ta>
            <ta e="T307" id="Seg_3915" s="T306">uttɨ</ta>
            <ta e="T308" id="Seg_3916" s="T307">säɣə</ta>
            <ta e="T309" id="Seg_3917" s="T308">qobɨ</ta>
            <ta e="T310" id="Seg_3918" s="T309">šide-mɨ</ta>
            <ta e="T311" id="Seg_3919" s="T310">assɨ</ta>
            <ta e="T312" id="Seg_3920" s="T311">anda-lɨ-mbɨ</ta>
            <ta e="T313" id="Seg_3921" s="T312">nakkɨr-mtelǯij</ta>
            <ta e="T314" id="Seg_3922" s="T313">tʼeːlɨ</ta>
            <ta e="T315" id="Seg_3923" s="T314">eː-sɨ-n</ta>
            <ta e="T316" id="Seg_3924" s="T315">soː</ta>
            <ta e="T317" id="Seg_3925" s="T316">tʼeːlɨ-tɨ</ta>
            <ta e="T318" id="Seg_3926" s="T317">na</ta>
            <ta e="T319" id="Seg_3927" s="T318">tʼeːlɨ</ta>
            <ta e="T320" id="Seg_3928" s="T319">qaj-m-naj-assɨ</ta>
            <ta e="T321" id="Seg_3929" s="T320">qwat-sɨ-m</ta>
            <ta e="T322" id="Seg_3930" s="T321">tʼeːlɨ-tɨ</ta>
            <ta e="T323" id="Seg_3931" s="T322">eː-sɨ-n</ta>
            <ta e="T324" id="Seg_3932" s="T323">tʼeːlɨ-tɨ</ta>
            <ta e="T325" id="Seg_3933" s="T324">tʼeːlɨ-m-mbɨdi</ta>
            <ta e="T326" id="Seg_3934" s="T325">man</ta>
            <ta e="T327" id="Seg_3935" s="T326">paja-nan</ta>
            <ta e="T328" id="Seg_3936" s="T327">na</ta>
            <ta e="T329" id="Seg_3937" s="T328">tʼeːlɨ</ta>
            <ta e="T330" id="Seg_3938" s="T329">lʼaqa-la-se</ta>
            <ta e="T331" id="Seg_3939" s="T330">okkɨr</ta>
            <ta e="T332" id="Seg_3940" s="T331">mɨ-qən</ta>
            <ta e="T333" id="Seg_3941" s="T332">paldʼu-sɨ-ut</ta>
            <ta e="T334" id="Seg_3942" s="T333">nʼarrɨ-n</ta>
            <ta e="T335" id="Seg_3943" s="T334">taːj</ta>
            <ta e="T336" id="Seg_3944" s="T335">pojkɨ-ndɨ</ta>
            <ta e="T337" id="Seg_3945" s="T336">natʼtʼa-n</ta>
            <ta e="T338" id="Seg_3946" s="T337">meː</ta>
            <ta e="T339" id="Seg_3947" s="T338">qwädɨ-sɨ-ut</ta>
            <ta e="T340" id="Seg_3948" s="T339">po-ndɨ</ta>
            <ta e="T341" id="Seg_3949" s="T340">nägɨ-r-le</ta>
            <ta e="T342" id="Seg_3950" s="T341">onut</ta>
            <ta e="T343" id="Seg_3951" s="T342">nim-la-wɨt</ta>
            <ta e="T344" id="Seg_3952" s="T343">a</ta>
            <ta e="T345" id="Seg_3953" s="T344">meː</ta>
            <ta e="T346" id="Seg_3954" s="T345">nim-wɨt</ta>
            <ta e="T347" id="Seg_3955" s="T346">Madʼö</ta>
            <ta e="T348" id="Seg_3956" s="T347">Мaksim</ta>
            <ta e="T349" id="Seg_3957" s="T348">i</ta>
            <ta e="T350" id="Seg_3958" s="T349">Wasʼka</ta>
            <ta e="T351" id="Seg_3959" s="T350">taw</ta>
            <ta e="T352" id="Seg_3960" s="T351">tʼeːlɨ</ta>
            <ta e="T353" id="Seg_3961" s="T352">meː</ta>
            <ta e="T354" id="Seg_3962" s="T353">tadɨ-sɨ-ut</ta>
            <ta e="T355" id="Seg_3963" s="T354">nakkɨr</ta>
            <ta e="T356" id="Seg_3964" s="T355">tadɨ-ŋɨ-tɨt</ta>
            <ta e="T357" id="Seg_3965" s="T356">quː-mbɨdi</ta>
            <ta e="T358" id="Seg_3966" s="T357">ässɨ-m</ta>
            <ta e="T359" id="Seg_3967" s="T358">Sämon-ɨ-m</ta>
            <ta e="T360" id="Seg_3968" s="T359">miɣnut</ta>
            <ta e="T361" id="Seg_3969" s="T360">eː-sɨ-n</ta>
            <ta e="T362" id="Seg_3970" s="T361">opsä</ta>
            <ta e="T363" id="Seg_3971" s="T362">assɨ</ta>
            <ta e="T364" id="Seg_3972" s="T363">soː-k</ta>
            <ta e="T365" id="Seg_3973" s="T364">tättɨ-mtelǯij</ta>
            <ta e="T366" id="Seg_3974" s="T365">tʼeːlɨ</ta>
            <ta e="T367" id="Seg_3975" s="T366">qarɨ-mɨqɨn</ta>
            <ta e="T368" id="Seg_3976" s="T367">pöm</ta>
            <ta e="T369" id="Seg_3977" s="T368">eː-sɨ</ta>
            <ta e="T370" id="Seg_3978" s="T369">üːdɨ-mɨn</ta>
            <ta e="T371" id="Seg_3979" s="T370">eː-sɨ-n</ta>
            <ta e="T372" id="Seg_3980" s="T371">särro-tɨ</ta>
            <ta e="T373" id="Seg_3981" s="T372">tʼeːlɨ-tɨ</ta>
            <ta e="T374" id="Seg_3982" s="T373">sätčuŋ</ta>
            <ta e="T375" id="Seg_3983" s="T374">eː-sɨ</ta>
            <ta e="T376" id="Seg_3984" s="T375">paldʼu-gu</ta>
            <ta e="T377" id="Seg_3985" s="T376">topǝ-la</ta>
            <ta e="T378" id="Seg_3986" s="T377">nuːnə-mbɨ-sɨ-tɨt</ta>
            <ta e="T379" id="Seg_3987" s="T378">na</ta>
            <ta e="T380" id="Seg_3988" s="T379">tʼeːlɨ</ta>
            <ta e="T381" id="Seg_3989" s="T380">tadɨ-ŋɨ-m</ta>
            <ta e="T382" id="Seg_3990" s="T381">nakkɨr</ta>
            <ta e="T383" id="Seg_3991" s="T382">nʼaja-m</ta>
            <ta e="T384" id="Seg_3992" s="T383">sombɨlʼe-mtelǯij</ta>
            <ta e="T385" id="Seg_3993" s="T384">tʼeːlɨ</ta>
            <ta e="T386" id="Seg_3994" s="T385">okkɨr</ta>
            <ta e="T387" id="Seg_3995" s="T386">lʼaqa</ta>
            <ta e="T388" id="Seg_3996" s="T387">nim-tɨ</ta>
            <ta e="T389" id="Seg_3997" s="T388">tap-ɨ-n</ta>
            <ta e="T390" id="Seg_3998" s="T389">Wasilij</ta>
            <ta e="T391" id="Seg_3999" s="T390">qwən-sɨ</ta>
            <ta e="T392" id="Seg_4000" s="T391">maːt-qəntɨ</ta>
            <ta e="T393" id="Seg_4001" s="T392">na</ta>
            <ta e="T394" id="Seg_4002" s="T393">tʼeːlɨ</ta>
            <ta e="T395" id="Seg_4003" s="T394">tadɨ-sɨ-m</ta>
            <ta e="T396" id="Seg_4004" s="T395">tättɨ</ta>
            <ta e="T397" id="Seg_4005" s="T396">nʼaja-m</ta>
            <ta e="T398" id="Seg_4006" s="T397">muktut-mtelǯij</ta>
            <ta e="T399" id="Seg_4007" s="T398">tʼeːlɨ</ta>
            <ta e="T400" id="Seg_4008" s="T399">paldʼu-sɨ-ŋ</ta>
            <ta e="T401" id="Seg_4009" s="T400">püruj</ta>
            <ta e="T402" id="Seg_4010" s="T401">nʼarrɨ-n</ta>
            <ta e="T403" id="Seg_4011" s="T402">taːj</ta>
            <ta e="T404" id="Seg_4012" s="T403">äːrukka-n</ta>
            <ta e="T405" id="Seg_4013" s="T404">čaːǯɨ-sɨ-n</ta>
            <ta e="T406" id="Seg_4014" s="T405">natqo</ta>
            <ta e="T407" id="Seg_4015" s="T406">üŋɨlʼǯɨ-mbɨ-le</ta>
            <ta e="T408" id="Seg_4016" s="T407">qaqä</ta>
            <ta e="T409" id="Seg_4017" s="T408">muːt-ɨ-lʼčǝ-nǯɨ-n</ta>
            <ta e="T410" id="Seg_4018" s="T409">man</ta>
            <ta e="T411" id="Seg_4019" s="T410">kanak-mɨ</ta>
            <ta e="T412" id="Seg_4020" s="T411">nim-tɨ</ta>
            <ta e="T413" id="Seg_4021" s="T412">tap-ɨ-n</ta>
            <ta e="T414" id="Seg_4022" s="T413">eːto</ta>
            <ta e="T415" id="Seg_4023" s="T414">ündɨ-ntɨ-m</ta>
            <ta e="T416" id="Seg_4024" s="T415">ündɨ-zä-ŋɨ-m</ta>
            <ta e="T417" id="Seg_4025" s="T416">kanak-mɨ</ta>
            <ta e="T418" id="Seg_4026" s="T417">muːt-ɨ-n</ta>
            <ta e="T419" id="Seg_4027" s="T418">nʼaja-ndɨ</ta>
            <ta e="T420" id="Seg_4028" s="T419">mittɨ-nče-gu</ta>
            <ta e="T421" id="Seg_4029" s="T420">eːto-nɨ</ta>
            <ta e="T422" id="Seg_4030" s="T421">tap</ta>
            <ta e="T423" id="Seg_4031" s="T422">soː-k</ta>
            <ta e="T424" id="Seg_4032" s="T423">muːt-ɨ-n</ta>
            <ta e="T425" id="Seg_4033" s="T424">wərkə</ta>
            <ta e="T426" id="Seg_4034" s="T425">pirgə</ta>
            <ta e="T427" id="Seg_4035" s="T426">tɨtɨŋ-ndɨ</ta>
            <ta e="T428" id="Seg_4036" s="T427">manǯɨ-mbɨ-ŋ</ta>
            <ta e="T429" id="Seg_4037" s="T428">innä</ta>
            <ta e="T430" id="Seg_4038" s="T429">assɨ</ta>
            <ta e="T431" id="Seg_4039" s="T430">qolʼdʼi-ku-ŋɨ-m</ta>
            <ta e="T432" id="Seg_4040" s="T431">nʼaja-m</ta>
            <ta e="T433" id="Seg_4041" s="T432">larɨ-ču-gu</ta>
            <ta e="T434" id="Seg_4042" s="T433">qoːnɨŋ</ta>
            <ta e="T435" id="Seg_4043" s="T434">tʼaččɨ-sɨ-n</ta>
            <ta e="T436" id="Seg_4044" s="T435">toːzopka-se</ta>
            <ta e="T437" id="Seg_4045" s="T436">a</ta>
            <ta e="T438" id="Seg_4046" s="T437">nɨːnɨ</ta>
            <ta e="T439" id="Seg_4047" s="T438">assɨ</ta>
            <ta e="T440" id="Seg_4048" s="T439">okkɨr-ɨ-k</ta>
            <ta e="T441" id="Seg_4049" s="T440">tʼaččɨ-sɨ-ŋ</ta>
            <ta e="T442" id="Seg_4050" s="T441">šittə</ta>
            <ta e="T443" id="Seg_4051" s="T442">puːčə</ta>
            <ta e="T444" id="Seg_4052" s="T443">tülʼse-se</ta>
            <ta e="T445" id="Seg_4053" s="T444">kɨkkɨ-sɨ-ŋ</ta>
            <ta e="T446" id="Seg_4054" s="T445">man</ta>
            <ta e="T447" id="Seg_4055" s="T446">tap-ɨ-m</ta>
            <ta e="T448" id="Seg_4056" s="T447">qo-lʼčǝ-gu</ta>
            <ta e="T449" id="Seg_4057" s="T448">man</ta>
            <ta e="T450" id="Seg_4058" s="T449">tap-ɨ-m</ta>
            <ta e="T451" id="Seg_4059" s="T450">qo-lʼčǝ-m</ta>
            <ta e="T452" id="Seg_4060" s="T451">tɨtɨŋ-n</ta>
            <ta e="T453" id="Seg_4061" s="T452">olɨ-qən</ta>
            <ta e="T454" id="Seg_4062" s="T453">assɨ</ta>
            <ta e="T455" id="Seg_4063" s="T454">nakkɨr-ɨ-k</ta>
            <ta e="T456" id="Seg_4064" s="T455">tättɨ-k</ta>
            <ta e="T457" id="Seg_4065" s="T456">tɨtɨŋ-n</ta>
            <ta e="T458" id="Seg_4066" s="T457">olɨ-ndɨ</ta>
            <ta e="T459" id="Seg_4067" s="T458">tʼaččɨ-ku-sɨ-n</ta>
            <ta e="T460" id="Seg_4068" s="T459">i</ta>
            <ta e="T461" id="Seg_4069" s="T460">qwädɨ-m</ta>
            <ta e="T462" id="Seg_4070" s="T461">negəčču-ŋɨ-ŋ</ta>
            <ta e="T463" id="Seg_4071" s="T462">nɨŋ-ɨ-ŋɨ-ŋ</ta>
            <ta e="T464" id="Seg_4072" s="T463">assɨ</ta>
            <ta e="T465" id="Seg_4073" s="T464">kɨkkɨ-sɨ-ŋ</ta>
            <ta e="T466" id="Seg_4074" s="T465">qwädɨ-gu</ta>
            <ta e="T467" id="Seg_4075" s="T466">nʼaja-m</ta>
            <ta e="T468" id="Seg_4076" s="T467">manǯɨ-mbɨ-ŋ</ta>
            <ta e="T469" id="Seg_4077" s="T468">man</ta>
            <ta e="T470" id="Seg_4078" s="T469">matʼtʼi-qɨlʼ</ta>
            <ta e="T471" id="Seg_4079" s="T470">lʼaqa-mɨ</ta>
            <ta e="T472" id="Seg_4080" s="T471">nim-tɨ</ta>
            <ta e="T473" id="Seg_4081" s="T472">tap-ɨ-n</ta>
            <ta e="T474" id="Seg_4082" s="T473">eːto</ta>
            <ta e="T475" id="Seg_4083" s="T474">qaj-m-taka</ta>
            <ta e="T476" id="Seg_4084" s="T475">meː-ku-tɨ</ta>
            <ta e="T477" id="Seg_4085" s="T476">tʼäǯi-ku-ndi</ta>
            <ta e="T478" id="Seg_4086" s="T477">tɨtɨŋ-n</ta>
            <ta e="T479" id="Seg_4087" s="T478">taːba-qən</ta>
            <ta e="T480" id="Seg_4088" s="T479">man</ta>
            <ta e="T481" id="Seg_4089" s="T480">tolʼdʼi-n</ta>
            <ta e="T482" id="Seg_4090" s="T481">nʼöɣɨn-ŋɨ-ŋ</ta>
            <ta e="T483" id="Seg_4091" s="T482">tap-ɨ-nɨ</ta>
            <ta e="T484" id="Seg_4092" s="T483">tap</ta>
            <ta e="T485" id="Seg_4093" s="T484">puŋgə-lɨ-mbɨdi</ta>
            <ta e="T486" id="Seg_4094" s="T485">nʼaja-m</ta>
            <ta e="T487" id="Seg_4095" s="T486">kuːn</ta>
            <ta e="T488" id="Seg_4096" s="T487">man</ta>
            <ta e="T489" id="Seg_4097" s="T488">nɨŋ-ɨ-sɨ-ŋ</ta>
            <ta e="T490" id="Seg_4098" s="T489">negəčču-sɨ-ŋ</ta>
            <ta e="T491" id="Seg_4099" s="T490">tap</ta>
            <ta e="T492" id="Seg_4100" s="T491">am-ɨ-mbɨ-tɨ</ta>
            <ta e="T493" id="Seg_4101" s="T492">qwädɨ-mbɨ-tɨ</ta>
            <ta e="T494" id="Seg_4102" s="T493">mäkkä</ta>
            <ta e="T495" id="Seg_4103" s="T494">moqə-qɨlʼ</ta>
            <ta e="T496" id="Seg_4104" s="T495">topǝ-ka-la-m-tɨ</ta>
            <ta e="T497" id="Seg_4105" s="T496">iː</ta>
            <ta e="T498" id="Seg_4106" s="T497">pušqaj</ta>
            <ta e="T499" id="Seg_4107" s="T498">talʼdʼu-m</ta>
            <ta e="T500" id="Seg_4108" s="T499">meː</ta>
            <ta e="T501" id="Seg_4109" s="T500">tap-se</ta>
            <ta e="T502" id="Seg_4110" s="T501">qwodɨ-sɨ-ut</ta>
            <ta e="T503" id="Seg_4111" s="T502">tɨttäru-sɨ-ut</ta>
            <ta e="T504" id="Seg_4112" s="T503">aj</ta>
            <ta e="T505" id="Seg_4113" s="T504">miriu-sɨ-ut</ta>
            <ta e="T506" id="Seg_4114" s="T505">na</ta>
            <ta e="T507" id="Seg_4115" s="T506">tʼeːlɨ</ta>
            <ta e="T508" id="Seg_4116" s="T507">meː</ta>
            <ta e="T509" id="Seg_4117" s="T508">tadɨ-sɨ-ut</ta>
            <ta e="T510" id="Seg_4118" s="T509">šittə</ta>
            <ta e="T511" id="Seg_4119" s="T510">nʼaja-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_4120" s="T1">two</ta>
            <ta e="T3" id="Seg_4121" s="T2">ten</ta>
            <ta e="T4" id="Seg_4122" s="T3">two</ta>
            <ta e="T5" id="Seg_4123" s="T4">superfluous-ten</ta>
            <ta e="T6" id="Seg_4124" s="T5">number-LOC.3SG</ta>
            <ta e="T7" id="Seg_4125" s="T6">October-EP-GEN</ta>
            <ta e="T8" id="Seg_4126" s="T7">month-LOC.3SG</ta>
            <ta e="T9" id="Seg_4127" s="T8">one</ta>
            <ta e="T10" id="Seg_4128" s="T9">thousand</ta>
            <ta e="T11" id="Seg_4129" s="T10">one</ta>
            <ta e="T12" id="Seg_4130" s="T11">nine</ta>
            <ta e="T13" id="Seg_4131" s="T12">hundred</ta>
            <ta e="T14" id="Seg_4132" s="T13">six</ta>
            <ta e="T15" id="Seg_4133" s="T14">ten</ta>
            <ta e="T16" id="Seg_4134" s="T15">year-3SG</ta>
            <ta e="T17" id="Seg_4135" s="T16">year-LOC.3SG</ta>
            <ta e="T18" id="Seg_4136" s="T17">I.NOM</ta>
            <ta e="T19" id="Seg_4137" s="T18">go.away-PST-1SG.S</ta>
            <ta e="T20" id="Seg_4138" s="T19">hunt-INCH-INF</ta>
            <ta e="T21" id="Seg_4139" s="T20">swamp-GEN-across</ta>
            <ta e="T22" id="Seg_4140" s="T21">hunt-CAP.ADJZ.[NOM]</ta>
            <ta e="T23" id="Seg_4141" s="T22">human.being-GEN</ta>
            <ta e="T24" id="Seg_4142" s="T23">house-ILL</ta>
            <ta e="T25" id="Seg_4143" s="T24">name-3SG</ta>
            <ta e="T26" id="Seg_4144" s="T25">taiga-GEN</ta>
            <ta e="T27" id="Seg_4145" s="T26">place-EP-GEN</ta>
            <ta e="T28" id="Seg_4146" s="T27">Mitrij.[NOM]</ta>
            <ta e="T29" id="Seg_4147" s="T28">Sidorovich-EP-GEN</ta>
            <ta e="T30" id="Seg_4148" s="T29">taiga.[NOM]</ta>
            <ta e="T31" id="Seg_4149" s="T30">this-3SG</ta>
            <ta e="T32" id="Seg_4150" s="T31">be-PST-3SG.S</ta>
            <ta e="T33" id="Seg_4151" s="T32">autumn-ADV.LOC</ta>
            <ta e="T34" id="Seg_4152" s="T33">fast</ta>
            <ta e="T35" id="Seg_4153" s="T34">be.visible.[3SG.S]</ta>
            <ta e="T36" id="Seg_4154" s="T35">autumn-ADV.LOC</ta>
            <ta e="T37" id="Seg_4155" s="T36">I.NOM</ta>
            <ta e="T38" id="Seg_4156" s="T37">road-1SG</ta>
            <ta e="T39" id="Seg_4157" s="T38">be-PST-3SG.S</ta>
            <ta e="T40" id="Seg_4158" s="T39">good</ta>
            <ta e="T41" id="Seg_4159" s="T40">therefore</ta>
            <ta e="T42" id="Seg_4160" s="T41">good</ta>
            <ta e="T43" id="Seg_4161" s="T42">be-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_4162" s="T43">sledge-PROL.3SG</ta>
            <ta e="T45" id="Seg_4163" s="T44">inside-PROL</ta>
            <ta e="T46" id="Seg_4164" s="T45">swamp-GEN</ta>
            <ta e="T47" id="Seg_4165" s="T46">across</ta>
            <ta e="T48" id="Seg_4166" s="T47">what-EP-PROL</ta>
            <ta e="T49" id="Seg_4167" s="T48">one.should-TRL-%%-PST.[3SG.S]</ta>
            <ta e="T50" id="Seg_4168" s="T49">travel-INF</ta>
            <ta e="T51" id="Seg_4169" s="T50">freeze-PST.NAR-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_4170" s="T51">and</ta>
            <ta e="T53" id="Seg_4171" s="T52">three</ta>
            <ta e="T54" id="Seg_4172" s="T53">friend.[NOM]</ta>
            <ta e="T55" id="Seg_4173" s="T54">go.away-DUR-PST-3PL</ta>
            <ta e="T56" id="Seg_4174" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_4175" s="T56">in.front.of-ADV.LOC</ta>
            <ta e="T58" id="Seg_4176" s="T57">(s)he-EP-PL-GEN</ta>
            <ta e="T59" id="Seg_4177" s="T58">name-3SG</ta>
            <ta e="T60" id="Seg_4178" s="T59">Vasiliy</ta>
            <ta e="T61" id="Seg_4179" s="T60">Maksim</ta>
            <ta e="T62" id="Seg_4180" s="T61">Maksim-EP-GEN</ta>
            <ta e="T63" id="Seg_4181" s="T62">younger.sibling-GEN-3SG</ta>
            <ta e="T64" id="Seg_4182" s="T63">name-3SG</ta>
            <ta e="T65" id="Seg_4183" s="T64">I.NOM</ta>
            <ta e="T66" id="Seg_4184" s="T65">forgot-PST.NAR-1SG.O</ta>
            <ta e="T67" id="Seg_4185" s="T66">(s)he-EP-PL</ta>
            <ta e="T68" id="Seg_4186" s="T67">go.away-IPFV-PST.NAR-3PL</ta>
            <ta e="T69" id="Seg_4187" s="T68">ski</ta>
            <ta e="T70" id="Seg_4188" s="T69">road-ACC</ta>
            <ta e="T71" id="Seg_4189" s="T70">(s)he-EP-PL-GEN</ta>
            <ta e="T72" id="Seg_4190" s="T71">ski-road.[NOM]</ta>
            <ta e="T73" id="Seg_4191" s="T72">freeze-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_4192" s="T73">this</ta>
            <ta e="T75" id="Seg_4193" s="T74">day.[NOM]</ta>
            <ta e="T76" id="Seg_4194" s="T75">which-ADVZ</ta>
            <ta e="T77" id="Seg_4195" s="T76">want-PST-1SG.S</ta>
            <ta e="T78" id="Seg_4196" s="T77">meet-RES-INF</ta>
            <ta e="T79" id="Seg_4197" s="T78">squirrel-DIM-COM</ta>
            <ta e="T80" id="Seg_4198" s="T79">NEG</ta>
            <ta e="T81" id="Seg_4199" s="T80">meet-RES-EP-1SG.S</ta>
            <ta e="T82" id="Seg_4200" s="T81">one</ta>
            <ta e="T83" id="Seg_4201" s="T82">squirrel-ACC</ta>
            <ta e="T84" id="Seg_4202" s="T83">dog.[NOM]-1SG</ta>
            <ta e="T85" id="Seg_4203" s="T84">dog-PL-1SG</ta>
            <ta e="T86" id="Seg_4204" s="T85">bark-DUR-PST-3SG.O</ta>
            <ta e="T87" id="Seg_4205" s="T86">I.NOM</ta>
            <ta e="T88" id="Seg_4206" s="T87">look-DUR-PST-1SG.O</ta>
            <ta e="T89" id="Seg_4207" s="T88">cedar-LOC</ta>
            <ta e="T90" id="Seg_4208" s="T89">NEG</ta>
            <ta e="T91" id="Seg_4209" s="T90">see-PST-1SG.O</ta>
            <ta e="T92" id="Seg_4210" s="T91">dog.[NOM]-1SG</ta>
            <ta e="T93" id="Seg_4211" s="T92">find-EP-3SG.O</ta>
            <ta e="T94" id="Seg_4212" s="T93">two-ORD</ta>
            <ta e="T95" id="Seg_4213" s="T94">squirrel.[NOM]</ta>
            <ta e="T96" id="Seg_4214" s="T95">pine-LOC</ta>
            <ta e="T97" id="Seg_4215" s="T96">I.NOM</ta>
            <ta e="T98" id="Seg_4216" s="T97">see-1SG.O</ta>
            <ta e="T99" id="Seg_4217" s="T98">(s)he-EP-ACC</ta>
            <ta e="T100" id="Seg_4218" s="T99">shoot-INF</ta>
            <ta e="T101" id="Seg_4219" s="T100">NEG</ta>
            <ta e="T102" id="Seg_4220" s="T101">be-PST-1SG.S</ta>
            <ta e="T103" id="Seg_4221" s="T102">therefore</ta>
            <ta e="T104" id="Seg_4222" s="T103">(s)he-EP-ACC</ta>
            <ta e="T105" id="Seg_4223" s="T104">NEG</ta>
            <ta e="T106" id="Seg_4224" s="T105">kill-PST-1SG.O</ta>
            <ta e="T107" id="Seg_4225" s="T106">(s)he.[NOM]</ta>
            <ta e="T108" id="Seg_4226" s="T107">red-PST.[3SG.S]</ta>
            <ta e="T109" id="Seg_4227" s="T108">red</ta>
            <ta e="T110" id="Seg_4228" s="T109">squirrel-ACC</ta>
            <ta e="T111" id="Seg_4229" s="T110">NEG</ta>
            <ta e="T112" id="Seg_4230" s="T111">shoot-HAB-1PL</ta>
            <ta e="T113" id="Seg_4231" s="T112">NEG</ta>
            <ta e="T114" id="Seg_4232" s="T113">reach-CVB</ta>
            <ta e="T115" id="Seg_4233" s="T114">place-EP-GEN</ta>
            <ta e="T116" id="Seg_4234" s="T115">name.[NOM]-3SG</ta>
            <ta e="T117" id="Seg_4235" s="T116">Mitrij.[NOM]</ta>
            <ta e="T118" id="Seg_4236" s="T117">Sidorovich-EP-GEN</ta>
            <ta e="T119" id="Seg_4237" s="T118">and</ta>
            <ta e="T120" id="Seg_4238" s="T119">Lukantyi-GEN</ta>
            <ta e="T121" id="Seg_4239" s="T120">board.[NOM]</ta>
            <ta e="T122" id="Seg_4240" s="T121">house-ILL</ta>
            <ta e="T123" id="Seg_4241" s="T122">break.down-3SG.S</ta>
            <ta e="T124" id="Seg_4242" s="T123">sledge-1SG</ta>
            <ta e="T125" id="Seg_4243" s="T124">this</ta>
            <ta e="T126" id="Seg_4244" s="T125">sledge-1SG</ta>
            <ta e="T127" id="Seg_4245" s="T126">give-PTCP.PST</ta>
            <ta e="T128" id="Seg_4246" s="T127">and</ta>
            <ta e="T129" id="Seg_4247" s="T128">find-PTCP.PST</ta>
            <ta e="T130" id="Seg_4248" s="T129">I.NOM</ta>
            <ta e="T131" id="Seg_4249" s="T130">old.woman-GEN-ALL</ta>
            <ta e="T132" id="Seg_4250" s="T131">road.[NOM]</ta>
            <ta e="T133" id="Seg_4251" s="T132">be-PST-3SG.S</ta>
            <ta e="T134" id="Seg_4252" s="T133">NEG</ta>
            <ta e="T135" id="Seg_4253" s="T134">good</ta>
            <ta e="T136" id="Seg_4254" s="T135">go.away-PTCP.PRS</ta>
            <ta e="T137" id="Seg_4255" s="T136">human.being-PL-GEN</ta>
            <ta e="T138" id="Seg_4256" s="T137">back-LOC.3SG</ta>
            <ta e="T139" id="Seg_4257" s="T138">sledge-PROL.3SG</ta>
            <ta e="T140" id="Seg_4258" s="T139">NEG</ta>
            <ta e="T141" id="Seg_4259" s="T140">freeze-DUR-PST.[3SG.S]</ta>
            <ta e="T142" id="Seg_4260" s="T141">evening-TRL-CVB</ta>
            <ta e="T143" id="Seg_4261" s="T142">start-3SG.S</ta>
            <ta e="T144" id="Seg_4262" s="T143">sledge-1SG</ta>
            <ta e="T145" id="Seg_4263" s="T144">break.down-3SG.S</ta>
            <ta e="T146" id="Seg_4264" s="T145">what-PL-1SG</ta>
            <ta e="T147" id="Seg_4265" s="T146">and</ta>
            <ta e="T148" id="Seg_4266" s="T147">break.down-PTCP.PST</ta>
            <ta e="T149" id="Seg_4267" s="T148">sledge-%%-1SG</ta>
            <ta e="T150" id="Seg_4268" s="T149">and</ta>
            <ta e="T151" id="Seg_4269" s="T150">what-PL-1SG</ta>
            <ta e="T152" id="Seg_4270" s="T151">back-CVB</ta>
            <ta e="T153" id="Seg_4271" s="T152">go.away-TR-1SG.O</ta>
            <ta e="T154" id="Seg_4272" s="T153">swamp-GEN</ta>
            <ta e="T155" id="Seg_4273" s="T154">leg.[NOM]</ta>
            <ta e="T156" id="Seg_4274" s="T155">cedar.[NOM]</ta>
            <ta e="T157" id="Seg_4275" s="T156">cape-EP-ILL</ta>
            <ta e="T158" id="Seg_4276" s="T157">cedar.[NOM]</ta>
            <ta e="T159" id="Seg_4277" s="T158">cape-EP-LOC</ta>
            <ta e="T160" id="Seg_4278" s="T159">put-CO-1SG.O</ta>
            <ta e="T161" id="Seg_4279" s="T160">fire-ACC</ta>
            <ta e="T162" id="Seg_4280" s="T161">break.down-CO-1SG.O</ta>
            <ta e="T163" id="Seg_4281" s="T162">cedar-GEN</ta>
            <ta e="T164" id="Seg_4282" s="T163">branch-PL-ACC</ta>
            <ta e="T165" id="Seg_4283" s="T164">do-CO-1SG.O</ta>
            <ta e="T166" id="Seg_4284" s="T165">sit.down-PST.[3SG.S]</ta>
            <ta e="T167" id="Seg_4285" s="T166">go.to.sleep-PST-3SG</ta>
            <ta e="T168" id="Seg_4286" s="T167">place-ACC</ta>
            <ta e="T169" id="Seg_4287" s="T168">Matvey.[NOM]</ta>
            <ta e="T170" id="Seg_4288" s="T169">hard-ADVZ</ta>
            <ta e="T171" id="Seg_4289" s="T170">stop-FRQ-EP-3SG.S</ta>
            <ta e="T172" id="Seg_4290" s="T171">we.DU.NOM-ADES</ta>
            <ta e="T173" id="Seg_4291" s="T172">one</ta>
            <ta e="T174" id="Seg_4292" s="T173">think.[3SG.S]</ta>
            <ta e="T175" id="Seg_4293" s="T174">how</ta>
            <ta e="T176" id="Seg_4294" s="T175">repair-INF</ta>
            <ta e="T177" id="Seg_4295" s="T176">break.down-PTCP.PST</ta>
            <ta e="T178" id="Seg_4296" s="T177">sledge-ACC</ta>
            <ta e="T179" id="Seg_4297" s="T178">we.DU.NOM-ADES</ta>
            <ta e="T180" id="Seg_4298" s="T179">be-PST-3PL</ta>
            <ta e="T181" id="Seg_4299" s="T180">hare-GEN</ta>
            <ta e="T182" id="Seg_4300" s="T181">loop-EP-PL</ta>
            <ta e="T183" id="Seg_4301" s="T182">two</ta>
            <ta e="T184" id="Seg_4302" s="T183">ten</ta>
            <ta e="T185" id="Seg_4303" s="T184">two</ta>
            <ta e="T186" id="Seg_4304" s="T185">superfluous-ten</ta>
            <ta e="T187" id="Seg_4305" s="T186">two</ta>
            <ta e="T188" id="Seg_4306" s="T187">ten</ta>
            <ta e="T189" id="Seg_4307" s="T188">three</ta>
            <ta e="T190" id="Seg_4308" s="T189">superfluous-ten</ta>
            <ta e="T191" id="Seg_4309" s="T190">night-ADV.LOC</ta>
            <ta e="T192" id="Seg_4310" s="T191">I.NOM</ta>
            <ta e="T193" id="Seg_4311" s="T192">work-PST-1SG.S</ta>
            <ta e="T194" id="Seg_4312" s="T193">hard-ADVZ</ta>
            <ta e="T195" id="Seg_4313" s="T194">%%-HAB-PST-1SG.O</ta>
            <ta e="T196" id="Seg_4314" s="T195">oneself.1SG</ta>
            <ta e="T197" id="Seg_4315" s="T196">sledge-ACC</ta>
            <ta e="T198" id="Seg_4316" s="T197">%%</ta>
            <ta e="T199" id="Seg_4317" s="T198">bring-FRQ-EP-INF</ta>
            <ta e="T200" id="Seg_4318" s="T199">what-PL-ACC</ta>
            <ta e="T201" id="Seg_4319" s="T200">%%-ADJZ</ta>
            <ta e="T202" id="Seg_4320" s="T201">I.ALL</ta>
            <ta e="T203" id="Seg_4321" s="T202">hard</ta>
            <ta e="T204" id="Seg_4322" s="T203">be-FUT-3SG.S</ta>
            <ta e="T205" id="Seg_4323" s="T204">therefore</ta>
            <ta e="T206" id="Seg_4324" s="T205">night-ADV.LOC</ta>
            <ta e="T207" id="Seg_4325" s="T206">good-ADVZ</ta>
            <ta e="T208" id="Seg_4326" s="T207">NEG</ta>
            <ta e="T209" id="Seg_4327" s="T208">sleep-PST-3SG.S</ta>
            <ta e="T210" id="Seg_4328" s="T209">sledge-ACC</ta>
            <ta e="T211" id="Seg_4329" s="T210">do-HAB-PST-1SG.O</ta>
            <ta e="T212" id="Seg_4330" s="T211">night-ADV.LOC</ta>
            <ta e="T213" id="Seg_4331" s="T212">middle-GEN</ta>
            <ta e="T214" id="Seg_4332" s="T213">until</ta>
            <ta e="T215" id="Seg_4333" s="T214">one-EP-ADVZ</ta>
            <ta e="T216" id="Seg_4334" s="T215">night-ADV.LOC</ta>
            <ta e="T217" id="Seg_4335" s="T216">dog.[NOM]-1SG</ta>
            <ta e="T218" id="Seg_4336" s="T217">bark-INF</ta>
            <ta e="T219" id="Seg_4337" s="T218">be-PST-3SG.S</ta>
            <ta e="T220" id="Seg_4338" s="T219">two-ADVZ</ta>
            <ta e="T221" id="Seg_4339" s="T220">three-ADVZ</ta>
            <ta e="T222" id="Seg_4340" s="T221">yap-PST.[3SG.S]</ta>
            <ta e="T223" id="Seg_4341" s="T222">I.NOM</ta>
            <ta e="T224" id="Seg_4342" s="T223">NEG</ta>
            <ta e="T225" id="Seg_4343" s="T224">be.afraid-DUR-PST-1SG.S</ta>
            <ta e="T226" id="Seg_4344" s="T225">I.NOM</ta>
            <ta e="T227" id="Seg_4345" s="T226">oneself.1SG</ta>
            <ta e="T228" id="Seg_4346" s="T227">self-1SG</ta>
            <ta e="T229" id="Seg_4347" s="T228">catch-DUR-PST-1SG.O</ta>
            <ta e="T230" id="Seg_4348" s="T229">how</ta>
            <ta e="T231" id="Seg_4349" s="T230">master.[NOM]</ta>
            <ta e="T232" id="Seg_4350" s="T231">oneself.1SG</ta>
            <ta e="T233" id="Seg_4351" s="T232">fire-%%</ta>
            <ta e="T234" id="Seg_4352" s="T233">I.NOM</ta>
            <ta e="T235" id="Seg_4353" s="T234">look-DUR-PST-1SG.O</ta>
            <ta e="T236" id="Seg_4354" s="T235">cedar.[NOM]</ta>
            <ta e="T237" id="Seg_4355" s="T236">island-ILL</ta>
            <ta e="T238" id="Seg_4356" s="T237">big</ta>
            <ta e="T239" id="Seg_4357" s="T238">swamp-GEN</ta>
            <ta e="T240" id="Seg_4358" s="T239">leg.[NOM]</ta>
            <ta e="T241" id="Seg_4359" s="T240">I.NOM</ta>
            <ta e="T242" id="Seg_4360" s="T241">think-PST-1SG.S</ta>
            <ta e="T243" id="Seg_4361" s="T242">morning</ta>
            <ta e="T244" id="Seg_4362" s="T243">forward</ta>
            <ta e="T245" id="Seg_4363" s="T244">travel-FUT-1SG.S</ta>
            <ta e="T246" id="Seg_4364" s="T245">this</ta>
            <ta e="T247" id="Seg_4365" s="T246">sledge.[NOM]</ta>
            <ta e="T248" id="Seg_4366" s="T247">road.[NOM]</ta>
            <ta e="T249" id="Seg_4367" s="T248">inside-PROL</ta>
            <ta e="T250" id="Seg_4368" s="T249">twenty</ta>
            <ta e="T251" id="Seg_4369" s="T250">three-ORD</ta>
            <ta e="T252" id="Seg_4370" s="T251">morning-something-LOC</ta>
            <ta e="T253" id="Seg_4371" s="T252">long</ta>
            <ta e="T254" id="Seg_4372" s="T253">NEG</ta>
            <ta e="T255" id="Seg_4373" s="T254">gather-INCH-DUR-PST-1SG.S</ta>
            <ta e="T256" id="Seg_4374" s="T255">travel-INF</ta>
            <ta e="T257" id="Seg_4375" s="T256">good</ta>
            <ta e="T258" id="Seg_4376" s="T257">be-PST.[3SG.S]</ta>
            <ta e="T259" id="Seg_4377" s="T258">autumn-ADJZ</ta>
            <ta e="T260" id="Seg_4378" s="T259">sledge.[NOM]</ta>
            <ta e="T261" id="Seg_4379" s="T260">road-PROL</ta>
            <ta e="T262" id="Seg_4380" s="T261">what</ta>
            <ta e="T263" id="Seg_4381" s="T262">freeze-DUR-PST.[3SG.S]</ta>
            <ta e="T264" id="Seg_4382" s="T263">one</ta>
            <ta e="T265" id="Seg_4383" s="T264">superfluous-ten</ta>
            <ta e="T266" id="Seg_4384" s="T265">hour-LOC.3SG</ta>
            <ta e="T267" id="Seg_4385" s="T266">day-ADV.LOC</ta>
            <ta e="T268" id="Seg_4386" s="T267">I.NOM</ta>
            <ta e="T269" id="Seg_4387" s="T268">be-PST-1SG.S</ta>
            <ta e="T270" id="Seg_4388" s="T269">three</ta>
            <ta e="T271" id="Seg_4389" s="T270">wild.animal-CAP.ADJZ</ta>
            <ta e="T272" id="Seg_4390" s="T271">friend-PL-GEN-ADES</ta>
            <ta e="T273" id="Seg_4391" s="T272">(s)he-EP-PL.[NOM]</ta>
            <ta e="T274" id="Seg_4392" s="T273">squirrel-ACC</ta>
            <ta e="T275" id="Seg_4393" s="T274">NEG</ta>
            <ta e="T276" id="Seg_4394" s="T275">kill-HAB-PST-3PL</ta>
            <ta e="T277" id="Seg_4395" s="T276">come-CVB2</ta>
            <ta e="T278" id="Seg_4396" s="T277">hunt-CAP.ADJZ</ta>
            <ta e="T279" id="Seg_4397" s="T278">house-ILL</ta>
            <ta e="T280" id="Seg_4398" s="T279">I.NOM</ta>
            <ta e="T281" id="Seg_4399" s="T280">go.away-PST-1SG.S</ta>
            <ta e="T282" id="Seg_4400" s="T281">hunt-INF</ta>
            <ta e="T283" id="Seg_4401" s="T282">this</ta>
            <ta e="T284" id="Seg_4402" s="T283">day.[NOM]</ta>
            <ta e="T285" id="Seg_4403" s="T284">I.NOM</ta>
            <ta e="T286" id="Seg_4404" s="T285">kill-CO-1SG.O</ta>
            <ta e="T287" id="Seg_4405" s="T286">one</ta>
            <ta e="T288" id="Seg_4406" s="T287">squirrel-ACC</ta>
            <ta e="T289" id="Seg_4407" s="T288">this</ta>
            <ta e="T290" id="Seg_4408" s="T289">this</ta>
            <ta e="T291" id="Seg_4409" s="T290">black-be-PST.[NOM]</ta>
            <ta e="T292" id="Seg_4410" s="T291">and</ta>
            <ta e="T293" id="Seg_4411" s="T292">one</ta>
            <ta e="T294" id="Seg_4412" s="T293">wood.grouse-ACC</ta>
            <ta e="T295" id="Seg_4413" s="T294">be-PST-3SG.S</ta>
            <ta e="T296" id="Seg_4414" s="T295">Sunday</ta>
            <ta e="T297" id="Seg_4415" s="T296">sun.[NOM]-3SG</ta>
            <ta e="T298" id="Seg_4416" s="T297">two-ORD</ta>
            <ta e="T299" id="Seg_4417" s="T298">day.[NOM]</ta>
            <ta e="T300" id="Seg_4418" s="T299">hunt-PST-1SG.S</ta>
            <ta e="T301" id="Seg_4419" s="T300">kill-PST-1SG.O</ta>
            <ta e="T302" id="Seg_4420" s="T301">two</ta>
            <ta e="T303" id="Seg_4421" s="T302">squirrel-ACC</ta>
            <ta e="T304" id="Seg_4422" s="T303">(s)he-DU</ta>
            <ta e="T305" id="Seg_4423" s="T304">be-PST-3DU.S</ta>
            <ta e="T306" id="Seg_4424" s="T305">blue</ta>
            <ta e="T307" id="Seg_4425" s="T306">hand</ta>
            <ta e="T308" id="Seg_4426" s="T307">black</ta>
            <ta e="T309" id="Seg_4427" s="T308">skin.[NOM]</ta>
            <ta e="T310" id="Seg_4428" s="T309">heart.[NOM]-1SG</ta>
            <ta e="T311" id="Seg_4429" s="T310">NEG</ta>
            <ta e="T312" id="Seg_4430" s="T311">be.happy-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T313" id="Seg_4431" s="T312">three-ORD</ta>
            <ta e="T314" id="Seg_4432" s="T313">day.[NOM]</ta>
            <ta e="T315" id="Seg_4433" s="T314">be-PST-3SG.S</ta>
            <ta e="T316" id="Seg_4434" s="T315">good</ta>
            <ta e="T317" id="Seg_4435" s="T316">day.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_4436" s="T317">this</ta>
            <ta e="T319" id="Seg_4437" s="T318">day.[NOM]</ta>
            <ta e="T320" id="Seg_4438" s="T319">what-ACC-EMPH-NEG</ta>
            <ta e="T321" id="Seg_4439" s="T320">kill-PST-1SG.O</ta>
            <ta e="T322" id="Seg_4440" s="T321">day.[NOM]-3SG</ta>
            <ta e="T323" id="Seg_4441" s="T322">be-PST-3SG.S</ta>
            <ta e="T324" id="Seg_4442" s="T323">day.[NOM]-3SG</ta>
            <ta e="T325" id="Seg_4443" s="T324">day-TRL-PTCP.PST</ta>
            <ta e="T326" id="Seg_4444" s="T325">I.NOM</ta>
            <ta e="T327" id="Seg_4445" s="T326">old.woman-ADES</ta>
            <ta e="T328" id="Seg_4446" s="T327">this</ta>
            <ta e="T329" id="Seg_4447" s="T328">day.[NOM]</ta>
            <ta e="T330" id="Seg_4448" s="T329">friend-PL-COM</ta>
            <ta e="T331" id="Seg_4449" s="T330">one</ta>
            <ta e="T332" id="Seg_4450" s="T331">something-LOC</ta>
            <ta e="T333" id="Seg_4451" s="T332">go-PST-1PL</ta>
            <ta e="T334" id="Seg_4452" s="T333">swamp-GEN</ta>
            <ta e="T335" id="Seg_4453" s="T334">across</ta>
            <ta e="T336" id="Seg_4454" s="T335">Dyerevyanka-ILL</ta>
            <ta e="T337" id="Seg_4455" s="T336">there-ADV.LOC</ta>
            <ta e="T338" id="Seg_4456" s="T337">we.NOM</ta>
            <ta e="T339" id="Seg_4457" s="T338">leave-PST-1PL</ta>
            <ta e="T340" id="Seg_4458" s="T339">tree-ILL</ta>
            <ta e="T341" id="Seg_4459" s="T340">write-FRQ-CVB</ta>
            <ta e="T342" id="Seg_4460" s="T341">own.1PL</ta>
            <ta e="T343" id="Seg_4461" s="T342">name-PL-1PL</ta>
            <ta e="T344" id="Seg_4462" s="T343">but</ta>
            <ta e="T345" id="Seg_4463" s="T344">we.NOM</ta>
            <ta e="T346" id="Seg_4464" s="T345">name-1PL</ta>
            <ta e="T347" id="Seg_4465" s="T346">Matvey.[NOM]</ta>
            <ta e="T348" id="Seg_4466" s="T347">Maksim.[NOM]</ta>
            <ta e="T349" id="Seg_4467" s="T348">and</ta>
            <ta e="T350" id="Seg_4468" s="T349">Vaska.[NOM]</ta>
            <ta e="T351" id="Seg_4469" s="T350">this</ta>
            <ta e="T352" id="Seg_4470" s="T351">day.[NOM]</ta>
            <ta e="T353" id="Seg_4471" s="T352">we.NOM</ta>
            <ta e="T354" id="Seg_4472" s="T353">bring-PST-1PL</ta>
            <ta e="T355" id="Seg_4473" s="T354">three</ta>
            <ta e="T356" id="Seg_4474" s="T355">bring-CO-3PL</ta>
            <ta e="T357" id="Seg_4475" s="T356">die-PTCP.PST</ta>
            <ta e="T358" id="Seg_4476" s="T357">father-ACC</ta>
            <ta e="T359" id="Seg_4477" s="T358">Semyon-EP-ACC</ta>
            <ta e="T360" id="Seg_4478" s="T359">we.ALL</ta>
            <ta e="T361" id="Seg_4479" s="T360">be-PST-3SG.S</ta>
            <ta e="T362" id="Seg_4480" s="T361">strongly</ta>
            <ta e="T363" id="Seg_4481" s="T362">NEG</ta>
            <ta e="T364" id="Seg_4482" s="T363">good-ADVZ</ta>
            <ta e="T365" id="Seg_4483" s="T364">four-ORD</ta>
            <ta e="T366" id="Seg_4484" s="T365">day.[NOM]</ta>
            <ta e="T367" id="Seg_4485" s="T366">morning-near</ta>
            <ta e="T368" id="Seg_4486" s="T367">hot</ta>
            <ta e="T369" id="Seg_4487" s="T368">be-PST.[3SG.S]</ta>
            <ta e="T370" id="Seg_4488" s="T369">evening-PROL</ta>
            <ta e="T371" id="Seg_4489" s="T370">be-PST-3SG.S</ta>
            <ta e="T372" id="Seg_4490" s="T371">rain.[NOM]-3SG</ta>
            <ta e="T373" id="Seg_4491" s="T372">day.[NOM]-3SG</ta>
            <ta e="T374" id="Seg_4492" s="T373">hard</ta>
            <ta e="T375" id="Seg_4493" s="T374">be-PST.[3SG.S]</ta>
            <ta e="T376" id="Seg_4494" s="T375">go-INF</ta>
            <ta e="T377" id="Seg_4495" s="T376">leg-PL.[NOM]</ta>
            <ta e="T378" id="Seg_4496" s="T377">get.tired-DUR-PST-3PL</ta>
            <ta e="T379" id="Seg_4497" s="T378">this</ta>
            <ta e="T380" id="Seg_4498" s="T379">day.[NOM]</ta>
            <ta e="T381" id="Seg_4499" s="T380">bring-CO-1SG.O</ta>
            <ta e="T382" id="Seg_4500" s="T381">three</ta>
            <ta e="T383" id="Seg_4501" s="T382">squirrel-ACC</ta>
            <ta e="T384" id="Seg_4502" s="T383">five-ORD</ta>
            <ta e="T385" id="Seg_4503" s="T384">day.[NOM]</ta>
            <ta e="T386" id="Seg_4504" s="T385">one</ta>
            <ta e="T387" id="Seg_4505" s="T386">friend.[NOM]</ta>
            <ta e="T388" id="Seg_4506" s="T387">name.[NOM]-3SG</ta>
            <ta e="T389" id="Seg_4507" s="T388">(s)he-EP-GEN</ta>
            <ta e="T390" id="Seg_4508" s="T389">Vasilij.[NOM]</ta>
            <ta e="T391" id="Seg_4509" s="T390">go.away-PST.[3SG.S]</ta>
            <ta e="T392" id="Seg_4510" s="T391">house-ILL.3SG</ta>
            <ta e="T393" id="Seg_4511" s="T392">this</ta>
            <ta e="T394" id="Seg_4512" s="T393">day.[NOM]</ta>
            <ta e="T395" id="Seg_4513" s="T394">bring-PST-1SG.O</ta>
            <ta e="T396" id="Seg_4514" s="T395">four</ta>
            <ta e="T397" id="Seg_4515" s="T396">squirrel-ACC</ta>
            <ta e="T398" id="Seg_4516" s="T397">six-ORD</ta>
            <ta e="T399" id="Seg_4517" s="T398">day.[NOM]</ta>
            <ta e="T400" id="Seg_4518" s="T399">go-PST-1SG.S</ta>
            <ta e="T401" id="Seg_4519" s="T400">round</ta>
            <ta e="T402" id="Seg_4520" s="T401">swamp-GEN</ta>
            <ta e="T403" id="Seg_4521" s="T402">across</ta>
            <ta e="T404" id="Seg_4522" s="T403">%%-3SG.S</ta>
            <ta e="T405" id="Seg_4523" s="T404">travel-PST-3SG.S</ta>
            <ta e="T406" id="Seg_4524" s="T405">therefore</ta>
            <ta e="T407" id="Seg_4525" s="T406">hear-DUR-CVB</ta>
            <ta e="T408" id="Seg_4526" s="T407">when</ta>
            <ta e="T409" id="Seg_4527" s="T408">bark-EP-PFV-FUT-3SG.S</ta>
            <ta e="T410" id="Seg_4528" s="T409">I.GEN</ta>
            <ta e="T411" id="Seg_4529" s="T410">dog.[NOM]-1SG</ta>
            <ta e="T412" id="Seg_4530" s="T411">name.[NOM]-3SG</ta>
            <ta e="T413" id="Seg_4531" s="T412">(s)he-EP-GEN</ta>
            <ta e="T414" id="Seg_4532" s="T413">Edu.[NOM]</ta>
            <ta e="T415" id="Seg_4533" s="T414">hear-IPFV-1SG.O</ta>
            <ta e="T416" id="Seg_4534" s="T415">hear-%%-CO-1SG.O</ta>
            <ta e="T417" id="Seg_4535" s="T416">dog.[NOM]-1SG</ta>
            <ta e="T418" id="Seg_4536" s="T417">bark-EP-3SG.S</ta>
            <ta e="T419" id="Seg_4537" s="T418">squirrel-ILL</ta>
            <ta e="T420" id="Seg_4538" s="T419">reach-IPFV3-INF</ta>
            <ta e="T421" id="Seg_4539" s="T420">Edu-ALL</ta>
            <ta e="T422" id="Seg_4540" s="T421">(s)he.[NOM]</ta>
            <ta e="T423" id="Seg_4541" s="T422">good-ADVZ</ta>
            <ta e="T424" id="Seg_4542" s="T423">bark-EP-3SG.S</ta>
            <ta e="T425" id="Seg_4543" s="T424">big</ta>
            <ta e="T426" id="Seg_4544" s="T425">high</ta>
            <ta e="T427" id="Seg_4545" s="T426">cedar-ILL</ta>
            <ta e="T428" id="Seg_4546" s="T427">look-DUR-1SG.S</ta>
            <ta e="T429" id="Seg_4547" s="T428">up</ta>
            <ta e="T430" id="Seg_4548" s="T429">NEG</ta>
            <ta e="T431" id="Seg_4549" s="T430">see-HAB-CO-1SG.O</ta>
            <ta e="T432" id="Seg_4550" s="T431">squirrel-ACC</ta>
            <ta e="T433" id="Seg_4551" s="T432">be.afraid-TR-INF</ta>
            <ta e="T434" id="Seg_4552" s="T433">many</ta>
            <ta e="T435" id="Seg_4553" s="T434">shoot-PST-3SG.S</ta>
            <ta e="T436" id="Seg_4554" s="T435">rifle-INSTR</ta>
            <ta e="T437" id="Seg_4555" s="T436">but</ta>
            <ta e="T438" id="Seg_4556" s="T437">then</ta>
            <ta e="T439" id="Seg_4557" s="T438">NEG</ta>
            <ta e="T440" id="Seg_4558" s="T439">one-EP-ADVZ</ta>
            <ta e="T441" id="Seg_4559" s="T440">shoot-PST-1SG.S</ta>
            <ta e="T442" id="Seg_4560" s="T441">two</ta>
            <ta e="T443" id="Seg_4561" s="T442">inside.[NOM]</ta>
            <ta e="T444" id="Seg_4562" s="T443">rifle-INSTR</ta>
            <ta e="T445" id="Seg_4563" s="T444">want-PST-1SG.S</ta>
            <ta e="T446" id="Seg_4564" s="T445">I.NOM</ta>
            <ta e="T447" id="Seg_4565" s="T446">(s)he-EP-ACC</ta>
            <ta e="T448" id="Seg_4566" s="T447">see-PFV-INF</ta>
            <ta e="T449" id="Seg_4567" s="T448">I.NOM</ta>
            <ta e="T450" id="Seg_4568" s="T449">(s)he-EP-ACC</ta>
            <ta e="T451" id="Seg_4569" s="T450">see-PFV-1SG.O</ta>
            <ta e="T452" id="Seg_4570" s="T451">cedar-GEN</ta>
            <ta e="T453" id="Seg_4571" s="T452">head-LOC</ta>
            <ta e="T454" id="Seg_4572" s="T453">NEG</ta>
            <ta e="T455" id="Seg_4573" s="T454">three-EP-ADVZ</ta>
            <ta e="T456" id="Seg_4574" s="T455">four-ADVZ</ta>
            <ta e="T457" id="Seg_4575" s="T456">cedar-GEN</ta>
            <ta e="T458" id="Seg_4576" s="T457">head-ILL</ta>
            <ta e="T459" id="Seg_4577" s="T458">shoot-HAB-PST-3SG.S</ta>
            <ta e="T460" id="Seg_4578" s="T459">and</ta>
            <ta e="T461" id="Seg_4579" s="T460">leave-1SG.O</ta>
            <ta e="T462" id="Seg_4580" s="T461">smoke-CO-1SG.S</ta>
            <ta e="T463" id="Seg_4581" s="T462">stand-EP-CO-1SG.S</ta>
            <ta e="T464" id="Seg_4582" s="T463">NEG</ta>
            <ta e="T465" id="Seg_4583" s="T464">want-PST-1SG.S</ta>
            <ta e="T466" id="Seg_4584" s="T465">leave-INF</ta>
            <ta e="T467" id="Seg_4585" s="T466">squirrel-ACC</ta>
            <ta e="T468" id="Seg_4586" s="T467">look-DUR-1SG.S</ta>
            <ta e="T469" id="Seg_4587" s="T468">I.NOM</ta>
            <ta e="T470" id="Seg_4588" s="T469">taiga-ADJZ</ta>
            <ta e="T471" id="Seg_4589" s="T470">friend.[NOM]-1SG</ta>
            <ta e="T472" id="Seg_4590" s="T471">name.[NOM]-3SG</ta>
            <ta e="T473" id="Seg_4591" s="T472">(s)he-EP-GEN</ta>
            <ta e="T474" id="Seg_4592" s="T473">Edu.[NOM]</ta>
            <ta e="T475" id="Seg_4593" s="T474">what-ACC-INDEF4</ta>
            <ta e="T476" id="Seg_4594" s="T475">do-HAB-3SG.O</ta>
            <ta e="T477" id="Seg_4595" s="T476">beat-HAB-PTCP.PRS</ta>
            <ta e="T478" id="Seg_4596" s="T477">cedar-GEN</ta>
            <ta e="T479" id="Seg_4597" s="T478">%%-LOC</ta>
            <ta e="T480" id="Seg_4598" s="T479">I.NOM</ta>
            <ta e="T481" id="Seg_4599" s="T480">ski-INSTR2</ta>
            <ta e="T482" id="Seg_4600" s="T481">skate-CO-1SG.S</ta>
            <ta e="T483" id="Seg_4601" s="T482">(s)he-EP-ALL</ta>
            <ta e="T484" id="Seg_4602" s="T483">(s)he.[NOM]</ta>
            <ta e="T485" id="Seg_4603" s="T484">fall-RES-PTCP.PST</ta>
            <ta e="T486" id="Seg_4604" s="T485">squirrel-ACC</ta>
            <ta e="T487" id="Seg_4605" s="T486">where</ta>
            <ta e="T488" id="Seg_4606" s="T487">I.NOM</ta>
            <ta e="T489" id="Seg_4607" s="T488">stand-EP-PST-1SG.S</ta>
            <ta e="T490" id="Seg_4608" s="T489">smoke-PST-1SG.S</ta>
            <ta e="T491" id="Seg_4609" s="T490">(s)he.[NOM]</ta>
            <ta e="T492" id="Seg_4610" s="T491">eat-EP-PST.NAR-3SG.O</ta>
            <ta e="T493" id="Seg_4611" s="T492">leave-PST.NAR-3SG.O</ta>
            <ta e="T494" id="Seg_4612" s="T493">I.ALL</ta>
            <ta e="T495" id="Seg_4613" s="T494">back-ADJZ</ta>
            <ta e="T496" id="Seg_4614" s="T495">leg-DIM-PL-ACC-3SG</ta>
            <ta e="T497" id="Seg_4615" s="T496">take</ta>
            <ta e="T498" id="Seg_4616" s="T497">fluffy</ta>
            <ta e="T499" id="Seg_4617" s="T498">tail-ACC</ta>
            <ta e="T500" id="Seg_4618" s="T499">we.NOM</ta>
            <ta e="T501" id="Seg_4619" s="T500">(s)he-INSTR</ta>
            <ta e="T502" id="Seg_4620" s="T501">scold-PST-1PL</ta>
            <ta e="T503" id="Seg_4621" s="T502">fight-PST-1PL</ta>
            <ta e="T504" id="Seg_4622" s="T503">again</ta>
            <ta e="T505" id="Seg_4623" s="T504">make.peace-PST-1PL</ta>
            <ta e="T506" id="Seg_4624" s="T505">this</ta>
            <ta e="T507" id="Seg_4625" s="T506">day.[NOM]</ta>
            <ta e="T508" id="Seg_4626" s="T507">we.NOM</ta>
            <ta e="T509" id="Seg_4627" s="T508">bring-PST-1PL</ta>
            <ta e="T510" id="Seg_4628" s="T509">two</ta>
            <ta e="T511" id="Seg_4629" s="T510">squirrel-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_4630" s="T1">два</ta>
            <ta e="T3" id="Seg_4631" s="T2">десять</ta>
            <ta e="T4" id="Seg_4632" s="T3">два</ta>
            <ta e="T5" id="Seg_4633" s="T4">излишний-десять</ta>
            <ta e="T6" id="Seg_4634" s="T5">число-LOC.3SG</ta>
            <ta e="T7" id="Seg_4635" s="T6">октябрь-EP-GEN</ta>
            <ta e="T8" id="Seg_4636" s="T7">месяц-LOC.3SG</ta>
            <ta e="T9" id="Seg_4637" s="T8">один</ta>
            <ta e="T10" id="Seg_4638" s="T9">тысяча</ta>
            <ta e="T11" id="Seg_4639" s="T10">один</ta>
            <ta e="T12" id="Seg_4640" s="T11">девять</ta>
            <ta e="T13" id="Seg_4641" s="T12">сто</ta>
            <ta e="T14" id="Seg_4642" s="T13">шесть</ta>
            <ta e="T15" id="Seg_4643" s="T14">десять</ta>
            <ta e="T16" id="Seg_4644" s="T15">год-3SG</ta>
            <ta e="T17" id="Seg_4645" s="T16">год-LOC.3SG</ta>
            <ta e="T18" id="Seg_4646" s="T17">я.NOM</ta>
            <ta e="T19" id="Seg_4647" s="T18">уйти-PST-1SG.S</ta>
            <ta e="T20" id="Seg_4648" s="T19">охотиться-INCH-INF</ta>
            <ta e="T21" id="Seg_4649" s="T20">болото-GEN-через</ta>
            <ta e="T22" id="Seg_4650" s="T21">охотиться-CAP.ADJZ.[NOM]</ta>
            <ta e="T23" id="Seg_4651" s="T22">человек-GEN</ta>
            <ta e="T24" id="Seg_4652" s="T23">дом-ILL</ta>
            <ta e="T25" id="Seg_4653" s="T24">имя-3SG</ta>
            <ta e="T26" id="Seg_4654" s="T25">тайга-GEN</ta>
            <ta e="T27" id="Seg_4655" s="T26">местность-EP-GEN</ta>
            <ta e="T28" id="Seg_4656" s="T27">Митрий.[NOM]</ta>
            <ta e="T29" id="Seg_4657" s="T28">Сидорович-EP-GEN</ta>
            <ta e="T30" id="Seg_4658" s="T29">тайга.[NOM]</ta>
            <ta e="T31" id="Seg_4659" s="T30">этот-3SG</ta>
            <ta e="T32" id="Seg_4660" s="T31">быть-PST-3SG.S</ta>
            <ta e="T33" id="Seg_4661" s="T32">осень-ADV.LOC</ta>
            <ta e="T34" id="Seg_4662" s="T33">быстро</ta>
            <ta e="T35" id="Seg_4663" s="T34">виднеться.[3SG.S]</ta>
            <ta e="T36" id="Seg_4664" s="T35">осень-ADV.LOC</ta>
            <ta e="T37" id="Seg_4665" s="T36">я.NOM</ta>
            <ta e="T38" id="Seg_4666" s="T37">дорога-1SG</ta>
            <ta e="T39" id="Seg_4667" s="T38">быть-PST-3SG.S</ta>
            <ta e="T40" id="Seg_4668" s="T39">хороший</ta>
            <ta e="T41" id="Seg_4669" s="T40">‎‎поэтому</ta>
            <ta e="T42" id="Seg_4670" s="T41">хороший</ta>
            <ta e="T43" id="Seg_4671" s="T42">быть-PST.[3SG.S]</ta>
            <ta e="T44" id="Seg_4672" s="T43">нарты-PROL.3SG</ta>
            <ta e="T45" id="Seg_4673" s="T44">нутро-PROL</ta>
            <ta e="T46" id="Seg_4674" s="T45">болото-GEN</ta>
            <ta e="T47" id="Seg_4675" s="T46">через</ta>
            <ta e="T48" id="Seg_4676" s="T47">что-EP-PROL</ta>
            <ta e="T49" id="Seg_4677" s="T48">надо-TRL-%%-PST.[3SG.S]</ta>
            <ta e="T50" id="Seg_4678" s="T49">ехать-INF</ta>
            <ta e="T51" id="Seg_4679" s="T50">замерзнуть-PST.NAR-PST.[3SG.S]</ta>
            <ta e="T52" id="Seg_4680" s="T51">и</ta>
            <ta e="T53" id="Seg_4681" s="T52">три</ta>
            <ta e="T54" id="Seg_4682" s="T53">друг.[NOM]</ta>
            <ta e="T55" id="Seg_4683" s="T54">уйти-DUR-PST-3PL</ta>
            <ta e="T56" id="Seg_4684" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_4685" s="T56">перед-ADV.LOC</ta>
            <ta e="T58" id="Seg_4686" s="T57">он(а)-EP-PL-GEN</ta>
            <ta e="T59" id="Seg_4687" s="T58">имя-3SG</ta>
            <ta e="T60" id="Seg_4688" s="T59">Василий</ta>
            <ta e="T61" id="Seg_4689" s="T60">Максим</ta>
            <ta e="T62" id="Seg_4690" s="T61">Максим-EP-GEN</ta>
            <ta e="T63" id="Seg_4691" s="T62">младший.брат/сестра-GEN-3SG</ta>
            <ta e="T64" id="Seg_4692" s="T63">имя-3SG</ta>
            <ta e="T65" id="Seg_4693" s="T64">я.NOM</ta>
            <ta e="T66" id="Seg_4694" s="T65">забыть-PST.NAR-1SG.O</ta>
            <ta e="T67" id="Seg_4695" s="T66">он(а)-EP-PL</ta>
            <ta e="T68" id="Seg_4696" s="T67">уйти-IPFV-PST.NAR-3PL</ta>
            <ta e="T69" id="Seg_4697" s="T68">лыжа</ta>
            <ta e="T70" id="Seg_4698" s="T69">дорога-ACC</ta>
            <ta e="T71" id="Seg_4699" s="T70">он(а)-EP-PL-GEN</ta>
            <ta e="T72" id="Seg_4700" s="T71">лыжа-дорога.[NOM]</ta>
            <ta e="T73" id="Seg_4701" s="T72">замерзнуть-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_4702" s="T73">этот</ta>
            <ta e="T75" id="Seg_4703" s="T74">день.[NOM]</ta>
            <ta e="T76" id="Seg_4704" s="T75">какой-ADVZ</ta>
            <ta e="T77" id="Seg_4705" s="T76">хотеть-PST-1SG.S</ta>
            <ta e="T78" id="Seg_4706" s="T77">встречать-RES-INF</ta>
            <ta e="T79" id="Seg_4707" s="T78">белка-DIM-COM</ta>
            <ta e="T80" id="Seg_4708" s="T79">NEG</ta>
            <ta e="T81" id="Seg_4709" s="T80">встречать-RES-EP-1SG.S</ta>
            <ta e="T82" id="Seg_4710" s="T81">один</ta>
            <ta e="T83" id="Seg_4711" s="T82">белка-ACC</ta>
            <ta e="T84" id="Seg_4712" s="T83">собака.[NOM]-1SG</ta>
            <ta e="T85" id="Seg_4713" s="T84">собака-PL-1SG</ta>
            <ta e="T86" id="Seg_4714" s="T85">залаять-DUR-PST-3SG.O</ta>
            <ta e="T87" id="Seg_4715" s="T86">я.NOM</ta>
            <ta e="T88" id="Seg_4716" s="T87">смотреть-DUR-PST-1SG.O</ta>
            <ta e="T89" id="Seg_4717" s="T88">кедр-LOC</ta>
            <ta e="T90" id="Seg_4718" s="T89">NEG</ta>
            <ta e="T91" id="Seg_4719" s="T90">увидеть-PST-1SG.O</ta>
            <ta e="T92" id="Seg_4720" s="T91">собака.[NOM]-1SG</ta>
            <ta e="T93" id="Seg_4721" s="T92">найти-EP-3SG.O</ta>
            <ta e="T94" id="Seg_4722" s="T93">два-ORD</ta>
            <ta e="T95" id="Seg_4723" s="T94">белка.[NOM]</ta>
            <ta e="T96" id="Seg_4724" s="T95">сосна-LOC</ta>
            <ta e="T97" id="Seg_4725" s="T96">я.NOM</ta>
            <ta e="T98" id="Seg_4726" s="T97">увидеть-1SG.O</ta>
            <ta e="T99" id="Seg_4727" s="T98">он(а)-EP-ACC</ta>
            <ta e="T100" id="Seg_4728" s="T99">стрелять-INF</ta>
            <ta e="T101" id="Seg_4729" s="T100">NEG</ta>
            <ta e="T102" id="Seg_4730" s="T101">быть-PST-1SG.S</ta>
            <ta e="T103" id="Seg_4731" s="T102">‎‎поэтому</ta>
            <ta e="T104" id="Seg_4732" s="T103">он(а)-EP-ACC</ta>
            <ta e="T105" id="Seg_4733" s="T104">NEG</ta>
            <ta e="T106" id="Seg_4734" s="T105">убить-PST-1SG.O</ta>
            <ta e="T107" id="Seg_4735" s="T106">он(а).[NOM]</ta>
            <ta e="T108" id="Seg_4736" s="T107">красный-PST.[3SG.S]</ta>
            <ta e="T109" id="Seg_4737" s="T108">красный</ta>
            <ta e="T110" id="Seg_4738" s="T109">белка-ACC</ta>
            <ta e="T111" id="Seg_4739" s="T110">NEG</ta>
            <ta e="T112" id="Seg_4740" s="T111">стрелять-HAB-1PL</ta>
            <ta e="T113" id="Seg_4741" s="T112">NEG</ta>
            <ta e="T114" id="Seg_4742" s="T113">дойти-CVB</ta>
            <ta e="T115" id="Seg_4743" s="T114">местность-EP-GEN</ta>
            <ta e="T116" id="Seg_4744" s="T115">имя.[NOM]-3SG</ta>
            <ta e="T117" id="Seg_4745" s="T116">Митрий.[NOM]</ta>
            <ta e="T118" id="Seg_4746" s="T117">Сидорович-EP-GEN</ta>
            <ta e="T119" id="Seg_4747" s="T118">и</ta>
            <ta e="T120" id="Seg_4748" s="T119">Луканти-GEN</ta>
            <ta e="T121" id="Seg_4749" s="T120">доска.[NOM]</ta>
            <ta e="T122" id="Seg_4750" s="T121">дом-ILL</ta>
            <ta e="T123" id="Seg_4751" s="T122">сломаться-3SG.S</ta>
            <ta e="T124" id="Seg_4752" s="T123">нарты-1SG</ta>
            <ta e="T125" id="Seg_4753" s="T124">этот</ta>
            <ta e="T126" id="Seg_4754" s="T125">нарты-1SG</ta>
            <ta e="T127" id="Seg_4755" s="T126">дать-PTCP.PST</ta>
            <ta e="T128" id="Seg_4756" s="T127">и</ta>
            <ta e="T129" id="Seg_4757" s="T128">найти-PTCP.PST</ta>
            <ta e="T130" id="Seg_4758" s="T129">я.NOM</ta>
            <ta e="T131" id="Seg_4759" s="T130">старуха-GEN-ALL</ta>
            <ta e="T132" id="Seg_4760" s="T131">дорога.[NOM]</ta>
            <ta e="T133" id="Seg_4761" s="T132">быть-PST-3SG.S</ta>
            <ta e="T134" id="Seg_4762" s="T133">NEG</ta>
            <ta e="T135" id="Seg_4763" s="T134">хороший</ta>
            <ta e="T136" id="Seg_4764" s="T135">уйти-PTCP.PRS</ta>
            <ta e="T137" id="Seg_4765" s="T136">человек-PL-GEN</ta>
            <ta e="T138" id="Seg_4766" s="T137">спина-LOC.3SG</ta>
            <ta e="T139" id="Seg_4767" s="T138">нарты-PROL.3SG</ta>
            <ta e="T140" id="Seg_4768" s="T139">NEG</ta>
            <ta e="T141" id="Seg_4769" s="T140">замерзнуть-DUR-PST.[3SG.S]</ta>
            <ta e="T142" id="Seg_4770" s="T141">вечер-TRL-CVB</ta>
            <ta e="T143" id="Seg_4771" s="T142">начать-3SG.S</ta>
            <ta e="T144" id="Seg_4772" s="T143">нарты-1SG</ta>
            <ta e="T145" id="Seg_4773" s="T144">сломаться-3SG.S</ta>
            <ta e="T146" id="Seg_4774" s="T145">что-PL-1SG</ta>
            <ta e="T147" id="Seg_4775" s="T146">и</ta>
            <ta e="T148" id="Seg_4776" s="T147">сломаться-PTCP.PST</ta>
            <ta e="T149" id="Seg_4777" s="T148">нарты-%%-1SG</ta>
            <ta e="T150" id="Seg_4778" s="T149">и</ta>
            <ta e="T151" id="Seg_4779" s="T150">что-PL-1SG</ta>
            <ta e="T152" id="Seg_4780" s="T151">спина-CVB</ta>
            <ta e="T153" id="Seg_4781" s="T152">уйти-TR-1SG.O</ta>
            <ta e="T154" id="Seg_4782" s="T153">болото-GEN</ta>
            <ta e="T155" id="Seg_4783" s="T154">нога.[NOM]</ta>
            <ta e="T156" id="Seg_4784" s="T155">кедр.[NOM]</ta>
            <ta e="T157" id="Seg_4785" s="T156">мыс-EP-ILL</ta>
            <ta e="T158" id="Seg_4786" s="T157">кедр.[NOM]</ta>
            <ta e="T159" id="Seg_4787" s="T158">мыс-EP-LOC</ta>
            <ta e="T160" id="Seg_4788" s="T159">положить-CO-1SG.O</ta>
            <ta e="T161" id="Seg_4789" s="T160">огонь-ACC</ta>
            <ta e="T162" id="Seg_4790" s="T161">сломать-CO-1SG.O</ta>
            <ta e="T163" id="Seg_4791" s="T162">кедр-GEN</ta>
            <ta e="T164" id="Seg_4792" s="T163">ветка-PL-ACC</ta>
            <ta e="T165" id="Seg_4793" s="T164">делать-CO-1SG.O</ta>
            <ta e="T166" id="Seg_4794" s="T165">сесть-PST.[3SG.S]</ta>
            <ta e="T167" id="Seg_4795" s="T166">улечься.спать-PST-3SG</ta>
            <ta e="T168" id="Seg_4796" s="T167">место-ACC</ta>
            <ta e="T169" id="Seg_4797" s="T168">Матвей.[NOM]</ta>
            <ta e="T170" id="Seg_4798" s="T169">твёрдый-ADVZ</ta>
            <ta e="T171" id="Seg_4799" s="T170">остановить-FRQ-EP-3SG.S</ta>
            <ta e="T172" id="Seg_4800" s="T171">мы.DU.NOM-ADES</ta>
            <ta e="T173" id="Seg_4801" s="T172">один</ta>
            <ta e="T174" id="Seg_4802" s="T173">думать.[3SG.S]</ta>
            <ta e="T175" id="Seg_4803" s="T174">как</ta>
            <ta e="T176" id="Seg_4804" s="T175">отремонтировать-INF</ta>
            <ta e="T177" id="Seg_4805" s="T176">сломаться-PTCP.PST</ta>
            <ta e="T178" id="Seg_4806" s="T177">нарты-ACC</ta>
            <ta e="T179" id="Seg_4807" s="T178">мы.DU.NOM-ADES</ta>
            <ta e="T180" id="Seg_4808" s="T179">быть-PST-3PL</ta>
            <ta e="T181" id="Seg_4809" s="T180">заяц-GEN</ta>
            <ta e="T182" id="Seg_4810" s="T181">петля-EP-PL</ta>
            <ta e="T183" id="Seg_4811" s="T182">два</ta>
            <ta e="T184" id="Seg_4812" s="T183">десять</ta>
            <ta e="T185" id="Seg_4813" s="T184">два</ta>
            <ta e="T186" id="Seg_4814" s="T185">излишний-десять</ta>
            <ta e="T187" id="Seg_4815" s="T186">два</ta>
            <ta e="T188" id="Seg_4816" s="T187">десять</ta>
            <ta e="T189" id="Seg_4817" s="T188">три</ta>
            <ta e="T190" id="Seg_4818" s="T189">излишний-десять</ta>
            <ta e="T191" id="Seg_4819" s="T190">ночь-ADV.LOC</ta>
            <ta e="T192" id="Seg_4820" s="T191">я.NOM</ta>
            <ta e="T193" id="Seg_4821" s="T192">работать-PST-1SG.S</ta>
            <ta e="T194" id="Seg_4822" s="T193">твёрдый-ADVZ</ta>
            <ta e="T195" id="Seg_4823" s="T194">%%-HAB-PST-1SG.O</ta>
            <ta e="T196" id="Seg_4824" s="T195">сам.1SG</ta>
            <ta e="T197" id="Seg_4825" s="T196">нарты-ACC</ta>
            <ta e="T198" id="Seg_4826" s="T197">%%</ta>
            <ta e="T199" id="Seg_4827" s="T198">принести-FRQ-EP-INF</ta>
            <ta e="T200" id="Seg_4828" s="T199">что-PL-ACC</ta>
            <ta e="T201" id="Seg_4829" s="T200">%%-ADJZ</ta>
            <ta e="T202" id="Seg_4830" s="T201">я.ALL</ta>
            <ta e="T203" id="Seg_4831" s="T202">тяжело</ta>
            <ta e="T204" id="Seg_4832" s="T203">быть-FUT-3SG.S</ta>
            <ta e="T205" id="Seg_4833" s="T204">‎‎поэтому</ta>
            <ta e="T206" id="Seg_4834" s="T205">ночь-ADV.LOC</ta>
            <ta e="T207" id="Seg_4835" s="T206">хороший-ADVZ</ta>
            <ta e="T208" id="Seg_4836" s="T207">NEG</ta>
            <ta e="T209" id="Seg_4837" s="T208">спать-PST-3SG.S</ta>
            <ta e="T210" id="Seg_4838" s="T209">нарты-ACC</ta>
            <ta e="T211" id="Seg_4839" s="T210">делать-HAB-PST-1SG.O</ta>
            <ta e="T212" id="Seg_4840" s="T211">ночь-ADV.LOC</ta>
            <ta e="T213" id="Seg_4841" s="T212">середина-GEN</ta>
            <ta e="T214" id="Seg_4842" s="T213">до</ta>
            <ta e="T215" id="Seg_4843" s="T214">один-EP-ADVZ</ta>
            <ta e="T216" id="Seg_4844" s="T215">ночь-ADV.LOC</ta>
            <ta e="T217" id="Seg_4845" s="T216">собака.[NOM]-1SG</ta>
            <ta e="T218" id="Seg_4846" s="T217">залаять-INF</ta>
            <ta e="T219" id="Seg_4847" s="T218">быть-PST-3SG.S</ta>
            <ta e="T220" id="Seg_4848" s="T219">два-ADVZ</ta>
            <ta e="T221" id="Seg_4849" s="T220">три-ADVZ</ta>
            <ta e="T222" id="Seg_4850" s="T221">тявкнуть-PST.[3SG.S]</ta>
            <ta e="T223" id="Seg_4851" s="T222">я.NOM</ta>
            <ta e="T224" id="Seg_4852" s="T223">NEG</ta>
            <ta e="T225" id="Seg_4853" s="T224">бояться-DUR-PST-1SG.S</ta>
            <ta e="T226" id="Seg_4854" s="T225">я.NOM</ta>
            <ta e="T227" id="Seg_4855" s="T226">сам.1SG</ta>
            <ta e="T228" id="Seg_4856" s="T227">себя-1SG</ta>
            <ta e="T229" id="Seg_4857" s="T228">поймать-DUR-PST-1SG.O</ta>
            <ta e="T230" id="Seg_4858" s="T229">как</ta>
            <ta e="T231" id="Seg_4859" s="T230">хозяин.[NOM]</ta>
            <ta e="T232" id="Seg_4860" s="T231">сам.1SG</ta>
            <ta e="T233" id="Seg_4861" s="T232">огонь-%%</ta>
            <ta e="T234" id="Seg_4862" s="T233">я.NOM</ta>
            <ta e="T235" id="Seg_4863" s="T234">смотреть-DUR-PST-1SG.O</ta>
            <ta e="T236" id="Seg_4864" s="T235">кедр.[NOM]</ta>
            <ta e="T237" id="Seg_4865" s="T236">остров-ILL</ta>
            <ta e="T238" id="Seg_4866" s="T237">большой</ta>
            <ta e="T239" id="Seg_4867" s="T238">болото-GEN</ta>
            <ta e="T240" id="Seg_4868" s="T239">нога.[NOM]</ta>
            <ta e="T241" id="Seg_4869" s="T240">я.NOM</ta>
            <ta e="T242" id="Seg_4870" s="T241">думать-PST-1SG.S</ta>
            <ta e="T243" id="Seg_4871" s="T242">утро</ta>
            <ta e="T244" id="Seg_4872" s="T243">вперёд</ta>
            <ta e="T245" id="Seg_4873" s="T244">ехать-FUT-1SG.S</ta>
            <ta e="T246" id="Seg_4874" s="T245">этот</ta>
            <ta e="T247" id="Seg_4875" s="T246">нарты.[NOM]</ta>
            <ta e="T248" id="Seg_4876" s="T247">дорога.[NOM]</ta>
            <ta e="T249" id="Seg_4877" s="T248">нутро-PROL</ta>
            <ta e="T250" id="Seg_4878" s="T249">двадцать</ta>
            <ta e="T251" id="Seg_4879" s="T250">три-ORD</ta>
            <ta e="T252" id="Seg_4880" s="T251">утро-нечто-LOC</ta>
            <ta e="T253" id="Seg_4881" s="T252">долго</ta>
            <ta e="T254" id="Seg_4882" s="T253">NEG</ta>
            <ta e="T255" id="Seg_4883" s="T254">собирать-INCH-DUR-PST-1SG.S</ta>
            <ta e="T256" id="Seg_4884" s="T255">ехать-INF</ta>
            <ta e="T257" id="Seg_4885" s="T256">хороший</ta>
            <ta e="T258" id="Seg_4886" s="T257">быть-PST.[3SG.S]</ta>
            <ta e="T259" id="Seg_4887" s="T258">осень-ADJZ</ta>
            <ta e="T260" id="Seg_4888" s="T259">нарты.[NOM]</ta>
            <ta e="T261" id="Seg_4889" s="T260">дорога-PROL</ta>
            <ta e="T262" id="Seg_4890" s="T261">что</ta>
            <ta e="T263" id="Seg_4891" s="T262">замерзнуть-DUR-PST.[3SG.S]</ta>
            <ta e="T264" id="Seg_4892" s="T263">один</ta>
            <ta e="T265" id="Seg_4893" s="T264">излишний-десять</ta>
            <ta e="T266" id="Seg_4894" s="T265">час-LOC.3SG</ta>
            <ta e="T267" id="Seg_4895" s="T266">день-ADV.LOC</ta>
            <ta e="T268" id="Seg_4896" s="T267">я.NOM</ta>
            <ta e="T269" id="Seg_4897" s="T268">быть-PST-1SG.S</ta>
            <ta e="T270" id="Seg_4898" s="T269">три</ta>
            <ta e="T271" id="Seg_4899" s="T270">зверь-CAP.ADJZ</ta>
            <ta e="T272" id="Seg_4900" s="T271">друг-PL-GEN-ADES</ta>
            <ta e="T273" id="Seg_4901" s="T272">он(а)-EP-PL.[NOM]</ta>
            <ta e="T274" id="Seg_4902" s="T273">белка-ACC</ta>
            <ta e="T275" id="Seg_4903" s="T274">NEG</ta>
            <ta e="T276" id="Seg_4904" s="T275">убить-HAB-PST-3PL</ta>
            <ta e="T277" id="Seg_4905" s="T276">прийти-CVB2</ta>
            <ta e="T278" id="Seg_4906" s="T277">охотиться-CAP.ADJZ</ta>
            <ta e="T279" id="Seg_4907" s="T278">дом-ILL</ta>
            <ta e="T280" id="Seg_4908" s="T279">я.NOM</ta>
            <ta e="T281" id="Seg_4909" s="T280">уйти-PST-1SG.S</ta>
            <ta e="T282" id="Seg_4910" s="T281">охотиться-INF</ta>
            <ta e="T283" id="Seg_4911" s="T282">этот</ta>
            <ta e="T284" id="Seg_4912" s="T283">день.[NOM]</ta>
            <ta e="T285" id="Seg_4913" s="T284">я.NOM</ta>
            <ta e="T286" id="Seg_4914" s="T285">убить-CO-1SG.O</ta>
            <ta e="T287" id="Seg_4915" s="T286">один</ta>
            <ta e="T288" id="Seg_4916" s="T287">белка-ACC</ta>
            <ta e="T289" id="Seg_4917" s="T288">этот</ta>
            <ta e="T290" id="Seg_4918" s="T289">этот</ta>
            <ta e="T291" id="Seg_4919" s="T290">чёрный-быть-PST.[NOM]</ta>
            <ta e="T292" id="Seg_4920" s="T291">и</ta>
            <ta e="T293" id="Seg_4921" s="T292">один</ta>
            <ta e="T294" id="Seg_4922" s="T293">глухарь-ACC</ta>
            <ta e="T295" id="Seg_4923" s="T294">быть-PST-3SG.S</ta>
            <ta e="T296" id="Seg_4924" s="T295">воскресный</ta>
            <ta e="T297" id="Seg_4925" s="T296">солнце.[NOM]-3SG</ta>
            <ta e="T298" id="Seg_4926" s="T297">два-ORD</ta>
            <ta e="T299" id="Seg_4927" s="T298">день.[NOM]</ta>
            <ta e="T300" id="Seg_4928" s="T299">охотиться-PST-1SG.S</ta>
            <ta e="T301" id="Seg_4929" s="T300">убить-PST-1SG.O</ta>
            <ta e="T302" id="Seg_4930" s="T301">два</ta>
            <ta e="T303" id="Seg_4931" s="T302">белка-ACC</ta>
            <ta e="T304" id="Seg_4932" s="T303">он(а)-DU</ta>
            <ta e="T305" id="Seg_4933" s="T304">быть-PST-3DU.S</ta>
            <ta e="T306" id="Seg_4934" s="T305">синий</ta>
            <ta e="T307" id="Seg_4935" s="T306">рука</ta>
            <ta e="T308" id="Seg_4936" s="T307">чёрный</ta>
            <ta e="T309" id="Seg_4937" s="T308">шкура.[NOM]</ta>
            <ta e="T310" id="Seg_4938" s="T309">сердце.[NOM]-1SG</ta>
            <ta e="T311" id="Seg_4939" s="T310">NEG</ta>
            <ta e="T312" id="Seg_4940" s="T311">радоваться-RES-PST.NAR.[3SG.S]</ta>
            <ta e="T313" id="Seg_4941" s="T312">три-ORD</ta>
            <ta e="T314" id="Seg_4942" s="T313">день.[NOM]</ta>
            <ta e="T315" id="Seg_4943" s="T314">быть-PST-3SG.S</ta>
            <ta e="T316" id="Seg_4944" s="T315">хороший</ta>
            <ta e="T317" id="Seg_4945" s="T316">день.[NOM]-3SG</ta>
            <ta e="T318" id="Seg_4946" s="T317">этот</ta>
            <ta e="T319" id="Seg_4947" s="T318">день.[NOM]</ta>
            <ta e="T320" id="Seg_4948" s="T319">что-ACC-EMPH-NEG</ta>
            <ta e="T321" id="Seg_4949" s="T320">убить-PST-1SG.O</ta>
            <ta e="T322" id="Seg_4950" s="T321">день.[NOM]-3SG</ta>
            <ta e="T323" id="Seg_4951" s="T322">быть-PST-3SG.S</ta>
            <ta e="T324" id="Seg_4952" s="T323">день.[NOM]-3SG</ta>
            <ta e="T325" id="Seg_4953" s="T324">день-TRL-PTCP.PST</ta>
            <ta e="T326" id="Seg_4954" s="T325">я.NOM</ta>
            <ta e="T327" id="Seg_4955" s="T326">старуха-ADES</ta>
            <ta e="T328" id="Seg_4956" s="T327">этот</ta>
            <ta e="T329" id="Seg_4957" s="T328">день.[NOM]</ta>
            <ta e="T330" id="Seg_4958" s="T329">друг-PL-COM</ta>
            <ta e="T331" id="Seg_4959" s="T330">один</ta>
            <ta e="T332" id="Seg_4960" s="T331">нечто-LOC</ta>
            <ta e="T333" id="Seg_4961" s="T332">ходить-PST-1PL</ta>
            <ta e="T334" id="Seg_4962" s="T333">болото-GEN</ta>
            <ta e="T335" id="Seg_4963" s="T334">через</ta>
            <ta e="T336" id="Seg_4964" s="T335">Деревянка-ILL</ta>
            <ta e="T337" id="Seg_4965" s="T336">туда-ADV.LOC</ta>
            <ta e="T338" id="Seg_4966" s="T337">мы.NOM</ta>
            <ta e="T339" id="Seg_4967" s="T338">оставить-PST-1PL</ta>
            <ta e="T340" id="Seg_4968" s="T339">дерево-ILL</ta>
            <ta e="T341" id="Seg_4969" s="T340">писать-FRQ-CVB</ta>
            <ta e="T342" id="Seg_4970" s="T341">свой.1PL</ta>
            <ta e="T343" id="Seg_4971" s="T342">имя-PL-1PL</ta>
            <ta e="T344" id="Seg_4972" s="T343">а</ta>
            <ta e="T345" id="Seg_4973" s="T344">мы.NOM</ta>
            <ta e="T346" id="Seg_4974" s="T345">имя-1PL</ta>
            <ta e="T347" id="Seg_4975" s="T346">Матвей.[NOM]</ta>
            <ta e="T348" id="Seg_4976" s="T347">Максим.[NOM]</ta>
            <ta e="T349" id="Seg_4977" s="T348">и</ta>
            <ta e="T350" id="Seg_4978" s="T349">Васка.[NOM]</ta>
            <ta e="T351" id="Seg_4979" s="T350">этот</ta>
            <ta e="T352" id="Seg_4980" s="T351">день.[NOM]</ta>
            <ta e="T353" id="Seg_4981" s="T352">мы.NOM</ta>
            <ta e="T354" id="Seg_4982" s="T353">принести-PST-1PL</ta>
            <ta e="T355" id="Seg_4983" s="T354">три</ta>
            <ta e="T356" id="Seg_4984" s="T355">принести-CO-3PL</ta>
            <ta e="T357" id="Seg_4985" s="T356">умереть-PTCP.PST</ta>
            <ta e="T358" id="Seg_4986" s="T357">отец-ACC</ta>
            <ta e="T359" id="Seg_4987" s="T358">Семён-EP-ACC</ta>
            <ta e="T360" id="Seg_4988" s="T359">мы.ALL</ta>
            <ta e="T361" id="Seg_4989" s="T360">быть-PST-3SG.S</ta>
            <ta e="T362" id="Seg_4990" s="T361">сильно</ta>
            <ta e="T363" id="Seg_4991" s="T362">NEG</ta>
            <ta e="T364" id="Seg_4992" s="T363">хороший-ADVZ</ta>
            <ta e="T365" id="Seg_4993" s="T364">четыре-ORD</ta>
            <ta e="T366" id="Seg_4994" s="T365">день.[NOM]</ta>
            <ta e="T367" id="Seg_4995" s="T366">утро-у</ta>
            <ta e="T368" id="Seg_4996" s="T367">тёплый</ta>
            <ta e="T369" id="Seg_4997" s="T368">быть-PST.[3SG.S]</ta>
            <ta e="T370" id="Seg_4998" s="T369">вечер-PROL</ta>
            <ta e="T371" id="Seg_4999" s="T370">быть-PST-3SG.S</ta>
            <ta e="T372" id="Seg_5000" s="T371">дождь.[NOM]-3SG</ta>
            <ta e="T373" id="Seg_5001" s="T372">день.[NOM]-3SG</ta>
            <ta e="T374" id="Seg_5002" s="T373">тяжело</ta>
            <ta e="T375" id="Seg_5003" s="T374">быть-PST.[3SG.S]</ta>
            <ta e="T376" id="Seg_5004" s="T375">ходить-INF</ta>
            <ta e="T377" id="Seg_5005" s="T376">нога-PL.[NOM]</ta>
            <ta e="T378" id="Seg_5006" s="T377">устать-DUR-PST-3PL</ta>
            <ta e="T379" id="Seg_5007" s="T378">этот</ta>
            <ta e="T380" id="Seg_5008" s="T379">день.[NOM]</ta>
            <ta e="T381" id="Seg_5009" s="T380">принести-CO-1SG.O</ta>
            <ta e="T382" id="Seg_5010" s="T381">три</ta>
            <ta e="T383" id="Seg_5011" s="T382">белка-ACC</ta>
            <ta e="T384" id="Seg_5012" s="T383">пять-ORD</ta>
            <ta e="T385" id="Seg_5013" s="T384">день.[NOM]</ta>
            <ta e="T386" id="Seg_5014" s="T385">один</ta>
            <ta e="T387" id="Seg_5015" s="T386">друг.[NOM]</ta>
            <ta e="T388" id="Seg_5016" s="T387">имя.[NOM]-3SG</ta>
            <ta e="T389" id="Seg_5017" s="T388">он(а)-EP-GEN</ta>
            <ta e="T390" id="Seg_5018" s="T389">Василий.[NOM]</ta>
            <ta e="T391" id="Seg_5019" s="T390">уйти-PST.[3SG.S]</ta>
            <ta e="T392" id="Seg_5020" s="T391">дом-ILL.3SG</ta>
            <ta e="T393" id="Seg_5021" s="T392">этот</ta>
            <ta e="T394" id="Seg_5022" s="T393">день.[NOM]</ta>
            <ta e="T395" id="Seg_5023" s="T394">принести-PST-1SG.O</ta>
            <ta e="T396" id="Seg_5024" s="T395">четыре</ta>
            <ta e="T397" id="Seg_5025" s="T396">белка-ACC</ta>
            <ta e="T398" id="Seg_5026" s="T397">шесть-ORD</ta>
            <ta e="T399" id="Seg_5027" s="T398">день.[NOM]</ta>
            <ta e="T400" id="Seg_5028" s="T399">ходить-PST-1SG.S</ta>
            <ta e="T401" id="Seg_5029" s="T400">круглый</ta>
            <ta e="T402" id="Seg_5030" s="T401">болото-GEN</ta>
            <ta e="T403" id="Seg_5031" s="T402">через</ta>
            <ta e="T404" id="Seg_5032" s="T403">%%-3SG.S</ta>
            <ta e="T405" id="Seg_5033" s="T404">ехать-PST-3SG.S</ta>
            <ta e="T406" id="Seg_5034" s="T405">‎‎поэтому</ta>
            <ta e="T407" id="Seg_5035" s="T406">слушать-DUR-CVB</ta>
            <ta e="T408" id="Seg_5036" s="T407">когда</ta>
            <ta e="T409" id="Seg_5037" s="T408">лаять-EP-PFV-FUT-3SG.S</ta>
            <ta e="T410" id="Seg_5038" s="T409">я.GEN</ta>
            <ta e="T411" id="Seg_5039" s="T410">собака.[NOM]-1SG</ta>
            <ta e="T412" id="Seg_5040" s="T411">имя.[NOM]-3SG</ta>
            <ta e="T413" id="Seg_5041" s="T412">он(а)-EP-GEN</ta>
            <ta e="T414" id="Seg_5042" s="T413">Эду.[NOM]</ta>
            <ta e="T415" id="Seg_5043" s="T414">слышать-IPFV-1SG.O</ta>
            <ta e="T416" id="Seg_5044" s="T415">слышать-%%-CO-1SG.O</ta>
            <ta e="T417" id="Seg_5045" s="T416">собака.[NOM]-1SG</ta>
            <ta e="T418" id="Seg_5046" s="T417">лаять-EP-3SG.S</ta>
            <ta e="T419" id="Seg_5047" s="T418">белка-ILL</ta>
            <ta e="T420" id="Seg_5048" s="T419">дойти-IPFV3-INF</ta>
            <ta e="T421" id="Seg_5049" s="T420">Эду-ALL</ta>
            <ta e="T422" id="Seg_5050" s="T421">он(а).[NOM]</ta>
            <ta e="T423" id="Seg_5051" s="T422">хороший-ADVZ</ta>
            <ta e="T424" id="Seg_5052" s="T423">лаять-EP-3SG.S</ta>
            <ta e="T425" id="Seg_5053" s="T424">большой</ta>
            <ta e="T426" id="Seg_5054" s="T425">высокий</ta>
            <ta e="T427" id="Seg_5055" s="T426">кедр-ILL</ta>
            <ta e="T428" id="Seg_5056" s="T427">смотреть-DUR-1SG.S</ta>
            <ta e="T429" id="Seg_5057" s="T428">вверх</ta>
            <ta e="T430" id="Seg_5058" s="T429">NEG</ta>
            <ta e="T431" id="Seg_5059" s="T430">увидеть-HAB-CO-1SG.O</ta>
            <ta e="T432" id="Seg_5060" s="T431">белка-ACC</ta>
            <ta e="T433" id="Seg_5061" s="T432">бояться-TR-INF</ta>
            <ta e="T434" id="Seg_5062" s="T433">много</ta>
            <ta e="T435" id="Seg_5063" s="T434">стрелять-PST-3SG.S</ta>
            <ta e="T436" id="Seg_5064" s="T435">тозовка-INSTR</ta>
            <ta e="T437" id="Seg_5065" s="T436">а</ta>
            <ta e="T438" id="Seg_5066" s="T437">потом</ta>
            <ta e="T439" id="Seg_5067" s="T438">NEG</ta>
            <ta e="T440" id="Seg_5068" s="T439">один-EP-ADVZ</ta>
            <ta e="T441" id="Seg_5069" s="T440">стрелять-PST-1SG.S</ta>
            <ta e="T442" id="Seg_5070" s="T441">два</ta>
            <ta e="T443" id="Seg_5071" s="T442">внутренность.[NOM]</ta>
            <ta e="T444" id="Seg_5072" s="T443">ружье-INSTR</ta>
            <ta e="T445" id="Seg_5073" s="T444">хотеть-PST-1SG.S</ta>
            <ta e="T446" id="Seg_5074" s="T445">я.NOM</ta>
            <ta e="T447" id="Seg_5075" s="T446">он(а)-EP-ACC</ta>
            <ta e="T448" id="Seg_5076" s="T447">увидеть-PFV-INF</ta>
            <ta e="T449" id="Seg_5077" s="T448">я.NOM</ta>
            <ta e="T450" id="Seg_5078" s="T449">он(а)-EP-ACC</ta>
            <ta e="T451" id="Seg_5079" s="T450">увидеть-PFV-1SG.O</ta>
            <ta e="T452" id="Seg_5080" s="T451">кедр-GEN</ta>
            <ta e="T453" id="Seg_5081" s="T452">голова-LOC</ta>
            <ta e="T454" id="Seg_5082" s="T453">NEG</ta>
            <ta e="T455" id="Seg_5083" s="T454">три-EP-ADVZ</ta>
            <ta e="T456" id="Seg_5084" s="T455">четыре-ADVZ</ta>
            <ta e="T457" id="Seg_5085" s="T456">кедр-GEN</ta>
            <ta e="T458" id="Seg_5086" s="T457">голова-ILL</ta>
            <ta e="T459" id="Seg_5087" s="T458">стрелять-HAB-PST-3SG.S</ta>
            <ta e="T460" id="Seg_5088" s="T459">и</ta>
            <ta e="T461" id="Seg_5089" s="T460">оставить-1SG.O</ta>
            <ta e="T462" id="Seg_5090" s="T461">курить-CO-1SG.S</ta>
            <ta e="T463" id="Seg_5091" s="T462">стоять-EP-CO-1SG.S</ta>
            <ta e="T464" id="Seg_5092" s="T463">NEG</ta>
            <ta e="T465" id="Seg_5093" s="T464">хотеть-PST-1SG.S</ta>
            <ta e="T466" id="Seg_5094" s="T465">оставить-INF</ta>
            <ta e="T467" id="Seg_5095" s="T466">белка-ACC</ta>
            <ta e="T468" id="Seg_5096" s="T467">смотреть-DUR-1SG.S</ta>
            <ta e="T469" id="Seg_5097" s="T468">я.NOM</ta>
            <ta e="T470" id="Seg_5098" s="T469">тайга-ADJZ</ta>
            <ta e="T471" id="Seg_5099" s="T470">друг.[NOM]-1SG</ta>
            <ta e="T472" id="Seg_5100" s="T471">имя.[NOM]-3SG</ta>
            <ta e="T473" id="Seg_5101" s="T472">он(а)-EP-GEN</ta>
            <ta e="T474" id="Seg_5102" s="T473">Эду.[NOM]</ta>
            <ta e="T475" id="Seg_5103" s="T474">что-ACC-INDEF4</ta>
            <ta e="T476" id="Seg_5104" s="T475">делать-HAB-3SG.O</ta>
            <ta e="T477" id="Seg_5105" s="T476">бить-HAB-PTCP.PRS</ta>
            <ta e="T478" id="Seg_5106" s="T477">кедр-GEN</ta>
            <ta e="T479" id="Seg_5107" s="T478">%%-LOC</ta>
            <ta e="T480" id="Seg_5108" s="T479">я.NOM</ta>
            <ta e="T481" id="Seg_5109" s="T480">лыжа-INSTR2</ta>
            <ta e="T482" id="Seg_5110" s="T481">кататься-CO-1SG.S</ta>
            <ta e="T483" id="Seg_5111" s="T482">он(а)-EP-ALL</ta>
            <ta e="T484" id="Seg_5112" s="T483">он(а).[NOM]</ta>
            <ta e="T485" id="Seg_5113" s="T484">упасть-RES-PTCP.PST</ta>
            <ta e="T486" id="Seg_5114" s="T485">белка-ACC</ta>
            <ta e="T487" id="Seg_5115" s="T486">где</ta>
            <ta e="T488" id="Seg_5116" s="T487">я.NOM</ta>
            <ta e="T489" id="Seg_5117" s="T488">стоять-EP-PST-1SG.S</ta>
            <ta e="T490" id="Seg_5118" s="T489">курить-PST-1SG.S</ta>
            <ta e="T491" id="Seg_5119" s="T490">он(а).[NOM]</ta>
            <ta e="T492" id="Seg_5120" s="T491">съесть-EP-PST.NAR-3SG.O</ta>
            <ta e="T493" id="Seg_5121" s="T492">оставить-PST.NAR-3SG.O</ta>
            <ta e="T494" id="Seg_5122" s="T493">я.ALL</ta>
            <ta e="T495" id="Seg_5123" s="T494">спина-ADJZ</ta>
            <ta e="T496" id="Seg_5124" s="T495">нога-DIM-PL-ACC-3SG</ta>
            <ta e="T497" id="Seg_5125" s="T496">взять</ta>
            <ta e="T498" id="Seg_5126" s="T497">пушистый</ta>
            <ta e="T499" id="Seg_5127" s="T498">хвост-ACC</ta>
            <ta e="T500" id="Seg_5128" s="T499">мы.NOM</ta>
            <ta e="T501" id="Seg_5129" s="T500">он(а)-INSTR</ta>
            <ta e="T502" id="Seg_5130" s="T501">выругать-PST-1PL</ta>
            <ta e="T503" id="Seg_5131" s="T502">драться-PST-1PL</ta>
            <ta e="T504" id="Seg_5132" s="T503">опять</ta>
            <ta e="T505" id="Seg_5133" s="T504">помириться-PST-1PL</ta>
            <ta e="T506" id="Seg_5134" s="T505">этот</ta>
            <ta e="T507" id="Seg_5135" s="T506">день.[NOM]</ta>
            <ta e="T508" id="Seg_5136" s="T507">мы.NOM</ta>
            <ta e="T509" id="Seg_5137" s="T508">принести-PST-1PL</ta>
            <ta e="T510" id="Seg_5138" s="T509">два</ta>
            <ta e="T511" id="Seg_5139" s="T510">белка-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_5140" s="T1">num</ta>
            <ta e="T3" id="Seg_5141" s="T2">num</ta>
            <ta e="T4" id="Seg_5142" s="T3">num</ta>
            <ta e="T5" id="Seg_5143" s="T4">adj-num</ta>
            <ta e="T6" id="Seg_5144" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_5145" s="T6">n-n:ins-n:case</ta>
            <ta e="T8" id="Seg_5146" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_5147" s="T8">num</ta>
            <ta e="T10" id="Seg_5148" s="T9">num</ta>
            <ta e="T11" id="Seg_5149" s="T10">num</ta>
            <ta e="T12" id="Seg_5150" s="T11">num</ta>
            <ta e="T13" id="Seg_5151" s="T12">num</ta>
            <ta e="T14" id="Seg_5152" s="T13">num</ta>
            <ta e="T15" id="Seg_5153" s="T14">num</ta>
            <ta e="T16" id="Seg_5154" s="T15">n-n:poss</ta>
            <ta e="T17" id="Seg_5155" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_5156" s="T17">pers</ta>
            <ta e="T19" id="Seg_5157" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_5158" s="T19">v-v&gt;v-v:inf</ta>
            <ta e="T21" id="Seg_5159" s="T20">n-n:case-pp</ta>
            <ta e="T22" id="Seg_5160" s="T21">v-v&gt;ptcp.[n:case]</ta>
            <ta e="T23" id="Seg_5161" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_5162" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_5163" s="T24">n-n:poss</ta>
            <ta e="T26" id="Seg_5164" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_5165" s="T26">n-n:ins-n:case</ta>
            <ta e="T28" id="Seg_5166" s="T27">nprop.[n:case]</ta>
            <ta e="T29" id="Seg_5167" s="T28">nprop-n:ins-n:case</ta>
            <ta e="T30" id="Seg_5168" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_5169" s="T30">dem-n:poss</ta>
            <ta e="T32" id="Seg_5170" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_5171" s="T32">n-adv:case</ta>
            <ta e="T34" id="Seg_5172" s="T33">adv</ta>
            <ta e="T35" id="Seg_5173" s="T34">v.[v:pn]</ta>
            <ta e="T36" id="Seg_5174" s="T35">n-adv:case</ta>
            <ta e="T37" id="Seg_5175" s="T36">pers</ta>
            <ta e="T38" id="Seg_5176" s="T37">n-n:poss</ta>
            <ta e="T39" id="Seg_5177" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_5178" s="T39">adj</ta>
            <ta e="T41" id="Seg_5179" s="T40">adv</ta>
            <ta e="T42" id="Seg_5180" s="T41">adj</ta>
            <ta e="T43" id="Seg_5181" s="T42">v-v:tense.[v:pn]</ta>
            <ta e="T44" id="Seg_5182" s="T43">n-n:case.poss</ta>
            <ta e="T45" id="Seg_5183" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_5184" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_5185" s="T46">pp</ta>
            <ta e="T48" id="Seg_5186" s="T47">interrog-n:ins-n:case</ta>
            <ta e="T49" id="Seg_5187" s="T48">ptcl-n&gt;v-%%-v:tense.[v:pn]</ta>
            <ta e="T50" id="Seg_5188" s="T49">v-v:inf</ta>
            <ta e="T51" id="Seg_5189" s="T50">v-v:tense-v:tense.[v:pn]</ta>
            <ta e="T52" id="Seg_5190" s="T51">conj</ta>
            <ta e="T53" id="Seg_5191" s="T52">num</ta>
            <ta e="T54" id="Seg_5192" s="T53">n.[n:case]</ta>
            <ta e="T55" id="Seg_5193" s="T54">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_5194" s="T55">pers</ta>
            <ta e="T57" id="Seg_5195" s="T56">pp-adv:case</ta>
            <ta e="T58" id="Seg_5196" s="T57">pers-n:ins-n:num-n:case</ta>
            <ta e="T59" id="Seg_5197" s="T58">n-n:poss</ta>
            <ta e="T60" id="Seg_5198" s="T59">nprop</ta>
            <ta e="T61" id="Seg_5199" s="T60">nprop</ta>
            <ta e="T62" id="Seg_5200" s="T61">nprop-n:ins-n:case</ta>
            <ta e="T63" id="Seg_5201" s="T62">n-n:case-n:poss</ta>
            <ta e="T64" id="Seg_5202" s="T63">n-n:poss</ta>
            <ta e="T65" id="Seg_5203" s="T64">pers</ta>
            <ta e="T66" id="Seg_5204" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_5205" s="T66">pers-n:ins-n:num</ta>
            <ta e="T68" id="Seg_5206" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_5207" s="T68">n</ta>
            <ta e="T70" id="Seg_5208" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_5209" s="T70">pers-n:ins-n:num-n:case</ta>
            <ta e="T72" id="Seg_5210" s="T71">n-n.[n:case]</ta>
            <ta e="T73" id="Seg_5211" s="T72">v-v:tense.[v:pn]</ta>
            <ta e="T74" id="Seg_5212" s="T73">dem</ta>
            <ta e="T75" id="Seg_5213" s="T74">n.[n:case]</ta>
            <ta e="T76" id="Seg_5214" s="T75">interrog-adj&gt;adv</ta>
            <ta e="T77" id="Seg_5215" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_5216" s="T77">v-v&gt;v-v:inf</ta>
            <ta e="T79" id="Seg_5217" s="T78">n-n&gt;n-n:case</ta>
            <ta e="T80" id="Seg_5218" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_5219" s="T80">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T82" id="Seg_5220" s="T81">num</ta>
            <ta e="T83" id="Seg_5221" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_5222" s="T83">n.[n:case]-n:poss</ta>
            <ta e="T85" id="Seg_5223" s="T84">n-n:num-n:poss</ta>
            <ta e="T86" id="Seg_5224" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_5225" s="T86">pers</ta>
            <ta e="T88" id="Seg_5226" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_5227" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_5228" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_5229" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_5230" s="T91">n.[n:case]-n:poss</ta>
            <ta e="T93" id="Seg_5231" s="T92">v-n:ins-v:pn</ta>
            <ta e="T94" id="Seg_5232" s="T93">num-num&gt;adj</ta>
            <ta e="T95" id="Seg_5233" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_5234" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_5235" s="T96">pers</ta>
            <ta e="T98" id="Seg_5236" s="T97">v-v:pn</ta>
            <ta e="T99" id="Seg_5237" s="T98">pers-n:ins-n:case</ta>
            <ta e="T100" id="Seg_5238" s="T99">v-v:inf</ta>
            <ta e="T101" id="Seg_5239" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_5240" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_5241" s="T102">adv</ta>
            <ta e="T104" id="Seg_5242" s="T103">pers-n:ins-n:case</ta>
            <ta e="T105" id="Seg_5243" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_5244" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_5245" s="T106">pers.[n:case]</ta>
            <ta e="T108" id="Seg_5246" s="T107">adj-v:tense.[v:pn]</ta>
            <ta e="T109" id="Seg_5247" s="T108">adj</ta>
            <ta e="T110" id="Seg_5248" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_5249" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_5250" s="T111">v-v&gt;v-v:pn</ta>
            <ta e="T113" id="Seg_5251" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_5252" s="T113">v-v&gt;adv</ta>
            <ta e="T115" id="Seg_5253" s="T114">n-n:ins-n:case</ta>
            <ta e="T116" id="Seg_5254" s="T115">n.[n:case]-n:poss</ta>
            <ta e="T117" id="Seg_5255" s="T116">nprop.[n:case]</ta>
            <ta e="T118" id="Seg_5256" s="T117">nprop-n:ins-n:case</ta>
            <ta e="T119" id="Seg_5257" s="T118">conj</ta>
            <ta e="T120" id="Seg_5258" s="T119">nprop-n:case</ta>
            <ta e="T121" id="Seg_5259" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_5260" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_5261" s="T122">v-v:pn</ta>
            <ta e="T124" id="Seg_5262" s="T123">n-n:poss</ta>
            <ta e="T125" id="Seg_5263" s="T124">dem</ta>
            <ta e="T126" id="Seg_5264" s="T125">n-n:poss</ta>
            <ta e="T127" id="Seg_5265" s="T126">v-v&gt;ptcp</ta>
            <ta e="T128" id="Seg_5266" s="T127">conj</ta>
            <ta e="T129" id="Seg_5267" s="T128">v-v&gt;ptcp</ta>
            <ta e="T130" id="Seg_5268" s="T129">pers</ta>
            <ta e="T131" id="Seg_5269" s="T130">n-n:case-n:case</ta>
            <ta e="T132" id="Seg_5270" s="T131">n.[n:case]</ta>
            <ta e="T133" id="Seg_5271" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_5272" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_5273" s="T134">adj</ta>
            <ta e="T136" id="Seg_5274" s="T135">v-v&gt;ptcp</ta>
            <ta e="T137" id="Seg_5275" s="T136">n-n:num-n:case</ta>
            <ta e="T138" id="Seg_5276" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_5277" s="T138">n-n:case.poss</ta>
            <ta e="T140" id="Seg_5278" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_5279" s="T140">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T142" id="Seg_5280" s="T141">n-n&gt;v-v&gt;adv</ta>
            <ta e="T143" id="Seg_5281" s="T142">v-v:pn</ta>
            <ta e="T144" id="Seg_5282" s="T143">n-n:poss</ta>
            <ta e="T145" id="Seg_5283" s="T144">v-v:pn</ta>
            <ta e="T146" id="Seg_5284" s="T145">interrog-n:num-n:poss</ta>
            <ta e="T147" id="Seg_5285" s="T146">conj</ta>
            <ta e="T148" id="Seg_5286" s="T147">v-v&gt;ptcp</ta>
            <ta e="T149" id="Seg_5287" s="T148">n-n&gt;n-n:poss</ta>
            <ta e="T150" id="Seg_5288" s="T149">conj</ta>
            <ta e="T151" id="Seg_5289" s="T150">interrog-n:num-n:poss</ta>
            <ta e="T152" id="Seg_5290" s="T151">n-v&gt;adv</ta>
            <ta e="T153" id="Seg_5291" s="T152">v-v&gt;v-v:pn</ta>
            <ta e="T154" id="Seg_5292" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_5293" s="T154">n.[n:case]</ta>
            <ta e="T156" id="Seg_5294" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_5295" s="T156">n-n:ins-n:case</ta>
            <ta e="T158" id="Seg_5296" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_5297" s="T158">n-n:ins-n:case</ta>
            <ta e="T160" id="Seg_5298" s="T159">v-v:ins-v:pn</ta>
            <ta e="T161" id="Seg_5299" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_5300" s="T161">v-v:ins-v:pn</ta>
            <ta e="T163" id="Seg_5301" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_5302" s="T163">n-n:num-n:case</ta>
            <ta e="T165" id="Seg_5303" s="T164">v-v:ins-v:pn</ta>
            <ta e="T166" id="Seg_5304" s="T165">v-v:tense.[v:pn]</ta>
            <ta e="T167" id="Seg_5305" s="T166">v-v:tense-n:poss</ta>
            <ta e="T168" id="Seg_5306" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_5307" s="T168">nprop.[n:case]</ta>
            <ta e="T170" id="Seg_5308" s="T169">adj-adj&gt;adv</ta>
            <ta e="T171" id="Seg_5309" s="T170">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T172" id="Seg_5310" s="T171">pers-n:case</ta>
            <ta e="T173" id="Seg_5311" s="T172">num</ta>
            <ta e="T174" id="Seg_5312" s="T173">v.[v:pn]</ta>
            <ta e="T175" id="Seg_5313" s="T174">interrog</ta>
            <ta e="T176" id="Seg_5314" s="T175">v-v:inf</ta>
            <ta e="T177" id="Seg_5315" s="T176">v-v&gt;ptcp</ta>
            <ta e="T178" id="Seg_5316" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_5317" s="T178">pers-n:case</ta>
            <ta e="T180" id="Seg_5318" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_5319" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_5320" s="T181">n-n:ins-n:num</ta>
            <ta e="T183" id="Seg_5321" s="T182">num</ta>
            <ta e="T184" id="Seg_5322" s="T183">num</ta>
            <ta e="T185" id="Seg_5323" s="T184">num</ta>
            <ta e="T186" id="Seg_5324" s="T185">adj-num</ta>
            <ta e="T187" id="Seg_5325" s="T186">num</ta>
            <ta e="T188" id="Seg_5326" s="T187">num</ta>
            <ta e="T189" id="Seg_5327" s="T188">num</ta>
            <ta e="T190" id="Seg_5328" s="T189">adj-num</ta>
            <ta e="T191" id="Seg_5329" s="T190">n-adv:case</ta>
            <ta e="T192" id="Seg_5330" s="T191">pers</ta>
            <ta e="T193" id="Seg_5331" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_5332" s="T193">adj-adj&gt;adv</ta>
            <ta e="T195" id="Seg_5333" s="T194">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_5334" s="T195">emphpro</ta>
            <ta e="T197" id="Seg_5335" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_5336" s="T197">pers</ta>
            <ta e="T199" id="Seg_5337" s="T198">v-v&gt;v-n:ins-v:inf</ta>
            <ta e="T200" id="Seg_5338" s="T199">interrog-n:num-n:case</ta>
            <ta e="T201" id="Seg_5339" s="T200">%%-n&gt;adj</ta>
            <ta e="T202" id="Seg_5340" s="T201">pers</ta>
            <ta e="T203" id="Seg_5341" s="T202">adv</ta>
            <ta e="T204" id="Seg_5342" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_5343" s="T204">adv</ta>
            <ta e="T206" id="Seg_5344" s="T205">n-adv:case</ta>
            <ta e="T207" id="Seg_5345" s="T206">adj-adj&gt;adv</ta>
            <ta e="T208" id="Seg_5346" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_5347" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_5348" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_5349" s="T210">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_5350" s="T211">n-adv:case</ta>
            <ta e="T213" id="Seg_5351" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_5352" s="T213">pp</ta>
            <ta e="T215" id="Seg_5353" s="T214">num-n:ins-adj&gt;adv</ta>
            <ta e="T216" id="Seg_5354" s="T215">n-adv:case</ta>
            <ta e="T217" id="Seg_5355" s="T216">n.[n:case]-n:poss</ta>
            <ta e="T218" id="Seg_5356" s="T217">v-v:inf</ta>
            <ta e="T219" id="Seg_5357" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_5358" s="T219">num-adj&gt;adv</ta>
            <ta e="T221" id="Seg_5359" s="T220">num-adj&gt;adv</ta>
            <ta e="T222" id="Seg_5360" s="T221">v-v:tense.[v:pn]</ta>
            <ta e="T223" id="Seg_5361" s="T222">pers</ta>
            <ta e="T224" id="Seg_5362" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_5363" s="T224">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T226" id="Seg_5364" s="T225">pers</ta>
            <ta e="T227" id="Seg_5365" s="T226">emphpro</ta>
            <ta e="T228" id="Seg_5366" s="T227">pro-n:poss</ta>
            <ta e="T229" id="Seg_5367" s="T228">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_5368" s="T229">interrog</ta>
            <ta e="T231" id="Seg_5369" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_5370" s="T231">emphpro</ta>
            <ta e="T233" id="Seg_5371" s="T232">n-%%</ta>
            <ta e="T234" id="Seg_5372" s="T233">pers</ta>
            <ta e="T235" id="Seg_5373" s="T234">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_5374" s="T235">n.[n:case]</ta>
            <ta e="T237" id="Seg_5375" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_5376" s="T237">adj</ta>
            <ta e="T239" id="Seg_5377" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_5378" s="T239">n.[n:case]</ta>
            <ta e="T241" id="Seg_5379" s="T240">pers</ta>
            <ta e="T242" id="Seg_5380" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_5381" s="T242">n</ta>
            <ta e="T244" id="Seg_5382" s="T243">adv</ta>
            <ta e="T245" id="Seg_5383" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_5384" s="T245">dem</ta>
            <ta e="T247" id="Seg_5385" s="T246">n.[n:case]</ta>
            <ta e="T248" id="Seg_5386" s="T247">n.[n:case]</ta>
            <ta e="T249" id="Seg_5387" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_5388" s="T249">num</ta>
            <ta e="T251" id="Seg_5389" s="T250">num-num&gt;adj</ta>
            <ta e="T252" id="Seg_5390" s="T251">n-n-n:case</ta>
            <ta e="T253" id="Seg_5391" s="T252">adv</ta>
            <ta e="T254" id="Seg_5392" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_5393" s="T254">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_5394" s="T255">v-v:inf</ta>
            <ta e="T257" id="Seg_5395" s="T256">adj</ta>
            <ta e="T258" id="Seg_5396" s="T257">v-v:tense.[v:pn]</ta>
            <ta e="T259" id="Seg_5397" s="T258">n-n&gt;adj</ta>
            <ta e="T260" id="Seg_5398" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_5399" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_5400" s="T261">interrog</ta>
            <ta e="T263" id="Seg_5401" s="T262">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T264" id="Seg_5402" s="T263">num</ta>
            <ta e="T265" id="Seg_5403" s="T264">adj-num</ta>
            <ta e="T266" id="Seg_5404" s="T265">n-n:case.poss</ta>
            <ta e="T267" id="Seg_5405" s="T266">n-adv:case</ta>
            <ta e="T268" id="Seg_5406" s="T267">pers</ta>
            <ta e="T269" id="Seg_5407" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_5408" s="T269">num</ta>
            <ta e="T271" id="Seg_5409" s="T270">n-v&gt;ptcp</ta>
            <ta e="T272" id="Seg_5410" s="T271">n-n:num-n:case-n:case</ta>
            <ta e="T273" id="Seg_5411" s="T272">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T274" id="Seg_5412" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_5413" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_5414" s="T275">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_5415" s="T276">v-v&gt;adv</ta>
            <ta e="T278" id="Seg_5416" s="T277">v-v&gt;ptcp</ta>
            <ta e="T279" id="Seg_5417" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_5418" s="T279">pers</ta>
            <ta e="T281" id="Seg_5419" s="T280">v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_5420" s="T281">v-v:inf</ta>
            <ta e="T283" id="Seg_5421" s="T282">dem</ta>
            <ta e="T284" id="Seg_5422" s="T283">n.[n:case]</ta>
            <ta e="T285" id="Seg_5423" s="T284">pers</ta>
            <ta e="T286" id="Seg_5424" s="T285">v-v:ins-v:pn</ta>
            <ta e="T287" id="Seg_5425" s="T286">num</ta>
            <ta e="T288" id="Seg_5426" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_5427" s="T288">dem</ta>
            <ta e="T290" id="Seg_5428" s="T289">dem</ta>
            <ta e="T291" id="Seg_5429" s="T290">adj-v-v:tense.[n:case]</ta>
            <ta e="T292" id="Seg_5430" s="T291">conj</ta>
            <ta e="T293" id="Seg_5431" s="T292">num</ta>
            <ta e="T294" id="Seg_5432" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_5433" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_5434" s="T295">adj</ta>
            <ta e="T297" id="Seg_5435" s="T296">n.[n:case]-n:poss</ta>
            <ta e="T298" id="Seg_5436" s="T297">num-num&gt;adj</ta>
            <ta e="T299" id="Seg_5437" s="T298">n.[n:case]</ta>
            <ta e="T300" id="Seg_5438" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_5439" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_5440" s="T301">num</ta>
            <ta e="T303" id="Seg_5441" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_5442" s="T303">pers-n:num</ta>
            <ta e="T305" id="Seg_5443" s="T304">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_5444" s="T305">adj</ta>
            <ta e="T307" id="Seg_5445" s="T306">n</ta>
            <ta e="T308" id="Seg_5446" s="T307">adj</ta>
            <ta e="T309" id="Seg_5447" s="T308">n.[n:case]</ta>
            <ta e="T310" id="Seg_5448" s="T309">n.[n:case]-n:poss</ta>
            <ta e="T311" id="Seg_5449" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_5450" s="T311">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T313" id="Seg_5451" s="T312">num-num&gt;adj</ta>
            <ta e="T314" id="Seg_5452" s="T313">n.[n:case]</ta>
            <ta e="T315" id="Seg_5453" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_5454" s="T315">adj</ta>
            <ta e="T317" id="Seg_5455" s="T316">n.[n:case]-n:poss</ta>
            <ta e="T318" id="Seg_5456" s="T317">dem</ta>
            <ta e="T319" id="Seg_5457" s="T318">n.[n:case]</ta>
            <ta e="T320" id="Seg_5458" s="T319">interrog-n:case--clit-ptcl</ta>
            <ta e="T321" id="Seg_5459" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_5460" s="T321">n.[n:case]-n:poss</ta>
            <ta e="T323" id="Seg_5461" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_5462" s="T323">n.[n:case]-n:poss</ta>
            <ta e="T325" id="Seg_5463" s="T324">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T326" id="Seg_5464" s="T325">pers</ta>
            <ta e="T327" id="Seg_5465" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_5466" s="T327">dem</ta>
            <ta e="T329" id="Seg_5467" s="T328">n.[n:case]</ta>
            <ta e="T330" id="Seg_5468" s="T329">n-n:num-n:case</ta>
            <ta e="T331" id="Seg_5469" s="T330">num</ta>
            <ta e="T332" id="Seg_5470" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_5471" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_5472" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_5473" s="T334">pp</ta>
            <ta e="T336" id="Seg_5474" s="T335">nprop-n:case</ta>
            <ta e="T337" id="Seg_5475" s="T336">adv-adv:case</ta>
            <ta e="T338" id="Seg_5476" s="T337">pers</ta>
            <ta e="T339" id="Seg_5477" s="T338">v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_5478" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_5479" s="T340">v-v&gt;v-v&gt;adv</ta>
            <ta e="T342" id="Seg_5480" s="T341">emphpro</ta>
            <ta e="T343" id="Seg_5481" s="T342">n-n:num-n:poss</ta>
            <ta e="T344" id="Seg_5482" s="T343">conj</ta>
            <ta e="T345" id="Seg_5483" s="T344">pers</ta>
            <ta e="T346" id="Seg_5484" s="T345">n-n:poss</ta>
            <ta e="T347" id="Seg_5485" s="T346">nprop.[n:case]</ta>
            <ta e="T348" id="Seg_5486" s="T347">nprop.[n:case]</ta>
            <ta e="T349" id="Seg_5487" s="T348">conj</ta>
            <ta e="T350" id="Seg_5488" s="T349">nprop.[n:case]</ta>
            <ta e="T351" id="Seg_5489" s="T350">dem</ta>
            <ta e="T352" id="Seg_5490" s="T351">n.[n:case]</ta>
            <ta e="T353" id="Seg_5491" s="T352">pers</ta>
            <ta e="T354" id="Seg_5492" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_5493" s="T354">num</ta>
            <ta e="T356" id="Seg_5494" s="T355">v-v:ins-v:pn</ta>
            <ta e="T357" id="Seg_5495" s="T356">v-v&gt;ptcp</ta>
            <ta e="T358" id="Seg_5496" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_5497" s="T358">nprop-n:ins-n:case</ta>
            <ta e="T360" id="Seg_5498" s="T359">pers</ta>
            <ta e="T361" id="Seg_5499" s="T360">v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_5500" s="T361">adv</ta>
            <ta e="T363" id="Seg_5501" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_5502" s="T363">adj-adj&gt;adv</ta>
            <ta e="T365" id="Seg_5503" s="T364">num-num&gt;adj</ta>
            <ta e="T366" id="Seg_5504" s="T365">n.[n:case]</ta>
            <ta e="T367" id="Seg_5505" s="T366">n-pp</ta>
            <ta e="T368" id="Seg_5506" s="T367">adj</ta>
            <ta e="T369" id="Seg_5507" s="T368">v-v:tense.[v:pn]</ta>
            <ta e="T370" id="Seg_5508" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_5509" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_5510" s="T371">n.[n:case]-n:poss</ta>
            <ta e="T373" id="Seg_5511" s="T372">n.[n:case]-n:poss</ta>
            <ta e="T374" id="Seg_5512" s="T373">adv</ta>
            <ta e="T375" id="Seg_5513" s="T374">v-v:tense.[v:pn]</ta>
            <ta e="T376" id="Seg_5514" s="T375">v-v:inf</ta>
            <ta e="T377" id="Seg_5515" s="T376">n-n:num.[n:case]</ta>
            <ta e="T378" id="Seg_5516" s="T377">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_5517" s="T378">dem</ta>
            <ta e="T380" id="Seg_5518" s="T379">n.[n:case]</ta>
            <ta e="T381" id="Seg_5519" s="T380">v-v:ins-v:pn</ta>
            <ta e="T382" id="Seg_5520" s="T381">num</ta>
            <ta e="T383" id="Seg_5521" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_5522" s="T383">num-num&gt;adj</ta>
            <ta e="T385" id="Seg_5523" s="T384">n.[n:case]</ta>
            <ta e="T386" id="Seg_5524" s="T385">num</ta>
            <ta e="T387" id="Seg_5525" s="T386">n.[n:case]</ta>
            <ta e="T388" id="Seg_5526" s="T387">n.[n:case]-n:poss</ta>
            <ta e="T389" id="Seg_5527" s="T388">pers-n:ins-n:case</ta>
            <ta e="T390" id="Seg_5528" s="T389">nprop.[n:case]</ta>
            <ta e="T391" id="Seg_5529" s="T390">v-v:tense.[v:pn]</ta>
            <ta e="T392" id="Seg_5530" s="T391">n-n:case.poss</ta>
            <ta e="T393" id="Seg_5531" s="T392">dem</ta>
            <ta e="T394" id="Seg_5532" s="T393">n.[n:case]</ta>
            <ta e="T395" id="Seg_5533" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_5534" s="T395">num</ta>
            <ta e="T397" id="Seg_5535" s="T396">n-n:case</ta>
            <ta e="T398" id="Seg_5536" s="T397">num-num&gt;adj</ta>
            <ta e="T399" id="Seg_5537" s="T398">n.[n:case]</ta>
            <ta e="T400" id="Seg_5538" s="T399">v-v:tense-v:pn</ta>
            <ta e="T401" id="Seg_5539" s="T400">adj</ta>
            <ta e="T402" id="Seg_5540" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_5541" s="T402">pp</ta>
            <ta e="T404" id="Seg_5542" s="T403">adj-v:pn</ta>
            <ta e="T405" id="Seg_5543" s="T404">v-v:tense-v:pn</ta>
            <ta e="T406" id="Seg_5544" s="T405">adv</ta>
            <ta e="T407" id="Seg_5545" s="T406">v-v&gt;v-v&gt;adv</ta>
            <ta e="T408" id="Seg_5546" s="T407">conj</ta>
            <ta e="T409" id="Seg_5547" s="T408">v-n:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T410" id="Seg_5548" s="T409">pers</ta>
            <ta e="T411" id="Seg_5549" s="T410">n.[n:case]-n:poss</ta>
            <ta e="T412" id="Seg_5550" s="T411">n.[n:case]-n:poss</ta>
            <ta e="T413" id="Seg_5551" s="T412">pers-n:ins-n:case</ta>
            <ta e="T414" id="Seg_5552" s="T413">nprop.[n:case]</ta>
            <ta e="T415" id="Seg_5553" s="T414">v-v&gt;v-v:pn</ta>
            <ta e="T416" id="Seg_5554" s="T415">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T417" id="Seg_5555" s="T416">n.[n:case]-n:poss</ta>
            <ta e="T418" id="Seg_5556" s="T417">v-v:ins-v:pn</ta>
            <ta e="T419" id="Seg_5557" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_5558" s="T419">v-v&gt;v-v:inf</ta>
            <ta e="T421" id="Seg_5559" s="T420">nprop-n:case</ta>
            <ta e="T422" id="Seg_5560" s="T421">pers.[n:case]</ta>
            <ta e="T423" id="Seg_5561" s="T422">adj-adj&gt;adv</ta>
            <ta e="T424" id="Seg_5562" s="T423">v-v:ins-v:pn</ta>
            <ta e="T425" id="Seg_5563" s="T424">adj</ta>
            <ta e="T426" id="Seg_5564" s="T425">adj</ta>
            <ta e="T427" id="Seg_5565" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_5566" s="T427">v-v&gt;v-v:pn</ta>
            <ta e="T429" id="Seg_5567" s="T428">adv</ta>
            <ta e="T430" id="Seg_5568" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_5569" s="T430">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T432" id="Seg_5570" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_5571" s="T432">v-n&gt;v-v:inf</ta>
            <ta e="T434" id="Seg_5572" s="T433">quant</ta>
            <ta e="T435" id="Seg_5573" s="T434">v-v:tense-v:pn</ta>
            <ta e="T436" id="Seg_5574" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_5575" s="T436">conj</ta>
            <ta e="T438" id="Seg_5576" s="T437">adv</ta>
            <ta e="T439" id="Seg_5577" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_5578" s="T439">num-n:ins-adj&gt;adv</ta>
            <ta e="T441" id="Seg_5579" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_5580" s="T441">num</ta>
            <ta e="T443" id="Seg_5581" s="T442">n.[n:case]</ta>
            <ta e="T444" id="Seg_5582" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_5583" s="T444">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_5584" s="T445">pers</ta>
            <ta e="T447" id="Seg_5585" s="T446">pers-n:ins-n:case</ta>
            <ta e="T448" id="Seg_5586" s="T447">v-v&gt;v-v:inf</ta>
            <ta e="T449" id="Seg_5587" s="T448">pers</ta>
            <ta e="T450" id="Seg_5588" s="T449">pers-n:ins-n:case</ta>
            <ta e="T451" id="Seg_5589" s="T450">v-v&gt;v-v:pn</ta>
            <ta e="T452" id="Seg_5590" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_5591" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_5592" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_5593" s="T454">num-n:ins-adj&gt;adv</ta>
            <ta e="T456" id="Seg_5594" s="T455">num-adj&gt;adv</ta>
            <ta e="T457" id="Seg_5595" s="T456">n-n:case</ta>
            <ta e="T458" id="Seg_5596" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_5597" s="T458">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_5598" s="T459">conj</ta>
            <ta e="T461" id="Seg_5599" s="T460">v-v:pn</ta>
            <ta e="T462" id="Seg_5600" s="T461">v-v:ins-v:pn</ta>
            <ta e="T463" id="Seg_5601" s="T462">v-n:ins-v:ins-v:pn</ta>
            <ta e="T464" id="Seg_5602" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_5603" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_5604" s="T465">v-v:inf</ta>
            <ta e="T467" id="Seg_5605" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_5606" s="T467">v-v&gt;v-v:pn</ta>
            <ta e="T469" id="Seg_5607" s="T468">pers</ta>
            <ta e="T470" id="Seg_5608" s="T469">n-n&gt;adj</ta>
            <ta e="T471" id="Seg_5609" s="T470">n.[n:case]-n:poss</ta>
            <ta e="T472" id="Seg_5610" s="T471">n.[n:case]-n:poss</ta>
            <ta e="T473" id="Seg_5611" s="T472">pers-n:ins-n:case</ta>
            <ta e="T474" id="Seg_5612" s="T473">nprop.[n:case]</ta>
            <ta e="T475" id="Seg_5613" s="T474">interrog-n:case-clit</ta>
            <ta e="T476" id="Seg_5614" s="T475">v-v&gt;v-v:pn</ta>
            <ta e="T477" id="Seg_5615" s="T476">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T478" id="Seg_5616" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_5617" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_5618" s="T479">pers</ta>
            <ta e="T481" id="Seg_5619" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_5620" s="T481">v-v:ins-v:pn</ta>
            <ta e="T483" id="Seg_5621" s="T482">pers-n:ins-n:case</ta>
            <ta e="T484" id="Seg_5622" s="T483">pers.[n:case]</ta>
            <ta e="T485" id="Seg_5623" s="T484">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T486" id="Seg_5624" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_5625" s="T486">interrog</ta>
            <ta e="T488" id="Seg_5626" s="T487">pers</ta>
            <ta e="T489" id="Seg_5627" s="T488">v-n:ins-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_5628" s="T489">v-v:tense-v:pn</ta>
            <ta e="T491" id="Seg_5629" s="T490">pers.[n:case]</ta>
            <ta e="T492" id="Seg_5630" s="T491">v-n:ins-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_5631" s="T492">v-v:tense-v:pn</ta>
            <ta e="T494" id="Seg_5632" s="T493">pers</ta>
            <ta e="T495" id="Seg_5633" s="T494">n-n&gt;adj</ta>
            <ta e="T496" id="Seg_5634" s="T495">n-n&gt;n-n:num-n:case-n:poss</ta>
            <ta e="T497" id="Seg_5635" s="T496">v</ta>
            <ta e="T498" id="Seg_5636" s="T497">adj</ta>
            <ta e="T499" id="Seg_5637" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_5638" s="T499">pers</ta>
            <ta e="T501" id="Seg_5639" s="T500">pers-n:case</ta>
            <ta e="T502" id="Seg_5640" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_5641" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_5642" s="T503">adv</ta>
            <ta e="T505" id="Seg_5643" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_5644" s="T505">dem</ta>
            <ta e="T507" id="Seg_5645" s="T506">n.[n:case]</ta>
            <ta e="T508" id="Seg_5646" s="T507">pers</ta>
            <ta e="T509" id="Seg_5647" s="T508">v-v:tense-v:pn</ta>
            <ta e="T510" id="Seg_5648" s="T509">num</ta>
            <ta e="T511" id="Seg_5649" s="T510">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_5650" s="T1">num</ta>
            <ta e="T3" id="Seg_5651" s="T2">num</ta>
            <ta e="T4" id="Seg_5652" s="T3">num</ta>
            <ta e="T5" id="Seg_5653" s="T4">adj</ta>
            <ta e="T6" id="Seg_5654" s="T5">n</ta>
            <ta e="T7" id="Seg_5655" s="T6">n</ta>
            <ta e="T8" id="Seg_5656" s="T7">n</ta>
            <ta e="T9" id="Seg_5657" s="T8">num</ta>
            <ta e="T10" id="Seg_5658" s="T9">num</ta>
            <ta e="T11" id="Seg_5659" s="T10">num</ta>
            <ta e="T12" id="Seg_5660" s="T11">num</ta>
            <ta e="T13" id="Seg_5661" s="T12">num</ta>
            <ta e="T14" id="Seg_5662" s="T13">num</ta>
            <ta e="T15" id="Seg_5663" s="T14">num</ta>
            <ta e="T16" id="Seg_5664" s="T15">n</ta>
            <ta e="T17" id="Seg_5665" s="T16">n</ta>
            <ta e="T18" id="Seg_5666" s="T17">pers</ta>
            <ta e="T19" id="Seg_5667" s="T18">v</ta>
            <ta e="T20" id="Seg_5668" s="T19">v</ta>
            <ta e="T21" id="Seg_5669" s="T20">n</ta>
            <ta e="T22" id="Seg_5670" s="T21">ptcp</ta>
            <ta e="T23" id="Seg_5671" s="T22">n</ta>
            <ta e="T24" id="Seg_5672" s="T23">n</ta>
            <ta e="T25" id="Seg_5673" s="T24">n</ta>
            <ta e="T26" id="Seg_5674" s="T25">n</ta>
            <ta e="T27" id="Seg_5675" s="T26">n</ta>
            <ta e="T28" id="Seg_5676" s="T27">nprop</ta>
            <ta e="T29" id="Seg_5677" s="T28">nprop</ta>
            <ta e="T30" id="Seg_5678" s="T29">n</ta>
            <ta e="T31" id="Seg_5679" s="T30">v</ta>
            <ta e="T32" id="Seg_5680" s="T31">v</ta>
            <ta e="T33" id="Seg_5681" s="T32">n</ta>
            <ta e="T34" id="Seg_5682" s="T33">adv</ta>
            <ta e="T35" id="Seg_5683" s="T34">v</ta>
            <ta e="T36" id="Seg_5684" s="T35">n</ta>
            <ta e="T37" id="Seg_5685" s="T36">pers</ta>
            <ta e="T38" id="Seg_5686" s="T37">n</ta>
            <ta e="T39" id="Seg_5687" s="T38">v</ta>
            <ta e="T40" id="Seg_5688" s="T39">adj</ta>
            <ta e="T41" id="Seg_5689" s="T40">adv</ta>
            <ta e="T42" id="Seg_5690" s="T41">adj</ta>
            <ta e="T43" id="Seg_5691" s="T42">v</ta>
            <ta e="T44" id="Seg_5692" s="T43">n</ta>
            <ta e="T45" id="Seg_5693" s="T44">n</ta>
            <ta e="T46" id="Seg_5694" s="T45">n</ta>
            <ta e="T47" id="Seg_5695" s="T46">pp</ta>
            <ta e="T48" id="Seg_5696" s="T47">interrog</ta>
            <ta e="T49" id="Seg_5697" s="T48">v</ta>
            <ta e="T50" id="Seg_5698" s="T49">v</ta>
            <ta e="T51" id="Seg_5699" s="T50">v</ta>
            <ta e="T52" id="Seg_5700" s="T51">conj</ta>
            <ta e="T53" id="Seg_5701" s="T52">num</ta>
            <ta e="T54" id="Seg_5702" s="T53">n</ta>
            <ta e="T55" id="Seg_5703" s="T54">v</ta>
            <ta e="T56" id="Seg_5704" s="T55">pers</ta>
            <ta e="T57" id="Seg_5705" s="T56">pp</ta>
            <ta e="T58" id="Seg_5706" s="T57">pers</ta>
            <ta e="T59" id="Seg_5707" s="T58">n</ta>
            <ta e="T60" id="Seg_5708" s="T59">nprop</ta>
            <ta e="T61" id="Seg_5709" s="T60">nprop</ta>
            <ta e="T62" id="Seg_5710" s="T61">nprop</ta>
            <ta e="T63" id="Seg_5711" s="T62">n</ta>
            <ta e="T64" id="Seg_5712" s="T63">n</ta>
            <ta e="T65" id="Seg_5713" s="T64">pers</ta>
            <ta e="T66" id="Seg_5714" s="T65">v</ta>
            <ta e="T67" id="Seg_5715" s="T66">pers</ta>
            <ta e="T68" id="Seg_5716" s="T67">v</ta>
            <ta e="T69" id="Seg_5717" s="T68">n</ta>
            <ta e="T70" id="Seg_5718" s="T69">n</ta>
            <ta e="T71" id="Seg_5719" s="T70">pers</ta>
            <ta e="T72" id="Seg_5720" s="T71">n</ta>
            <ta e="T73" id="Seg_5721" s="T72">v</ta>
            <ta e="T74" id="Seg_5722" s="T73">dem</ta>
            <ta e="T75" id="Seg_5723" s="T74">n</ta>
            <ta e="T76" id="Seg_5724" s="T75">adv</ta>
            <ta e="T77" id="Seg_5725" s="T76">v</ta>
            <ta e="T78" id="Seg_5726" s="T77">v</ta>
            <ta e="T79" id="Seg_5727" s="T78">n</ta>
            <ta e="T80" id="Seg_5728" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_5729" s="T80">v</ta>
            <ta e="T82" id="Seg_5730" s="T81">num</ta>
            <ta e="T83" id="Seg_5731" s="T82">n</ta>
            <ta e="T84" id="Seg_5732" s="T83">n</ta>
            <ta e="T85" id="Seg_5733" s="T84">n</ta>
            <ta e="T86" id="Seg_5734" s="T85">v</ta>
            <ta e="T87" id="Seg_5735" s="T86">pers</ta>
            <ta e="T88" id="Seg_5736" s="T87">v</ta>
            <ta e="T89" id="Seg_5737" s="T88">n</ta>
            <ta e="T90" id="Seg_5738" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_5739" s="T90">v</ta>
            <ta e="T92" id="Seg_5740" s="T91">n</ta>
            <ta e="T93" id="Seg_5741" s="T92">v</ta>
            <ta e="T94" id="Seg_5742" s="T93">adj</ta>
            <ta e="T95" id="Seg_5743" s="T94">n</ta>
            <ta e="T96" id="Seg_5744" s="T95">n</ta>
            <ta e="T97" id="Seg_5745" s="T96">pers</ta>
            <ta e="T98" id="Seg_5746" s="T97">v</ta>
            <ta e="T99" id="Seg_5747" s="T98">pers</ta>
            <ta e="T100" id="Seg_5748" s="T99">v</ta>
            <ta e="T101" id="Seg_5749" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_5750" s="T101">v</ta>
            <ta e="T103" id="Seg_5751" s="T102">adv</ta>
            <ta e="T104" id="Seg_5752" s="T103">pers</ta>
            <ta e="T105" id="Seg_5753" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_5754" s="T105">v</ta>
            <ta e="T107" id="Seg_5755" s="T106">pers</ta>
            <ta e="T108" id="Seg_5756" s="T107">adj</ta>
            <ta e="T109" id="Seg_5757" s="T108">adj</ta>
            <ta e="T110" id="Seg_5758" s="T109">n</ta>
            <ta e="T111" id="Seg_5759" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_5760" s="T111">v</ta>
            <ta e="T113" id="Seg_5761" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_5762" s="T113">adv</ta>
            <ta e="T115" id="Seg_5763" s="T114">n</ta>
            <ta e="T116" id="Seg_5764" s="T115">n</ta>
            <ta e="T117" id="Seg_5765" s="T116">nprop</ta>
            <ta e="T118" id="Seg_5766" s="T117">nprop</ta>
            <ta e="T119" id="Seg_5767" s="T118">conj</ta>
            <ta e="T120" id="Seg_5768" s="T119">nprop</ta>
            <ta e="T121" id="Seg_5769" s="T120">n</ta>
            <ta e="T122" id="Seg_5770" s="T121">n</ta>
            <ta e="T123" id="Seg_5771" s="T122">v</ta>
            <ta e="T124" id="Seg_5772" s="T123">n</ta>
            <ta e="T125" id="Seg_5773" s="T124">dem</ta>
            <ta e="T126" id="Seg_5774" s="T125">n</ta>
            <ta e="T127" id="Seg_5775" s="T126">ptcp</ta>
            <ta e="T128" id="Seg_5776" s="T127">conj</ta>
            <ta e="T129" id="Seg_5777" s="T128">ptcp</ta>
            <ta e="T130" id="Seg_5778" s="T129">pers</ta>
            <ta e="T131" id="Seg_5779" s="T130">n</ta>
            <ta e="T132" id="Seg_5780" s="T131">n</ta>
            <ta e="T133" id="Seg_5781" s="T132">v</ta>
            <ta e="T134" id="Seg_5782" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_5783" s="T134">adj</ta>
            <ta e="T136" id="Seg_5784" s="T135">ptcp</ta>
            <ta e="T137" id="Seg_5785" s="T136">n</ta>
            <ta e="T138" id="Seg_5786" s="T137">n</ta>
            <ta e="T139" id="Seg_5787" s="T138">n</ta>
            <ta e="T140" id="Seg_5788" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_5789" s="T140">v</ta>
            <ta e="T142" id="Seg_5790" s="T141">adv</ta>
            <ta e="T143" id="Seg_5791" s="T142">v</ta>
            <ta e="T144" id="Seg_5792" s="T143">n</ta>
            <ta e="T145" id="Seg_5793" s="T144">v</ta>
            <ta e="T146" id="Seg_5794" s="T145">interrog</ta>
            <ta e="T147" id="Seg_5795" s="T146">conj</ta>
            <ta e="T148" id="Seg_5796" s="T147">ptcp</ta>
            <ta e="T149" id="Seg_5797" s="T148">n</ta>
            <ta e="T150" id="Seg_5798" s="T149">conj</ta>
            <ta e="T151" id="Seg_5799" s="T150">interrog</ta>
            <ta e="T152" id="Seg_5800" s="T151">adv</ta>
            <ta e="T153" id="Seg_5801" s="T152">v</ta>
            <ta e="T154" id="Seg_5802" s="T153">n</ta>
            <ta e="T155" id="Seg_5803" s="T154">n</ta>
            <ta e="T156" id="Seg_5804" s="T155">n</ta>
            <ta e="T157" id="Seg_5805" s="T156">n</ta>
            <ta e="T158" id="Seg_5806" s="T157">n</ta>
            <ta e="T159" id="Seg_5807" s="T158">n</ta>
            <ta e="T160" id="Seg_5808" s="T159">v</ta>
            <ta e="T161" id="Seg_5809" s="T160">n</ta>
            <ta e="T162" id="Seg_5810" s="T161">v</ta>
            <ta e="T163" id="Seg_5811" s="T162">n</ta>
            <ta e="T164" id="Seg_5812" s="T163">n</ta>
            <ta e="T165" id="Seg_5813" s="T164">v</ta>
            <ta e="T166" id="Seg_5814" s="T165">v</ta>
            <ta e="T167" id="Seg_5815" s="T166">v</ta>
            <ta e="T168" id="Seg_5816" s="T167">n</ta>
            <ta e="T169" id="Seg_5817" s="T168">nprop</ta>
            <ta e="T170" id="Seg_5818" s="T169">adv</ta>
            <ta e="T171" id="Seg_5819" s="T170">v</ta>
            <ta e="T172" id="Seg_5820" s="T171">pers</ta>
            <ta e="T173" id="Seg_5821" s="T172">num</ta>
            <ta e="T174" id="Seg_5822" s="T173">v</ta>
            <ta e="T175" id="Seg_5823" s="T174">interrog</ta>
            <ta e="T176" id="Seg_5824" s="T175">v</ta>
            <ta e="T177" id="Seg_5825" s="T176">ptcp</ta>
            <ta e="T178" id="Seg_5826" s="T177">n</ta>
            <ta e="T179" id="Seg_5827" s="T178">pers</ta>
            <ta e="T180" id="Seg_5828" s="T179">v</ta>
            <ta e="T181" id="Seg_5829" s="T180">n</ta>
            <ta e="T182" id="Seg_5830" s="T181">n</ta>
            <ta e="T183" id="Seg_5831" s="T182">num</ta>
            <ta e="T184" id="Seg_5832" s="T183">num</ta>
            <ta e="T185" id="Seg_5833" s="T184">num</ta>
            <ta e="T186" id="Seg_5834" s="T185">adj</ta>
            <ta e="T187" id="Seg_5835" s="T186">num</ta>
            <ta e="T188" id="Seg_5836" s="T187">num</ta>
            <ta e="T189" id="Seg_5837" s="T188">num</ta>
            <ta e="T190" id="Seg_5838" s="T189">adj</ta>
            <ta e="T191" id="Seg_5839" s="T190">v</ta>
            <ta e="T192" id="Seg_5840" s="T191">pers</ta>
            <ta e="T193" id="Seg_5841" s="T192">v</ta>
            <ta e="T194" id="Seg_5842" s="T193">adv</ta>
            <ta e="T195" id="Seg_5843" s="T194">v</ta>
            <ta e="T196" id="Seg_5844" s="T195">emphpro</ta>
            <ta e="T197" id="Seg_5845" s="T196">n</ta>
            <ta e="T198" id="Seg_5846" s="T197">pers</ta>
            <ta e="T199" id="Seg_5847" s="T198">v</ta>
            <ta e="T200" id="Seg_5848" s="T199">interrog</ta>
            <ta e="T202" id="Seg_5849" s="T201">pers</ta>
            <ta e="T203" id="Seg_5850" s="T202">adv</ta>
            <ta e="T204" id="Seg_5851" s="T203">v</ta>
            <ta e="T205" id="Seg_5852" s="T204">adv</ta>
            <ta e="T206" id="Seg_5853" s="T205">v</ta>
            <ta e="T207" id="Seg_5854" s="T206">adv</ta>
            <ta e="T208" id="Seg_5855" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_5856" s="T208">v</ta>
            <ta e="T210" id="Seg_5857" s="T209">n</ta>
            <ta e="T211" id="Seg_5858" s="T210">v</ta>
            <ta e="T212" id="Seg_5859" s="T211">v</ta>
            <ta e="T213" id="Seg_5860" s="T212">n</ta>
            <ta e="T214" id="Seg_5861" s="T213">pp</ta>
            <ta e="T215" id="Seg_5862" s="T214">adv</ta>
            <ta e="T216" id="Seg_5863" s="T215">v</ta>
            <ta e="T217" id="Seg_5864" s="T216">n</ta>
            <ta e="T218" id="Seg_5865" s="T217">v</ta>
            <ta e="T219" id="Seg_5866" s="T218">v</ta>
            <ta e="T220" id="Seg_5867" s="T219">adv</ta>
            <ta e="T221" id="Seg_5868" s="T220">num</ta>
            <ta e="T222" id="Seg_5869" s="T221">v</ta>
            <ta e="T223" id="Seg_5870" s="T222">pers</ta>
            <ta e="T224" id="Seg_5871" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_5872" s="T224">v</ta>
            <ta e="T226" id="Seg_5873" s="T225">pers</ta>
            <ta e="T227" id="Seg_5874" s="T226">emphpro</ta>
            <ta e="T228" id="Seg_5875" s="T227">pro</ta>
            <ta e="T229" id="Seg_5876" s="T228">v</ta>
            <ta e="T230" id="Seg_5877" s="T229">interrog</ta>
            <ta e="T231" id="Seg_5878" s="T230">n</ta>
            <ta e="T232" id="Seg_5879" s="T231">emphpro</ta>
            <ta e="T233" id="Seg_5880" s="T232">n</ta>
            <ta e="T234" id="Seg_5881" s="T233">pers</ta>
            <ta e="T235" id="Seg_5882" s="T234">v</ta>
            <ta e="T236" id="Seg_5883" s="T235">n</ta>
            <ta e="T237" id="Seg_5884" s="T236">n</ta>
            <ta e="T238" id="Seg_5885" s="T237">adj</ta>
            <ta e="T239" id="Seg_5886" s="T238">n</ta>
            <ta e="T240" id="Seg_5887" s="T239">n</ta>
            <ta e="T241" id="Seg_5888" s="T240">pers</ta>
            <ta e="T242" id="Seg_5889" s="T241">v</ta>
            <ta e="T243" id="Seg_5890" s="T242">n</ta>
            <ta e="T244" id="Seg_5891" s="T243">adv</ta>
            <ta e="T245" id="Seg_5892" s="T244">v</ta>
            <ta e="T246" id="Seg_5893" s="T245">dem</ta>
            <ta e="T247" id="Seg_5894" s="T246">n</ta>
            <ta e="T248" id="Seg_5895" s="T247">n</ta>
            <ta e="T249" id="Seg_5896" s="T248">n</ta>
            <ta e="T250" id="Seg_5897" s="T249">num</ta>
            <ta e="T251" id="Seg_5898" s="T250">adj</ta>
            <ta e="T252" id="Seg_5899" s="T251">n</ta>
            <ta e="T253" id="Seg_5900" s="T252">adj</ta>
            <ta e="T254" id="Seg_5901" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_5902" s="T254">v</ta>
            <ta e="T256" id="Seg_5903" s="T255">v</ta>
            <ta e="T257" id="Seg_5904" s="T256">adj</ta>
            <ta e="T258" id="Seg_5905" s="T257">v</ta>
            <ta e="T259" id="Seg_5906" s="T258">adj</ta>
            <ta e="T260" id="Seg_5907" s="T259">n</ta>
            <ta e="T261" id="Seg_5908" s="T260">n</ta>
            <ta e="T262" id="Seg_5909" s="T261">interrog</ta>
            <ta e="T263" id="Seg_5910" s="T262">v</ta>
            <ta e="T264" id="Seg_5911" s="T263">num</ta>
            <ta e="T265" id="Seg_5912" s="T264">adj</ta>
            <ta e="T266" id="Seg_5913" s="T265">n</ta>
            <ta e="T267" id="Seg_5914" s="T266">n</ta>
            <ta e="T268" id="Seg_5915" s="T267">pers</ta>
            <ta e="T269" id="Seg_5916" s="T268">v</ta>
            <ta e="T270" id="Seg_5917" s="T269">num</ta>
            <ta e="T271" id="Seg_5918" s="T270">ptcp</ta>
            <ta e="T272" id="Seg_5919" s="T271">n</ta>
            <ta e="T273" id="Seg_5920" s="T272">pers</ta>
            <ta e="T274" id="Seg_5921" s="T273">n</ta>
            <ta e="T275" id="Seg_5922" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_5923" s="T275">v</ta>
            <ta e="T277" id="Seg_5924" s="T276">adv</ta>
            <ta e="T278" id="Seg_5925" s="T277">ptcp</ta>
            <ta e="T279" id="Seg_5926" s="T278">n</ta>
            <ta e="T280" id="Seg_5927" s="T279">pers</ta>
            <ta e="T281" id="Seg_5928" s="T280">v</ta>
            <ta e="T282" id="Seg_5929" s="T281">v</ta>
            <ta e="T283" id="Seg_5930" s="T282">dem</ta>
            <ta e="T284" id="Seg_5931" s="T283">n</ta>
            <ta e="T285" id="Seg_5932" s="T284">pers</ta>
            <ta e="T286" id="Seg_5933" s="T285">v</ta>
            <ta e="T287" id="Seg_5934" s="T286">num</ta>
            <ta e="T288" id="Seg_5935" s="T287">n</ta>
            <ta e="T289" id="Seg_5936" s="T288">dem</ta>
            <ta e="T290" id="Seg_5937" s="T289">dem</ta>
            <ta e="T291" id="Seg_5938" s="T290">v</ta>
            <ta e="T292" id="Seg_5939" s="T291">conj</ta>
            <ta e="T293" id="Seg_5940" s="T292">num</ta>
            <ta e="T294" id="Seg_5941" s="T293">n</ta>
            <ta e="T295" id="Seg_5942" s="T294">v</ta>
            <ta e="T296" id="Seg_5943" s="T295">adj</ta>
            <ta e="T297" id="Seg_5944" s="T296">n</ta>
            <ta e="T298" id="Seg_5945" s="T297">adj</ta>
            <ta e="T299" id="Seg_5946" s="T298">n</ta>
            <ta e="T300" id="Seg_5947" s="T299">v</ta>
            <ta e="T301" id="Seg_5948" s="T300">v</ta>
            <ta e="T302" id="Seg_5949" s="T301">num</ta>
            <ta e="T303" id="Seg_5950" s="T302">n</ta>
            <ta e="T304" id="Seg_5951" s="T303">pers</ta>
            <ta e="T305" id="Seg_5952" s="T304">v</ta>
            <ta e="T306" id="Seg_5953" s="T305">adj</ta>
            <ta e="T307" id="Seg_5954" s="T306">n</ta>
            <ta e="T308" id="Seg_5955" s="T307">adj</ta>
            <ta e="T309" id="Seg_5956" s="T308">n</ta>
            <ta e="T310" id="Seg_5957" s="T309">n</ta>
            <ta e="T311" id="Seg_5958" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_5959" s="T311">v</ta>
            <ta e="T313" id="Seg_5960" s="T312">v</ta>
            <ta e="T314" id="Seg_5961" s="T313">n</ta>
            <ta e="T315" id="Seg_5962" s="T314">v</ta>
            <ta e="T316" id="Seg_5963" s="T315">adj</ta>
            <ta e="T317" id="Seg_5964" s="T316">n</ta>
            <ta e="T318" id="Seg_5965" s="T317">dem</ta>
            <ta e="T319" id="Seg_5966" s="T318">n</ta>
            <ta e="T320" id="Seg_5967" s="T319">interrog</ta>
            <ta e="T321" id="Seg_5968" s="T320">v</ta>
            <ta e="T322" id="Seg_5969" s="T321">n</ta>
            <ta e="T323" id="Seg_5970" s="T322">v</ta>
            <ta e="T324" id="Seg_5971" s="T323">n</ta>
            <ta e="T325" id="Seg_5972" s="T324">ptcp</ta>
            <ta e="T326" id="Seg_5973" s="T325">pers</ta>
            <ta e="T327" id="Seg_5974" s="T326">n</ta>
            <ta e="T328" id="Seg_5975" s="T327">dem</ta>
            <ta e="T329" id="Seg_5976" s="T328">n</ta>
            <ta e="T330" id="Seg_5977" s="T329">n</ta>
            <ta e="T331" id="Seg_5978" s="T330">num</ta>
            <ta e="T332" id="Seg_5979" s="T331">n</ta>
            <ta e="T333" id="Seg_5980" s="T332">v</ta>
            <ta e="T334" id="Seg_5981" s="T333">n</ta>
            <ta e="T335" id="Seg_5982" s="T334">pp</ta>
            <ta e="T336" id="Seg_5983" s="T335">nprop</ta>
            <ta e="T337" id="Seg_5984" s="T336">adv</ta>
            <ta e="T338" id="Seg_5985" s="T337">pers</ta>
            <ta e="T339" id="Seg_5986" s="T338">v</ta>
            <ta e="T340" id="Seg_5987" s="T339">n</ta>
            <ta e="T341" id="Seg_5988" s="T340">adv</ta>
            <ta e="T342" id="Seg_5989" s="T341">emphpro</ta>
            <ta e="T343" id="Seg_5990" s="T342">n</ta>
            <ta e="T344" id="Seg_5991" s="T343">conj</ta>
            <ta e="T345" id="Seg_5992" s="T344">pers</ta>
            <ta e="T346" id="Seg_5993" s="T345">n</ta>
            <ta e="T347" id="Seg_5994" s="T346">nprop</ta>
            <ta e="T348" id="Seg_5995" s="T347">nprop</ta>
            <ta e="T349" id="Seg_5996" s="T348">conj</ta>
            <ta e="T350" id="Seg_5997" s="T349">nprop</ta>
            <ta e="T351" id="Seg_5998" s="T350">dem</ta>
            <ta e="T352" id="Seg_5999" s="T351">n</ta>
            <ta e="T353" id="Seg_6000" s="T352">pers</ta>
            <ta e="T354" id="Seg_6001" s="T353">n</ta>
            <ta e="T355" id="Seg_6002" s="T354">num</ta>
            <ta e="T356" id="Seg_6003" s="T355">v</ta>
            <ta e="T357" id="Seg_6004" s="T356">ptcp</ta>
            <ta e="T358" id="Seg_6005" s="T357">n</ta>
            <ta e="T359" id="Seg_6006" s="T358">nprop</ta>
            <ta e="T360" id="Seg_6007" s="T359">pro</ta>
            <ta e="T361" id="Seg_6008" s="T360">v</ta>
            <ta e="T362" id="Seg_6009" s="T361">adv</ta>
            <ta e="T363" id="Seg_6010" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_6011" s="T363">adv</ta>
            <ta e="T365" id="Seg_6012" s="T364">adj</ta>
            <ta e="T366" id="Seg_6013" s="T365">n</ta>
            <ta e="T367" id="Seg_6014" s="T366">n</ta>
            <ta e="T368" id="Seg_6015" s="T367">adj</ta>
            <ta e="T369" id="Seg_6016" s="T368">v</ta>
            <ta e="T370" id="Seg_6017" s="T369">n</ta>
            <ta e="T371" id="Seg_6018" s="T370">v</ta>
            <ta e="T372" id="Seg_6019" s="T371">n</ta>
            <ta e="T373" id="Seg_6020" s="T372">n</ta>
            <ta e="T374" id="Seg_6021" s="T373">adv</ta>
            <ta e="T375" id="Seg_6022" s="T374">v</ta>
            <ta e="T376" id="Seg_6023" s="T375">v</ta>
            <ta e="T377" id="Seg_6024" s="T376">n</ta>
            <ta e="T378" id="Seg_6025" s="T377">v</ta>
            <ta e="T379" id="Seg_6026" s="T378">dem</ta>
            <ta e="T380" id="Seg_6027" s="T379">n</ta>
            <ta e="T381" id="Seg_6028" s="T380">v</ta>
            <ta e="T382" id="Seg_6029" s="T381">num</ta>
            <ta e="T383" id="Seg_6030" s="T382">n</ta>
            <ta e="T384" id="Seg_6031" s="T383">adj</ta>
            <ta e="T385" id="Seg_6032" s="T384">n</ta>
            <ta e="T386" id="Seg_6033" s="T385">num</ta>
            <ta e="T387" id="Seg_6034" s="T386">n</ta>
            <ta e="T388" id="Seg_6035" s="T387">n</ta>
            <ta e="T389" id="Seg_6036" s="T388">pers</ta>
            <ta e="T390" id="Seg_6037" s="T389">nprop</ta>
            <ta e="T391" id="Seg_6038" s="T390">v</ta>
            <ta e="T392" id="Seg_6039" s="T391">n</ta>
            <ta e="T393" id="Seg_6040" s="T392">dem</ta>
            <ta e="T394" id="Seg_6041" s="T393">n</ta>
            <ta e="T395" id="Seg_6042" s="T394">v</ta>
            <ta e="T396" id="Seg_6043" s="T395">num</ta>
            <ta e="T397" id="Seg_6044" s="T396">n</ta>
            <ta e="T398" id="Seg_6045" s="T397">adj</ta>
            <ta e="T399" id="Seg_6046" s="T398">n</ta>
            <ta e="T400" id="Seg_6047" s="T399">v</ta>
            <ta e="T401" id="Seg_6048" s="T400">adj</ta>
            <ta e="T402" id="Seg_6049" s="T401">n</ta>
            <ta e="T403" id="Seg_6050" s="T402">pp</ta>
            <ta e="T404" id="Seg_6051" s="T403">adv</ta>
            <ta e="T405" id="Seg_6052" s="T404">v</ta>
            <ta e="T406" id="Seg_6053" s="T405">adv</ta>
            <ta e="T407" id="Seg_6054" s="T406">adv</ta>
            <ta e="T408" id="Seg_6055" s="T407">conj</ta>
            <ta e="T409" id="Seg_6056" s="T408">v</ta>
            <ta e="T410" id="Seg_6057" s="T409">pers</ta>
            <ta e="T411" id="Seg_6058" s="T410">n</ta>
            <ta e="T412" id="Seg_6059" s="T411">n</ta>
            <ta e="T413" id="Seg_6060" s="T412">pers</ta>
            <ta e="T414" id="Seg_6061" s="T413">nprop</ta>
            <ta e="T415" id="Seg_6062" s="T414">v</ta>
            <ta e="T416" id="Seg_6063" s="T415">v</ta>
            <ta e="T417" id="Seg_6064" s="T416">n</ta>
            <ta e="T418" id="Seg_6065" s="T417">v</ta>
            <ta e="T419" id="Seg_6066" s="T418">n</ta>
            <ta e="T420" id="Seg_6067" s="T419">v</ta>
            <ta e="T421" id="Seg_6068" s="T420">nprop</ta>
            <ta e="T422" id="Seg_6069" s="T421">pers</ta>
            <ta e="T423" id="Seg_6070" s="T422">adv</ta>
            <ta e="T424" id="Seg_6071" s="T423">v</ta>
            <ta e="T425" id="Seg_6072" s="T424">adj</ta>
            <ta e="T426" id="Seg_6073" s="T425">adj</ta>
            <ta e="T427" id="Seg_6074" s="T426">n</ta>
            <ta e="T428" id="Seg_6075" s="T427">v</ta>
            <ta e="T429" id="Seg_6076" s="T428">adv</ta>
            <ta e="T430" id="Seg_6077" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_6078" s="T430">v</ta>
            <ta e="T432" id="Seg_6079" s="T431">n</ta>
            <ta e="T433" id="Seg_6080" s="T432">v</ta>
            <ta e="T434" id="Seg_6081" s="T433">quant</ta>
            <ta e="T435" id="Seg_6082" s="T434">v</ta>
            <ta e="T436" id="Seg_6083" s="T435">n</ta>
            <ta e="T437" id="Seg_6084" s="T436">conj</ta>
            <ta e="T438" id="Seg_6085" s="T437">adv</ta>
            <ta e="T439" id="Seg_6086" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_6087" s="T439">adv</ta>
            <ta e="T441" id="Seg_6088" s="T440">v</ta>
            <ta e="T442" id="Seg_6089" s="T441">num</ta>
            <ta e="T443" id="Seg_6090" s="T442">n</ta>
            <ta e="T444" id="Seg_6091" s="T443">n</ta>
            <ta e="T445" id="Seg_6092" s="T444">v</ta>
            <ta e="T446" id="Seg_6093" s="T445">pers</ta>
            <ta e="T447" id="Seg_6094" s="T446">pers</ta>
            <ta e="T448" id="Seg_6095" s="T447">v</ta>
            <ta e="T449" id="Seg_6096" s="T448">pers</ta>
            <ta e="T450" id="Seg_6097" s="T449">pers</ta>
            <ta e="T451" id="Seg_6098" s="T450">v</ta>
            <ta e="T452" id="Seg_6099" s="T451">n</ta>
            <ta e="T453" id="Seg_6100" s="T452">n</ta>
            <ta e="T454" id="Seg_6101" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_6102" s="T454">adv</ta>
            <ta e="T456" id="Seg_6103" s="T455">adv</ta>
            <ta e="T457" id="Seg_6104" s="T456">n</ta>
            <ta e="T458" id="Seg_6105" s="T457">n</ta>
            <ta e="T459" id="Seg_6106" s="T458">v</ta>
            <ta e="T460" id="Seg_6107" s="T459">conj</ta>
            <ta e="T461" id="Seg_6108" s="T460">v</ta>
            <ta e="T462" id="Seg_6109" s="T461">v</ta>
            <ta e="T463" id="Seg_6110" s="T462">v</ta>
            <ta e="T464" id="Seg_6111" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_6112" s="T464">v</ta>
            <ta e="T466" id="Seg_6113" s="T465">v</ta>
            <ta e="T467" id="Seg_6114" s="T466">n</ta>
            <ta e="T468" id="Seg_6115" s="T467">v</ta>
            <ta e="T469" id="Seg_6116" s="T468">pers</ta>
            <ta e="T470" id="Seg_6117" s="T469">adj</ta>
            <ta e="T471" id="Seg_6118" s="T470">n</ta>
            <ta e="T472" id="Seg_6119" s="T471">n</ta>
            <ta e="T473" id="Seg_6120" s="T472">pers</ta>
            <ta e="T474" id="Seg_6121" s="T473">nprop</ta>
            <ta e="T475" id="Seg_6122" s="T474">pro</ta>
            <ta e="T476" id="Seg_6123" s="T475">v</ta>
            <ta e="T477" id="Seg_6124" s="T476">adj</ta>
            <ta e="T478" id="Seg_6125" s="T477">n</ta>
            <ta e="T479" id="Seg_6126" s="T478">n</ta>
            <ta e="T480" id="Seg_6127" s="T479">pers</ta>
            <ta e="T481" id="Seg_6128" s="T480">n</ta>
            <ta e="T482" id="Seg_6129" s="T481">v</ta>
            <ta e="T483" id="Seg_6130" s="T482">pers</ta>
            <ta e="T484" id="Seg_6131" s="T483">pers</ta>
            <ta e="T485" id="Seg_6132" s="T484">adj</ta>
            <ta e="T486" id="Seg_6133" s="T485">n</ta>
            <ta e="T487" id="Seg_6134" s="T486">interrog</ta>
            <ta e="T488" id="Seg_6135" s="T487">pers</ta>
            <ta e="T489" id="Seg_6136" s="T488">v</ta>
            <ta e="T490" id="Seg_6137" s="T489">v</ta>
            <ta e="T491" id="Seg_6138" s="T490">pers</ta>
            <ta e="T492" id="Seg_6139" s="T491">v</ta>
            <ta e="T493" id="Seg_6140" s="T492">v</ta>
            <ta e="T494" id="Seg_6141" s="T493">pers</ta>
            <ta e="T495" id="Seg_6142" s="T494">adj</ta>
            <ta e="T496" id="Seg_6143" s="T495">n</ta>
            <ta e="T497" id="Seg_6144" s="T496">v</ta>
            <ta e="T498" id="Seg_6145" s="T497">adj</ta>
            <ta e="T499" id="Seg_6146" s="T498">n</ta>
            <ta e="T500" id="Seg_6147" s="T499">pers</ta>
            <ta e="T501" id="Seg_6148" s="T500">pers</ta>
            <ta e="T502" id="Seg_6149" s="T501">v</ta>
            <ta e="T503" id="Seg_6150" s="T502">v</ta>
            <ta e="T504" id="Seg_6151" s="T503">adv</ta>
            <ta e="T505" id="Seg_6152" s="T504">v</ta>
            <ta e="T506" id="Seg_6153" s="T505">dem</ta>
            <ta e="T507" id="Seg_6154" s="T506">n</ta>
            <ta e="T508" id="Seg_6155" s="T507">pers</ta>
            <ta e="T509" id="Seg_6156" s="T508">n</ta>
            <ta e="T510" id="Seg_6157" s="T509">num</ta>
            <ta e="T511" id="Seg_6158" s="T510">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T8" id="Seg_6159" s="T7">np:Time</ta>
            <ta e="T17" id="Seg_6160" s="T16">np:Time</ta>
            <ta e="T18" id="Seg_6161" s="T17">pro.h:A</ta>
            <ta e="T21" id="Seg_6162" s="T20">pp:Path</ta>
            <ta e="T23" id="Seg_6163" s="T22">np.h:Poss</ta>
            <ta e="T24" id="Seg_6164" s="T23">np:G</ta>
            <ta e="T25" id="Seg_6165" s="T24">np:Th</ta>
            <ta e="T26" id="Seg_6166" s="T25">np:Poss</ta>
            <ta e="T27" id="Seg_6167" s="T26">np:Poss</ta>
            <ta e="T29" id="Seg_6168" s="T28">np:Poss</ta>
            <ta e="T32" id="Seg_6169" s="T31">0.3:Th</ta>
            <ta e="T33" id="Seg_6170" s="T32">adv:Time</ta>
            <ta e="T37" id="Seg_6171" s="T36">pro.h:Poss</ta>
            <ta e="T38" id="Seg_6172" s="T37">np:Th 0.1.h:Poss</ta>
            <ta e="T43" id="Seg_6173" s="T42">0.3:Th</ta>
            <ta e="T46" id="Seg_6174" s="T45">pp:Path</ta>
            <ta e="T51" id="Seg_6175" s="T50">0.3:P</ta>
            <ta e="T54" id="Seg_6176" s="T53">np.h:A</ta>
            <ta e="T56" id="Seg_6177" s="T55">pp:L</ta>
            <ta e="T58" id="Seg_6178" s="T57">pro.h:Poss</ta>
            <ta e="T59" id="Seg_6179" s="T58">np:Th 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_6180" s="T61">np.h:Poss</ta>
            <ta e="T63" id="Seg_6181" s="T62">np.h:Poss</ta>
            <ta e="T64" id="Seg_6182" s="T63">np:Th 0.3.h:Poss</ta>
            <ta e="T65" id="Seg_6183" s="T64">pro.h:E</ta>
            <ta e="T67" id="Seg_6184" s="T66">pro.h:A</ta>
            <ta e="T70" id="Seg_6185" s="T69">np:Th</ta>
            <ta e="T71" id="Seg_6186" s="T70">pro.h:Poss</ta>
            <ta e="T72" id="Seg_6187" s="T71">np:P</ta>
            <ta e="T75" id="Seg_6188" s="T74">np:Time</ta>
            <ta e="T77" id="Seg_6189" s="T76">0.1.h:E</ta>
            <ta e="T78" id="Seg_6190" s="T77">v:Th</ta>
            <ta e="T79" id="Seg_6191" s="T78">np:Th</ta>
            <ta e="T81" id="Seg_6192" s="T80">0.1.h:A</ta>
            <ta e="T84" id="Seg_6193" s="T83">np:A 0.1.h:Poss</ta>
            <ta e="T87" id="Seg_6194" s="T86">pro.h:A</ta>
            <ta e="T89" id="Seg_6195" s="T88">np:G</ta>
            <ta e="T91" id="Seg_6196" s="T90">0.1.h:E</ta>
            <ta e="T92" id="Seg_6197" s="T91">np:A 0.1.h:Poss</ta>
            <ta e="T95" id="Seg_6198" s="T94">np:Th</ta>
            <ta e="T96" id="Seg_6199" s="T95">np:L</ta>
            <ta e="T97" id="Seg_6200" s="T96">pro.h:E</ta>
            <ta e="T98" id="Seg_6201" s="T97">0.3:Th</ta>
            <ta e="T99" id="Seg_6202" s="T98">pro:P</ta>
            <ta e="T100" id="Seg_6203" s="T99">v:Th</ta>
            <ta e="T104" id="Seg_6204" s="T103">pro:P</ta>
            <ta e="T106" id="Seg_6205" s="T105">0.1.h:A</ta>
            <ta e="T107" id="Seg_6206" s="T106">pro:Th</ta>
            <ta e="T110" id="Seg_6207" s="T109">np:P</ta>
            <ta e="T112" id="Seg_6208" s="T111">0.1.h:A</ta>
            <ta e="T118" id="Seg_6209" s="T117">np:Poss</ta>
            <ta e="T120" id="Seg_6210" s="T119">np:Poss</ta>
            <ta e="T124" id="Seg_6211" s="T123">np:P 0.1.h:Poss</ta>
            <ta e="T126" id="Seg_6212" s="T125">np:Th 0.1.h:Poss</ta>
            <ta e="T132" id="Seg_6213" s="T131">np:Th</ta>
            <ta e="T137" id="Seg_6214" s="T136">np.h:Poss</ta>
            <ta e="T138" id="Seg_6215" s="T137">np:L</ta>
            <ta e="T139" id="Seg_6216" s="T138">np:Path</ta>
            <ta e="T141" id="Seg_6217" s="T140">0.3:P</ta>
            <ta e="T142" id="Seg_6218" s="T141">v:Th</ta>
            <ta e="T143" id="Seg_6219" s="T142">0.3:A</ta>
            <ta e="T144" id="Seg_6220" s="T143">np:P 0.1.h:Poss</ta>
            <ta e="T149" id="Seg_6221" s="T148">np:Th 0.1.h:Poss</ta>
            <ta e="T151" id="Seg_6222" s="T150">pro:Th 0.1.h:Poss</ta>
            <ta e="T153" id="Seg_6223" s="T152">0.1.h:A</ta>
            <ta e="T154" id="Seg_6224" s="T153">np:Poss</ta>
            <ta e="T157" id="Seg_6225" s="T156">np:G</ta>
            <ta e="T159" id="Seg_6226" s="T158">np:L</ta>
            <ta e="T160" id="Seg_6227" s="T159">0.1.h:A</ta>
            <ta e="T161" id="Seg_6228" s="T160">np:P</ta>
            <ta e="T162" id="Seg_6229" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_6230" s="T162">np:Poss</ta>
            <ta e="T164" id="Seg_6231" s="T163">np:P</ta>
            <ta e="T165" id="Seg_6232" s="T164">0.1.h:A</ta>
            <ta e="T168" id="Seg_6233" s="T167">np:P</ta>
            <ta e="T169" id="Seg_6234" s="T168">np.h:Th</ta>
            <ta e="T174" id="Seg_6235" s="T173">0.3.h:E</ta>
            <ta e="T178" id="Seg_6236" s="T177">np:Th</ta>
            <ta e="T179" id="Seg_6237" s="T178">pro.h:Poss</ta>
            <ta e="T181" id="Seg_6238" s="T180">np:Poss</ta>
            <ta e="T182" id="Seg_6239" s="T181">np:Th</ta>
            <ta e="T191" id="Seg_6240" s="T190">adv:Time</ta>
            <ta e="T192" id="Seg_6241" s="T191">pro.h:A</ta>
            <ta e="T195" id="Seg_6242" s="T194">0.1.h:A</ta>
            <ta e="T197" id="Seg_6243" s="T196">np:Th</ta>
            <ta e="T199" id="Seg_6244" s="T198">v:Th</ta>
            <ta e="T200" id="Seg_6245" s="T199">pro:Th</ta>
            <ta e="T204" id="Seg_6246" s="T203">0.3:Th</ta>
            <ta e="T206" id="Seg_6247" s="T205">adv:Time</ta>
            <ta e="T209" id="Seg_6248" s="T208">0.1.h:Th</ta>
            <ta e="T210" id="Seg_6249" s="T209">np:P</ta>
            <ta e="T211" id="Seg_6250" s="T210">0.1.h:A</ta>
            <ta e="T213" id="Seg_6251" s="T212">pp:Time</ta>
            <ta e="T216" id="Seg_6252" s="T215">adv:Time</ta>
            <ta e="T217" id="Seg_6253" s="T216">np:Th 0.1.h:Poss</ta>
            <ta e="T222" id="Seg_6254" s="T221">0.3:A</ta>
            <ta e="T223" id="Seg_6255" s="T222">pro.h:E</ta>
            <ta e="T226" id="Seg_6256" s="T225">pro.h:A</ta>
            <ta e="T234" id="Seg_6257" s="T233">pro.h:A</ta>
            <ta e="T237" id="Seg_6258" s="T236">np:G</ta>
            <ta e="T239" id="Seg_6259" s="T238">np:Poss</ta>
            <ta e="T241" id="Seg_6260" s="T240">pro.h:E</ta>
            <ta e="T243" id="Seg_6261" s="T242">np:Time</ta>
            <ta e="T244" id="Seg_6262" s="T243">adv:G</ta>
            <ta e="T245" id="Seg_6263" s="T244">0.1.h:A</ta>
            <ta e="T249" id="Seg_6264" s="T248">np:Path</ta>
            <ta e="T252" id="Seg_6265" s="T251">np:Time</ta>
            <ta e="T255" id="Seg_6266" s="T254">0.1.h:A</ta>
            <ta e="T256" id="Seg_6267" s="T255">v:Th</ta>
            <ta e="T261" id="Seg_6268" s="T260">np:Path</ta>
            <ta e="T262" id="Seg_6269" s="T261">pro:P</ta>
            <ta e="T267" id="Seg_6270" s="T266">adv:Time</ta>
            <ta e="T268" id="Seg_6271" s="T267">pro.h:Th</ta>
            <ta e="T272" id="Seg_6272" s="T271">np:L</ta>
            <ta e="T273" id="Seg_6273" s="T272">pro.h:A</ta>
            <ta e="T274" id="Seg_6274" s="T273">np:P</ta>
            <ta e="T279" id="Seg_6275" s="T278">np:G</ta>
            <ta e="T280" id="Seg_6276" s="T279">pro.h:A</ta>
            <ta e="T284" id="Seg_6277" s="T283">np:Time</ta>
            <ta e="T285" id="Seg_6278" s="T284">pro.h:A</ta>
            <ta e="T288" id="Seg_6279" s="T287">np:P</ta>
            <ta e="T291" id="Seg_6280" s="T290">0.3:Th</ta>
            <ta e="T294" id="Seg_6281" s="T293">np:P</ta>
            <ta e="T297" id="Seg_6282" s="T296">np:Th</ta>
            <ta e="T300" id="Seg_6283" s="T299">0.1.h:A</ta>
            <ta e="T301" id="Seg_6284" s="T300">0.1.h:A</ta>
            <ta e="T303" id="Seg_6285" s="T302">np:P</ta>
            <ta e="T307" id="Seg_6286" s="T306">np:Th</ta>
            <ta e="T309" id="Seg_6287" s="T308">np:Th</ta>
            <ta e="T310" id="Seg_6288" s="T309">np:E 0.1.h:Poss</ta>
            <ta e="T314" id="Seg_6289" s="T313">np:Th</ta>
            <ta e="T319" id="Seg_6290" s="T318">np:Time</ta>
            <ta e="T320" id="Seg_6291" s="T319">pro:P</ta>
            <ta e="T321" id="Seg_6292" s="T320">0.1.h:A</ta>
            <ta e="T322" id="Seg_6293" s="T321">np:Th</ta>
            <ta e="T326" id="Seg_6294" s="T325">pro.h:Poss</ta>
            <ta e="T327" id="Seg_6295" s="T326">np.h:Poss</ta>
            <ta e="T329" id="Seg_6296" s="T328">np:Time</ta>
            <ta e="T330" id="Seg_6297" s="T329">np:Com</ta>
            <ta e="T333" id="Seg_6298" s="T332">0.1.h:A</ta>
            <ta e="T334" id="Seg_6299" s="T333">pp:Path</ta>
            <ta e="T336" id="Seg_6300" s="T335">np:G</ta>
            <ta e="T337" id="Seg_6301" s="T336">adv:L</ta>
            <ta e="T338" id="Seg_6302" s="T337">pro.h:A</ta>
            <ta e="T340" id="Seg_6303" s="T339">np:G</ta>
            <ta e="T343" id="Seg_6304" s="T342">np:Th 0.1.h:Poss</ta>
            <ta e="T345" id="Seg_6305" s="T344">pro.h:Poss</ta>
            <ta e="T346" id="Seg_6306" s="T345">np:Th 0.1.h:Poss</ta>
            <ta e="T352" id="Seg_6307" s="T351">np:Time</ta>
            <ta e="T353" id="Seg_6308" s="T352">pro.h:A</ta>
            <ta e="T359" id="Seg_6309" s="T358">np.h:Th</ta>
            <ta e="T367" id="Seg_6310" s="T366">np:Time</ta>
            <ta e="T369" id="Seg_6311" s="T368">0.3:Th</ta>
            <ta e="T370" id="Seg_6312" s="T369">np:Time</ta>
            <ta e="T372" id="Seg_6313" s="T371">np:Th</ta>
            <ta e="T373" id="Seg_6314" s="T372">np:Th</ta>
            <ta e="T377" id="Seg_6315" s="T376">np:E</ta>
            <ta e="T380" id="Seg_6316" s="T379">np:Time</ta>
            <ta e="T381" id="Seg_6317" s="T380">0.1.h:A</ta>
            <ta e="T383" id="Seg_6318" s="T382">np:Th</ta>
            <ta e="T385" id="Seg_6319" s="T384">np:Time</ta>
            <ta e="T387" id="Seg_6320" s="T386">np.h:A</ta>
            <ta e="T388" id="Seg_6321" s="T387">np:Th 0.3.h:Poss</ta>
            <ta e="T389" id="Seg_6322" s="T388">pro.h:Poss</ta>
            <ta e="T392" id="Seg_6323" s="T391">np:G</ta>
            <ta e="T394" id="Seg_6324" s="T393">np:Time</ta>
            <ta e="T395" id="Seg_6325" s="T394">0.1.h:A</ta>
            <ta e="T397" id="Seg_6326" s="T396">np:Th</ta>
            <ta e="T399" id="Seg_6327" s="T398">np:Time</ta>
            <ta e="T400" id="Seg_6328" s="T399">0.1.h:A</ta>
            <ta e="T402" id="Seg_6329" s="T401">pp:Path</ta>
            <ta e="T405" id="Seg_6330" s="T404">0.1.h:A</ta>
            <ta e="T410" id="Seg_6331" s="T409">pro.h:Poss</ta>
            <ta e="T411" id="Seg_6332" s="T410">np:A 0.1.h:Poss</ta>
            <ta e="T412" id="Seg_6333" s="T411">np:Th 0.3:Poss</ta>
            <ta e="T413" id="Seg_6334" s="T412">pro.h:Poss</ta>
            <ta e="T415" id="Seg_6335" s="T414">0.1.h:E</ta>
            <ta e="T417" id="Seg_6336" s="T416">np:A 0.1.h:Poss</ta>
            <ta e="T419" id="Seg_6337" s="T418">np:G</ta>
            <ta e="T421" id="Seg_6338" s="T420">np:G</ta>
            <ta e="T422" id="Seg_6339" s="T421">pro:A</ta>
            <ta e="T427" id="Seg_6340" s="T426">np:G</ta>
            <ta e="T428" id="Seg_6341" s="T427">0.1.h:A</ta>
            <ta e="T429" id="Seg_6342" s="T428">adv:G</ta>
            <ta e="T431" id="Seg_6343" s="T430">0.1.h:E</ta>
            <ta e="T435" id="Seg_6344" s="T434">0.1.h:A</ta>
            <ta e="T436" id="Seg_6345" s="T435">np:Ins</ta>
            <ta e="T438" id="Seg_6346" s="T437">adv:Time</ta>
            <ta e="T441" id="Seg_6347" s="T440">0.1.h:A</ta>
            <ta e="T444" id="Seg_6348" s="T443">np:Ins</ta>
            <ta e="T446" id="Seg_6349" s="T445">pro.h:E</ta>
            <ta e="T447" id="Seg_6350" s="T446">pro:Th</ta>
            <ta e="T448" id="Seg_6351" s="T447">v:Th</ta>
            <ta e="T449" id="Seg_6352" s="T448">pro.h:E</ta>
            <ta e="T450" id="Seg_6353" s="T449">pro:Th</ta>
            <ta e="T452" id="Seg_6354" s="T451">np:Poss</ta>
            <ta e="T453" id="Seg_6355" s="T452">np:L</ta>
            <ta e="T457" id="Seg_6356" s="T456">np:Poss</ta>
            <ta e="T458" id="Seg_6357" s="T457">np:G</ta>
            <ta e="T459" id="Seg_6358" s="T458">0.1.h:A</ta>
            <ta e="T461" id="Seg_6359" s="T460">0.1.h:A</ta>
            <ta e="T462" id="Seg_6360" s="T461">0.1.h:A</ta>
            <ta e="T463" id="Seg_6361" s="T462">0.1.h:Th</ta>
            <ta e="T465" id="Seg_6362" s="T464">0.1.h:E</ta>
            <ta e="T466" id="Seg_6363" s="T465">v:Th</ta>
            <ta e="T467" id="Seg_6364" s="T466">np:Th</ta>
            <ta e="T469" id="Seg_6365" s="T468">pro.h:A</ta>
            <ta e="T471" id="Seg_6366" s="T470">np:A</ta>
            <ta e="T472" id="Seg_6367" s="T471">np:Th 0.3:Poss</ta>
            <ta e="T473" id="Seg_6368" s="T472">pro:Poss</ta>
            <ta e="T475" id="Seg_6369" s="T474">pro:Th</ta>
            <ta e="T478" id="Seg_6370" s="T477">np:Poss</ta>
            <ta e="T479" id="Seg_6371" s="T478">np:L</ta>
            <ta e="T480" id="Seg_6372" s="T479">pro.h:A</ta>
            <ta e="T481" id="Seg_6373" s="T480">np:Ins</ta>
            <ta e="T483" id="Seg_6374" s="T482">pro:G</ta>
            <ta e="T484" id="Seg_6375" s="T483">pro:A</ta>
            <ta e="T486" id="Seg_6376" s="T485">np:P</ta>
            <ta e="T488" id="Seg_6377" s="T487">pro.h:Th</ta>
            <ta e="T490" id="Seg_6378" s="T489">0.1.h:A</ta>
            <ta e="T491" id="Seg_6379" s="T490">pro:A</ta>
            <ta e="T492" id="Seg_6380" s="T491">0.3:P</ta>
            <ta e="T493" id="Seg_6381" s="T492">0.3:A</ta>
            <ta e="T494" id="Seg_6382" s="T493">pro.h:B</ta>
            <ta e="T496" id="Seg_6383" s="T495">np:Th 0.3:Poss</ta>
            <ta e="T499" id="Seg_6384" s="T498">np:Th</ta>
            <ta e="T500" id="Seg_6385" s="T499">pro.h:A</ta>
            <ta e="T501" id="Seg_6386" s="T500">pro:Com</ta>
            <ta e="T503" id="Seg_6387" s="T502">0.1.h:A</ta>
            <ta e="T505" id="Seg_6388" s="T504">0.1.h:A</ta>
            <ta e="T507" id="Seg_6389" s="T506">np:Time</ta>
            <ta e="T508" id="Seg_6390" s="T507">pro.h:A</ta>
            <ta e="T511" id="Seg_6391" s="T510">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T18" id="Seg_6392" s="T17">pro.h:S</ta>
            <ta e="T19" id="Seg_6393" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_6394" s="T19">s:purp</ta>
            <ta e="T25" id="Seg_6395" s="T24">np:S</ta>
            <ta e="T30" id="Seg_6396" s="T29">n:pred</ta>
            <ta e="T32" id="Seg_6397" s="T31">0.3:S v:pred</ta>
            <ta e="T38" id="Seg_6398" s="T37">np:S</ta>
            <ta e="T39" id="Seg_6399" s="T38">cop</ta>
            <ta e="T40" id="Seg_6400" s="T39">adj:pred</ta>
            <ta e="T42" id="Seg_6401" s="T41">adj:pred</ta>
            <ta e="T43" id="Seg_6402" s="T42">0.3:S cop</ta>
            <ta e="T51" id="Seg_6403" s="T50">0.3:S v:pred</ta>
            <ta e="T54" id="Seg_6404" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_6405" s="T54">v:pred</ta>
            <ta e="T59" id="Seg_6406" s="T58">np:S</ta>
            <ta e="T64" id="Seg_6407" s="T63">np:O</ta>
            <ta e="T65" id="Seg_6408" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_6409" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_6410" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_6411" s="T67">v:pred</ta>
            <ta e="T72" id="Seg_6412" s="T71">np:S</ta>
            <ta e="T73" id="Seg_6413" s="T72">v:pred</ta>
            <ta e="T77" id="Seg_6414" s="T76">0.1.h:S v:pred</ta>
            <ta e="T78" id="Seg_6415" s="T77">v:O</ta>
            <ta e="T81" id="Seg_6416" s="T80">0.1.h:S v:pred</ta>
            <ta e="T83" id="Seg_6417" s="T82">np:O</ta>
            <ta e="T84" id="Seg_6418" s="T83">np:S</ta>
            <ta e="T86" id="Seg_6419" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_6420" s="T86">pro.h:S</ta>
            <ta e="T88" id="Seg_6421" s="T87">v:pred</ta>
            <ta e="T91" id="Seg_6422" s="T90">0.1.h:S v:pred 0.3:O</ta>
            <ta e="T92" id="Seg_6423" s="T91">np:S</ta>
            <ta e="T93" id="Seg_6424" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_6425" s="T94">np:O</ta>
            <ta e="T97" id="Seg_6426" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_6427" s="T97">v:pred 0.3:O</ta>
            <ta e="T100" id="Seg_6428" s="T99">v:O</ta>
            <ta e="T102" id="Seg_6429" s="T101">0.1.h:S v:pred</ta>
            <ta e="T104" id="Seg_6430" s="T103">pro:O</ta>
            <ta e="T106" id="Seg_6431" s="T105">0.1.h:S v:pred</ta>
            <ta e="T107" id="Seg_6432" s="T106">pro:S</ta>
            <ta e="T108" id="Seg_6433" s="T107">adj:pred</ta>
            <ta e="T110" id="Seg_6434" s="T109">np:O</ta>
            <ta e="T112" id="Seg_6435" s="T111">0.1.h:S v:pred</ta>
            <ta e="T120" id="Seg_6436" s="T112">s:temp</ta>
            <ta e="T123" id="Seg_6437" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_6438" s="T123">np:S</ta>
            <ta e="T126" id="Seg_6439" s="T125">np:S</ta>
            <ta e="T127" id="Seg_6440" s="T126">s:rel</ta>
            <ta e="T131" id="Seg_6441" s="T128">s:rel</ta>
            <ta e="T132" id="Seg_6442" s="T131">np:S</ta>
            <ta e="T133" id="Seg_6443" s="T132">cop</ta>
            <ta e="T135" id="Seg_6444" s="T134">adj:pred</ta>
            <ta e="T141" id="Seg_6445" s="T140">0.3:S v:pred</ta>
            <ta e="T142" id="Seg_6446" s="T141">v:O</ta>
            <ta e="T143" id="Seg_6447" s="T142">0.3:S v:pred</ta>
            <ta e="T144" id="Seg_6448" s="T143">np:S</ta>
            <ta e="T145" id="Seg_6449" s="T144">v:pred</ta>
            <ta e="T149" id="Seg_6450" s="T148">np:O</ta>
            <ta e="T151" id="Seg_6451" s="T150">pro:O</ta>
            <ta e="T153" id="Seg_6452" s="T152">0.1.h:S v:pred</ta>
            <ta e="T160" id="Seg_6453" s="T159">0.1.h:S v:pred</ta>
            <ta e="T161" id="Seg_6454" s="T160">np:O</ta>
            <ta e="T162" id="Seg_6455" s="T161">0.1.h:S v:pred</ta>
            <ta e="T164" id="Seg_6456" s="T163">np:O</ta>
            <ta e="T165" id="Seg_6457" s="T164">0.1.h:S v:pred</ta>
            <ta e="T168" id="Seg_6458" s="T167">np:O</ta>
            <ta e="T169" id="Seg_6459" s="T168">np.h:S</ta>
            <ta e="T171" id="Seg_6460" s="T170">v:pred</ta>
            <ta e="T174" id="Seg_6461" s="T173">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_6462" s="T174">s:compl</ta>
            <ta e="T180" id="Seg_6463" s="T179">v:pred</ta>
            <ta e="T182" id="Seg_6464" s="T181">np:S</ta>
            <ta e="T192" id="Seg_6465" s="T191">pro.h:S</ta>
            <ta e="T193" id="Seg_6466" s="T192">v:pred</ta>
            <ta e="T195" id="Seg_6467" s="T194">0.1.h:S v:pred</ta>
            <ta e="T197" id="Seg_6468" s="T196">np:O</ta>
            <ta e="T199" id="Seg_6469" s="T198">v:S</ta>
            <ta e="T203" id="Seg_6470" s="T202">adj:pred</ta>
            <ta e="T204" id="Seg_6471" s="T203">0.3:S cop</ta>
            <ta e="T209" id="Seg_6472" s="T208">0.1.h:S v:pred</ta>
            <ta e="T210" id="Seg_6473" s="T209">np:O</ta>
            <ta e="T211" id="Seg_6474" s="T210">0.1.h:S v:pred</ta>
            <ta e="T217" id="Seg_6475" s="T216">np:S</ta>
            <ta e="T219" id="Seg_6476" s="T218">v:pred</ta>
            <ta e="T222" id="Seg_6477" s="T221">0.3:S v:pred</ta>
            <ta e="T223" id="Seg_6478" s="T222">pro.h:S</ta>
            <ta e="T225" id="Seg_6479" s="T224">v:pred</ta>
            <ta e="T226" id="Seg_6480" s="T225">pro.h:S</ta>
            <ta e="T229" id="Seg_6481" s="T228">v:pred</ta>
            <ta e="T234" id="Seg_6482" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_6483" s="T234">v:pred</ta>
            <ta e="T241" id="Seg_6484" s="T240">pro.h:S</ta>
            <ta e="T242" id="Seg_6485" s="T241">v:pred</ta>
            <ta e="T245" id="Seg_6486" s="T244">0.1.h:S v:pred</ta>
            <ta e="T255" id="Seg_6487" s="T254">0.1.h:S v:pred</ta>
            <ta e="T256" id="Seg_6488" s="T255">v:S</ta>
            <ta e="T257" id="Seg_6489" s="T256">adj:pred</ta>
            <ta e="T258" id="Seg_6490" s="T257">cop</ta>
            <ta e="T263" id="Seg_6491" s="T261">s:rel</ta>
            <ta e="T268" id="Seg_6492" s="T267">pro.h:S</ta>
            <ta e="T269" id="Seg_6493" s="T268">v:pred</ta>
            <ta e="T273" id="Seg_6494" s="T272">pro.h:S</ta>
            <ta e="T274" id="Seg_6495" s="T273">np:O</ta>
            <ta e="T276" id="Seg_6496" s="T275">v:pred</ta>
            <ta e="T279" id="Seg_6497" s="T276">s:temp</ta>
            <ta e="T280" id="Seg_6498" s="T279">pro.h:S</ta>
            <ta e="T281" id="Seg_6499" s="T280">v:pred</ta>
            <ta e="T282" id="Seg_6500" s="T281">s:purp</ta>
            <ta e="T285" id="Seg_6501" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_6502" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_6503" s="T287">np:O</ta>
            <ta e="T291" id="Seg_6504" s="T290">0.3:S v:pred</ta>
            <ta e="T294" id="Seg_6505" s="T293">np:O</ta>
            <ta e="T295" id="Seg_6506" s="T294">v:pred</ta>
            <ta e="T297" id="Seg_6507" s="T296">np:S</ta>
            <ta e="T300" id="Seg_6508" s="T299">0.1.h:S v:pred</ta>
            <ta e="T301" id="Seg_6509" s="T300">0.1.h:S v:pred</ta>
            <ta e="T303" id="Seg_6510" s="T302">np:O</ta>
            <ta e="T305" id="Seg_6511" s="T304">v:pred</ta>
            <ta e="T307" id="Seg_6512" s="T306">np:S</ta>
            <ta e="T309" id="Seg_6513" s="T308">np:S</ta>
            <ta e="T310" id="Seg_6514" s="T309">np:S</ta>
            <ta e="T312" id="Seg_6515" s="T311">v:pred</ta>
            <ta e="T314" id="Seg_6516" s="T313">np:S</ta>
            <ta e="T315" id="Seg_6517" s="T314">cop</ta>
            <ta e="T317" id="Seg_6518" s="T316">n:pred</ta>
            <ta e="T320" id="Seg_6519" s="T319">pro:O</ta>
            <ta e="T321" id="Seg_6520" s="T320">0.1.h:S v:pred</ta>
            <ta e="T322" id="Seg_6521" s="T321">np:S</ta>
            <ta e="T323" id="Seg_6522" s="T322">cop</ta>
            <ta e="T324" id="Seg_6523" s="T323">n:pred</ta>
            <ta e="T327" id="Seg_6524" s="T324">s:rel</ta>
            <ta e="T333" id="Seg_6525" s="T332">0.1.h:S v:pred</ta>
            <ta e="T338" id="Seg_6526" s="T337">pro.h:S</ta>
            <ta e="T339" id="Seg_6527" s="T338">v:pred</ta>
            <ta e="T341" id="Seg_6528" s="T340">s:adv</ta>
            <ta e="T343" id="Seg_6529" s="T342">np:O</ta>
            <ta e="T346" id="Seg_6530" s="T345">np:S</ta>
            <ta e="T353" id="Seg_6531" s="T352">pro.h:S</ta>
            <ta e="T354" id="Seg_6532" s="T353">v:pred</ta>
            <ta e="T359" id="Seg_6533" s="T358">np.h:O</ta>
            <ta e="T368" id="Seg_6534" s="T367">adj:pred</ta>
            <ta e="T369" id="Seg_6535" s="T368">0.3:S cop</ta>
            <ta e="T371" id="Seg_6536" s="T370">v:pred</ta>
            <ta e="T372" id="Seg_6537" s="T371">np:S</ta>
            <ta e="T373" id="Seg_6538" s="T372">np:S</ta>
            <ta e="T374" id="Seg_6539" s="T373">adj:pred</ta>
            <ta e="T375" id="Seg_6540" s="T374">cop</ta>
            <ta e="T377" id="Seg_6541" s="T376">np:S</ta>
            <ta e="T378" id="Seg_6542" s="T377">v:pred</ta>
            <ta e="T381" id="Seg_6543" s="T380">0.1.h:S v:pred</ta>
            <ta e="T383" id="Seg_6544" s="T382">np:O</ta>
            <ta e="T387" id="Seg_6545" s="T386">np:S</ta>
            <ta e="T388" id="Seg_6546" s="T387">np:S</ta>
            <ta e="T390" id="Seg_6547" s="T389">n:pred</ta>
            <ta e="T391" id="Seg_6548" s="T390">v:pred</ta>
            <ta e="T395" id="Seg_6549" s="T394">0.1.h:S v:pred</ta>
            <ta e="T397" id="Seg_6550" s="T396">np:O</ta>
            <ta e="T400" id="Seg_6551" s="T399">0.1.h:S v:pred</ta>
            <ta e="T405" id="Seg_6552" s="T404">0.1.h:S v:pred</ta>
            <ta e="T407" id="Seg_6553" s="T405">s:temp</ta>
            <ta e="T411" id="Seg_6554" s="T407">s:temp</ta>
            <ta e="T412" id="Seg_6555" s="T411">np:S</ta>
            <ta e="T414" id="Seg_6556" s="T413">n:pred</ta>
            <ta e="T415" id="Seg_6557" s="T414">0.1.h:S v:pred</ta>
            <ta e="T417" id="Seg_6558" s="T416">np:S</ta>
            <ta e="T418" id="Seg_6559" s="T417">v:pred</ta>
            <ta e="T421" id="Seg_6560" s="T419">s:temp</ta>
            <ta e="T422" id="Seg_6561" s="T421">pro:S</ta>
            <ta e="T424" id="Seg_6562" s="T423">v:pred</ta>
            <ta e="T428" id="Seg_6563" s="T427">0.1.h:S v:pred</ta>
            <ta e="T431" id="Seg_6564" s="T430">0.1.h:S v:pred</ta>
            <ta e="T433" id="Seg_6565" s="T431">s:purp</ta>
            <ta e="T435" id="Seg_6566" s="T434">0.1.h:S v:pred</ta>
            <ta e="T441" id="Seg_6567" s="T440">0.1.h:S v:pred</ta>
            <ta e="T445" id="Seg_6568" s="T444">v:pred</ta>
            <ta e="T446" id="Seg_6569" s="T445">pro.h:S</ta>
            <ta e="T448" id="Seg_6570" s="T447">v:O</ta>
            <ta e="T449" id="Seg_6571" s="T448">pro.h:S</ta>
            <ta e="T450" id="Seg_6572" s="T449">pro:O</ta>
            <ta e="T451" id="Seg_6573" s="T450">v:pred</ta>
            <ta e="T459" id="Seg_6574" s="T458">0.1.h:S v:pred</ta>
            <ta e="T461" id="Seg_6575" s="T460">0.1.h:S v:pred</ta>
            <ta e="T462" id="Seg_6576" s="T461">0.1.h:S v:pred</ta>
            <ta e="T463" id="Seg_6577" s="T462">0.1.h:S v:pred</ta>
            <ta e="T465" id="Seg_6578" s="T464">0.1.h:S v:pred</ta>
            <ta e="T466" id="Seg_6579" s="T465">v:O</ta>
            <ta e="T468" id="Seg_6580" s="T467">v:pred</ta>
            <ta e="T469" id="Seg_6581" s="T468">pro.h:S</ta>
            <ta e="T471" id="Seg_6582" s="T470">np:S</ta>
            <ta e="T472" id="Seg_6583" s="T471">np:S</ta>
            <ta e="T474" id="Seg_6584" s="T473">n:pred</ta>
            <ta e="T475" id="Seg_6585" s="T474">pro:O</ta>
            <ta e="T476" id="Seg_6586" s="T475">v:pred</ta>
            <ta e="T480" id="Seg_6587" s="T479">pro.h:S</ta>
            <ta e="T482" id="Seg_6588" s="T481">v:pred</ta>
            <ta e="T484" id="Seg_6589" s="T483">pro:S</ta>
            <ta e="T486" id="Seg_6590" s="T485">np:O</ta>
            <ta e="T488" id="Seg_6591" s="T487">pro.h:S</ta>
            <ta e="T489" id="Seg_6592" s="T488">v:pred</ta>
            <ta e="T490" id="Seg_6593" s="T489">0.1.h:S v:pred</ta>
            <ta e="T491" id="Seg_6594" s="T490">pro:S</ta>
            <ta e="T492" id="Seg_6595" s="T491">v:pred 0.3:O</ta>
            <ta e="T493" id="Seg_6596" s="T492">0.3:S v:pred</ta>
            <ta e="T496" id="Seg_6597" s="T495">np:O</ta>
            <ta e="T499" id="Seg_6598" s="T498">np:O</ta>
            <ta e="T500" id="Seg_6599" s="T499">pro.h:S</ta>
            <ta e="T502" id="Seg_6600" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_6601" s="T502">0.1.h:S v:pred</ta>
            <ta e="T505" id="Seg_6602" s="T504">0.1.h:S v:pred</ta>
            <ta e="T508" id="Seg_6603" s="T507">pro.h:S</ta>
            <ta e="T509" id="Seg_6604" s="T508">v:pred</ta>
            <ta e="T511" id="Seg_6605" s="T510">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_6606" s="T5">RUS:cult</ta>
            <ta e="T29" id="Seg_6607" s="T28">RUS:cult</ta>
            <ta e="T49" id="Seg_6608" s="T48">RUS:mod</ta>
            <ta e="T52" id="Seg_6609" s="T51">RUS:gram</ta>
            <ta e="T60" id="Seg_6610" s="T59">RUS:cult</ta>
            <ta e="T61" id="Seg_6611" s="T60">RUS:cult</ta>
            <ta e="T62" id="Seg_6612" s="T61">RUS:cult</ta>
            <ta e="T118" id="Seg_6613" s="T117">RUS:cult</ta>
            <ta e="T119" id="Seg_6614" s="T118">RUS:gram</ta>
            <ta e="T128" id="Seg_6615" s="T127">RUS:gram</ta>
            <ta e="T147" id="Seg_6616" s="T146">RUS:gram</ta>
            <ta e="T150" id="Seg_6617" s="T149">RUS:gram</ta>
            <ta e="T169" id="Seg_6618" s="T168">RUS:cult</ta>
            <ta e="T230" id="Seg_6619" s="T229">RUS:gram</ta>
            <ta e="T231" id="Seg_6620" s="T230">RUS:cult</ta>
            <ta e="T266" id="Seg_6621" s="T265">RUS:cult</ta>
            <ta e="T292" id="Seg_6622" s="T291">RUS:gram</ta>
            <ta e="T296" id="Seg_6623" s="T295">RUS:cult</ta>
            <ta e="T306" id="Seg_6624" s="T305">RUS:cult</ta>
            <ta e="T320" id="Seg_6625" s="T319">-</ta>
            <ta e="T344" id="Seg_6626" s="T343">RUS:gram</ta>
            <ta e="T347" id="Seg_6627" s="T346">RUS:cult</ta>
            <ta e="T348" id="Seg_6628" s="T347">RUS:cult</ta>
            <ta e="T349" id="Seg_6629" s="T348">RUS:gram</ta>
            <ta e="T359" id="Seg_6630" s="T358">RUS:cult</ta>
            <ta e="T390" id="Seg_6631" s="T389">RUS:cult</ta>
            <ta e="T437" id="Seg_6632" s="T436">RUS:gram</ta>
            <ta e="T460" id="Seg_6633" s="T459">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T24" id="Seg_6634" s="T1">On the twenty-second of October 1960 I went hunting to a hunting house through a swamp.</ta>
            <ta e="T30" id="Seg_6635" s="T24">The name of the taiga is Mitrij Sidyryn Land.</ta>
            <ta e="T36" id="Seg_6636" s="T30">It was early in autumn.</ta>
            <ta e="T40" id="Seg_6637" s="T36">My road was good.</ta>
            <ta e="T51" id="Seg_6638" s="T40">Ir was comforatble to drive sledges through the swamp, [the road?] to travel by was frozen.</ta>
            <ta e="T57" id="Seg_6639" s="T51">And three friends left before me.</ta>
            <ta e="T61" id="Seg_6640" s="T57">Their names were Vasilij and Maksim.</ta>
            <ta e="T66" id="Seg_6641" s="T61">I've forgotten the name of Maksim's younger brother.</ta>
            <ta e="T70" id="Seg_6642" s="T66">A ski road was left after them.</ta>
            <ta e="T73" id="Seg_6643" s="T70">Their ski road had frozen.</ta>
            <ta e="T79" id="Seg_6644" s="T73">That day I wanted to meet a squirrel very much.</ta>
            <ta e="T81" id="Seg_6645" s="T79">I didn't meet it.</ta>
            <ta e="T86" id="Seg_6646" s="T81">My dog was barking at one squirrel.</ta>
            <ta e="T91" id="Seg_6647" s="T86">I looked at a cedar, but didn't see it.</ta>
            <ta e="T96" id="Seg_6648" s="T91">My dog found a second squirrel on a pine tree.</ta>
            <ta e="T98" id="Seg_6649" s="T96">I saw it.</ta>
            <ta e="T102" id="Seg_6650" s="T98">I didn't want to shoot it.</ta>
            <ta e="T106" id="Seg_6651" s="T102">Therefore I didn't kill it.</ta>
            <ta e="T108" id="Seg_6652" s="T106">It was red.</ta>
            <ta e="T112" id="Seg_6653" s="T108">We don't shoot red squirrels.</ta>
            <ta e="T124" id="Seg_6654" s="T112">Before I managed to reach the land, called Mitrij Sidor and Lukantij, my sledge made from boards broke.</ta>
            <ta e="T131" id="Seg_6655" s="T124">This sledge was given [to me] and found by my wife.</ta>
            <ta e="T135" id="Seg_6656" s="T131">The road was not good.</ta>
            <ta e="T141" id="Seg_6657" s="T135">The trace, left by the people who left [ahead of me], didn't freeze.</ta>
            <ta e="T143" id="Seg_6658" s="T141">It got dark.</ta>
            <ta e="T145" id="Seg_6659" s="T143">My sledge broke.</ta>
            <ta e="T157" id="Seg_6660" s="T145">I carried the broken sledge and all my belongings on my back to a cedar cape on the ridge of the swamp.</ta>
            <ta e="T168" id="Seg_6661" s="T157">At this cedar cape I made fire, broke cedar branches and made a place to sleep and to sit on.</ta>
            <ta e="T171" id="Seg_6662" s="T168">Matvey got stuck.</ta>
            <ta e="T178" id="Seg_6663" s="T171">I had one thought: how do I repair the broken sledge?</ta>
            <ta e="T182" id="Seg_6664" s="T178">I had got hare loops.</ta>
            <ta e="T197" id="Seg_6665" s="T182">On the night of the twenty-second I was working hard repairing(?) my sledge.</ta>
            <ta e="T204" id="Seg_6666" s="T197">It would be hard to carry all the things by myself. </ta>
            <ta e="T209" id="Seg_6667" s="T204">So I didn't sleep well that night.</ta>
            <ta e="T214" id="Seg_6668" s="T209">I was making the sledge till midnight.</ta>
            <ta e="T219" id="Seg_6669" s="T214">And that night my dog decided to start barking.</ta>
            <ta e="T222" id="Seg_6670" s="T219">She yapped two or three times.</ta>
            <ta e="T225" id="Seg_6671" s="T222">I was not afraid.</ta>
            <ta e="T233" id="Seg_6672" s="T225">I hold myself as the master of my fire.</ta>
            <ta e="T240" id="Seg_6673" s="T233">I saw a cedar island amid the big swamp.</ta>
            <ta e="T249" id="Seg_6674" s="T240">I was thinking: tomorrow I would follow this sledge road.</ta>
            <ta e="T255" id="Seg_6675" s="T249">It didn't took me long to pack in the morning on the twenty-third.</ta>
            <ta e="T263" id="Seg_6676" s="T255">It was easy to walk, the autumn sledge road got frozen.</ta>
            <ta e="T272" id="Seg_6677" s="T263">At 11 a.m. I was by my three hunting friends.</ta>
            <ta e="T276" id="Seg_6678" s="T272">They didn't manage to kill any squirrels.</ta>
            <ta e="T282" id="Seg_6679" s="T276">Having come to the hunting house I went hunting.</ta>
            <ta e="T294" id="Seg_6680" s="T282">That day I killed one squirrel, it was black, and one wood-grouse.</ta>
            <ta e="T297" id="Seg_6681" s="T294">It was Sunday.</ta>
            <ta e="T299" id="Seg_6682" s="T297">The second day.</ta>
            <ta e="T303" id="Seg_6683" s="T299">I went hunting and killed two squirrels.</ta>
            <ta e="T309" id="Seg_6684" s="T303">They had black skin and blue pads(?).</ta>
            <ta e="T312" id="Seg_6685" s="T309">My heart was not glad.</ta>
            <ta e="T317" id="Seg_6686" s="T312">The third day was good.</ta>
            <ta e="T321" id="Seg_6687" s="T317">I didn't manage to get anything that day.</ta>
            <ta e="T327" id="Seg_6688" s="T321">That day was my wife's birthday.</ta>
            <ta e="T336" id="Seg_6689" s="T327">That day my friends and I went together through a swamp to the Derevyanka river.</ta>
            <ta e="T343" id="Seg_6690" s="T336">There we wrote our names on a tree.</ta>
            <ta e="T350" id="Seg_6691" s="T343">And our names are Matvey, Maksim and Vasjka.</ta>
            <ta e="T359" id="Seg_6692" s="T350">That day the three of us brought father Semyon, who had passed away.</ta>
            <ta e="T364" id="Seg_6693" s="T359">We felt ourselves far from good.</ta>
            <ta e="T369" id="Seg_6694" s="T364">In the morning of the fourth day it was warm.</ta>
            <ta e="T372" id="Seg_6695" s="T369">In the evening it rained.</ta>
            <ta e="T375" id="Seg_6696" s="T372">The day was a hard one.</ta>
            <ta e="T378" id="Seg_6697" s="T375">The legs were tired from walking.</ta>
            <ta e="T383" id="Seg_6698" s="T378">That day I brought three squirrels.</ta>
            <ta e="T392" id="Seg_6699" s="T383">On the fifth day one friend, Vasilij, left home.</ta>
            <ta e="T397" id="Seg_6700" s="T392">That day I brought four squirrels.</ta>
            <ta e="T403" id="Seg_6701" s="T397">On the sixth day I went through a round swamp.</ta>
            <ta e="T411" id="Seg_6702" s="T403">I was walking silently, because I was listening, whether my dog barks.</ta>
            <ta e="T414" id="Seg_6703" s="T411">Its name is Edu.</ta>
            <ta e="T419" id="Seg_6704" s="T414">I heard my dog barking at a squirrel.</ta>
            <ta e="T427" id="Seg_6705" s="T419">I came to Edu: it was barking loudly at a big high cedar.</ta>
            <ta e="T431" id="Seg_6706" s="T427">I looked up, but saw nothing.</ta>
            <ta e="T444" id="Seg_6707" s="T431">In order to frighten the squirrel I shot from a rifle, then more than once from a double-barrel.</ta>
            <ta e="T448" id="Seg_6708" s="T444">I wanted to see it.</ta>
            <ta e="T453" id="Seg_6709" s="T448">I saw it on the top of the cedar.</ta>
            <ta e="T461" id="Seg_6710" s="T453">I shot more than three or four times in the direction of the top of the cedar, then I ceased [shooting].</ta>
            <ta e="T463" id="Seg_6711" s="T461">I stood smoking [for some time].</ta>
            <ta e="T467" id="Seg_6712" s="T463">I didn't want to leave the squirrel.</ta>
            <ta e="T479" id="Seg_6713" s="T467">I looked there: my taiga friend named Edu was doing something under the cedar, where I was shooting.</ta>
            <ta e="T483" id="Seg_6714" s="T479">I skied towards him.</ta>
            <ta e="T492" id="Seg_6715" s="T483">While I was standing and smoking, he ate the fallen squirrel.</ta>
            <ta e="T499" id="Seg_6716" s="T492">He left me back pads and the fluffy tail.</ta>
            <ta e="T505" id="Seg_6717" s="T499">I quarreled and scuffled with him, then we made peace.</ta>
            <ta e="T511" id="Seg_6718" s="T505">That day we brought two squirrels.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T24" id="Seg_6719" s="T1">Am 22. Oktober 1960 ging ich jagen zu einer Jagdhütte durch einen Sumpf.</ta>
            <ta e="T30" id="Seg_6720" s="T24">Der Name der Taiga ist Mitrij-Sidyryn-Land</ta>
            <ta e="T36" id="Seg_6721" s="T30">Es war früher Herbst.</ta>
            <ta e="T40" id="Seg_6722" s="T36">Meine Straße war gut.</ta>
            <ta e="T51" id="Seg_6723" s="T40">Es war angenehm, die Schlitten durch den Sumpf zu ziehen, [die Straße?] zum Reisen war gefroren.</ta>
            <ta e="T57" id="Seg_6724" s="T51">Und drei Freunde brachen vor mir auf.</ta>
            <ta e="T61" id="Seg_6725" s="T57">Ihre Namen waren Wasilij und Maxim.</ta>
            <ta e="T66" id="Seg_6726" s="T61">Ich habe den Namen von Maxims kleinem Bruder vergessen.</ta>
            <ta e="T70" id="Seg_6727" s="T66">Sie hinterließen eine Spur von Skiern.</ta>
            <ta e="T73" id="Seg_6728" s="T70">Ihre Ski-Straße war gefroren.</ta>
            <ta e="T79" id="Seg_6729" s="T73">An diesem Tag wollte ich so sehr ein Eichhörnchen treffen.</ta>
            <ta e="T81" id="Seg_6730" s="T79">Ich habe keines getroffen.</ta>
            <ta e="T86" id="Seg_6731" s="T81">Mein Hund bellte ein Eichhörnchen an.</ta>
            <ta e="T91" id="Seg_6732" s="T86">Ich sah eine Zeder an, aber ich sah es nicht.</ta>
            <ta e="T96" id="Seg_6733" s="T91">Mein Hund fand ein zweites Eichhörnchen auf einer Kiefer.</ta>
            <ta e="T98" id="Seg_6734" s="T96">Ich sah es.</ta>
            <ta e="T102" id="Seg_6735" s="T98">Ich wollte es nicht erschießen.</ta>
            <ta e="T106" id="Seg_6736" s="T102">Deswegen habe ich es nicht getötet.</ta>
            <ta e="T108" id="Seg_6737" s="T106">Es war rot.</ta>
            <ta e="T112" id="Seg_6738" s="T108">Wir schießen keine roten Eichhörnchen.</ta>
            <ta e="T124" id="Seg_6739" s="T112">Bevor ich das Land namens Mitrij Sidor und Lukantij erreichte, zerbrach mein aus Brettern gefertigter Schlitten.</ta>
            <ta e="T131" id="Seg_6740" s="T124">Der Schlitten wurde [mir] gegeben und gefunden von meiner Frau.</ta>
            <ta e="T135" id="Seg_6741" s="T131">Die Straße war nicht gut.</ta>
            <ta e="T141" id="Seg_6742" s="T135">Die Spur, die die Leute [vor mir] hinterlassen hatten, gefror nicht.</ta>
            <ta e="T143" id="Seg_6743" s="T141">Es wurde dunkel.</ta>
            <ta e="T145" id="Seg_6744" s="T143">Mein Schlitten ging kaputt.</ta>
            <ta e="T157" id="Seg_6745" s="T145">Ich trug den kaputten Schlitten und all meine Sachen auf dem Rücken zu einem Zedernhain am Rand des Sumpfes.</ta>
            <ta e="T168" id="Seg_6746" s="T157">Bei diesem Zedernhain entfachte ich ein Feuer, zerbrach Zedernäste und baute einen Platz zum Schlafen und Sitzen.</ta>
            <ta e="T171" id="Seg_6747" s="T168">Matvey steckte fest.</ta>
            <ta e="T178" id="Seg_6748" s="T171">Ich hatte nur einen Gedanken: wie repariere ich den kaputten Schlitten?</ta>
            <ta e="T182" id="Seg_6749" s="T178">Ich hatte Hasenschlingen.</ta>
            <ta e="T197" id="Seg_6750" s="T182">In der Nacht auf den 22. arbeitete ich hart, um meinen Schlitten zu reparieren(?).</ta>
            <ta e="T204" id="Seg_6751" s="T197">Es wäre hart, alle Dinge selbst zu tragen.</ta>
            <ta e="T209" id="Seg_6752" s="T204">Daher schlief ich in dieser Nacht nicht gut.</ta>
            <ta e="T214" id="Seg_6753" s="T209">Ich arbeitete am Schlitten bis Mitternacht.</ta>
            <ta e="T219" id="Seg_6754" s="T214">Und in dieser Nacht entschied mein Hund zu bellen.</ta>
            <ta e="T222" id="Seg_6755" s="T219">Sie kläffte zwei oder drei Male.</ta>
            <ta e="T225" id="Seg_6756" s="T222">Ich hatte keine Angst.</ta>
            <ta e="T233" id="Seg_6757" s="T225">Ich hielt mich als der Meister meines Feuers.</ta>
            <ta e="T240" id="Seg_6758" s="T233">Ich sah eine Zederninsel mitten im großen Sumpf.</ta>
            <ta e="T249" id="Seg_6759" s="T240">Ich dachte: Morgen werde ich diesem Schlittenweg folgen.</ta>
            <ta e="T255" id="Seg_6760" s="T249">Ich brauchte nicht lang, um am Morgen des 23. zu packen.</ta>
            <ta e="T263" id="Seg_6761" s="T255">Es war einfach zu laufen, der Herbstschlittenweg war gefroren.</ta>
            <ta e="T272" id="Seg_6762" s="T263">Um elf war ich bei meinen drei Jagdfreunden.</ta>
            <ta e="T276" id="Seg_6763" s="T272">Sie hatten es nicht geschafft, Eichhörnchen zu töten.</ta>
            <ta e="T282" id="Seg_6764" s="T276">Zur Jagdhütte gekommen ging ich jagen.</ta>
            <ta e="T294" id="Seg_6765" s="T282">An diesem Tag tötete ich ein Eichhörnchen, es war schwarz, und einen Auerhahn.</ta>
            <ta e="T297" id="Seg_6766" s="T294">Es war Sonntag.</ta>
            <ta e="T299" id="Seg_6767" s="T297">Der zweite Tag.</ta>
            <ta e="T303" id="Seg_6768" s="T299">Ich ging jagen und tötete zwei Eichhörnchen.</ta>
            <ta e="T309" id="Seg_6769" s="T303">Sie hatten schwarze Haut und blaue Ballen(?).</ta>
            <ta e="T312" id="Seg_6770" s="T309">Mein Herz war nicht froh.</ta>
            <ta e="T317" id="Seg_6771" s="T312">Der dritte Tag war gut.</ta>
            <ta e="T321" id="Seg_6772" s="T317">An diesem Tag erlegte ich nichts.</ta>
            <ta e="T327" id="Seg_6773" s="T321">Dieser Tag war der Geburtstag meiner Frau.</ta>
            <ta e="T336" id="Seg_6774" s="T327">An diesem Tag gingen meine Freunde und ich zusammen durch den Sumpf zum Derevyanka-Fluss.</ta>
            <ta e="T343" id="Seg_6775" s="T336">Dort ritzten wir unsere Namen in einen Baum.</ta>
            <ta e="T350" id="Seg_6776" s="T343">Und unsere Namen sind Matvey, Maxim und Vaska.</ta>
            <ta e="T359" id="Seg_6777" s="T350">An diesem Tag brachten wir drei Vater Semjon, der gestorben war.</ta>
            <ta e="T364" id="Seg_6778" s="T359">Wir fühlten uns alles andere als gut.</ta>
            <ta e="T369" id="Seg_6779" s="T364">Am Morgen des vierten Tages war es warm.</ta>
            <ta e="T372" id="Seg_6780" s="T369">Am Abend regnete es.</ta>
            <ta e="T375" id="Seg_6781" s="T372">Der Tag war hart.</ta>
            <ta e="T378" id="Seg_6782" s="T375">Die Beine waren müde vom Laufen.</ta>
            <ta e="T383" id="Seg_6783" s="T378">An diesem Tag brachte ich drei Eichhörnchen.</ta>
            <ta e="T392" id="Seg_6784" s="T383">Am fünften Tag ging ein Freund, Vasilij, nach Hause.</ta>
            <ta e="T397" id="Seg_6785" s="T392">An diesem Tag brachte ich vier Eichhörnchen.</ta>
            <ta e="T403" id="Seg_6786" s="T397">Am sechsten Tag ging ich durch einen runden Sumpf.</ta>
            <ta e="T411" id="Seg_6787" s="T403">Ich ging leise, weil ich horchte, ob mein Hund bellte.</ta>
            <ta e="T414" id="Seg_6788" s="T411">Sein Name ist Edu.</ta>
            <ta e="T419" id="Seg_6789" s="T414">Ich hörte meinen Hund ein Eichhörnchen anbellen.</ta>
            <ta e="T427" id="Seg_6790" s="T419">Ich erreichte Edu: er bellte eine große, hohe Zeder laut an.</ta>
            <ta e="T431" id="Seg_6791" s="T427">Ich schaute hinauf, sah aber nichts.</ta>
            <ta e="T444" id="Seg_6792" s="T431">Um dem Eichhörnchen Angst einzujagen, schoss ich mit einem Gewehr, dann mehr als einmal mit einem doppelläufigen (Gewehr).</ta>
            <ta e="T448" id="Seg_6793" s="T444">Ich wollte es sehen.</ta>
            <ta e="T453" id="Seg_6794" s="T448">Ich sah es auf der Spitze der Zeder.</ta>
            <ta e="T461" id="Seg_6795" s="T453">Ich schoss mehr als drei- oder viermal in Richtung der Zedernspitze, dann hörte ich auf [zu schießen]</ta>
            <ta e="T463" id="Seg_6796" s="T461">Ich stand rauchend [einige Zeit]</ta>
            <ta e="T467" id="Seg_6797" s="T463">Ich wollte das Eichhörnchen nicht zurücklassen.</ta>
            <ta e="T479" id="Seg_6798" s="T467">Ich schaute dorthin: mein Taigafreund namens Edu machte irgendetwas unter der Zeder, auf die ich schoss.</ta>
            <ta e="T483" id="Seg_6799" s="T479">Auf den Skiern lief ich zu ihm.</ta>
            <ta e="T492" id="Seg_6800" s="T483">Während ich stand und rauchte, fraß er das heruntergefallene Eichhörnchen.</ta>
            <ta e="T499" id="Seg_6801" s="T492">Er ließ mir die Hinterpfoten und den flauschigen Schwanz.</ta>
            <ta e="T505" id="Seg_6802" s="T499">Ich stritt und raufte mit ihm, dann schlossen wir Frieden.</ta>
            <ta e="T511" id="Seg_6803" s="T505">An diesem Tag brachten wir zwei Eichhörnchen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T24" id="Seg_6804" s="T1">Двадцать второго октября тысяча девятсот шестидесятого года я пошел на охоту через болото в охотничью избушку.</ta>
            <ta e="T30" id="Seg_6805" s="T24">Название тайги – Митрий Сидоровичев материк.</ta>
            <ta e="T36" id="Seg_6806" s="T30">Это было осенью, ранней осенью.</ta>
            <ta e="T40" id="Seg_6807" s="T36">Моя дорога была хорошей.</ta>
            <ta e="T51" id="Seg_6808" s="T40">Потому было хорошо [ехать?] по нартах через болото, [дорога?], по которой надо было идти, была замерзшей.</ta>
            <ta e="T57" id="Seg_6809" s="T51">И три товарища ушли до меня.</ta>
            <ta e="T61" id="Seg_6810" s="T57">Их зовут Василий, Максим.</ta>
            <ta e="T66" id="Seg_6811" s="T61">Имя братишки Максима я забыл.</ta>
            <ta e="T70" id="Seg_6812" s="T66">Они прошли на лыжах.</ta>
            <ta e="T73" id="Seg_6813" s="T70">Их лыжная дорога подстыла.</ta>
            <ta e="T79" id="Seg_6814" s="T73">В этот день как мне хотелось встретиться с белочкой.</ta>
            <ta e="T81" id="Seg_6815" s="T79">Не встретился.</ta>
            <ta e="T86" id="Seg_6816" s="T81">Одну белку моя собака облаяла.</ta>
            <ta e="T91" id="Seg_6817" s="T86">Я смотрел на кедр, ее не увидел.</ta>
            <ta e="T96" id="Seg_6818" s="T91">Моя собака нашла вторую белку на сосне.</ta>
            <ta e="T98" id="Seg_6819" s="T96">Я увидел ее.</ta>
            <ta e="T102" id="Seg_6820" s="T98">Не хотел в нее стрелять.</ta>
            <ta e="T106" id="Seg_6821" s="T102">Поэтому я ее не убил.</ta>
            <ta e="T108" id="Seg_6822" s="T106">Она красная была.</ta>
            <ta e="T112" id="Seg_6823" s="T108">Красную белку мы не стреляем.</ta>
            <ta e="T124" id="Seg_6824" s="T112">Когда я еще не дошел до местности по названию Митрий Сидоровича и Лукантьев, сломалась моя нарта из досок избы.</ta>
            <ta e="T131" id="Seg_6825" s="T124">Эта моя нарта была дана и найдена моей женой.</ta>
            <ta e="T135" id="Seg_6826" s="T131">Дорога была неважная.</ta>
            <ta e="T141" id="Seg_6827" s="T135">(След от нарт)? прошедших людей не подмерз.</ta>
            <ta e="T143" id="Seg_6828" s="T141">Настал вечер.</ta>
            <ta e="T145" id="Seg_6829" s="T143">Мои нарты сломались.</ta>
            <ta e="T157" id="Seg_6830" s="T145">Вещи и сломанные нарты я унес на спине на кедровый мыс, вдающийся в болото.</ta>
            <ta e="T168" id="Seg_6831" s="T157">На кедровом мысу разложил костер, наломал кедровые ветки, сделал место, чтобы сидеть и спать.</ta>
            <ta e="T171" id="Seg_6832" s="T168">Матвей крепко остановился (застрял?).</ta>
            <ta e="T178" id="Seg_6833" s="T171">У меня одна мысль: как отремонтировать сломанные нарты?</ta>
            <ta e="T182" id="Seg_6834" s="T178">У меня были заячьи петли.</ta>
            <ta e="T197" id="Seg_6835" s="T182">Ночью с двадцать второго на двадцать третье я работал, как следует чинил(?) свои нарты.</ta>
            <ta e="T204" id="Seg_6836" s="T197">Нести вещи на себе мне будет тяжело.</ta>
            <ta e="T209" id="Seg_6837" s="T204">Поэтому ночью я хорошо не спал.</ta>
            <ta e="T214" id="Seg_6838" s="T209">Нарты я делал до полуночи.</ta>
            <ta e="T219" id="Seg_6839" s="T214">Однажды ночью моя собака вздумала залаять.</ta>
            <ta e="T222" id="Seg_6840" s="T219">Два-три раза тявкнула.</ta>
            <ta e="T225" id="Seg_6841" s="T222">Я не боялся.</ta>
            <ta e="T233" id="Seg_6842" s="T225">Я сам себя держал как хозяин своего костра.</ta>
            <ta e="T240" id="Seg_6843" s="T233">Я посмотрел на кедровый остров в большом болоте.</ta>
            <ta e="T249" id="Seg_6844" s="T240">Я думал: завтра вперед пойду по этой нартной дороге (по нартнице).</ta>
            <ta e="T255" id="Seg_6845" s="T249">Двадцать третьего утром я долго не собирался.</ta>
            <ta e="T263" id="Seg_6846" s="T255">Идти было хорошо, на осенней нартной дороге подморозило.</ta>
            <ta e="T272" id="Seg_6847" s="T263">В одиннадцать часов дня я был у трех друзей-охотников.</ta>
            <ta e="T276" id="Seg_6848" s="T272">Они белок не добывали.</ta>
            <ta e="T282" id="Seg_6849" s="T276">После того как я пришел в охотничью избушку, я пошел на охоту.</ta>
            <ta e="T294" id="Seg_6850" s="T282">В этот день я добыл одну белку, она черная была, и одного глухаря.</ta>
            <ta e="T297" id="Seg_6851" s="T294">Был воскресный день.</ta>
            <ta e="T299" id="Seg_6852" s="T297">Второй день.</ta>
            <ta e="T303" id="Seg_6853" s="T299">Я охотился, убил двух белок.</ta>
            <ta e="T309" id="Seg_6854" s="T303">Это были синеручки с черной шкурой.</ta>
            <ta e="T312" id="Seg_6855" s="T309">Мое сердце не радовалось.</ta>
            <ta e="T317" id="Seg_6856" s="T312">Третий день был хороший день.</ta>
            <ta e="T321" id="Seg_6857" s="T317">В этот день я никого не добыл.</ta>
            <ta e="T327" id="Seg_6858" s="T321">В этот день был день рождения моей жены.</ta>
            <ta e="T336" id="Seg_6859" s="T327">В этот день мы с друзьями вместе ходили через болото на «Деревянку» (речку).</ta>
            <ta e="T343" id="Seg_6860" s="T336">Там мы на дереве мы написали свои имена.</ta>
            <ta e="T350" id="Seg_6861" s="T343">А наши имена Матвей, Максим и Василий.</ta>
            <ta e="T359" id="Seg_6862" s="T350">В этот день мы принесли втроем мертвого отца Семена.</ta>
            <ta e="T364" id="Seg_6863" s="T359">Нам было очень плохо.</ta>
            <ta e="T369" id="Seg_6864" s="T364">На четвертый день утром тепло было.</ta>
            <ta e="T372" id="Seg_6865" s="T369">Вечером был дождь.</ta>
            <ta e="T375" id="Seg_6866" s="T372">День был тяжелый.</ta>
            <ta e="T378" id="Seg_6867" s="T375">Ноги устали ходить.</ta>
            <ta e="T383" id="Seg_6868" s="T378">В этот день я принес трех белок.</ta>
            <ta e="T392" id="Seg_6869" s="T383">На пятый день один друг, его имя Василий, ушел домой.</ta>
            <ta e="T397" id="Seg_6870" s="T392">В этот день я принес четыре белки.</ta>
            <ta e="T403" id="Seg_6871" s="T397">На шестой день я ходил через круглое болото.</ta>
            <ta e="T411" id="Seg_6872" s="T403">Я шел тихо, потому что прислушивался, когда залает моя собака.</ta>
            <ta e="T414" id="Seg_6873" s="T411">Звать ее Эду.</ta>
            <ta e="T419" id="Seg_6874" s="T414">Я услышал, как моя собака лает на белку.</ta>
            <ta e="T427" id="Seg_6875" s="T419">Подходя к Эду: она хорошо лает на большой высокий кедр.</ta>
            <ta e="T431" id="Seg_6876" s="T427">Смотрю вверх, не могу увидеть.</ta>
            <ta e="T444" id="Seg_6877" s="T431">Чтобы спугнуть белку, я много стрелял из тозовки, а потом не раз стрелял из двухстволки.</ta>
            <ta e="T448" id="Seg_6878" s="T444">Я хотел ее увидеть.</ta>
            <ta e="T453" id="Seg_6879" s="T448">Я ее увидел на вершине кедра.</ta>
            <ta e="T461" id="Seg_6880" s="T453">Не три и не четыре раза я стрелял в вершину кедра, перестал (стрелять).</ta>
            <ta e="T463" id="Seg_6881" s="T461">Покурил, постоял.</ta>
            <ta e="T467" id="Seg_6882" s="T463">Мне не хотелось оставлять белку.</ta>
            <ta e="T479" id="Seg_6883" s="T467">Я взглянул: мой таежный товарищ по имени Эду что-то делает под кедром, в который я стрелял.</ta>
            <ta e="T483" id="Seg_6884" s="T479">Я на лыжах подкатился к нему.</ta>
            <ta e="T492" id="Seg_6885" s="T483">Пока я стоял-курил, он съел упавшую белку.</ta>
            <ta e="T499" id="Seg_6886" s="T492">Оставил мне задние лапки и пушистый хвост.</ta>
            <ta e="T505" id="Seg_6887" s="T499">Мы с ним поругались, подрались, опять помирились.</ta>
            <ta e="T511" id="Seg_6888" s="T505">В этот день мы принесли две белки.</ta>
         </annotation>
         <annotation name="ltr" tierref="TIE0">
            <ta e="T24" id="Seg_6889" s="T1">22-го числа октября тысяча девятсот шестидесятого года я пошел на охоту через болото в охот. избушку.</ta>
            <ta e="T30" id="Seg_6890" s="T24">Название тайги Митрий Сидоровичев материк.</ta>
            <ta e="T36" id="Seg_6891" s="T30">Это было осенью, ранней осенью.</ta>
            <ta e="T40" id="Seg_6892" s="T36">Моя дорога (путь) был хорошим.</ta>
            <ta e="T51" id="Seg_6893" s="T40">Поэтому было хорошо по нартнице через болото куда надо было идти была стылой.</ta>
            <ta e="T57" id="Seg_6894" s="T51">Три товарища ушли передо мной.</ta>
            <ta e="T61" id="Seg_6895" s="T57">Их зовут Василий, Максим.</ta>
            <ta e="T66" id="Seg_6896" s="T61">Имя братишки Максима я забыл.</ta>
            <ta e="T70" id="Seg_6897" s="T66">Они прошли на лыжах.</ta>
            <ta e="T73" id="Seg_6898" s="T70">Их лыжница (лыжная дорога) подстыла.</ta>
            <ta e="T79" id="Seg_6899" s="T73">В этот день как хотелось встретиться с белочкой.</ta>
            <ta e="T81" id="Seg_6900" s="T79">Не встретился.</ta>
            <ta e="T86" id="Seg_6901" s="T81">Одну белку моя собака облаяла.</ta>
            <ta e="T91" id="Seg_6902" s="T86">Я смотрел на кедре, ее не увидел.</ta>
            <ta e="T96" id="Seg_6903" s="T91">Моя собака нашла вторую белку на сосне.</ta>
            <ta e="T98" id="Seg_6904" s="T96">Я увидел ее.</ta>
            <ta e="T102" id="Seg_6905" s="T98">Не хотел ее стрелять.</ta>
            <ta e="T106" id="Seg_6906" s="T102">Поэтому я ее не убил.</ta>
            <ta e="T108" id="Seg_6907" s="T106">Она красная была.</ta>
            <ta e="T112" id="Seg_6908" s="T108">Красную белку мы не стреляем.</ta>
            <ta e="T124" id="Seg_6909" s="T112">Не доходя местности по названию Митрий Сидоровича и Лукантьев из досок избы сломалась нарта</ta>
            <ta e="T131" id="Seg_6910" s="T124">Эта нарта была данная и найдена моей женой</ta>
            <ta e="T135" id="Seg_6911" s="T131">Дорога была неважная.</ta>
            <ta e="T141" id="Seg_6912" s="T135">ушедших людей сзади их нартница не была стылой</ta>
            <ta e="T143" id="Seg_6913" s="T141">Вечер стал.</ta>
            <ta e="T145" id="Seg_6914" s="T143">Моя нарта сломалась.</ta>
            <ta e="T157" id="Seg_6915" s="T145">вещи изломанную нарту и вещи на спине унес на кедровый мыс вдающийся в болото</ta>
            <ta e="T168" id="Seg_6916" s="T157">на кедровом мысу разложил костер, наломал кедровые ветки, сделал чтобы сесть лечь место</ta>
            <ta e="T171" id="Seg_6917" s="T168">Матвей крепко остановился</ta>
            <ta e="T178" id="Seg_6918" s="T171">у меня одна дума как направить (отремонтировать) сломанную нарту</ta>
            <ta e="T182" id="Seg_6919" s="T178">У меня были заячьи петли.</ta>
            <ta e="T197" id="Seg_6920" s="T182">с двадцать второго на двадцать третье в ночь я работал крепко направил свою нарту</ta>
            <ta e="T204" id="Seg_6921" s="T197">на себе нести вещи мне тяжело будет</ta>
            <ta e="T209" id="Seg_6922" s="T204">Поэтому ночью не спал хорошо.</ta>
            <ta e="T214" id="Seg_6923" s="T209">Нарту я делал до полночи.</ta>
            <ta e="T219" id="Seg_6924" s="T214">Однажды ночью моя собака лаять вздумала.</ta>
            <ta e="T222" id="Seg_6925" s="T219">два три раза тявкнула</ta>
            <ta e="T225" id="Seg_6926" s="T222">Я не боялся.</ta>
            <ta e="T233" id="Seg_6927" s="T225">Я сам себя держал как хозяин своего огнища (костра)</ta>
            <ta e="T240" id="Seg_6928" s="T233">я посмотрел на кедровый остров в большом болоте</ta>
            <ta e="T249" id="Seg_6929" s="T240">Я думал: завтра вперед пойду по этой нартной дороге (по нартнице)</ta>
            <ta e="T255" id="Seg_6930" s="T249">двадцать третьего утром долго не собирался</ta>
            <ta e="T263" id="Seg_6931" s="T255">идти было хорошо осенней нартной дорогой которая застыла (была стылая)</ta>
            <ta e="T272" id="Seg_6932" s="T263">в одиннадцать часов дня я был у трех охотников товарищей</ta>
            <ta e="T276" id="Seg_6933" s="T272">они белок не добывали</ta>
            <ta e="T282" id="Seg_6934" s="T276">После того как пришел в охот. избушку, я пошел на охоту.</ta>
            <ta e="T294" id="Seg_6935" s="T282">В этот день я добыл одну белку, эта она черная была, и одного глухаря</ta>
            <ta e="T297" id="Seg_6936" s="T294">был воскресный день</ta>
            <ta e="T299" id="Seg_6937" s="T297">второй день</ta>
            <ta e="T303" id="Seg_6938" s="T299">охотился убил двух белок</ta>
            <ta e="T309" id="Seg_6939" s="T303">они (эти белки) были синеручки с черной шкурой</ta>
            <ta e="T312" id="Seg_6940" s="T309">Мое сердце не радует.</ta>
            <ta e="T317" id="Seg_6941" s="T312">третий день был хороший день</ta>
            <ta e="T321" id="Seg_6942" s="T317">В этот день я никого не добыл (убил).</ta>
            <ta e="T327" id="Seg_6943" s="T321">Этот день был день рождения (родивший) моей жены.</ta>
            <ta e="T336" id="Seg_6944" s="T327">в этот день с товарищами вместе ходили через болото в «Деревянку» (речку)</ta>
            <ta e="T343" id="Seg_6945" s="T336">Там мы на дерево написали свои имена.</ta>
            <ta e="T350" id="Seg_6946" s="T343">А наши имена Матвей, Максим и Василий.</ta>
            <ta e="T359" id="Seg_6947" s="T350">в этот день мы принесли втроем мертвого отца Семена</ta>
            <ta e="T364" id="Seg_6948" s="T359">нам было очень плохо.</ta>
            <ta e="T369" id="Seg_6949" s="T364">На четвертый день утром тепло было.</ta>
            <ta e="T372" id="Seg_6950" s="T369">Вечером был дождь.</ta>
            <ta e="T375" id="Seg_6951" s="T372">День тяжелый был.</ta>
            <ta e="T378" id="Seg_6952" s="T375">ходить ноги устали</ta>
            <ta e="T383" id="Seg_6953" s="T378">В этот день я принес трех белок.</ta>
            <ta e="T392" id="Seg_6954" s="T383">На пятый день один товарищ, его имя Василий, ушел домой.</ta>
            <ta e="T397" id="Seg_6955" s="T392">В этот день я принес четыре белки.</ta>
            <ta e="T403" id="Seg_6956" s="T397">На шестой день ходил я через круглое болото.</ta>
            <ta e="T411" id="Seg_6957" s="T403">тихо шел потому что прислушиваясь когда залает моя собака</ta>
            <ta e="T414" id="Seg_6958" s="T411">звать ее Эду</ta>
            <ta e="T419" id="Seg_6959" s="T414">услышав моя собака лает на белку</ta>
            <ta e="T427" id="Seg_6960" s="T419">подходя (не дойдя) к Эду (собаке) он хорошо лает на большую высокую кедру</ta>
            <ta e="T431" id="Seg_6961" s="T427">смотрю вверх не могу увидеть</ta>
            <ta e="T444" id="Seg_6962" s="T431">белку спугнуть много стрелял из тозовки а потом не раз стрелял с двухстволки</ta>
            <ta e="T448" id="Seg_6963" s="T444">хотел я ее увидеть</ta>
            <ta e="T453" id="Seg_6964" s="T448">я ее увидел на вершине кедры</ta>
            <ta e="T461" id="Seg_6965" s="T453">не три четыре раза в вершину кедры стрелял оставил (перестал стрелять)</ta>
            <ta e="T463" id="Seg_6966" s="T461">покурил постоял</ta>
            <ta e="T467" id="Seg_6967" s="T463">не хотел оставить белку</ta>
            <ta e="T479" id="Seg_6968" s="T467">взглянул мой таежный товарищ по имени Эду что-то делает около стрелянный комля кедры (около комля кедры, в которую я стрелял)</ta>
            <ta e="T483" id="Seg_6969" s="T479">я на лыжах подкатился к нему</ta>
            <ta e="T492" id="Seg_6970" s="T483">он упавшую белку когда я стоял курил он съел</ta>
            <ta e="T499" id="Seg_6971" s="T492">оставил мне задние ножки (лапки) пушистый хвост</ta>
            <ta e="T505" id="Seg_6972" s="T499">Мы с ним поругались подрались опять помирились.</ta>
            <ta e="T511" id="Seg_6973" s="T505">в этот день мы принесли две белки</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T102" id="Seg_6974" s="T98">WNB: the verb 'want' is kɨkkɨgu. It is not clear, why KMS uses the verb 'be'.</ta>
            <ta e="T131" id="Seg_6975" s="T124">WNB: I am not sure that it is really allative. </ta>
            <ta e="T359" id="Seg_6976" s="T350">[WNB:] the translation is not clear</ta>
            <ta e="T414" id="Seg_6977" s="T411">[WNB:] eːdu mens 'village'.</ta>
            <ta e="T444" id="Seg_6978" s="T431">[WNB:] tozovka is a small-caliber rifle of the Tula arms factory</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
