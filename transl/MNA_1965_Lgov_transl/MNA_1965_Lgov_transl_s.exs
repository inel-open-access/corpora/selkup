<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>MNA_1965_Lgov_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">MNA_1965_Lgov_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">183</ud-information>
            <ud-information attribute-name="# HIAT:w">138</ud-information>
            <ud-information attribute-name="# e">138</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MNA">
            <abbreviation>MNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T139" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Lʼgov</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Qənnɛjlimɨt</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <nts id="Seg_13" n="HIAT:ip">/</nts>
                  <ts e="T3" id="Seg_15" n="HIAT:w" s="T2">qəllimɨn</ts>
                  <nts id="Seg_16" n="HIAT:ip">)</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_19" n="HIAT:w" s="T3">Lʼgovtɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip">,</nts>
                  <nts id="Seg_21" n="HIAT:ip">—</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_24" n="HIAT:w" s="T4">kətɨsɨt</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_27" n="HIAT:w" s="T5">mäkkʼä</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">ukkɨr</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">pɔːr</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">ukkur</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">qup</ts>
                  <nts id="Seg_40" n="HIAT:ip">,</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">täpɨp</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">qärɨsɔːtɨt</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Jermolaj</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_53" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">Meː</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">nɨmtɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">šiːpap</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">čʼatälʼtɛntɔːm</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kočʼik</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_71" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">Soma</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">suːrɨčʼčʼilʼ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">qumɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">čʼɔːtɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">mačʼoːqɨlʼ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">šiːpa</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">aša</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">orɨlʼlʼä</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">qəttɨtɨ</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">no</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">alpäqɨt</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">jarɨk</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">suːrɨp</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">ni</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">kut</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">čʼäːŋka</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_123" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">Mat</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">üŋɨltɨsak</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">mat</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">surɨčʼčʼilʼ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">qummɨ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">i</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">qəssi</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">Lʼgovtɨ</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_150" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">Lʼgov</ts>
                  <nts id="Seg_153" n="HIAT:ip">—</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">aša</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">wärqə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">qäːttɨ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">äsɨčʼerälʼ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">pülʼ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">nuːlʼ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">mɔːssɨmpɨlʼ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T50">aj</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">šitɨ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">mʼelʼnicatɨ</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">lɨmpälʼ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_189" n="HIAT:w" s="T54">kɨkʼäqɨt</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_192" n="HIAT:w" s="T55">Rosota</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_196" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_198" n="HIAT:w" s="T56">Na</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_201" n="HIAT:w" s="T57">kɨkʼälʼa</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_204" n="HIAT:w" s="T58">sompala</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_207" n="HIAT:w" s="T59">kilometašalʼ</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_210" n="HIAT:w" s="T60">mɨqɨt</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_213" n="HIAT:w" s="T61">Lʼgov</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_216" n="HIAT:w" s="T62">nɔːnɨ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">ɛsɨmpa</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_222" n="HIAT:w" s="T64">tɔːntɨ</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_225" n="HIAT:w" s="T65">lɨmpälʼ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_227" n="HIAT:ip">(</nts>
                  <ts e="T67" id="Seg_229" n="HIAT:w" s="T66">toːqɨt</ts>
                  <nts id="Seg_230" n="HIAT:ip">)</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_233" n="HIAT:w" s="T67">kɨkʼätqo</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_237" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_239" n="HIAT:w" s="T68">Qanɨqqɨntɨ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_242" n="HIAT:w" s="T69">aj</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">kuttoːqɨn</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">čʼontoːqɨntɨ</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">sočʼimpa</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_254" n="HIAT:w" s="T73">kočʼi</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_257" n="HIAT:w" s="T74">nʼuːtɨsä</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_260" n="HIAT:w" s="T75">i</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_263" n="HIAT:w" s="T76">lotɨksä</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_265" n="HIAT:ip">(</nts>
                  <nts id="Seg_266" n="HIAT:ip">/</nts>
                  <ts e="T78" id="Seg_268" n="HIAT:w" s="T77">lotɨssä</ts>
                  <nts id="Seg_269" n="HIAT:ip">)</nts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_273" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_275" n="HIAT:w" s="T78">Vot</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_278" n="HIAT:w" s="T79">na</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_281" n="HIAT:w" s="T80">kɨkʼäqɨt</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_284" n="HIAT:w" s="T81">mɛrkɨkɨtɨlʼ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_287" n="HIAT:w" s="T82">aj</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_290" n="HIAT:w" s="T83">küŋɨkɨtɨlʼ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">mɨqɨt</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_296" n="HIAT:w" s="T85">nʼuːtɨt</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_299" n="HIAT:w" s="T86">aj</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_302" n="HIAT:w" s="T87">lotɨn</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_305" n="HIAT:w" s="T88">puːtoːqɨt</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_308" n="HIAT:w" s="T89">orɨmka</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_311" n="HIAT:w" s="T90">aj</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">ilɨkka</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_317" n="HIAT:w" s="T92">orsa</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_320" n="HIAT:w" s="T93">kočʼi</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_323" n="HIAT:w" s="T94">kunmɔːntɨlʼ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_326" n="HIAT:w" s="T95">poːrätɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_329" n="HIAT:w" s="T96">kurassɨmpɨlʼ</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_332" n="HIAT:w" s="T97">šiːpat</ts>
                  <nts id="Seg_333" n="HIAT:ip">:</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_336" n="HIAT:w" s="T98">paqkät</ts>
                  <nts id="Seg_337" n="HIAT:ip">,</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_340" n="HIAT:w" s="T99">alqɨt</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_344" n="HIAT:w" s="T100">mɔːčʼipɨt</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_348" n="HIAT:w" s="T101">ɛrjat</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_352" n="HIAT:w" s="T102">čʼünkočʼit</ts>
                  <nts id="Seg_353" n="HIAT:ip">,</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_356" n="HIAT:w" s="T103">poːčʼiqot</ts>
                  <nts id="Seg_357" n="HIAT:ip">,</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_360" n="HIAT:w" s="T104">čʼarkočʼit</ts>
                  <nts id="Seg_361" n="HIAT:ip">,</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_364" n="HIAT:w" s="T105">üt</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_367" n="HIAT:w" s="T106">šiːpat</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_371" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_373" n="HIAT:w" s="T107">Kɨpa</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_376" n="HIAT:w" s="T108">šiːpalʼ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_379" n="HIAT:w" s="T109">kärɨt</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_382" n="HIAT:w" s="T110">šit</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_385" n="HIAT:w" s="T111">nennɨ</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_388" n="HIAT:w" s="T112">tılʼtaıːmpɔːtɨt</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_390" n="HIAT:ip">(</nts>
                  <nts id="Seg_391" n="HIAT:ip">/</nts>
                  <ts e="T114" id="Seg_393" n="HIAT:w" s="T113">tılʼtälʼıːmpɔːtɨn</ts>
                  <nts id="Seg_394" n="HIAT:ip">/</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_397" n="HIAT:w" s="T114">tılʼtälʼɛːmpɔːt</ts>
                  <nts id="Seg_398" n="HIAT:ip">)</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_401" n="HIAT:w" s="T115">aj</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_404" n="HIAT:w" s="T116">ütə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_407" n="HIAT:w" s="T117">pɔːrɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_410" n="HIAT:w" s="T118">tıːmpɨsɔːtɨt</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_414" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_416" n="HIAT:w" s="T119">Puškanɨt</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_419" n="HIAT:w" s="T120">passɛːtäkkɨt</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_423" n="HIAT:w" s="T121">wačʼičʼikkɔːtɨt</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_425" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_427" n="HIAT:w" s="T122">mäčʼičʼikkɔːtɨt</ts>
                  <nts id="Seg_428" n="HIAT:ip">)</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_431" n="HIAT:w" s="T123">na</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_434" n="HIAT:w" s="T124">mantɨ</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_437" n="HIAT:w" s="T125">kočʼi</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_440" n="HIAT:w" s="T126">šiːpalʼ</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_443" n="HIAT:w" s="T127">kärɨt</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_447" n="HIAT:w" s="T129">suːručʼčʼilʼ</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_450" n="HIAT:w" s="T130">qup</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_453" n="HIAT:w" s="T131">ukkur</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_456" n="HIAT:w" s="T132">utɨntɨsä</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_459" n="HIAT:w" s="T133">orakkɨtɨ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_462" n="HIAT:w" s="T134">ükomtɨ</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_464" n="HIAT:ip">(</nts>
                  <nts id="Seg_465" n="HIAT:ip">/</nts>
                  <ts e="T136" id="Seg_467" n="HIAT:w" s="T135">olomtɨ</ts>
                  <nts id="Seg_468" n="HIAT:ip">)</nts>
                  <nts id="Seg_469" n="HIAT:ip">,</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_472" n="HIAT:w" s="T136">nɨːnɨ</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_475" n="HIAT:w" s="T137">kətɨkkɨt</ts>
                  <nts id="Seg_476" n="HIAT:ip">:</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_479" n="HIAT:w" s="T138">Fuːuːuː</ts>
                  <nts id="Seg_480" n="HIAT:ip">!</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T139" id="Seg_482" n="sc" s="T0">
               <ts e="T1" id="Seg_484" n="e" s="T0">Lʼgov. </ts>
               <ts e="T2" id="Seg_486" n="e" s="T1">Qənnɛjlimɨt </ts>
               <ts e="T3" id="Seg_488" n="e" s="T2">(/qəllimɨn) </ts>
               <ts e="T4" id="Seg_490" n="e" s="T3">Lʼgovtɨ,— </ts>
               <ts e="T5" id="Seg_492" n="e" s="T4">kətɨsɨt </ts>
               <ts e="T6" id="Seg_494" n="e" s="T5">mäkkʼä </ts>
               <ts e="T7" id="Seg_496" n="e" s="T6">ukkɨr </ts>
               <ts e="T8" id="Seg_498" n="e" s="T7">pɔːr </ts>
               <ts e="T9" id="Seg_500" n="e" s="T8">ukkur </ts>
               <ts e="T10" id="Seg_502" n="e" s="T9">qup, </ts>
               <ts e="T11" id="Seg_504" n="e" s="T10">täpɨp </ts>
               <ts e="T12" id="Seg_506" n="e" s="T11">qärɨsɔːtɨt </ts>
               <ts e="T13" id="Seg_508" n="e" s="T12">Jermolaj. </ts>
               <ts e="T14" id="Seg_510" n="e" s="T13">Meː </ts>
               <ts e="T15" id="Seg_512" n="e" s="T14">nɨmtɨ </ts>
               <ts e="T16" id="Seg_514" n="e" s="T15">šiːpap </ts>
               <ts e="T17" id="Seg_516" n="e" s="T16">čʼatälʼtɛntɔːm </ts>
               <ts e="T18" id="Seg_518" n="e" s="T17">kočʼik. </ts>
               <ts e="T19" id="Seg_520" n="e" s="T18">Soma </ts>
               <ts e="T20" id="Seg_522" n="e" s="T19">suːrɨčʼčʼilʼ </ts>
               <ts e="T21" id="Seg_524" n="e" s="T20">qumɨt </ts>
               <ts e="T22" id="Seg_526" n="e" s="T21">čʼɔːtɨ </ts>
               <ts e="T23" id="Seg_528" n="e" s="T22">mačʼoːqɨlʼ </ts>
               <ts e="T24" id="Seg_530" n="e" s="T23">šiːpa </ts>
               <ts e="T25" id="Seg_532" n="e" s="T24">aša </ts>
               <ts e="T26" id="Seg_534" n="e" s="T25">orɨlʼlʼä </ts>
               <ts e="T27" id="Seg_536" n="e" s="T26">qəttɨtɨ, </ts>
               <ts e="T28" id="Seg_538" n="e" s="T27">no </ts>
               <ts e="T29" id="Seg_540" n="e" s="T28">alpäqɨt </ts>
               <ts e="T30" id="Seg_542" n="e" s="T29">jarɨk </ts>
               <ts e="T31" id="Seg_544" n="e" s="T30">suːrɨp </ts>
               <ts e="T32" id="Seg_546" n="e" s="T31">ni </ts>
               <ts e="T33" id="Seg_548" n="e" s="T32">kut </ts>
               <ts e="T34" id="Seg_550" n="e" s="T33">čʼäːŋka. </ts>
               <ts e="T35" id="Seg_552" n="e" s="T34">Mat </ts>
               <ts e="T36" id="Seg_554" n="e" s="T35">üŋɨltɨsak </ts>
               <ts e="T37" id="Seg_556" n="e" s="T36">mat </ts>
               <ts e="T38" id="Seg_558" n="e" s="T37">surɨčʼčʼilʼ </ts>
               <ts e="T39" id="Seg_560" n="e" s="T38">qummɨ </ts>
               <ts e="T40" id="Seg_562" n="e" s="T39">i </ts>
               <ts e="T41" id="Seg_564" n="e" s="T40">qəssi </ts>
               <ts e="T42" id="Seg_566" n="e" s="T41">Lʼgovtɨ. </ts>
               <ts e="T43" id="Seg_568" n="e" s="T42">Lʼgov— </ts>
               <ts e="T44" id="Seg_570" n="e" s="T43">aša </ts>
               <ts e="T45" id="Seg_572" n="e" s="T44">wärqə </ts>
               <ts e="T46" id="Seg_574" n="e" s="T45">qäːttɨ </ts>
               <ts e="T47" id="Seg_576" n="e" s="T46">äsɨčʼerälʼ </ts>
               <ts e="T48" id="Seg_578" n="e" s="T47">pülʼ </ts>
               <ts e="T49" id="Seg_580" n="e" s="T48">nuːlʼ </ts>
               <ts e="T50" id="Seg_582" n="e" s="T49">mɔːssɨmpɨlʼ </ts>
               <ts e="T51" id="Seg_584" n="e" s="T50">aj </ts>
               <ts e="T52" id="Seg_586" n="e" s="T51">šitɨ </ts>
               <ts e="T53" id="Seg_588" n="e" s="T52">mʼelʼnicatɨ </ts>
               <ts e="T54" id="Seg_590" n="e" s="T53">lɨmpälʼ </ts>
               <ts e="T55" id="Seg_592" n="e" s="T54">kɨkʼäqɨt </ts>
               <ts e="T56" id="Seg_594" n="e" s="T55">Rosota. </ts>
               <ts e="T57" id="Seg_596" n="e" s="T56">Na </ts>
               <ts e="T58" id="Seg_598" n="e" s="T57">kɨkʼälʼa </ts>
               <ts e="T59" id="Seg_600" n="e" s="T58">sompala </ts>
               <ts e="T60" id="Seg_602" n="e" s="T59">kilometašalʼ </ts>
               <ts e="T61" id="Seg_604" n="e" s="T60">mɨqɨt </ts>
               <ts e="T62" id="Seg_606" n="e" s="T61">Lʼgov </ts>
               <ts e="T63" id="Seg_608" n="e" s="T62">nɔːnɨ </ts>
               <ts e="T64" id="Seg_610" n="e" s="T63">ɛsɨmpa </ts>
               <ts e="T65" id="Seg_612" n="e" s="T64">tɔːntɨ </ts>
               <ts e="T66" id="Seg_614" n="e" s="T65">lɨmpälʼ </ts>
               <ts e="T67" id="Seg_616" n="e" s="T66">(toːqɨt) </ts>
               <ts e="T68" id="Seg_618" n="e" s="T67">kɨkʼätqo. </ts>
               <ts e="T69" id="Seg_620" n="e" s="T68">Qanɨqqɨntɨ </ts>
               <ts e="T70" id="Seg_622" n="e" s="T69">aj </ts>
               <ts e="T71" id="Seg_624" n="e" s="T70">kuttoːqɨn </ts>
               <ts e="T72" id="Seg_626" n="e" s="T71">čʼontoːqɨntɨ </ts>
               <ts e="T73" id="Seg_628" n="e" s="T72">sočʼimpa </ts>
               <ts e="T74" id="Seg_630" n="e" s="T73">kočʼi </ts>
               <ts e="T75" id="Seg_632" n="e" s="T74">nʼuːtɨsä </ts>
               <ts e="T76" id="Seg_634" n="e" s="T75">i </ts>
               <ts e="T77" id="Seg_636" n="e" s="T76">lotɨksä </ts>
               <ts e="T78" id="Seg_638" n="e" s="T77">(/lotɨssä). </ts>
               <ts e="T79" id="Seg_640" n="e" s="T78">Vot </ts>
               <ts e="T80" id="Seg_642" n="e" s="T79">na </ts>
               <ts e="T81" id="Seg_644" n="e" s="T80">kɨkʼäqɨt </ts>
               <ts e="T82" id="Seg_646" n="e" s="T81">mɛrkɨkɨtɨlʼ </ts>
               <ts e="T83" id="Seg_648" n="e" s="T82">aj </ts>
               <ts e="T84" id="Seg_650" n="e" s="T83">küŋɨkɨtɨlʼ </ts>
               <ts e="T85" id="Seg_652" n="e" s="T84">mɨqɨt </ts>
               <ts e="T86" id="Seg_654" n="e" s="T85">nʼuːtɨt </ts>
               <ts e="T87" id="Seg_656" n="e" s="T86">aj </ts>
               <ts e="T88" id="Seg_658" n="e" s="T87">lotɨn </ts>
               <ts e="T89" id="Seg_660" n="e" s="T88">puːtoːqɨt </ts>
               <ts e="T90" id="Seg_662" n="e" s="T89">orɨmka </ts>
               <ts e="T91" id="Seg_664" n="e" s="T90">aj </ts>
               <ts e="T92" id="Seg_666" n="e" s="T91">ilɨkka </ts>
               <ts e="T93" id="Seg_668" n="e" s="T92">orsa </ts>
               <ts e="T94" id="Seg_670" n="e" s="T93">kočʼi </ts>
               <ts e="T95" id="Seg_672" n="e" s="T94">kunmɔːntɨlʼ </ts>
               <ts e="T96" id="Seg_674" n="e" s="T95">poːrätɨ </ts>
               <ts e="T97" id="Seg_676" n="e" s="T96">kurassɨmpɨlʼ </ts>
               <ts e="T98" id="Seg_678" n="e" s="T97">šiːpat: </ts>
               <ts e="T99" id="Seg_680" n="e" s="T98">paqkät, </ts>
               <ts e="T100" id="Seg_682" n="e" s="T99">alqɨt, </ts>
               <ts e="T101" id="Seg_684" n="e" s="T100">mɔːčʼipɨt, </ts>
               <ts e="T102" id="Seg_686" n="e" s="T101">ɛrjat, </ts>
               <ts e="T103" id="Seg_688" n="e" s="T102">čʼünkočʼit, </ts>
               <ts e="T104" id="Seg_690" n="e" s="T103">poːčʼiqot, </ts>
               <ts e="T105" id="Seg_692" n="e" s="T104">čʼarkočʼit, </ts>
               <ts e="T106" id="Seg_694" n="e" s="T105">üt </ts>
               <ts e="T107" id="Seg_696" n="e" s="T106">šiːpat. </ts>
               <ts e="T108" id="Seg_698" n="e" s="T107">Kɨpa </ts>
               <ts e="T109" id="Seg_700" n="e" s="T108">šiːpalʼ </ts>
               <ts e="T110" id="Seg_702" n="e" s="T109">kärɨt </ts>
               <ts e="T111" id="Seg_704" n="e" s="T110">šit </ts>
               <ts e="T112" id="Seg_706" n="e" s="T111">nennɨ </ts>
               <ts e="T113" id="Seg_708" n="e" s="T112">tılʼtaıːmpɔːtɨt </ts>
               <ts e="T114" id="Seg_710" n="e" s="T113">(/tılʼtälʼıːmpɔːtɨn/ </ts>
               <ts e="T115" id="Seg_712" n="e" s="T114">tılʼtälʼɛːmpɔːt) </ts>
               <ts e="T116" id="Seg_714" n="e" s="T115">aj </ts>
               <ts e="T117" id="Seg_716" n="e" s="T116">ütə </ts>
               <ts e="T118" id="Seg_718" n="e" s="T117">pɔːrɨn </ts>
               <ts e="T119" id="Seg_720" n="e" s="T118">tıːmpɨsɔːtɨt. </ts>
               <ts e="T120" id="Seg_722" n="e" s="T119">Puškanɨt </ts>
               <ts e="T121" id="Seg_724" n="e" s="T120">passɛːtäkkɨt, </ts>
               <ts e="T122" id="Seg_726" n="e" s="T121">wačʼičʼikkɔːtɨt </ts>
               <ts e="T123" id="Seg_728" n="e" s="T122">(mäčʼičʼikkɔːtɨt) </ts>
               <ts e="T124" id="Seg_730" n="e" s="T123">na </ts>
               <ts e="T125" id="Seg_732" n="e" s="T124">mantɨ </ts>
               <ts e="T126" id="Seg_734" n="e" s="T125">kočʼi </ts>
               <ts e="T127" id="Seg_736" n="e" s="T126">šiːpalʼ </ts>
               <ts e="T129" id="Seg_738" n="e" s="T127">kärɨt, </ts>
               <ts e="T130" id="Seg_740" n="e" s="T129">suːručʼčʼilʼ </ts>
               <ts e="T131" id="Seg_742" n="e" s="T130">qup </ts>
               <ts e="T132" id="Seg_744" n="e" s="T131">ukkur </ts>
               <ts e="T133" id="Seg_746" n="e" s="T132">utɨntɨsä </ts>
               <ts e="T134" id="Seg_748" n="e" s="T133">orakkɨtɨ </ts>
               <ts e="T135" id="Seg_750" n="e" s="T134">ükomtɨ </ts>
               <ts e="T136" id="Seg_752" n="e" s="T135">(/olomtɨ), </ts>
               <ts e="T137" id="Seg_754" n="e" s="T136">nɨːnɨ </ts>
               <ts e="T138" id="Seg_756" n="e" s="T137">kətɨkkɨt: </ts>
               <ts e="T139" id="Seg_758" n="e" s="T138">Fuːuːuː! </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_759" s="T0">MNA_1965_Lgov_transl.001 (001)</ta>
            <ta e="T13" id="Seg_760" s="T1">MNA_1965_Lgov_transl.002 (002.001)</ta>
            <ta e="T18" id="Seg_761" s="T13">MNA_1965_Lgov_transl.003 (002.002)</ta>
            <ta e="T34" id="Seg_762" s="T18">MNA_1965_Lgov_transl.004 (002.003)</ta>
            <ta e="T42" id="Seg_763" s="T34">MNA_1965_Lgov_transl.005 (002.004)</ta>
            <ta e="T56" id="Seg_764" s="T42">MNA_1965_Lgov_transl.006 (002.005)</ta>
            <ta e="T68" id="Seg_765" s="T56">MNA_1965_Lgov_transl.007 (002.006)</ta>
            <ta e="T78" id="Seg_766" s="T68">MNA_1965_Lgov_transl.008 (002.007)</ta>
            <ta e="T107" id="Seg_767" s="T78">MNA_1965_Lgov_transl.009 (002.008)</ta>
            <ta e="T119" id="Seg_768" s="T107">MNA_1965_Lgov_transl.010 (002.009)</ta>
            <ta e="T139" id="Seg_769" s="T119">MNA_1965_Lgov_transl.011 (002.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_770" s="T0">Льгов.</ta>
            <ta e="T13" id="Seg_771" s="T1">kъ̊′ннайlимыт [′kъ̊llимын] ′лʼговты, къ̊тысыт ′меккʼе ′уккырпор ук′кур kуп, тӓпып ′кӓрыс′отыт Ермолай.</ta>
            <ta e="T18" id="Seg_772" s="T13">ме ′нымд̂ы ′шʼипап ′чатӓлʼтендом ′кочик.</ta>
            <ta e="T34" id="Seg_773" s="T18">′сома ′суру[ы]чиl ′kумы′чо̄ты ′ма̄чоɣ[k]ыl ′шʼипа а′ша ′орылʼе ′kъ̊ттыты но ′аlбеkыт ′jарык ′сурып ни ′кут ′чеңга. </ta>
            <ta e="T42" id="Seg_774" s="T34">мат ′ӱңыlдысак мат ′сурычиl ′kуммы и ′kъ̊сси ′лʼгофты.</ta>
            <ta e="T56" id="Seg_775" s="T42">Лʼгов ′ашʼа ′wӓрkъ kӓтты ′ӓсыче‵рӓl пӱ̄l нулʼ′мосымбыl ай ′шʼиты мʼелʼницаты ′lымбелʼ кы′кʼеkыт – «росота».</ta>
            <ta e="T68" id="Seg_776" s="T56">на ′кыкʼеlа ′сомбала кило′мета′шаlмыkыт ′лʼговноны ′ӓсымпа ′тонды lымпеl (′тоkыт) кы′кʼетkо̄.</ta>
            <ta e="T78" id="Seg_777" s="T68">′kа̄ныkынты ай kут′тоkын чон′доkынды ′сочимпа ′кочи ′нʼӱтыз̂е и ′лотыксӓ[ссӓ].</ta>
            <ta e="T107" id="Seg_778" s="T78">вот на кы′кʼеɣ[k]ыт ′ме̨ргыкыти[ы]l ай кӱңыкытыlмыkыт ′нʼутыт ай лотын ′путоkыт ′орымка ай ′илыкка ′орса ′кочи ′кун′мандыlпо̄рӓты ку′расымбыl ′шʼипат: ′паkкет, ′алkыт, ′мочипыт, ′эрjат, чӱн′кочʼит ′по̄чиkо[ы]т, ′чаркочит, ӱт шʼипат.</ta>
            <ta e="T119" id="Seg_779" s="T107">′кыпа ′шʼипаl ′kӓрыт ′шʼит ′ненны тилʼдаимпотыт [тилʼд̂ӓлʼ импотын[т)) (′тилʼд̂[т]елʼемпот) ′ай ′ӱ̄дъ порым ′тимпы′сотыт.</ta>
            <ta e="T139" id="Seg_780" s="T119">пуш′каныт пас′сӓтӓkыт, ватʼич′икотыт [′мӓчичи′котыт] на манды ′кочи ′шʼипаl kӓрыт суручиl kуп ′уккур ′ӯтынтысе о′раkkыты ′ӱкомты [оlомты] ′ныны къ̊тыкыт: фӯӯӯ.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T1" id="Seg_781" s="T0">Льgov.</ta>
            <ta e="T13" id="Seg_782" s="T1">qənnajlʼimɨt [qəlʼlʼimɨn] lʼgovtɨ, kətɨsɨt mekkʼe ukkɨrpor ukkur qup, täpɨp kärɨsotɨt Еrmolaj.</ta>
            <ta e="T18" id="Seg_783" s="T13">me nɨmd̂ɨ šʼipap čʼatälʼtendom kočʼik.</ta>
            <ta e="T34" id="Seg_784" s="T18">soma suru[ɨ]čʼilʼ qumɨčʼoːtɨ maːčʼoq[q]ɨlʼ šipa aša orɨlʼe qəttɨtɨ no alʼpeqɨt jarɨk surɨp ni kut čʼeŋga.</ta>
            <ta e="T42" id="Seg_785" s="T34">mat üŋɨlʼdɨsak mat surɨčʼilʼ qummɨ i qəssi lʼgoftɨ.</ta>
            <ta e="T56" id="Seg_786" s="T42">Лʼgov ašʼa wärqə qättɨ äsɨčʼerälʼ püːlʼ nulʼmosɨmpɨlʼ aj šʼitɨ mʼelʼnicatɨ lʼɨmpelʼ kɨkʼeqɨt – «rosota».</ta>
            <ta e="T68" id="Seg_787" s="T56">na kɨkʼelʼa sompala kilometašalʼmɨqɨt lʼgovnonɨ äsɨmpa tondɨ lʼɨmpelʼ (toqɨt) kɨkʼetqoː.</ta>
            <ta e="T78" id="Seg_788" s="T68">qaːnɨqɨntɨ aj quttoqɨn čʼondoqɨndɨ sočʼimpa kočʼi nʼütɨẑe i lotɨksä[ssä].</ta>
            <ta e="T107" id="Seg_789" s="T78">vot na kɨkʼeq[q]ɨt mergɨkɨti[ɨ]lʼ aj küŋɨkɨtɨlʼmɨqɨt nʼutɨt aj lotɨn putoqɨt orɨmka aj ilɨkka orsa kočʼi kunmandɨlʼpoːrätɨ kurasɨmpɨlʼ šipat: paqket, alqɨt, močʼipɨt, эrjat, čʼünkočʼʼit poːčʼiqo[ɨ]t, čʼarkočʼit, üt šipat.</ta>
            <ta e="T119" id="Seg_790" s="T107">kɨpa šʼipalʼ qärɨt šʼit nennɨ tilʼdaimpotɨt [tilʼd̂älʼ impotɨn[t)) (tilʼd̂[t]elʼempot) aj üːdə porɨm timpɨsotɨt.</ta>
            <ta e="T139" id="Seg_791" s="T119">puškanɨt passätäqɨt, vatʼičʼikotɨt [mäčʼičʼikotɨt] na mandɨ kočʼi šʼipalʼ qärɨt suručʼilʼ qup ukkur uːtɨntɨse oraqqɨtɨ ükomtɨ [olʼomtɨ] nɨnɨ kətɨkɨt: fuːuːuː.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_792" s="T0">Lʼgov. </ta>
            <ta e="T13" id="Seg_793" s="T1">Qənnɛjlimɨt (/qəllimɨn) Lʼgovtɨ,— kətɨsɨt mäkkʼä ukkɨr pɔːr ukkur qup, täpɨp qärɨsɔːtɨt Jermolaj. </ta>
            <ta e="T18" id="Seg_794" s="T13">Meː nɨmtɨ šiːpap čʼatälʼtɛntɔːm kočʼik. </ta>
            <ta e="T34" id="Seg_795" s="T18">Soma suːrɨčʼčʼilʼ qumɨt čʼɔːtɨ mačʼoːqɨlʼ šiːpa aša orɨlʼlʼä qəttɨtɨ, no alpäqɨt jarɨk suːrɨp ni kut čʼäːŋka. </ta>
            <ta e="T42" id="Seg_796" s="T34">Mat üŋɨltɨsak mat surɨčʼčʼilʼ qummɨ i qəssi Lʼgovtɨ. </ta>
            <ta e="T56" id="Seg_797" s="T42">Lʼgov— aša wärqə qäːttɨ äsɨčʼerälʼ pülʼ nuːlʼ mɔːssɨmpɨlʼ aj šitɨ mʼelʼnicatɨ lɨmpälʼ kɨkʼäqɨt Rosota. </ta>
            <ta e="T68" id="Seg_798" s="T56">Na kɨkʼälʼa sompala kilometašalʼ mɨqɨt Lʼgov nɔːnɨ ɛsɨmpa tɔːntɨ lɨmpälʼ (toːqɨt) kɨkʼätqo. </ta>
            <ta e="T78" id="Seg_799" s="T68">Qanɨqqɨntɨ aj kuttoːqɨn čʼontoːqɨntɨ sočʼimpa kočʼi nʼuːtɨsä i lotɨksä (/lotɨssä). </ta>
            <ta e="T107" id="Seg_800" s="T78">Vot na kɨkʼäqɨt mɛrkɨkɨtɨlʼ aj küŋɨkɨtɨlʼ mɨqɨt nʼuːtɨt aj lotɨn puːtoːqɨt orɨmka aj ilɨkka orsa kočʼi kunmɔːntɨlʼ poːrätɨ kurassɨmpɨlʼ šiːpat: paqkät, alqɨt, mɔːčʼipɨt, ɛrjat, čʼünkočʼit, poːčʼiqot, čʼarkočʼit, üt šiːpat. </ta>
            <ta e="T119" id="Seg_801" s="T107">Kɨpa šiːpalʼ kärɨt šit nennɨ tılʼtaıːmpɔːtɨt (/tılʼtälʼıːmpɔːtɨn/ tılʼtälʼɛːmpɔːt) aj ütə pɔːrɨn tıːmpɨsɔːtɨt. </ta>
            <ta e="T139" id="Seg_802" s="T119">Puškanɨt passɛːtäkkɨt, wačʼičʼikkɔːtɨt (mäčʼičʼikkɔːtɨt) na mantɨ kočʼi šiːpalʼ kärɨt, suːručʼčʼilʼ qup ukkur utɨntɨsä orakkɨtɨ ükomtɨ (/olomtɨ), nɨːnɨ kətɨkkɨt: Fuːuːuː! </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_803" s="T0">lʼgov</ta>
            <ta e="T2" id="Seg_804" s="T1">qənn-ɛj-li-mɨt</ta>
            <ta e="T3" id="Seg_805" s="T2">qəl-li-mɨn</ta>
            <ta e="T4" id="Seg_806" s="T3">lʼgov-tɨ</ta>
            <ta e="T5" id="Seg_807" s="T4">kətɨ-sɨ-t</ta>
            <ta e="T6" id="Seg_808" s="T5">mäkkʼä</ta>
            <ta e="T7" id="Seg_809" s="T6">ukkɨr</ta>
            <ta e="T8" id="Seg_810" s="T7">pɔːr</ta>
            <ta e="T9" id="Seg_811" s="T8">ukkur</ta>
            <ta e="T10" id="Seg_812" s="T9">qup</ta>
            <ta e="T11" id="Seg_813" s="T10">täp-ɨ-p</ta>
            <ta e="T12" id="Seg_814" s="T11">qärɨ-sɔː-tɨt</ta>
            <ta e="T13" id="Seg_815" s="T12">Jermolaj</ta>
            <ta e="T14" id="Seg_816" s="T13">meː</ta>
            <ta e="T15" id="Seg_817" s="T14">nɨmtɨ</ta>
            <ta e="T16" id="Seg_818" s="T15">šiːpa-p</ta>
            <ta e="T17" id="Seg_819" s="T16">čʼat-älʼ-tɛntɔː-m</ta>
            <ta e="T18" id="Seg_820" s="T17">kočʼi-k</ta>
            <ta e="T19" id="Seg_821" s="T18">soma</ta>
            <ta e="T20" id="Seg_822" s="T19">suːrɨ-čʼ-čʼilʼ</ta>
            <ta e="T21" id="Seg_823" s="T20">qum-ɨ-t</ta>
            <ta e="T22" id="Seg_824" s="T21">čʼɔːtɨ</ta>
            <ta e="T23" id="Seg_825" s="T22">mačʼoː-qɨ-lʼ</ta>
            <ta e="T24" id="Seg_826" s="T23">šiːpa</ta>
            <ta e="T25" id="Seg_827" s="T24">aša</ta>
            <ta e="T26" id="Seg_828" s="T25">orɨ-lʼ-lʼä</ta>
            <ta e="T27" id="Seg_829" s="T26">qət-tɨ-tɨ</ta>
            <ta e="T28" id="Seg_830" s="T27">no</ta>
            <ta e="T29" id="Seg_831" s="T28">alpä-qɨt</ta>
            <ta e="T30" id="Seg_832" s="T29">jarɨk</ta>
            <ta e="T31" id="Seg_833" s="T30">suːrɨp</ta>
            <ta e="T32" id="Seg_834" s="T31">ni</ta>
            <ta e="T33" id="Seg_835" s="T32">kut</ta>
            <ta e="T34" id="Seg_836" s="T33">čʼäːŋka</ta>
            <ta e="T35" id="Seg_837" s="T34">mat</ta>
            <ta e="T36" id="Seg_838" s="T35">üŋɨl-tɨ-sa-k</ta>
            <ta e="T37" id="Seg_839" s="T36">mat</ta>
            <ta e="T38" id="Seg_840" s="T37">surɨ-čʼ-čʼilʼ</ta>
            <ta e="T39" id="Seg_841" s="T38">qummɨ</ta>
            <ta e="T40" id="Seg_842" s="T39">i</ta>
            <ta e="T41" id="Seg_843" s="T40">qəs-si</ta>
            <ta e="T42" id="Seg_844" s="T41">lʼgov-tɨ</ta>
            <ta e="T43" id="Seg_845" s="T42">lʼgov</ta>
            <ta e="T44" id="Seg_846" s="T43">aša</ta>
            <ta e="T45" id="Seg_847" s="T44">wärqə</ta>
            <ta e="T46" id="Seg_848" s="T45">qäːttɨ</ta>
            <ta e="T47" id="Seg_849" s="T46">äsɨčʼerä-lʼ</ta>
            <ta e="T48" id="Seg_850" s="T47">pü-lʼ</ta>
            <ta e="T49" id="Seg_851" s="T48">nuː-lʼ</ta>
            <ta e="T50" id="Seg_852" s="T49">mɔːs-sɨmpɨ-lʼ</ta>
            <ta e="T51" id="Seg_853" s="T50">aj</ta>
            <ta e="T52" id="Seg_854" s="T51">šitɨ</ta>
            <ta e="T53" id="Seg_855" s="T52">mʼelʼnica-tɨ</ta>
            <ta e="T54" id="Seg_856" s="T53">lɨmpä-lʼ</ta>
            <ta e="T55" id="Seg_857" s="T54">kɨkʼä-qɨt</ta>
            <ta e="T56" id="Seg_858" s="T55">rosota</ta>
            <ta e="T57" id="Seg_859" s="T56">na</ta>
            <ta e="T58" id="Seg_860" s="T57">kɨkʼä-lʼa</ta>
            <ta e="T59" id="Seg_861" s="T58">sompala</ta>
            <ta e="T60" id="Seg_862" s="T59">kilometa-ša-lʼ</ta>
            <ta e="T61" id="Seg_863" s="T60">mɨ-qɨt</ta>
            <ta e="T62" id="Seg_864" s="T61">Lʼgov</ta>
            <ta e="T63" id="Seg_865" s="T62">nɔː-nɨ</ta>
            <ta e="T64" id="Seg_866" s="T63">ɛsɨ-mpa</ta>
            <ta e="T65" id="Seg_867" s="T64">tɔːntɨ</ta>
            <ta e="T66" id="Seg_868" s="T65">lɨmpä-lʼ</ta>
            <ta e="T67" id="Seg_869" s="T66">toː-qɨt</ta>
            <ta e="T68" id="Seg_870" s="T67">kɨkʼä-tqo</ta>
            <ta e="T69" id="Seg_871" s="T68">qanɨq-qɨn-tɨ</ta>
            <ta e="T70" id="Seg_872" s="T69">aj</ta>
            <ta e="T71" id="Seg_873" s="T70">kut-toː-qɨn</ta>
            <ta e="T72" id="Seg_874" s="T71">čʼontoː-qɨn-tɨ</ta>
            <ta e="T73" id="Seg_875" s="T72">sočʼi-mpa</ta>
            <ta e="T74" id="Seg_876" s="T73">kočʼi</ta>
            <ta e="T75" id="Seg_877" s="T74">nʼuːtɨ-sä</ta>
            <ta e="T76" id="Seg_878" s="T75">i</ta>
            <ta e="T77" id="Seg_879" s="T76">lotɨk-sä</ta>
            <ta e="T78" id="Seg_880" s="T77">lotɨs-sä</ta>
            <ta e="T79" id="Seg_881" s="T78">vot</ta>
            <ta e="T80" id="Seg_882" s="T79">na</ta>
            <ta e="T81" id="Seg_883" s="T80">kɨkʼä-qɨt</ta>
            <ta e="T82" id="Seg_884" s="T81">mɛrkɨ-kɨtɨ-lʼ</ta>
            <ta e="T83" id="Seg_885" s="T82">aj</ta>
            <ta e="T84" id="Seg_886" s="T83">küŋɨ-kɨtɨ-lʼ</ta>
            <ta e="T85" id="Seg_887" s="T84">mɨ-qɨt</ta>
            <ta e="T86" id="Seg_888" s="T85">nʼuːtɨ-t</ta>
            <ta e="T87" id="Seg_889" s="T86">aj</ta>
            <ta e="T88" id="Seg_890" s="T87">lotɨ-n</ta>
            <ta e="T89" id="Seg_891" s="T88">puːtoː-qɨt</ta>
            <ta e="T90" id="Seg_892" s="T89">orɨ-m-ka</ta>
            <ta e="T91" id="Seg_893" s="T90">aj</ta>
            <ta e="T92" id="Seg_894" s="T91">ilɨ-kka</ta>
            <ta e="T93" id="Seg_895" s="T92">or-sa</ta>
            <ta e="T94" id="Seg_896" s="T93">kočʼi</ta>
            <ta e="T95" id="Seg_897" s="T94">kun-mɔːntɨ-lʼ</ta>
            <ta e="T96" id="Seg_898" s="T95">poːrä-tɨ</ta>
            <ta e="T97" id="Seg_899" s="T96">kuras-sɨmpɨ-lʼ</ta>
            <ta e="T98" id="Seg_900" s="T97">šiːpa-t</ta>
            <ta e="T99" id="Seg_901" s="T98">paqkä-t</ta>
            <ta e="T100" id="Seg_902" s="T99">alqɨ-t</ta>
            <ta e="T101" id="Seg_903" s="T100">mɔːčʼipɨ-t</ta>
            <ta e="T102" id="Seg_904" s="T101">ɛrja-t</ta>
            <ta e="T103" id="Seg_905" s="T102">čʼünkočʼi-t</ta>
            <ta e="T104" id="Seg_906" s="T103">poːčʼiqo-t</ta>
            <ta e="T105" id="Seg_907" s="T104">čʼarkočʼi-t</ta>
            <ta e="T106" id="Seg_908" s="T105">üt</ta>
            <ta e="T107" id="Seg_909" s="T106">šiːpa-t</ta>
            <ta e="T108" id="Seg_910" s="T107">kɨpa</ta>
            <ta e="T109" id="Seg_911" s="T108">šiːpa-lʼ</ta>
            <ta e="T110" id="Seg_912" s="T109">kärɨ-t</ta>
            <ta e="T111" id="Seg_913" s="T110">šit</ta>
            <ta e="T112" id="Seg_914" s="T111">nennɨ</ta>
            <ta e="T113" id="Seg_915" s="T112">tılʼ-ta-ıː-mpɔː-tɨt</ta>
            <ta e="T114" id="Seg_916" s="T113">tılʼ-tä-lʼ-ıː-mpɔː-tɨn</ta>
            <ta e="T115" id="Seg_917" s="T114">tılʼ-tä-lʼ-ɛː-mpɔː-t</ta>
            <ta e="T116" id="Seg_918" s="T115">aj</ta>
            <ta e="T117" id="Seg_919" s="T116">ütə</ta>
            <ta e="T118" id="Seg_920" s="T117">pɔːrɨ-n</ta>
            <ta e="T119" id="Seg_921" s="T118">tıː-mpɨ-sɔː-tɨt</ta>
            <ta e="T120" id="Seg_922" s="T119">puška-nɨt</ta>
            <ta e="T121" id="Seg_923" s="T120">passɛː-tä-kkɨ-t</ta>
            <ta e="T122" id="Seg_924" s="T121">wačʼi-čʼi-kkɔː-tɨt</ta>
            <ta e="T123" id="Seg_925" s="T122">mäčʼi-čʼi-kkɔː-tɨt</ta>
            <ta e="T124" id="Seg_926" s="T123">na</ta>
            <ta e="T125" id="Seg_927" s="T124">mantɨ</ta>
            <ta e="T126" id="Seg_928" s="T125">kočʼi</ta>
            <ta e="T127" id="Seg_929" s="T126">šiːpa-lʼ</ta>
            <ta e="T129" id="Seg_930" s="T127">kärɨ-t</ta>
            <ta e="T130" id="Seg_931" s="T129">suːru-čʼ-čʼilʼ</ta>
            <ta e="T131" id="Seg_932" s="T130">qup</ta>
            <ta e="T132" id="Seg_933" s="T131">ukkur</ta>
            <ta e="T133" id="Seg_934" s="T132">utɨ-ntɨ-sä</ta>
            <ta e="T134" id="Seg_935" s="T133">ora-kkɨ-tɨ</ta>
            <ta e="T135" id="Seg_936" s="T134">üko-m-tɨ</ta>
            <ta e="T136" id="Seg_937" s="T135">olo-m-tɨ</ta>
            <ta e="T137" id="Seg_938" s="T136">nɨːnɨ</ta>
            <ta e="T138" id="Seg_939" s="T137">kətɨ-kkɨ-t</ta>
            <ta e="T139" id="Seg_940" s="T138">fuːuːuː</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_941" s="T0">Lʼgov</ta>
            <ta e="T2" id="Seg_942" s="T1">qən-ɛː-li-mɨt</ta>
            <ta e="T3" id="Seg_943" s="T2">qən-lä-mɨt</ta>
            <ta e="T4" id="Seg_944" s="T3">Lʼgov-ntɨ</ta>
            <ta e="T5" id="Seg_945" s="T4">kətɨ-sɨ-tɨ</ta>
            <ta e="T6" id="Seg_946" s="T5">mäkkä</ta>
            <ta e="T7" id="Seg_947" s="T6">ukkɨr</ta>
            <ta e="T8" id="Seg_948" s="T7">pɔːrɨ</ta>
            <ta e="T9" id="Seg_949" s="T8">ukkɨr</ta>
            <ta e="T10" id="Seg_950" s="T9">qum</ta>
            <ta e="T11" id="Seg_951" s="T10">təp-ɨ-m</ta>
            <ta e="T12" id="Seg_952" s="T11">qərɨ-sɨ-tɨt</ta>
            <ta e="T13" id="Seg_953" s="T12">Jermolaj</ta>
            <ta e="T14" id="Seg_954" s="T13">meː</ta>
            <ta e="T15" id="Seg_955" s="T14">nɨmtɨ</ta>
            <ta e="T16" id="Seg_956" s="T15">šiːpa-m</ta>
            <ta e="T17" id="Seg_957" s="T16">čʼattɨ-olʼ-ɛntɨ-mɨt</ta>
            <ta e="T18" id="Seg_958" s="T17">kočʼčʼɨ-k</ta>
            <ta e="T19" id="Seg_959" s="T18">soma</ta>
            <ta e="T20" id="Seg_960" s="T19">suːrɨm-š-ntɨlʼ</ta>
            <ta e="T21" id="Seg_961" s="T20">qum-ɨ-n</ta>
            <ta e="T22" id="Seg_962" s="T21">čʼɔːtɨ</ta>
            <ta e="T23" id="Seg_963" s="T22">mačʼɨ-qɨn-lʼ</ta>
            <ta e="T24" id="Seg_964" s="T23">šiːpa</ta>
            <ta e="T25" id="Seg_965" s="T24">ašša</ta>
            <ta e="T26" id="Seg_966" s="T25">orɨ-š-lä</ta>
            <ta e="T27" id="Seg_967" s="T26">qət-tɨ-tɨ</ta>
            <ta e="T28" id="Seg_968" s="T27">no</ta>
            <ta e="T29" id="Seg_969" s="T28">älpä-qɨn</ta>
            <ta e="T30" id="Seg_970" s="T29">jarɨk</ta>
            <ta e="T31" id="Seg_971" s="T30">suːrɨm</ta>
            <ta e="T32" id="Seg_972" s="T31">nʼi</ta>
            <ta e="T33" id="Seg_973" s="T32">kun</ta>
            <ta e="T34" id="Seg_974" s="T33">čʼäːŋkɨ</ta>
            <ta e="T35" id="Seg_975" s="T34">man</ta>
            <ta e="T36" id="Seg_976" s="T35">üŋkɨl-tɨ-sɨ-k</ta>
            <ta e="T37" id="Seg_977" s="T36">man</ta>
            <ta e="T38" id="Seg_978" s="T37">suːrɨm-š-ntɨlʼ</ta>
            <ta e="T39" id="Seg_979" s="T38">qum</ta>
            <ta e="T40" id="Seg_980" s="T39">i</ta>
            <ta e="T41" id="Seg_981" s="T40">qən-sɨ</ta>
            <ta e="T42" id="Seg_982" s="T41">Lʼgov-ntɨ</ta>
            <ta e="T43" id="Seg_983" s="T42">Lʼgov</ta>
            <ta e="T44" id="Seg_984" s="T43">ašša</ta>
            <ta e="T45" id="Seg_985" s="T44">wərqɨ</ta>
            <ta e="T46" id="Seg_986" s="T45">qəːttɨ</ta>
            <ta e="T47" id="Seg_987" s="T46">isɛčʼarɨ-lʼ</ta>
            <ta e="T48" id="Seg_988" s="T47">pü-lʼ</ta>
            <ta e="T49" id="Seg_989" s="T48">nom-lʼ</ta>
            <ta e="T50" id="Seg_990" s="T49">mɔːt-sɨma-lʼ</ta>
            <ta e="T51" id="Seg_991" s="T50">aj</ta>
            <ta e="T52" id="Seg_992" s="T51">šittɨ</ta>
            <ta e="T53" id="Seg_993" s="T52">mʼelʼnica-tɨ</ta>
            <ta e="T54" id="Seg_994" s="T53">lɨmpä-lʼ</ta>
            <ta e="T55" id="Seg_995" s="T54">kıkä-qɨn</ta>
            <ta e="T56" id="Seg_996" s="T55">Rosota</ta>
            <ta e="T57" id="Seg_997" s="T56">na</ta>
            <ta e="T58" id="Seg_998" s="T57">kıkä-lʼa</ta>
            <ta e="T59" id="Seg_999" s="T58">sompɨla</ta>
            <ta e="T60" id="Seg_1000" s="T59">kilometa-ššak-lʼ</ta>
            <ta e="T61" id="Seg_1001" s="T60">mɨ-qɨn</ta>
            <ta e="T62" id="Seg_1002" s="T61">Lʼgov</ta>
            <ta e="T63" id="Seg_1003" s="T62">*nɔː-nɨ</ta>
            <ta e="T64" id="Seg_1004" s="T63">ɛsɨ-mpɨ</ta>
            <ta e="T65" id="Seg_1005" s="T64">tɔːntɨ</ta>
            <ta e="T66" id="Seg_1006" s="T65">lɨmpä-lʼ</ta>
            <ta e="T67" id="Seg_1007" s="T66">toː-qɨn</ta>
            <ta e="T68" id="Seg_1008" s="T67">kıkä-tqo</ta>
            <ta e="T69" id="Seg_1009" s="T68">qanɨŋ-qɨn-ntɨ</ta>
            <ta e="T70" id="Seg_1010" s="T69">aj</ta>
            <ta e="T71" id="Seg_1011" s="T70">kun-toː-qɨn</ta>
            <ta e="T72" id="Seg_1012" s="T71">čʼontɨ-qɨn-ntɨ</ta>
            <ta e="T73" id="Seg_1013" s="T72">sočʼɨ-mpɨ</ta>
            <ta e="T74" id="Seg_1014" s="T73">kočʼčʼɨ</ta>
            <ta e="T75" id="Seg_1015" s="T74">nʼuːtɨ-sä</ta>
            <ta e="T76" id="Seg_1016" s="T75">i</ta>
            <ta e="T77" id="Seg_1017" s="T76">lotɨŋ-sä</ta>
            <ta e="T78" id="Seg_1018" s="T77">lotɨŋ-sä</ta>
            <ta e="T79" id="Seg_1019" s="T78">vot</ta>
            <ta e="T80" id="Seg_1020" s="T79">na</ta>
            <ta e="T81" id="Seg_1021" s="T80">kıkä-qɨn</ta>
            <ta e="T82" id="Seg_1022" s="T81">mɛrkɨ-kɨtɨ-lʼ</ta>
            <ta e="T83" id="Seg_1023" s="T82">aj</ta>
            <ta e="T84" id="Seg_1024" s="T83">küŋɨ-kɨtɨ-lʼ</ta>
            <ta e="T85" id="Seg_1025" s="T84">mɨ-qɨn</ta>
            <ta e="T86" id="Seg_1026" s="T85">nʼuːtɨ-n</ta>
            <ta e="T87" id="Seg_1027" s="T86">aj</ta>
            <ta e="T88" id="Seg_1028" s="T87">lotɨŋ-n</ta>
            <ta e="T89" id="Seg_1029" s="T88">puːtɨ-qɨn</ta>
            <ta e="T90" id="Seg_1030" s="T89">orɨ-m-kkɨ</ta>
            <ta e="T91" id="Seg_1031" s="T90">aj</ta>
            <ta e="T92" id="Seg_1032" s="T91">ilɨ-kkɨ</ta>
            <ta e="T93" id="Seg_1033" s="T92">orɨ-sä</ta>
            <ta e="T94" id="Seg_1034" s="T93">kočʼčʼɨ</ta>
            <ta e="T95" id="Seg_1035" s="T94">kun-mɔːntɨ-lʼ</ta>
            <ta e="T96" id="Seg_1036" s="T95">poːrä-tɨ</ta>
            <ta e="T97" id="Seg_1037" s="T96">kuras-sɨma-lʼ</ta>
            <ta e="T98" id="Seg_1038" s="T97">šiːpa-t</ta>
            <ta e="T99" id="Seg_1039" s="T98">pakä-t</ta>
            <ta e="T100" id="Seg_1040" s="T99">alqɨ-t</ta>
            <ta e="T101" id="Seg_1041" s="T100">wɔːčʼɨpo-t</ta>
            <ta e="T102" id="Seg_1042" s="T101">ərɨja-t</ta>
            <ta e="T103" id="Seg_1043" s="T102">čʼınkočʼɨ-t</ta>
            <ta e="T104" id="Seg_1044" s="T103">poːčʼɨqo-t</ta>
            <ta e="T105" id="Seg_1045" s="T104">čʼarkočʼi-t</ta>
            <ta e="T106" id="Seg_1046" s="T105">üt</ta>
            <ta e="T107" id="Seg_1047" s="T106">šiːpa-t</ta>
            <ta e="T108" id="Seg_1048" s="T107">kɨpa</ta>
            <ta e="T109" id="Seg_1049" s="T108">šiːpa-lʼ</ta>
            <ta e="T110" id="Seg_1050" s="T109">kärɨ-t</ta>
            <ta e="T111" id="Seg_1051" s="T110">šittɨ</ta>
            <ta e="T112" id="Seg_1052" s="T111">nʼennä</ta>
            <ta e="T113" id="Seg_1053" s="T112">tıː-tä-ıː-mpɨ-tɨt</ta>
            <ta e="T114" id="Seg_1054" s="T113">tıː-tä-lɨ-ıː-mpɨ-tɨt</ta>
            <ta e="T115" id="Seg_1055" s="T114">tıː-tä-lɨ-ɛː-mpɨ-tɨt</ta>
            <ta e="T116" id="Seg_1056" s="T115">aj</ta>
            <ta e="T117" id="Seg_1057" s="T116">üt</ta>
            <ta e="T118" id="Seg_1058" s="T117">pɔːrɨ-n</ta>
            <ta e="T119" id="Seg_1059" s="T118">tıː-mpɨ-sɨ-tɨt</ta>
            <ta e="T120" id="Seg_1060" s="T119">puška-nɨt</ta>
            <ta e="T121" id="Seg_1061" s="T120">pasɨ-tä-kkɨ-tɨ</ta>
            <ta e="T122" id="Seg_1062" s="T121">wəčʼčʼɨ-čʼɨ-kkɨ-tɨt</ta>
            <ta e="T123" id="Seg_1063" s="T122">wəčʼčʼɨ-čʼɨ-kkɨ-tɨt</ta>
            <ta e="T124" id="Seg_1064" s="T123">na</ta>
            <ta e="T125" id="Seg_1065" s="T124">montɨ</ta>
            <ta e="T126" id="Seg_1066" s="T125">kočʼčʼɨ</ta>
            <ta e="T127" id="Seg_1067" s="T126">šiːpa-lʼ</ta>
            <ta e="T129" id="Seg_1068" s="T127">kärɨ-t</ta>
            <ta e="T130" id="Seg_1069" s="T129">suːrɨm-š-ntɨlʼ</ta>
            <ta e="T131" id="Seg_1070" s="T130">qum</ta>
            <ta e="T132" id="Seg_1071" s="T131">ukkɨr</ta>
            <ta e="T133" id="Seg_1072" s="T132">utɨ-ntɨ-sä</ta>
            <ta e="T134" id="Seg_1073" s="T133">ora-kkɨ-tɨ</ta>
            <ta e="T135" id="Seg_1074" s="T134">ükɨ-m-tɨ</ta>
            <ta e="T136" id="Seg_1075" s="T135">olɨ-m-tɨ</ta>
            <ta e="T137" id="Seg_1076" s="T136">nɨːnɨ</ta>
            <ta e="T138" id="Seg_1077" s="T137">kətɨ-kkɨ-tɨ</ta>
            <ta e="T139" id="Seg_1078" s="T138">fuː</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1079" s="T0">Lgov.[NOM]</ta>
            <ta e="T2" id="Seg_1080" s="T1">leave-PFV-OPT-1PL</ta>
            <ta e="T3" id="Seg_1081" s="T2">leave-OPT-1PL</ta>
            <ta e="T4" id="Seg_1082" s="T3">Lgov-ILL</ta>
            <ta e="T5" id="Seg_1083" s="T4">say-PST-3SG.O</ta>
            <ta e="T6" id="Seg_1084" s="T5">I.ALL</ta>
            <ta e="T7" id="Seg_1085" s="T6">one</ta>
            <ta e="T8" id="Seg_1086" s="T7">time.[NOM]</ta>
            <ta e="T9" id="Seg_1087" s="T8">one</ta>
            <ta e="T10" id="Seg_1088" s="T9">human.being.[NOM]</ta>
            <ta e="T11" id="Seg_1089" s="T10">(s)he-EP-ACC</ta>
            <ta e="T12" id="Seg_1090" s="T11">call-PST-3PL</ta>
            <ta e="T13" id="Seg_1091" s="T12">Jermolaj.[NOM]</ta>
            <ta e="T14" id="Seg_1092" s="T13">we.PL.NOM</ta>
            <ta e="T15" id="Seg_1093" s="T14">there</ta>
            <ta e="T16" id="Seg_1094" s="T15">duck-ACC</ta>
            <ta e="T17" id="Seg_1095" s="T16">shoot-FRQ-FUT-1PL</ta>
            <ta e="T18" id="Seg_1096" s="T17">much-ADVZ</ta>
            <ta e="T19" id="Seg_1097" s="T18">good</ta>
            <ta e="T20" id="Seg_1098" s="T19">wild.animal-VBLZ-PTCP.PRS</ta>
            <ta e="T21" id="Seg_1099" s="T20">human.being-EP-GEN</ta>
            <ta e="T22" id="Seg_1100" s="T21">for</ta>
            <ta e="T23" id="Seg_1101" s="T22">forest-LOC-ADJZ</ta>
            <ta e="T24" id="Seg_1102" s="T23">duck.[NOM]</ta>
            <ta e="T25" id="Seg_1103" s="T24">NEG</ta>
            <ta e="T26" id="Seg_1104" s="T25">force-VBLZ-CVB</ta>
            <ta e="T27" id="Seg_1105" s="T26">kill-TR-3SG.O</ta>
            <ta e="T28" id="Seg_1106" s="T27">but</ta>
            <ta e="T29" id="Seg_1107" s="T28">aside-ADV.LOC</ta>
            <ta e="T30" id="Seg_1108" s="T29">other</ta>
            <ta e="T31" id="Seg_1109" s="T30">wild.animal.[NOM]</ta>
            <ta e="T32" id="Seg_1110" s="T31">NEG</ta>
            <ta e="T33" id="Seg_1111" s="T32">where</ta>
            <ta e="T34" id="Seg_1112" s="T33">NEG.EX.[3SG.S]</ta>
            <ta e="T35" id="Seg_1113" s="T34">I.NOM</ta>
            <ta e="T36" id="Seg_1114" s="T35">hear-IPFV-PST-1SG.S</ta>
            <ta e="T37" id="Seg_1115" s="T36">I.GEN</ta>
            <ta e="T38" id="Seg_1116" s="T37">wild.animal-VBLZ-PTCP.PRS</ta>
            <ta e="T39" id="Seg_1117" s="T38">human.being.[NOM]</ta>
            <ta e="T40" id="Seg_1118" s="T39">and</ta>
            <ta e="T41" id="Seg_1119" s="T40">leave-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_1120" s="T41">Lgov-ILL</ta>
            <ta e="T43" id="Seg_1121" s="T42">Lgov.[NOM]</ta>
            <ta e="T44" id="Seg_1122" s="T43">NEG</ta>
            <ta e="T45" id="Seg_1123" s="T44">big</ta>
            <ta e="T46" id="Seg_1124" s="T45">town.[NOM]</ta>
            <ta e="T47" id="Seg_1125" s="T46">long.time.ago-ADJZ</ta>
            <ta e="T48" id="Seg_1126" s="T47">stone-ADJZ</ta>
            <ta e="T49" id="Seg_1127" s="T48">god-ADJZ</ta>
            <ta e="T50" id="Seg_1128" s="T49">house-PROPR-ADJZ</ta>
            <ta e="T51" id="Seg_1129" s="T50">and</ta>
            <ta e="T52" id="Seg_1130" s="T51">two</ta>
            <ta e="T53" id="Seg_1131" s="T52">mill.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_1132" s="T53">swamp-ADJZ</ta>
            <ta e="T55" id="Seg_1133" s="T54">small.river-LOC</ta>
            <ta e="T56" id="Seg_1134" s="T55">Rosota.[NOM]</ta>
            <ta e="T57" id="Seg_1135" s="T56">this</ta>
            <ta e="T58" id="Seg_1136" s="T57">small.river-DIM.[NOM]</ta>
            <ta e="T59" id="Seg_1137" s="T58">five</ta>
            <ta e="T60" id="Seg_1138" s="T59">kilometer-COR-ADJZ</ta>
            <ta e="T61" id="Seg_1139" s="T60">something-LOC</ta>
            <ta e="T62" id="Seg_1140" s="T61">Lgov.[NOM]</ta>
            <ta e="T63" id="Seg_1141" s="T62">out-ADV.EL</ta>
            <ta e="T64" id="Seg_1142" s="T63">become-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1143" s="T64">wide</ta>
            <ta e="T66" id="Seg_1144" s="T65">swamp-ADJZ</ta>
            <ta e="T67" id="Seg_1145" s="T66">lake-LOC</ta>
            <ta e="T68" id="Seg_1146" s="T67">small.river-TRL</ta>
            <ta e="T69" id="Seg_1147" s="T68">bank-LOC-OBL.3SG</ta>
            <ta e="T70" id="Seg_1148" s="T69">and</ta>
            <ta e="T71" id="Seg_1149" s="T70">where-there-ADV.LOC</ta>
            <ta e="T72" id="Seg_1150" s="T71">middle-LOC-OBL.3SG</ta>
            <ta e="T73" id="Seg_1151" s="T72">grow-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1152" s="T73">much</ta>
            <ta e="T75" id="Seg_1153" s="T74">grass-INSTR</ta>
            <ta e="T76" id="Seg_1154" s="T75">and</ta>
            <ta e="T77" id="Seg_1155" s="T76">equisetum-INSTR</ta>
            <ta e="T78" id="Seg_1156" s="T77">equisetum-INSTR</ta>
            <ta e="T79" id="Seg_1157" s="T78">here</ta>
            <ta e="T80" id="Seg_1158" s="T79">this</ta>
            <ta e="T81" id="Seg_1159" s="T80">small.river-LOC</ta>
            <ta e="T82" id="Seg_1160" s="T81">wind-CAR-ADJZ</ta>
            <ta e="T83" id="Seg_1161" s="T82">and</ta>
            <ta e="T84" id="Seg_1162" s="T83">stream-CAR-ADJZ</ta>
            <ta e="T85" id="Seg_1163" s="T84">something-LOC</ta>
            <ta e="T86" id="Seg_1164" s="T85">grass-GEN</ta>
            <ta e="T87" id="Seg_1165" s="T86">and</ta>
            <ta e="T88" id="Seg_1166" s="T87">equisetum-GEN</ta>
            <ta e="T89" id="Seg_1167" s="T88">inside-LOC</ta>
            <ta e="T90" id="Seg_1168" s="T89">force-TRL-HAB.[3SG.S]</ta>
            <ta e="T91" id="Seg_1169" s="T90">and</ta>
            <ta e="T92" id="Seg_1170" s="T91">live-HAB.[3SG.S]</ta>
            <ta e="T93" id="Seg_1171" s="T92">force-INSTR</ta>
            <ta e="T94" id="Seg_1172" s="T93">much</ta>
            <ta e="T95" id="Seg_1173" s="T94">where-extent-ADJZ</ta>
            <ta e="T96" id="Seg_1174" s="T95">deep.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_1175" s="T96">appearance-PROPR-ADJZ</ta>
            <ta e="T98" id="Seg_1176" s="T97">duck-PL.[NOM]</ta>
            <ta e="T99" id="Seg_1177" s="T98">teal-PL.[NOM]</ta>
            <ta e="T100" id="Seg_1178" s="T99">scoter-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1179" s="T100">diver-PL.[NOM]</ta>
            <ta e="T102" id="Seg_1180" s="T101">pochard-PL.[NOM]</ta>
            <ta e="T103" id="Seg_1181" s="T102">pintail-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1182" s="T103">wigeon-PL.[NOM]</ta>
            <ta e="T105" id="Seg_1183" s="T104">%%-PL.[NOM]</ta>
            <ta e="T106" id="Seg_1184" s="T105">water.[NOM]</ta>
            <ta e="T107" id="Seg_1185" s="T106">duck-PL.[NOM]</ta>
            <ta e="T108" id="Seg_1186" s="T107">small</ta>
            <ta e="T109" id="Seg_1187" s="T108">duck-ADJZ</ta>
            <ta e="T110" id="Seg_1188" s="T109">flock-PL.[NOM]</ta>
            <ta e="T111" id="Seg_1189" s="T110">every.which.way</ta>
            <ta e="T112" id="Seg_1190" s="T111">forward</ta>
            <ta e="T113" id="Seg_1191" s="T112">fly-%%-RFL.PFV-PST.NAR-3PL</ta>
            <ta e="T114" id="Seg_1192" s="T113">fly-%%-INCH-RFL.PFV-PST.NAR-3PL</ta>
            <ta e="T115" id="Seg_1193" s="T114">fly-%%-INCH-PFV-PST.NAR-3PL</ta>
            <ta e="T116" id="Seg_1194" s="T115">and</ta>
            <ta e="T117" id="Seg_1195" s="T116">water.[NOM]</ta>
            <ta e="T118" id="Seg_1196" s="T117">above-ADV.LOC</ta>
            <ta e="T119" id="Seg_1197" s="T118">fly-DUR-PST-3PL</ta>
            <ta e="T120" id="Seg_1198" s="T119">rifle.[NOM]-OBL.1PL</ta>
            <ta e="T121" id="Seg_1199" s="T120">explode-%%-HAB-3SG.O</ta>
            <ta e="T122" id="Seg_1200" s="T121">lift-RFL-HAB-3PL</ta>
            <ta e="T123" id="Seg_1201" s="T122">lift-RFL-HAB-3PL</ta>
            <ta e="T124" id="Seg_1202" s="T123">here</ta>
            <ta e="T125" id="Seg_1203" s="T124">apparently</ta>
            <ta e="T126" id="Seg_1204" s="T125">much</ta>
            <ta e="T127" id="Seg_1205" s="T126">duck-ADJZ</ta>
            <ta e="T129" id="Seg_1206" s="T127">flock-PL.[NOM]</ta>
            <ta e="T130" id="Seg_1207" s="T129">wild.animal-VBLZ-PTCP.PRS</ta>
            <ta e="T131" id="Seg_1208" s="T130">human.being.[NOM]</ta>
            <ta e="T132" id="Seg_1209" s="T131">one</ta>
            <ta e="T133" id="Seg_1210" s="T132">hand-OBL.3SG-INSTR</ta>
            <ta e="T134" id="Seg_1211" s="T133">catch-HAB-3SG.O</ta>
            <ta e="T135" id="Seg_1212" s="T134">cap-ACC-3SG</ta>
            <ta e="T136" id="Seg_1213" s="T135">head-ACC-3SG</ta>
            <ta e="T137" id="Seg_1214" s="T136">then</ta>
            <ta e="T138" id="Seg_1215" s="T137">say-HAB-3SG.O</ta>
            <ta e="T139" id="Seg_1216" s="T138">phew</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1217" s="T0">Льгов.[NOM]</ta>
            <ta e="T2" id="Seg_1218" s="T1">отправиться-PFV-OPT-1PL</ta>
            <ta e="T3" id="Seg_1219" s="T2">отправиться-OPT-1PL</ta>
            <ta e="T4" id="Seg_1220" s="T3">Льгов-ILL</ta>
            <ta e="T5" id="Seg_1221" s="T4">сказать-PST-3SG.O</ta>
            <ta e="T6" id="Seg_1222" s="T5">я.ALL</ta>
            <ta e="T7" id="Seg_1223" s="T6">один</ta>
            <ta e="T8" id="Seg_1224" s="T7">раз.[NOM]</ta>
            <ta e="T9" id="Seg_1225" s="T8">один</ta>
            <ta e="T10" id="Seg_1226" s="T9">человек.[NOM]</ta>
            <ta e="T11" id="Seg_1227" s="T10">он(а)-EP-ACC</ta>
            <ta e="T12" id="Seg_1228" s="T11">звать-PST-3PL</ta>
            <ta e="T13" id="Seg_1229" s="T12">Ермолай.[NOM]</ta>
            <ta e="T14" id="Seg_1230" s="T13">мы.PL.NOM</ta>
            <ta e="T15" id="Seg_1231" s="T14">там</ta>
            <ta e="T16" id="Seg_1232" s="T15">утка-ACC</ta>
            <ta e="T17" id="Seg_1233" s="T16">стрелять-FRQ-FUT-1PL</ta>
            <ta e="T18" id="Seg_1234" s="T17">много-ADVZ</ta>
            <ta e="T19" id="Seg_1235" s="T18">хороший</ta>
            <ta e="T20" id="Seg_1236" s="T19">зверь-VBLZ-PTCP.PRS</ta>
            <ta e="T21" id="Seg_1237" s="T20">человек-EP-GEN</ta>
            <ta e="T22" id="Seg_1238" s="T21">для</ta>
            <ta e="T23" id="Seg_1239" s="T22">лес-LOC-ADJZ</ta>
            <ta e="T24" id="Seg_1240" s="T23">утка.[NOM]</ta>
            <ta e="T25" id="Seg_1241" s="T24">NEG</ta>
            <ta e="T26" id="Seg_1242" s="T25">сила-VBLZ-CVB</ta>
            <ta e="T27" id="Seg_1243" s="T26">убить-TR-3SG.O</ta>
            <ta e="T28" id="Seg_1244" s="T27">но</ta>
            <ta e="T29" id="Seg_1245" s="T28">в.сторону-ADV.LOC</ta>
            <ta e="T30" id="Seg_1246" s="T29">другой</ta>
            <ta e="T31" id="Seg_1247" s="T30">зверь.[NOM]</ta>
            <ta e="T32" id="Seg_1248" s="T31">NEG</ta>
            <ta e="T33" id="Seg_1249" s="T32">где</ta>
            <ta e="T34" id="Seg_1250" s="T33">NEG.EX.[3SG.S]</ta>
            <ta e="T35" id="Seg_1251" s="T34">я.NOM</ta>
            <ta e="T36" id="Seg_1252" s="T35">слушать-IPFV-PST-1SG.S</ta>
            <ta e="T37" id="Seg_1253" s="T36">я.GEN</ta>
            <ta e="T38" id="Seg_1254" s="T37">зверь-VBLZ-PTCP.PRS</ta>
            <ta e="T39" id="Seg_1255" s="T38">человек.[NOM]</ta>
            <ta e="T40" id="Seg_1256" s="T39">и</ta>
            <ta e="T41" id="Seg_1257" s="T40">отправиться-PST.[3SG.S]</ta>
            <ta e="T42" id="Seg_1258" s="T41">Льгов-ILL</ta>
            <ta e="T43" id="Seg_1259" s="T42">Льгов.[NOM]</ta>
            <ta e="T44" id="Seg_1260" s="T43">NEG</ta>
            <ta e="T45" id="Seg_1261" s="T44">большой</ta>
            <ta e="T46" id="Seg_1262" s="T45">город.[NOM]</ta>
            <ta e="T47" id="Seg_1263" s="T46">в.старину-ADJZ</ta>
            <ta e="T48" id="Seg_1264" s="T47">камень-ADJZ</ta>
            <ta e="T49" id="Seg_1265" s="T48">бог-ADJZ</ta>
            <ta e="T50" id="Seg_1266" s="T49">дом-PROPR-ADJZ</ta>
            <ta e="T51" id="Seg_1267" s="T50">и</ta>
            <ta e="T52" id="Seg_1268" s="T51">два</ta>
            <ta e="T53" id="Seg_1269" s="T52">мельница.[NOM]-3SG</ta>
            <ta e="T54" id="Seg_1270" s="T53">болото-ADJZ</ta>
            <ta e="T55" id="Seg_1271" s="T54">речка-LOC</ta>
            <ta e="T56" id="Seg_1272" s="T55">Росота.[NOM]</ta>
            <ta e="T57" id="Seg_1273" s="T56">этот</ta>
            <ta e="T58" id="Seg_1274" s="T57">речка-DIM.[NOM]</ta>
            <ta e="T59" id="Seg_1275" s="T58">пять</ta>
            <ta e="T60" id="Seg_1276" s="T59">километр-COR-ADJZ</ta>
            <ta e="T61" id="Seg_1277" s="T60">нечто-LOC</ta>
            <ta e="T62" id="Seg_1278" s="T61">Льгов.[NOM]</ta>
            <ta e="T63" id="Seg_1279" s="T62">из-ADV.EL</ta>
            <ta e="T64" id="Seg_1280" s="T63">стать-DUR.[3SG.S]</ta>
            <ta e="T65" id="Seg_1281" s="T64">широкий</ta>
            <ta e="T66" id="Seg_1282" s="T65">болото-ADJZ</ta>
            <ta e="T67" id="Seg_1283" s="T66">озеро-LOC</ta>
            <ta e="T68" id="Seg_1284" s="T67">речка-TRL</ta>
            <ta e="T69" id="Seg_1285" s="T68">берег-LOC-OBL.3SG</ta>
            <ta e="T70" id="Seg_1286" s="T69">и</ta>
            <ta e="T71" id="Seg_1287" s="T70">где-туда-ADV.LOC</ta>
            <ta e="T72" id="Seg_1288" s="T71">середина-LOC-OBL.3SG</ta>
            <ta e="T73" id="Seg_1289" s="T72">вырасти-PST.NAR.[3SG.S]</ta>
            <ta e="T74" id="Seg_1290" s="T73">много</ta>
            <ta e="T75" id="Seg_1291" s="T74">трава-INSTR</ta>
            <ta e="T76" id="Seg_1292" s="T75">и</ta>
            <ta e="T77" id="Seg_1293" s="T76">хвощ-INSTR</ta>
            <ta e="T78" id="Seg_1294" s="T77">хвощ-INSTR</ta>
            <ta e="T79" id="Seg_1295" s="T78">вот</ta>
            <ta e="T80" id="Seg_1296" s="T79">этот</ta>
            <ta e="T81" id="Seg_1297" s="T80">речка-LOC</ta>
            <ta e="T82" id="Seg_1298" s="T81">ветер-CAR-ADJZ</ta>
            <ta e="T83" id="Seg_1299" s="T82">и</ta>
            <ta e="T84" id="Seg_1300" s="T83">течение-CAR-ADJZ</ta>
            <ta e="T85" id="Seg_1301" s="T84">нечто-LOC</ta>
            <ta e="T86" id="Seg_1302" s="T85">трава-GEN</ta>
            <ta e="T87" id="Seg_1303" s="T86">и</ta>
            <ta e="T88" id="Seg_1304" s="T87">хвощ-GEN</ta>
            <ta e="T89" id="Seg_1305" s="T88">внутри-LOC</ta>
            <ta e="T90" id="Seg_1306" s="T89">сила-TRL-HAB.[3SG.S]</ta>
            <ta e="T91" id="Seg_1307" s="T90">и</ta>
            <ta e="T92" id="Seg_1308" s="T91">жить-HAB.[3SG.S]</ta>
            <ta e="T93" id="Seg_1309" s="T92">сила-INSTR</ta>
            <ta e="T94" id="Seg_1310" s="T93">много</ta>
            <ta e="T95" id="Seg_1311" s="T94">где-мера-ADJZ</ta>
            <ta e="T96" id="Seg_1312" s="T95">заводь.[NOM]-3SG</ta>
            <ta e="T97" id="Seg_1313" s="T96">вид-PROPR-ADJZ</ta>
            <ta e="T98" id="Seg_1314" s="T97">утка-PL.[NOM]</ta>
            <ta e="T99" id="Seg_1315" s="T98">чирок-PL.[NOM]</ta>
            <ta e="T100" id="Seg_1316" s="T99">турпан-PL.[NOM]</ta>
            <ta e="T101" id="Seg_1317" s="T100">гагара-PL.[NOM]</ta>
            <ta e="T102" id="Seg_1318" s="T101">нырок-PL.[NOM]</ta>
            <ta e="T103" id="Seg_1319" s="T102">шилохвость-PL.[NOM]</ta>
            <ta e="T104" id="Seg_1320" s="T103">свиязь-PL.[NOM]</ta>
            <ta e="T105" id="Seg_1321" s="T104">%%-PL.[NOM]</ta>
            <ta e="T106" id="Seg_1322" s="T105">вода.[NOM]</ta>
            <ta e="T107" id="Seg_1323" s="T106">утка-PL.[NOM]</ta>
            <ta e="T108" id="Seg_1324" s="T107">маленький</ta>
            <ta e="T109" id="Seg_1325" s="T108">утка-ADJZ</ta>
            <ta e="T110" id="Seg_1326" s="T109">стая-PL.[NOM]</ta>
            <ta e="T111" id="Seg_1327" s="T110">в.разные.стороны</ta>
            <ta e="T112" id="Seg_1328" s="T111">вперёд</ta>
            <ta e="T113" id="Seg_1329" s="T112">летать-%%-RFL.PFV-PST.NAR-3PL</ta>
            <ta e="T114" id="Seg_1330" s="T113">летать-%%-INCH-RFL.PFV-PST.NAR-3PL</ta>
            <ta e="T115" id="Seg_1331" s="T114">летать-%%-INCH-PFV-PST.NAR-3PL</ta>
            <ta e="T116" id="Seg_1332" s="T115">и</ta>
            <ta e="T117" id="Seg_1333" s="T116">вода.[NOM]</ta>
            <ta e="T118" id="Seg_1334" s="T117">над-ADV.LOC</ta>
            <ta e="T119" id="Seg_1335" s="T118">летать-DUR-PST-3PL</ta>
            <ta e="T120" id="Seg_1336" s="T119">ружьё.[NOM]-OBL.1PL</ta>
            <ta e="T121" id="Seg_1337" s="T120">взорваться-%%-HAB-3SG.O</ta>
            <ta e="T122" id="Seg_1338" s="T121">поднять-RFL-HAB-3PL</ta>
            <ta e="T123" id="Seg_1339" s="T122">поднять-RFL-HAB-3PL</ta>
            <ta e="T124" id="Seg_1340" s="T123">вот</ta>
            <ta e="T125" id="Seg_1341" s="T124">видать</ta>
            <ta e="T126" id="Seg_1342" s="T125">много</ta>
            <ta e="T127" id="Seg_1343" s="T126">утка-ADJZ</ta>
            <ta e="T129" id="Seg_1344" s="T127">стая-PL.[NOM]</ta>
            <ta e="T130" id="Seg_1345" s="T129">зверь-VBLZ-PTCP.PRS</ta>
            <ta e="T131" id="Seg_1346" s="T130">человек.[NOM]</ta>
            <ta e="T132" id="Seg_1347" s="T131">один</ta>
            <ta e="T133" id="Seg_1348" s="T132">рука-OBL.3SG-INSTR</ta>
            <ta e="T134" id="Seg_1349" s="T133">хватать-HAB-3SG.O</ta>
            <ta e="T135" id="Seg_1350" s="T134">шапка-ACC-3SG</ta>
            <ta e="T136" id="Seg_1351" s="T135">голова-ACC-3SG</ta>
            <ta e="T137" id="Seg_1352" s="T136">потом</ta>
            <ta e="T138" id="Seg_1353" s="T137">сказать-HAB-3SG.O</ta>
            <ta e="T139" id="Seg_1354" s="T138">фу</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1355" s="T0">nprop-n:case</ta>
            <ta e="T2" id="Seg_1356" s="T1">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T3" id="Seg_1357" s="T2">v-v:mood-v:pn</ta>
            <ta e="T4" id="Seg_1358" s="T3">nprop-n:case</ta>
            <ta e="T5" id="Seg_1359" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_1360" s="T5">pers</ta>
            <ta e="T7" id="Seg_1361" s="T6">num</ta>
            <ta e="T8" id="Seg_1362" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1363" s="T8">num</ta>
            <ta e="T10" id="Seg_1364" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_1365" s="T10">pers-n:ins-n:case</ta>
            <ta e="T12" id="Seg_1366" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1367" s="T12">nprop-n:case</ta>
            <ta e="T14" id="Seg_1368" s="T13">pers</ta>
            <ta e="T15" id="Seg_1369" s="T14">adv</ta>
            <ta e="T16" id="Seg_1370" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1371" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1372" s="T17">quant-adj&gt;adv</ta>
            <ta e="T19" id="Seg_1373" s="T18">adj</ta>
            <ta e="T20" id="Seg_1374" s="T19">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T21" id="Seg_1375" s="T20">n-n:ins-n:case</ta>
            <ta e="T22" id="Seg_1376" s="T21">pp</ta>
            <ta e="T23" id="Seg_1377" s="T22">n-n:case-n&gt;adj</ta>
            <ta e="T24" id="Seg_1378" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_1379" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1380" s="T25">n-n&gt;v-v&gt;adv</ta>
            <ta e="T27" id="Seg_1381" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_1382" s="T27">conj</ta>
            <ta e="T29" id="Seg_1383" s="T28">adv-adv&gt;adv</ta>
            <ta e="T30" id="Seg_1384" s="T29">adj</ta>
            <ta e="T31" id="Seg_1385" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1386" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1387" s="T32">interrog</ta>
            <ta e="T34" id="Seg_1388" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_1389" s="T34">pers</ta>
            <ta e="T36" id="Seg_1390" s="T35">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1391" s="T36">pers</ta>
            <ta e="T38" id="Seg_1392" s="T37">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T39" id="Seg_1393" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1394" s="T39">conj</ta>
            <ta e="T41" id="Seg_1395" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1396" s="T41">nprop-n:case</ta>
            <ta e="T43" id="Seg_1397" s="T42">nprop-n:case</ta>
            <ta e="T44" id="Seg_1398" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1399" s="T44">adj</ta>
            <ta e="T46" id="Seg_1400" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_1401" s="T46">adv-adv&gt;adj</ta>
            <ta e="T48" id="Seg_1402" s="T47">n-n&gt;adj</ta>
            <ta e="T49" id="Seg_1403" s="T48">n-n&gt;adj</ta>
            <ta e="T50" id="Seg_1404" s="T49">n-n&gt;n-n&gt;adj</ta>
            <ta e="T51" id="Seg_1405" s="T50">conj</ta>
            <ta e="T52" id="Seg_1406" s="T51">num</ta>
            <ta e="T53" id="Seg_1407" s="T52">n-n:case-n:poss</ta>
            <ta e="T54" id="Seg_1408" s="T53">n-n&gt;adj</ta>
            <ta e="T55" id="Seg_1409" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1410" s="T55">nprop-n:case</ta>
            <ta e="T57" id="Seg_1411" s="T56">dem</ta>
            <ta e="T58" id="Seg_1412" s="T57">n-n&gt;n-n:case</ta>
            <ta e="T59" id="Seg_1413" s="T58">num</ta>
            <ta e="T60" id="Seg_1414" s="T59">n-n:case-n&gt;adj</ta>
            <ta e="T61" id="Seg_1415" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1416" s="T61">nprop-n:case</ta>
            <ta e="T63" id="Seg_1417" s="T62">pp-adv:case</ta>
            <ta e="T64" id="Seg_1418" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_1419" s="T64">adj</ta>
            <ta e="T66" id="Seg_1420" s="T65">n-n&gt;adj</ta>
            <ta e="T67" id="Seg_1421" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1422" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1423" s="T68">n-n:case-n:obl.poss</ta>
            <ta e="T70" id="Seg_1424" s="T69">conj</ta>
            <ta e="T71" id="Seg_1425" s="T70">interrog-adv-adv&gt;adv</ta>
            <ta e="T72" id="Seg_1426" s="T71">n-n:case-n:obl.poss</ta>
            <ta e="T73" id="Seg_1427" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1428" s="T73">quant</ta>
            <ta e="T75" id="Seg_1429" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_1430" s="T75">conj</ta>
            <ta e="T77" id="Seg_1431" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1432" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1433" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1434" s="T79">dem</ta>
            <ta e="T81" id="Seg_1435" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1436" s="T81">n-n&gt;n-n&gt;adj</ta>
            <ta e="T83" id="Seg_1437" s="T82">conj</ta>
            <ta e="T84" id="Seg_1438" s="T83">n-n&gt;n-n&gt;adj</ta>
            <ta e="T85" id="Seg_1439" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1440" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1441" s="T86">conj</ta>
            <ta e="T88" id="Seg_1442" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1443" s="T88">pp-n:case</ta>
            <ta e="T90" id="Seg_1444" s="T89">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T91" id="Seg_1445" s="T90">conj</ta>
            <ta e="T92" id="Seg_1446" s="T91">v-v&gt;v-v:pn</ta>
            <ta e="T93" id="Seg_1447" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1448" s="T93">quant</ta>
            <ta e="T95" id="Seg_1449" s="T94">interrog-n-n&gt;adj</ta>
            <ta e="T96" id="Seg_1450" s="T95">n-n:case-n:poss</ta>
            <ta e="T97" id="Seg_1451" s="T96">n-n&gt;n-n&gt;adj</ta>
            <ta e="T98" id="Seg_1452" s="T97">n-n:num-n:case</ta>
            <ta e="T99" id="Seg_1453" s="T98">n-n:num-n:case</ta>
            <ta e="T100" id="Seg_1454" s="T99">n-n:num-n:case</ta>
            <ta e="T101" id="Seg_1455" s="T100">n-n:num-n:case</ta>
            <ta e="T102" id="Seg_1456" s="T101">n-n:num-n:case</ta>
            <ta e="T103" id="Seg_1457" s="T102">n-n:num-n:case</ta>
            <ta e="T104" id="Seg_1458" s="T103">n-n:num-n:case</ta>
            <ta e="T105" id="Seg_1459" s="T104">n-n:num-n:case</ta>
            <ta e="T106" id="Seg_1460" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1461" s="T106">n-n:num-n:case</ta>
            <ta e="T108" id="Seg_1462" s="T107">adj</ta>
            <ta e="T109" id="Seg_1463" s="T108">n-n&gt;adj</ta>
            <ta e="T110" id="Seg_1464" s="T109">n-n:num-n:case</ta>
            <ta e="T111" id="Seg_1465" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1466" s="T111">adv</ta>
            <ta e="T113" id="Seg_1467" s="T112">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_1468" s="T113">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_1469" s="T114">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_1470" s="T115">conj</ta>
            <ta e="T117" id="Seg_1471" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_1472" s="T117">pp-n&gt;adv</ta>
            <ta e="T119" id="Seg_1473" s="T118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1474" s="T119">n-n:case-n:obl.poss</ta>
            <ta e="T121" id="Seg_1475" s="T120">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T122" id="Seg_1476" s="T121">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T123" id="Seg_1477" s="T122">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T124" id="Seg_1478" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1479" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1480" s="T125">quant</ta>
            <ta e="T127" id="Seg_1481" s="T126">n-n&gt;adj</ta>
            <ta e="T129" id="Seg_1482" s="T127">n-n:num-n:case</ta>
            <ta e="T130" id="Seg_1483" s="T129">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T131" id="Seg_1484" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_1485" s="T131">num</ta>
            <ta e="T133" id="Seg_1486" s="T132">n-n:obl.poss-n:case</ta>
            <ta e="T134" id="Seg_1487" s="T133">v-v&gt;v-v:pn</ta>
            <ta e="T135" id="Seg_1488" s="T134">n-n:case-n:poss</ta>
            <ta e="T136" id="Seg_1489" s="T135">n-n:case-n:poss</ta>
            <ta e="T137" id="Seg_1490" s="T136">adv</ta>
            <ta e="T138" id="Seg_1491" s="T137">v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_1492" s="T138">interj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1493" s="T0">nprop</ta>
            <ta e="T2" id="Seg_1494" s="T1">v</ta>
            <ta e="T3" id="Seg_1495" s="T2">v</ta>
            <ta e="T4" id="Seg_1496" s="T3">nprop</ta>
            <ta e="T5" id="Seg_1497" s="T4">v</ta>
            <ta e="T6" id="Seg_1498" s="T5">pers</ta>
            <ta e="T7" id="Seg_1499" s="T6">num</ta>
            <ta e="T8" id="Seg_1500" s="T7">n</ta>
            <ta e="T9" id="Seg_1501" s="T8">num</ta>
            <ta e="T10" id="Seg_1502" s="T9">n</ta>
            <ta e="T11" id="Seg_1503" s="T10">pers</ta>
            <ta e="T12" id="Seg_1504" s="T11">v</ta>
            <ta e="T13" id="Seg_1505" s="T12">nprop</ta>
            <ta e="T14" id="Seg_1506" s="T13">pers</ta>
            <ta e="T15" id="Seg_1507" s="T14">adv</ta>
            <ta e="T16" id="Seg_1508" s="T15">n</ta>
            <ta e="T17" id="Seg_1509" s="T16">v</ta>
            <ta e="T18" id="Seg_1510" s="T17">adv</ta>
            <ta e="T19" id="Seg_1511" s="T18">adj</ta>
            <ta e="T20" id="Seg_1512" s="T19">ptcp</ta>
            <ta e="T21" id="Seg_1513" s="T20">n</ta>
            <ta e="T22" id="Seg_1514" s="T21">pp</ta>
            <ta e="T23" id="Seg_1515" s="T22">adj</ta>
            <ta e="T24" id="Seg_1516" s="T23">n</ta>
            <ta e="T25" id="Seg_1517" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1518" s="T25">adv</ta>
            <ta e="T27" id="Seg_1519" s="T26">v</ta>
            <ta e="T28" id="Seg_1520" s="T27">conj</ta>
            <ta e="T29" id="Seg_1521" s="T28">adv</ta>
            <ta e="T30" id="Seg_1522" s="T29">adj</ta>
            <ta e="T31" id="Seg_1523" s="T30">n</ta>
            <ta e="T32" id="Seg_1524" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1525" s="T32">interrog</ta>
            <ta e="T34" id="Seg_1526" s="T33">v</ta>
            <ta e="T35" id="Seg_1527" s="T34">pers</ta>
            <ta e="T36" id="Seg_1528" s="T35">v</ta>
            <ta e="T37" id="Seg_1529" s="T36">pers</ta>
            <ta e="T38" id="Seg_1530" s="T37">ptcp</ta>
            <ta e="T39" id="Seg_1531" s="T38">n</ta>
            <ta e="T40" id="Seg_1532" s="T39">conj</ta>
            <ta e="T41" id="Seg_1533" s="T40">v</ta>
            <ta e="T42" id="Seg_1534" s="T41">nprop</ta>
            <ta e="T43" id="Seg_1535" s="T42">nprop</ta>
            <ta e="T44" id="Seg_1536" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1537" s="T44">adj</ta>
            <ta e="T46" id="Seg_1538" s="T45">n</ta>
            <ta e="T47" id="Seg_1539" s="T46">adj</ta>
            <ta e="T48" id="Seg_1540" s="T47">adj</ta>
            <ta e="T49" id="Seg_1541" s="T48">adj</ta>
            <ta e="T50" id="Seg_1542" s="T49">adj</ta>
            <ta e="T51" id="Seg_1543" s="T50">conj</ta>
            <ta e="T52" id="Seg_1544" s="T51">num</ta>
            <ta e="T53" id="Seg_1545" s="T52">n</ta>
            <ta e="T54" id="Seg_1546" s="T53">adj</ta>
            <ta e="T55" id="Seg_1547" s="T54">n</ta>
            <ta e="T56" id="Seg_1548" s="T55">nprop</ta>
            <ta e="T57" id="Seg_1549" s="T56">dem</ta>
            <ta e="T58" id="Seg_1550" s="T57">n</ta>
            <ta e="T59" id="Seg_1551" s="T58">num</ta>
            <ta e="T60" id="Seg_1552" s="T59">adj</ta>
            <ta e="T61" id="Seg_1553" s="T60">n</ta>
            <ta e="T62" id="Seg_1554" s="T61">nprop</ta>
            <ta e="T63" id="Seg_1555" s="T62">pp</ta>
            <ta e="T64" id="Seg_1556" s="T63">v</ta>
            <ta e="T65" id="Seg_1557" s="T64">adj</ta>
            <ta e="T66" id="Seg_1558" s="T65">adj</ta>
            <ta e="T67" id="Seg_1559" s="T66">n</ta>
            <ta e="T68" id="Seg_1560" s="T67">n</ta>
            <ta e="T69" id="Seg_1561" s="T68">n</ta>
            <ta e="T70" id="Seg_1562" s="T69">conj</ta>
            <ta e="T71" id="Seg_1563" s="T70">adv</ta>
            <ta e="T72" id="Seg_1564" s="T71">n</ta>
            <ta e="T73" id="Seg_1565" s="T72">v</ta>
            <ta e="T74" id="Seg_1566" s="T73">quant</ta>
            <ta e="T75" id="Seg_1567" s="T74">n</ta>
            <ta e="T76" id="Seg_1568" s="T75">conj</ta>
            <ta e="T77" id="Seg_1569" s="T76">n</ta>
            <ta e="T78" id="Seg_1570" s="T77">n</ta>
            <ta e="T79" id="Seg_1571" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1572" s="T79">dem</ta>
            <ta e="T81" id="Seg_1573" s="T80">n</ta>
            <ta e="T82" id="Seg_1574" s="T81">adj</ta>
            <ta e="T83" id="Seg_1575" s="T82">conj</ta>
            <ta e="T84" id="Seg_1576" s="T83">adj</ta>
            <ta e="T85" id="Seg_1577" s="T84">n</ta>
            <ta e="T86" id="Seg_1578" s="T85">n</ta>
            <ta e="T87" id="Seg_1579" s="T86">conj</ta>
            <ta e="T88" id="Seg_1580" s="T87">n</ta>
            <ta e="T89" id="Seg_1581" s="T88">pp</ta>
            <ta e="T90" id="Seg_1582" s="T89">v</ta>
            <ta e="T91" id="Seg_1583" s="T90">conj</ta>
            <ta e="T92" id="Seg_1584" s="T91">v</ta>
            <ta e="T93" id="Seg_1585" s="T92">n</ta>
            <ta e="T94" id="Seg_1586" s="T93">quant</ta>
            <ta e="T95" id="Seg_1587" s="T94">adj</ta>
            <ta e="T96" id="Seg_1588" s="T95">n</ta>
            <ta e="T97" id="Seg_1589" s="T96">adj</ta>
            <ta e="T98" id="Seg_1590" s="T97">n</ta>
            <ta e="T99" id="Seg_1591" s="T98">n</ta>
            <ta e="T100" id="Seg_1592" s="T99">n</ta>
            <ta e="T101" id="Seg_1593" s="T100">n</ta>
            <ta e="T102" id="Seg_1594" s="T101">n</ta>
            <ta e="T103" id="Seg_1595" s="T102">n</ta>
            <ta e="T104" id="Seg_1596" s="T103">n</ta>
            <ta e="T105" id="Seg_1597" s="T104">n</ta>
            <ta e="T106" id="Seg_1598" s="T105">n</ta>
            <ta e="T107" id="Seg_1599" s="T106">n</ta>
            <ta e="T108" id="Seg_1600" s="T107">adj</ta>
            <ta e="T109" id="Seg_1601" s="T108">adj</ta>
            <ta e="T110" id="Seg_1602" s="T109">n</ta>
            <ta e="T111" id="Seg_1603" s="T110">preverb</ta>
            <ta e="T112" id="Seg_1604" s="T111">adv</ta>
            <ta e="T113" id="Seg_1605" s="T112">v</ta>
            <ta e="T114" id="Seg_1606" s="T113">v</ta>
            <ta e="T115" id="Seg_1607" s="T114">v</ta>
            <ta e="T116" id="Seg_1608" s="T115">conj</ta>
            <ta e="T117" id="Seg_1609" s="T116">n</ta>
            <ta e="T118" id="Seg_1610" s="T117">pp</ta>
            <ta e="T119" id="Seg_1611" s="T118">v</ta>
            <ta e="T120" id="Seg_1612" s="T119">n</ta>
            <ta e="T121" id="Seg_1613" s="T120">v</ta>
            <ta e="T122" id="Seg_1614" s="T121">v</ta>
            <ta e="T123" id="Seg_1615" s="T122">v</ta>
            <ta e="T124" id="Seg_1616" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1617" s="T124">v</ta>
            <ta e="T126" id="Seg_1618" s="T125">quant</ta>
            <ta e="T127" id="Seg_1619" s="T126">adj</ta>
            <ta e="T129" id="Seg_1620" s="T127">n</ta>
            <ta e="T130" id="Seg_1621" s="T129">ptcp</ta>
            <ta e="T131" id="Seg_1622" s="T130">n</ta>
            <ta e="T132" id="Seg_1623" s="T131">num</ta>
            <ta e="T133" id="Seg_1624" s="T132">n</ta>
            <ta e="T134" id="Seg_1625" s="T133">v</ta>
            <ta e="T135" id="Seg_1626" s="T134">v</ta>
            <ta e="T136" id="Seg_1627" s="T135">n</ta>
            <ta e="T137" id="Seg_1628" s="T136">adv</ta>
            <ta e="T138" id="Seg_1629" s="T137">v</ta>
            <ta e="T139" id="Seg_1630" s="T138">interj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1631" s="T0">np:Th</ta>
            <ta e="T2" id="Seg_1632" s="T1">0.1.h:A</ta>
            <ta e="T4" id="Seg_1633" s="T3">np:G</ta>
            <ta e="T6" id="Seg_1634" s="T5">pro.h:R</ta>
            <ta e="T8" id="Seg_1635" s="T7">np:Time</ta>
            <ta e="T10" id="Seg_1636" s="T9">np.h:A</ta>
            <ta e="T11" id="Seg_1637" s="T10">pro.h:Th</ta>
            <ta e="T12" id="Seg_1638" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_1639" s="T13">pro.h:A</ta>
            <ta e="T15" id="Seg_1640" s="T14">adv:L</ta>
            <ta e="T16" id="Seg_1641" s="T15">np:P</ta>
            <ta e="T24" id="Seg_1642" s="T23">np:P</ta>
            <ta e="T27" id="Seg_1643" s="T26">0.3.h:A</ta>
            <ta e="T29" id="Seg_1644" s="T28">adv:L</ta>
            <ta e="T31" id="Seg_1645" s="T30">np:Th</ta>
            <ta e="T33" id="Seg_1646" s="T32">pro:L</ta>
            <ta e="T35" id="Seg_1647" s="T34">pro.h:A</ta>
            <ta e="T37" id="Seg_1648" s="T36">pro.h:Poss</ta>
            <ta e="T39" id="Seg_1649" s="T38">np.h:Th</ta>
            <ta e="T41" id="Seg_1650" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_1651" s="T41">np:G</ta>
            <ta e="T43" id="Seg_1652" s="T42">np:Th</ta>
            <ta e="T55" id="Seg_1653" s="T54">np:L</ta>
            <ta e="T58" id="Seg_1654" s="T57">np:Th</ta>
            <ta e="T62" id="Seg_1655" s="T61">pp:L</ta>
            <ta e="T69" id="Seg_1656" s="T68">np:L</ta>
            <ta e="T71" id="Seg_1657" s="T70">pro:L</ta>
            <ta e="T72" id="Seg_1658" s="T71">np:L</ta>
            <ta e="T73" id="Seg_1659" s="T72">0.3:Th</ta>
            <ta e="T75" id="Seg_1660" s="T74">np:Ins</ta>
            <ta e="T77" id="Seg_1661" s="T76">np:Ins</ta>
            <ta e="T81" id="Seg_1662" s="T80">np:L</ta>
            <ta e="T85" id="Seg_1663" s="T84">np:L</ta>
            <ta e="T86" id="Seg_1664" s="T85">pp:L</ta>
            <ta e="T88" id="Seg_1665" s="T87">pp:L</ta>
            <ta e="T90" id="Seg_1666" s="T89">0.3:P</ta>
            <ta e="T98" id="Seg_1667" s="T97">np:Th</ta>
            <ta e="T99" id="Seg_1668" s="T98">np:Th</ta>
            <ta e="T100" id="Seg_1669" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_1670" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_1671" s="T101">np:Th</ta>
            <ta e="T103" id="Seg_1672" s="T102">np:Th</ta>
            <ta e="T104" id="Seg_1673" s="T103">np:Th</ta>
            <ta e="T105" id="Seg_1674" s="T104">np:Th</ta>
            <ta e="T107" id="Seg_1675" s="T106">np:Th</ta>
            <ta e="T110" id="Seg_1676" s="T109">np:A</ta>
            <ta e="T111" id="Seg_1677" s="T110">adv:G</ta>
            <ta e="T112" id="Seg_1678" s="T111">adv:G</ta>
            <ta e="T117" id="Seg_1679" s="T116">pp:Path</ta>
            <ta e="T119" id="Seg_1680" s="T118">0.3:A</ta>
            <ta e="T120" id="Seg_1681" s="T119">0.1.h:Poss np:P</ta>
            <ta e="T129" id="Seg_1682" s="T127">np:A</ta>
            <ta e="T131" id="Seg_1683" s="T130">np.h:A</ta>
            <ta e="T133" id="Seg_1684" s="T132">np:Ins</ta>
            <ta e="T135" id="Seg_1685" s="T134">np:Th 0.3.h:Poss</ta>
            <ta e="T137" id="Seg_1686" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_1687" s="T137">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1688" s="T1">0.1.h:S v:pred</ta>
            <ta e="T5" id="Seg_1689" s="T4">v:pred</ta>
            <ta e="T10" id="Seg_1690" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_1691" s="T10">pro.h:O</ta>
            <ta e="T12" id="Seg_1692" s="T11">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_1693" s="T13">pro.h:S</ta>
            <ta e="T16" id="Seg_1694" s="T15">np:O</ta>
            <ta e="T17" id="Seg_1695" s="T16">v:pred</ta>
            <ta e="T24" id="Seg_1696" s="T23">np:O</ta>
            <ta e="T26" id="Seg_1697" s="T25">s:adv</ta>
            <ta e="T27" id="Seg_1698" s="T26">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_1699" s="T30">np:S</ta>
            <ta e="T34" id="Seg_1700" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_1701" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_1702" s="T35">v:pred</ta>
            <ta e="T39" id="Seg_1703" s="T38">np.h:O</ta>
            <ta e="T41" id="Seg_1704" s="T40">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_1705" s="T42">np:S</ta>
            <ta e="T46" id="Seg_1706" s="T45">n:pred</ta>
            <ta e="T58" id="Seg_1707" s="T57">np:S</ta>
            <ta e="T64" id="Seg_1708" s="T63">cop</ta>
            <ta e="T68" id="Seg_1709" s="T67">n:pred</ta>
            <ta e="T73" id="Seg_1710" s="T72">0.3:S v:pred</ta>
            <ta e="T90" id="Seg_1711" s="T89">0.3:S v:pred</ta>
            <ta e="T92" id="Seg_1712" s="T91">v:pred</ta>
            <ta e="T98" id="Seg_1713" s="T97">np:S</ta>
            <ta e="T99" id="Seg_1714" s="T98">np:S</ta>
            <ta e="T100" id="Seg_1715" s="T99">np:S</ta>
            <ta e="T101" id="Seg_1716" s="T100">np:S</ta>
            <ta e="T102" id="Seg_1717" s="T101">np:S</ta>
            <ta e="T103" id="Seg_1718" s="T102">np:S</ta>
            <ta e="T104" id="Seg_1719" s="T103">np:S</ta>
            <ta e="T105" id="Seg_1720" s="T104">np:S</ta>
            <ta e="T107" id="Seg_1721" s="T106">np:S</ta>
            <ta e="T110" id="Seg_1722" s="T109">np:S</ta>
            <ta e="T113" id="Seg_1723" s="T112">v:pred</ta>
            <ta e="T119" id="Seg_1724" s="T118">0.3:S v:pred</ta>
            <ta e="T120" id="Seg_1725" s="T119">np:S</ta>
            <ta e="T121" id="Seg_1726" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_1727" s="T121">v:pred</ta>
            <ta e="T129" id="Seg_1728" s="T127">np:S</ta>
            <ta e="T131" id="Seg_1729" s="T130">np.h:S</ta>
            <ta e="T134" id="Seg_1730" s="T133">v:pred</ta>
            <ta e="T135" id="Seg_1731" s="T134">np:O</ta>
            <ta e="T138" id="Seg_1732" s="T137">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_1733" s="T0">RUS:cult</ta>
            <ta e="T4" id="Seg_1734" s="T3">RUS:cult</ta>
            <ta e="T13" id="Seg_1735" s="T12">RUS:cult</ta>
            <ta e="T28" id="Seg_1736" s="T27">RUS:gram</ta>
            <ta e="T40" id="Seg_1737" s="T39">RUS:gram</ta>
            <ta e="T42" id="Seg_1738" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_1739" s="T42">RUS:cult</ta>
            <ta e="T53" id="Seg_1740" s="T52">RUS:cult</ta>
            <ta e="T56" id="Seg_1741" s="T55">RUS:cult</ta>
            <ta e="T60" id="Seg_1742" s="T59">RUS:cult</ta>
            <ta e="T62" id="Seg_1743" s="T61">RUS:cult</ta>
            <ta e="T76" id="Seg_1744" s="T75">RUS:gram</ta>
            <ta e="T79" id="Seg_1745" s="T78">RUS:mod</ta>
            <ta e="T120" id="Seg_1746" s="T119">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1" id="Seg_1747" s="T0">Льгов.</ta>
            <ta e="T13" id="Seg_1748" s="T1">"Пойдёмте во Льгов", — сказал мне однажды человек по имени Ермолай.</ta>
            <ta e="T18" id="Seg_1749" s="T13">"Мы там много уток настреляем.</ta>
            <ta e="T34" id="Seg_1750" s="T18">Для хорошего охотника не много усилий надо, чтобы убить дикую утку, но здесь другой птицы нет". </ta>
            <ta e="T42" id="Seg_1751" s="T34">Я послушался своего охотника и отправился во Льгов.</ta>
            <ta e="T56" id="Seg_1752" s="T42">Льгов — небольшой городок с древней каменной церковью и двумя мельницами на болотистой речке Росоти.</ta>
            <ta e="T68" id="Seg_1753" s="T56">Эта речка за пять километров от Льгова превращается в широкую болотистую речку (/мелководное озеро).</ta>
            <ta e="T78" id="Seg_1754" s="T68">По краям и кое-где посредине она заросла болотной травой и камышом. </ta>
            <ta e="T107" id="Seg_1755" s="T78">На этой речке в заводях и там, где нет течения, в траве и в осоке выводилось и жило очень много всевозможных пород уток: чирков, турпанов, гагар, нырков, шилохвосток, связей, белокрылых уток, черниц.</ta>
            <ta e="T119" id="Seg_1756" s="T107">Небольшие стаи уток перелетали взад и вперёд и летали над водой.</ta>
            <ta e="T139" id="Seg_1757" s="T119">Выстрелит ружьё, то поднимались такие стаи уток, что охотник одной рукой хватался за шапку (/за голову) и говорил: "Фу-у-у!"</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_1758" s="T0">Lgov.</ta>
            <ta e="T13" id="Seg_1759" s="T1">"Let's go to Lgov", — said to me once a man called Yermolay.</ta>
            <ta e="T18" id="Seg_1760" s="T13">"We will shoot many ducks there.</ta>
            <ta e="T34" id="Seg_1761" s="T18">It's not hard for a good hunter to kill a wild duck, but there is no other bird here".</ta>
            <ta e="T42" id="Seg_1762" s="T34">I've listened to my hunter and went to Lgov.</ta>
            <ta e="T56" id="Seg_1763" s="T42">Lgov — a small town with an ancient stone church and two mills on the small river Rosota.</ta>
            <ta e="T68" id="Seg_1764" s="T56">Five kilometers from Lgov this small river turnes to a broad swampy river (/shallow lake).</ta>
            <ta e="T78" id="Seg_1765" s="T68">On the river banks and here and there in the middle it is overgrown with grass and reed.</ta>
            <ta e="T107" id="Seg_1766" s="T78">On this river in the backwater where there is no stream, in the grass and in the sedge hatched and lived a lot of different breeds of ducks: teals, scoters, divers, pochards, pintails, widgeons, white-winged ducks, tufted ducks.</ta>
            <ta e="T119" id="Seg_1767" s="T107">Small duck flocks flew back and forth over the water.</ta>
            <ta e="T139" id="Seg_1768" s="T119">When a rifle shot, giant duck flocks flew up, so that the hunter grabbed his hat (/head) and said: "Whoa!"</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_1769" s="T0">Lgov.</ta>
            <ta e="T13" id="Seg_1770" s="T1">"Lass uns nach Lgov fahren", — sagte mir einmal ein Mann namens Jermolay.</ta>
            <ta e="T18" id="Seg_1771" s="T13">"Wir werden dort viele Enten schießen.</ta>
            <ta e="T34" id="Seg_1772" s="T18">Für einen guten Jäger ist es nicht schwierig, eine wilde Ente zu schießen, aber hier gibt es keine anderen Vögel."</ta>
            <ta e="T42" id="Seg_1773" s="T34">Ich hörte auf meinen Jäger und fuhr nach Lgov.</ta>
            <ta e="T56" id="Seg_1774" s="T42">Lgov — eine kleine Stadt mit einer Steinkirche und zwei Mühlen am kleinen sumpfigen Fluss Rosota.</ta>
            <ta e="T68" id="Seg_1775" s="T56">Fünf Kilometer entfernt von Lgov wird dieser kleine Fluss zu einem breiten sumpfigen Fluss (/seichten See).</ta>
            <ta e="T78" id="Seg_1776" s="T68">An den Ufern und hier und dort in der Mitte ist er durch Gras und Schilf verwildert.</ta>
            <ta e="T107" id="Seg_1777" s="T78">An dem Fluss im Stillwasser und an Orten ohne Strömung, im Gras und im Riedgras schlüpften und lebten viele Entenarten: Krickenten, Samtenten, Alke, Tauchenten, Spießenten, Pfeifenten, weißflügelige Enten, Reiherenten.</ta>
            <ta e="T119" id="Seg_1778" s="T107">Kleine Entenschwärme flogen über das Wasser hin und her.</ta>
            <ta e="T139" id="Seg_1779" s="T119">Als das Gewehr schoss, flogen riesige Entenschwärme auf, sodass der Jäger nach seiner Mütze (/seinem Kopf) griff und sagte: "Oha!"</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T13" id="Seg_1780" s="T1">Поедемте во Льгов сказал мне однажды один человек, звали его Ермолай.</ta>
            <ta e="T18" id="Seg_1781" s="T13">мы там уток настреляем много.</ta>
            <ta e="T34" id="Seg_1782" s="T18">для хорошего охотника дикую утку не много усилий усилий убить но (так как) нигде в стороне [в другом месте] другой птицы нет</ta>
            <ta e="T42" id="Seg_1783" s="T34">я послушался своего охотника и отправился во Льгов</ta>
            <ta e="T56" id="Seg_1784" s="T42">не большой городок с древней каменной церковью и двумя мельницами на болотистой речке росоте.</ta>
            <ta e="T68" id="Seg_1785" s="T56">эта речка за 5 верст от Льгова превращается в широкую болотистую [орловую] (озеро) речку.</ta>
            <ta e="T78" id="Seg_1786" s="T68">по краям [на берегу] и кое-где посредине заросла [растет] многой травой и болотной травой (камыш). </ta>
            <ta e="T107" id="Seg_1787" s="T78">На этой-то речке в заводях (где течения нет) и затишье (где течения нет) в траве в болот. траве выводилось [выпаривалось] держалось [жило] сильно много всевозможных пород уток: чирк, турпаны, гагары, нырки шилохвосты связи белокрылая [белобокая], черница</ta>
            <ta e="T119" id="Seg_1788" s="T107">небольшие уток стаи взад вперед перелетывали над водой летали.</ta>
            <ta e="T139" id="Seg_1789" s="T119">ружье выстрелит мимо поднимались такие много утиных туч (стаи) охотник одной рукой хватался за шапку говорил: фууу</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T34" id="Seg_1790" s="T18">[OSV:] "qumɨ čʼoːtɨ" has been edited into "qumɨt čʼɔːtɨ". [BrM:] Unclear syntax of the first clause/</ta>
            <ta e="T42" id="Seg_1791" s="T34">[BrM:] Unclear usage of 3SG in "qəssi", according to translation it shoul be 1SG.</ta>
            <ta e="T78" id="Seg_1792" s="T68">[OSV:] unclear adverbial form "kuttoːqɨn" (where there-ADV.LOC).</ta>
            <ta e="T107" id="Seg_1793" s="T78">[OSV:] 1) "üt šiːpa" - "a tufted duck"; 2) "kunmɔːntɨ" - "how far". [AAV:] "в заводях (где течения [crossed out: «ветра»] нет)" </ta>
            <ta e="T119" id="Seg_1794" s="T107">[OSV:] 1) The speaker alternates between Present and Past tenses; 2) the postpositional form in "ütə pɔːrɨ-m" has been edited into "ütə pɔːrɨ-n" (water above-ADV.LOC). </ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
