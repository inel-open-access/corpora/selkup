<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>PVD_1964_Tyssyja_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">PVD_1964_Tyssyja_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">186</ud-information>
            <ud-information attribute-name="# HIAT:w">138</ud-information>
            <ud-information attribute-name="# e">138</ud-information>
            <ud-information attribute-name="# HIAT:u">31</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PVD">
            <abbreviation>PVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PVD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T139" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Okkɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">qum</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ilɨss</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">täbɨn</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">nimdə</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">Тɨssija</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_24" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">Täbnan</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">okkɨr</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">iːt</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">okkɨr</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">net</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_43" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">Kəlɨlʼla</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">mütsä</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">tüːwattə</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_55" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">Mütse</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">tʼüattə</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">kəlolla</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_67" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">Tebla</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">qwatpattɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">Тɨssijam</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_79" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">Täbɨn</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">iːmdə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_90" n="HIAT:w" s="T24">nämdə</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">maːttɨ</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">omdɨlǯɨlʼe</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">qwädipattə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_103" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">Okkɨr</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">aːtʼam</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">qwɛdʼibattə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_113" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_115" n="HIAT:w" s="T31">konar</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_118" n="HIAT:w" s="T32">tarla</ts>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_123" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_125" n="HIAT:w" s="T33">Na</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">iːw</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_132" n="HIAT:w" s="T35">qəlɨlla</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">qwannattə</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">ille</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">übəran</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_144" n="HIAT:ip">(</nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">nildʼzʼin</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">warkəlʼe</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">übəran</ts>
                  <nts id="Seg_153" n="HIAT:ip">)</nts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_157" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">Jolkazʼe</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">meːbat</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">maːdɨm</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_169" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">Qaptʼe</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">okkɨr</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">qarʼemɨɣɨn</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">nanʼätdänä</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">nɨldʼzʼin</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">tʼärɨn</ts>
                  <nts id="Seg_187" n="HIAT:ip">:</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">“</nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">Tan</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">amdaq</ts>
                  <nts id="Seg_195" n="HIAT:ip">,</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">a</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_201" n="HIAT:w" s="T54">man</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">qwaǯan</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">kuropaškalam</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">mannɨbugu</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip">”</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_215" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">Qwannɨn</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Nanʼät</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">tʼüːrlʼe</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">qalɨn</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_233" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">Qwannɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_239" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">Tʼäkgula</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">melʼe</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">taːdərətdɨt</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_251" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">Na</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">dʼel</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_259" n="HIAT:w" s="T68">nagur</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_262" n="HIAT:w" s="T69">tʼäkku</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_265" n="HIAT:w" s="T70">mematdɨt</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_269" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">Kəssə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">tüːa</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_276" n="HIAT:ip">(</nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">moɣunä</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">tüwa</ts>
                  <nts id="Seg_282" n="HIAT:ip">)</nts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_286" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">Sədəmdʼettɨ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">dʼel</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">qarʼemɨɣɨn</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">aj</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">qwannɨn</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">tʼäkgulaɣɨtdə</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_307" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">Tüa</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">okkɨr</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">kurapaška</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_318" n="HIAT:w" s="T84">qwatpɨtdɨt</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_322" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_324" n="HIAT:w" s="T85">Aj</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_327" n="HIAT:w" s="T86">nagur</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">tʼäkgu</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">mematdɨt</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_337" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_339" n="HIAT:w" s="T89">Aj</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_342" n="HIAT:w" s="T90">köse</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_345" n="HIAT:w" s="T91">tüwa</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_349" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">Nanetdɨzʼe</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">täbɨstaɣə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">sədəqun</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_360" n="HIAT:w" s="T95">kurapaškan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_363" n="HIAT:w" s="T96">plʼäka</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_366" n="HIAT:w" s="T97">ammɨtdədɨ</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_370" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_372" n="HIAT:w" s="T98">A</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_375" n="HIAT:w" s="T99">plʼäkamdə</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_378" n="HIAT:w" s="T100">pennɨtɨ</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_380" n="HIAT:ip">–</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_383" n="HIAT:w" s="T101">qaptʼe</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_386" n="HIAT:w" s="T102">štobɨ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_389" n="HIAT:w" s="T103">amgu</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_393" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">Qardʼen</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_398" n="HIAT:w" s="T105">aj</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_401" n="HIAT:w" s="T106">qwannɨn</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_404" n="HIAT:w" s="T107">otdə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_407" n="HIAT:w" s="T108">tʼäkgulaɣɨtdə</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_411" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_413" n="HIAT:w" s="T109">Tüan</ts>
                  <nts id="Seg_414" n="HIAT:ip">:</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_417" n="HIAT:w" s="T110">sədə</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_420" n="HIAT:w" s="T111">tʼäkgu</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_423" n="HIAT:w" s="T112">pɨkgɨlamɨt</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_427" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_429" n="HIAT:w" s="T113">Tawtʼe</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_432" n="HIAT:w" s="T114">tüa</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_435" n="HIAT:w" s="T115">adun</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_438" n="HIAT:w" s="T116">sədə</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">kuropaška</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_444" n="HIAT:w" s="T118">qwatpɨdɨt</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_448" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_450" n="HIAT:w" s="T119">Apʼätʼ</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_453" n="HIAT:w" s="T120">nagur</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_456" n="HIAT:w" s="T121">tʼäkgu</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_459" n="HIAT:w" s="T122">memmɨdɨt</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_463" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_465" n="HIAT:w" s="T123">Kösʼsʼe</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_468" n="HIAT:w" s="T124">tüwa</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_472" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_474" n="HIAT:w" s="T125">Okkɨr</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_477" n="HIAT:w" s="T126">kurapaška</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_480" n="HIAT:w" s="T127">pläkat</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_483" n="HIAT:w" s="T128">amdɨdə</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_487" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_489" n="HIAT:w" s="T129">Sʼeqqaːɣɨ</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_493" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_495" n="HIAT:w" s="T130">Qarʼemɨɣɨn</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_498" n="HIAT:w" s="T131">aj</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_501" n="HIAT:w" s="T132">qwannɨn</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_504" n="HIAT:w" s="T133">tʼäkgulagɨtdə</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_508" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_510" n="HIAT:w" s="T134">Tüa</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_513" n="HIAT:w" s="T135">aːdun</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_516" n="HIAT:w" s="T136">nagur</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_519" n="HIAT:w" s="T137">tʼäqgu</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_522" n="HIAT:w" s="T138">pɨqgɨlɨmɨt</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T139" id="Seg_525" n="sc" s="T1">
               <ts e="T2" id="Seg_527" n="e" s="T1">Okkɨr </ts>
               <ts e="T3" id="Seg_529" n="e" s="T2">qum </ts>
               <ts e="T4" id="Seg_531" n="e" s="T3">ilɨss, </ts>
               <ts e="T5" id="Seg_533" n="e" s="T4">täbɨn </ts>
               <ts e="T6" id="Seg_535" n="e" s="T5">nimdə </ts>
               <ts e="T7" id="Seg_537" n="e" s="T6">Тɨssija. </ts>
               <ts e="T8" id="Seg_539" n="e" s="T7">Täbnan </ts>
               <ts e="T9" id="Seg_541" n="e" s="T8">okkɨr </ts>
               <ts e="T10" id="Seg_543" n="e" s="T9">iːt, </ts>
               <ts e="T11" id="Seg_545" n="e" s="T10">okkɨr </ts>
               <ts e="T12" id="Seg_547" n="e" s="T11">net. </ts>
               <ts e="T13" id="Seg_549" n="e" s="T12">Kəlɨlʼla </ts>
               <ts e="T14" id="Seg_551" n="e" s="T13">mütsä </ts>
               <ts e="T15" id="Seg_553" n="e" s="T14">tüːwattə. </ts>
               <ts e="T16" id="Seg_555" n="e" s="T15">Mütse </ts>
               <ts e="T17" id="Seg_557" n="e" s="T16">tʼüattə </ts>
               <ts e="T18" id="Seg_559" n="e" s="T17">kəlolla. </ts>
               <ts e="T19" id="Seg_561" n="e" s="T18">Tebla </ts>
               <ts e="T20" id="Seg_563" n="e" s="T19">qwatpattɨ </ts>
               <ts e="T21" id="Seg_565" n="e" s="T20">Тɨssijam. </ts>
               <ts e="T22" id="Seg_567" n="e" s="T21">Täbɨn </ts>
               <ts e="T23" id="Seg_569" n="e" s="T22">iːmdə </ts>
               <ts e="T24" id="Seg_571" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_573" n="e" s="T24">nämdə </ts>
               <ts e="T26" id="Seg_575" n="e" s="T25">maːttɨ </ts>
               <ts e="T27" id="Seg_577" n="e" s="T26">omdɨlǯɨlʼe </ts>
               <ts e="T28" id="Seg_579" n="e" s="T27">qwädipattə. </ts>
               <ts e="T29" id="Seg_581" n="e" s="T28">Okkɨr </ts>
               <ts e="T30" id="Seg_583" n="e" s="T29">aːtʼam </ts>
               <ts e="T31" id="Seg_585" n="e" s="T30">qwɛdʼibattə </ts>
               <ts e="T32" id="Seg_587" n="e" s="T31">(konar </ts>
               <ts e="T33" id="Seg_589" n="e" s="T32">tarla). </ts>
               <ts e="T34" id="Seg_591" n="e" s="T33">Na </ts>
               <ts e="T35" id="Seg_593" n="e" s="T34">iːw, </ts>
               <ts e="T36" id="Seg_595" n="e" s="T35">qəlɨlla </ts>
               <ts e="T37" id="Seg_597" n="e" s="T36">qwannattə, </ts>
               <ts e="T38" id="Seg_599" n="e" s="T37">ille </ts>
               <ts e="T39" id="Seg_601" n="e" s="T38">übəran </ts>
               <ts e="T40" id="Seg_603" n="e" s="T39">(nildʼzʼin </ts>
               <ts e="T41" id="Seg_605" n="e" s="T40">warkəlʼe </ts>
               <ts e="T42" id="Seg_607" n="e" s="T41">übəran). </ts>
               <ts e="T43" id="Seg_609" n="e" s="T42">Jolkazʼe </ts>
               <ts e="T44" id="Seg_611" n="e" s="T43">meːbat </ts>
               <ts e="T45" id="Seg_613" n="e" s="T44">maːdɨm. </ts>
               <ts e="T46" id="Seg_615" n="e" s="T45">Qaptʼe </ts>
               <ts e="T47" id="Seg_617" n="e" s="T46">okkɨr </ts>
               <ts e="T48" id="Seg_619" n="e" s="T47">qarʼemɨɣɨn </ts>
               <ts e="T49" id="Seg_621" n="e" s="T48">nanʼätdänä </ts>
               <ts e="T50" id="Seg_623" n="e" s="T49">nɨldʼzʼin </ts>
               <ts e="T51" id="Seg_625" n="e" s="T50">tʼärɨn: </ts>
               <ts e="T52" id="Seg_627" n="e" s="T51">“Tan </ts>
               <ts e="T53" id="Seg_629" n="e" s="T52">amdaq, </ts>
               <ts e="T54" id="Seg_631" n="e" s="T53">a </ts>
               <ts e="T55" id="Seg_633" n="e" s="T54">man </ts>
               <ts e="T56" id="Seg_635" n="e" s="T55">qwaǯan </ts>
               <ts e="T57" id="Seg_637" n="e" s="T56">kuropaškalam </ts>
               <ts e="T58" id="Seg_639" n="e" s="T57">mannɨbugu.” </ts>
               <ts e="T59" id="Seg_641" n="e" s="T58">Qwannɨn. </ts>
               <ts e="T60" id="Seg_643" n="e" s="T59">Nanʼät </ts>
               <ts e="T61" id="Seg_645" n="e" s="T60">tʼüːrlʼe </ts>
               <ts e="T62" id="Seg_647" n="e" s="T61">qalɨn. </ts>
               <ts e="T63" id="Seg_649" n="e" s="T62">Qwannɨn. </ts>
               <ts e="T64" id="Seg_651" n="e" s="T63">Tʼäkgula </ts>
               <ts e="T65" id="Seg_653" n="e" s="T64">melʼe </ts>
               <ts e="T66" id="Seg_655" n="e" s="T65">taːdərətdɨt. </ts>
               <ts e="T67" id="Seg_657" n="e" s="T66">Na </ts>
               <ts e="T68" id="Seg_659" n="e" s="T67">dʼel </ts>
               <ts e="T69" id="Seg_661" n="e" s="T68">nagur </ts>
               <ts e="T70" id="Seg_663" n="e" s="T69">tʼäkku </ts>
               <ts e="T71" id="Seg_665" n="e" s="T70">mematdɨt. </ts>
               <ts e="T72" id="Seg_667" n="e" s="T71">Kəssə </ts>
               <ts e="T73" id="Seg_669" n="e" s="T72">tüːa </ts>
               <ts e="T74" id="Seg_671" n="e" s="T73">(moɣunä </ts>
               <ts e="T75" id="Seg_673" n="e" s="T74">tüwa). </ts>
               <ts e="T76" id="Seg_675" n="e" s="T75">Sədəmdʼettɨ </ts>
               <ts e="T77" id="Seg_677" n="e" s="T76">dʼel </ts>
               <ts e="T78" id="Seg_679" n="e" s="T77">qarʼemɨɣɨn </ts>
               <ts e="T79" id="Seg_681" n="e" s="T78">aj </ts>
               <ts e="T80" id="Seg_683" n="e" s="T79">qwannɨn </ts>
               <ts e="T81" id="Seg_685" n="e" s="T80">tʼäkgulaɣɨtdə. </ts>
               <ts e="T82" id="Seg_687" n="e" s="T81">Tüa </ts>
               <ts e="T83" id="Seg_689" n="e" s="T82">okkɨr </ts>
               <ts e="T84" id="Seg_691" n="e" s="T83">kurapaška </ts>
               <ts e="T85" id="Seg_693" n="e" s="T84">qwatpɨtdɨt. </ts>
               <ts e="T86" id="Seg_695" n="e" s="T85">Aj </ts>
               <ts e="T87" id="Seg_697" n="e" s="T86">nagur </ts>
               <ts e="T88" id="Seg_699" n="e" s="T87">tʼäkgu </ts>
               <ts e="T89" id="Seg_701" n="e" s="T88">mematdɨt. </ts>
               <ts e="T90" id="Seg_703" n="e" s="T89">Aj </ts>
               <ts e="T91" id="Seg_705" n="e" s="T90">köse </ts>
               <ts e="T92" id="Seg_707" n="e" s="T91">tüwa. </ts>
               <ts e="T93" id="Seg_709" n="e" s="T92">Nanetdɨzʼe </ts>
               <ts e="T94" id="Seg_711" n="e" s="T93">täbɨstaɣə </ts>
               <ts e="T95" id="Seg_713" n="e" s="T94">sədəqun </ts>
               <ts e="T96" id="Seg_715" n="e" s="T95">kurapaškan </ts>
               <ts e="T97" id="Seg_717" n="e" s="T96">plʼäka </ts>
               <ts e="T98" id="Seg_719" n="e" s="T97">ammɨtdədɨ. </ts>
               <ts e="T99" id="Seg_721" n="e" s="T98">A </ts>
               <ts e="T100" id="Seg_723" n="e" s="T99">plʼäkamdə </ts>
               <ts e="T101" id="Seg_725" n="e" s="T100">pennɨtɨ – </ts>
               <ts e="T102" id="Seg_727" n="e" s="T101">qaptʼe </ts>
               <ts e="T103" id="Seg_729" n="e" s="T102">štobɨ </ts>
               <ts e="T104" id="Seg_731" n="e" s="T103">amgu. </ts>
               <ts e="T105" id="Seg_733" n="e" s="T104">Qardʼen </ts>
               <ts e="T106" id="Seg_735" n="e" s="T105">aj </ts>
               <ts e="T107" id="Seg_737" n="e" s="T106">qwannɨn </ts>
               <ts e="T108" id="Seg_739" n="e" s="T107">otdə </ts>
               <ts e="T109" id="Seg_741" n="e" s="T108">tʼäkgulaɣɨtdə. </ts>
               <ts e="T110" id="Seg_743" n="e" s="T109">Tüan: </ts>
               <ts e="T111" id="Seg_745" n="e" s="T110">sədə </ts>
               <ts e="T112" id="Seg_747" n="e" s="T111">tʼäkgu </ts>
               <ts e="T113" id="Seg_749" n="e" s="T112">pɨkgɨlamɨt. </ts>
               <ts e="T114" id="Seg_751" n="e" s="T113">Tawtʼe </ts>
               <ts e="T115" id="Seg_753" n="e" s="T114">tüa </ts>
               <ts e="T116" id="Seg_755" n="e" s="T115">adun </ts>
               <ts e="T117" id="Seg_757" n="e" s="T116">sədə </ts>
               <ts e="T118" id="Seg_759" n="e" s="T117">kuropaška </ts>
               <ts e="T119" id="Seg_761" n="e" s="T118">qwatpɨdɨt. </ts>
               <ts e="T120" id="Seg_763" n="e" s="T119">Apʼätʼ </ts>
               <ts e="T121" id="Seg_765" n="e" s="T120">nagur </ts>
               <ts e="T122" id="Seg_767" n="e" s="T121">tʼäkgu </ts>
               <ts e="T123" id="Seg_769" n="e" s="T122">memmɨdɨt. </ts>
               <ts e="T124" id="Seg_771" n="e" s="T123">Kösʼsʼe </ts>
               <ts e="T125" id="Seg_773" n="e" s="T124">tüwa. </ts>
               <ts e="T126" id="Seg_775" n="e" s="T125">Okkɨr </ts>
               <ts e="T127" id="Seg_777" n="e" s="T126">kurapaška </ts>
               <ts e="T128" id="Seg_779" n="e" s="T127">pläkat </ts>
               <ts e="T129" id="Seg_781" n="e" s="T128">amdɨdə. </ts>
               <ts e="T130" id="Seg_783" n="e" s="T129">Sʼeqqaːɣɨ. </ts>
               <ts e="T131" id="Seg_785" n="e" s="T130">Qarʼemɨɣɨn </ts>
               <ts e="T132" id="Seg_787" n="e" s="T131">aj </ts>
               <ts e="T133" id="Seg_789" n="e" s="T132">qwannɨn </ts>
               <ts e="T134" id="Seg_791" n="e" s="T133">tʼäkgulagɨtdə. </ts>
               <ts e="T135" id="Seg_793" n="e" s="T134">Tüa </ts>
               <ts e="T136" id="Seg_795" n="e" s="T135">aːdun </ts>
               <ts e="T137" id="Seg_797" n="e" s="T136">nagur </ts>
               <ts e="T138" id="Seg_799" n="e" s="T137">tʼäqgu </ts>
               <ts e="T139" id="Seg_801" n="e" s="T138">pɨqgɨlɨmɨt. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_802" s="T1">PVD_1964_Tyssyja_transl.001 (001.001)</ta>
            <ta e="T12" id="Seg_803" s="T7">PVD_1964_Tyssyja_transl.002 (001.002)</ta>
            <ta e="T15" id="Seg_804" s="T12">PVD_1964_Tyssyja_transl.003 (001.003)</ta>
            <ta e="T18" id="Seg_805" s="T15">PVD_1964_Tyssyja_transl.004 (001.004)</ta>
            <ta e="T21" id="Seg_806" s="T18">PVD_1964_Tyssyja_transl.005 (001.005)</ta>
            <ta e="T28" id="Seg_807" s="T21">PVD_1964_Tyssyja_transl.006 (001.006)</ta>
            <ta e="T33" id="Seg_808" s="T28">PVD_1964_Tyssyja_transl.007 (001.007)</ta>
            <ta e="T42" id="Seg_809" s="T33">PVD_1964_Tyssyja_transl.008 (001.008)</ta>
            <ta e="T45" id="Seg_810" s="T42">PVD_1964_Tyssyja_transl.009 (001.009)</ta>
            <ta e="T58" id="Seg_811" s="T45">PVD_1964_Tyssyja_transl.010 (001.010)</ta>
            <ta e="T59" id="Seg_812" s="T58">PVD_1964_Tyssyja_transl.011 (001.011)</ta>
            <ta e="T62" id="Seg_813" s="T59">PVD_1964_Tyssyja_transl.012 (001.012)</ta>
            <ta e="T63" id="Seg_814" s="T62">PVD_1964_Tyssyja_transl.013 (001.013)</ta>
            <ta e="T66" id="Seg_815" s="T63">PVD_1964_Tyssyja_transl.014 (001.014)</ta>
            <ta e="T71" id="Seg_816" s="T66">PVD_1964_Tyssyja_transl.015 (001.015)</ta>
            <ta e="T75" id="Seg_817" s="T71">PVD_1964_Tyssyja_transl.016 (001.016)</ta>
            <ta e="T81" id="Seg_818" s="T75">PVD_1964_Tyssyja_transl.017 (001.017)</ta>
            <ta e="T85" id="Seg_819" s="T81">PVD_1964_Tyssyja_transl.018 (001.018)</ta>
            <ta e="T89" id="Seg_820" s="T85">PVD_1964_Tyssyja_transl.019 (001.019)</ta>
            <ta e="T92" id="Seg_821" s="T89">PVD_1964_Tyssyja_transl.020 (001.020)</ta>
            <ta e="T98" id="Seg_822" s="T92">PVD_1964_Tyssyja_transl.021 (001.021)</ta>
            <ta e="T104" id="Seg_823" s="T98">PVD_1964_Tyssyja_transl.022 (001.022)</ta>
            <ta e="T109" id="Seg_824" s="T104">PVD_1964_Tyssyja_transl.023 (001.023)</ta>
            <ta e="T113" id="Seg_825" s="T109">PVD_1964_Tyssyja_transl.024 (001.024)</ta>
            <ta e="T119" id="Seg_826" s="T113">PVD_1964_Tyssyja_transl.025 (001.025)</ta>
            <ta e="T123" id="Seg_827" s="T119">PVD_1964_Tyssyja_transl.026 (001.026)</ta>
            <ta e="T125" id="Seg_828" s="T123">PVD_1964_Tyssyja_transl.027 (001.027)</ta>
            <ta e="T129" id="Seg_829" s="T125">PVD_1964_Tyssyja_transl.028 (001.028)</ta>
            <ta e="T130" id="Seg_830" s="T129">PVD_1964_Tyssyja_transl.029 (001.029)</ta>
            <ta e="T134" id="Seg_831" s="T130">PVD_1964_Tyssyja_transl.030 (001.030)</ta>
            <ta e="T139" id="Seg_832" s="T134">PVD_1964_Tyssyja_transl.031 (001.031)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_833" s="T1">ок′кыр kум и′лысс, ′тӓбын ′нимдъ Тыссиjа.</ta>
            <ta e="T12" id="Seg_834" s="T7">′тӓбнан ок′кыр ӣт, оккыр ′не(ӓ)т.</ta>
            <ta e="T15" id="Seg_835" s="T12">къ̊lылʼла ′мӱтсӓ ′тӱ̄wаттъ.</ta>
            <ta e="T18" id="Seg_836" s="T15">(мӱтсе ′тʼӱаттъ ′къ̊lолла.)</ta>
            <ta e="T21" id="Seg_837" s="T18">теб′ла kwат′патты ′Тыссиjам.</ta>
            <ta e="T28" id="Seg_838" s="T21">′тӓбын ′ӣмдъ и ′нӓмдъ ′ма̄тты ‵омдыlджы′лʼе ′kwӓдипаттъ.</ta>
            <ta e="T33" id="Seg_839" s="T28">ок′кыр ′а̄тʼам ′kwɛдʼи(е)баттъ (ко′нар тарла).</ta>
            <ta e="T42" id="Seg_840" s="T33">на ′ӣw, ′kъ̊lыл′ла kwа′ннаттъ, иl′l(лʼ)е̨ ′ӱбъран (ниl′дʼзʼин ′варкълʼе ӱбъран).</ta>
            <ta e="T45" id="Seg_841" s="T42">′jолказʼе ′ме̄бат ′ма̄дым.</ta>
            <ta e="T58" id="Seg_842" s="T45">kап′тʼе оккыр kа′рʼемыɣын на′нʼӓтдӓнӓ ныl′дʼзʼин тʼӓрын: тан ′амдаk, а ман kwа′джан куро′пашкалам ′манныбу′гу.</ta>
            <ta e="T59" id="Seg_843" s="T58">′kwаннын.</ta>
            <ta e="T62" id="Seg_844" s="T59">на′нʼӓт ′тʼӱ̄рлʼе ′kалын.</ta>
            <ta e="T63" id="Seg_845" s="T62">kwаннын.</ta>
            <ta e="T66" id="Seg_846" s="T63">′тʼӓкгула ′мелʼе ′та̄дърътдыт.</ta>
            <ta e="T71" id="Seg_847" s="T66">′на дʼел ′нагур ′тʼӓкку ′мематдыт.</ta>
            <ta e="T75" id="Seg_848" s="T71">′къ̊ссъ ′тӱ̄а (моɣу′нӓ ′тӱwа).</ta>
            <ta e="T81" id="Seg_849" s="T75">‵съдъм′дʼетты д̂ʼел kа′рʼемыɣын ′ай kwа′ннын ′тʼӓкгу‵лаɣытдъ.</ta>
            <ta e="T85" id="Seg_850" s="T81">′тӱа оккыр курапашка ′kwатпытдыт.</ta>
            <ta e="T89" id="Seg_851" s="T85">′ай ′нагур ′тʼӓкг̂у ′мематдыт.</ta>
            <ta e="T92" id="Seg_852" s="T89">ай ′кӧсе ′тӱwа.</ta>
            <ta e="T98" id="Seg_853" s="T92">на′нетдызʼе ′тӓбыстаɣъ съдъ′k(ɣ)ун курапашкан плʼӓ′ка ′аммытдъды.</ta>
            <ta e="T104" id="Seg_854" s="T98">а плʼӓ′камдъ пенныты - kап′тʼе штобы ам′гу.</ta>
            <ta e="T109" id="Seg_855" s="T104">kар′дʼен ай kwа′ннын ′отдъ тʼӓкг̂улаɣытдъ.</ta>
            <ta e="T113" id="Seg_856" s="T109">′тӱан ′съдъ ′тʼӓкг(о)у ′пыкгыламыт.</ta>
            <ta e="T119" id="Seg_857" s="T113">таw(ф)′тʼе ′тӱ(w)а а′дун съдъ куро′пашка ′kwатпыдыт.</ta>
            <ta e="T123" id="Seg_858" s="T119">а′пʼӓтʼ ′нагур тʼӓкг̂у ′меммыдыт.</ta>
            <ta e="T125" id="Seg_859" s="T123">′кӧсʼсʼе ′тӱwа.</ta>
            <ta e="T129" id="Seg_860" s="T125">ок′кыр кура′пашка пlӓкат ′амдыдъ.</ta>
            <ta e="T130" id="Seg_861" s="T129">′сʼеkkа̄ɣы.</ta>
            <ta e="T134" id="Seg_862" s="T130">kа′рʼемыɣын ай ′kwаннын ′тʼӓкгу′лагытдъ.</ta>
            <ta e="T139" id="Seg_863" s="T134">′тӱа а̄′дун ′нагур тʼӓк′г̂у ′пыкгылы‵мыт.</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_864" s="T1">okkɨr qum ilɨss, täbɨn nimdə Тɨssija.</ta>
            <ta e="T12" id="Seg_865" s="T7">täbnan okkɨr iːt, okkɨr ne(ä)t.</ta>
            <ta e="T15" id="Seg_866" s="T12">kəlɨlʼla mütsä tüːwattə.</ta>
            <ta e="T18" id="Seg_867" s="T15">(mütse tʼüattə kəlolla.)</ta>
            <ta e="T21" id="Seg_868" s="T18">tebla qwatpattɨ Тɨssijam.</ta>
            <ta e="T28" id="Seg_869" s="T21">täbɨn iːmdə i nämdə maːttɨ omdɨlǯɨlʼe qwädipattə.</ta>
            <ta e="T33" id="Seg_870" s="T28">okkɨr aːtʼam qwɛdʼi(e)battə (konar tarla).</ta>
            <ta e="T42" id="Seg_871" s="T33">na iːw, qəlɨlla qwannattə, ill(lʼ)e übəran (nildʼzʼin warkəlʼe übəran).</ta>
            <ta e="T45" id="Seg_872" s="T42">jolkazʼe meːbat maːdɨm.</ta>
            <ta e="T58" id="Seg_873" s="T45">qaptʼe okkɨr qarʼemɨɣɨn nanʼätdänä nɨldʼzʼin tʼärɨn: tan amdaq, a man qwaǯan kuropaškalam mannɨbugu.</ta>
            <ta e="T59" id="Seg_874" s="T58">qwannɨn.</ta>
            <ta e="T62" id="Seg_875" s="T59">nanʼät tʼüːrlʼe qalɨn.</ta>
            <ta e="T63" id="Seg_876" s="T62">qwannɨn.</ta>
            <ta e="T66" id="Seg_877" s="T63">tʼäkgula melʼe taːdərətdɨt.</ta>
            <ta e="T71" id="Seg_878" s="T66">na dʼel nagur tʼäkku mematdɨt.</ta>
            <ta e="T75" id="Seg_879" s="T71">kəssə tüːa (moɣunä tüwa).</ta>
            <ta e="T81" id="Seg_880" s="T75">sədəmdʼettɨ d̂ʼel qarʼemɨɣɨn aj qwannɨn tʼäkgulaɣɨtdə.</ta>
            <ta e="T85" id="Seg_881" s="T81">tüa okkɨr kurapaška qwatpɨtdɨt.</ta>
            <ta e="T89" id="Seg_882" s="T85">aj nagur tʼäkĝu mematdɨt.</ta>
            <ta e="T92" id="Seg_883" s="T89">aj köse tüwa.</ta>
            <ta e="T98" id="Seg_884" s="T92">nanetdɨzʼe täbɨstaɣə sədəq(ɣ)un kurapaškan plʼäka ammɨtdədɨ.</ta>
            <ta e="T104" id="Seg_885" s="T98">a plʼäkamdə pennɨtɨ - qaptʼe štobɨ amgu.</ta>
            <ta e="T109" id="Seg_886" s="T104">qardʼen aj qwannɨn otdə tʼäkĝulaɣɨtdə.</ta>
            <ta e="T113" id="Seg_887" s="T109">tüan sədə tʼäkg(o)u pɨkgɨlamɨt.</ta>
            <ta e="T119" id="Seg_888" s="T113">taw(f)tʼe tü(w)a adun sədə kuropaška qwatpɨdɨt.</ta>
            <ta e="T123" id="Seg_889" s="T119">apʼätʼ nagur tʼäkĝu memmɨdɨt.</ta>
            <ta e="T125" id="Seg_890" s="T123">kösʼsʼe tüwa.</ta>
            <ta e="T129" id="Seg_891" s="T125">okkɨr kurapaška pläkat amdɨdə.</ta>
            <ta e="T130" id="Seg_892" s="T129">sʼeqqaːɣɨ.</ta>
            <ta e="T134" id="Seg_893" s="T130">qarʼemɨɣɨn aj qwannɨn tʼäkgulagɨtdə.</ta>
            <ta e="T139" id="Seg_894" s="T134">tüa aːdun nagur tʼäqĝu pɨqgɨlɨmɨt.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_895" s="T1">Okkɨr qum ilɨss, täbɨn nimdə Тɨssija. </ta>
            <ta e="T12" id="Seg_896" s="T7">Täbnan okkɨr iːt, okkɨr net. </ta>
            <ta e="T15" id="Seg_897" s="T12">Kəlɨlʼla mütsä tüːwattə. </ta>
            <ta e="T18" id="Seg_898" s="T15">(Mütse tʼüattə kəlolla.) </ta>
            <ta e="T21" id="Seg_899" s="T18">Tebla qwatpattɨ Тɨssijam. </ta>
            <ta e="T28" id="Seg_900" s="T21">Täbɨn iːmdə i nämdə maːttɨ omdɨlǯɨlʼe qwädipattə. </ta>
            <ta e="T33" id="Seg_901" s="T28">Okkɨr aːtʼam qwɛdʼibattə (konar tarla). </ta>
            <ta e="T42" id="Seg_902" s="T33">Na iːw, qəlɨlla qwannattə, ille übəran (nildʼzʼin warkəlʼe übəran). </ta>
            <ta e="T45" id="Seg_903" s="T42">Jolkazʼe meːbat maːdɨm. </ta>
            <ta e="T58" id="Seg_904" s="T45">Qaptʼe okkɨr qarʼemɨɣɨn nanʼätdänä nɨldʼzʼin tʼärɨn: “Tan amdaq, a man qwaǯan kuropaškalam mannɨbugu.” </ta>
            <ta e="T59" id="Seg_905" s="T58">Qwannɨn. </ta>
            <ta e="T62" id="Seg_906" s="T59">Nanʼät tʼüːrlʼe qalɨn. </ta>
            <ta e="T63" id="Seg_907" s="T62">Qwannɨn. </ta>
            <ta e="T66" id="Seg_908" s="T63">Tʼäkgula melʼe taːdərətdɨt. </ta>
            <ta e="T71" id="Seg_909" s="T66">Na dʼel nagur tʼäkku mematdɨt. </ta>
            <ta e="T75" id="Seg_910" s="T71">Kəssə tüːa (moɣunä tüwa). </ta>
            <ta e="T81" id="Seg_911" s="T75">Sədəmdʼettɨ dʼel qarʼemɨɣɨn aj qwannɨn tʼäkgulaɣɨtdə. </ta>
            <ta e="T85" id="Seg_912" s="T81">Tüa okkɨr kurapaška qwatpɨtdɨt. </ta>
            <ta e="T89" id="Seg_913" s="T85">Aj nagur tʼäkgu mematdɨt. </ta>
            <ta e="T92" id="Seg_914" s="T89">Aj köse tüwa. </ta>
            <ta e="T98" id="Seg_915" s="T92">Nanetdɨzʼe täbɨstaɣə sədəqun kurapaškan plʼäka ammɨtdədɨ. </ta>
            <ta e="T104" id="Seg_916" s="T98">A plʼäkamdə pennɨtɨ – qaptʼe štobɨ amgu. </ta>
            <ta e="T109" id="Seg_917" s="T104">Qardʼen aj qwannɨn otdə tʼäkgulaɣɨtdə. </ta>
            <ta e="T113" id="Seg_918" s="T109">Tüan: sədə tʼäkgu pɨkgɨlamɨt. </ta>
            <ta e="T119" id="Seg_919" s="T113">Tawtʼe tüa adun sədə kuropaška qwatpɨdɨt. </ta>
            <ta e="T123" id="Seg_920" s="T119">Apʼätʼ nagur tʼäkgu memmɨdɨt. </ta>
            <ta e="T125" id="Seg_921" s="T123">Kösʼsʼe tüwa. </ta>
            <ta e="T129" id="Seg_922" s="T125">Okkɨr kurapaška pläkat amdɨdə. </ta>
            <ta e="T130" id="Seg_923" s="T129">Sʼeqqaːɣɨ. </ta>
            <ta e="T134" id="Seg_924" s="T130">Qarʼemɨɣɨn aj qwannɨn tʼäkgulagɨtdə. </ta>
            <ta e="T139" id="Seg_925" s="T134">Tüa aːdun nagur tʼäqgu pɨqgɨlɨmɨt. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_926" s="T1">okkɨr</ta>
            <ta e="T3" id="Seg_927" s="T2">qum</ta>
            <ta e="T4" id="Seg_928" s="T3">ilɨ-ss</ta>
            <ta e="T5" id="Seg_929" s="T4">täb-ɨ-n</ta>
            <ta e="T6" id="Seg_930" s="T5">nim-də</ta>
            <ta e="T7" id="Seg_931" s="T6">Тɨssija</ta>
            <ta e="T8" id="Seg_932" s="T7">täb-nan</ta>
            <ta e="T9" id="Seg_933" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_934" s="T9">iː-t</ta>
            <ta e="T11" id="Seg_935" s="T10">okkɨr</ta>
            <ta e="T12" id="Seg_936" s="T11">ne-t</ta>
            <ta e="T13" id="Seg_937" s="T12">kəlɨlʼ-la</ta>
            <ta e="T14" id="Seg_938" s="T13">müt-sä</ta>
            <ta e="T15" id="Seg_939" s="T14">tüː-wa-ttə</ta>
            <ta e="T16" id="Seg_940" s="T15">müt-se</ta>
            <ta e="T17" id="Seg_941" s="T16">tʼü-a-ttə</ta>
            <ta e="T18" id="Seg_942" s="T17">kəlol-la</ta>
            <ta e="T19" id="Seg_943" s="T18">teb-la</ta>
            <ta e="T20" id="Seg_944" s="T19">qwat-pa-ttɨ</ta>
            <ta e="T21" id="Seg_945" s="T20">Тɨssija-m</ta>
            <ta e="T22" id="Seg_946" s="T21">täb-ɨ-n</ta>
            <ta e="T23" id="Seg_947" s="T22">iː-m-də</ta>
            <ta e="T24" id="Seg_948" s="T23">i</ta>
            <ta e="T25" id="Seg_949" s="T24">nä-m-də</ta>
            <ta e="T26" id="Seg_950" s="T25">maːt-tɨ</ta>
            <ta e="T27" id="Seg_951" s="T26">omdɨ-lǯɨ-lʼe</ta>
            <ta e="T28" id="Seg_952" s="T27">qwädi-pa-ttə</ta>
            <ta e="T29" id="Seg_953" s="T28">okkɨr</ta>
            <ta e="T30" id="Seg_954" s="T29">aːtʼa-m</ta>
            <ta e="T31" id="Seg_955" s="T30">qwɛdʼi-ba-ttə</ta>
            <ta e="T32" id="Seg_956" s="T31">konar</ta>
            <ta e="T33" id="Seg_957" s="T32">tar-la</ta>
            <ta e="T34" id="Seg_958" s="T33">na</ta>
            <ta e="T35" id="Seg_959" s="T34">iː-w</ta>
            <ta e="T36" id="Seg_960" s="T35">qəlɨl-la</ta>
            <ta e="T37" id="Seg_961" s="T36">qwan-na-ttə</ta>
            <ta e="T38" id="Seg_962" s="T37">il-le</ta>
            <ta e="T39" id="Seg_963" s="T38">übə-r-a-n</ta>
            <ta e="T40" id="Seg_964" s="T39">nildʼzʼi-n</ta>
            <ta e="T41" id="Seg_965" s="T40">warkə-lʼe</ta>
            <ta e="T42" id="Seg_966" s="T41">übə-r-a-n</ta>
            <ta e="T43" id="Seg_967" s="T42">jolka-zʼe</ta>
            <ta e="T44" id="Seg_968" s="T43">meː-ba-t</ta>
            <ta e="T45" id="Seg_969" s="T44">maːd-ɨ-m</ta>
            <ta e="T46" id="Seg_970" s="T45">qaptʼe</ta>
            <ta e="T47" id="Seg_971" s="T46">okkɨr</ta>
            <ta e="T48" id="Seg_972" s="T47">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T49" id="Seg_973" s="T48">nanʼä-tdä-nä</ta>
            <ta e="T50" id="Seg_974" s="T49">nɨldʼzʼi-n</ta>
            <ta e="T51" id="Seg_975" s="T50">tʼärɨ-n</ta>
            <ta e="T52" id="Seg_976" s="T51">Tan</ta>
            <ta e="T53" id="Seg_977" s="T52">amda-q</ta>
            <ta e="T54" id="Seg_978" s="T53">a</ta>
            <ta e="T55" id="Seg_979" s="T54">man</ta>
            <ta e="T56" id="Seg_980" s="T55">qwa-ǯa-n</ta>
            <ta e="T57" id="Seg_981" s="T56">kuropaška-la-m</ta>
            <ta e="T58" id="Seg_982" s="T57">mannɨ-bu-gu</ta>
            <ta e="T59" id="Seg_983" s="T58">qwan-nɨ-n</ta>
            <ta e="T60" id="Seg_984" s="T59">nanʼä-t</ta>
            <ta e="T61" id="Seg_985" s="T60">tʼüːr-lʼe</ta>
            <ta e="T62" id="Seg_986" s="T61">qalɨ-n</ta>
            <ta e="T63" id="Seg_987" s="T62">qwan-nɨ-n</ta>
            <ta e="T64" id="Seg_988" s="T63">tʼäkgu-la</ta>
            <ta e="T65" id="Seg_989" s="T64">me-lʼe</ta>
            <ta e="T66" id="Seg_990" s="T65">taːd-ə-r-ə-tdɨ-t</ta>
            <ta e="T67" id="Seg_991" s="T66">na</ta>
            <ta e="T68" id="Seg_992" s="T67">dʼel</ta>
            <ta e="T69" id="Seg_993" s="T68">nagur</ta>
            <ta e="T70" id="Seg_994" s="T69">tʼäkku</ta>
            <ta e="T71" id="Seg_995" s="T70">me-ma-tdɨ-t</ta>
            <ta e="T72" id="Seg_996" s="T71">kəssə</ta>
            <ta e="T73" id="Seg_997" s="T72">tüː-a</ta>
            <ta e="T74" id="Seg_998" s="T73">moɣunä</ta>
            <ta e="T75" id="Seg_999" s="T74">tü-wa</ta>
            <ta e="T76" id="Seg_1000" s="T75">sədə-mdʼettɨ</ta>
            <ta e="T77" id="Seg_1001" s="T76">dʼel</ta>
            <ta e="T78" id="Seg_1002" s="T77">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T79" id="Seg_1003" s="T78">aj</ta>
            <ta e="T80" id="Seg_1004" s="T79">qwan-nɨ-n</ta>
            <ta e="T81" id="Seg_1005" s="T80">tʼäkgu-la-ɣɨtdə</ta>
            <ta e="T82" id="Seg_1006" s="T81">tü-a</ta>
            <ta e="T83" id="Seg_1007" s="T82">okkɨr</ta>
            <ta e="T84" id="Seg_1008" s="T83">kurapaška</ta>
            <ta e="T85" id="Seg_1009" s="T84">qwat-pɨ-tdɨ-t</ta>
            <ta e="T86" id="Seg_1010" s="T85">aj</ta>
            <ta e="T87" id="Seg_1011" s="T86">nagur</ta>
            <ta e="T88" id="Seg_1012" s="T87">tʼäkgu</ta>
            <ta e="T89" id="Seg_1013" s="T88">me-ma-tdɨ-t</ta>
            <ta e="T90" id="Seg_1014" s="T89">aj</ta>
            <ta e="T91" id="Seg_1015" s="T90">köse</ta>
            <ta e="T92" id="Seg_1016" s="T91">tü-wa</ta>
            <ta e="T93" id="Seg_1017" s="T92">nane-tdɨ-zʼe</ta>
            <ta e="T94" id="Seg_1018" s="T93">täb-ɨ-staɣə</ta>
            <ta e="T95" id="Seg_1019" s="T94">sədə-qun</ta>
            <ta e="T96" id="Seg_1020" s="T95">kurapaška-n</ta>
            <ta e="T97" id="Seg_1021" s="T96">plʼäka</ta>
            <ta e="T98" id="Seg_1022" s="T97">am-mɨ-tdə-dɨ</ta>
            <ta e="T99" id="Seg_1023" s="T98">a</ta>
            <ta e="T100" id="Seg_1024" s="T99">plʼäka-m-də</ta>
            <ta e="T101" id="Seg_1025" s="T100">pen-nɨ-tɨ</ta>
            <ta e="T102" id="Seg_1026" s="T101">qaptʼe</ta>
            <ta e="T103" id="Seg_1027" s="T102">štobɨ</ta>
            <ta e="T104" id="Seg_1028" s="T103">am-gu</ta>
            <ta e="T105" id="Seg_1029" s="T104">qar-dʼe-n</ta>
            <ta e="T106" id="Seg_1030" s="T105">aj</ta>
            <ta e="T107" id="Seg_1031" s="T106">qwan-nɨ-n</ta>
            <ta e="T108" id="Seg_1032" s="T107">otdə</ta>
            <ta e="T109" id="Seg_1033" s="T108">tʼäkgu-la-ɣɨtdə</ta>
            <ta e="T110" id="Seg_1034" s="T109">tü-a-n</ta>
            <ta e="T111" id="Seg_1035" s="T110">sədə</ta>
            <ta e="T112" id="Seg_1036" s="T111">tʼäkgu</ta>
            <ta e="T113" id="Seg_1037" s="T112">pɨkgɨl-a-mɨ-t</ta>
            <ta e="T114" id="Seg_1038" s="T113">tawtʼe</ta>
            <ta e="T115" id="Seg_1039" s="T114">tü-a</ta>
            <ta e="T116" id="Seg_1040" s="T115">adu-n</ta>
            <ta e="T117" id="Seg_1041" s="T116">sədə</ta>
            <ta e="T118" id="Seg_1042" s="T117">kuropaška</ta>
            <ta e="T119" id="Seg_1043" s="T118">qwat-pɨ-dɨ-t</ta>
            <ta e="T120" id="Seg_1044" s="T119">apʼätʼ</ta>
            <ta e="T121" id="Seg_1045" s="T120">nagur</ta>
            <ta e="T122" id="Seg_1046" s="T121">tʼäkgu</ta>
            <ta e="T123" id="Seg_1047" s="T122">me-mmɨ-dɨ-t</ta>
            <ta e="T124" id="Seg_1048" s="T123">kösʼsʼe</ta>
            <ta e="T125" id="Seg_1049" s="T124">tü-wa</ta>
            <ta e="T126" id="Seg_1050" s="T125">okkɨr</ta>
            <ta e="T127" id="Seg_1051" s="T126">kurapaška</ta>
            <ta e="T128" id="Seg_1052" s="T127">pläka-t</ta>
            <ta e="T129" id="Seg_1053" s="T128">am-dɨ-də</ta>
            <ta e="T130" id="Seg_1054" s="T129">sʼeqqaː-ɣɨ</ta>
            <ta e="T131" id="Seg_1055" s="T130">qarʼe-mɨ-ɣɨn</ta>
            <ta e="T132" id="Seg_1056" s="T131">aj</ta>
            <ta e="T133" id="Seg_1057" s="T132">qwan-nɨ-n</ta>
            <ta e="T134" id="Seg_1058" s="T133">tʼäkgu-la-gɨtdə</ta>
            <ta e="T135" id="Seg_1059" s="T134">tü-a</ta>
            <ta e="T136" id="Seg_1060" s="T135">aːdu-n</ta>
            <ta e="T137" id="Seg_1061" s="T136">nagur</ta>
            <ta e="T138" id="Seg_1062" s="T137">tʼäqg-u</ta>
            <ta e="T139" id="Seg_1063" s="T138">pɨqgɨl-ɨ-mɨ-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1064" s="T1">okkɨr</ta>
            <ta e="T3" id="Seg_1065" s="T2">qum</ta>
            <ta e="T4" id="Seg_1066" s="T3">elɨ-sɨ</ta>
            <ta e="T5" id="Seg_1067" s="T4">täp-ɨ-n</ta>
            <ta e="T6" id="Seg_1068" s="T5">nim-tə</ta>
            <ta e="T7" id="Seg_1069" s="T6">Тɨssija</ta>
            <ta e="T8" id="Seg_1070" s="T7">täp-nan</ta>
            <ta e="T9" id="Seg_1071" s="T8">okkɨr</ta>
            <ta e="T10" id="Seg_1072" s="T9">iː-tə</ta>
            <ta e="T11" id="Seg_1073" s="T10">okkɨr</ta>
            <ta e="T12" id="Seg_1074" s="T11">ne-tə</ta>
            <ta e="T13" id="Seg_1075" s="T12">kəlɨl-la</ta>
            <ta e="T14" id="Seg_1076" s="T13">müt-se</ta>
            <ta e="T15" id="Seg_1077" s="T14">tüː-nɨ-tɨn</ta>
            <ta e="T16" id="Seg_1078" s="T15">müt-se</ta>
            <ta e="T17" id="Seg_1079" s="T16">tüː-nɨ-tɨn</ta>
            <ta e="T18" id="Seg_1080" s="T17">kəlɨl-la</ta>
            <ta e="T19" id="Seg_1081" s="T18">täp-la</ta>
            <ta e="T20" id="Seg_1082" s="T19">qwat-mbɨ-tɨn</ta>
            <ta e="T21" id="Seg_1083" s="T20">Тɨssija-m</ta>
            <ta e="T22" id="Seg_1084" s="T21">täp-ɨ-n</ta>
            <ta e="T23" id="Seg_1085" s="T22">iː-m-tə</ta>
            <ta e="T24" id="Seg_1086" s="T23">i</ta>
            <ta e="T25" id="Seg_1087" s="T24">ne-m-tə</ta>
            <ta e="T26" id="Seg_1088" s="T25">maːt-ntə</ta>
            <ta e="T27" id="Seg_1089" s="T26">amdɨ-lǯɨ-le</ta>
            <ta e="T28" id="Seg_1090" s="T27">qwɛdʼi-mbɨ-tɨn</ta>
            <ta e="T29" id="Seg_1091" s="T28">okkɨr</ta>
            <ta e="T30" id="Seg_1092" s="T29">aːta-m</ta>
            <ta e="T31" id="Seg_1093" s="T30">qwɛdʼi-mbɨ-tɨn</ta>
            <ta e="T32" id="Seg_1094" s="T31">konar</ta>
            <ta e="T33" id="Seg_1095" s="T32">tar-la</ta>
            <ta e="T34" id="Seg_1096" s="T33">na</ta>
            <ta e="T35" id="Seg_1097" s="T34">iː-w</ta>
            <ta e="T36" id="Seg_1098" s="T35">kəlɨl-la</ta>
            <ta e="T37" id="Seg_1099" s="T36">qwan-nɨ-tɨn</ta>
            <ta e="T38" id="Seg_1100" s="T37">elɨ-le</ta>
            <ta e="T39" id="Seg_1101" s="T38">übɨ-r-ɨ-n</ta>
            <ta e="T40" id="Seg_1102" s="T39">nʼilʼdʼi-ŋ</ta>
            <ta e="T41" id="Seg_1103" s="T40">warkɨ-le</ta>
            <ta e="T42" id="Seg_1104" s="T41">übɨ-r-ɨ-n</ta>
            <ta e="T43" id="Seg_1105" s="T42">jolka-se</ta>
            <ta e="T44" id="Seg_1106" s="T43">meː-mbɨ-t</ta>
            <ta e="T45" id="Seg_1107" s="T44">maːt-ɨ-m</ta>
            <ta e="T46" id="Seg_1108" s="T45">qaptʼe</ta>
            <ta e="T47" id="Seg_1109" s="T46">okkɨr</ta>
            <ta e="T48" id="Seg_1110" s="T47">qare-mɨ-qɨn</ta>
            <ta e="T49" id="Seg_1111" s="T48">nanʼa-tə-nä</ta>
            <ta e="T50" id="Seg_1112" s="T49">nʼilʼdʼi-ŋ</ta>
            <ta e="T51" id="Seg_1113" s="T50">tʼärɨ-n</ta>
            <ta e="T52" id="Seg_1114" s="T51">tan</ta>
            <ta e="T53" id="Seg_1115" s="T52">amdɨ-kɨ</ta>
            <ta e="T54" id="Seg_1116" s="T53">a</ta>
            <ta e="T55" id="Seg_1117" s="T54">man</ta>
            <ta e="T56" id="Seg_1118" s="T55">qwan-enǯɨ-ŋ</ta>
            <ta e="T57" id="Seg_1119" s="T56">kuropaška-la-m</ta>
            <ta e="T58" id="Seg_1120" s="T57">*mantɨ-mbɨ-gu</ta>
            <ta e="T59" id="Seg_1121" s="T58">qwan-nɨ-n</ta>
            <ta e="T60" id="Seg_1122" s="T59">nanʼa-tə</ta>
            <ta e="T61" id="Seg_1123" s="T60">tʼüru-le</ta>
            <ta e="T62" id="Seg_1124" s="T61">qalɨ-n</ta>
            <ta e="T63" id="Seg_1125" s="T62">qwan-nɨ-n</ta>
            <ta e="T64" id="Seg_1126" s="T63">tʼäŋgu-la</ta>
            <ta e="T65" id="Seg_1127" s="T64">meː-le</ta>
            <ta e="T66" id="Seg_1128" s="T65">tat-ɨ-r-ɨ-ntɨ-t</ta>
            <ta e="T67" id="Seg_1129" s="T66">na</ta>
            <ta e="T68" id="Seg_1130" s="T67">dʼel</ta>
            <ta e="T69" id="Seg_1131" s="T68">nagur</ta>
            <ta e="T70" id="Seg_1132" s="T69">tʼäŋgu</ta>
            <ta e="T71" id="Seg_1133" s="T70">meː-mbɨ-ntɨ-t</ta>
            <ta e="T72" id="Seg_1134" s="T71">kössə</ta>
            <ta e="T73" id="Seg_1135" s="T72">tüː-nɨ</ta>
            <ta e="T74" id="Seg_1136" s="T73">moqɨnä</ta>
            <ta e="T75" id="Seg_1137" s="T74">tüː-nɨ</ta>
            <ta e="T76" id="Seg_1138" s="T75">sədə-mtätte</ta>
            <ta e="T77" id="Seg_1139" s="T76">dʼel</ta>
            <ta e="T78" id="Seg_1140" s="T77">qare-mɨ-qɨn</ta>
            <ta e="T79" id="Seg_1141" s="T78">aj</ta>
            <ta e="T80" id="Seg_1142" s="T79">qwan-nɨ-n</ta>
            <ta e="T81" id="Seg_1143" s="T80">tʼäŋgu-la-qɨntɨ</ta>
            <ta e="T82" id="Seg_1144" s="T81">tüː-nɨ</ta>
            <ta e="T83" id="Seg_1145" s="T82">okkɨr</ta>
            <ta e="T84" id="Seg_1146" s="T83">kuropaška</ta>
            <ta e="T85" id="Seg_1147" s="T84">qwat-mbɨ-ntɨ-t</ta>
            <ta e="T86" id="Seg_1148" s="T85">aj</ta>
            <ta e="T87" id="Seg_1149" s="T86">nagur</ta>
            <ta e="T88" id="Seg_1150" s="T87">tʼäŋgu</ta>
            <ta e="T89" id="Seg_1151" s="T88">meː-mbɨ-ntɨ-t</ta>
            <ta e="T90" id="Seg_1152" s="T89">aj</ta>
            <ta e="T91" id="Seg_1153" s="T90">kössə</ta>
            <ta e="T92" id="Seg_1154" s="T91">tüː-nɨ</ta>
            <ta e="T93" id="Seg_1155" s="T92">nanʼa-ntɨ-se</ta>
            <ta e="T94" id="Seg_1156" s="T93">täp-ɨ-staɣɨ</ta>
            <ta e="T95" id="Seg_1157" s="T94">sədə-qɨn</ta>
            <ta e="T96" id="Seg_1158" s="T95">kuropaška-n</ta>
            <ta e="T97" id="Seg_1159" s="T96">plʼäka</ta>
            <ta e="T98" id="Seg_1160" s="T97">am-mbɨ-ntɨ-di</ta>
            <ta e="T99" id="Seg_1161" s="T98">a</ta>
            <ta e="T100" id="Seg_1162" s="T99">plʼäka-m-tə</ta>
            <ta e="T101" id="Seg_1163" s="T100">pen-nɨ-di</ta>
            <ta e="T102" id="Seg_1164" s="T101">qaptʼe</ta>
            <ta e="T103" id="Seg_1165" s="T102">štobɨ</ta>
            <ta e="T104" id="Seg_1166" s="T103">am-gu</ta>
            <ta e="T105" id="Seg_1167" s="T104">qare-dʼel-n</ta>
            <ta e="T106" id="Seg_1168" s="T105">aj</ta>
            <ta e="T107" id="Seg_1169" s="T106">qwan-nɨ-n</ta>
            <ta e="T108" id="Seg_1170" s="T107">ondə</ta>
            <ta e="T109" id="Seg_1171" s="T108">tʼäŋgu-la-qɨntɨ</ta>
            <ta e="T110" id="Seg_1172" s="T109">tüː-nɨ-n</ta>
            <ta e="T111" id="Seg_1173" s="T110">sədə</ta>
            <ta e="T112" id="Seg_1174" s="T111">tʼäŋgu</ta>
            <ta e="T113" id="Seg_1175" s="T112">pɨŋgəl-ɨ-mbɨ-ntɨ</ta>
            <ta e="T114" id="Seg_1176" s="T113">tautʼe</ta>
            <ta e="T115" id="Seg_1177" s="T114">tüː-nɨ</ta>
            <ta e="T116" id="Seg_1178" s="T115">aːdu-n</ta>
            <ta e="T117" id="Seg_1179" s="T116">sədə</ta>
            <ta e="T118" id="Seg_1180" s="T117">kuropaška</ta>
            <ta e="T119" id="Seg_1181" s="T118">qwat-mbɨ-ntɨ-t</ta>
            <ta e="T120" id="Seg_1182" s="T119">apʼatʼ</ta>
            <ta e="T121" id="Seg_1183" s="T120">nagur</ta>
            <ta e="T122" id="Seg_1184" s="T121">tʼäŋgu</ta>
            <ta e="T123" id="Seg_1185" s="T122">meː-mbɨ-ntɨ-t</ta>
            <ta e="T124" id="Seg_1186" s="T123">kössə</ta>
            <ta e="T125" id="Seg_1187" s="T124">tüː-nɨ</ta>
            <ta e="T126" id="Seg_1188" s="T125">okkɨr</ta>
            <ta e="T127" id="Seg_1189" s="T126">kuropaška</ta>
            <ta e="T128" id="Seg_1190" s="T127">plʼäka-tə</ta>
            <ta e="T129" id="Seg_1191" s="T128">am-ntɨ-di</ta>
            <ta e="T130" id="Seg_1192" s="T129">seqqɨ-qij</ta>
            <ta e="T131" id="Seg_1193" s="T130">qare-mɨ-qɨn</ta>
            <ta e="T132" id="Seg_1194" s="T131">aj</ta>
            <ta e="T133" id="Seg_1195" s="T132">qwan-nɨ-n</ta>
            <ta e="T134" id="Seg_1196" s="T133">tʼäŋgu-la-qɨntɨ</ta>
            <ta e="T135" id="Seg_1197" s="T134">tüː-nɨ</ta>
            <ta e="T136" id="Seg_1198" s="T135">aːdu-n</ta>
            <ta e="T137" id="Seg_1199" s="T136">nagur</ta>
            <ta e="T138" id="Seg_1200" s="T137">tʼäŋgu-w</ta>
            <ta e="T139" id="Seg_1201" s="T138">pɨŋgəl-ɨ-mbɨ-tɨn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1202" s="T1">one</ta>
            <ta e="T3" id="Seg_1203" s="T2">human.being.[NOM]</ta>
            <ta e="T4" id="Seg_1204" s="T3">live-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_1205" s="T4">(s)he-EP-GEN</ta>
            <ta e="T6" id="Seg_1206" s="T5">name.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_1207" s="T6">Tyssija.[NOM]</ta>
            <ta e="T8" id="Seg_1208" s="T7">(s)he-ADES</ta>
            <ta e="T9" id="Seg_1209" s="T8">one</ta>
            <ta e="T10" id="Seg_1210" s="T9">son.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_1211" s="T10">one</ta>
            <ta e="T12" id="Seg_1212" s="T11">daughter.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_1213" s="T12">Tungus-PL.[NOM]</ta>
            <ta e="T14" id="Seg_1214" s="T13">war-COM</ta>
            <ta e="T15" id="Seg_1215" s="T14">come-CO-3PL</ta>
            <ta e="T16" id="Seg_1216" s="T15">war-COM</ta>
            <ta e="T17" id="Seg_1217" s="T16">come-CO-3PL</ta>
            <ta e="T18" id="Seg_1218" s="T17">Tungus-PL.[NOM]</ta>
            <ta e="T19" id="Seg_1219" s="T18">(s)he-PL.[NOM]</ta>
            <ta e="T20" id="Seg_1220" s="T19">kill-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_1221" s="T20">Tyssija-ACC</ta>
            <ta e="T22" id="Seg_1222" s="T21">(s)he-EP-GEN</ta>
            <ta e="T23" id="Seg_1223" s="T22">son-ACC-3SG</ta>
            <ta e="T24" id="Seg_1224" s="T23">and</ta>
            <ta e="T25" id="Seg_1225" s="T24">daughter-ACC-3SG</ta>
            <ta e="T26" id="Seg_1226" s="T25">house-ILL</ta>
            <ta e="T27" id="Seg_1227" s="T26">sit-TR-CVB</ta>
            <ta e="T28" id="Seg_1228" s="T27">leave-PST.NAR-3PL</ta>
            <ta e="T29" id="Seg_1229" s="T28">one</ta>
            <ta e="T30" id="Seg_1230" s="T29">reindeer-ACC</ta>
            <ta e="T31" id="Seg_1231" s="T30">leave-PST.NAR-3PL</ta>
            <ta e="T32" id="Seg_1232" s="T31">%%</ta>
            <ta e="T33" id="Seg_1233" s="T32">hair-PL.[NOM]</ta>
            <ta e="T34" id="Seg_1234" s="T33">this</ta>
            <ta e="T35" id="Seg_1235" s="T34">son.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_1236" s="T35">Tungus-PL.[NOM]</ta>
            <ta e="T37" id="Seg_1237" s="T36">leave-CO-3PL</ta>
            <ta e="T38" id="Seg_1238" s="T37">live-CVB</ta>
            <ta e="T39" id="Seg_1239" s="T38">begin-FRQ-EP-3SG.S</ta>
            <ta e="T40" id="Seg_1240" s="T39">so-ADVZ</ta>
            <ta e="T41" id="Seg_1241" s="T40">live-CVB</ta>
            <ta e="T42" id="Seg_1242" s="T41">begin-FRQ-EP-3SG.S</ta>
            <ta e="T43" id="Seg_1243" s="T42">fir.tree-INSTR</ta>
            <ta e="T44" id="Seg_1244" s="T43">do-PST.NAR-3SG.O</ta>
            <ta e="T45" id="Seg_1245" s="T44">house-EP-ACC</ta>
            <ta e="T46" id="Seg_1246" s="T45">then</ta>
            <ta e="T47" id="Seg_1247" s="T46">one</ta>
            <ta e="T48" id="Seg_1248" s="T47">morning-something-LOC</ta>
            <ta e="T49" id="Seg_1249" s="T48">sister-3SG-ALL</ta>
            <ta e="T50" id="Seg_1250" s="T49">such-ADVZ</ta>
            <ta e="T51" id="Seg_1251" s="T50">say-3SG.S</ta>
            <ta e="T52" id="Seg_1252" s="T51">you.SG.NOM</ta>
            <ta e="T53" id="Seg_1253" s="T52">sit-IMP.2SG.S</ta>
            <ta e="T54" id="Seg_1254" s="T53">and</ta>
            <ta e="T55" id="Seg_1255" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_1256" s="T55">leave-FUT-1SG.S</ta>
            <ta e="T57" id="Seg_1257" s="T56">partridge-PL-ACC</ta>
            <ta e="T58" id="Seg_1258" s="T57">look-DUR-INF</ta>
            <ta e="T59" id="Seg_1259" s="T58">leave-CO-3SG.S</ta>
            <ta e="T60" id="Seg_1260" s="T59">sister.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_1261" s="T60">cry-CVB</ta>
            <ta e="T62" id="Seg_1262" s="T61">stay-3SG.S</ta>
            <ta e="T63" id="Seg_1263" s="T62">leave-CO-3SG.S</ta>
            <ta e="T64" id="Seg_1264" s="T63">trap-PL.[NOM]</ta>
            <ta e="T65" id="Seg_1265" s="T64">do-CVB</ta>
            <ta e="T66" id="Seg_1266" s="T65">bring-EP-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T67" id="Seg_1267" s="T66">this</ta>
            <ta e="T68" id="Seg_1268" s="T67">day.[NOM]</ta>
            <ta e="T69" id="Seg_1269" s="T68">three</ta>
            <ta e="T70" id="Seg_1270" s="T69">trap.[NOM]</ta>
            <ta e="T71" id="Seg_1271" s="T70">do-PST.NAR-INFER-3SG.O</ta>
            <ta e="T72" id="Seg_1272" s="T71">backward</ta>
            <ta e="T73" id="Seg_1273" s="T72">come-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1274" s="T73">back</ta>
            <ta e="T75" id="Seg_1275" s="T74">come-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_1276" s="T75">two-ORD</ta>
            <ta e="T77" id="Seg_1277" s="T76">day.[NOM]</ta>
            <ta e="T78" id="Seg_1278" s="T77">morning-something-LOC</ta>
            <ta e="T79" id="Seg_1279" s="T78">again</ta>
            <ta e="T80" id="Seg_1280" s="T79">leave-CO-3SG.S</ta>
            <ta e="T81" id="Seg_1281" s="T80">trap-PL-ILL.3SG</ta>
            <ta e="T82" id="Seg_1282" s="T81">come-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_1283" s="T82">one</ta>
            <ta e="T84" id="Seg_1284" s="T83">partridge.[NOM]</ta>
            <ta e="T85" id="Seg_1285" s="T84">catch-RES-INFER-3SG.O</ta>
            <ta e="T86" id="Seg_1286" s="T85">again</ta>
            <ta e="T87" id="Seg_1287" s="T86">three</ta>
            <ta e="T88" id="Seg_1288" s="T87">trap.[NOM]</ta>
            <ta e="T89" id="Seg_1289" s="T88">do-PST.NAR-INFER-3SG.O</ta>
            <ta e="T90" id="Seg_1290" s="T89">again</ta>
            <ta e="T91" id="Seg_1291" s="T90">backward</ta>
            <ta e="T92" id="Seg_1292" s="T91">come-CO.[3SG.S]</ta>
            <ta e="T93" id="Seg_1293" s="T92">sister-OBL.3SG-COM</ta>
            <ta e="T94" id="Seg_1294" s="T93">(s)he-EP-DU.[NOM]</ta>
            <ta e="T95" id="Seg_1295" s="T94">two-LOC</ta>
            <ta e="T96" id="Seg_1296" s="T95">partridge-GEN</ta>
            <ta e="T97" id="Seg_1297" s="T96">half.[NOM]</ta>
            <ta e="T98" id="Seg_1298" s="T97">eat-PST.NAR-INFER-3DU.O</ta>
            <ta e="T99" id="Seg_1299" s="T98">and</ta>
            <ta e="T100" id="Seg_1300" s="T99">half-ACC-3SG</ta>
            <ta e="T101" id="Seg_1301" s="T100">put-CO-3DU.O</ta>
            <ta e="T102" id="Seg_1302" s="T101">then</ta>
            <ta e="T103" id="Seg_1303" s="T102">so.that</ta>
            <ta e="T104" id="Seg_1304" s="T103">eat-INF</ta>
            <ta e="T105" id="Seg_1305" s="T104">morning-day-ADV.LOC</ta>
            <ta e="T106" id="Seg_1306" s="T105">again</ta>
            <ta e="T107" id="Seg_1307" s="T106">leave-CO-3SG.S</ta>
            <ta e="T108" id="Seg_1308" s="T107">own.3SG</ta>
            <ta e="T109" id="Seg_1309" s="T108">trap-PL-ILL.3SG</ta>
            <ta e="T110" id="Seg_1310" s="T109">come-CO-3SG.S</ta>
            <ta e="T111" id="Seg_1311" s="T110">two</ta>
            <ta e="T112" id="Seg_1312" s="T111">trap.[NOM]</ta>
            <ta e="T113" id="Seg_1313" s="T112">fall.down-EP-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T114" id="Seg_1314" s="T113">here</ta>
            <ta e="T115" id="Seg_1315" s="T114">come-CO.[3SG.S]</ta>
            <ta e="T116" id="Seg_1316" s="T115">be.visible-3SG.S</ta>
            <ta e="T117" id="Seg_1317" s="T116">two</ta>
            <ta e="T118" id="Seg_1318" s="T117">partridge.[NOM]</ta>
            <ta e="T119" id="Seg_1319" s="T118">manage.to.get-PST.NAR-INFER-3SG.O</ta>
            <ta e="T120" id="Seg_1320" s="T119">again</ta>
            <ta e="T121" id="Seg_1321" s="T120">three</ta>
            <ta e="T122" id="Seg_1322" s="T121">trap.[NOM]</ta>
            <ta e="T123" id="Seg_1323" s="T122">do-PST.NAR-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_1324" s="T123">backward</ta>
            <ta e="T125" id="Seg_1325" s="T124">come-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_1326" s="T125">one</ta>
            <ta e="T127" id="Seg_1327" s="T126">partridge.[NOM]</ta>
            <ta e="T128" id="Seg_1328" s="T127">half.[NOM]-3SG</ta>
            <ta e="T129" id="Seg_1329" s="T128">eat-INFER-3DU.O</ta>
            <ta e="T130" id="Seg_1330" s="T129">overnight-3DU.S</ta>
            <ta e="T131" id="Seg_1331" s="T130">morning-something-LOC</ta>
            <ta e="T132" id="Seg_1332" s="T131">again</ta>
            <ta e="T133" id="Seg_1333" s="T132">leave-CO-3SG.S</ta>
            <ta e="T134" id="Seg_1334" s="T133">trap-PL-ILL.3SG</ta>
            <ta e="T135" id="Seg_1335" s="T134">come-CO.[3SG.S]</ta>
            <ta e="T136" id="Seg_1336" s="T135">be.visible-3SG.S</ta>
            <ta e="T137" id="Seg_1337" s="T136">three</ta>
            <ta e="T138" id="Seg_1338" s="T137">trap.[NOM]-1SG</ta>
            <ta e="T139" id="Seg_1339" s="T138">fall.down-EP-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1340" s="T1">один</ta>
            <ta e="T3" id="Seg_1341" s="T2">человек.[NOM]</ta>
            <ta e="T4" id="Seg_1342" s="T3">жить-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_1343" s="T4">он(а)-EP-GEN</ta>
            <ta e="T6" id="Seg_1344" s="T5">имя.[NOM]-3SG</ta>
            <ta e="T7" id="Seg_1345" s="T6">Тыссия.[NOM]</ta>
            <ta e="T8" id="Seg_1346" s="T7">он(а)-ADES</ta>
            <ta e="T9" id="Seg_1347" s="T8">один</ta>
            <ta e="T10" id="Seg_1348" s="T9">сын.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_1349" s="T10">один</ta>
            <ta e="T12" id="Seg_1350" s="T11">дочь.[NOM]-3SG</ta>
            <ta e="T13" id="Seg_1351" s="T12">тунгус-PL.[NOM]</ta>
            <ta e="T14" id="Seg_1352" s="T13">война-COM</ta>
            <ta e="T15" id="Seg_1353" s="T14">прийти-CO-3PL</ta>
            <ta e="T16" id="Seg_1354" s="T15">война-COM</ta>
            <ta e="T17" id="Seg_1355" s="T16">приехать-CO-3PL</ta>
            <ta e="T18" id="Seg_1356" s="T17">тунгус-PL.[NOM]</ta>
            <ta e="T19" id="Seg_1357" s="T18">он(а)-PL.[NOM]</ta>
            <ta e="T20" id="Seg_1358" s="T19">убить-PST.NAR-3PL</ta>
            <ta e="T21" id="Seg_1359" s="T20">Тыссия-ACC</ta>
            <ta e="T22" id="Seg_1360" s="T21">он(а)-EP-GEN</ta>
            <ta e="T23" id="Seg_1361" s="T22">сын-ACC-3SG</ta>
            <ta e="T24" id="Seg_1362" s="T23">и</ta>
            <ta e="T25" id="Seg_1363" s="T24">дочь-ACC-3SG</ta>
            <ta e="T26" id="Seg_1364" s="T25">дом-ILL</ta>
            <ta e="T27" id="Seg_1365" s="T26">сидеть-TR-CVB</ta>
            <ta e="T28" id="Seg_1366" s="T27">оставить-PST.NAR-3PL</ta>
            <ta e="T29" id="Seg_1367" s="T28">один</ta>
            <ta e="T30" id="Seg_1368" s="T29">олень-ACC</ta>
            <ta e="T31" id="Seg_1369" s="T30">оставить-PST.NAR-3PL</ta>
            <ta e="T32" id="Seg_1370" s="T31">%%</ta>
            <ta e="T33" id="Seg_1371" s="T32">шерсть-PL.[NOM]</ta>
            <ta e="T34" id="Seg_1372" s="T33">этот</ta>
            <ta e="T35" id="Seg_1373" s="T34">сын.[NOM]-1SG</ta>
            <ta e="T36" id="Seg_1374" s="T35">тунгус-PL.[NOM]</ta>
            <ta e="T37" id="Seg_1375" s="T36">отправиться-CO-3PL</ta>
            <ta e="T38" id="Seg_1376" s="T37">жить-CVB</ta>
            <ta e="T39" id="Seg_1377" s="T38">начать-FRQ-EP-3SG.S</ta>
            <ta e="T40" id="Seg_1378" s="T39">так-ADVZ</ta>
            <ta e="T41" id="Seg_1379" s="T40">жить-CVB</ta>
            <ta e="T42" id="Seg_1380" s="T41">начать-FRQ-EP-3SG.S</ta>
            <ta e="T43" id="Seg_1381" s="T42">елка-INSTR</ta>
            <ta e="T44" id="Seg_1382" s="T43">сделать-PST.NAR-3SG.O</ta>
            <ta e="T45" id="Seg_1383" s="T44">дом-EP-ACC</ta>
            <ta e="T46" id="Seg_1384" s="T45">потом</ta>
            <ta e="T47" id="Seg_1385" s="T46">один</ta>
            <ta e="T48" id="Seg_1386" s="T47">утро-нечто-LOC</ta>
            <ta e="T49" id="Seg_1387" s="T48">сестра-3SG-ALL</ta>
            <ta e="T50" id="Seg_1388" s="T49">такой-ADVZ</ta>
            <ta e="T51" id="Seg_1389" s="T50">сказать-3SG.S</ta>
            <ta e="T52" id="Seg_1390" s="T51">ты.NOM</ta>
            <ta e="T53" id="Seg_1391" s="T52">сидеть-IMP.2SG.S</ta>
            <ta e="T54" id="Seg_1392" s="T53">а</ta>
            <ta e="T55" id="Seg_1393" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_1394" s="T55">отправиться-FUT-1SG.S</ta>
            <ta e="T57" id="Seg_1395" s="T56">куропатка-PL-ACC</ta>
            <ta e="T58" id="Seg_1396" s="T57">посмотреть-DUR-INF</ta>
            <ta e="T59" id="Seg_1397" s="T58">отправиться-CO-3SG.S</ta>
            <ta e="T60" id="Seg_1398" s="T59">сестра.[NOM]-3SG</ta>
            <ta e="T61" id="Seg_1399" s="T60">плакать-CVB</ta>
            <ta e="T62" id="Seg_1400" s="T61">остаться-3SG.S</ta>
            <ta e="T63" id="Seg_1401" s="T62">отправиться-CO-3SG.S</ta>
            <ta e="T64" id="Seg_1402" s="T63">ловушка-PL.[NOM]</ta>
            <ta e="T65" id="Seg_1403" s="T64">сделать-CVB</ta>
            <ta e="T66" id="Seg_1404" s="T65">принести-EP-FRQ-EP-INFER-3SG.O</ta>
            <ta e="T67" id="Seg_1405" s="T66">этот</ta>
            <ta e="T68" id="Seg_1406" s="T67">день.[NOM]</ta>
            <ta e="T69" id="Seg_1407" s="T68">три</ta>
            <ta e="T70" id="Seg_1408" s="T69">ловушка.[NOM]</ta>
            <ta e="T71" id="Seg_1409" s="T70">сделать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T72" id="Seg_1410" s="T71">назад</ta>
            <ta e="T73" id="Seg_1411" s="T72">прийти-CO.[3SG.S]</ta>
            <ta e="T74" id="Seg_1412" s="T73">назад</ta>
            <ta e="T75" id="Seg_1413" s="T74">прийти-CO.[3SG.S]</ta>
            <ta e="T76" id="Seg_1414" s="T75">два-ORD</ta>
            <ta e="T77" id="Seg_1415" s="T76">день.[NOM]</ta>
            <ta e="T78" id="Seg_1416" s="T77">утро-нечто-LOC</ta>
            <ta e="T79" id="Seg_1417" s="T78">опять</ta>
            <ta e="T80" id="Seg_1418" s="T79">отправиться-CO-3SG.S</ta>
            <ta e="T81" id="Seg_1419" s="T80">ловушка-PL-ILL.3SG</ta>
            <ta e="T82" id="Seg_1420" s="T81">прийти-CO.[3SG.S]</ta>
            <ta e="T83" id="Seg_1421" s="T82">один</ta>
            <ta e="T84" id="Seg_1422" s="T83">куропатка.[NOM]</ta>
            <ta e="T85" id="Seg_1423" s="T84">поймать-RES-INFER-3SG.O</ta>
            <ta e="T86" id="Seg_1424" s="T85">опять</ta>
            <ta e="T87" id="Seg_1425" s="T86">три</ta>
            <ta e="T88" id="Seg_1426" s="T87">ловушка.[NOM]</ta>
            <ta e="T89" id="Seg_1427" s="T88">сделать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T90" id="Seg_1428" s="T89">опять</ta>
            <ta e="T91" id="Seg_1429" s="T90">назад</ta>
            <ta e="T92" id="Seg_1430" s="T91">прийти-CO.[3SG.S]</ta>
            <ta e="T93" id="Seg_1431" s="T92">сестра-OBL.3SG-COM</ta>
            <ta e="T94" id="Seg_1432" s="T93">он(а)-EP-DU.[NOM]</ta>
            <ta e="T95" id="Seg_1433" s="T94">два-LOC</ta>
            <ta e="T96" id="Seg_1434" s="T95">куропатка-GEN</ta>
            <ta e="T97" id="Seg_1435" s="T96">половина.[NOM]</ta>
            <ta e="T98" id="Seg_1436" s="T97">съесть-PST.NAR-INFER-3DU.O</ta>
            <ta e="T99" id="Seg_1437" s="T98">а</ta>
            <ta e="T100" id="Seg_1438" s="T99">половина-ACC-3SG</ta>
            <ta e="T101" id="Seg_1439" s="T100">положить-CO-3DU.O</ta>
            <ta e="T102" id="Seg_1440" s="T101">потом</ta>
            <ta e="T103" id="Seg_1441" s="T102">чтобы</ta>
            <ta e="T104" id="Seg_1442" s="T103">съесть-INF</ta>
            <ta e="T105" id="Seg_1443" s="T104">утро-день-ADV.LOC</ta>
            <ta e="T106" id="Seg_1444" s="T105">опять</ta>
            <ta e="T107" id="Seg_1445" s="T106">отправиться-CO-3SG.S</ta>
            <ta e="T108" id="Seg_1446" s="T107">свой.3SG</ta>
            <ta e="T109" id="Seg_1447" s="T108">ловушка-PL-ILL.3SG</ta>
            <ta e="T110" id="Seg_1448" s="T109">прийти-CO-3SG.S</ta>
            <ta e="T111" id="Seg_1449" s="T110">два</ta>
            <ta e="T112" id="Seg_1450" s="T111">ловушка.[NOM]</ta>
            <ta e="T113" id="Seg_1451" s="T112">упасть-EP-PST.NAR-INFER.[3SG.S]</ta>
            <ta e="T114" id="Seg_1452" s="T113">сюда</ta>
            <ta e="T115" id="Seg_1453" s="T114">прийти-CO.[3SG.S]</ta>
            <ta e="T116" id="Seg_1454" s="T115">виднеться-3SG.S</ta>
            <ta e="T117" id="Seg_1455" s="T116">два</ta>
            <ta e="T118" id="Seg_1456" s="T117">куропатка.[NOM]</ta>
            <ta e="T119" id="Seg_1457" s="T118">добыть-PST.NAR-INFER-3SG.O</ta>
            <ta e="T120" id="Seg_1458" s="T119">опять</ta>
            <ta e="T121" id="Seg_1459" s="T120">три</ta>
            <ta e="T122" id="Seg_1460" s="T121">ловушка.[NOM]</ta>
            <ta e="T123" id="Seg_1461" s="T122">сделать-PST.NAR-INFER-3SG.O</ta>
            <ta e="T124" id="Seg_1462" s="T123">назад</ta>
            <ta e="T125" id="Seg_1463" s="T124">прийти-CO.[3SG.S]</ta>
            <ta e="T126" id="Seg_1464" s="T125">один</ta>
            <ta e="T127" id="Seg_1465" s="T126">куропатка.[NOM]</ta>
            <ta e="T128" id="Seg_1466" s="T127">половина.[NOM]-3SG</ta>
            <ta e="T129" id="Seg_1467" s="T128">съесть-INFER-3DU.O</ta>
            <ta e="T130" id="Seg_1468" s="T129">ночевать-3DU.S</ta>
            <ta e="T131" id="Seg_1469" s="T130">утро-нечто-LOC</ta>
            <ta e="T132" id="Seg_1470" s="T131">опять</ta>
            <ta e="T133" id="Seg_1471" s="T132">отправиться-CO-3SG.S</ta>
            <ta e="T134" id="Seg_1472" s="T133">ловушка-PL-ILL.3SG</ta>
            <ta e="T135" id="Seg_1473" s="T134">прийти-CO.[3SG.S]</ta>
            <ta e="T136" id="Seg_1474" s="T135">виднеться-3SG.S</ta>
            <ta e="T137" id="Seg_1475" s="T136">три</ta>
            <ta e="T138" id="Seg_1476" s="T137">ловушка.[NOM]-1SG</ta>
            <ta e="T139" id="Seg_1477" s="T138">упасть-EP-PST.NAR-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1478" s="T1">num</ta>
            <ta e="T3" id="Seg_1479" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_1480" s="T3">v-v:tense.[v:pn]</ta>
            <ta e="T5" id="Seg_1481" s="T4">pers-n:ins-n:case</ta>
            <ta e="T6" id="Seg_1482" s="T5">n.[n:case]-n:poss</ta>
            <ta e="T7" id="Seg_1483" s="T6">nprop.[n:case]</ta>
            <ta e="T8" id="Seg_1484" s="T7">pers-n:case</ta>
            <ta e="T9" id="Seg_1485" s="T8">num</ta>
            <ta e="T10" id="Seg_1486" s="T9">n.[n:case]-n:poss</ta>
            <ta e="T11" id="Seg_1487" s="T10">num</ta>
            <ta e="T12" id="Seg_1488" s="T11">n.[n:case]-n:poss</ta>
            <ta e="T13" id="Seg_1489" s="T12">n-n:num.[n:case]</ta>
            <ta e="T14" id="Seg_1490" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1491" s="T14">v-v:ins-v:pn</ta>
            <ta e="T16" id="Seg_1492" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1493" s="T16">v-v:ins-v:pn</ta>
            <ta e="T18" id="Seg_1494" s="T17">n-n:num.[n:case]</ta>
            <ta e="T19" id="Seg_1495" s="T18">pers-n:num.[n:case]</ta>
            <ta e="T20" id="Seg_1496" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1497" s="T20">nprop-n:case</ta>
            <ta e="T22" id="Seg_1498" s="T21">pers-n:ins-n:case</ta>
            <ta e="T23" id="Seg_1499" s="T22">n-n:case-n:poss</ta>
            <ta e="T24" id="Seg_1500" s="T23">conj</ta>
            <ta e="T25" id="Seg_1501" s="T24">n-n:case-n:poss</ta>
            <ta e="T26" id="Seg_1502" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_1503" s="T26">v-v&gt;v-v&gt;adv</ta>
            <ta e="T28" id="Seg_1504" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1505" s="T28">num</ta>
            <ta e="T30" id="Seg_1506" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1507" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1508" s="T31">%%</ta>
            <ta e="T33" id="Seg_1509" s="T32">n-n:num.[n:case]</ta>
            <ta e="T34" id="Seg_1510" s="T33">dem</ta>
            <ta e="T35" id="Seg_1511" s="T34">n.[n:case]-n:poss</ta>
            <ta e="T36" id="Seg_1512" s="T35">n-n:num.[n:case]</ta>
            <ta e="T37" id="Seg_1513" s="T36">v-v:ins-v:pn</ta>
            <ta e="T38" id="Seg_1514" s="T37">v-v&gt;adv</ta>
            <ta e="T39" id="Seg_1515" s="T38">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T40" id="Seg_1516" s="T39">adv-adj&gt;adv</ta>
            <ta e="T41" id="Seg_1517" s="T40">v-v&gt;adv</ta>
            <ta e="T42" id="Seg_1518" s="T41">v-v&gt;v-n:ins-v:pn</ta>
            <ta e="T43" id="Seg_1519" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1520" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1521" s="T44">n-n:ins-n:case</ta>
            <ta e="T46" id="Seg_1522" s="T45">adv</ta>
            <ta e="T47" id="Seg_1523" s="T46">num</ta>
            <ta e="T48" id="Seg_1524" s="T47">n-n-n:case</ta>
            <ta e="T49" id="Seg_1525" s="T48">n-n:poss-n:case</ta>
            <ta e="T50" id="Seg_1526" s="T49">adj-adj&gt;adv</ta>
            <ta e="T51" id="Seg_1527" s="T50">v-v:pn</ta>
            <ta e="T52" id="Seg_1528" s="T51">pers</ta>
            <ta e="T53" id="Seg_1529" s="T52">v-v:mood.pn</ta>
            <ta e="T54" id="Seg_1530" s="T53">conj</ta>
            <ta e="T55" id="Seg_1531" s="T54">pers</ta>
            <ta e="T56" id="Seg_1532" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1533" s="T56">n-n:num-n:case</ta>
            <ta e="T58" id="Seg_1534" s="T57">v-v&gt;v-v:inf</ta>
            <ta e="T59" id="Seg_1535" s="T58">v-v:ins-v:pn</ta>
            <ta e="T60" id="Seg_1536" s="T59">n.[n:case]-n:poss</ta>
            <ta e="T61" id="Seg_1537" s="T60">v-v&gt;adv</ta>
            <ta e="T62" id="Seg_1538" s="T61">v-v:pn</ta>
            <ta e="T63" id="Seg_1539" s="T62">v-v:ins-v:pn</ta>
            <ta e="T64" id="Seg_1540" s="T63">n-n:num.[n:case]</ta>
            <ta e="T65" id="Seg_1541" s="T64">v-v&gt;adv</ta>
            <ta e="T66" id="Seg_1542" s="T65">v-v:ins-v&gt;v-v:ins-v:mood-v:pn</ta>
            <ta e="T67" id="Seg_1543" s="T66">dem</ta>
            <ta e="T68" id="Seg_1544" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_1545" s="T68">num</ta>
            <ta e="T70" id="Seg_1546" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_1547" s="T70">v-v:tense-v:mood-v:pn</ta>
            <ta e="T72" id="Seg_1548" s="T71">adv</ta>
            <ta e="T73" id="Seg_1549" s="T72">v-v:ins.[v:pn]</ta>
            <ta e="T74" id="Seg_1550" s="T73">adv</ta>
            <ta e="T75" id="Seg_1551" s="T74">v-v:ins.[v:pn]</ta>
            <ta e="T76" id="Seg_1552" s="T75">num-num&gt;adj</ta>
            <ta e="T77" id="Seg_1553" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_1554" s="T77">n-n-n:case</ta>
            <ta e="T79" id="Seg_1555" s="T78">adv</ta>
            <ta e="T80" id="Seg_1556" s="T79">v-v:ins-v:pn</ta>
            <ta e="T81" id="Seg_1557" s="T80">n-n:num-n:case.poss</ta>
            <ta e="T82" id="Seg_1558" s="T81">v-v:ins.[v:pn]</ta>
            <ta e="T83" id="Seg_1559" s="T82">num</ta>
            <ta e="T84" id="Seg_1560" s="T83">n.[n:case]</ta>
            <ta e="T85" id="Seg_1561" s="T84">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T86" id="Seg_1562" s="T85">adv</ta>
            <ta e="T87" id="Seg_1563" s="T86">num</ta>
            <ta e="T88" id="Seg_1564" s="T87">n.[n:case]</ta>
            <ta e="T89" id="Seg_1565" s="T88">v-v:tense-v:mood-v:pn</ta>
            <ta e="T90" id="Seg_1566" s="T89">adv</ta>
            <ta e="T91" id="Seg_1567" s="T90">adv</ta>
            <ta e="T92" id="Seg_1568" s="T91">v-v:ins.[v:pn]</ta>
            <ta e="T93" id="Seg_1569" s="T92">n-n:obl.poss-n:case</ta>
            <ta e="T94" id="Seg_1570" s="T93">pers-n:ins-n:num.[n:case]</ta>
            <ta e="T95" id="Seg_1571" s="T94">num-n:case</ta>
            <ta e="T96" id="Seg_1572" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_1573" s="T96">n.[n:case]</ta>
            <ta e="T98" id="Seg_1574" s="T97">v-v:tense-v:mood-v:pn</ta>
            <ta e="T99" id="Seg_1575" s="T98">conj</ta>
            <ta e="T100" id="Seg_1576" s="T99">n-n:case-n:poss</ta>
            <ta e="T101" id="Seg_1577" s="T100">v-v:ins-v:pn</ta>
            <ta e="T102" id="Seg_1578" s="T101">adv</ta>
            <ta e="T103" id="Seg_1579" s="T102">conj</ta>
            <ta e="T104" id="Seg_1580" s="T103">v-v:inf</ta>
            <ta e="T105" id="Seg_1581" s="T104">n-n-adv:case</ta>
            <ta e="T106" id="Seg_1582" s="T105">adv</ta>
            <ta e="T107" id="Seg_1583" s="T106">v-v:ins-v:pn</ta>
            <ta e="T108" id="Seg_1584" s="T107">emphpro</ta>
            <ta e="T109" id="Seg_1585" s="T108">n-n:num-n:case.poss</ta>
            <ta e="T110" id="Seg_1586" s="T109">v-v:ins-v:pn</ta>
            <ta e="T111" id="Seg_1587" s="T110">num</ta>
            <ta e="T112" id="Seg_1588" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_1589" s="T112">v-v:ins-v:tense-v:mood.[v:pn]</ta>
            <ta e="T114" id="Seg_1590" s="T113">adv</ta>
            <ta e="T115" id="Seg_1591" s="T114">v-v:ins.[v:pn]</ta>
            <ta e="T116" id="Seg_1592" s="T115">v-v:pn</ta>
            <ta e="T117" id="Seg_1593" s="T116">num</ta>
            <ta e="T118" id="Seg_1594" s="T117">n.[n:case]</ta>
            <ta e="T119" id="Seg_1595" s="T118">v-v:tense-v:mood-v:pn</ta>
            <ta e="T120" id="Seg_1596" s="T119">adv</ta>
            <ta e="T121" id="Seg_1597" s="T120">num</ta>
            <ta e="T122" id="Seg_1598" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_1599" s="T122">v-v:tense-v:mood-v:pn</ta>
            <ta e="T124" id="Seg_1600" s="T123">adv</ta>
            <ta e="T125" id="Seg_1601" s="T124">v-v:ins.[v:pn]</ta>
            <ta e="T126" id="Seg_1602" s="T125">num</ta>
            <ta e="T127" id="Seg_1603" s="T126">n.[n:case]</ta>
            <ta e="T128" id="Seg_1604" s="T127">n.[n:case]-n:poss</ta>
            <ta e="T129" id="Seg_1605" s="T128">v-v:mood-v:pn</ta>
            <ta e="T130" id="Seg_1606" s="T129">v-v:pn</ta>
            <ta e="T131" id="Seg_1607" s="T130">n-n-n:case</ta>
            <ta e="T132" id="Seg_1608" s="T131">adv</ta>
            <ta e="T133" id="Seg_1609" s="T132">v-v:ins-v:pn</ta>
            <ta e="T134" id="Seg_1610" s="T133">n-n:num-n:case.poss</ta>
            <ta e="T135" id="Seg_1611" s="T134">v-v:ins.[v:pn]</ta>
            <ta e="T136" id="Seg_1612" s="T135">v-v:pn</ta>
            <ta e="T137" id="Seg_1613" s="T136">num</ta>
            <ta e="T138" id="Seg_1614" s="T137">n.[n:case]-n:poss</ta>
            <ta e="T139" id="Seg_1615" s="T138">v-v:ins-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1616" s="T1">num</ta>
            <ta e="T3" id="Seg_1617" s="T2">n</ta>
            <ta e="T4" id="Seg_1618" s="T3">v</ta>
            <ta e="T5" id="Seg_1619" s="T4">pers</ta>
            <ta e="T6" id="Seg_1620" s="T5">n</ta>
            <ta e="T7" id="Seg_1621" s="T6">nprop</ta>
            <ta e="T8" id="Seg_1622" s="T7">pers</ta>
            <ta e="T9" id="Seg_1623" s="T8">num</ta>
            <ta e="T10" id="Seg_1624" s="T9">n</ta>
            <ta e="T11" id="Seg_1625" s="T10">num</ta>
            <ta e="T12" id="Seg_1626" s="T11">n</ta>
            <ta e="T13" id="Seg_1627" s="T12">n</ta>
            <ta e="T14" id="Seg_1628" s="T13">n</ta>
            <ta e="T15" id="Seg_1629" s="T14">v</ta>
            <ta e="T16" id="Seg_1630" s="T15">n</ta>
            <ta e="T17" id="Seg_1631" s="T16">v</ta>
            <ta e="T18" id="Seg_1632" s="T17">n</ta>
            <ta e="T19" id="Seg_1633" s="T18">pers</ta>
            <ta e="T20" id="Seg_1634" s="T19">v</ta>
            <ta e="T21" id="Seg_1635" s="T20">nprop</ta>
            <ta e="T22" id="Seg_1636" s="T21">pers</ta>
            <ta e="T23" id="Seg_1637" s="T22">n</ta>
            <ta e="T24" id="Seg_1638" s="T23">conj</ta>
            <ta e="T25" id="Seg_1639" s="T24">n</ta>
            <ta e="T26" id="Seg_1640" s="T25">n</ta>
            <ta e="T27" id="Seg_1641" s="T26">adv</ta>
            <ta e="T28" id="Seg_1642" s="T27">v</ta>
            <ta e="T29" id="Seg_1643" s="T28">num</ta>
            <ta e="T30" id="Seg_1644" s="T29">n</ta>
            <ta e="T31" id="Seg_1645" s="T30">v</ta>
            <ta e="T33" id="Seg_1646" s="T32">n</ta>
            <ta e="T34" id="Seg_1647" s="T33">dem</ta>
            <ta e="T35" id="Seg_1648" s="T34">n</ta>
            <ta e="T36" id="Seg_1649" s="T35">n</ta>
            <ta e="T37" id="Seg_1650" s="T36">v</ta>
            <ta e="T38" id="Seg_1651" s="T37">adv</ta>
            <ta e="T39" id="Seg_1652" s="T38">v</ta>
            <ta e="T40" id="Seg_1653" s="T39">adv</ta>
            <ta e="T41" id="Seg_1654" s="T40">adv</ta>
            <ta e="T42" id="Seg_1655" s="T41">v</ta>
            <ta e="T43" id="Seg_1656" s="T42">n</ta>
            <ta e="T44" id="Seg_1657" s="T43">v</ta>
            <ta e="T45" id="Seg_1658" s="T44">n</ta>
            <ta e="T46" id="Seg_1659" s="T45">adv</ta>
            <ta e="T47" id="Seg_1660" s="T46">num</ta>
            <ta e="T48" id="Seg_1661" s="T47">n</ta>
            <ta e="T49" id="Seg_1662" s="T48">n</ta>
            <ta e="T50" id="Seg_1663" s="T49">adv</ta>
            <ta e="T51" id="Seg_1664" s="T50">v</ta>
            <ta e="T52" id="Seg_1665" s="T51">pers</ta>
            <ta e="T53" id="Seg_1666" s="T52">v</ta>
            <ta e="T54" id="Seg_1667" s="T53">conj</ta>
            <ta e="T55" id="Seg_1668" s="T54">pers</ta>
            <ta e="T56" id="Seg_1669" s="T55">v</ta>
            <ta e="T57" id="Seg_1670" s="T56">n</ta>
            <ta e="T58" id="Seg_1671" s="T57">v</ta>
            <ta e="T59" id="Seg_1672" s="T58">v</ta>
            <ta e="T60" id="Seg_1673" s="T59">n</ta>
            <ta e="T61" id="Seg_1674" s="T60">v</ta>
            <ta e="T62" id="Seg_1675" s="T61">v</ta>
            <ta e="T63" id="Seg_1676" s="T62">v</ta>
            <ta e="T64" id="Seg_1677" s="T63">n</ta>
            <ta e="T65" id="Seg_1678" s="T64">adv</ta>
            <ta e="T66" id="Seg_1679" s="T65">v</ta>
            <ta e="T67" id="Seg_1680" s="T66">dem</ta>
            <ta e="T68" id="Seg_1681" s="T67">n</ta>
            <ta e="T69" id="Seg_1682" s="T68">num</ta>
            <ta e="T70" id="Seg_1683" s="T69">n</ta>
            <ta e="T71" id="Seg_1684" s="T70">v</ta>
            <ta e="T72" id="Seg_1685" s="T71">adv</ta>
            <ta e="T73" id="Seg_1686" s="T72">v</ta>
            <ta e="T74" id="Seg_1687" s="T73">adv</ta>
            <ta e="T75" id="Seg_1688" s="T74">v</ta>
            <ta e="T76" id="Seg_1689" s="T75">adj</ta>
            <ta e="T77" id="Seg_1690" s="T76">n</ta>
            <ta e="T78" id="Seg_1691" s="T77">n</ta>
            <ta e="T79" id="Seg_1692" s="T78">adv</ta>
            <ta e="T80" id="Seg_1693" s="T79">v</ta>
            <ta e="T81" id="Seg_1694" s="T80">n</ta>
            <ta e="T82" id="Seg_1695" s="T81">v</ta>
            <ta e="T83" id="Seg_1696" s="T82">num</ta>
            <ta e="T84" id="Seg_1697" s="T83">n</ta>
            <ta e="T85" id="Seg_1698" s="T84">v</ta>
            <ta e="T86" id="Seg_1699" s="T85">adv</ta>
            <ta e="T87" id="Seg_1700" s="T86">num</ta>
            <ta e="T88" id="Seg_1701" s="T87">n</ta>
            <ta e="T89" id="Seg_1702" s="T88">v</ta>
            <ta e="T90" id="Seg_1703" s="T89">adv</ta>
            <ta e="T91" id="Seg_1704" s="T90">adv</ta>
            <ta e="T92" id="Seg_1705" s="T91">v</ta>
            <ta e="T93" id="Seg_1706" s="T92">n</ta>
            <ta e="T94" id="Seg_1707" s="T93">pers</ta>
            <ta e="T95" id="Seg_1708" s="T94">num</ta>
            <ta e="T96" id="Seg_1709" s="T95">n</ta>
            <ta e="T97" id="Seg_1710" s="T96">n</ta>
            <ta e="T98" id="Seg_1711" s="T97">v</ta>
            <ta e="T99" id="Seg_1712" s="T98">conj</ta>
            <ta e="T100" id="Seg_1713" s="T99">n</ta>
            <ta e="T101" id="Seg_1714" s="T100">v</ta>
            <ta e="T102" id="Seg_1715" s="T101">adv</ta>
            <ta e="T103" id="Seg_1716" s="T102">conj</ta>
            <ta e="T104" id="Seg_1717" s="T103">v</ta>
            <ta e="T105" id="Seg_1718" s="T104">adv</ta>
            <ta e="T106" id="Seg_1719" s="T105">adv</ta>
            <ta e="T107" id="Seg_1720" s="T106">v</ta>
            <ta e="T108" id="Seg_1721" s="T107">emphpro</ta>
            <ta e="T109" id="Seg_1722" s="T108">n</ta>
            <ta e="T110" id="Seg_1723" s="T109">v</ta>
            <ta e="T111" id="Seg_1724" s="T110">num</ta>
            <ta e="T112" id="Seg_1725" s="T111">n</ta>
            <ta e="T113" id="Seg_1726" s="T112">v</ta>
            <ta e="T114" id="Seg_1727" s="T113">adv</ta>
            <ta e="T115" id="Seg_1728" s="T114">v</ta>
            <ta e="T116" id="Seg_1729" s="T115">v</ta>
            <ta e="T117" id="Seg_1730" s="T116">num</ta>
            <ta e="T118" id="Seg_1731" s="T117">n</ta>
            <ta e="T119" id="Seg_1732" s="T118">v</ta>
            <ta e="T120" id="Seg_1733" s="T119">adv</ta>
            <ta e="T121" id="Seg_1734" s="T120">num</ta>
            <ta e="T122" id="Seg_1735" s="T121">n</ta>
            <ta e="T123" id="Seg_1736" s="T122">v</ta>
            <ta e="T124" id="Seg_1737" s="T123">adv</ta>
            <ta e="T125" id="Seg_1738" s="T124">v</ta>
            <ta e="T126" id="Seg_1739" s="T125">num</ta>
            <ta e="T127" id="Seg_1740" s="T126">n</ta>
            <ta e="T128" id="Seg_1741" s="T127">n</ta>
            <ta e="T129" id="Seg_1742" s="T128">v</ta>
            <ta e="T130" id="Seg_1743" s="T129">v</ta>
            <ta e="T131" id="Seg_1744" s="T130">n</ta>
            <ta e="T132" id="Seg_1745" s="T131">adv</ta>
            <ta e="T133" id="Seg_1746" s="T132">v</ta>
            <ta e="T134" id="Seg_1747" s="T133">n</ta>
            <ta e="T135" id="Seg_1748" s="T134">v</ta>
            <ta e="T136" id="Seg_1749" s="T135">v</ta>
            <ta e="T137" id="Seg_1750" s="T136">num</ta>
            <ta e="T138" id="Seg_1751" s="T137">n</ta>
            <ta e="T139" id="Seg_1752" s="T138">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1753" s="T2">np.h:Th</ta>
            <ta e="T5" id="Seg_1754" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_1755" s="T5">np:Th</ta>
            <ta e="T8" id="Seg_1756" s="T7">pro.h:Poss</ta>
            <ta e="T10" id="Seg_1757" s="T9">np.h:Th </ta>
            <ta e="T12" id="Seg_1758" s="T11">np.h:Th</ta>
            <ta e="T13" id="Seg_1759" s="T12">np.h:A</ta>
            <ta e="T14" id="Seg_1760" s="T13">np:Com</ta>
            <ta e="T16" id="Seg_1761" s="T15">np:Com</ta>
            <ta e="T18" id="Seg_1762" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_1763" s="T18">pro.h:A</ta>
            <ta e="T21" id="Seg_1764" s="T20">np.h:P</ta>
            <ta e="T22" id="Seg_1765" s="T21">pro.h:Poss</ta>
            <ta e="T23" id="Seg_1766" s="T22">np.h:Th</ta>
            <ta e="T25" id="Seg_1767" s="T24">np.h:Th </ta>
            <ta e="T26" id="Seg_1768" s="T25">np:G</ta>
            <ta e="T28" id="Seg_1769" s="T27">0.3.h:A</ta>
            <ta e="T30" id="Seg_1770" s="T29">np:Th</ta>
            <ta e="T31" id="Seg_1771" s="T30">0.3.h:A</ta>
            <ta e="T35" id="Seg_1772" s="T34">np.h:A</ta>
            <ta e="T36" id="Seg_1773" s="T35">np.h:A</ta>
            <ta e="T43" id="Seg_1774" s="T42">np:Ins</ta>
            <ta e="T44" id="Seg_1775" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_1776" s="T44">np:P</ta>
            <ta e="T48" id="Seg_1777" s="T47">np:Time</ta>
            <ta e="T49" id="Seg_1778" s="T48">np.h:R 0.3.h:Poss</ta>
            <ta e="T51" id="Seg_1779" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1780" s="T51">pro.h:Th</ta>
            <ta e="T55" id="Seg_1781" s="T54">pro.h:A</ta>
            <ta e="T57" id="Seg_1782" s="T56">np:Th</ta>
            <ta e="T59" id="Seg_1783" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_1784" s="T59">np.h:Th 0.3.h:Poss</ta>
            <ta e="T63" id="Seg_1785" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_1786" s="T63">np:Th</ta>
            <ta e="T66" id="Seg_1787" s="T65">0.3.h:A</ta>
            <ta e="T68" id="Seg_1788" s="T67">np:Time</ta>
            <ta e="T70" id="Seg_1789" s="T69">np:P</ta>
            <ta e="T71" id="Seg_1790" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_1791" s="T71">adv:G</ta>
            <ta e="T73" id="Seg_1792" s="T72">0.3.h:A</ta>
            <ta e="T78" id="Seg_1793" s="T77">np:Time</ta>
            <ta e="T80" id="Seg_1794" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1795" s="T80">np:G 0.3.h:Poss</ta>
            <ta e="T82" id="Seg_1796" s="T81">0.3.h:A</ta>
            <ta e="T84" id="Seg_1797" s="T83">np:P</ta>
            <ta e="T85" id="Seg_1798" s="T84">0.3.h:A</ta>
            <ta e="T88" id="Seg_1799" s="T87">np:P</ta>
            <ta e="T89" id="Seg_1800" s="T88">0.3.h:A</ta>
            <ta e="T91" id="Seg_1801" s="T90">adv:G</ta>
            <ta e="T92" id="Seg_1802" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_1803" s="T92">np:Com 0.3.h:Poss</ta>
            <ta e="T94" id="Seg_1804" s="T93">pro.h:A</ta>
            <ta e="T96" id="Seg_1805" s="T95">np:Poss</ta>
            <ta e="T97" id="Seg_1806" s="T96">np:P</ta>
            <ta e="T100" id="Seg_1807" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_1808" s="T100">0.3.h:A</ta>
            <ta e="T105" id="Seg_1809" s="T104">adv:Time</ta>
            <ta e="T107" id="Seg_1810" s="T106">0.3.h:A</ta>
            <ta e="T109" id="Seg_1811" s="T108">np:G 0.3.h:Poss</ta>
            <ta e="T110" id="Seg_1812" s="T109">0.3.h:A</ta>
            <ta e="T112" id="Seg_1813" s="T111">np:Th</ta>
            <ta e="T115" id="Seg_1814" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_1815" s="T115">0.3:Th</ta>
            <ta e="T118" id="Seg_1816" s="T117">np:P</ta>
            <ta e="T119" id="Seg_1817" s="T118">0.3.h:A</ta>
            <ta e="T122" id="Seg_1818" s="T121">np:P</ta>
            <ta e="T123" id="Seg_1819" s="T122">0.3.h:A</ta>
            <ta e="T124" id="Seg_1820" s="T123">adv:G</ta>
            <ta e="T125" id="Seg_1821" s="T124">0.3.h:A</ta>
            <ta e="T128" id="Seg_1822" s="T127">np:P</ta>
            <ta e="T129" id="Seg_1823" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_1824" s="T129">0.3.h:Th</ta>
            <ta e="T131" id="Seg_1825" s="T130">np:Time</ta>
            <ta e="T133" id="Seg_1826" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_1827" s="T133">np:G 0.3.h:Poss</ta>
            <ta e="T135" id="Seg_1828" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_1829" s="T135">0.3:Th</ta>
            <ta e="T138" id="Seg_1830" s="T137">np:Th 0.1.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_1831" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_1832" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_1833" s="T5">np:S</ta>
            <ta e="T7" id="Seg_1834" s="T6">n:pred</ta>
            <ta e="T10" id="Seg_1835" s="T9">np.h:S</ta>
            <ta e="T12" id="Seg_1836" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_1837" s="T12">np.h:S</ta>
            <ta e="T15" id="Seg_1838" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_1839" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_1840" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_1841" s="T18">pro.h:S</ta>
            <ta e="T20" id="Seg_1842" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1843" s="T20">np.h:O</ta>
            <ta e="T23" id="Seg_1844" s="T22">np.h:O</ta>
            <ta e="T25" id="Seg_1845" s="T24">np.h:O</ta>
            <ta e="T27" id="Seg_1846" s="T25">s:temp</ta>
            <ta e="T28" id="Seg_1847" s="T27">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_1848" s="T29">np:O</ta>
            <ta e="T31" id="Seg_1849" s="T30">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_1850" s="T34">np.h:S</ta>
            <ta e="T36" id="Seg_1851" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_1852" s="T36">v:pred</ta>
            <ta e="T39" id="Seg_1853" s="T38">v:pred</ta>
            <ta e="T44" id="Seg_1854" s="T43">0.3.h:S v:pred</ta>
            <ta e="T45" id="Seg_1855" s="T44">np:O</ta>
            <ta e="T51" id="Seg_1856" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_1857" s="T51">pro.h:S</ta>
            <ta e="T53" id="Seg_1858" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_1859" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_1860" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_1861" s="T56">s:purp</ta>
            <ta e="T59" id="Seg_1862" s="T58">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_1863" s="T59">np.h:S</ta>
            <ta e="T61" id="Seg_1864" s="T60">s:temp</ta>
            <ta e="T62" id="Seg_1865" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1866" s="T62">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_1867" s="T63">np:O</ta>
            <ta e="T65" id="Seg_1868" s="T64">s:temp</ta>
            <ta e="T66" id="Seg_1869" s="T65">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_1870" s="T69">np:O</ta>
            <ta e="T71" id="Seg_1871" s="T70">0.3.h:S v:pred</ta>
            <ta e="T73" id="Seg_1872" s="T72">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_1873" s="T79">0.3.h:S v:pred</ta>
            <ta e="T82" id="Seg_1874" s="T81">0.3.h:S v:pred</ta>
            <ta e="T84" id="Seg_1875" s="T83">np:O</ta>
            <ta e="T85" id="Seg_1876" s="T84">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_1877" s="T87">np:O</ta>
            <ta e="T89" id="Seg_1878" s="T88">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_1879" s="T91">0.3.h:S v:pred</ta>
            <ta e="T94" id="Seg_1880" s="T93">pro.h:S</ta>
            <ta e="T97" id="Seg_1881" s="T96">np:O</ta>
            <ta e="T98" id="Seg_1882" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_1883" s="T99">np:O</ta>
            <ta e="T101" id="Seg_1884" s="T100">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_1885" s="T101">s:purp</ta>
            <ta e="T107" id="Seg_1886" s="T106">0.3.h:S v:pred</ta>
            <ta e="T110" id="Seg_1887" s="T109">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_1888" s="T111">np:S</ta>
            <ta e="T113" id="Seg_1889" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_1890" s="T114">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_1891" s="T115">0.3:S v:pred</ta>
            <ta e="T118" id="Seg_1892" s="T117">np:O</ta>
            <ta e="T119" id="Seg_1893" s="T118">0.3.h:S v:pred</ta>
            <ta e="T122" id="Seg_1894" s="T121">np:O</ta>
            <ta e="T123" id="Seg_1895" s="T122">0.3.h:S v:pred</ta>
            <ta e="T125" id="Seg_1896" s="T124">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_1897" s="T127">np:O</ta>
            <ta e="T129" id="Seg_1898" s="T128">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_1899" s="T129">0.3.h:S v:pred</ta>
            <ta e="T133" id="Seg_1900" s="T132">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_1901" s="T134">0.3.h:S v:pred</ta>
            <ta e="T136" id="Seg_1902" s="T135">0.3:S v:pred</ta>
            <ta e="T138" id="Seg_1903" s="T137">np:S</ta>
            <ta e="T139" id="Seg_1904" s="T138">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T24" id="Seg_1905" s="T23">RUS:gram</ta>
            <ta e="T43" id="Seg_1906" s="T42">RUS:core</ta>
            <ta e="T54" id="Seg_1907" s="T53">RUS:gram</ta>
            <ta e="T57" id="Seg_1908" s="T56">RUS:core</ta>
            <ta e="T84" id="Seg_1909" s="T83">RUS:core</ta>
            <ta e="T96" id="Seg_1910" s="T95">RUS:core</ta>
            <ta e="T99" id="Seg_1911" s="T98">RUS:gram</ta>
            <ta e="T103" id="Seg_1912" s="T102">RUS:gram</ta>
            <ta e="T118" id="Seg_1913" s="T117">RUS:core</ta>
            <ta e="T120" id="Seg_1914" s="T119">RUS:core</ta>
            <ta e="T127" id="Seg_1915" s="T126">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1916" s="T1">There lived one man, his name was Tyssija.</ta>
            <ta e="T12" id="Seg_1917" s="T7">He had one son and one daughter.</ta>
            <ta e="T15" id="Seg_1918" s="T12">Tunguses came to fight.</ta>
            <ta e="T18" id="Seg_1919" s="T15">Tunguses came to fight.</ta>
            <ta e="T21" id="Seg_1920" s="T18">They killed Tyssija.</ta>
            <ta e="T28" id="Seg_1921" s="T21">His son and his daughter were put into a house.</ta>
            <ta e="T33" id="Seg_1922" s="T28">they left one reindeer (reverse((unknown)) hair).</ta>
            <ta e="T42" id="Seg_1923" s="T33">After the Tunguses had left, the son began to live (so).</ta>
            <ta e="T45" id="Seg_1924" s="T42">He made a house from firtree.</ta>
            <ta e="T58" id="Seg_1925" s="T45">One morning she said so to his sister: “You stay here, I'll go and look for partridges.”</ta>
            <ta e="T59" id="Seg_1926" s="T58">He left.</ta>
            <ta e="T62" id="Seg_1927" s="T59">His sister stayed crying.</ta>
            <ta e="T63" id="Seg_1928" s="T62">He left.</ta>
            <ta e="T66" id="Seg_1929" s="T63">He was making traps.</ta>
            <ta e="T71" id="Seg_1930" s="T66">That day he made three traps.</ta>
            <ta e="T75" id="Seg_1931" s="T71">He came back.</ta>
            <ta e="T81" id="Seg_1932" s="T75">Next morning he went again to his traps.</ta>
            <ta e="T85" id="Seg_1933" s="T81">He came and caught one partridge.</ta>
            <ta e="T89" id="Seg_1934" s="T85">He made three traps again.</ta>
            <ta e="T92" id="Seg_1935" s="T89">He came back again.</ta>
            <ta e="T98" id="Seg_1936" s="T92">He ate half of the partridge together with his sister.</ta>
            <ta e="T104" id="Seg_1937" s="T98">And they put aside another half, to eat it later.</ta>
            <ta e="T109" id="Seg_1938" s="T104">Next morning he went to his traps again.</ta>
            <ta e="T113" id="Seg_1939" s="T109">He came: two traps had fallen.</ta>
            <ta e="T119" id="Seg_1940" s="T113">He came here and caught two partridges.</ta>
            <ta e="T123" id="Seg_1941" s="T119">He made three traps again.</ta>
            <ta e="T125" id="Seg_1942" s="T123">He came back.</ta>
            <ta e="T129" id="Seg_1943" s="T125">They ate half of their partridge.</ta>
            <ta e="T130" id="Seg_1944" s="T129">They spent a night.</ta>
            <ta e="T134" id="Seg_1945" s="T130">Next morning he went to his traps again.</ta>
            <ta e="T139" id="Seg_1946" s="T134">He came: it seems that three of my traps had fallen.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1947" s="T1">Es lebte ein Mann, sein Name war Tyssija.</ta>
            <ta e="T12" id="Seg_1948" s="T7">Er hatte einen Sohn und eine Tochter.</ta>
            <ta e="T15" id="Seg_1949" s="T12">Tungusen kamen um zu kämpfen.</ta>
            <ta e="T18" id="Seg_1950" s="T15">Tungusen kamen um zu kämpfen.</ta>
            <ta e="T21" id="Seg_1951" s="T18">Sie töteten Tyssija.</ta>
            <ta e="T28" id="Seg_1952" s="T21">Sein Sohn und seine Tochter wurden in ein Haus gesperrt.</ta>
            <ta e="T33" id="Seg_1953" s="T28">Sie ließen ein Rentier zurück.</ta>
            <ta e="T42" id="Seg_1954" s="T33">Als die Tungusier gegangen waren, begann der Sohn so zu leben.</ta>
            <ta e="T45" id="Seg_1955" s="T42">Er baute ein Haus aus Tannenholz.</ta>
            <ta e="T58" id="Seg_1956" s="T45">Eines Morgens sagte er so zu seiner Schwester: "Du bleibst hier, ich gehe, um nach Rebhühnern zu suchen."</ta>
            <ta e="T59" id="Seg_1957" s="T58">Er ging.</ta>
            <ta e="T62" id="Seg_1958" s="T59">Seine Schwester begann zu weinen.</ta>
            <ta e="T63" id="Seg_1959" s="T62">Er ging.</ta>
            <ta e="T66" id="Seg_1960" s="T63">Er legte Fallen.</ta>
            <ta e="T71" id="Seg_1961" s="T66">An diesem Tag legte er drei Fallen.</ta>
            <ta e="T75" id="Seg_1962" s="T71">Er kam zurück.</ta>
            <ta e="T81" id="Seg_1963" s="T75">Am nächsten Tag ging er wieder zu seinen Fallen.</ta>
            <ta e="T85" id="Seg_1964" s="T81">Er kam und fing ein Rebhuhn.</ta>
            <ta e="T89" id="Seg_1965" s="T85">Er legte wieder drei Fallen.</ta>
            <ta e="T92" id="Seg_1966" s="T89">Er kam wieder zurück.</ta>
            <ta e="T98" id="Seg_1967" s="T92">Mit seiner Schwester aß er die Hälfte des Rebhuhns.</ta>
            <ta e="T104" id="Seg_1968" s="T98">Und sie legten die andere Hälfte zur Seite um sie später zu essen.</ta>
            <ta e="T109" id="Seg_1969" s="T104">Am nächsten Morgen ging er wieder zu seinen Fallen.</ta>
            <ta e="T113" id="Seg_1970" s="T109">Er kam: Zwei Fallen waren zugeschnappt.</ta>
            <ta e="T119" id="Seg_1971" s="T113">Er kam hierher und fing zwei Rebhühner.</ta>
            <ta e="T123" id="Seg_1972" s="T119">Er legte wieder drei Fallen.</ta>
            <ta e="T125" id="Seg_1973" s="T123">Er kam zurück.</ta>
            <ta e="T129" id="Seg_1974" s="T125">Sie aßen die Hälfte ihres Rebhuhns.</ta>
            <ta e="T130" id="Seg_1975" s="T129">Sie verbrachten die Nacht.</ta>
            <ta e="T134" id="Seg_1976" s="T130">Am nächsten Morgen ging er wieder zu seinen Fallen.</ta>
            <ta e="T139" id="Seg_1977" s="T134">Er kam: Es scheint, dass meine drei Fallen zugeschnappt sind.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1978" s="T1">Один человек жил, имя его Тыссия.</ta>
            <ta e="T12" id="Seg_1979" s="T7">Один сын у него и одна дочь у него.</ta>
            <ta e="T15" id="Seg_1980" s="T12">Тунгусы с войной пришли.</ta>
            <ta e="T18" id="Seg_1981" s="T15">Тунгусы с войной пришли.</ta>
            <ta e="T21" id="Seg_1982" s="T18">Они убили Тыссия.</ta>
            <ta e="T28" id="Seg_1983" s="T21">Его сына и дочь в избу посадили.</ta>
            <ta e="T33" id="Seg_1984" s="T28">Одного оленя оставили (обратная? шерсть).</ta>
            <ta e="T42" id="Seg_1985" s="T33">Этот сын, (после того как) тунгусы ушли, жить стал (так жить стал).</ta>
            <ta e="T45" id="Seg_1986" s="T42">Дом из елок сделал.</ta>
            <ta e="T58" id="Seg_1987" s="T45">Затем однажды утром своей сестре так сказал: “Ты сиди, я куропаток поглядеть схожу”.</ta>
            <ta e="T59" id="Seg_1988" s="T58">Пошел.</ta>
            <ta e="T62" id="Seg_1989" s="T59">Его сестра, заплакав, осталась.</ta>
            <ta e="T63" id="Seg_1990" s="T62">Он пошел.</ta>
            <ta e="T66" id="Seg_1991" s="T63">Пасти делает.</ta>
            <ta e="T71" id="Seg_1992" s="T66">В этот день три пасти сделал.</ta>
            <ta e="T75" id="Seg_1993" s="T71">Назад пришел (назад пришел).</ta>
            <ta e="T81" id="Seg_1994" s="T75">На следующее утро опять пошел к пастям своим.</ta>
            <ta e="T85" id="Seg_1995" s="T81">Пришел, одну куропатку добыл.</ta>
            <ta e="T89" id="Seg_1996" s="T85">Опять три пасти сделал.</ta>
            <ta e="T92" id="Seg_1997" s="T89">Опять обратно пришел.</ta>
            <ta e="T98" id="Seg_1998" s="T92">С сестрой они вдвоем половину куропатки съели.</ta>
            <ta e="T104" id="Seg_1999" s="T98">А половину положили, чтобы потом съесть.</ta>
            <ta e="T109" id="Seg_2000" s="T104">На следующее утро опять пошел к своим пастям.</ta>
            <ta e="T113" id="Seg_2001" s="T109">Пришел: видать, две пасти упали.</ta>
            <ta e="T119" id="Seg_2002" s="T113">Сюда лишь пришел, видать, двух куропаток добыл.</ta>
            <ta e="T123" id="Seg_2003" s="T119">Опять три пасти сделал.</ta>
            <ta e="T125" id="Seg_2004" s="T123">Обратно пришел.</ta>
            <ta e="T129" id="Seg_2005" s="T125">Одной куропатки своей половину съели.</ta>
            <ta e="T130" id="Seg_2006" s="T129">Переночевали.</ta>
            <ta e="T134" id="Seg_2007" s="T130">На следующее утро к пастям своим опять пошел.</ta>
            <ta e="T139" id="Seg_2008" s="T134">Пришел, видать, три пасти мои упали.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_2009" s="T1">один человек жил жил имя его Тыссыя</ta>
            <ta e="T12" id="Seg_2010" s="T7">один сын у него и одна дочь у него</ta>
            <ta e="T15" id="Seg_2011" s="T12">тунгусы с войной пришли</ta>
            <ta e="T18" id="Seg_2012" s="T15">тунгусы с войной пришли</ta>
            <ta e="T21" id="Seg_2013" s="T18">убили они Тыссыя</ta>
            <ta e="T28" id="Seg_2014" s="T21">его сына и дочь в избу посадили (оставили)</ta>
            <ta e="T33" id="Seg_2015" s="T28">одну олень оставили (обратная шерсть)</ta>
            <ta e="T42" id="Seg_2016" s="T33">этот сын после того как ненцы ушли так жить стал</ta>
            <ta e="T45" id="Seg_2017" s="T42">дом из елок сделал</ta>
            <ta e="T58" id="Seg_2018" s="T45">затем однажды утром так сказал: ты сиди, я куропаток поглядеть схожу</ta>
            <ta e="T59" id="Seg_2019" s="T58">пошел</ta>
            <ta e="T62" id="Seg_2020" s="T59">сестра заплакав осталась</ta>
            <ta e="T63" id="Seg_2021" s="T62">пошел</ta>
            <ta e="T66" id="Seg_2022" s="T63">клипцы (пасти) делает</ta>
            <ta e="T71" id="Seg_2023" s="T66">в этот день три пасти сделал</ta>
            <ta e="T75" id="Seg_2024" s="T71">назад пришел</ta>
            <ta e="T81" id="Seg_2025" s="T75">на следующее утро опять пошел к пастям своим</ta>
            <ta e="T85" id="Seg_2026" s="T81">лишь пришел одну куропатку добыл</ta>
            <ta e="T89" id="Seg_2027" s="T85">опять три пасти сделал</ta>
            <ta e="T92" id="Seg_2028" s="T89">обратно пришел</ta>
            <ta e="T98" id="Seg_2029" s="T92">с сестрой они двое куропатки своей половину сделали</ta>
            <ta e="T104" id="Seg_2030" s="T98">половину положили - после скушать им чтобы</ta>
            <ta e="T109" id="Seg_2031" s="T104">на следующее утро опять пошел к пастям своим</ta>
            <ta e="T113" id="Seg_2032" s="T109">лишь пришел, видать две пасти мои упали</ta>
            <ta e="T119" id="Seg_2033" s="T113">сюда лишь пришел, видать двух куропаток добыл</ta>
            <ta e="T123" id="Seg_2034" s="T119">опять три пасти сделал</ta>
            <ta e="T125" id="Seg_2035" s="T123">обратно пришел</ta>
            <ta e="T129" id="Seg_2036" s="T125">одной куропатки своей половину съели</ta>
            <ta e="T130" id="Seg_2037" s="T129">переночевали</ta>
            <ta e="T134" id="Seg_2038" s="T130">на следующее утро к пастям своим опять пошел</ta>
            <ta e="T139" id="Seg_2039" s="T134">лишь пришел, видать, три пасти мои упали</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T12" id="Seg_2040" s="T7">[KuAI:] Variant: 'nät'.</ta>
            <ta e="T33" id="Seg_2041" s="T28">[KuAI:] Variant: 'qwɛdʼebattə'.</ta>
            <ta e="T42" id="Seg_2042" s="T33">[KuAI:] Variant: 'illʼe'.</ta>
            <ta e="T98" id="Seg_2043" s="T92">[KuAI:] Variant: 'sədəɣun'.</ta>
            <ta e="T119" id="Seg_2044" s="T113">[KuAI:] Variants: 'Taftʼe', 'tüwa'.</ta>
         </annotation>
         <annotation name="nto" tierref="nto">
            <ta e="T15" id="Seg_2045" s="T12">так говорят на низу, у нас тунгус наз. по-русски</ta>
            <ta e="T98" id="Seg_2046" s="T92">съели?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
