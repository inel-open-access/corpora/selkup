<?xml version="1.0" encoding="UTF-8"?>
<segmented-transcription>
   <head>
      <meta-information>
         <project-name>Selkup</project-name>
         <transcription-name>SAG_1965_FairytaleAboutDeadPrincess_transl</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">SAG_1965_TaleOfDeadPrincess_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1149</ud-information>
            <ud-information attribute-name="# HIAT:w">835</ud-information>
            <ud-information attribute-name="# e">834</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">101</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SAG">
            <abbreviation>SAG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
         <tli id="T300" />
         <tli id="T301" />
         <tli id="T302" />
         <tli id="T303" />
         <tli id="T304" />
         <tli id="T305" />
         <tli id="T306" />
         <tli id="T307" />
         <tli id="T308" />
         <tli id="T309" />
         <tli id="T310" />
         <tli id="T311" />
         <tli id="T312" />
         <tli id="T313" />
         <tli id="T314" />
         <tli id="T315" />
         <tli id="T316" />
         <tli id="T317" />
         <tli id="T318" />
         <tli id="T319" />
         <tli id="T320" />
         <tli id="T321" />
         <tli id="T322" />
         <tli id="T323" />
         <tli id="T324" />
         <tli id="T325" />
         <tli id="T326" />
         <tli id="T327" />
         <tli id="T328" />
         <tli id="T329" />
         <tli id="T330" />
         <tli id="T331" />
         <tli id="T332" />
         <tli id="T333" />
         <tli id="T334" />
         <tli id="T335" />
         <tli id="T336" />
         <tli id="T337" />
         <tli id="T338" />
         <tli id="T339" />
         <tli id="T340" />
         <tli id="T341" />
         <tli id="T342" />
         <tli id="T343" />
         <tli id="T344" />
         <tli id="T345" />
         <tli id="T346" />
         <tli id="T347" />
         <tli id="T348" />
         <tli id="T349" />
         <tli id="T350" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" />
         <tli id="T354" />
         <tli id="T355" />
         <tli id="T356" />
         <tli id="T357" />
         <tli id="T358" />
         <tli id="T359" />
         <tli id="T360" />
         <tli id="T361" />
         <tli id="T362" />
         <tli id="T363" />
         <tli id="T364" />
         <tli id="T365" />
         <tli id="T366" />
         <tli id="T367" />
         <tli id="T368" />
         <tli id="T369" />
         <tli id="T370" />
         <tli id="T371" />
         <tli id="T372" />
         <tli id="T373" />
         <tli id="T374" />
         <tli id="T375" />
         <tli id="T376" />
         <tli id="T377" />
         <tli id="T378" />
         <tli id="T379" />
         <tli id="T380" />
         <tli id="T381" />
         <tli id="T382" />
         <tli id="T383" />
         <tli id="T384" />
         <tli id="T385" />
         <tli id="T386" />
         <tli id="T387" />
         <tli id="T388" />
         <tli id="T389" />
         <tli id="T390" />
         <tli id="T391" />
         <tli id="T392" />
         <tli id="T393" />
         <tli id="T394" />
         <tli id="T395" />
         <tli id="T396" />
         <tli id="T397" />
         <tli id="T398" />
         <tli id="T399" />
         <tli id="T400" />
         <tli id="T401" />
         <tli id="T402" />
         <tli id="T403" />
         <tli id="T404" />
         <tli id="T405" />
         <tli id="T406" />
         <tli id="T407" />
         <tli id="T408" />
         <tli id="T409" />
         <tli id="T410" />
         <tli id="T411" />
         <tli id="T412" />
         <tli id="T413" />
         <tli id="T414" />
         <tli id="T415" />
         <tli id="T416" />
         <tli id="T417" />
         <tli id="T418" />
         <tli id="T419" />
         <tli id="T420" />
         <tli id="T421" />
         <tli id="T422" />
         <tli id="T423" />
         <tli id="T424" />
         <tli id="T425" />
         <tli id="T426" />
         <tli id="T427" />
         <tli id="T428" />
         <tli id="T429" />
         <tli id="T430" />
         <tli id="T431" />
         <tli id="T432" />
         <tli id="T433" />
         <tli id="T434" />
         <tli id="T435" />
         <tli id="T436" />
         <tli id="T437" />
         <tli id="T438" />
         <tli id="T439" />
         <tli id="T440" />
         <tli id="T441" />
         <tli id="T442" />
         <tli id="T443" />
         <tli id="T444" />
         <tli id="T445" />
         <tli id="T446" />
         <tli id="T447" />
         <tli id="T448" />
         <tli id="T449" />
         <tli id="T450" />
         <tli id="T451" />
         <tli id="T452" />
         <tli id="T453" />
         <tli id="T454" />
         <tli id="T455" />
         <tli id="T456" />
         <tli id="T457" />
         <tli id="T458" />
         <tli id="T459" />
         <tli id="T460" />
         <tli id="T461" />
         <tli id="T462" />
         <tli id="T463" />
         <tli id="T464" />
         <tli id="T465" />
         <tli id="T466" />
         <tli id="T467" />
         <tli id="T468" />
         <tli id="T469" />
         <tli id="T470" />
         <tli id="T471" />
         <tli id="T472" />
         <tli id="T473" />
         <tli id="T474" />
         <tli id="T475" />
         <tli id="T476" />
         <tli id="T477" />
         <tli id="T478" />
         <tli id="T479" />
         <tli id="T480" />
         <tli id="T481" />
         <tli id="T482" />
         <tli id="T483" />
         <tli id="T484" />
         <tli id="T485" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T493" />
         <tli id="T494" />
         <tli id="T495" />
         <tli id="T496" />
         <tli id="T497" />
         <tli id="T498" />
         <tli id="T499" />
         <tli id="T500" />
         <tli id="T501" />
         <tli id="T502" />
         <tli id="T503" />
         <tli id="T504" />
         <tli id="T505" />
         <tli id="T506" />
         <tli id="T507" />
         <tli id="T508" />
         <tli id="T509" />
         <tli id="T510" />
         <tli id="T511" />
         <tli id="T512" />
         <tli id="T513" />
         <tli id="T514" />
         <tli id="T515" />
         <tli id="T516" />
         <tli id="T517" />
         <tli id="T518" />
         <tli id="T519" />
         <tli id="T520" />
         <tli id="T521" />
         <tli id="T522" />
         <tli id="T523" />
         <tli id="T524" />
         <tli id="T525" />
         <tli id="T526" />
         <tli id="T527" />
         <tli id="T528" />
         <tli id="T529" />
         <tli id="T530" />
         <tli id="T531" />
         <tli id="T532" />
         <tli id="T533" />
         <tli id="T534" />
         <tli id="T535" />
         <tli id="T536" />
         <tli id="T537" />
         <tli id="T538" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" />
         <tli id="T545" />
         <tli id="T546" />
         <tli id="T547" />
         <tli id="T548" />
         <tli id="T549" />
         <tli id="T550" />
         <tli id="T551" />
         <tli id="T552" />
         <tli id="T553" />
         <tli id="T554" />
         <tli id="T555" />
         <tli id="T556" />
         <tli id="T557" />
         <tli id="T558" />
         <tli id="T559" />
         <tli id="T560" />
         <tli id="T561" />
         <tli id="T562" />
         <tli id="T563" />
         <tli id="T564" />
         <tli id="T565" />
         <tli id="T566" />
         <tli id="T567" />
         <tli id="T568" />
         <tli id="T569" />
         <tli id="T570" />
         <tli id="T571" />
         <tli id="T572" />
         <tli id="T573" />
         <tli id="T574" />
         <tli id="T575" />
         <tli id="T576" />
         <tli id="T577" />
         <tli id="T578" />
         <tli id="T579" />
         <tli id="T580" />
         <tli id="T581" />
         <tli id="T582" />
         <tli id="T583" />
         <tli id="T584" />
         <tli id="T585" />
         <tli id="T586" />
         <tli id="T587" />
         <tli id="T588" />
         <tli id="T589" />
         <tli id="T590" />
         <tli id="T591" />
         <tli id="T592" />
         <tli id="T593" />
         <tli id="T594" />
         <tli id="T595" />
         <tli id="T596" />
         <tli id="T597" />
         <tli id="T598" />
         <tli id="T599" />
         <tli id="T600" />
         <tli id="T601" />
         <tli id="T602" />
         <tli id="T603" />
         <tli id="T604" />
         <tli id="T605" />
         <tli id="T606" />
         <tli id="T607" />
         <tli id="T608" />
         <tli id="T609" />
         <tli id="T610" />
         <tli id="T611" />
         <tli id="T612" />
         <tli id="T613" />
         <tli id="T614" />
         <tli id="T615" />
         <tli id="T616" />
         <tli id="T617" />
         <tli id="T618" />
         <tli id="T619" />
         <tli id="T620" />
         <tli id="T621" />
         <tli id="T622" />
         <tli id="T623" />
         <tli id="T624" />
         <tli id="T625" />
         <tli id="T626" />
         <tli id="T627" />
         <tli id="T628" />
         <tli id="T629" />
         <tli id="T630" />
         <tli id="T631" />
         <tli id="T632" />
         <tli id="T633" />
         <tli id="T634" />
         <tli id="T635" />
         <tli id="T636" />
         <tli id="T637" />
         <tli id="T638" />
         <tli id="T639" />
         <tli id="T640" />
         <tli id="T641" />
         <tli id="T642" />
         <tli id="T643" />
         <tli id="T644" />
         <tli id="T645" />
         <tli id="T646" />
         <tli id="T647" />
         <tli id="T648" />
         <tli id="T649" />
         <tli id="T650" />
         <tli id="T651" />
         <tli id="T652" />
         <tli id="T653" />
         <tli id="T654" />
         <tli id="T655" />
         <tli id="T656" />
         <tli id="T657" />
         <tli id="T658" />
         <tli id="T659" />
         <tli id="T660" />
         <tli id="T661" />
         <tli id="T662" />
         <tli id="T663" />
         <tli id="T664" />
         <tli id="T665" />
         <tli id="T666" />
         <tli id="T667" />
         <tli id="T668" />
         <tli id="T669" />
         <tli id="T670" />
         <tli id="T671" />
         <tli id="T672" />
         <tli id="T673" />
         <tli id="T674" />
         <tli id="T675" />
         <tli id="T676" />
         <tli id="T677" />
         <tli id="T678" />
         <tli id="T679" />
         <tli id="T680" />
         <tli id="T681" />
         <tli id="T682" />
         <tli id="T683" />
         <tli id="T684" />
         <tli id="T685" />
         <tli id="T686" />
         <tli id="T687" />
         <tli id="T688" />
         <tli id="T689" />
         <tli id="T690" />
         <tli id="T691" />
         <tli id="T692" />
         <tli id="T693" />
         <tli id="T694" />
         <tli id="T695" />
         <tli id="T696" />
         <tli id="T697" />
         <tli id="T698" />
         <tli id="T699" />
         <tli id="T700" />
         <tli id="T701" />
         <tli id="T702" />
         <tli id="T703" />
         <tli id="T704" />
         <tli id="T705" />
         <tli id="T706" />
         <tli id="T707" />
         <tli id="T708" />
         <tli id="T709" />
         <tli id="T710" />
         <tli id="T711" />
         <tli id="T712" />
         <tli id="T713" />
         <tli id="T714" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" />
         <tli id="T718" />
         <tli id="T719" />
         <tli id="T720" />
         <tli id="T721" />
         <tli id="T722" />
         <tli id="T723" />
         <tli id="T724" />
         <tli id="T725" />
         <tli id="T726" />
         <tli id="T727" />
         <tli id="T728" />
         <tli id="T729" />
         <tli id="T730" />
         <tli id="T731" />
         <tli id="T732" />
         <tli id="T733" />
         <tli id="T734" />
         <tli id="T735" />
         <tli id="T736" />
         <tli id="T737" />
         <tli id="T738" />
         <tli id="T739" />
         <tli id="T740" />
         <tli id="T741" />
         <tli id="T742" />
         <tli id="T743" />
         <tli id="T744" />
         <tli id="T745" />
         <tli id="T746" />
         <tli id="T747" />
         <tli id="T748" />
         <tli id="T749" />
         <tli id="T750" />
         <tli id="T751" />
         <tli id="T752" />
         <tli id="T753" />
         <tli id="T754" />
         <tli id="T755" />
         <tli id="T756" />
         <tli id="T757" />
         <tli id="T758" />
         <tli id="T759" />
         <tli id="T760" />
         <tli id="T761" />
         <tli id="T762" />
         <tli id="T763" />
         <tli id="T764" />
         <tli id="T765" />
         <tli id="T766" />
         <tli id="T767" />
         <tli id="T768" />
         <tli id="T769" />
         <tli id="T770" />
         <tli id="T771" />
         <tli id="T772" />
         <tli id="T773" />
         <tli id="T774" />
         <tli id="T775" />
         <tli id="T776" />
         <tli id="T777" />
         <tli id="T778" />
         <tli id="T779" />
         <tli id="T780" />
         <tli id="T781" />
         <tli id="T782" />
         <tli id="T783" />
         <tli id="T784" />
         <tli id="T785" />
         <tli id="T786" />
         <tli id="T787" />
         <tli id="T788" />
         <tli id="T789" />
         <tli id="T790" />
         <tli id="T791" />
         <tli id="T792" />
         <tli id="T793" />
         <tli id="T794" />
         <tli id="T795" />
         <tli id="T796" />
         <tli id="T797" />
         <tli id="T798" />
         <tli id="T799" />
         <tli id="T800" />
         <tli id="T801" />
         <tli id="T802" />
         <tli id="T803" />
         <tli id="T804" />
         <tli id="T805" />
         <tli id="T806" />
         <tli id="T807" />
         <tli id="T808" />
         <tli id="T809" />
         <tli id="T810" />
         <tli id="T811" />
         <tli id="T812" />
         <tli id="T813" />
         <tli id="T814" />
         <tli id="T815" />
         <tli id="T816" />
         <tli id="T817" />
         <tli id="T818" />
         <tli id="T819" />
         <tli id="T820" />
         <tli id="T821" />
         <tli id="T822" />
         <tli id="T823" />
         <tli id="T824" />
         <tli id="T825" />
         <tli id="T826" />
         <tli id="T827" />
         <tli id="T828" />
         <tli id="T829" />
         <tli id="T830" />
         <tli id="T831" />
         <tli id="T832" />
         <tli id="T833" />
         <tli id="T834" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SAG"
                      type="t">
         <timeline-fork end="T202" start="T201">
            <tli id="T201.tx.1" />
         </timeline-fork>
         <timeline-fork end="T221" start="T220">
            <tli id="T220.tx.1" />
         </timeline-fork>
         <timeline-fork end="T528" start="T527">
            <tli id="T527.tx.1" />
         </timeline-fork>
         <timeline-fork end="T591" start="T590">
            <tli id="T590.tx.1" />
         </timeline-fork>
         <timeline-fork end="T627" start="T626">
            <tli id="T626.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T834" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Sar</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">imantɨsa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nɛtɨrsɨtɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <nts id="Seg_13" n="HIAT:ip">/</nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">nɛkärsɨ</ts>
                  <nts id="Seg_16" n="HIAT:ip">)</nts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">kuntak</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">wəttont</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">toqtaltɨŋa</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_30" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">Oknot</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">qantɨ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">imatɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">tɛpɨp</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">ɛtɨqo</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">ontɨ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">omta</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Qarɨn</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">nɔːnɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">üːtɨt</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">tɛtɨ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">ɛtɨkkɨtɨ</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">ɛtɨkkɨtɨ</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">meːltɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">limont</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">mannɨmpa</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">tɛp</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_89" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">Sailʼ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">tɔːktɨ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">mantalpɨlʼa</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">čʼüšalɨsɔːt</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">kutɨlʼ</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">ɛːmɨlʼa</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">qarɨlʼ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">kɛnnɨnt</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">uːkoːqɨnɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">pit</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">tɛt</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_126" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">Ɔːmɨ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">neːtɨ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">aša</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">ata</ts>
                  <nts id="Seg_138" n="HIAT:ip">!</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_141" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">Kekɨsä</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">təp</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">mannɨmpatɨ</ts>
                  <nts id="Seg_150" n="HIAT:ip">:</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">mɛrkɨlʼ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">palʼčʼa</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">küčʼinimpa</ts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">sɨrɨ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_166" n="HIAT:w" s="T46">čʼomna</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_169" n="HIAT:w" s="T47">limɨt</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_172" n="HIAT:w" s="T48">iːntɨ</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_176" n="HIAT:w" s="T49">təttɨ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_179" n="HIAT:w" s="T50">muntɨk</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_182" n="HIAT:w" s="T51">sɛrɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_185" n="HIAT:w" s="T52">ɛsa</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_189" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">Qənna</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_193" n="HIAT:ip">(</nts>
                  <nts id="Seg_194" n="HIAT:ip">/</nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">qänna</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">okkɨr</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">čʼäː</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">köt</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">irät</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_213" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">Limoːqɨn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">saimtɨ</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_221" n="HIAT:w" s="T61">alpa</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_224" n="HIAT:w" s="T62">ašša</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_227" n="HIAT:w" s="T63">iːtɨt</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_229" n="HIAT:ip">(</nts>
                  <nts id="Seg_230" n="HIAT:ip">/</nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">iːtɨŋɨt</ts>
                  <nts id="Seg_233" n="HIAT:ip">)</nts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_237" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">Nʼɔːt</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">sočʼelʼnikaqɨt</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">pit</ts>
                  <nts id="Seg_247" n="HIAT:ip">,</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">təpɨn</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">minɨt</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">nälʼam</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_258" n="HIAT:ip">(</nts>
                  <nts id="Seg_259" n="HIAT:ip">/</nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">nälʼamtɨ</ts>
                  <nts id="Seg_262" n="HIAT:ip">)</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">nom</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_267" n="HIAT:ip">(</nts>
                  <nts id="Seg_268" n="HIAT:ip">/</nts>
                  <ts e="T74" id="Seg_270" n="HIAT:w" s="T73">nomtɨ</ts>
                  <nts id="Seg_271" n="HIAT:ip">)</nts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_275" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_277" n="HIAT:w" s="T74">Rɛmkɨ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_280" n="HIAT:w" s="T75">qarɨt</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_283" n="HIAT:w" s="T76">nɔːtnɨlʼ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_286" n="HIAT:w" s="T77">gostʼ</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">pisa</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">čʼeːlɨsa</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">kuntɨ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_299" n="HIAT:w" s="T81">ətɨpɨlʼ</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_303" n="HIAT:w" s="T82">kuntaqɨnɨ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_306" n="HIAT:w" s="T83">nʼɔːtɨ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_309" n="HIAT:w" s="T84">qessɨs</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_313" n="HIAT:w" s="T85">mɔːtqɨn</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_316" n="HIAT:w" s="T86">tüsa</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_319" n="HIAT:w" s="T87">sar-əsɨtɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_323" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_325" n="HIAT:w" s="T88">Mantɛčʼekkus</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_328" n="HIAT:w" s="T89">təpɨtkinɨ</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_332" n="HIAT:w" s="T90">sɛttɨmela</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_335" n="HIAT:w" s="T91">kɛšqɨlʼesa</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_339" n="HIAT:w" s="T92">ɔːnnɨmt</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_342" n="HIAT:w" s="T93">aša</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_345" n="HIAT:w" s="T94">sʼeːpɨrelsɨt</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_347" n="HIAT:ip">(</nts>
                  <nts id="Seg_348" n="HIAT:ip">/</nts>
                  <ts e="T96" id="Seg_350" n="HIAT:w" s="T95">seːpɨrsɨ</ts>
                  <nts id="Seg_351" n="HIAT:ip">)</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_354" n="HIAT:w" s="T96">aje</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_357" n="HIAT:w" s="T97">čʼeːlɨ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_360" n="HIAT:w" s="T98">čʼontot</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">urrɛːčʼesa</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_367" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_369" n="HIAT:w" s="T100">Kekkalpɨsa</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">(</nts>
                  <nts id="Seg_372" n="HIAT:ip">/</nts>
                  <ts e="T102" id="Seg_374" n="HIAT:w" s="T101">nakkalpɨs</ts>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_378" n="HIAT:w" s="T102">kuntɨ</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_381" n="HIAT:w" s="T103">sar</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_385" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_387" n="HIAT:w" s="T104">Kutar</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_390" n="HIAT:w" s="T105">ɛːqo</ts>
                  <nts id="Seg_391" n="HIAT:ip">?</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_394" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_396" n="HIAT:w" s="T106">Grešnɨk</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_399" n="HIAT:w" s="T107">ɛːppa</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_403" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_405" n="HIAT:w" s="T108">Poːtɨ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_408" n="HIAT:w" s="T109">qɛnna</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_410" n="HIAT:ip">(</nts>
                  <nts id="Seg_411" n="HIAT:ip">/</nts>
                  <ts e="T111" id="Seg_413" n="HIAT:w" s="T110">qännɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip">)</nts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_418" n="HIAT:w" s="T111">mitɨ</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_421" n="HIAT:w" s="T112">ɔːnʼkɨ</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_425" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_427" n="HIAT:w" s="T113">Mənɨlʼ</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <nts id="Seg_430" n="HIAT:ip">/</nts>
                  <ts e="T115" id="Seg_432" n="HIAT:w" s="T114">wänɨlʼ</ts>
                  <nts id="Seg_433" n="HIAT:ip">)</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_436" n="HIAT:w" s="T115">imap</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_439" n="HIAT:w" s="T116">ɨːnʼɨt</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_442" n="HIAT:w" s="T117">sar</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_446" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_448" n="HIAT:w" s="T118">Kutar</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_451" n="HIAT:w" s="T119">kɛtɨqo</ts>
                  <nts id="Seg_452" n="HIAT:ip">–</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_455" n="HIAT:w" s="T120">nälʼqup</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_458" n="HIAT:w" s="T121">ɛːsa</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_461" n="HIAT:w" s="T122">onɨlʼ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_464" n="HIAT:w" s="T123">mitɨ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_467" n="HIAT:w" s="T124">sarɨlʼ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_470" n="HIAT:w" s="T125">ima</ts>
                  <nts id="Seg_471" n="HIAT:ip">:</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_474" n="HIAT:w" s="T126">pirqɨ</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_477" n="HIAT:w" s="T127">tašik</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">sərɨ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_484" n="HIAT:w" s="T129">ɛːsa</ts>
                  <nts id="Seg_485" n="HIAT:ip">,</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_488" n="HIAT:w" s="T130">tɛnɨl</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_491" n="HIAT:w" s="T131">tɔːksa</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_495" n="HIAT:w" s="T132">muntokt</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_498" n="HIAT:w" s="T133">iːsɨt</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_502" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_504" n="HIAT:w" s="T134">Nɨːn</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_507" n="HIAT:w" s="T135">aj</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_510" n="HIAT:w" s="T136">nʼɔːtɨ</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_513" n="HIAT:w" s="T137">somalʼčʼɨmčʼat</ts>
                  <nts id="Seg_514" n="HIAT:ip">,</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_517" n="HIAT:w" s="T138">qɔːt-ol</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_520" n="HIAT:w" s="T139">nʼɔːtɨ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_523" n="HIAT:w" s="T140">jarɨk</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_526" n="HIAT:w" s="T141">ɛsa</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_530" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_532" n="HIAT:w" s="T142">Mɔːttɨ</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_535" n="HIAT:w" s="T143">nɔːnɨ</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_538" n="HIAT:w" s="T144">taːtɨmpɨstɨ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_541" n="HIAT:w" s="T145">kekɨs</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_544" n="HIAT:w" s="T146">zerkalalʼam</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_547" n="HIAT:w" s="T147">okɨr</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_551" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_553" n="HIAT:w" s="T148">Zerkalalʼa</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_556" n="HIAT:w" s="T149">nılʼčʼilʼ</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_559" n="HIAT:w" s="T150">ɛːsa</ts>
                  <nts id="Seg_560" n="HIAT:ip">:</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_563" n="HIAT:w" s="T151">konʼɨmpɨtqo</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_566" n="HIAT:w" s="T152">tɛnɨmɨmpa</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_570" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">Təpsa</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_575" n="HIAT:w" s="T154">okɨr</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_578" n="HIAT:w" s="T155">təp</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_581" n="HIAT:w" s="T156">qan</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_584" n="HIAT:w" s="T157">ɛːkka</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_587" n="HIAT:w" s="T158">soma</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_590" n="HIAT:w" s="T159">puːtɨk</ts>
                  <nts id="Seg_591" n="HIAT:ip">,</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_594" n="HIAT:w" s="T160">ɔːntalpɨkka</ts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_598" n="HIAT:w" s="T161">təpsa</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_601" n="HIAT:w" s="T162">šoqqak</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_604" n="HIAT:w" s="T163">karampɨkka</ts>
                  <nts id="Seg_605" n="HIAT:ip">,</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_608" n="HIAT:w" s="T164">somalʼčʼimpɨl</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_611" n="HIAT:w" s="T165">konʼɨmpɨkka</ts>
                  <nts id="Seg_612" n="HIAT:ip">:</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_615" n="HIAT:w" s="T166">Čʼeːlom</ts>
                  <nts id="Seg_616" n="HIAT:ip">,</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_619" n="HIAT:w" s="T167">zerkalalʼam</ts>
                  <nts id="Seg_620" n="HIAT:ip">!</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_623" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">Kɛtat</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">napa</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_632" n="HIAT:w" s="T170">čʼeːlɨk</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_635" n="HIAT:w" s="T171">ınna</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_638" n="HIAT:w" s="T172">pintɨ</ts>
                  <nts id="Seg_639" n="HIAT:ip">:</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_642" n="HIAT:w" s="T173">mat</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_645" n="HIAT:w" s="T174">qaj</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_648" n="HIAT:w" s="T175">tətɨn</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_651" n="HIAT:w" s="T176">iːqɨt</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_654" n="HIAT:w" s="T177">muntokt</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_657" n="HIAT:w" s="T178">soma</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_660" n="HIAT:w" s="T179">kuras</ts>
                  <nts id="Seg_661" n="HIAT:ip">,</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_664" n="HIAT:w" s="T180">sɛrɨ</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_667" n="HIAT:w" s="T181">ɛːnʼak</ts>
                  <nts id="Seg_668" n="HIAT:ip">?</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_671" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_673" n="HIAT:w" s="T182">Zerkalalʼa</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_676" n="HIAT:w" s="T183">kətɨkkɨtɨ</ts>
                  <nts id="Seg_677" n="HIAT:ip">:</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_680" n="HIAT:w" s="T184">Tat</ts>
                  <nts id="Seg_681" n="HIAT:ip">,</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_684" n="HIAT:w" s="T185">sarɨt</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_687" n="HIAT:w" s="T186">ima</ts>
                  <nts id="Seg_688" n="HIAT:ip">,</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_691" n="HIAT:w" s="T187">aša</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_694" n="HIAT:w" s="T188">antak</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_698" n="HIAT:w" s="T189">tat</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_701" n="HIAT:w" s="T190">kes</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_704" n="HIAT:w" s="T191">okɨr</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_707" n="HIAT:w" s="T192">muntokt</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_710" n="HIAT:w" s="T193">soma</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_713" n="HIAT:w" s="T194">kurasa</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_716" n="HIAT:w" s="T195">aj</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_719" n="HIAT:w" s="T196">sɛrɨ</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_722" n="HIAT:w" s="T197">ɛːnʼant</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_726" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_728" n="HIAT:w" s="T198">Sarɨt</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_731" n="HIAT:w" s="T199">ima</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_734" n="HIAT:w" s="T200">laqqɨmɔːnna</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201.tx.1" id="Seg_738" n="HIAT:w" s="T201">qəqtɨ</ts>
                  <nts id="Seg_739" n="HIAT:ip">_</nts>
                  <ts e="T202" id="Seg_741" n="HIAT:w" s="T201.tx.1">pɔːrɨm</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_744" n="HIAT:w" s="T202">ipqalnɨtɨ</ts>
                  <nts id="Seg_745" n="HIAT:ip">,</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_748" n="HIAT:w" s="T203">saintɨsa</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_751" n="HIAT:w" s="T204">rɨpkalʼčʼenʼɨt</ts>
                  <nts id="Seg_752" n="HIAT:ip">,</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_755" n="HIAT:w" s="T205">munɨntɨsa</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_758" n="HIAT:w" s="T206">kašilʼčʼenɨt</ts>
                  <nts id="Seg_759" n="HIAT:ip">,</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_762" n="HIAT:w" s="T207">utɨp</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_765" n="HIAT:w" s="T208">qenʼɨnt</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_768" n="HIAT:w" s="T209">kompɨlɨmpa</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_772" n="HIAT:w" s="T210">zerkalalʼant</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_775" n="HIAT:w" s="T211">mannɨmpɨla</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_779" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_781" n="HIAT:w" s="T212">Sarɨt</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_784" n="HIAT:w" s="T213">nälʼa</ts>
                  <nts id="Seg_785" n="HIAT:ip">–</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_788" n="HIAT:w" s="T214">čʼilʼalʼ</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_791" n="HIAT:w" s="T215">nätäk</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_794" n="HIAT:w" s="T216">lʼamɨk</ts>
                  <nts id="Seg_795" n="HIAT:ip">,</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_798" n="HIAT:w" s="T217">čʼontɨk</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_801" n="HIAT:w" s="T218">orɨmnanta</ts>
                  <nts id="Seg_802" n="HIAT:ip">,</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_805" n="HIAT:w" s="T219">nat</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220.tx.1" id="Seg_808" n="HIAT:w" s="T220">sitɨ</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_811" n="HIAT:w" s="T220.tx.1">kočʼat</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_814" n="HIAT:w" s="T221">orɨmnanta</ts>
                  <nts id="Seg_815" n="HIAT:ip">,</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_818" n="HIAT:w" s="T222">məčʼišinta</ts>
                  <nts id="Seg_819" n="HIAT:ip">,</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_822" n="HIAT:w" s="T223">orɨmtana</ts>
                  <nts id="Seg_823" n="HIAT:ip">,</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_826" n="HIAT:w" s="T224">sərɨ</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_829" n="HIAT:w" s="T225">məčʼik</ts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_833" n="HIAT:w" s="T226">sʼaː</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_836" n="HIAT:w" s="T227">sain</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_839" n="HIAT:w" s="T228">untɨk</ts>
                  <nts id="Seg_840" n="HIAT:ip">,</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_843" n="HIAT:w" s="T229">selmokɔːlɨk</ts>
                  <nts id="Seg_844" n="HIAT:ip">,</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_847" n="HIAT:w" s="T230">tɔːtɨk</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_850" n="HIAT:w" s="T231">puːtɨk</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_854" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_856" n="HIAT:w" s="T232">Titalʼ</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_859" n="HIAT:w" s="T233">qumtɨ</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_862" n="HIAT:w" s="T234">peːqɨlʼisa</ts>
                  <nts id="Seg_863" n="HIAT:ip">,</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_866" n="HIAT:w" s="T235">korolʼt</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_869" n="HIAT:w" s="T236">ija</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_872" n="HIAT:w" s="T237">Elisej</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_876" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_878" n="HIAT:w" s="T238">Tüsa</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_881" n="HIAT:w" s="T239">qup</ts>
                  <nts id="Seg_882" n="HIAT:ip">–</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_885" n="HIAT:w" s="T240">sar</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_888" n="HIAT:w" s="T241">əːtɨp</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_891" n="HIAT:w" s="T242">misɨt</ts>
                  <nts id="Seg_892" n="HIAT:ip">,</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_895" n="HIAT:w" s="T243">mɔːtqɨn</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_898" n="HIAT:w" s="T244">ipsa</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_901" n="HIAT:w" s="T245">mɨta</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_904" n="HIAT:w" s="T246">ɛːnʼa</ts>
                  <nts id="Seg_905" n="HIAT:ip">:</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_908" n="HIAT:w" s="T247">seːlʼčʼi</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_911" n="HIAT:w" s="T248">təmtɨtili</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_914" n="HIAT:w" s="T249">qɛːtɨt</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_917" n="HIAT:w" s="T250">aje</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_920" n="HIAT:w" s="T251">toːn</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_923" n="HIAT:w" s="T252">aj</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_926" n="HIAT:w" s="T253">tɛːsarɨli</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_929" n="HIAT:w" s="T254">mɔːtɨjät</ts>
                  <nts id="Seg_930" n="HIAT:ip">.</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_933" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_935" n="HIAT:w" s="T255">Nätatɨtkinɨ</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_938" n="HIAT:w" s="T256">taqqɨliqo</ts>
                  <nts id="Seg_939" n="HIAT:ip">,</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_942" n="HIAT:w" s="T257">sarɨt</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_945" n="HIAT:w" s="T258">ima</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_948" n="HIAT:w" s="T259">toqtatkutɨl</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_951" n="HIAT:w" s="T260">zerkalalʼantɨsa</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_954" n="HIAT:w" s="T261">ontɨ</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_958" n="HIAT:w" s="T262">əːtɨk</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_961" n="HIAT:w" s="T263">tɛpɨn</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_964" n="HIAT:w" s="T264">čʼattɨkkustɨ</ts>
                  <nts id="Seg_965" n="HIAT:ip">:</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_968" n="HIAT:w" s="T265">Mat</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_971" n="HIAT:w" s="T266">qaj</ts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_975" n="HIAT:w" s="T267">kətat</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_978" n="HIAT:w" s="T268">mäkkä</ts>
                  <nts id="Seg_979" n="HIAT:ip">,</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_982" n="HIAT:w" s="T269">muntok</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_985" n="HIAT:w" s="T270">kurassa</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_988" n="HIAT:w" s="T271">aj</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_991" n="HIAT:w" s="T272">sərɨ</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_994" n="HIAT:w" s="T273">ɛːnʼak</ts>
                  <nts id="Seg_995" n="HIAT:ip">?</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_998" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1000" n="HIAT:w" s="T274">Qaj</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1003" n="HIAT:w" s="T275">kat</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1006" n="HIAT:w" s="T276">zerkalalʼa</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1009" n="HIAT:w" s="T277">tompɨt</ts>
                  <nts id="Seg_1010" n="HIAT:ip">?</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1013" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1015" n="HIAT:w" s="T278">Kurassa</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1018" n="HIAT:w" s="T279">ɛːnʼant</ts>
                  <nts id="Seg_1019" n="HIAT:ip">,</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1022" n="HIAT:w" s="T280">aša</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1025" n="HIAT:w" s="T281">antak</ts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1029" n="HIAT:w" s="T282">sarɨt</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1032" n="HIAT:w" s="T283">nälʼa</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1035" n="HIAT:w" s="T284">qotəl</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1038" n="HIAT:w" s="T285">muntokt</ts>
                  <nts id="Seg_1039" n="HIAT:ip">,</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1042" n="HIAT:w" s="T286">kurassa</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1045" n="HIAT:w" s="T287">aj</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1048" n="HIAT:w" s="T288">sərɨ</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1051" n="HIAT:w" s="T289">ɛːnʼa</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1055" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1057" n="HIAT:w" s="T290">Sarɨt</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1060" n="HIAT:w" s="T291">ima</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1063" n="HIAT:w" s="T292">alpa</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1066" n="HIAT:w" s="T293">pakta</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1069" n="HIAT:w" s="T294">aj</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1072" n="HIAT:w" s="T295">küratɔːlna</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1075" n="HIAT:w" s="T296">utɨlʼantɨs</ts>
                  <nts id="Seg_1076" n="HIAT:ip">,</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1079" n="HIAT:w" s="T297">zerkalalʼap</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1082" n="HIAT:w" s="T298">pəqšatɨnʼɨt</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1086" n="HIAT:w" s="T299">lʼakčʼinlʼantɨs</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1089" n="HIAT:w" s="T300">čʼelʼčʼɔːlna</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1092" n="HIAT:w" s="T301">nɨː</ts>
                  <nts id="Seg_1093" n="HIAT:ip">!</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1096" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1098" n="HIAT:w" s="T302">Аh</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1102" n="HIAT:w" s="T303">tat</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1105" n="HIAT:w" s="T304">nılʼčʼilʼ</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1108" n="HIAT:w" s="T305">steklonʼontɨ</ts>
                  <nts id="Seg_1109" n="HIAT:ip">!</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1112" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1114" n="HIAT:w" s="T306">Tat</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1117" n="HIAT:w" s="T307">na</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1120" n="HIAT:w" s="T308">moːlmɨtantɨ</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1123" n="HIAT:w" s="T309">mat</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1126" n="HIAT:w" s="T310">čʼɔːt</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1130" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1132" n="HIAT:w" s="T311">Təp</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1135" n="HIAT:w" s="T312">kutar</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1138" n="HIAT:w" s="T313">mannɨmmänta</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1141" n="HIAT:w" s="T314">matsa</ts>
                  <nts id="Seg_1142" n="HIAT:ip">?</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1145" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1147" n="HIAT:w" s="T315">Mat</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1150" n="HIAT:w" s="T316">təpɨt</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1153" n="HIAT:w" s="T317">moːmɨt</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1156" n="HIAT:w" s="T318">lʼamqaltantap</ts>
                  <nts id="Seg_1157" n="HIAT:ip">.</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1160" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1162" n="HIAT:w" s="T319">Qontɨrnɔːlɨt</ts>
                  <nts id="Seg_1163" n="HIAT:ip">,</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1166" n="HIAT:w" s="T320">orɨmna</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1169" n="HIAT:w" s="T321">qaj</ts>
                  <nts id="Seg_1170" n="HIAT:ip">!</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1173" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1175" n="HIAT:w" s="T322">Kutar</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1178" n="HIAT:w" s="T323">aša</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1181" n="HIAT:w" s="T324">sərɨ</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1184" n="HIAT:w" s="T325">ɛːnʼa</ts>
                  <nts id="Seg_1185" n="HIAT:ip">:</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1188" n="HIAT:w" s="T326">əmɨt</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1191" n="HIAT:w" s="T327">pɛrqɨlʼ</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1194" n="HIAT:w" s="T328">tıːri</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1197" n="HIAT:w" s="T329">ɔːmtsa</ts>
                  <nts id="Seg_1198" n="HIAT:ip">,</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1201" n="HIAT:w" s="T330">kekɨs</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1204" n="HIAT:w" s="T331">sɨront</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1207" n="HIAT:w" s="T332">mantalpɨsa</ts>
                  <nts id="Seg_1208" n="HIAT:ip">.</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1211" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1213" n="HIAT:w" s="T333">Kɨsa</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1216" n="HIAT:w" s="T334">kɛtat</ts>
                  <nts id="Seg_1217" n="HIAT:ip">:</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1220" n="HIAT:w" s="T335">qantɨk</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1223" n="HIAT:w" s="T336">man</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1226" n="HIAT:w" s="T337">nɔːn</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1229" n="HIAT:w" s="T338">nɨːnɨ</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1232" n="HIAT:w" s="T339">puːt</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1235" n="HIAT:w" s="T340">təp</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1238" n="HIAT:w" s="T341">kurassa</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1241" n="HIAT:w" s="T342">ɛːnta</ts>
                  <nts id="Seg_1242" n="HIAT:ip">?</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1245" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1247" n="HIAT:w" s="T343">İnna</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1250" n="HIAT:w" s="T344">kətat</ts>
                  <nts id="Seg_1251" n="HIAT:ip">:</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1254" n="HIAT:w" s="T345">mat</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1257" n="HIAT:w" s="T346">kes</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1260" n="HIAT:w" s="T347">nʼančʼak</ts>
                  <nts id="Seg_1261" n="HIAT:ip">.</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1264" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1266" n="HIAT:w" s="T348">Ɔːmtɨlʼ</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1269" n="HIAT:w" s="T349">qoː</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1272" n="HIAT:w" s="T350">mɨt</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1275" n="HIAT:w" s="T351">kolʼaltatɨ</ts>
                  <nts id="Seg_1276" n="HIAT:ip">,</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1279" n="HIAT:w" s="T352">qos</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1282" n="HIAT:w" s="T353">tətɨ</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1285" n="HIAT:w" s="T354">muntɨk</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1289" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1291" n="HIAT:w" s="T355">Matsa</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1294" n="HIAT:w" s="T356">čʼäːŋka</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1297" n="HIAT:w" s="T357">nʼančʼitɨlʼ</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1300" n="HIAT:w" s="T358">mɨ</ts>
                  <nts id="Seg_1301" n="HIAT:ip">.</nts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1304" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1306" n="HIAT:w" s="T359">Nılʼčʼik</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1309" n="HIAT:w" s="T360">qɔːtɨ</ts>
                  <nts id="Seg_1310" n="HIAT:ip">?</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1313" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1315" n="HIAT:w" s="T361">Zerkalalʼa</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1318" n="HIAT:w" s="T362">kətɨnʼɨtɨ</ts>
                  <nts id="Seg_1319" n="HIAT:ip">:</nts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1322" n="HIAT:w" s="T363">sarɨt</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1325" n="HIAT:w" s="T364">nälʼa</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1328" n="HIAT:w" s="T365">qotol</ts>
                  <nts id="Seg_1329" n="HIAT:ip">,</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1332" n="HIAT:w" s="T366">pija</ts>
                  <nts id="Seg_1333" n="HIAT:ip">,</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1336" n="HIAT:w" s="T367">kurassa</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1339" n="HIAT:w" s="T368">aj</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1342" n="HIAT:w" s="T369">sərɨ</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1345" n="HIAT:w" s="T370">ɛːnʼa</ts>
                  <nts id="Seg_1346" n="HIAT:ip">.</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1349" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1351" n="HIAT:w" s="T371">Kutar</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1354" n="HIAT:w" s="T372">meːtal</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1358" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1360" n="HIAT:w" s="T373">Puːtɨmt</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1363" n="HIAT:w" s="T374">amla</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1366" n="HIAT:w" s="T375">zerkalalʼap</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1369" n="HIAT:w" s="T376">konna</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1372" n="HIAT:w" s="T377">čʼattɨt</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1376" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1378" n="HIAT:w" s="T378">Čʼernavkamtɨ</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1381" n="HIAT:w" s="T379">qərɨnʼɨtɨ</ts>
                  <nts id="Seg_1382" n="HIAT:ip">,</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1385" n="HIAT:w" s="T380">nılʼčʼik</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1388" n="HIAT:w" s="T381">təptɨ</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1391" n="HIAT:w" s="T382">quraltɨnʼɨt</ts>
                  <nts id="Seg_1392" n="HIAT:ip">,</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1394" n="HIAT:ip">(</nts>
                  <ts e="T384" id="Seg_1396" n="HIAT:w" s="T383">ontɨ</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1399" n="HIAT:w" s="T384">sennɨlʼ</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1402" n="HIAT:w" s="T385">nätak</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1405" n="HIAT:w" s="T386">tɨna</ts>
                  <nts id="Seg_1406" n="HIAT:ip">)</nts>
                  <nts id="Seg_1407" n="HIAT:ip">:</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1410" n="HIAT:w" s="T387">Sarɨt</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1413" n="HIAT:w" s="T388">nälʼap</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1416" n="HIAT:w" s="T389">mačʼɨt</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1419" n="HIAT:w" s="T390">puːtont</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1422" n="HIAT:w" s="T391">qɛntɨrtaltɨl</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1425" n="HIAT:w" s="T392">kurɨnʼɨmpɨj</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1428" n="HIAT:w" s="T393">ilɨla</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1431" n="HIAT:w" s="T394">aj</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1434" n="HIAT:w" s="T395">qəːčʼinʼɨmpɨj</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1437" n="HIAT:w" s="T396">näčʼčʼäːtɨnʼɨt</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1440" n="HIAT:w" s="T397">čʼöːn</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1443" n="HIAT:w" s="T398">ılnʼɨt</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1446" n="HIAT:w" s="T399">čʼumpɨ</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1449" n="HIAT:w" s="T400">neːet</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1452" n="HIAT:w" s="T401">amnʼɨntitqo</ts>
                  <nts id="Seg_1453" n="HIAT:ip">.</nts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1456" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_1458" n="HIAT:w" s="T402">Loːsɨ</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1461" n="HIAT:w" s="T403">montɨ</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1464" n="HIAT:w" s="T404">antɨtanta</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1467" n="HIAT:w" s="T405">nʼenʼnʼalʼ</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1470" n="HIAT:w" s="T406">imas</ts>
                  <nts id="Seg_1471" n="HIAT:ip">?</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1474" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1476" n="HIAT:w" s="T407">Sarɨt</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1479" n="HIAT:w" s="T408">nälʼas</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1482" n="HIAT:w" s="T409">Čʼernovka</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1485" n="HIAT:w" s="T410">qənta</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1488" n="HIAT:w" s="T411">mačʼontɨ</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1491" n="HIAT:w" s="T412">na</ts>
                  <nts id="Seg_1492" n="HIAT:ip">.</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1495" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1497" n="HIAT:w" s="T413">Našak</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1500" n="HIAT:w" s="T414">kuntak</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1503" n="HIAT:w" s="T415">tɨna</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1506" n="HIAT:w" s="T416">qəntoj</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1509" n="HIAT:w" s="T417">sarɨt</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1512" n="HIAT:w" s="T418">nälʼa</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1515" n="HIAT:w" s="T419">nɨːna</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1518" n="HIAT:w" s="T420">qənta</ts>
                  <nts id="Seg_1519" n="HIAT:ip">.</nts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1522" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1524" n="HIAT:w" s="T421">Qukɨntɨšak</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1527" n="HIAT:w" s="T422">nɨrkɨmɔːnna</ts>
                  <nts id="Seg_1528" n="HIAT:ip">,</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1531" n="HIAT:w" s="T423">omtaltana</ts>
                  <nts id="Seg_1532" n="HIAT:ip">:</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1535" n="HIAT:w" s="T424">Ilɨptamɨ</ts>
                  <nts id="Seg_1536" n="HIAT:ip">!</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1539" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1541" n="HIAT:w" s="T425">Qajqɨt</ts>
                  <nts id="Seg_1542" n="HIAT:ip">,</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1545" n="HIAT:w" s="T426">kɛtat</ts>
                  <nts id="Seg_1546" n="HIAT:ip">,</nts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1549" n="HIAT:w" s="T427">urop</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1552" n="HIAT:w" s="T428">ɛːnʼa</ts>
                  <nts id="Seg_1553" n="HIAT:ip">?</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1556" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1558" n="HIAT:w" s="T429">Nälɨqup</ts>
                  <nts id="Seg_1559" n="HIAT:ip">,</nts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1562" n="HIAT:w" s="T430">ɨkɨ</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1565" n="HIAT:w" s="T431">sip</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1568" n="HIAT:w" s="T432">talʼčʼolʼašik</ts>
                  <nts id="Seg_1569" n="HIAT:ip">!</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1572" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1574" n="HIAT:w" s="T433">Kutar</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1577" n="HIAT:w" s="T434">qumɨp</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1580" n="HIAT:w" s="T435">qontap</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1583" n="HIAT:w" s="T436">tašınt</ts>
                  <nts id="Seg_1584" n="HIAT:ip">,</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1587" n="HIAT:w" s="T437">qajqo</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1590" n="HIAT:w" s="T438">kɨkant</ts>
                  <nts id="Seg_1591" n="HIAT:ip">–</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1594" n="HIAT:w" s="T439">natqo</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1597" n="HIAT:w" s="T440">meːtak</ts>
                  <nts id="Seg_1598" n="HIAT:ip">.</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1601" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1603" n="HIAT:w" s="T441">Toːnnam</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1606" n="HIAT:w" s="T442">puːtsa</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1609" n="HIAT:w" s="T443">təptɨ</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1612" n="HIAT:w" s="T444">kɨkal</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1615" n="HIAT:w" s="T445">aša</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1618" n="HIAT:w" s="T446">kurasɨt</ts>
                  <nts id="Seg_1619" n="HIAT:ip">,</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1622" n="HIAT:w" s="T447">aša</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1625" n="HIAT:w" s="T448">qəssɨt</ts>
                  <nts id="Seg_1626" n="HIAT:ip">,</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1629" n="HIAT:w" s="T449">uːtɨsɨtɨ</ts>
                  <nts id="Seg_1630" n="HIAT:ip">,</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1633" n="HIAT:w" s="T450">nılʼčʼik</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1636" n="HIAT:w" s="T451">kətɨl</ts>
                  <nts id="Seg_1637" n="HIAT:ip">:</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1640" n="HIAT:w" s="T452">Qɛnʼaš</ts>
                  <nts id="Seg_1641" n="HIAT:ip">,</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1644" n="HIAT:w" s="T453">nop</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1647" n="HIAT:w" s="T454">sıntɨ</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1650" n="HIAT:w" s="T455">na</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1653" n="HIAT:w" s="T456">məret</ts>
                  <nts id="Seg_1654" n="HIAT:ip">.</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1657" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1659" n="HIAT:w" s="T457">Nɨːnɨ</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1662" n="HIAT:w" s="T458">ontɨ</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1665" n="HIAT:w" s="T459">moqɨn</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1668" n="HIAT:w" s="T460">tüssa</ts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1672" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1674" n="HIAT:w" s="T461">Qajɨ</ts>
                  <nts id="Seg_1675" n="HIAT:ip">?</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1678" n="HIAT:u" s="T462">
                  <nts id="Seg_1679" n="HIAT:ip">–</nts>
                  <ts e="T463" id="Seg_1681" n="HIAT:w" s="T462">Sarɨt</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1684" n="HIAT:w" s="T463">ima</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1687" n="HIAT:w" s="T464">kətɨnʼɨtɨ</ts>
                  <nts id="Seg_1688" n="HIAT:ip">.</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1691" n="HIAT:u" s="T465">
                  <nts id="Seg_1692" n="HIAT:ip">–</nts>
                  <ts e="T466" id="Seg_1694" n="HIAT:w" s="T465">Kurassɨmɨlʼ</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1697" n="HIAT:w" s="T466">nälʼqup</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1700" n="HIAT:w" s="T467">kun</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1703" n="HIAT:w" s="T468">ɛːnʼ</ts>
                  <nts id="Seg_1704" n="HIAT:ip">?</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1707" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1709" n="HIAT:w" s="T469">Pɛlɨkɔːlɨk</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1712" n="HIAT:w" s="T470">mačʼot</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1715" n="HIAT:w" s="T471">nɨŋa</ts>
                  <nts id="Seg_1716" n="HIAT:ip">,</nts>
                  <nts id="Seg_1717" n="HIAT:ip">–</nts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1720" n="HIAT:w" s="T472">təptɨ</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1723" n="HIAT:w" s="T473">kətɨnʼɨtɨ</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1726" n="HIAT:w" s="T474">təpɨn</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1730" n="HIAT:u" s="T475">
                  <nts id="Seg_1731" n="HIAT:ip">–</nts>
                  <ts e="T476" id="Seg_1733" n="HIAT:w" s="T475">Sɔːrɨmpa</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1736" n="HIAT:w" s="T476">qɔːmɨk</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1739" n="HIAT:w" s="T477">sɨnʼte</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1742" n="HIAT:w" s="T478">neːttɨ</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1746" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1748" n="HIAT:w" s="T479">Suːrɨt</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1751" n="HIAT:w" s="T480">qatont</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1754" n="HIAT:w" s="T481">olʼčʼačʼemmä</ts>
                  <nts id="Seg_1755" n="HIAT:ip">,</nts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1758" n="HIAT:w" s="T482">qɔːnak</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1761" n="HIAT:w" s="T483">təp</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1764" n="HIAT:w" s="T484">na</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1767" n="HIAT:w" s="T485">kəkkɨtanɨt</ts>
                  <nts id="Seg_1768" n="HIAT:ip">,</nts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1771" n="HIAT:w" s="T486">qurmot</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1774" n="HIAT:w" s="T487">aj</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1777" n="HIAT:w" s="T488">na</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1780" n="HIAT:w" s="T489">seːpɨ</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1783" n="HIAT:w" s="T490">ɛːnnɨnt</ts>
                  <nts id="Seg_1784" n="HIAT:ip">.</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1787" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1789" n="HIAT:w" s="T491">Əːtɨ</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1792" n="HIAT:w" s="T492">qənna</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1795" n="HIAT:w" s="T493">nɨːnɨ</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1798" n="HIAT:w" s="T494">nılʼčʼilʼ</ts>
                  <nts id="Seg_1799" n="HIAT:ip">:</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1802" n="HIAT:w" s="T495">sarɨt</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1805" n="HIAT:w" s="T496">nälʼa</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1808" n="HIAT:w" s="T497">urɨntana</ts>
                  <nts id="Seg_1809" n="HIAT:ip">!</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1812" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1814" n="HIAT:w" s="T498">Paširna</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1817" n="HIAT:w" s="T499">sar</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1820" n="HIAT:w" s="T500">nälʼantɨqo</ts>
                  <nts id="Seg_1821" n="HIAT:ip">.</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_1824" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1826" n="HIAT:w" s="T501">Korolʼ</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1829" n="HIAT:w" s="T502">ija</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1832" n="HIAT:w" s="T503">Elisej</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1835" n="HIAT:w" s="T504">nomtɨ</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1838" n="HIAT:w" s="T505">omtɨtɨla</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1841" n="HIAT:w" s="T506">puːlä</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1844" n="HIAT:w" s="T507">toqtalta</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1847" n="HIAT:w" s="T508">aj</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1850" n="HIAT:w" s="T509">peːlʼa</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1853" n="HIAT:w" s="T510">qənna</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1856" n="HIAT:w" s="T511">kurassɨmat</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1859" n="HIAT:w" s="T512">sıːčʼint</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1862" n="HIAT:w" s="T513">olɨp</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1865" n="HIAT:w" s="T514">tıːtap</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1868" n="HIAT:w" s="T515">iːpsa</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1871" n="HIAT:w" s="T516">imtɨ</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1874" n="HIAT:w" s="T517">neːmtɨ</ts>
                  <nts id="Seg_1875" n="HIAT:ip">.</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1878" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_1880" n="HIAT:w" s="T518">A</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1883" n="HIAT:w" s="T519">itɨ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1886" n="HIAT:w" s="T520">neːtɨ</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1889" n="HIAT:w" s="T521">qarɨt</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1892" n="HIAT:w" s="T522">təttɨ</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1895" n="HIAT:w" s="T523">mačʼit</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1898" n="HIAT:w" s="T524">puːtot</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1901" n="HIAT:w" s="T525">urkɨmpɨla</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1904" n="HIAT:w" s="T526">kurɨtäntɨ</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527.tx.1" id="Seg_1907" n="HIAT:w" s="T527">sitɨ</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1910" n="HIAT:w" s="T527.tx.1">kotät</ts>
                  <nts id="Seg_1911" n="HIAT:ip">,</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1914" n="HIAT:w" s="T528">mɔːtɨn</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1917" n="HIAT:w" s="T529">iːntɨ</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1920" n="HIAT:w" s="T530">tulʼčʼeıːmpa</ts>
                  <nts id="Seg_1921" n="HIAT:ip">.</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_1924" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1926" n="HIAT:w" s="T531">Čʼəssä</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1929" n="HIAT:w" s="T532">läqa</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1932" n="HIAT:w" s="T533">təpɨn</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1935" n="HIAT:w" s="T534">muːttɨl</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1938" n="HIAT:w" s="T535">läːjätɔːlta</ts>
                  <nts id="Seg_1939" n="HIAT:ip">,</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1942" n="HIAT:w" s="T536">lʼamkaltenta</ts>
                  <nts id="Seg_1943" n="HIAT:ip">,</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1946" n="HIAT:w" s="T537">lüčʼimpɨla</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1949" n="HIAT:w" s="T538">ɔːntalpɨla</ts>
                  <nts id="Seg_1950" n="HIAT:ip">.</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1953" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_1955" n="HIAT:w" s="T539">Vorotamɨt</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1958" n="HIAT:w" s="T540">təp</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1961" n="HIAT:w" s="T541">na</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1964" n="HIAT:w" s="T542">seːra</ts>
                  <nts id="Seg_1965" n="HIAT:ip">.</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_1968" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1970" n="HIAT:w" s="T543">Etɨmantot</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1973" n="HIAT:w" s="T544">lʼamɨk</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1976" n="HIAT:w" s="T545">ɛːnʼa</ts>
                  <nts id="Seg_1977" n="HIAT:ip">.</nts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_1980" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_1982" n="HIAT:w" s="T546">Läqa</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1985" n="HIAT:w" s="T547">ɔːntalpɨlʼa</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1988" n="HIAT:w" s="T548">nʼoːnʼɨt</ts>
                  <nts id="Seg_1989" n="HIAT:ip">.</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_1992" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_1994" n="HIAT:w" s="T549">A</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1997" n="HIAT:w" s="T550">sarɨt</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2000" n="HIAT:w" s="T551">nälʼa</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2003" n="HIAT:w" s="T552">kurɨtelʼa</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2006" n="HIAT:w" s="T553">krɨlʼcot</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2009" n="HIAT:w" s="T554">iːntɨ</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2012" n="HIAT:w" s="T555">sɨːqɨlenta</ts>
                  <nts id="Seg_2013" n="HIAT:ip">,</nts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2016" n="HIAT:w" s="T556">pürɨj</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2019" n="HIAT:w" s="T557">utoːqɨntɨ</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2022" n="HIAT:w" s="T558">iːntɨt</ts>
                  <nts id="Seg_2023" n="HIAT:ip">.</nts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2026" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2028" n="HIAT:w" s="T559">Mɔːtat</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2031" n="HIAT:w" s="T560">čʼontɨka</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2034" n="HIAT:w" s="T561">na</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2037" n="HIAT:w" s="T562">nʼüːntɨt</ts>
                  <nts id="Seg_2038" n="HIAT:ip">,</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2041" n="HIAT:w" s="T563">mɔːttɨ</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2044" n="HIAT:w" s="T564">čʼontɨka</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2047" n="HIAT:w" s="T565">na</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2050" n="HIAT:w" s="T566">seːra</ts>
                  <nts id="Seg_2051" n="HIAT:ip">.</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2054" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2056" n="HIAT:w" s="T567">Qonempa</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2058" n="HIAT:ip">(</nts>
                  <nts id="Seg_2059" n="HIAT:ip">/</nts>
                  <ts e="T569" id="Seg_2061" n="HIAT:w" s="T568">qonnɨmpa</ts>
                  <nts id="Seg_2062" n="HIAT:ip">,</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2065" n="HIAT:w" s="T569">qonnänta</ts>
                  <nts id="Seg_2066" n="HIAT:ip">)</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2069" n="HIAT:w" s="T570">koversa</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2072" n="HIAT:w" s="T571">lavkat</ts>
                  <nts id="Seg_2073" n="HIAT:ip">,</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2076" n="HIAT:w" s="T572">ikonat</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2079" n="HIAT:w" s="T573">ılqɨt</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2082" n="HIAT:w" s="T574">dupɨlʼ</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2085" n="HIAT:w" s="T575">lem</ts>
                  <nts id="Seg_2086" n="HIAT:ip">,</nts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2089" n="HIAT:w" s="T576">nəkɨrpɨlʼ</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2092" n="HIAT:w" s="T577">ippɨpsasa</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2095" n="HIAT:w" s="T578">šoːqɨr</ts>
                  <nts id="Seg_2096" n="HIAT:ip">.</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2099" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2101" n="HIAT:w" s="T579">Nälʼqup</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2104" n="HIAT:w" s="T580">mannɨmpatɨ</ts>
                  <nts id="Seg_2105" n="HIAT:ip">:</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2108" n="HIAT:w" s="T581">tɨmtɨ</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2111" n="HIAT:w" s="T582">qɔːtɨ</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2114" n="HIAT:w" s="T583">soma</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2117" n="HIAT:w" s="T584">qumɨt</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2120" n="HIAT:w" s="T585">ilʼɔːt</ts>
                  <nts id="Seg_2121" n="HIAT:ip">.</nts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2124" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2126" n="HIAT:w" s="T586">Tɛpɨn</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2129" n="HIAT:w" s="T587">aša</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2132" n="HIAT:w" s="T588">toːtelʼančʼɔːt</ts>
                  <nts id="Seg_2133" n="HIAT:ip">!</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2136" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2138" n="HIAT:w" s="T589">Nɔːt</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590.tx.1" id="Seg_2141" n="HIAT:w" s="T590">sitɨ</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2144" n="HIAT:w" s="T590.tx.1">kotat</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2147" n="HIAT:w" s="T591">ni</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2150" n="HIAT:w" s="T592">kutɨ</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2153" n="HIAT:w" s="T593">aš</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2156" n="HIAT:w" s="T594">ata</ts>
                  <nts id="Seg_2157" n="HIAT:ip">.</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2160" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2162" n="HIAT:w" s="T595">Mɔːtɨp</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2165" n="HIAT:w" s="T596">tumɨt</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2168" n="HIAT:w" s="T597">kolʼaltɨntɨt</ts>
                  <nts id="Seg_2169" n="HIAT:ip">,</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2172" n="HIAT:w" s="T598">muntok</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2175" n="HIAT:w" s="T599">somak</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2178" n="HIAT:w" s="T600">täšaltɨstɨ</ts>
                  <nts id="Seg_2179" n="HIAT:ip">,</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2182" n="HIAT:w" s="T601">ikonat</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2185" n="HIAT:w" s="T602">sečʼčʼip</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2188" n="HIAT:w" s="T603">ačʼaltɨntɨt</ts>
                  <nts id="Seg_2189" n="HIAT:ip">,</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2192" n="HIAT:w" s="T604">šoːqɨrɨp</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2195" n="HIAT:w" s="T605">pötpɨlla</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2198" n="HIAT:w" s="T606">čʼetɨntɨtɨ</ts>
                  <nts id="Seg_2199" n="HIAT:ip">,</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2202" n="HIAT:w" s="T607">polatit</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2205" n="HIAT:w" s="T608">iːntɨ</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2208" n="HIAT:w" s="T609">sɨːqɨlelʼa</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2211" n="HIAT:w" s="T610">lʼamɨk</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2214" n="HIAT:w" s="T611">ılla</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2217" n="HIAT:w" s="T612">olʼčʼeıːnta</ts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2221" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2223" n="HIAT:w" s="T613">Čʼeːlɨt</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2226" n="HIAT:w" s="T614">čʼonnont</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2229" n="HIAT:w" s="T615">tətčʼe</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2232" n="HIAT:w" s="T616">ɛːsa</ts>
                  <nts id="Seg_2233" n="HIAT:ip">.</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_2236" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2238" n="HIAT:w" s="T617">Sümɨ</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2241" n="HIAT:w" s="T618">untalɨnta</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2244" n="HIAT:w" s="T619">poːqɨt</ts>
                  <nts id="Seg_2245" n="HIAT:ip">:</nts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2248" n="HIAT:w" s="T620">seːlʼčʼi</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2251" n="HIAT:w" s="T621">mɔːtɨrɨn</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2254" n="HIAT:w" s="T622">na</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2257" n="HIAT:w" s="T623">seːrɔːt</ts>
                  <nts id="Seg_2258" n="HIAT:ip">,</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2261" n="HIAT:w" s="T624">seːlʼčʼit</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2264" n="HIAT:w" s="T625">muntɨk</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626.tx.1" id="Seg_2267" n="HIAT:w" s="T626">untɨlʼ</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2270" n="HIAT:w" s="T626.tx.1">uːkɨk</ts>
                  <nts id="Seg_2271" n="HIAT:ip">.</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2274" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_2276" n="HIAT:w" s="T627">Mərqɨ</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2279" n="HIAT:w" s="T628">neːtɨt</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2282" n="HIAT:w" s="T629">nılʼčʼik</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2285" n="HIAT:w" s="T630">ɛsa</ts>
                  <nts id="Seg_2286" n="HIAT:ip">:</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2289" n="HIAT:w" s="T631">Na</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2292" n="HIAT:w" s="T632">lʼa</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2295" n="HIAT:w" s="T633">qaj</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2298" n="HIAT:w" s="T634">ɛsʼ</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2301" n="HIAT:w" s="T635">kutar</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2304" n="HIAT:w" s="T636">some</ts>
                  <nts id="Seg_2305" n="HIAT:ip">,</nts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2308" n="HIAT:w" s="T637">kutar</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2311" n="HIAT:w" s="T638">muntɨk</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2314" n="HIAT:w" s="T639">kurass</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2317" n="HIAT:w" s="T640">ɛːnʼa</ts>
                  <nts id="Seg_2318" n="HIAT:ip">.</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2321" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2323" n="HIAT:w" s="T641">Kutɨ</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2326" n="HIAT:w" s="T642">qos</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2329" n="HIAT:w" s="T643">mɔːnmɨt</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2332" n="HIAT:w" s="T644">təšanʼpatɨ</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2335" n="HIAT:w" s="T645">mitɨ</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2338" n="HIAT:w" s="T646">meːšımɨt</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2341" n="HIAT:w" s="T647">ətakkɔːtɨj</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2343" n="HIAT:ip">(</nts>
                  <nts id="Seg_2344" n="HIAT:ip">/</nts>
                  <ts e="T649" id="Seg_2346" n="HIAT:w" s="T648">ätɨmpɔːtɨn</ts>
                  <nts id="Seg_2347" n="HIAT:ip">)</nts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2351" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2353" n="HIAT:w" s="T649">Kutontʼ</ts>
                  <nts id="Seg_2354" n="HIAT:ip">?</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2357" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2359" n="HIAT:w" s="T650">Tantaš</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2362" n="HIAT:w" s="T651">atɨltašik</ts>
                  <nts id="Seg_2363" n="HIAT:ip">,</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2366" n="HIAT:w" s="T652">meːšım</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2369" n="HIAT:w" s="T653">toːnɨ</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2372" n="HIAT:w" s="T654">qotak</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2375" n="HIAT:w" s="T655">čʼɛːtäš</ts>
                  <nts id="Seg_2376" n="HIAT:ip">.</nts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2379" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2381" n="HIAT:w" s="T656">Qatamol</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2384" n="HIAT:w" s="T657">tat</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2387" n="HIAT:w" s="T658">mɛrqɨ</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2390" n="HIAT:w" s="T659">qumont</ts>
                  <nts id="Seg_2391" n="HIAT:ip">,</nts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2394" n="HIAT:w" s="T660">ilʼčʼanɨtqo</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2397" n="HIAT:w" s="T661">meːltɨ</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2400" n="HIAT:w" s="T662">ɛːnnant</ts>
                  <nts id="Seg_2401" n="HIAT:ip">.</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2404" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2406" n="HIAT:w" s="T663">Qatamol</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2409" n="HIAT:w" s="T664">tat</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2412" n="HIAT:w" s="T665">ilʼmat</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2415" n="HIAT:w" s="T666">nʼoːntɨ</ts>
                  <nts id="Seg_2416" n="HIAT:ip">,</nts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2419" n="HIAT:w" s="T667">meːqin</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2422" n="HIAT:w" s="T668">čʼopanɨtqo</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2425" n="HIAT:w" s="T669">ɛːnnat</ts>
                  <nts id="Seg_2426" n="HIAT:ip">.</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2429" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2431" n="HIAT:w" s="T670">Qatamol</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2434" n="HIAT:w" s="T671">tat</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2437" n="HIAT:w" s="T672">imaqota</ts>
                  <nts id="Seg_2438" n="HIAT:ip">–</nts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2441" n="HIAT:w" s="T673">qɛrʼantɨmɨt</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2444" n="HIAT:w" s="T674">ämɨnɨtqo</ts>
                  <nts id="Seg_2445" n="HIAT:ip">.</nts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2448" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2450" n="HIAT:w" s="T675">Qatamol</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2453" n="HIAT:w" s="T676">tat</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2456" n="HIAT:w" s="T677">nälʼ</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2459" n="HIAT:w" s="T678">qumontɨ</ts>
                  <nts id="Seg_2460" n="HIAT:ip">–</nts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2463" n="HIAT:w" s="T679">meːqin</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2466" n="HIAT:w" s="T680">nʼenʼnʼanɨtqo</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2469" n="HIAT:w" s="T681">ɛːnnat</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2473" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2475" n="HIAT:w" s="T682">Sarɨt</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2478" n="HIAT:w" s="T683">nälʼa</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2481" n="HIAT:w" s="T684">ılla</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2484" n="HIAT:w" s="T685">pančʼa</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2486" n="HIAT:ip">(</nts>
                  <nts id="Seg_2487" n="HIAT:ip">/</nts>
                  <ts e="T687" id="Seg_2489" n="HIAT:w" s="T686">ılʼlʼɨmpanɨ</ts>
                  <nts id="Seg_2490" n="HIAT:ip">)</nts>
                  <nts id="Seg_2491" n="HIAT:ip">,</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2494" n="HIAT:w" s="T687">torowatɨnt</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2497" n="HIAT:w" s="T688">təpɨtɨsa</ts>
                  <nts id="Seg_2498" n="HIAT:ip">,</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2501" n="HIAT:w" s="T689">čʼüːntɨt</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2504" n="HIAT:w" s="T690">tättɨ</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2507" n="HIAT:w" s="T691">mušqaıːtɨla</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2509" n="HIAT:ip">(</nts>
                  <nts id="Seg_2510" n="HIAT:ip">/</nts>
                  <ts e="T693" id="Seg_2512" n="HIAT:w" s="T692">nʼuːtɨ</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2515" n="HIAT:w" s="T693">sočʼ</ts>
                  <nts id="Seg_2516" n="HIAT:ip">)</nts>
                  <nts id="Seg_2517" n="HIAT:ip">.</nts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2520" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2522" n="HIAT:w" s="T694">Nʼertarɨs</ts>
                  <nts id="Seg_2523" n="HIAT:ip">,</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2526" n="HIAT:w" s="T695">nılʼ</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2529" n="HIAT:w" s="T696">ɛsɨt</ts>
                  <nts id="Seg_2530" n="HIAT:ip">:</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2533" n="HIAT:w" s="T697">Mɔːtqɨntit</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2536" n="HIAT:w" s="T698">mol</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2539" n="HIAT:w" s="T699">seːrsak</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2542" n="HIAT:w" s="T700">mat</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2545" n="HIAT:w" s="T701">tiː</ts>
                  <nts id="Seg_2546" n="HIAT:ip">,</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2549" n="HIAT:w" s="T702">qos</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2552" n="HIAT:w" s="T703">sıt</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2555" n="HIAT:w" s="T704">qɛrɨptälɨt</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2558" n="HIAT:w" s="T705">čʼäːnʼa</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2562" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2564" n="HIAT:w" s="T706">Nɨːnɨ</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2567" n="HIAT:w" s="T707">ɛːtomɨntɨ</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2570" n="HIAT:w" s="T708">qəntɔːtɨt</ts>
                  <nts id="Seg_2571" n="HIAT:ip">:</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2574" n="HIAT:w" s="T709">tam</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2577" n="HIAT:w" s="T710">aš</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2580" n="HIAT:w" s="T711">sarɨt</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2583" n="HIAT:w" s="T712">nälʼa</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2586" n="HIAT:w" s="T713">ɛːsa</ts>
                  <nts id="Seg_2587" n="HIAT:ip">.</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2590" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2592" n="HIAT:w" s="T714">Sänʼälʼantɨ</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2595" n="HIAT:w" s="T715">omtɨltɨsɔːt</ts>
                  <nts id="Seg_2596" n="HIAT:ip">,</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2599" n="HIAT:w" s="T716">nʼanʼilʼasa</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2601" n="HIAT:ip">(</nts>
                  <nts id="Seg_2602" n="HIAT:ip">/</nts>
                  <ts e="T718" id="Seg_2604" n="HIAT:w" s="T717">nʼänʼilʼäsa</ts>
                  <nts id="Seg_2605" n="HIAT:ip">)</nts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2608" n="HIAT:w" s="T718">milʼčʼečʼesɔːt</ts>
                  <nts id="Seg_2609" n="HIAT:ip">,</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2612" n="HIAT:w" s="T719">rʼumkaj</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2615" n="HIAT:w" s="T720">tıːrla</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2618" n="HIAT:w" s="T721">qamtečʼesɔːt</ts>
                  <nts id="Seg_2619" n="HIAT:ip">,</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2622" n="HIAT:w" s="T722">ilit</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2625" n="HIAT:w" s="T723">iːqɨt</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2628" n="HIAT:w" s="T724">taːtäčʼesɔːtɨ</ts>
                  <nts id="Seg_2629" n="HIAT:ip">.</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2632" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2634" n="HIAT:w" s="T725">Tapɨlʼ</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2637" n="HIAT:w" s="T726">utɨp</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2640" n="HIAT:w" s="T727">aša</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2643" n="HIAT:w" s="T728">iːstɨ</ts>
                  <nts id="Seg_2644" n="HIAT:ip">,</nts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2647" n="HIAT:w" s="T729">nʼanʼilʼap</ts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2650" n="HIAT:w" s="T730">kes</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2653" n="HIAT:w" s="T731">sepɨtela</ts>
                  <nts id="Seg_2654" n="HIAT:ip">,</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2657" n="HIAT:w" s="T732">kɨpɛ</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2660" n="HIAT:w" s="T733">lıːpɨmt</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2663" n="HIAT:w" s="T734">ɔːkɨnt</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2666" n="HIAT:w" s="T735">iːstɨ</ts>
                  <nts id="Seg_2667" n="HIAT:ip">,</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2670" n="HIAT:w" s="T736">wɛtɨn</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2673" n="HIAT:w" s="T737">nɔːnɨ</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2676" n="HIAT:w" s="T738">nʼɨšqɨntoːqo</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2679" n="HIAT:w" s="T739">koptɨtqo</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2682" n="HIAT:w" s="T740">no</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2685" n="HIAT:w" s="T741">mɔːtɨrana</ts>
                  <nts id="Seg_2686" n="HIAT:ip">.</nts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_2689" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_2691" n="HIAT:w" s="T742">Qɛntɨsɔːtɨt</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2694" n="HIAT:w" s="T743">tɛpɨt</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2697" n="HIAT:w" s="T744">nätap</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2700" n="HIAT:w" s="T745">ınna</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2703" n="HIAT:w" s="T746">čʼeːlɨnʼpɨtɨlʼ</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2706" n="HIAT:w" s="T747">sünnöt</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2709" n="HIAT:w" s="T748">aj</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2712" n="HIAT:w" s="T749">ontɨqɨt</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2715" n="HIAT:w" s="T750">na</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2717" n="HIAT:ip">(</nts>
                  <nts id="Seg_2718" n="HIAT:ip">/</nts>
                  <ts e="T752" id="Seg_2720" n="HIAT:w" s="T751">nɨmtɨ</ts>
                  <nts id="Seg_2721" n="HIAT:ip">)</nts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2724" n="HIAT:w" s="T752">qəːčʼɨntɔːtɨt</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2726" n="HIAT:ip">(</nts>
                  <nts id="Seg_2727" n="HIAT:ip">/</nts>
                  <ts e="T754" id="Seg_2729" n="HIAT:w" s="T753">qəːčʼɨsɔːtɨt</ts>
                  <nts id="Seg_2730" n="HIAT:ip">)</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2733" n="HIAT:w" s="T754">təpɨp</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2736" n="HIAT:w" s="T755">ɔːŋkɨtitɨl</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2739" n="HIAT:w" s="T756">pɛlɨkɔːlɨk</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2741" n="HIAT:ip">(</nts>
                  <nts id="Seg_2742" n="HIAT:ip">(</nts>
                  <ats e="T758" id="Seg_2743" n="HIAT:non-pho" s="T757">GAP</ats>
                  <nts id="Seg_2744" n="HIAT:ip">)</nts>
                  <nts id="Seg_2745" n="HIAT:ip">)</nts>
                  <nts id="Seg_2746" n="HIAT:ip">.</nts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_2749" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_2751" n="HIAT:w" s="T758">Nʼarqɨ</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2754" n="HIAT:w" s="T759">čʼeːlon</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2756" n="HIAT:ip">(</nts>
                  <nts id="Seg_2757" n="HIAT:ip">/</nts>
                  <ts e="T761" id="Seg_2759" n="HIAT:w" s="T760">čʼeːlʼi</ts>
                  <nts id="Seg_2760" n="HIAT:ip">)</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2763" n="HIAT:w" s="T761">moːroqɨntɨ</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2766" n="HIAT:w" s="T762">ilʼmat</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2769" n="HIAT:w" s="T763">koraltɨpsa</ts>
                  <nts id="Seg_2770" n="HIAT:ip">:</nts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2773" n="HIAT:w" s="T764">Čʼeːlɨnʼpɨta</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2776" n="HIAT:w" s="T765">čʼeːlɨmɨt</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2779" n="HIAT:w" s="T766">meː</ts>
                  <nts id="Seg_2780" n="HIAT:ip">!</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T783" id="Seg_2783" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_2785" n="HIAT:w" s="T767">Tat</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2788" n="HIAT:w" s="T768">karanʼnʼantɨ</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2790" n="HIAT:ip">(</nts>
                  <nts id="Seg_2791" n="HIAT:ip">/</nts>
                  <ts e="T770" id="Seg_2793" n="HIAT:w" s="T769">kutornantɨ</ts>
                  <nts id="Seg_2794" n="HIAT:ip">)</nts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2797" n="HIAT:w" s="T770">poːntɨ</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2800" n="HIAT:w" s="T771">kuntɨ</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2803" n="HIAT:w" s="T772">nuːt</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2806" n="HIAT:w" s="T773">nekmɨ</ts>
                  <nts id="Seg_2807" n="HIAT:ip">,</nts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2810" n="HIAT:w" s="T774">totaltukkal</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2813" n="HIAT:w" s="T775">kəp</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2816" n="HIAT:w" s="T776">petpɨlʼ</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2819" n="HIAT:w" s="T777">utɨsa</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2822" n="HIAT:w" s="T778">mutokt</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2825" n="HIAT:w" s="T779">meːsım</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2828" n="HIAT:w" s="T780">onant</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2831" n="HIAT:w" s="T781">ılqɨt</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2834" n="HIAT:w" s="T782">qontɨrnantɨ</ts>
                  <nts id="Seg_2835" n="HIAT:ip">.</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_2838" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_2840" n="HIAT:w" s="T783">Montɨ</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2843" n="HIAT:w" s="T784">əːtop</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2846" n="HIAT:w" s="T785">toː</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2849" n="HIAT:w" s="T786">čʼatantal</ts>
                  <nts id="Seg_2850" n="HIAT:ip">?</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_2853" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_2855" n="HIAT:w" s="T787">Qontɨrsal</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2858" n="HIAT:w" s="T788">qaj</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2861" n="HIAT:w" s="T789">kun</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2864" n="HIAT:w" s="T790">ɛːm</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2867" n="HIAT:w" s="T791">tətot</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2870" n="HIAT:w" s="T792">sarɨt</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2873" n="HIAT:w" s="T793">nälʼap</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2876" n="HIAT:w" s="T794">ilʼmatɨlʼ</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2879" n="HIAT:w" s="T795">tat</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2881" n="HIAT:ip">(</nts>
                  <nts id="Seg_2882" n="HIAT:ip">(</nts>
                  <ats e="T797" id="Seg_2883" n="HIAT:non-pho" s="T796">GAP</ats>
                  <nts id="Seg_2884" n="HIAT:ip">)</nts>
                  <nts id="Seg_2885" n="HIAT:ip">)</nts>
                  <nts id="Seg_2886" n="HIAT:ip">.</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_2889" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_2891" n="HIAT:w" s="T797">Irät</ts>
                  <nts id="Seg_2892" n="HIAT:ip">,</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2895" n="HIAT:w" s="T798">irät</ts>
                  <nts id="Seg_2896" n="HIAT:ip">,</nts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2899" n="HIAT:w" s="T799">onak</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2902" n="HIAT:w" s="T800">neːmɨ</ts>
                  <nts id="Seg_2903" n="HIAT:ip">,</nts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2906" n="HIAT:w" s="T801">zoločʼenɨlʼ</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2909" n="HIAT:w" s="T802">ɔːttɨlʼa</ts>
                  <nts id="Seg_2910" n="HIAT:ip">!</nts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_2913" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_2915" n="HIAT:w" s="T803">Məšikkantɨ</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2918" n="HIAT:w" s="T804">korel</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2921" n="HIAT:w" s="T805">pit</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2924" n="HIAT:w" s="T806">tat</ts>
                  <nts id="Seg_2925" n="HIAT:ip">,</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2928" n="HIAT:w" s="T807">pürɨlʼ</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2931" n="HIAT:w" s="T808">mɛntɨlʼ</ts>
                  <nts id="Seg_2932" n="HIAT:ip">,</nts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2935" n="HIAT:w" s="T809">čʼeːlɨlʼ</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2938" n="HIAT:w" s="T810">sailʼ</ts>
                  <nts id="Seg_2939" n="HIAT:ip">.</nts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_2942" n="HIAT:u" s="T811">
                  <ts e="T812" id="Seg_2944" n="HIAT:w" s="T811">Aj</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2947" n="HIAT:w" s="T812">mat</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2950" n="HIAT:w" s="T813">serlɨ</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2953" n="HIAT:w" s="T814">kɨkɨla</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2956" n="HIAT:w" s="T815">qıšqat</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2959" n="HIAT:w" s="T816">tantɨ</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2962" n="HIAT:w" s="T817">mannɨmpɔːtɨt</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2964" n="HIAT:ip">(</nts>
                  <nts id="Seg_2965" n="HIAT:ip">(</nts>
                  <ats e="T819" id="Seg_2966" n="HIAT:non-pho" s="T818">GAP</ats>
                  <nts id="Seg_2967" n="HIAT:ip">)</nts>
                  <nts id="Seg_2968" n="HIAT:ip">)</nts>
                  <nts id="Seg_2969" n="HIAT:ip">.</nts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_2972" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_2974" n="HIAT:w" s="T819">Näčʼät</ts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2977" n="HIAT:w" s="T820">čʼontɨ</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2980" n="HIAT:w" s="T821">künɨlʼ</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2983" n="HIAT:w" s="T822">kɨqat</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2986" n="HIAT:w" s="T823">toap</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2989" n="HIAT:w" s="T824">ɛːnʼa</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2992" n="HIAT:w" s="T825">pirqɨ</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2995" n="HIAT:w" s="T826">toanʼ</ts>
                  <nts id="Seg_2996" n="HIAT:ip">,</nts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2999" n="HIAT:w" s="T827">nɨmtɨ</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3002" n="HIAT:w" s="T828">korɨ</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3005" n="HIAT:w" s="T829">mü</ts>
                  <nts id="Seg_3006" n="HIAT:ip">.</nts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3009" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3011" n="HIAT:w" s="T830">Pitɨ</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3014" n="HIAT:w" s="T831">matɨ</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3017" n="HIAT:w" s="T832">tətɨ</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3019" n="HIAT:ip">(</nts>
                  <nts id="Seg_3020" n="HIAT:ip">(</nts>
                  <ats e="T834" id="Seg_3021" n="HIAT:non-pho" s="T833">GAP</ats>
                  <nts id="Seg_3022" n="HIAT:ip">)</nts>
                  <nts id="Seg_3023" n="HIAT:ip">)</nts>
                  <nts id="Seg_3024" n="HIAT:ip">.</nts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T834" id="Seg_3026" n="sc" s="T0">
               <ts e="T1" id="Seg_3028" n="e" s="T0">Sar </ts>
               <ts e="T2" id="Seg_3030" n="e" s="T1">imantɨsa </ts>
               <ts e="T3" id="Seg_3032" n="e" s="T2">nɛtɨrsɨtɨ </ts>
               <ts e="T4" id="Seg_3034" n="e" s="T3">(/nɛkärsɨ), </ts>
               <ts e="T5" id="Seg_3036" n="e" s="T4">kuntak </ts>
               <ts e="T6" id="Seg_3038" n="e" s="T5">wəttont </ts>
               <ts e="T7" id="Seg_3040" n="e" s="T6">toqtaltɨŋa. </ts>
               <ts e="T8" id="Seg_3042" n="e" s="T7">Oknot </ts>
               <ts e="T9" id="Seg_3044" n="e" s="T8">qantɨ </ts>
               <ts e="T10" id="Seg_3046" n="e" s="T9">imatɨ </ts>
               <ts e="T11" id="Seg_3048" n="e" s="T10">tɛpɨp </ts>
               <ts e="T12" id="Seg_3050" n="e" s="T11">ɛtɨqo </ts>
               <ts e="T13" id="Seg_3052" n="e" s="T12">ontɨ </ts>
               <ts e="T14" id="Seg_3054" n="e" s="T13">omta. </ts>
               <ts e="T15" id="Seg_3056" n="e" s="T14">Qarɨn </ts>
               <ts e="T16" id="Seg_3058" n="e" s="T15">nɔːnɨ </ts>
               <ts e="T17" id="Seg_3060" n="e" s="T16">üːtɨt </ts>
               <ts e="T18" id="Seg_3062" n="e" s="T17">tɛtɨ </ts>
               <ts e="T19" id="Seg_3064" n="e" s="T18">ɛtɨkkɨtɨ, </ts>
               <ts e="T20" id="Seg_3066" n="e" s="T19">ɛtɨkkɨtɨ, </ts>
               <ts e="T21" id="Seg_3068" n="e" s="T20">meːltɨ </ts>
               <ts e="T22" id="Seg_3070" n="e" s="T21">limont </ts>
               <ts e="T23" id="Seg_3072" n="e" s="T22">mannɨmpa </ts>
               <ts e="T24" id="Seg_3074" n="e" s="T23">tɛp. </ts>
               <ts e="T25" id="Seg_3076" n="e" s="T24">Sailʼ </ts>
               <ts e="T26" id="Seg_3078" n="e" s="T25">tɔːktɨ </ts>
               <ts e="T27" id="Seg_3080" n="e" s="T26">mantalpɨlʼa </ts>
               <ts e="T28" id="Seg_3082" n="e" s="T27">čʼüšalɨsɔːt, </ts>
               <ts e="T29" id="Seg_3084" n="e" s="T28">kutɨlʼ </ts>
               <ts e="T30" id="Seg_3086" n="e" s="T29">ɛːmɨlʼa </ts>
               <ts e="T31" id="Seg_3088" n="e" s="T30">qarɨlʼ </ts>
               <ts e="T32" id="Seg_3090" n="e" s="T31">kɛnnɨnt </ts>
               <ts e="T33" id="Seg_3092" n="e" s="T32">uːkoːqɨnɨ </ts>
               <ts e="T34" id="Seg_3094" n="e" s="T33">pit </ts>
               <ts e="T35" id="Seg_3096" n="e" s="T34">tɛt. </ts>
               <ts e="T36" id="Seg_3098" n="e" s="T35">Ɔːmɨ </ts>
               <ts e="T37" id="Seg_3100" n="e" s="T36">neːtɨ </ts>
               <ts e="T38" id="Seg_3102" n="e" s="T37">aša </ts>
               <ts e="T39" id="Seg_3104" n="e" s="T38">ata! </ts>
               <ts e="T40" id="Seg_3106" n="e" s="T39">Kekɨsä </ts>
               <ts e="T41" id="Seg_3108" n="e" s="T40">təp </ts>
               <ts e="T42" id="Seg_3110" n="e" s="T41">mannɨmpatɨ: </ts>
               <ts e="T43" id="Seg_3112" n="e" s="T42">mɛrkɨlʼ </ts>
               <ts e="T44" id="Seg_3114" n="e" s="T43">palʼčʼa </ts>
               <ts e="T45" id="Seg_3116" n="e" s="T44">küčʼinimpa, </ts>
               <ts e="T46" id="Seg_3118" n="e" s="T45">sɨrɨ </ts>
               <ts e="T47" id="Seg_3120" n="e" s="T46">čʼomna </ts>
               <ts e="T48" id="Seg_3122" n="e" s="T47">limɨt </ts>
               <ts e="T49" id="Seg_3124" n="e" s="T48">iːntɨ, </ts>
               <ts e="T50" id="Seg_3126" n="e" s="T49">təttɨ </ts>
               <ts e="T51" id="Seg_3128" n="e" s="T50">muntɨk </ts>
               <ts e="T52" id="Seg_3130" n="e" s="T51">sɛrɨ </ts>
               <ts e="T53" id="Seg_3132" n="e" s="T52">ɛsa. </ts>
               <ts e="T54" id="Seg_3134" n="e" s="T53">Qənna </ts>
               <ts e="T55" id="Seg_3136" n="e" s="T54">(/qänna) </ts>
               <ts e="T56" id="Seg_3138" n="e" s="T55">okkɨr </ts>
               <ts e="T57" id="Seg_3140" n="e" s="T56">čʼäː </ts>
               <ts e="T58" id="Seg_3142" n="e" s="T57">köt </ts>
               <ts e="T59" id="Seg_3144" n="e" s="T58">irät. </ts>
               <ts e="T60" id="Seg_3146" n="e" s="T59">Limoːqɨn </ts>
               <ts e="T61" id="Seg_3148" n="e" s="T60">saimtɨ </ts>
               <ts e="T62" id="Seg_3150" n="e" s="T61">alpa </ts>
               <ts e="T63" id="Seg_3152" n="e" s="T62">ašša </ts>
               <ts e="T64" id="Seg_3154" n="e" s="T63">iːtɨt </ts>
               <ts e="T65" id="Seg_3156" n="e" s="T64">(/iːtɨŋɨt). </ts>
               <ts e="T66" id="Seg_3158" n="e" s="T65">Nʼɔːt </ts>
               <ts e="T67" id="Seg_3160" n="e" s="T66">sočʼelʼnikaqɨt, </ts>
               <ts e="T68" id="Seg_3162" n="e" s="T67">pit, </ts>
               <ts e="T69" id="Seg_3164" n="e" s="T68">təpɨn </ts>
               <ts e="T70" id="Seg_3166" n="e" s="T69">minɨt </ts>
               <ts e="T71" id="Seg_3168" n="e" s="T70">nälʼam </ts>
               <ts e="T72" id="Seg_3170" n="e" s="T71">(/nälʼamtɨ) </ts>
               <ts e="T73" id="Seg_3172" n="e" s="T72">nom </ts>
               <ts e="T74" id="Seg_3174" n="e" s="T73">(/nomtɨ). </ts>
               <ts e="T75" id="Seg_3176" n="e" s="T74">Rɛmkɨ </ts>
               <ts e="T76" id="Seg_3178" n="e" s="T75">qarɨt </ts>
               <ts e="T77" id="Seg_3180" n="e" s="T76">nɔːtnɨlʼ </ts>
               <ts e="T78" id="Seg_3182" n="e" s="T77">gostʼ, </ts>
               <ts e="T79" id="Seg_3184" n="e" s="T78">pisa </ts>
               <ts e="T80" id="Seg_3186" n="e" s="T79">čʼeːlɨsa </ts>
               <ts e="T81" id="Seg_3188" n="e" s="T80">kuntɨ </ts>
               <ts e="T82" id="Seg_3190" n="e" s="T81">ətɨpɨlʼ, </ts>
               <ts e="T83" id="Seg_3192" n="e" s="T82">kuntaqɨnɨ </ts>
               <ts e="T84" id="Seg_3194" n="e" s="T83">nʼɔːtɨ </ts>
               <ts e="T85" id="Seg_3196" n="e" s="T84">qessɨs, </ts>
               <ts e="T86" id="Seg_3198" n="e" s="T85">mɔːtqɨn </ts>
               <ts e="T87" id="Seg_3200" n="e" s="T86">tüsa </ts>
               <ts e="T88" id="Seg_3202" n="e" s="T87">sar-əsɨtɨ. </ts>
               <ts e="T89" id="Seg_3204" n="e" s="T88">Mantɛčʼekkus </ts>
               <ts e="T90" id="Seg_3206" n="e" s="T89">təpɨtkinɨ, </ts>
               <ts e="T91" id="Seg_3208" n="e" s="T90">sɛttɨmela </ts>
               <ts e="T92" id="Seg_3210" n="e" s="T91">kɛšqɨlʼesa, </ts>
               <ts e="T93" id="Seg_3212" n="e" s="T92">ɔːnnɨmt </ts>
               <ts e="T94" id="Seg_3214" n="e" s="T93">aša </ts>
               <ts e="T95" id="Seg_3216" n="e" s="T94">sʼeːpɨrelsɨt </ts>
               <ts e="T96" id="Seg_3218" n="e" s="T95">(/seːpɨrsɨ) </ts>
               <ts e="T97" id="Seg_3220" n="e" s="T96">aje </ts>
               <ts e="T98" id="Seg_3222" n="e" s="T97">čʼeːlɨ </ts>
               <ts e="T99" id="Seg_3224" n="e" s="T98">čʼontot </ts>
               <ts e="T100" id="Seg_3226" n="e" s="T99">urrɛːčʼesa. </ts>
               <ts e="T101" id="Seg_3228" n="e" s="T100">Kekkalpɨsa </ts>
               <ts e="T102" id="Seg_3230" n="e" s="T101">(/nakkalpɨs) </ts>
               <ts e="T103" id="Seg_3232" n="e" s="T102">kuntɨ </ts>
               <ts e="T104" id="Seg_3234" n="e" s="T103">sar. </ts>
               <ts e="T105" id="Seg_3236" n="e" s="T104">Kutar </ts>
               <ts e="T106" id="Seg_3238" n="e" s="T105">ɛːqo? </ts>
               <ts e="T107" id="Seg_3240" n="e" s="T106">Grešnɨk </ts>
               <ts e="T108" id="Seg_3242" n="e" s="T107">ɛːppa. </ts>
               <ts e="T109" id="Seg_3244" n="e" s="T108">Poːtɨ </ts>
               <ts e="T110" id="Seg_3246" n="e" s="T109">qɛnna </ts>
               <ts e="T111" id="Seg_3248" n="e" s="T110">(/qännɨ), </ts>
               <ts e="T112" id="Seg_3250" n="e" s="T111">mitɨ </ts>
               <ts e="T113" id="Seg_3252" n="e" s="T112">ɔːnʼkɨ. </ts>
               <ts e="T114" id="Seg_3254" n="e" s="T113">Mənɨlʼ </ts>
               <ts e="T115" id="Seg_3256" n="e" s="T114">(/wänɨlʼ) </ts>
               <ts e="T116" id="Seg_3258" n="e" s="T115">imap </ts>
               <ts e="T117" id="Seg_3260" n="e" s="T116">ɨːnʼɨt </ts>
               <ts e="T118" id="Seg_3262" n="e" s="T117">sar. </ts>
               <ts e="T119" id="Seg_3264" n="e" s="T118">Kutar </ts>
               <ts e="T120" id="Seg_3266" n="e" s="T119">kɛtɨqo– </ts>
               <ts e="T121" id="Seg_3268" n="e" s="T120">nälʼqup </ts>
               <ts e="T122" id="Seg_3270" n="e" s="T121">ɛːsa </ts>
               <ts e="T123" id="Seg_3272" n="e" s="T122">onɨlʼ </ts>
               <ts e="T124" id="Seg_3274" n="e" s="T123">mitɨ </ts>
               <ts e="T125" id="Seg_3276" n="e" s="T124">sarɨlʼ </ts>
               <ts e="T126" id="Seg_3278" n="e" s="T125">ima: </ts>
               <ts e="T127" id="Seg_3280" n="e" s="T126">pirqɨ </ts>
               <ts e="T128" id="Seg_3282" n="e" s="T127">tašik, </ts>
               <ts e="T129" id="Seg_3284" n="e" s="T128">sərɨ </ts>
               <ts e="T130" id="Seg_3286" n="e" s="T129">ɛːsa, </ts>
               <ts e="T131" id="Seg_3288" n="e" s="T130">tɛnɨl </ts>
               <ts e="T132" id="Seg_3290" n="e" s="T131">tɔːksa, </ts>
               <ts e="T133" id="Seg_3292" n="e" s="T132">muntokt </ts>
               <ts e="T134" id="Seg_3294" n="e" s="T133">iːsɨt. </ts>
               <ts e="T135" id="Seg_3296" n="e" s="T134">Nɨːn </ts>
               <ts e="T136" id="Seg_3298" n="e" s="T135">aj </ts>
               <ts e="T137" id="Seg_3300" n="e" s="T136">nʼɔːtɨ </ts>
               <ts e="T138" id="Seg_3302" n="e" s="T137">somalʼčʼɨmčʼat, </ts>
               <ts e="T139" id="Seg_3304" n="e" s="T138">qɔːt-ol </ts>
               <ts e="T140" id="Seg_3306" n="e" s="T139">nʼɔːtɨ </ts>
               <ts e="T141" id="Seg_3308" n="e" s="T140">jarɨk </ts>
               <ts e="T142" id="Seg_3310" n="e" s="T141">ɛsa. </ts>
               <ts e="T143" id="Seg_3312" n="e" s="T142">Mɔːttɨ </ts>
               <ts e="T144" id="Seg_3314" n="e" s="T143">nɔːnɨ </ts>
               <ts e="T145" id="Seg_3316" n="e" s="T144">taːtɨmpɨstɨ </ts>
               <ts e="T146" id="Seg_3318" n="e" s="T145">kekɨs </ts>
               <ts e="T147" id="Seg_3320" n="e" s="T146">zerkalalʼam </ts>
               <ts e="T148" id="Seg_3322" n="e" s="T147">okɨr. </ts>
               <ts e="T149" id="Seg_3324" n="e" s="T148">Zerkalalʼa </ts>
               <ts e="T150" id="Seg_3326" n="e" s="T149">nılʼčʼilʼ </ts>
               <ts e="T151" id="Seg_3328" n="e" s="T150">ɛːsa: </ts>
               <ts e="T152" id="Seg_3330" n="e" s="T151">konʼɨmpɨtqo </ts>
               <ts e="T153" id="Seg_3332" n="e" s="T152">tɛnɨmɨmpa. </ts>
               <ts e="T154" id="Seg_3334" n="e" s="T153">Təpsa </ts>
               <ts e="T155" id="Seg_3336" n="e" s="T154">okɨr </ts>
               <ts e="T156" id="Seg_3338" n="e" s="T155">təp </ts>
               <ts e="T157" id="Seg_3340" n="e" s="T156">qan </ts>
               <ts e="T158" id="Seg_3342" n="e" s="T157">ɛːkka </ts>
               <ts e="T159" id="Seg_3344" n="e" s="T158">soma </ts>
               <ts e="T160" id="Seg_3346" n="e" s="T159">puːtɨk, </ts>
               <ts e="T161" id="Seg_3348" n="e" s="T160">ɔːntalpɨkka, </ts>
               <ts e="T162" id="Seg_3350" n="e" s="T161">təpsa </ts>
               <ts e="T163" id="Seg_3352" n="e" s="T162">šoqqak </ts>
               <ts e="T164" id="Seg_3354" n="e" s="T163">karampɨkka, </ts>
               <ts e="T165" id="Seg_3356" n="e" s="T164">somalʼčʼimpɨl </ts>
               <ts e="T166" id="Seg_3358" n="e" s="T165">konʼɨmpɨkka: </ts>
               <ts e="T167" id="Seg_3360" n="e" s="T166">Čʼeːlom, </ts>
               <ts e="T168" id="Seg_3362" n="e" s="T167">zerkalalʼam! </ts>
               <ts e="T169" id="Seg_3364" n="e" s="T168">Kɛtat </ts>
               <ts e="T170" id="Seg_3366" n="e" s="T169">napa, </ts>
               <ts e="T171" id="Seg_3368" n="e" s="T170">čʼeːlɨk </ts>
               <ts e="T172" id="Seg_3370" n="e" s="T171">ınna </ts>
               <ts e="T173" id="Seg_3372" n="e" s="T172">pintɨ: </ts>
               <ts e="T174" id="Seg_3374" n="e" s="T173">mat </ts>
               <ts e="T175" id="Seg_3376" n="e" s="T174">qaj </ts>
               <ts e="T176" id="Seg_3378" n="e" s="T175">tətɨn </ts>
               <ts e="T177" id="Seg_3380" n="e" s="T176">iːqɨt </ts>
               <ts e="T178" id="Seg_3382" n="e" s="T177">muntokt </ts>
               <ts e="T179" id="Seg_3384" n="e" s="T178">soma </ts>
               <ts e="T180" id="Seg_3386" n="e" s="T179">kuras, </ts>
               <ts e="T181" id="Seg_3388" n="e" s="T180">sɛrɨ </ts>
               <ts e="T182" id="Seg_3390" n="e" s="T181">ɛːnʼak? </ts>
               <ts e="T183" id="Seg_3392" n="e" s="T182">Zerkalalʼa </ts>
               <ts e="T184" id="Seg_3394" n="e" s="T183">kətɨkkɨtɨ: </ts>
               <ts e="T185" id="Seg_3396" n="e" s="T184">Tat, </ts>
               <ts e="T186" id="Seg_3398" n="e" s="T185">sarɨt </ts>
               <ts e="T187" id="Seg_3400" n="e" s="T186">ima, </ts>
               <ts e="T188" id="Seg_3402" n="e" s="T187">aša </ts>
               <ts e="T189" id="Seg_3404" n="e" s="T188">antak, </ts>
               <ts e="T190" id="Seg_3406" n="e" s="T189">tat </ts>
               <ts e="T191" id="Seg_3408" n="e" s="T190">kes </ts>
               <ts e="T192" id="Seg_3410" n="e" s="T191">okɨr </ts>
               <ts e="T193" id="Seg_3412" n="e" s="T192">muntokt </ts>
               <ts e="T194" id="Seg_3414" n="e" s="T193">soma </ts>
               <ts e="T195" id="Seg_3416" n="e" s="T194">kurasa </ts>
               <ts e="T196" id="Seg_3418" n="e" s="T195">aj </ts>
               <ts e="T197" id="Seg_3420" n="e" s="T196">sɛrɨ </ts>
               <ts e="T198" id="Seg_3422" n="e" s="T197">ɛːnʼant. </ts>
               <ts e="T199" id="Seg_3424" n="e" s="T198">Sarɨt </ts>
               <ts e="T200" id="Seg_3426" n="e" s="T199">ima </ts>
               <ts e="T201" id="Seg_3428" n="e" s="T200">laqqɨmɔːnna, </ts>
               <ts e="T202" id="Seg_3430" n="e" s="T201">qəqtɨ_pɔːrɨm </ts>
               <ts e="T203" id="Seg_3432" n="e" s="T202">ipqalnɨtɨ, </ts>
               <ts e="T204" id="Seg_3434" n="e" s="T203">saintɨsa </ts>
               <ts e="T205" id="Seg_3436" n="e" s="T204">rɨpkalʼčʼenʼɨt, </ts>
               <ts e="T206" id="Seg_3438" n="e" s="T205">munɨntɨsa </ts>
               <ts e="T207" id="Seg_3440" n="e" s="T206">kašilʼčʼenɨt, </ts>
               <ts e="T208" id="Seg_3442" n="e" s="T207">utɨp </ts>
               <ts e="T209" id="Seg_3444" n="e" s="T208">qenʼɨnt </ts>
               <ts e="T210" id="Seg_3446" n="e" s="T209">kompɨlɨmpa, </ts>
               <ts e="T211" id="Seg_3448" n="e" s="T210">zerkalalʼant </ts>
               <ts e="T212" id="Seg_3450" n="e" s="T211">mannɨmpɨla. </ts>
               <ts e="T213" id="Seg_3452" n="e" s="T212">Sarɨt </ts>
               <ts e="T214" id="Seg_3454" n="e" s="T213">nälʼa– </ts>
               <ts e="T215" id="Seg_3456" n="e" s="T214">čʼilʼalʼ </ts>
               <ts e="T216" id="Seg_3458" n="e" s="T215">nätäk </ts>
               <ts e="T217" id="Seg_3460" n="e" s="T216">lʼamɨk, </ts>
               <ts e="T218" id="Seg_3462" n="e" s="T217">čʼontɨk </ts>
               <ts e="T219" id="Seg_3464" n="e" s="T218">orɨmnanta, </ts>
               <ts e="T220" id="Seg_3466" n="e" s="T219">nat </ts>
               <ts e="T221" id="Seg_3468" n="e" s="T220">sitɨ kočʼat </ts>
               <ts e="T222" id="Seg_3470" n="e" s="T221">orɨmnanta, </ts>
               <ts e="T223" id="Seg_3472" n="e" s="T222">məčʼišinta, </ts>
               <ts e="T224" id="Seg_3474" n="e" s="T223">orɨmtana, </ts>
               <ts e="T225" id="Seg_3476" n="e" s="T224">sərɨ </ts>
               <ts e="T226" id="Seg_3478" n="e" s="T225">məčʼik, </ts>
               <ts e="T227" id="Seg_3480" n="e" s="T226">sʼaː </ts>
               <ts e="T228" id="Seg_3482" n="e" s="T227">sain </ts>
               <ts e="T229" id="Seg_3484" n="e" s="T228">untɨk, </ts>
               <ts e="T230" id="Seg_3486" n="e" s="T229">selmokɔːlɨk, </ts>
               <ts e="T231" id="Seg_3488" n="e" s="T230">tɔːtɨk </ts>
               <ts e="T232" id="Seg_3490" n="e" s="T231">puːtɨk. </ts>
               <ts e="T233" id="Seg_3492" n="e" s="T232">Titalʼ </ts>
               <ts e="T234" id="Seg_3494" n="e" s="T233">qumtɨ </ts>
               <ts e="T235" id="Seg_3496" n="e" s="T234">peːqɨlʼisa, </ts>
               <ts e="T236" id="Seg_3498" n="e" s="T235">korolʼt </ts>
               <ts e="T237" id="Seg_3500" n="e" s="T236">ija </ts>
               <ts e="T238" id="Seg_3502" n="e" s="T237">Elisej. </ts>
               <ts e="T239" id="Seg_3504" n="e" s="T238">Tüsa </ts>
               <ts e="T240" id="Seg_3506" n="e" s="T239">qup– </ts>
               <ts e="T241" id="Seg_3508" n="e" s="T240">sar </ts>
               <ts e="T242" id="Seg_3510" n="e" s="T241">əːtɨp </ts>
               <ts e="T243" id="Seg_3512" n="e" s="T242">misɨt, </ts>
               <ts e="T244" id="Seg_3514" n="e" s="T243">mɔːtqɨn </ts>
               <ts e="T245" id="Seg_3516" n="e" s="T244">ipsa </ts>
               <ts e="T246" id="Seg_3518" n="e" s="T245">mɨta </ts>
               <ts e="T247" id="Seg_3520" n="e" s="T246">ɛːnʼa: </ts>
               <ts e="T248" id="Seg_3522" n="e" s="T247">seːlʼčʼi </ts>
               <ts e="T249" id="Seg_3524" n="e" s="T248">təmtɨtili </ts>
               <ts e="T250" id="Seg_3526" n="e" s="T249">qɛːtɨt </ts>
               <ts e="T251" id="Seg_3528" n="e" s="T250">aje </ts>
               <ts e="T252" id="Seg_3530" n="e" s="T251">toːn </ts>
               <ts e="T253" id="Seg_3532" n="e" s="T252">aj </ts>
               <ts e="T254" id="Seg_3534" n="e" s="T253">tɛːsarɨli </ts>
               <ts e="T255" id="Seg_3536" n="e" s="T254">mɔːtɨjät. </ts>
               <ts e="T256" id="Seg_3538" n="e" s="T255">Nätatɨtkinɨ </ts>
               <ts e="T257" id="Seg_3540" n="e" s="T256">taqqɨliqo, </ts>
               <ts e="T258" id="Seg_3542" n="e" s="T257">sarɨt </ts>
               <ts e="T259" id="Seg_3544" n="e" s="T258">ima </ts>
               <ts e="T260" id="Seg_3546" n="e" s="T259">toqtatkutɨl </ts>
               <ts e="T261" id="Seg_3548" n="e" s="T260">zerkalalʼantɨsa </ts>
               <ts e="T262" id="Seg_3550" n="e" s="T261">ontɨ, </ts>
               <ts e="T263" id="Seg_3552" n="e" s="T262">əːtɨk </ts>
               <ts e="T264" id="Seg_3554" n="e" s="T263">tɛpɨn </ts>
               <ts e="T265" id="Seg_3556" n="e" s="T264">čʼattɨkkustɨ: </ts>
               <ts e="T266" id="Seg_3558" n="e" s="T265">Mat </ts>
               <ts e="T267" id="Seg_3560" n="e" s="T266">qaj, </ts>
               <ts e="T268" id="Seg_3562" n="e" s="T267">kətat </ts>
               <ts e="T269" id="Seg_3564" n="e" s="T268">mäkkä, </ts>
               <ts e="T270" id="Seg_3566" n="e" s="T269">muntok </ts>
               <ts e="T271" id="Seg_3568" n="e" s="T270">kurassa </ts>
               <ts e="T272" id="Seg_3570" n="e" s="T271">aj </ts>
               <ts e="T273" id="Seg_3572" n="e" s="T272">sərɨ </ts>
               <ts e="T274" id="Seg_3574" n="e" s="T273">ɛːnʼak? </ts>
               <ts e="T275" id="Seg_3576" n="e" s="T274">Qaj </ts>
               <ts e="T276" id="Seg_3578" n="e" s="T275">kat </ts>
               <ts e="T277" id="Seg_3580" n="e" s="T276">zerkalalʼa </ts>
               <ts e="T278" id="Seg_3582" n="e" s="T277">tompɨt? </ts>
               <ts e="T279" id="Seg_3584" n="e" s="T278">Kurassa </ts>
               <ts e="T280" id="Seg_3586" n="e" s="T279">ɛːnʼant, </ts>
               <ts e="T281" id="Seg_3588" n="e" s="T280">aša </ts>
               <ts e="T282" id="Seg_3590" n="e" s="T281">antak, </ts>
               <ts e="T283" id="Seg_3592" n="e" s="T282">sarɨt </ts>
               <ts e="T284" id="Seg_3594" n="e" s="T283">nälʼa </ts>
               <ts e="T285" id="Seg_3596" n="e" s="T284">qotəl </ts>
               <ts e="T286" id="Seg_3598" n="e" s="T285">muntokt, </ts>
               <ts e="T287" id="Seg_3600" n="e" s="T286">kurassa </ts>
               <ts e="T288" id="Seg_3602" n="e" s="T287">aj </ts>
               <ts e="T289" id="Seg_3604" n="e" s="T288">sərɨ </ts>
               <ts e="T290" id="Seg_3606" n="e" s="T289">ɛːnʼa. </ts>
               <ts e="T291" id="Seg_3608" n="e" s="T290">Sarɨt </ts>
               <ts e="T292" id="Seg_3610" n="e" s="T291">ima </ts>
               <ts e="T293" id="Seg_3612" n="e" s="T292">alpa </ts>
               <ts e="T294" id="Seg_3614" n="e" s="T293">pakta </ts>
               <ts e="T295" id="Seg_3616" n="e" s="T294">aj </ts>
               <ts e="T296" id="Seg_3618" n="e" s="T295">küratɔːlna </ts>
               <ts e="T297" id="Seg_3620" n="e" s="T296">utɨlʼantɨs, </ts>
               <ts e="T298" id="Seg_3622" n="e" s="T297">zerkalalʼap </ts>
               <ts e="T299" id="Seg_3624" n="e" s="T298">pəqšatɨnʼɨt, </ts>
               <ts e="T300" id="Seg_3626" n="e" s="T299">lʼakčʼinlʼantɨs </ts>
               <ts e="T301" id="Seg_3628" n="e" s="T300">čʼelʼčʼɔːlna </ts>
               <ts e="T302" id="Seg_3630" n="e" s="T301">nɨː! </ts>
               <ts e="T303" id="Seg_3632" n="e" s="T302">Аh, </ts>
               <ts e="T304" id="Seg_3634" n="e" s="T303">tat </ts>
               <ts e="T305" id="Seg_3636" n="e" s="T304">nılʼčʼilʼ </ts>
               <ts e="T306" id="Seg_3638" n="e" s="T305">steklonʼontɨ! </ts>
               <ts e="T307" id="Seg_3640" n="e" s="T306">Tat </ts>
               <ts e="T308" id="Seg_3642" n="e" s="T307">na </ts>
               <ts e="T309" id="Seg_3644" n="e" s="T308">moːlmɨtantɨ </ts>
               <ts e="T310" id="Seg_3646" n="e" s="T309">mat </ts>
               <ts e="T311" id="Seg_3648" n="e" s="T310">čʼɔːt. </ts>
               <ts e="T312" id="Seg_3650" n="e" s="T311">Təp </ts>
               <ts e="T313" id="Seg_3652" n="e" s="T312">kutar </ts>
               <ts e="T314" id="Seg_3654" n="e" s="T313">mannɨmmänta </ts>
               <ts e="T315" id="Seg_3656" n="e" s="T314">matsa? </ts>
               <ts e="T316" id="Seg_3658" n="e" s="T315">Mat </ts>
               <ts e="T317" id="Seg_3660" n="e" s="T316">təpɨt </ts>
               <ts e="T318" id="Seg_3662" n="e" s="T317">moːmɨt </ts>
               <ts e="T319" id="Seg_3664" n="e" s="T318">lʼamqaltantap. </ts>
               <ts e="T320" id="Seg_3666" n="e" s="T319">Qontɨrnɔːlɨt, </ts>
               <ts e="T321" id="Seg_3668" n="e" s="T320">orɨmna </ts>
               <ts e="T322" id="Seg_3670" n="e" s="T321">qaj! </ts>
               <ts e="T323" id="Seg_3672" n="e" s="T322">Kutar </ts>
               <ts e="T324" id="Seg_3674" n="e" s="T323">aša </ts>
               <ts e="T325" id="Seg_3676" n="e" s="T324">sərɨ </ts>
               <ts e="T326" id="Seg_3678" n="e" s="T325">ɛːnʼa: </ts>
               <ts e="T327" id="Seg_3680" n="e" s="T326">əmɨt </ts>
               <ts e="T328" id="Seg_3682" n="e" s="T327">pɛrqɨlʼ </ts>
               <ts e="T329" id="Seg_3684" n="e" s="T328">tıːri </ts>
               <ts e="T330" id="Seg_3686" n="e" s="T329">ɔːmtsa, </ts>
               <ts e="T331" id="Seg_3688" n="e" s="T330">kekɨs </ts>
               <ts e="T332" id="Seg_3690" n="e" s="T331">sɨront </ts>
               <ts e="T333" id="Seg_3692" n="e" s="T332">mantalpɨsa. </ts>
               <ts e="T334" id="Seg_3694" n="e" s="T333">Kɨsa </ts>
               <ts e="T335" id="Seg_3696" n="e" s="T334">kɛtat: </ts>
               <ts e="T336" id="Seg_3698" n="e" s="T335">qantɨk </ts>
               <ts e="T337" id="Seg_3700" n="e" s="T336">man </ts>
               <ts e="T338" id="Seg_3702" n="e" s="T337">nɔːn </ts>
               <ts e="T339" id="Seg_3704" n="e" s="T338">nɨːnɨ </ts>
               <ts e="T340" id="Seg_3706" n="e" s="T339">puːt </ts>
               <ts e="T341" id="Seg_3708" n="e" s="T340">təp </ts>
               <ts e="T342" id="Seg_3710" n="e" s="T341">kurassa </ts>
               <ts e="T343" id="Seg_3712" n="e" s="T342">ɛːnta? </ts>
               <ts e="T344" id="Seg_3714" n="e" s="T343">İnna </ts>
               <ts e="T345" id="Seg_3716" n="e" s="T344">kətat: </ts>
               <ts e="T346" id="Seg_3718" n="e" s="T345">mat </ts>
               <ts e="T347" id="Seg_3720" n="e" s="T346">kes </ts>
               <ts e="T348" id="Seg_3722" n="e" s="T347">nʼančʼak. </ts>
               <ts e="T349" id="Seg_3724" n="e" s="T348">Ɔːmtɨlʼ </ts>
               <ts e="T350" id="Seg_3726" n="e" s="T349">qoː </ts>
               <ts e="T351" id="Seg_3728" n="e" s="T350">mɨt </ts>
               <ts e="T352" id="Seg_3730" n="e" s="T351">kolʼaltatɨ, </ts>
               <ts e="T353" id="Seg_3732" n="e" s="T352">qos </ts>
               <ts e="T354" id="Seg_3734" n="e" s="T353">tətɨ </ts>
               <ts e="T355" id="Seg_3736" n="e" s="T354">muntɨk. </ts>
               <ts e="T356" id="Seg_3738" n="e" s="T355">Matsa </ts>
               <ts e="T357" id="Seg_3740" n="e" s="T356">čʼäːŋka </ts>
               <ts e="T358" id="Seg_3742" n="e" s="T357">nʼančʼitɨlʼ </ts>
               <ts e="T359" id="Seg_3744" n="e" s="T358">mɨ. </ts>
               <ts e="T360" id="Seg_3746" n="e" s="T359">Nılʼčʼik </ts>
               <ts e="T361" id="Seg_3748" n="e" s="T360">qɔːtɨ? </ts>
               <ts e="T362" id="Seg_3750" n="e" s="T361">Zerkalalʼa </ts>
               <ts e="T363" id="Seg_3752" n="e" s="T362">kətɨnʼɨtɨ: </ts>
               <ts e="T364" id="Seg_3754" n="e" s="T363">sarɨt </ts>
               <ts e="T365" id="Seg_3756" n="e" s="T364">nälʼa </ts>
               <ts e="T366" id="Seg_3758" n="e" s="T365">qotol, </ts>
               <ts e="T367" id="Seg_3760" n="e" s="T366">pija, </ts>
               <ts e="T368" id="Seg_3762" n="e" s="T367">kurassa </ts>
               <ts e="T369" id="Seg_3764" n="e" s="T368">aj </ts>
               <ts e="T370" id="Seg_3766" n="e" s="T369">sərɨ </ts>
               <ts e="T371" id="Seg_3768" n="e" s="T370">ɛːnʼa. </ts>
               <ts e="T372" id="Seg_3770" n="e" s="T371">Kutar </ts>
               <ts e="T373" id="Seg_3772" n="e" s="T372">meːtal. </ts>
               <ts e="T374" id="Seg_3774" n="e" s="T373">Puːtɨmt </ts>
               <ts e="T375" id="Seg_3776" n="e" s="T374">amla </ts>
               <ts e="T376" id="Seg_3778" n="e" s="T375">zerkalalʼap </ts>
               <ts e="T377" id="Seg_3780" n="e" s="T376">konna </ts>
               <ts e="T378" id="Seg_3782" n="e" s="T377">čʼattɨt. </ts>
               <ts e="T379" id="Seg_3784" n="e" s="T378">Čʼernavkamtɨ </ts>
               <ts e="T380" id="Seg_3786" n="e" s="T379">qərɨnʼɨtɨ, </ts>
               <ts e="T381" id="Seg_3788" n="e" s="T380">nılʼčʼik </ts>
               <ts e="T382" id="Seg_3790" n="e" s="T381">təptɨ </ts>
               <ts e="T383" id="Seg_3792" n="e" s="T382">quraltɨnʼɨt, </ts>
               <ts e="T384" id="Seg_3794" n="e" s="T383">(ontɨ </ts>
               <ts e="T385" id="Seg_3796" n="e" s="T384">sennɨlʼ </ts>
               <ts e="T386" id="Seg_3798" n="e" s="T385">nätak </ts>
               <ts e="T387" id="Seg_3800" n="e" s="T386">tɨna): </ts>
               <ts e="T388" id="Seg_3802" n="e" s="T387">Sarɨt </ts>
               <ts e="T389" id="Seg_3804" n="e" s="T388">nälʼap </ts>
               <ts e="T390" id="Seg_3806" n="e" s="T389">mačʼɨt </ts>
               <ts e="T391" id="Seg_3808" n="e" s="T390">puːtont </ts>
               <ts e="T392" id="Seg_3810" n="e" s="T391">qɛntɨrtaltɨl </ts>
               <ts e="T393" id="Seg_3812" n="e" s="T392">kurɨnʼɨmpɨj </ts>
               <ts e="T394" id="Seg_3814" n="e" s="T393">ilɨla </ts>
               <ts e="T395" id="Seg_3816" n="e" s="T394">aj </ts>
               <ts e="T396" id="Seg_3818" n="e" s="T395">qəːčʼinʼɨmpɨj </ts>
               <ts e="T397" id="Seg_3820" n="e" s="T396">näčʼčʼäːtɨnʼɨt </ts>
               <ts e="T398" id="Seg_3822" n="e" s="T397">čʼöːn </ts>
               <ts e="T399" id="Seg_3824" n="e" s="T398">ılnʼɨt </ts>
               <ts e="T400" id="Seg_3826" n="e" s="T399">čʼumpɨ </ts>
               <ts e="T401" id="Seg_3828" n="e" s="T400">neːet </ts>
               <ts e="T402" id="Seg_3830" n="e" s="T401">amnʼɨntitqo. </ts>
               <ts e="T403" id="Seg_3832" n="e" s="T402">Loːsɨ </ts>
               <ts e="T404" id="Seg_3834" n="e" s="T403">montɨ </ts>
               <ts e="T405" id="Seg_3836" n="e" s="T404">antɨtanta </ts>
               <ts e="T406" id="Seg_3838" n="e" s="T405">nʼenʼnʼalʼ </ts>
               <ts e="T407" id="Seg_3840" n="e" s="T406">imas? </ts>
               <ts e="T408" id="Seg_3842" n="e" s="T407">Sarɨt </ts>
               <ts e="T409" id="Seg_3844" n="e" s="T408">nälʼas </ts>
               <ts e="T410" id="Seg_3846" n="e" s="T409">Čʼernovka </ts>
               <ts e="T411" id="Seg_3848" n="e" s="T410">qənta </ts>
               <ts e="T412" id="Seg_3850" n="e" s="T411">mačʼontɨ </ts>
               <ts e="T413" id="Seg_3852" n="e" s="T412">na. </ts>
               <ts e="T414" id="Seg_3854" n="e" s="T413">Našak </ts>
               <ts e="T415" id="Seg_3856" n="e" s="T414">kuntak </ts>
               <ts e="T416" id="Seg_3858" n="e" s="T415">tɨna </ts>
               <ts e="T417" id="Seg_3860" n="e" s="T416">qəntoj </ts>
               <ts e="T418" id="Seg_3862" n="e" s="T417">sarɨt </ts>
               <ts e="T419" id="Seg_3864" n="e" s="T418">nälʼa </ts>
               <ts e="T420" id="Seg_3866" n="e" s="T419">nɨːna </ts>
               <ts e="T421" id="Seg_3868" n="e" s="T420">qənta. </ts>
               <ts e="T422" id="Seg_3870" n="e" s="T421">Qukɨntɨšak </ts>
               <ts e="T423" id="Seg_3872" n="e" s="T422">nɨrkɨmɔːnna, </ts>
               <ts e="T424" id="Seg_3874" n="e" s="T423">omtaltana: </ts>
               <ts e="T425" id="Seg_3876" n="e" s="T424">Ilɨptamɨ! </ts>
               <ts e="T426" id="Seg_3878" n="e" s="T425">Qajqɨt, </ts>
               <ts e="T427" id="Seg_3880" n="e" s="T426">kɛtat, </ts>
               <ts e="T428" id="Seg_3882" n="e" s="T427">urop </ts>
               <ts e="T429" id="Seg_3884" n="e" s="T428">ɛːnʼa? </ts>
               <ts e="T430" id="Seg_3886" n="e" s="T429">Nälɨqup, </ts>
               <ts e="T431" id="Seg_3888" n="e" s="T430">ɨkɨ </ts>
               <ts e="T432" id="Seg_3890" n="e" s="T431">sip </ts>
               <ts e="T433" id="Seg_3892" n="e" s="T432">talʼčʼolʼašik! </ts>
               <ts e="T434" id="Seg_3894" n="e" s="T433">Kutar </ts>
               <ts e="T435" id="Seg_3896" n="e" s="T434">qumɨp </ts>
               <ts e="T436" id="Seg_3898" n="e" s="T435">qontap </ts>
               <ts e="T437" id="Seg_3900" n="e" s="T436">tašınt, </ts>
               <ts e="T438" id="Seg_3902" n="e" s="T437">qajqo </ts>
               <ts e="T439" id="Seg_3904" n="e" s="T438">kɨkant– </ts>
               <ts e="T440" id="Seg_3906" n="e" s="T439">natqo </ts>
               <ts e="T441" id="Seg_3908" n="e" s="T440">meːtak. </ts>
               <ts e="T442" id="Seg_3910" n="e" s="T441">Toːnnam </ts>
               <ts e="T443" id="Seg_3912" n="e" s="T442">puːtsa </ts>
               <ts e="T444" id="Seg_3914" n="e" s="T443">təptɨ </ts>
               <ts e="T445" id="Seg_3916" n="e" s="T444">kɨkal </ts>
               <ts e="T446" id="Seg_3918" n="e" s="T445">aša </ts>
               <ts e="T447" id="Seg_3920" n="e" s="T446">kurasɨt, </ts>
               <ts e="T448" id="Seg_3922" n="e" s="T447">aša </ts>
               <ts e="T449" id="Seg_3924" n="e" s="T448">qəssɨt, </ts>
               <ts e="T450" id="Seg_3926" n="e" s="T449">uːtɨsɨtɨ, </ts>
               <ts e="T451" id="Seg_3928" n="e" s="T450">nılʼčʼik </ts>
               <ts e="T452" id="Seg_3930" n="e" s="T451">kətɨl: </ts>
               <ts e="T453" id="Seg_3932" n="e" s="T452">Qɛnʼaš, </ts>
               <ts e="T454" id="Seg_3934" n="e" s="T453">nop </ts>
               <ts e="T455" id="Seg_3936" n="e" s="T454">sıntɨ </ts>
               <ts e="T456" id="Seg_3938" n="e" s="T455">na </ts>
               <ts e="T457" id="Seg_3940" n="e" s="T456">məret. </ts>
               <ts e="T458" id="Seg_3942" n="e" s="T457">Nɨːnɨ </ts>
               <ts e="T459" id="Seg_3944" n="e" s="T458">ontɨ </ts>
               <ts e="T460" id="Seg_3946" n="e" s="T459">moqɨn </ts>
               <ts e="T461" id="Seg_3948" n="e" s="T460">tüssa. </ts>
               <ts e="T462" id="Seg_3950" n="e" s="T461">Qajɨ? </ts>
               <ts e="T463" id="Seg_3952" n="e" s="T462">–Sarɨt </ts>
               <ts e="T464" id="Seg_3954" n="e" s="T463">ima </ts>
               <ts e="T465" id="Seg_3956" n="e" s="T464">kətɨnʼɨtɨ. </ts>
               <ts e="T466" id="Seg_3958" n="e" s="T465">–Kurassɨmɨlʼ </ts>
               <ts e="T467" id="Seg_3960" n="e" s="T466">nälʼqup </ts>
               <ts e="T468" id="Seg_3962" n="e" s="T467">kun </ts>
               <ts e="T469" id="Seg_3964" n="e" s="T468">ɛːnʼ? </ts>
               <ts e="T470" id="Seg_3966" n="e" s="T469">Pɛlɨkɔːlɨk </ts>
               <ts e="T471" id="Seg_3968" n="e" s="T470">mačʼot </ts>
               <ts e="T472" id="Seg_3970" n="e" s="T471">nɨŋa,– </ts>
               <ts e="T473" id="Seg_3972" n="e" s="T472">təptɨ </ts>
               <ts e="T474" id="Seg_3974" n="e" s="T473">kətɨnʼɨtɨ </ts>
               <ts e="T475" id="Seg_3976" n="e" s="T474">təpɨn. </ts>
               <ts e="T476" id="Seg_3978" n="e" s="T475">–Sɔːrɨmpa </ts>
               <ts e="T477" id="Seg_3980" n="e" s="T476">qɔːmɨk </ts>
               <ts e="T478" id="Seg_3982" n="e" s="T477">sɨnʼte </ts>
               <ts e="T479" id="Seg_3984" n="e" s="T478">neːttɨ. </ts>
               <ts e="T480" id="Seg_3986" n="e" s="T479">Suːrɨt </ts>
               <ts e="T481" id="Seg_3988" n="e" s="T480">qatont </ts>
               <ts e="T482" id="Seg_3990" n="e" s="T481">olʼčʼačʼemmä, </ts>
               <ts e="T483" id="Seg_3992" n="e" s="T482">qɔːnak </ts>
               <ts e="T484" id="Seg_3994" n="e" s="T483">təp </ts>
               <ts e="T485" id="Seg_3996" n="e" s="T484">na </ts>
               <ts e="T486" id="Seg_3998" n="e" s="T485">kəkkɨtanɨt, </ts>
               <ts e="T487" id="Seg_4000" n="e" s="T486">qurmot </ts>
               <ts e="T488" id="Seg_4002" n="e" s="T487">aj </ts>
               <ts e="T489" id="Seg_4004" n="e" s="T488">na </ts>
               <ts e="T490" id="Seg_4006" n="e" s="T489">seːpɨ </ts>
               <ts e="T491" id="Seg_4008" n="e" s="T490">ɛːnnɨnt. </ts>
               <ts e="T492" id="Seg_4010" n="e" s="T491">Əːtɨ </ts>
               <ts e="T493" id="Seg_4012" n="e" s="T492">qənna </ts>
               <ts e="T494" id="Seg_4014" n="e" s="T493">nɨːnɨ </ts>
               <ts e="T495" id="Seg_4016" n="e" s="T494">nılʼčʼilʼ: </ts>
               <ts e="T496" id="Seg_4018" n="e" s="T495">sarɨt </ts>
               <ts e="T497" id="Seg_4020" n="e" s="T496">nälʼa </ts>
               <ts e="T498" id="Seg_4022" n="e" s="T497">urɨntana! </ts>
               <ts e="T499" id="Seg_4024" n="e" s="T498">Paširna </ts>
               <ts e="T500" id="Seg_4026" n="e" s="T499">sar </ts>
               <ts e="T501" id="Seg_4028" n="e" s="T500">nälʼantɨqo. </ts>
               <ts e="T502" id="Seg_4030" n="e" s="T501">Korolʼ </ts>
               <ts e="T503" id="Seg_4032" n="e" s="T502">ija </ts>
               <ts e="T504" id="Seg_4034" n="e" s="T503">Elisej </ts>
               <ts e="T505" id="Seg_4036" n="e" s="T504">nomtɨ </ts>
               <ts e="T506" id="Seg_4038" n="e" s="T505">omtɨtɨla </ts>
               <ts e="T507" id="Seg_4040" n="e" s="T506">puːlä </ts>
               <ts e="T508" id="Seg_4042" n="e" s="T507">toqtalta </ts>
               <ts e="T509" id="Seg_4044" n="e" s="T508">aj </ts>
               <ts e="T510" id="Seg_4046" n="e" s="T509">peːlʼa </ts>
               <ts e="T511" id="Seg_4048" n="e" s="T510">qənna </ts>
               <ts e="T512" id="Seg_4050" n="e" s="T511">kurassɨmat </ts>
               <ts e="T513" id="Seg_4052" n="e" s="T512">sıːčʼint </ts>
               <ts e="T514" id="Seg_4054" n="e" s="T513">olɨp </ts>
               <ts e="T515" id="Seg_4056" n="e" s="T514">tıːtap </ts>
               <ts e="T516" id="Seg_4058" n="e" s="T515">iːpsa </ts>
               <ts e="T517" id="Seg_4060" n="e" s="T516">imtɨ </ts>
               <ts e="T518" id="Seg_4062" n="e" s="T517">neːmtɨ. </ts>
               <ts e="T519" id="Seg_4064" n="e" s="T518">A </ts>
               <ts e="T520" id="Seg_4066" n="e" s="T519">itɨ </ts>
               <ts e="T521" id="Seg_4068" n="e" s="T520">neːtɨ </ts>
               <ts e="T522" id="Seg_4070" n="e" s="T521">qarɨt </ts>
               <ts e="T523" id="Seg_4072" n="e" s="T522">təttɨ </ts>
               <ts e="T524" id="Seg_4074" n="e" s="T523">mačʼit </ts>
               <ts e="T525" id="Seg_4076" n="e" s="T524">puːtot </ts>
               <ts e="T526" id="Seg_4078" n="e" s="T525">urkɨmpɨla </ts>
               <ts e="T527" id="Seg_4080" n="e" s="T526">kurɨtäntɨ </ts>
               <ts e="T528" id="Seg_4082" n="e" s="T527">sitɨ kotät, </ts>
               <ts e="T529" id="Seg_4084" n="e" s="T528">mɔːtɨn </ts>
               <ts e="T530" id="Seg_4086" n="e" s="T529">iːntɨ </ts>
               <ts e="T531" id="Seg_4088" n="e" s="T530">tulʼčʼeıːmpa. </ts>
               <ts e="T532" id="Seg_4090" n="e" s="T531">Čʼəssä </ts>
               <ts e="T533" id="Seg_4092" n="e" s="T532">läqa </ts>
               <ts e="T534" id="Seg_4094" n="e" s="T533">təpɨn </ts>
               <ts e="T535" id="Seg_4096" n="e" s="T534">muːttɨl </ts>
               <ts e="T536" id="Seg_4098" n="e" s="T535">läːjätɔːlta, </ts>
               <ts e="T537" id="Seg_4100" n="e" s="T536">lʼamkaltenta, </ts>
               <ts e="T538" id="Seg_4102" n="e" s="T537">lüčʼimpɨla </ts>
               <ts e="T539" id="Seg_4104" n="e" s="T538">ɔːntalpɨla. </ts>
               <ts e="T540" id="Seg_4106" n="e" s="T539">Vorotamɨt </ts>
               <ts e="T541" id="Seg_4108" n="e" s="T540">təp </ts>
               <ts e="T542" id="Seg_4110" n="e" s="T541">na </ts>
               <ts e="T543" id="Seg_4112" n="e" s="T542">seːra. </ts>
               <ts e="T544" id="Seg_4114" n="e" s="T543">Etɨmantot </ts>
               <ts e="T545" id="Seg_4116" n="e" s="T544">lʼamɨk </ts>
               <ts e="T546" id="Seg_4118" n="e" s="T545">ɛːnʼa. </ts>
               <ts e="T547" id="Seg_4120" n="e" s="T546">Läqa </ts>
               <ts e="T548" id="Seg_4122" n="e" s="T547">ɔːntalpɨlʼa </ts>
               <ts e="T549" id="Seg_4124" n="e" s="T548">nʼoːnʼɨt. </ts>
               <ts e="T550" id="Seg_4126" n="e" s="T549">A </ts>
               <ts e="T551" id="Seg_4128" n="e" s="T550">sarɨt </ts>
               <ts e="T552" id="Seg_4130" n="e" s="T551">nälʼa </ts>
               <ts e="T553" id="Seg_4132" n="e" s="T552">kurɨtelʼa </ts>
               <ts e="T554" id="Seg_4134" n="e" s="T553">krɨlʼcot </ts>
               <ts e="T555" id="Seg_4136" n="e" s="T554">iːntɨ </ts>
               <ts e="T556" id="Seg_4138" n="e" s="T555">sɨːqɨlenta, </ts>
               <ts e="T557" id="Seg_4140" n="e" s="T556">pürɨj </ts>
               <ts e="T558" id="Seg_4142" n="e" s="T557">utoːqɨntɨ </ts>
               <ts e="T559" id="Seg_4144" n="e" s="T558">iːntɨt. </ts>
               <ts e="T560" id="Seg_4146" n="e" s="T559">Mɔːtat </ts>
               <ts e="T561" id="Seg_4148" n="e" s="T560">čʼontɨka </ts>
               <ts e="T562" id="Seg_4150" n="e" s="T561">na </ts>
               <ts e="T563" id="Seg_4152" n="e" s="T562">nʼüːntɨt, </ts>
               <ts e="T564" id="Seg_4154" n="e" s="T563">mɔːttɨ </ts>
               <ts e="T565" id="Seg_4156" n="e" s="T564">čʼontɨka </ts>
               <ts e="T566" id="Seg_4158" n="e" s="T565">na </ts>
               <ts e="T567" id="Seg_4160" n="e" s="T566">seːra. </ts>
               <ts e="T568" id="Seg_4162" n="e" s="T567">Qonempa </ts>
               <ts e="T569" id="Seg_4164" n="e" s="T568">(/qonnɨmpa, </ts>
               <ts e="T570" id="Seg_4166" n="e" s="T569">qonnänta) </ts>
               <ts e="T571" id="Seg_4168" n="e" s="T570">koversa </ts>
               <ts e="T572" id="Seg_4170" n="e" s="T571">lavkat, </ts>
               <ts e="T573" id="Seg_4172" n="e" s="T572">ikonat </ts>
               <ts e="T574" id="Seg_4174" n="e" s="T573">ılqɨt </ts>
               <ts e="T575" id="Seg_4176" n="e" s="T574">dupɨlʼ </ts>
               <ts e="T576" id="Seg_4178" n="e" s="T575">lem, </ts>
               <ts e="T577" id="Seg_4180" n="e" s="T576">nəkɨrpɨlʼ </ts>
               <ts e="T578" id="Seg_4182" n="e" s="T577">ippɨpsasa </ts>
               <ts e="T579" id="Seg_4184" n="e" s="T578">šoːqɨr. </ts>
               <ts e="T580" id="Seg_4186" n="e" s="T579">Nälʼqup </ts>
               <ts e="T581" id="Seg_4188" n="e" s="T580">mannɨmpatɨ: </ts>
               <ts e="T582" id="Seg_4190" n="e" s="T581">tɨmtɨ </ts>
               <ts e="T583" id="Seg_4192" n="e" s="T582">qɔːtɨ </ts>
               <ts e="T584" id="Seg_4194" n="e" s="T583">soma </ts>
               <ts e="T585" id="Seg_4196" n="e" s="T584">qumɨt </ts>
               <ts e="T586" id="Seg_4198" n="e" s="T585">ilʼɔːt. </ts>
               <ts e="T587" id="Seg_4200" n="e" s="T586">Tɛpɨn </ts>
               <ts e="T588" id="Seg_4202" n="e" s="T587">aša </ts>
               <ts e="T589" id="Seg_4204" n="e" s="T588">toːtelʼančʼɔːt! </ts>
               <ts e="T590" id="Seg_4206" n="e" s="T589">Nɔːt </ts>
               <ts e="T591" id="Seg_4208" n="e" s="T590">sitɨ kotat </ts>
               <ts e="T592" id="Seg_4210" n="e" s="T591">ni </ts>
               <ts e="T593" id="Seg_4212" n="e" s="T592">kutɨ </ts>
               <ts e="T594" id="Seg_4214" n="e" s="T593">aš </ts>
               <ts e="T595" id="Seg_4216" n="e" s="T594">ata. </ts>
               <ts e="T596" id="Seg_4218" n="e" s="T595">Mɔːtɨp </ts>
               <ts e="T597" id="Seg_4220" n="e" s="T596">tumɨt </ts>
               <ts e="T598" id="Seg_4222" n="e" s="T597">kolʼaltɨntɨt, </ts>
               <ts e="T599" id="Seg_4224" n="e" s="T598">muntok </ts>
               <ts e="T600" id="Seg_4226" n="e" s="T599">somak </ts>
               <ts e="T601" id="Seg_4228" n="e" s="T600">täšaltɨstɨ, </ts>
               <ts e="T602" id="Seg_4230" n="e" s="T601">ikonat </ts>
               <ts e="T603" id="Seg_4232" n="e" s="T602">sečʼčʼip </ts>
               <ts e="T604" id="Seg_4234" n="e" s="T603">ačʼaltɨntɨt, </ts>
               <ts e="T605" id="Seg_4236" n="e" s="T604">šoːqɨrɨp </ts>
               <ts e="T606" id="Seg_4238" n="e" s="T605">pötpɨlla </ts>
               <ts e="T607" id="Seg_4240" n="e" s="T606">čʼetɨntɨtɨ, </ts>
               <ts e="T608" id="Seg_4242" n="e" s="T607">polatit </ts>
               <ts e="T609" id="Seg_4244" n="e" s="T608">iːntɨ </ts>
               <ts e="T610" id="Seg_4246" n="e" s="T609">sɨːqɨlelʼa </ts>
               <ts e="T611" id="Seg_4248" n="e" s="T610">lʼamɨk </ts>
               <ts e="T612" id="Seg_4250" n="e" s="T611">ılla </ts>
               <ts e="T613" id="Seg_4252" n="e" s="T612">olʼčʼeıːnta. </ts>
               <ts e="T614" id="Seg_4254" n="e" s="T613">Čʼeːlɨt </ts>
               <ts e="T615" id="Seg_4256" n="e" s="T614">čʼonnont </ts>
               <ts e="T616" id="Seg_4258" n="e" s="T615">tətčʼe </ts>
               <ts e="T617" id="Seg_4260" n="e" s="T616">ɛːsa. </ts>
               <ts e="T618" id="Seg_4262" n="e" s="T617">Sümɨ </ts>
               <ts e="T619" id="Seg_4264" n="e" s="T618">untalɨnta </ts>
               <ts e="T620" id="Seg_4266" n="e" s="T619">poːqɨt: </ts>
               <ts e="T621" id="Seg_4268" n="e" s="T620">seːlʼčʼi </ts>
               <ts e="T622" id="Seg_4270" n="e" s="T621">mɔːtɨrɨn </ts>
               <ts e="T623" id="Seg_4272" n="e" s="T622">na </ts>
               <ts e="T624" id="Seg_4274" n="e" s="T623">seːrɔːt, </ts>
               <ts e="T625" id="Seg_4276" n="e" s="T624">seːlʼčʼit </ts>
               <ts e="T626" id="Seg_4278" n="e" s="T625">muntɨk </ts>
               <ts e="T627" id="Seg_4280" n="e" s="T626">untɨlʼ uːkɨk. </ts>
               <ts e="T628" id="Seg_4282" n="e" s="T627">Mərqɨ </ts>
               <ts e="T629" id="Seg_4284" n="e" s="T628">neːtɨt </ts>
               <ts e="T630" id="Seg_4286" n="e" s="T629">nılʼčʼik </ts>
               <ts e="T631" id="Seg_4288" n="e" s="T630">ɛsa: </ts>
               <ts e="T632" id="Seg_4290" n="e" s="T631">Na </ts>
               <ts e="T633" id="Seg_4292" n="e" s="T632">lʼa </ts>
               <ts e="T634" id="Seg_4294" n="e" s="T633">qaj </ts>
               <ts e="T635" id="Seg_4296" n="e" s="T634">ɛsʼ </ts>
               <ts e="T636" id="Seg_4298" n="e" s="T635">kutar </ts>
               <ts e="T637" id="Seg_4300" n="e" s="T636">some, </ts>
               <ts e="T638" id="Seg_4302" n="e" s="T637">kutar </ts>
               <ts e="T639" id="Seg_4304" n="e" s="T638">muntɨk </ts>
               <ts e="T640" id="Seg_4306" n="e" s="T639">kurass </ts>
               <ts e="T641" id="Seg_4308" n="e" s="T640">ɛːnʼa. </ts>
               <ts e="T642" id="Seg_4310" n="e" s="T641">Kutɨ </ts>
               <ts e="T643" id="Seg_4312" n="e" s="T642">qos </ts>
               <ts e="T644" id="Seg_4314" n="e" s="T643">mɔːnmɨt </ts>
               <ts e="T645" id="Seg_4316" n="e" s="T644">təšanʼpatɨ </ts>
               <ts e="T646" id="Seg_4318" n="e" s="T645">mitɨ </ts>
               <ts e="T647" id="Seg_4320" n="e" s="T646">meːšımɨt </ts>
               <ts e="T648" id="Seg_4322" n="e" s="T647">ətakkɔːtɨj </ts>
               <ts e="T649" id="Seg_4324" n="e" s="T648">(/ätɨmpɔːtɨn). </ts>
               <ts e="T650" id="Seg_4326" n="e" s="T649">Kutontʼ? </ts>
               <ts e="T651" id="Seg_4328" n="e" s="T650">Tantaš </ts>
               <ts e="T652" id="Seg_4330" n="e" s="T651">atɨltašik, </ts>
               <ts e="T653" id="Seg_4332" n="e" s="T652">meːšım </ts>
               <ts e="T654" id="Seg_4334" n="e" s="T653">toːnɨ </ts>
               <ts e="T655" id="Seg_4336" n="e" s="T654">qotak </ts>
               <ts e="T656" id="Seg_4338" n="e" s="T655">čʼɛːtäš. </ts>
               <ts e="T657" id="Seg_4340" n="e" s="T656">Qatamol </ts>
               <ts e="T658" id="Seg_4342" n="e" s="T657">tat </ts>
               <ts e="T659" id="Seg_4344" n="e" s="T658">mɛrqɨ </ts>
               <ts e="T660" id="Seg_4346" n="e" s="T659">qumont, </ts>
               <ts e="T661" id="Seg_4348" n="e" s="T660">ilʼčʼanɨtqo </ts>
               <ts e="T662" id="Seg_4350" n="e" s="T661">meːltɨ </ts>
               <ts e="T663" id="Seg_4352" n="e" s="T662">ɛːnnant. </ts>
               <ts e="T664" id="Seg_4354" n="e" s="T663">Qatamol </ts>
               <ts e="T665" id="Seg_4356" n="e" s="T664">tat </ts>
               <ts e="T666" id="Seg_4358" n="e" s="T665">ilʼmat </ts>
               <ts e="T667" id="Seg_4360" n="e" s="T666">nʼoːntɨ, </ts>
               <ts e="T668" id="Seg_4362" n="e" s="T667">meːqin </ts>
               <ts e="T669" id="Seg_4364" n="e" s="T668">čʼopanɨtqo </ts>
               <ts e="T670" id="Seg_4366" n="e" s="T669">ɛːnnat. </ts>
               <ts e="T671" id="Seg_4368" n="e" s="T670">Qatamol </ts>
               <ts e="T672" id="Seg_4370" n="e" s="T671">tat </ts>
               <ts e="T673" id="Seg_4372" n="e" s="T672">imaqota– </ts>
               <ts e="T674" id="Seg_4374" n="e" s="T673">qɛrʼantɨmɨt </ts>
               <ts e="T675" id="Seg_4376" n="e" s="T674">ämɨnɨtqo. </ts>
               <ts e="T676" id="Seg_4378" n="e" s="T675">Qatamol </ts>
               <ts e="T677" id="Seg_4380" n="e" s="T676">tat </ts>
               <ts e="T678" id="Seg_4382" n="e" s="T677">nälʼ </ts>
               <ts e="T679" id="Seg_4384" n="e" s="T678">qumontɨ– </ts>
               <ts e="T680" id="Seg_4386" n="e" s="T679">meːqin </ts>
               <ts e="T681" id="Seg_4388" n="e" s="T680">nʼenʼnʼanɨtqo </ts>
               <ts e="T682" id="Seg_4390" n="e" s="T681">ɛːnnat. </ts>
               <ts e="T683" id="Seg_4392" n="e" s="T682">Sarɨt </ts>
               <ts e="T684" id="Seg_4394" n="e" s="T683">nälʼa </ts>
               <ts e="T685" id="Seg_4396" n="e" s="T684">ılla </ts>
               <ts e="T686" id="Seg_4398" n="e" s="T685">pančʼa </ts>
               <ts e="T687" id="Seg_4400" n="e" s="T686">(/ılʼlʼɨmpanɨ), </ts>
               <ts e="T688" id="Seg_4402" n="e" s="T687">torowatɨnt </ts>
               <ts e="T689" id="Seg_4404" n="e" s="T688">təpɨtɨsa, </ts>
               <ts e="T690" id="Seg_4406" n="e" s="T689">čʼüːntɨt </ts>
               <ts e="T691" id="Seg_4408" n="e" s="T690">tättɨ </ts>
               <ts e="T692" id="Seg_4410" n="e" s="T691">mušqaıːtɨla </ts>
               <ts e="T693" id="Seg_4412" n="e" s="T692">(/nʼuːtɨ </ts>
               <ts e="T694" id="Seg_4414" n="e" s="T693">sočʼ). </ts>
               <ts e="T695" id="Seg_4416" n="e" s="T694">Nʼertarɨs, </ts>
               <ts e="T696" id="Seg_4418" n="e" s="T695">nılʼ </ts>
               <ts e="T697" id="Seg_4420" n="e" s="T696">ɛsɨt: </ts>
               <ts e="T698" id="Seg_4422" n="e" s="T697">Mɔːtqɨntit </ts>
               <ts e="T699" id="Seg_4424" n="e" s="T698">mol </ts>
               <ts e="T700" id="Seg_4426" n="e" s="T699">seːrsak </ts>
               <ts e="T701" id="Seg_4428" n="e" s="T700">mat </ts>
               <ts e="T702" id="Seg_4430" n="e" s="T701">tiː, </ts>
               <ts e="T703" id="Seg_4432" n="e" s="T702">qos </ts>
               <ts e="T704" id="Seg_4434" n="e" s="T703">sıt </ts>
               <ts e="T705" id="Seg_4436" n="e" s="T704">qɛrɨptälɨt </ts>
               <ts e="T706" id="Seg_4438" n="e" s="T705">čʼäːnʼa. </ts>
               <ts e="T707" id="Seg_4440" n="e" s="T706">Nɨːnɨ </ts>
               <ts e="T708" id="Seg_4442" n="e" s="T707">ɛːtomɨntɨ </ts>
               <ts e="T709" id="Seg_4444" n="e" s="T708">qəntɔːtɨt: </ts>
               <ts e="T710" id="Seg_4446" n="e" s="T709">tam </ts>
               <ts e="T711" id="Seg_4448" n="e" s="T710">aš </ts>
               <ts e="T712" id="Seg_4450" n="e" s="T711">sarɨt </ts>
               <ts e="T713" id="Seg_4452" n="e" s="T712">nälʼa </ts>
               <ts e="T714" id="Seg_4454" n="e" s="T713">ɛːsa. </ts>
               <ts e="T715" id="Seg_4456" n="e" s="T714">Sänʼälʼantɨ </ts>
               <ts e="T716" id="Seg_4458" n="e" s="T715">omtɨltɨsɔːt, </ts>
               <ts e="T717" id="Seg_4460" n="e" s="T716">nʼanʼilʼasa </ts>
               <ts e="T718" id="Seg_4462" n="e" s="T717">(/nʼänʼilʼäsa) </ts>
               <ts e="T719" id="Seg_4464" n="e" s="T718">milʼčʼečʼesɔːt, </ts>
               <ts e="T720" id="Seg_4466" n="e" s="T719">rʼumkaj </ts>
               <ts e="T721" id="Seg_4468" n="e" s="T720">tıːrla </ts>
               <ts e="T722" id="Seg_4470" n="e" s="T721">qamtečʼesɔːt, </ts>
               <ts e="T723" id="Seg_4472" n="e" s="T722">ilit </ts>
               <ts e="T724" id="Seg_4474" n="e" s="T723">iːqɨt </ts>
               <ts e="T725" id="Seg_4476" n="e" s="T724">taːtäčʼesɔːtɨ. </ts>
               <ts e="T726" id="Seg_4478" n="e" s="T725">Tapɨlʼ </ts>
               <ts e="T727" id="Seg_4480" n="e" s="T726">utɨp </ts>
               <ts e="T728" id="Seg_4482" n="e" s="T727">aša </ts>
               <ts e="T729" id="Seg_4484" n="e" s="T728">iːstɨ, </ts>
               <ts e="T730" id="Seg_4486" n="e" s="T729">nʼanʼilʼap </ts>
               <ts e="T731" id="Seg_4488" n="e" s="T730">kes </ts>
               <ts e="T732" id="Seg_4490" n="e" s="T731">sepɨtela, </ts>
               <ts e="T733" id="Seg_4492" n="e" s="T732">kɨpɛ </ts>
               <ts e="T734" id="Seg_4494" n="e" s="T733">lıːpɨmt </ts>
               <ts e="T735" id="Seg_4496" n="e" s="T734">ɔːkɨnt </ts>
               <ts e="T736" id="Seg_4498" n="e" s="T735">iːstɨ, </ts>
               <ts e="T737" id="Seg_4500" n="e" s="T736">wɛtɨn </ts>
               <ts e="T738" id="Seg_4502" n="e" s="T737">nɔːnɨ </ts>
               <ts e="T739" id="Seg_4504" n="e" s="T738">nʼɨšqɨntoːqo </ts>
               <ts e="T740" id="Seg_4506" n="e" s="T739">koptɨtqo </ts>
               <ts e="T741" id="Seg_4508" n="e" s="T740">no </ts>
               <ts e="T742" id="Seg_4510" n="e" s="T741">mɔːtɨrana. </ts>
               <ts e="T743" id="Seg_4512" n="e" s="T742">Qɛntɨsɔːtɨt </ts>
               <ts e="T744" id="Seg_4514" n="e" s="T743">tɛpɨt </ts>
               <ts e="T745" id="Seg_4516" n="e" s="T744">nätap </ts>
               <ts e="T746" id="Seg_4518" n="e" s="T745">ınna </ts>
               <ts e="T747" id="Seg_4520" n="e" s="T746">čʼeːlɨnʼpɨtɨlʼ </ts>
               <ts e="T748" id="Seg_4522" n="e" s="T747">sünnöt </ts>
               <ts e="T749" id="Seg_4524" n="e" s="T748">aj </ts>
               <ts e="T750" id="Seg_4526" n="e" s="T749">ontɨqɨt </ts>
               <ts e="T751" id="Seg_4528" n="e" s="T750">na </ts>
               <ts e="T752" id="Seg_4530" n="e" s="T751">(/nɨmtɨ) </ts>
               <ts e="T753" id="Seg_4532" n="e" s="T752">qəːčʼɨntɔːtɨt </ts>
               <ts e="T754" id="Seg_4534" n="e" s="T753">(/qəːčʼɨsɔːtɨt) </ts>
               <ts e="T755" id="Seg_4536" n="e" s="T754">təpɨp </ts>
               <ts e="T756" id="Seg_4538" n="e" s="T755">ɔːŋkɨtitɨl </ts>
               <ts e="T757" id="Seg_4540" n="e" s="T756">pɛlɨkɔːlɨk </ts>
               <ts e="T758" id="Seg_4542" n="e" s="T757">((GAP)). </ts>
               <ts e="T759" id="Seg_4544" n="e" s="T758">Nʼarqɨ </ts>
               <ts e="T760" id="Seg_4546" n="e" s="T759">čʼeːlon </ts>
               <ts e="T761" id="Seg_4548" n="e" s="T760">(/čʼeːlʼi) </ts>
               <ts e="T762" id="Seg_4550" n="e" s="T761">moːroqɨntɨ </ts>
               <ts e="T763" id="Seg_4552" n="e" s="T762">ilʼmat </ts>
               <ts e="T764" id="Seg_4554" n="e" s="T763">koraltɨpsa: </ts>
               <ts e="T765" id="Seg_4556" n="e" s="T764">Čʼeːlɨnʼpɨta </ts>
               <ts e="T766" id="Seg_4558" n="e" s="T765">čʼeːlɨmɨt </ts>
               <ts e="T767" id="Seg_4560" n="e" s="T766">meː! </ts>
               <ts e="T768" id="Seg_4562" n="e" s="T767">Tat </ts>
               <ts e="T769" id="Seg_4564" n="e" s="T768">karanʼnʼantɨ </ts>
               <ts e="T770" id="Seg_4566" n="e" s="T769">(/kutornantɨ) </ts>
               <ts e="T771" id="Seg_4568" n="e" s="T770">poːntɨ </ts>
               <ts e="T772" id="Seg_4570" n="e" s="T771">kuntɨ </ts>
               <ts e="T773" id="Seg_4572" n="e" s="T772">nuːt </ts>
               <ts e="T774" id="Seg_4574" n="e" s="T773">nekmɨ, </ts>
               <ts e="T775" id="Seg_4576" n="e" s="T774">totaltukkal </ts>
               <ts e="T776" id="Seg_4578" n="e" s="T775">kəp </ts>
               <ts e="T777" id="Seg_4580" n="e" s="T776">petpɨlʼ </ts>
               <ts e="T778" id="Seg_4582" n="e" s="T777">utɨsa </ts>
               <ts e="T779" id="Seg_4584" n="e" s="T778">mutokt </ts>
               <ts e="T780" id="Seg_4586" n="e" s="T779">meːsım </ts>
               <ts e="T781" id="Seg_4588" n="e" s="T780">onant </ts>
               <ts e="T782" id="Seg_4590" n="e" s="T781">ılqɨt </ts>
               <ts e="T783" id="Seg_4592" n="e" s="T782">qontɨrnantɨ. </ts>
               <ts e="T784" id="Seg_4594" n="e" s="T783">Montɨ </ts>
               <ts e="T785" id="Seg_4596" n="e" s="T784">əːtop </ts>
               <ts e="T786" id="Seg_4598" n="e" s="T785">toː </ts>
               <ts e="T787" id="Seg_4600" n="e" s="T786">čʼatantal? </ts>
               <ts e="T788" id="Seg_4602" n="e" s="T787">Qontɨrsal </ts>
               <ts e="T789" id="Seg_4604" n="e" s="T788">qaj </ts>
               <ts e="T790" id="Seg_4606" n="e" s="T789">kun </ts>
               <ts e="T791" id="Seg_4608" n="e" s="T790">ɛːm </ts>
               <ts e="T792" id="Seg_4610" n="e" s="T791">tətot </ts>
               <ts e="T793" id="Seg_4612" n="e" s="T792">sarɨt </ts>
               <ts e="T794" id="Seg_4614" n="e" s="T793">nälʼap </ts>
               <ts e="T795" id="Seg_4616" n="e" s="T794">ilʼmatɨlʼ </ts>
               <ts e="T796" id="Seg_4618" n="e" s="T795">tat </ts>
               <ts e="T797" id="Seg_4620" n="e" s="T796">((GAP)). </ts>
               <ts e="T798" id="Seg_4622" n="e" s="T797">Irät, </ts>
               <ts e="T799" id="Seg_4624" n="e" s="T798">irät, </ts>
               <ts e="T800" id="Seg_4626" n="e" s="T799">onak </ts>
               <ts e="T801" id="Seg_4628" n="e" s="T800">neːmɨ, </ts>
               <ts e="T802" id="Seg_4630" n="e" s="T801">zoločʼenɨlʼ </ts>
               <ts e="T803" id="Seg_4632" n="e" s="T802">ɔːttɨlʼa! </ts>
               <ts e="T804" id="Seg_4634" n="e" s="T803">Məšikkantɨ </ts>
               <ts e="T805" id="Seg_4636" n="e" s="T804">korel </ts>
               <ts e="T806" id="Seg_4638" n="e" s="T805">pit </ts>
               <ts e="T807" id="Seg_4640" n="e" s="T806">tat, </ts>
               <ts e="T808" id="Seg_4642" n="e" s="T807">pürɨlʼ </ts>
               <ts e="T809" id="Seg_4644" n="e" s="T808">mɛntɨlʼ, </ts>
               <ts e="T810" id="Seg_4646" n="e" s="T809">čʼeːlɨlʼ </ts>
               <ts e="T811" id="Seg_4648" n="e" s="T810">sailʼ. </ts>
               <ts e="T812" id="Seg_4650" n="e" s="T811">Aj </ts>
               <ts e="T813" id="Seg_4652" n="e" s="T812">mat </ts>
               <ts e="T814" id="Seg_4654" n="e" s="T813">serlɨ </ts>
               <ts e="T815" id="Seg_4656" n="e" s="T814">kɨkɨla </ts>
               <ts e="T816" id="Seg_4658" n="e" s="T815">qıšqat </ts>
               <ts e="T817" id="Seg_4660" n="e" s="T816">tantɨ </ts>
               <ts e="T818" id="Seg_4662" n="e" s="T817">mannɨmpɔːtɨt </ts>
               <ts e="T819" id="Seg_4664" n="e" s="T818">((GAP)). </ts>
               <ts e="T820" id="Seg_4666" n="e" s="T819">Näčʼät </ts>
               <ts e="T821" id="Seg_4668" n="e" s="T820">čʼontɨ </ts>
               <ts e="T822" id="Seg_4670" n="e" s="T821">künɨlʼ </ts>
               <ts e="T823" id="Seg_4672" n="e" s="T822">kɨqat </ts>
               <ts e="T824" id="Seg_4674" n="e" s="T823">toap </ts>
               <ts e="T825" id="Seg_4676" n="e" s="T824">ɛːnʼa </ts>
               <ts e="T826" id="Seg_4678" n="e" s="T825">pirqɨ </ts>
               <ts e="T827" id="Seg_4680" n="e" s="T826">toanʼ, </ts>
               <ts e="T828" id="Seg_4682" n="e" s="T827">nɨmtɨ </ts>
               <ts e="T829" id="Seg_4684" n="e" s="T828">korɨ </ts>
               <ts e="T830" id="Seg_4686" n="e" s="T829">mü. </ts>
               <ts e="T831" id="Seg_4688" n="e" s="T830">Pitɨ </ts>
               <ts e="T832" id="Seg_4690" n="e" s="T831">matɨ </ts>
               <ts e="T833" id="Seg_4692" n="e" s="T832">tətɨ </ts>
               <ts e="T834" id="Seg_4694" n="e" s="T833">((GAP)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_4695" s="T0">SAG_1965_TaleOfDeadPrincess_transl.001 (001.001)</ta>
            <ta e="T14" id="Seg_4696" s="T7">SAG_1965_TaleOfDeadPrincess_transl.002 (001.002)</ta>
            <ta e="T24" id="Seg_4697" s="T14">SAG_1965_TaleOfDeadPrincess_transl.003 (001.003)</ta>
            <ta e="T35" id="Seg_4698" s="T24">SAG_1965_TaleOfDeadPrincess_transl.004 (001.004)</ta>
            <ta e="T39" id="Seg_4699" s="T35">SAG_1965_TaleOfDeadPrincess_transl.005 (001.005)</ta>
            <ta e="T53" id="Seg_4700" s="T39">SAG_1965_TaleOfDeadPrincess_transl.006 (001.006)</ta>
            <ta e="T59" id="Seg_4701" s="T53">SAG_1965_TaleOfDeadPrincess_transl.007 (001.007)</ta>
            <ta e="T65" id="Seg_4702" s="T59">SAG_1965_TaleOfDeadPrincess_transl.008 (001.008)</ta>
            <ta e="T74" id="Seg_4703" s="T65">SAG_1965_TaleOfDeadPrincess_transl.009 (001.009)</ta>
            <ta e="T88" id="Seg_4704" s="T74">SAG_1965_TaleOfDeadPrincess_transl.010 (001.010)</ta>
            <ta e="T100" id="Seg_4705" s="T88">SAG_1965_TaleOfDeadPrincess_transl.011 (001.011)</ta>
            <ta e="T104" id="Seg_4706" s="T100">SAG_1965_TaleOfDeadPrincess_transl.012 (002.001)</ta>
            <ta e="T106" id="Seg_4707" s="T104">SAG_1965_TaleOfDeadPrincess_transl.013 (002.002)</ta>
            <ta e="T108" id="Seg_4708" s="T106">SAG_1965_TaleOfDeadPrincess_transl.014 (002.003)</ta>
            <ta e="T113" id="Seg_4709" s="T108">SAG_1965_TaleOfDeadPrincess_transl.015 (002.004)</ta>
            <ta e="T118" id="Seg_4710" s="T113">SAG_1965_TaleOfDeadPrincess_transl.016 (002.005)</ta>
            <ta e="T134" id="Seg_4711" s="T118">SAG_1965_TaleOfDeadPrincess_transl.017 (002.006)</ta>
            <ta e="T142" id="Seg_4712" s="T134">SAG_1965_TaleOfDeadPrincess_transl.018 (002.007)</ta>
            <ta e="T148" id="Seg_4713" s="T142">SAG_1965_TaleOfDeadPrincess_transl.019 (002.008)</ta>
            <ta e="T153" id="Seg_4714" s="T148">SAG_1965_TaleOfDeadPrincess_transl.020 (002.009)</ta>
            <ta e="T168" id="Seg_4715" s="T153">SAG_1965_TaleOfDeadPrincess_transl.021 (002.010)</ta>
            <ta e="T182" id="Seg_4716" s="T168">SAG_1965_TaleOfDeadPrincess_transl.022 (002.011)</ta>
            <ta e="T198" id="Seg_4717" s="T182">SAG_1965_TaleOfDeadPrincess_transl.023 (002.012)</ta>
            <ta e="T212" id="Seg_4718" s="T198">SAG_1965_TaleOfDeadPrincess_transl.024 (002.013)</ta>
            <ta e="T232" id="Seg_4719" s="T212">SAG_1965_TaleOfDeadPrincess_transl.025 (003.001)</ta>
            <ta e="T238" id="Seg_4720" s="T232">SAG_1965_TaleOfDeadPrincess_transl.026 (003.002)</ta>
            <ta e="T255" id="Seg_4721" s="T238">SAG_1965_TaleOfDeadPrincess_transl.027 (003.003)</ta>
            <ta e="T274" id="Seg_4722" s="T255">SAG_1965_TaleOfDeadPrincess_transl.028 (004.001)</ta>
            <ta e="T278" id="Seg_4723" s="T274">SAG_1965_TaleOfDeadPrincess_transl.029 (004.002)</ta>
            <ta e="T290" id="Seg_4724" s="T278">SAG_1965_TaleOfDeadPrincess_transl.030 (004.003)</ta>
            <ta e="T302" id="Seg_4725" s="T290">SAG_1965_TaleOfDeadPrincess_transl.031 (004.004)</ta>
            <ta e="T306" id="Seg_4726" s="T302">SAG_1965_TaleOfDeadPrincess_transl.032 (004.005)</ta>
            <ta e="T311" id="Seg_4727" s="T306">SAG_1965_TaleOfDeadPrincess_transl.033 (004.006)</ta>
            <ta e="T315" id="Seg_4728" s="T311">SAG_1965_TaleOfDeadPrincess_transl.034 (004.007)</ta>
            <ta e="T319" id="Seg_4729" s="T315">SAG_1965_TaleOfDeadPrincess_transl.035 (004.008)</ta>
            <ta e="T322" id="Seg_4730" s="T319">SAG_1965_TaleOfDeadPrincess_transl.036 (004.009)</ta>
            <ta e="T333" id="Seg_4731" s="T322">SAG_1965_TaleOfDeadPrincess_transl.037 (004.010)</ta>
            <ta e="T343" id="Seg_4732" s="T333">SAG_1965_TaleOfDeadPrincess_transl.038 (004.011)</ta>
            <ta e="T348" id="Seg_4733" s="T343">SAG_1965_TaleOfDeadPrincess_transl.039 (004.012)</ta>
            <ta e="T355" id="Seg_4734" s="T348">SAG_1965_TaleOfDeadPrincess_transl.040 (004.013)</ta>
            <ta e="T359" id="Seg_4735" s="T355">SAG_1965_TaleOfDeadPrincess_transl.041 (004.014)</ta>
            <ta e="T361" id="Seg_4736" s="T359">SAG_1965_TaleOfDeadPrincess_transl.042 (004.015)</ta>
            <ta e="T371" id="Seg_4737" s="T361">SAG_1965_TaleOfDeadPrincess_transl.043 (004.016)</ta>
            <ta e="T373" id="Seg_4738" s="T371">SAG_1965_TaleOfDeadPrincess_transl.044 (004.017)</ta>
            <ta e="T378" id="Seg_4739" s="T373">SAG_1965_TaleOfDeadPrincess_transl.045 (004.018)</ta>
            <ta e="T402" id="Seg_4740" s="T378">SAG_1965_TaleOfDeadPrincess_transl.046 (004.019)</ta>
            <ta e="T407" id="Seg_4741" s="T402">SAG_1965_TaleOfDeadPrincess_transl.047 (005.001)</ta>
            <ta e="T413" id="Seg_4742" s="T407">SAG_1965_TaleOfDeadPrincess_transl.048 (005.002)</ta>
            <ta e="T421" id="Seg_4743" s="T413">SAG_1965_TaleOfDeadPrincess_transl.049 (005.003)</ta>
            <ta e="T425" id="Seg_4744" s="T421">SAG_1965_TaleOfDeadPrincess_transl.050 (005.004)</ta>
            <ta e="T429" id="Seg_4745" s="T425">SAG_1965_TaleOfDeadPrincess_transl.051 (005.005)</ta>
            <ta e="T433" id="Seg_4746" s="T429">SAG_1965_TaleOfDeadPrincess_transl.052 (005.006)</ta>
            <ta e="T441" id="Seg_4747" s="T433">SAG_1965_TaleOfDeadPrincess_transl.053 (005.007)</ta>
            <ta e="T457" id="Seg_4748" s="T441">SAG_1965_TaleOfDeadPrincess_transl.054 (005.008)</ta>
            <ta e="T461" id="Seg_4749" s="T457">SAG_1965_TaleOfDeadPrincess_transl.055 (005.009)</ta>
            <ta e="T462" id="Seg_4750" s="T461">SAG_1965_TaleOfDeadPrincess_transl.056 (005.010)</ta>
            <ta e="T465" id="Seg_4751" s="T462">SAG_1965_TaleOfDeadPrincess_transl.057 (005.011)</ta>
            <ta e="T469" id="Seg_4752" s="T465">SAG_1965_TaleOfDeadPrincess_transl.058 (005.012)</ta>
            <ta e="T475" id="Seg_4753" s="T469">SAG_1965_TaleOfDeadPrincess_transl.059 (005.013)</ta>
            <ta e="T479" id="Seg_4754" s="T475">SAG_1965_TaleOfDeadPrincess_transl.060 (005.014)</ta>
            <ta e="T491" id="Seg_4755" s="T479">SAG_1965_TaleOfDeadPrincess_transl.061 (005.015)</ta>
            <ta e="T498" id="Seg_4756" s="T491">SAG_1965_TaleOfDeadPrincess_transl.062 (006.001)</ta>
            <ta e="T501" id="Seg_4757" s="T498">SAG_1965_TaleOfDeadPrincess_transl.063 (006.002)</ta>
            <ta e="T518" id="Seg_4758" s="T501">SAG_1965_TaleOfDeadPrincess_transl.064 (006.003)</ta>
            <ta e="T531" id="Seg_4759" s="T518">SAG_1965_TaleOfDeadPrincess_transl.065 (007.001)</ta>
            <ta e="T539" id="Seg_4760" s="T531">SAG_1965_TaleOfDeadPrincess_transl.066 (007.002)</ta>
            <ta e="T543" id="Seg_4761" s="T539">SAG_1965_TaleOfDeadPrincess_transl.067 (007.003)</ta>
            <ta e="T546" id="Seg_4762" s="T543">SAG_1965_TaleOfDeadPrincess_transl.068 (007.004)</ta>
            <ta e="T549" id="Seg_4763" s="T546">SAG_1965_TaleOfDeadPrincess_transl.069 (007.005)</ta>
            <ta e="T559" id="Seg_4764" s="T549">SAG_1965_TaleOfDeadPrincess_transl.070 (007.006)</ta>
            <ta e="T567" id="Seg_4765" s="T559">SAG_1965_TaleOfDeadPrincess_transl.071 (007.007)</ta>
            <ta e="T579" id="Seg_4766" s="T567">SAG_1965_TaleOfDeadPrincess_transl.072 (007.008)</ta>
            <ta e="T586" id="Seg_4767" s="T579">SAG_1965_TaleOfDeadPrincess_transl.073 (007.009)</ta>
            <ta e="T589" id="Seg_4768" s="T586">SAG_1965_TaleOfDeadPrincess_transl.074 (007.010)</ta>
            <ta e="T595" id="Seg_4769" s="T589">SAG_1965_TaleOfDeadPrincess_transl.075 (007.011)</ta>
            <ta e="T613" id="Seg_4770" s="T595">SAG_1965_TaleOfDeadPrincess_transl.076 (007.012)</ta>
            <ta e="T617" id="Seg_4771" s="T613">SAG_1965_TaleOfDeadPrincess_transl.077 (008.001)</ta>
            <ta e="T627" id="Seg_4772" s="T617">SAG_1965_TaleOfDeadPrincess_transl.078 (008.002)</ta>
            <ta e="T641" id="Seg_4773" s="T627">SAG_1965_TaleOfDeadPrincess_transl.079 (008.003)</ta>
            <ta e="T649" id="Seg_4774" s="T641">SAG_1965_TaleOfDeadPrincess_transl.080 (008.004)</ta>
            <ta e="T650" id="Seg_4775" s="T649">SAG_1965_TaleOfDeadPrincess_transl.081 (008.005)</ta>
            <ta e="T656" id="Seg_4776" s="T650">SAG_1965_TaleOfDeadPrincess_transl.082 (008.006)</ta>
            <ta e="T663" id="Seg_4777" s="T656">SAG_1965_TaleOfDeadPrincess_transl.083 (008.007)</ta>
            <ta e="T670" id="Seg_4778" s="T663">SAG_1965_TaleOfDeadPrincess_transl.084 (008.008)</ta>
            <ta e="T675" id="Seg_4779" s="T670">SAG_1965_TaleOfDeadPrincess_transl.085 (008.009)</ta>
            <ta e="T682" id="Seg_4780" s="T675">SAG_1965_TaleOfDeadPrincess_transl.086 (008.010)</ta>
            <ta e="T694" id="Seg_4781" s="T682">SAG_1965_TaleOfDeadPrincess_transl.087 (009.001)</ta>
            <ta e="T706" id="Seg_4782" s="T694">SAG_1965_TaleOfDeadPrincess_transl.088 (009.002)</ta>
            <ta e="T714" id="Seg_4783" s="T706">SAG_1965_TaleOfDeadPrincess_transl.089 (009.003)</ta>
            <ta e="T725" id="Seg_4784" s="T714">SAG_1965_TaleOfDeadPrincess_transl.090 (009.004)</ta>
            <ta e="T742" id="Seg_4785" s="T725">SAG_1965_TaleOfDeadPrincess_transl.091 (009.005)</ta>
            <ta e="T758" id="Seg_4786" s="T742">SAG_1965_TaleOfDeadPrincess_transl.092 (009.006)</ta>
            <ta e="T767" id="Seg_4787" s="T758">SAG_1965_TaleOfDeadPrincess_transl.093 (010.001)</ta>
            <ta e="T783" id="Seg_4788" s="T767">SAG_1965_TaleOfDeadPrincess_transl.094 (010.002)</ta>
            <ta e="T787" id="Seg_4789" s="T783">SAG_1965_TaleOfDeadPrincess_transl.095 (010.003)</ta>
            <ta e="T797" id="Seg_4790" s="T787">SAG_1965_TaleOfDeadPrincess_transl.096 (010.004)</ta>
            <ta e="T803" id="Seg_4791" s="T797">SAG_1965_TaleOfDeadPrincess_transl.097 (011.001)</ta>
            <ta e="T811" id="Seg_4792" s="T803">SAG_1965_TaleOfDeadPrincess_transl.098 (011.002)</ta>
            <ta e="T819" id="Seg_4793" s="T811">SAG_1965_TaleOfDeadPrincess_transl.099 (011.003)</ta>
            <ta e="T830" id="Seg_4794" s="T819">SAG_1965_TaleOfDeadPrincess_transl.100 (012.001)</ta>
            <ta e="T834" id="Seg_4795" s="T830">SAG_1965_TaleOfDeadPrincess_transl.101 (012.002)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_4796" s="T0">сар ӣ′ман[м]тыса нэтырсыңа [нэтырсыты, нэкӓрсы], / kунтак вəттонт тоkталтыңа. / </ta>
            <ta e="T14" id="Seg_4797" s="T7">окнот kанты иматы / тэпып этыkо онты омта. /</ta>
            <ta e="T24" id="Seg_4798" s="T14">kарын ноны ӱт[т]ыт тэты / этыkыты, этыkыты, / мелты лимонт маннымпа тэп. / </ta>
            <ta e="T35" id="Seg_4799" s="T24">саилʼ токты манталпылʼа / чущалысот кутылʼ е′мылʼа / kарыль кэннынт укоkыны питтэт. /</ta>
            <ta e="T39" id="Seg_4800" s="T35">о′мынеты аша ата! /</ta>
            <ta e="T53" id="Seg_4801" s="T39">кекысе тəп маннымпаты: / меркыль пальча кючинимпа, / сыры чомна лимыт инды, / тəтты мундык сэры е′са. /</ta>
            <ta e="T59" id="Seg_4802" s="T53">kəнна [kӓнна] ок[к]ырче кёт ′ӣрет. / </ta>
            <ta e="T65" id="Seg_4803" s="T59">лимоkын саимт[ы] а′лп[б]а аш[шʼ]а итыт [′ӣтыңыт]. /</ta>
            <ta e="T74" id="Seg_4804" s="T65">нʼот сочельникаkыт, пит, / тəпын миныт нелям[н] [′не̨лʼамты] ном [‵номты]. /</ta>
            <ta e="T88" id="Seg_4805" s="T74">ремкы кʼарыт нотныль гость, / писа чел[ы]са кунты əтыпыль, / кʼунтакʼыны нʼоты кесыс / моткʼын тʼуса сар-′əсыты. /</ta>
            <ta e="T100" id="Seg_4806" s="T88">мантэчекус тəпыткины, / сэттымела кэшкʼылʼеса, / о′ннымт аша сʼепы′релсыт [сепырсы] / а′е челы чонтот у′речеса. /</ta>
            <ta e="T104" id="Seg_4807" s="T100">кеккалпыса [наккалбыс] кунды сар, / </ta>
            <ta e="T106" id="Seg_4808" s="T104">кутар е′кʼо?</ta>
            <ta e="T108" id="Seg_4809" s="T106">грешнык э′па [эппа]. /</ta>
            <ta e="T113" id="Seg_4810" s="T108">поты кэнна [kӓнны], мит[т]ы о′нʼкы. / </ta>
            <ta e="T118" id="Seg_4811" s="T113">мəныль [вӓныль] имап ынʼыт сар. /</ta>
            <ta e="T134" id="Seg_4812" s="T118">кутар кэтыkо – нель kуп е′са / о′нылʼ миты сарылʼ има: / пиркы тащик. сəры е′са, / тэнылтокса, му[н]докт исыт. /</ta>
            <ta e="T142" id="Seg_4813" s="T134">нынай нʼоты сомальчымтʼат, / кʼотол нʼоты ярык е′са. /</ta>
            <ta e="T148" id="Seg_4814" s="T142">мотыноны татымпысты / кекыс зеркалалям окыр. /</ta>
            <ta e="T153" id="Seg_4815" s="T148">зеркалаля нильчиль е′са: / конʼымпытко тʼенымымпа. /</ta>
            <ta e="T168" id="Seg_4816" s="T153">тəпса окыр тəпкян е′кка / сома путык, о′нталпыка, / тəпса щокʼкʼак карампыка / сомальчимпыл конʼымпыка: / “челом, зеркалалям!</ta>
            <ta e="T182" id="Seg_4817" s="T168">кэтʼат / на[па], чёлык инна пинты: / маткʼай тəтын икʼыт мундокт / сома кʼурас, сэры е′нʼак?” /</ta>
            <ta e="T198" id="Seg_4818" s="T182">зеркалаля кəтыкыты: / “тат сарыт има, аша антак, / тат кес окыр мундокт сома / кураса ай сэры е′нʼант.” /</ta>
            <ta e="T212" id="Seg_4819" s="T198">сарыт има лаккымонна, / кʼəкты порым ипкʼалныты. / саинтыса рыпкалтʼенʼыт. / мунынтыса кящилтʼеныт, / утып кʼенʼынт компылымпа, / зеркалалянт манымпыла. /</ta>
            <ta e="T232" id="Seg_4820" s="T212">сарыт неля – чиляль нетэк / лямык, чондык орымнанда. / нат ситы котʼят орымнанда, / мəчищинда, орымтана, / сəры мəчик, сʼя саин ундык, / селмо колык, ′тотык путык. /</ta>
            <ta e="T238" id="Seg_4821" s="T232">тʼиталь кʼумты пекʼылʼиса, / корольт ия Елисей. /</ta>
            <ta e="T255" id="Seg_4822" s="T238">тʼуса кʼуп – сар əтып мисыт, / мотkын ипса мыта е′нʼа: / сельчи тəмтытʼили кʼэтыт / ае тон ай тʼесарыли мотыет. /</ta>
            <ta e="T274" id="Seg_4823" s="T255">нʼетатыткины такʼылико, / сарыт има ток′таткутып[л] / зеркалалянтыса онды, / əтык тэпын чаттыкусты: / мат кʼай, кə′тат мяккя, ′мундок / курасса ай сəры е′нʼак? /</ta>
            <ta e="T278" id="Seg_4824" s="T274">кʼай кʼат зеркалаля томпыт? /</ta>
            <ta e="T290" id="Seg_4825" s="T278">“курасса е′нʼант, аша антак, / сарыт неля кʼотəл мундокт. / курасса ай сəры е′нʼа.” /</ta>
            <ta e="T302" id="Seg_4826" s="T290">сарыт има алт[п]а пакта. / ай кюратолна утылянтыс. / зеркалаляп пəк′щатынʼыт. / лякчинлянтыс чельчолнаны! /</ta>
            <ta e="T306" id="Seg_4827" s="T302">“Ах, тат нильчиль стеклонʼонты! /</ta>
            <ta e="T311" id="Seg_4828" s="T306">тат на молмытанты мат чёт. /</ta>
            <ta e="T315" id="Seg_4829" s="T311">тəп кутар мо[а]ннымянта матса? /</ta>
            <ta e="T319" id="Seg_4830" s="T315">мат тəпыт ′момыт лямкал тантап. /</ta>
            <ta e="T322" id="Seg_4831" s="T319">кʼонтырнолыт, орымна кʼай! /</ta>
            <ta e="T333" id="Seg_4832" s="T322">кутар аша сəры е′нʼа: / əмыт перкыль тʼири о′мтса / кекыс сыронт манталпыса. /</ta>
            <ta e="T343" id="Seg_4833" s="T333">кыса кэтат: кʼянтык ман нон / ныны пӯт тəп курасса е′нта? /</ta>
            <ta e="T348" id="Seg_4834" s="T343">инна кəтʼат: мат кес нʼянчак. /</ta>
            <ta e="T355" id="Seg_4835" s="T348">омтыль кʼомыт коляптʼаты / кʼос тəты мундык.</ta>
            <ta e="T359" id="Seg_4836" s="T355">матса чʼенга / нʼянчитыль мы </ta>
            <ta e="T361" id="Seg_4837" s="T359">ниlчик кʼоты? /</ta>
            <ta e="T371" id="Seg_4838" s="T361">зеркалаля кəтынʼыты: / “сарыт неля, кʼотол, пийя, / курас[с]а ай сəры е′нʼа.” /</ta>
            <ta e="T373" id="Seg_4839" s="T371">кʼутар метал.</ta>
            <ta e="T378" id="Seg_4840" s="T373">путымт амла / зеркалаляп конна чаттыт. /</ta>
            <ta e="T402" id="Seg_4841" s="T378">чернавкамты кʼəрынʼыты, / нильчик тəпты кʼуралтынʼыт, / (онды сенныл[п]ь нетак тына): / сарыт неляп мачыт путонт / кʼэнтырталтыл курынʼымтый / илыла ай кʼəчинʼымтый / нэччетынʼыт чён илнʼыт / чумпынеэт амнʼын титкʼо. / </ta>
            <ta e="T407" id="Seg_4842" s="T402">лосы монты антытʼанта. / неняль имас.</ta>
            <ta e="T413" id="Seg_4843" s="T407">сарыт неляс / черновка кʼəнта мачонтына. /</ta>
            <ta e="T421" id="Seg_4844" s="T413">нащак кунтак тына кʼəнтой / сарыт неля нына кəнта. /</ta>
            <ta e="T425" id="Seg_4845" s="T421">кʼукынтыщак ныркымонна, / омталтана: “илыптамы! /</ta>
            <ta e="T429" id="Seg_4846" s="T425">кʼайкыт, кэтʼат у′роп е′нʼа. /</ta>
            <ta e="T433" id="Seg_4847" s="T429">нелы куп ыкысип тальчолʼащик! /</ta>
            <ta e="T441" id="Seg_4848" s="T433">кутар кумып кʼонтап тащинт, / кʼайкʼо кыкант – наткʼо метак.” /</ta>
            <ta e="T457" id="Seg_4849" s="T441">тоннам путса тəпты кыкал / аша курасыт, аша кʼəссыт, / у′тысыты, нильчик кəтыл: / “кʼэнʼаш нопсинты на мəрет.” /</ta>
            <ta e="T461" id="Seg_4850" s="T457">ныны онды мокʼын тʼусса. /</ta>
            <ta e="T462" id="Seg_4851" s="T461">“кʼайы?” </ta>
            <ta e="T465" id="Seg_4852" s="T462">– сарыт има кəтынʼыты, /</ta>
            <ta e="T469" id="Seg_4853" s="T465">– кʼурассымыль нелькуп кун е′нʼ.” /</ta>
            <ta e="T475" id="Seg_4854" s="T469">“пелыколык мачот нынка, – / тə[о]пты [тəппам] кəтынʼыты тəпын. – /</ta>
            <ta e="T479" id="Seg_4855" s="T475">сорымпа кʼомык сынʼтʼенетты. /</ta>
            <ta e="T491" id="Seg_4856" s="T479">сурыт кʼатонт ольчячеммя, / кʼонак тəпна кəкытʼаныт, / кʼурмот айна сепы е′ннынт.” /</ta>
            <ta e="T498" id="Seg_4857" s="T491">əты кəнна ныны нильчиль: / сарыт неля у′рынтана. /</ta>
            <ta e="T501" id="Seg_4858" s="T498">пащирна сар нелянтыкʼо. /</ta>
            <ta e="T518" id="Seg_4859" s="T501">король ия Елисей / номты омтытыла пуле / токʼталта ай и[п]елʼа кəнна / курассымат сʼич[ш]инт олып / титап ий[п]са имты-немты. /</ta>
            <ta e="T531" id="Seg_4860" s="T518">а иты-неты карыт тəтты / мачит путот уркымпыла / ′куры[тʼ]тӓнты ситы котят / мотын инды тульчеипта [тульче′импа]. /</ta>
            <ta e="T539" id="Seg_4861" s="T531">чəссэ лека тəпын му[н]тыл / лейетолта. лямкалтента, / лʼучимпыла ондалбыла. /</ta>
            <ta e="T543" id="Seg_4862" s="T539">воротамыт тəп на сера. / </ta>
            <ta e="T546" id="Seg_4863" s="T543">етымантот лямык е′нʼа. /</ta>
            <ta e="T549" id="Seg_4864" s="T546">лека о′нталпылʼа нʼёнʼыт. /</ta>
            <ta e="T559" id="Seg_4865" s="T549">а сарыт неля ′курытʼелʼа [куры ′тӓлʼя] / крыльцот инды сыкʼылента. / пюрый утокʼынты интыт. /</ta>
            <ta e="T567" id="Seg_4866" s="T559">мотат чондыкана нʼунтыт, / мо[о̄]т[т]ы чондыкана сера. /</ta>
            <ta e="T579" id="Seg_4867" s="T567">кʼонемпа [kоннымпа, ′kоннӓнта] коверса лавкат, / иконат илкʼыд дубыль лем, / нəкырпыль иппыпсаса шокʼыр. /</ta>
            <ta e="T586" id="Seg_4868" s="T579">нель куп маннымпаты: тымты / кʼоты сома кумыт илʼот. /</ta>
            <ta e="T589" id="Seg_4869" s="T586">тэпын аша тотʼелʼантʼот. /</ta>
            <ta e="T595" id="Seg_4870" s="T589">нот ситы котʼат ни куты аш ата. /</ta>
            <ta e="T613" id="Seg_4871" s="T595">мо̄тып ′туммыт колялтынтыт. / мундок сомак ′тӓшалтысты, / иконат сеччип ачалтынтыт, / шокʼырып пе[ӧ]тпыла четынтыты, / полатит инды сыкʼылелʼа / лямык илла ольчеинта. /</ta>
            <ta e="T617" id="Seg_4872" s="T613">челыт чоннонт тəтче еса. / </ta>
            <ta e="T627" id="Seg_4873" s="T617">сʼӱмы у′нтʼалында покʼыт: / сельчи мотырын на серот, / сельчит мундык унтыль укык. /</ta>
            <ta e="T641" id="Seg_4874" s="T627">мəркы нетыт нильчик еса: / “на ля kай есʼ кутар соме, / кутар мундык курасс е′нʼа. /</ta>
            <ta e="T649" id="Seg_4875" s="T641">куты кʼос монмыт тəщанʼпаты / миты мещимыт əта кʼотый [ӓтым′потын]. /</ta>
            <ta e="T650" id="Seg_4876" s="T649">кутонтʼ?</ta>
            <ta e="T656" id="Seg_4877" s="T650">тантʼаш атылтʼащик, / мещим тоныкʼотак чэтэш. /</ta>
            <ta e="T663" id="Seg_4878" s="T656">кʼатамол тат мэр′кʼы кʼумонт, / ильчаныткʼо мелты е′ннант. /</ta>
            <ta e="T670" id="Seg_4879" s="T663">кʼатамол тат ильматнʼонты, / мекин чопаныткʼо е′ннат. /</ta>
            <ta e="T675" id="Seg_4880" s="T670">кʼатамол тат имаkота – / кэрянтымыт эмыныткʼо. /</ta>
            <ta e="T682" id="Seg_4881" s="T675">кʼатамол тат нель кумонты – / мекин неньняныткʼо еннат.” /</ta>
            <ta e="T694" id="Seg_4882" s="T682">сарыт неля илла панча [илʼлʼымпа̄ны], / тороватынт тəпытыса, / чунтыт тӓтты мукшаитыла [нʼӱты соч]. /</ta>
            <ta e="T706" id="Seg_4883" s="T694">нʼер тарыс, ниль е′сыт: / моткʼынтʼит мол серсак мат тʼи, / кʼос сит кʼэрыптелыт ченʼа. /</ta>
            <ta e="T714" id="Seg_4884" s="T706">нына[ы] этомынты кəнтот[ыт]: / там аш сарыт неля еса. /</ta>
            <ta e="T725" id="Seg_4885" s="T714">сенʼелянты омтылтысот, / няниляса [нʼӓнилʼлʼӓса] мильчечесот, / рюмкай тирла кʼамтечесот / илит икʼыт татӓче̄соты. /</ta>
            <ta e="T742" id="Seg_4886" s="T725">′тапыль у′тып аша исты. / няниляп кес сепытела, / кыпэ липымт о′кʼынт исты, / вэтын ноны нʼышкʼынтокʼо / коптыткʼоно мотырана. /</ta>
            <ta e="T758" id="Seg_4887" s="T742">кʼэнтысотыт тэпыт нетʼап / инна челынʼпытыль сюннёт / ай ондыкʼыт на [нымты] кʼəчынт[с]отыт тəпып. / онг[д]ыт итыл пелыколык. / </ta>
            <ta e="T767" id="Seg_4888" s="T758">няркы челон [челʼи] морокʼынты / ильмат коралтынса: / челынʼпыта чеп[л]ымыт ме! /</ta>
            <ta e="T783" id="Seg_4889" s="T767">тат каранʼанты [куторнанты] / понты кунды нут некмы, / тоталтукал кəп[т] петпыль ′утыса / мудокт месим онант илкыт контырнанты. /</ta>
            <ta e="T787" id="Seg_4890" s="T783">монты əтоп то чатантал? /</ta>
            <ta e="T797" id="Seg_4891" s="T787">контырсал кʼай кунем тəтот / сарыт неляп ильматыль тат? / </ta>
            <ta e="T803" id="Seg_4892" s="T797">ирет, ирет, онак немы, / золоченыль оттыля! /</ta>
            <ta e="T811" id="Seg_4893" s="T803">мəшиканты корел пит тат, / пюрыль мэнтыль, челыль саиль. / </ta>
            <ta e="T819" id="Seg_4894" s="T811">ай мат серлы кыкыла / кʼишкʼят тянты маннымпотыт. / </ta>
            <ta e="T830" id="Seg_4895" s="T819">нечет чонты кюныль кыкят тоаб / енʼа пирkы тоанʼ / нымты коры мю. /</ta>
            <ta e="T834" id="Seg_4896" s="T830">питы маты тəты. /</ta>
         </annotation>
         <annotation name="stl" tierref="stl">
            <ta e="T7" id="Seg_4897" s="T0">sar iːman[m]tɨsa nɛtɨrsɨŋa [nɛtɨrsɨtɨ, nɛkärsɨ], / quntak vəttont toqtaltɨŋa. / </ta>
            <ta e="T14" id="Seg_4898" s="T7">oknot qantɨ imatɨ / tɛpɨp ɛtɨqo ontɨ omta. /</ta>
            <ta e="T24" id="Seg_4899" s="T14">qarɨn nonɨ üt[t]ɨt tɛtɨ / ɛtɨqɨtɨ, ɛtɨqɨtɨ, / meltɨ limont mannɨmpa tɛp. / </ta>
            <ta e="T35" id="Seg_4900" s="T24">sailʼ toktɨ mantalpɨlʼa / čušalɨsot kutɨlʼ emɨlʼa / qarɨlʼ kɛnnɨnt ukoqɨnɨ pittɛt. /</ta>
            <ta e="T39" id="Seg_4901" s="T35">omɨnetɨ aša ata! /</ta>
            <ta e="T53" id="Seg_4902" s="T39">kekɨse təp mannɨmpatɨ: / merkɨlʼ palʼča kюčinimpa, / sɨrɨ čomna limɨt indɨ, / təttɨ mundɨk sɛrɨ esa. /</ta>
            <ta e="T59" id="Seg_4903" s="T53">qənna [qänna] ok[k]ɨrče kёt iːret. / </ta>
            <ta e="T65" id="Seg_4904" s="T59">limoqɨn saimt[ɨ] alp[p]a aš[š]a itɨt [iːtɨŋɨt]. /</ta>
            <ta e="T74" id="Seg_4905" s="T65">nʼot sočelʼnikaqɨt, pit, / təpɨn minɨt nelяm[n] [nelʼamtɨ] nom [nomtɨ]. /</ta>
            <ta e="T88" id="Seg_4906" s="T74">remkɨ kʼarɨt notnɨlʼ gostʼ, / pisa čel[ɨ]sa kuntɨ ətɨpɨlʼ, / kʼuntakʼɨnɨ nʼotɨ kesɨs / motkʼɨn čusa sar-əsɨtɨ. /</ta>
            <ta e="T100" id="Seg_4907" s="T88">mantɛčekus təpɨtkinɨ, / sɛttɨmela kɛškʼɨlʼesa, / onnɨmt aša sʼepɨrelsɨt [sepɨrsɨ] / ae čelɨ čontot urečesa. /</ta>
            <ta e="T104" id="Seg_4908" s="T100">kekkalpɨsa [nakkalpɨs] kundɨ sar, / </ta>
            <ta e="T106" id="Seg_4909" s="T104">kutar ekʼo?</ta>
            <ta e="T108" id="Seg_4910" s="T106">grešnɨk ɛpa [ɛppa]. /</ta>
            <ta e="T113" id="Seg_4911" s="T108">potɨ kɛnna [qännɨ], mit[t]ɨ onʼkɨ. / </ta>
            <ta e="T118" id="Seg_4912" s="T113">mənɨlʼ [vänɨlʼ] imap ɨnʼɨt sar. /</ta>
            <ta e="T134" id="Seg_4913" s="T118">kutar kɛtɨqo – nelʼ qup esa / onɨlʼ mitɨ sarɨlʼ ima: / pirkɨ tašik. sərɨ esa, / tɛnɨltoksa, mu[n]dokt isɨt. /</ta>
            <ta e="T142" id="Seg_4914" s="T134">nɨnaj nʼotɨ somalʼčɨmčat, / kʼotol nʼotɨ яrɨk esa. /</ta>
            <ta e="T148" id="Seg_4915" s="T142">motɨnonɨ tatɨmpɨstɨ / kekɨs zerkalalяm okɨr. /</ta>
            <ta e="T153" id="Seg_4916" s="T148">zerkalalя nilʼčilʼ esa: / konʼɨmpɨtko čenɨmɨmpa. /</ta>
            <ta e="T168" id="Seg_4917" s="T153">təpsa okɨr təpkяn ekka / soma putɨk, ontalpɨka, / təpsa šokʼkʼak karampɨka / somalʼčimpɨl konʼɨmpɨka: / “čelom, zerkalalяm! </ta>
            <ta e="T182" id="Seg_4918" s="T168">kɛčat / na[pa], čёlɨk inna pintɨ: / matkʼaj tətɨn ikʼɨt mundokt / soma kʼuras, sɛrɨ enʼak?” /</ta>
            <ta e="T198" id="Seg_4919" s="T182">zerkalalя kətɨkɨtɨ: / “tat sarɨt ima, aša antak, / tat kes okɨr mundokt soma / kurasa aj sɛrɨ enʼant.” /</ta>
            <ta e="T212" id="Seg_4920" s="T198">sarɨt ima lakkɨmonna, / kʼəktɨ porɨm ipkʼalnɨtɨ. / saintɨsa rɨpkalčenʼɨt. / munɨntɨsa kяšilčenɨt, / utɨp kʼenʼɨnt kompɨlɨmpa, / zerkalalяnt manɨmpɨla. /</ta>
            <ta e="T232" id="Seg_4921" s="T212">sarɨt nelя – čilяlʼ netɛk / lяmɨk, čondɨk orɨmnanda. / nat sitɨ kočяt orɨmnanda, / məčišinda, orɨmtana, / sərɨ məčik, sʼя sain undɨk, / selmo kolɨk, totɨk putɨk. /</ta>
            <ta e="T238" id="Seg_4922" s="T232">čitalʼ kʼumtɨ pekʼɨlʼisa, / korolʼt iя Еlisej. /</ta>
            <ta e="T255" id="Seg_4923" s="T238">čusa kʼup – sar ətɨp misɨt, / motqɨn ipsa mɨta enʼa: / selʼči təmtɨčili kʼɛtɨt / ae ton aj česarɨli motɨet. /</ta>
            <ta e="T274" id="Seg_4924" s="T255">nʼetatɨtkinɨ takʼɨliko, / sarɨt ima toktatkutɨp[l] / zerkalalяntɨsa ondɨ, / ətɨk tɛpɨn čattɨkustɨ: / mat kʼaj, kətat mяkkя, mundok / kurassa aj sərɨ enʼak? /</ta>
            <ta e="T278" id="Seg_4925" s="T274">kʼaj kʼat zerkalalя tompɨt? /</ta>
            <ta e="T290" id="Seg_4926" s="T278">“kurassa enʼant, aša antak, / sarɨt nelя kʼotəl mundokt. / kurassa aj sərɨ enʼa.” /</ta>
            <ta e="T302" id="Seg_4927" s="T290">sarɨt ima alt[p]a pakta. / aj kюratolna utɨlяntɨs. / zerkalalяp pəkšatɨnʼɨt. / lяkčinlяntɨs čelʼčolnanɨ! /</ta>
            <ta e="T306" id="Seg_4928" s="T302">“Аh, tat nilʼčilʼ steklonʼontɨ! /</ta>
            <ta e="T311" id="Seg_4929" s="T306">tat na molmɨtantɨ mat čёt. /</ta>
            <ta e="T315" id="Seg_4930" s="T311">təp kutar mo[a]nnɨmяnta matsa? /</ta>
            <ta e="T319" id="Seg_4931" s="T315">mat təpɨt momɨt lяmkal tantap. /</ta>
            <ta e="T322" id="Seg_4932" s="T319">kʼontɨrnolɨt, orɨmna kʼaj! /</ta>
            <ta e="T333" id="Seg_4933" s="T322">kutar aša sərɨ enʼa: / əmɨt perkɨlʼ čiri omtsa / kekɨs sɨront mantalpɨsa. /</ta>
            <ta e="T343" id="Seg_4934" s="T333">kɨsa kɛtat: kʼяntɨk man non / nɨnɨ puːt təp kurassa enta? /</ta>
            <ta e="T348" id="Seg_4935" s="T343">inna kəčat: mat kes nʼяnčak. /</ta>
            <ta e="T355" id="Seg_4936" s="T348">omtɨlʼ kʼomɨt kolяpčatɨ / kʼos tətɨ mundɨk.</ta>
            <ta e="T359" id="Seg_4937" s="T355">matsa čʼenga / nʼяnčitɨlʼ mɨ </ta>
            <ta e="T361" id="Seg_4938" s="T359">nilʼčik kʼotɨ? /</ta>
            <ta e="T371" id="Seg_4939" s="T361">zerkalalя kətɨnʼɨtɨ: / “sarɨt nelя, kʼotol, pijя, / kuras[s]a aj sərɨ enʼa.” /</ta>
            <ta e="T373" id="Seg_4940" s="T371">kʼutar metal.</ta>
            <ta e="T378" id="Seg_4941" s="T373">putɨmt amla / zerkalalяp konna čattɨt. /</ta>
            <ta e="T402" id="Seg_4942" s="T378">černavkamtɨ kʼərɨnʼɨtɨ, / nilʼčik təptɨ kʼuraltɨnʼɨt, / (ondɨ sennɨl[p]ʼ netak tɨna): / sarɨt nelяp mačɨt putont / kʼɛntɨrtaltɨl kurɨnʼɨmtɨj / ilɨla aj kʼəčinʼɨmtɨj / nɛččetɨnʼɨt čёn ilnʼɨt / čumpɨneɛt amnʼɨn titkʼo / </ta>
            <ta e="T407" id="Seg_4943" s="T402">losɨ montɨ antɨčanta. / nenяlʼ imas.</ta>
            <ta e="T413" id="Seg_4944" s="T407">sarɨt nelяs / černovka kʼənta mačontɨna. /</ta>
            <ta e="T421" id="Seg_4945" s="T413">našak kuntak tɨna kʼəntoj / sarɨt nelя nɨna kənta. /</ta>
            <ta e="T425" id="Seg_4946" s="T421">kʼukɨntɨšak nɨrkɨmonna, / omtaltana: “ilɨptamɨ! /</ta>
            <ta e="T429" id="Seg_4947" s="T425">kʼajkɨt, kɛčat urop enʼa. /</ta>
            <ta e="T433" id="Seg_4948" s="T429">nelɨ kup ɨkɨsip talʼčolʼašik! /</ta>
            <ta e="T441" id="Seg_4949" s="T433">kutar kumɨp kʼontap tašint, / kʼajkʼo kɨkant – natkʼo metak.” /</ta>
            <ta e="T457" id="Seg_4950" s="T441">tonnam putsa təptɨ kɨkal / aša kurasɨt, aša kʼəssɨt, / utɨsɨtɨ, nilʼčik kətɨl: / “kʼɛnʼaš nopsintɨ na məret.” /</ta>
            <ta e="T461" id="Seg_4951" s="T457">nɨnɨ ondɨ mokʼɨn čussa. /</ta>
            <ta e="T462" id="Seg_4952" s="T461">“kʼajɨ?” </ta>
            <ta e="T465" id="Seg_4953" s="T462">– sarɨt ima kətɨnʼɨtɨ, /</ta>
            <ta e="T469" id="Seg_4954" s="T465">– kʼurassɨmɨlʼ nelʼkup kun enʼ.” /</ta>
            <ta e="T475" id="Seg_4955" s="T469">“pelɨkolɨk mačot nɨnka, – / tə[o]ptɨ [təppam] kətɨnʼɨtɨ təpɨn. – /</ta>
            <ta e="T479" id="Seg_4956" s="T475">sorɨmpa kʼomɨk sɨnʼčenettɨ. /</ta>
            <ta e="T491" id="Seg_4957" s="T479">surɨt kʼatont olʼčяčemmя, / kʼonak təpna kəkɨčanɨt, / kʼurmot ajna sepɨ ennɨnt.” /</ta>
            <ta e="T498" id="Seg_4958" s="T491">ətɨ kənna nɨnɨ nilʼčilʼ: / sarɨt nelя urɨntana. /</ta>
            <ta e="T501" id="Seg_4959" s="T498">paširna sar nelяntɨkʼo. /</ta>
            <ta e="T518" id="Seg_4960" s="T501">korolʼ iя Еlisej / nomtɨ omtɨtɨla pule / tokʼtalta aj i[p]elʼa kənna / kurassɨmat sʼič[š]int olɨp / titap ij[p]sa imtɨ-nemtɨ. /</ta>
            <ta e="T531" id="Seg_4961" s="T518">a itɨ-netɨ karɨt təttɨ / mačit putot urkɨmpɨla / kurɨ[č]täntɨ sitɨ kotяt / motɨn indɨ tulʼčeipta [tulʼčeimpa]. /</ta>
            <ta e="T539" id="Seg_4962" s="T531">čəssɛ leka təpɨn mu[n]tɨl / lejetolta. lяmkaltenta, / lʼučimpɨla ondalpɨla. /</ta>
            <ta e="T543" id="Seg_4963" s="T539">vorotamɨt təp na sera. / </ta>
            <ta e="T546" id="Seg_4964" s="T543">etɨmantot lяmɨk enʼa. /</ta>
            <ta e="T549" id="Seg_4965" s="T546">leka ontalpɨlʼa nʼёnʼɨt. /</ta>
            <ta e="T559" id="Seg_4966" s="T549">a sarɨt nelя kurɨčelʼa [kurɨ tälʼя] / krɨlʼcot indɨ sɨkʼɨlenta. / pюrɨj utokʼɨntɨ intɨt. /</ta>
            <ta e="T567" id="Seg_4967" s="T559">motat čondɨkana nʼuntɨt, / mo[oː]t[t]ɨ čondɨkana sera. /</ta>
            <ta e="T579" id="Seg_4968" s="T567">kʼonempa [qonnɨmpa, qonnänta] koversa lavkat, / ikonat ilkʼɨd dupɨlʼ lem, / nəkɨrpɨlʼ ippɨpsasa šokʼɨr. /</ta>
            <ta e="T586" id="Seg_4969" s="T579">nelʼ kup mannɨmpatɨ: tɨmtɨ / kʼotɨ soma kumɨt ilʼot. /</ta>
            <ta e="T589" id="Seg_4970" s="T586">tɛpɨn aša točelʼančot. /</ta>
            <ta e="T595" id="Seg_4971" s="T589">not sitɨ kočat ni kutɨ aš ata. /</ta>
            <ta e="T613" id="Seg_4972" s="T595">moːtɨp tummɨt kolяltɨntɨt. / mundok somak täšaltɨstɨ, / ikonat seččip ačaltɨntɨt, / šokʼɨrɨp pe[ö]tpɨla četɨntɨtɨ, / polatit indɨ sɨkʼɨlelʼa / lяmɨk illa olʼčeinta. /</ta>
            <ta e="T617" id="Seg_4973" s="T613">čelɨt čonnont tətče esa. / </ta>
            <ta e="T627" id="Seg_4974" s="T617">sʼümɨ unčalɨnda pokʼɨt: / selʼči motɨrɨn na serot, / selʼčit mundɨk untɨlʼ ukɨk. /</ta>
            <ta e="T641" id="Seg_4975" s="T627">mərkɨ netɨt nilʼčik esa: / “na lя qaj esʼ kutar some, / kutar mundɨk kurass enʼa. /</ta>
            <ta e="T649" id="Seg_4976" s="T641">kutɨ kʼos monmɨt təšanʼpatɨ / mitɨ mešimɨt əta kʼotɨj ätɨmpotɨn. /</ta>
            <ta e="T650" id="Seg_4977" s="T649">kutonč?</ta>
            <ta e="T656" id="Seg_4978" s="T650">tančaš atɨlčašik, / mešim tonɨkʼotak čɛtɛš. /</ta>
            <ta e="T663" id="Seg_4979" s="T656">kʼatamol tat mɛrkʼɨ kʼumont, / ilʼčanɨtkʼo meltɨ ennant. /</ta>
            <ta e="T670" id="Seg_4980" s="T663">kʼatamol tat ilʼmatnʼontɨ, / mekin čopanɨtkʼo ennat. /</ta>
            <ta e="T675" id="Seg_4981" s="T670">kʼatamol tat imaqota – / kɛrяntɨmɨt ɛmɨnɨtkʼo. /</ta>
            <ta e="T682" id="Seg_4982" s="T675">kʼatamol tat nelʼ kumontɨ – / mekin nenʼnяnɨtkʼo ennat.” /</ta>
            <ta e="T694" id="Seg_4983" s="T682">sarɨt nelя illa panča [ilʼlʼɨmpaːnɨ], / torovatɨnt təpɨtɨsa, / čuntɨt tättɨ mukšaitɨla [nʼütɨ soč]. /</ta>
            <ta e="T706" id="Seg_4984" s="T694">nʼer tarɨs, nilʼ esɨt: / motkʼɨnčit mol sersak mat či, / kʼos sit kʼɛrɨptelɨt čenʼa. /</ta>
            <ta e="T714" id="Seg_4985" s="T706">nɨna[ɨ] ɛtomɨntɨ kəntot[ɨt]: / tam aš sarɨt nelя esa. /</ta>
            <ta e="T725" id="Seg_4986" s="T714">senʼelяntɨ omtɨltɨsot, / nяnilяsa [nʼänilʼlʼäsa] milʼčečesot, / rюmkaj tirla kʼamtečesot / ilit ikʼɨt tatäčeːsotɨ. /</ta>
            <ta e="T742" id="Seg_4987" s="T725">tapɨlʼ utɨp aša istɨ. / nяnilяp kes sepɨtela, / kɨpɛ lipɨmt okʼɨnt istɨ, / vɛtɨn nonɨ nʼɨškʼɨntokʼo / koptɨtkʼono motɨrana. /</ta>
            <ta e="T758" id="Seg_4988" s="T742">kʼɛntɨsotɨt tɛpɨt nečap / inna čelɨnʼpɨtɨlʼ sюnnёt / aj ondɨkʼɨt na [nɨmtɨ] kʼəčɨnt[s]otɨt təpɨp. / ong[d]ɨt itɨl pelɨkolɨk. / </ta>
            <ta e="T767" id="Seg_4989" s="T758">nяrkɨ čelon [čelʼi] morokʼɨntɨ / ilʼmat koraltɨnsa: / čelɨnʼpɨta čep[l]ɨmɨt me! /</ta>
            <ta e="T783" id="Seg_4990" s="T767">tat karanʼantɨ [kutornantɨ] / pontɨ kundɨ nut nekmɨ, / totaltukal kəp[t] petpɨlʼ utɨsa / mudokt mesim onant ilkɨt kontɨrnantɨ. /</ta>
            <ta e="T787" id="Seg_4991" s="T783">montɨ ətop to čatantal? /</ta>
            <ta e="T797" id="Seg_4992" s="T787">kontɨrsal kʼaj kunem tətot / sarɨt nelяp ilʼmatɨlʼ tat? / </ta>
            <ta e="T803" id="Seg_4993" s="T797">iret, iret, onak nemɨ, / zoločenɨlʼ ottɨlя! /</ta>
            <ta e="T811" id="Seg_4994" s="T803">məšikantɨ korel pit tat, / pюrɨlʼ mɛntɨlʼ, čelɨlʼ sailʼ. / </ta>
            <ta e="T819" id="Seg_4995" s="T811">aj mat serlɨ kɨkɨla / kʼiškʼяt tяntɨ mannɨmpotɨt. / </ta>
            <ta e="T830" id="Seg_4996" s="T819">nečet čontɨ kюnɨlʼ kɨkяt toap / enʼa pirqɨ toanʼ / nɨmtɨ korɨ mю. /</ta>
            <ta e="T834" id="Seg_4997" s="T830">pitɨ matɨ tətɨ. /</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_4998" s="T0">Sar imantɨsa nɛtɨrsɨtɨ (/nɛkärsɨ)/, kuntak wəttont toqtaltɨŋa/. </ta>
            <ta e="T14" id="Seg_4999" s="T7">Oknot qantɨ imatɨ/ tɛpɨp ɛtɨqo ontɨ omta/. </ta>
            <ta e="T24" id="Seg_5000" s="T14">Qarɨn nɔːnɨ üːtɨt tɛtɨ/ ɛtɨkkɨtɨ, ɛtɨkkɨtɨ/, meːltɨ limont mannɨmpa tɛp/. </ta>
            <ta e="T35" id="Seg_5001" s="T24">Sailʼ tɔːktɨ mantalpɨlʼa/ čʼüšalɨsɔːt, kutɨlʼ ɛːmɨlʼa/ qarɨlʼ kɛnnɨnt uːkoːqɨnɨ pit tɛt/. </ta>
            <ta e="T39" id="Seg_5002" s="T35">Ɔːmɨ neːtɨ aša ata/! </ta>
            <ta e="T53" id="Seg_5003" s="T39">Kekɨsä təp mannɨmpatɨ/: mɛrkɨlʼ palʼčʼa küčʼinimpa/, sɨrɨ čʼomna limɨt iːntɨ/, təttɨ muntɨk sɛrɨ ɛsa/. </ta>
            <ta e="T59" id="Seg_5004" s="T53">Qənna (/qänna) okkɨr čʼäː köt irät. </ta>
            <ta e="T65" id="Seg_5005" s="T59">/Limoːqɨn saimtɨ alpa ašša iːtɨt (/iːtɨŋɨt)/. </ta>
            <ta e="T74" id="Seg_5006" s="T65">Nʼɔːt sočʼelʼnikaqɨt, pit/, təpɨn minɨt nälʼam (/nälʼamtɨ) nom (/nomtɨ)/. </ta>
            <ta e="T88" id="Seg_5007" s="T74">Rɛmkɨ qarɨt nɔːtnɨlʼ gostʼ/, pisa čʼeːlɨsa kuntɨ ətɨpɨlʼ/, kuntaqɨnɨ nʼɔːtɨ qessɨs,/ mɔːtqɨn tüsa sar-əsɨtɨ/. </ta>
            <ta e="T100" id="Seg_5008" s="T88">Mantɛčʼekkus təpɨtkinɨ/, sɛttɨmela kɛšqɨlʼesa/, ɔːnnɨmt aša sʼeːpɨrelsɨt (/seːpɨrsɨ)/ aje čʼeːlɨ čʼontot urrɛːčʼesa/. </ta>
            <ta e="T104" id="Seg_5009" s="T100">Kekkalpɨsa (/nakkalpɨs) kuntɨ sar. </ta>
            <ta e="T106" id="Seg_5010" s="T104">/Kutar ɛːqo? </ta>
            <ta e="T108" id="Seg_5011" s="T106">Grešnɨk ɛːppa/. </ta>
            <ta e="T113" id="Seg_5012" s="T108">Poːtɨ qɛnna (/qännɨ), mitɨ ɔːnʼkɨ/. </ta>
            <ta e="T118" id="Seg_5013" s="T113">Mənɨlʼ (/wänɨlʼ) imap ɨːnʼɨt sar/. </ta>
            <ta e="T134" id="Seg_5014" s="T118">Kutar kɛtɨqo– nälʼqup ɛːsa/ onɨlʼ mitɨ sarɨlʼ ima/: pirqɨ tašik, sərɨ ɛːsa/, tɛnɨl tɔːksa, muntokt iːsɨt/. </ta>
            <ta e="T142" id="Seg_5015" s="T134">Nɨːn aj nʼɔːtɨ somalʼčʼɨmčʼat/, qɔːt-ol nʼɔːtɨ jarɨk ɛsa/. </ta>
            <ta e="T148" id="Seg_5016" s="T142">Mɔːttɨ nɔːnɨ taːtɨmpɨstɨ/ kekɨs zerkalalʼam okɨr/. </ta>
            <ta e="T153" id="Seg_5017" s="T148">Zerkalalʼa nılʼčʼilʼ ɛːsa/: konʼɨmpɨtqo tɛnɨmɨmpa/. </ta>
            <ta e="T168" id="Seg_5018" s="T153">Təpsa okɨr təp qan ɛːkka/ soma puːtɨk, ɔːntalpɨkka/, təpsa šoqqak karampɨkka/, somalʼčʼimpɨl konʼɨmpɨkka:/ Čʼeːlom, zerkalalʼam! </ta>
            <ta e="T182" id="Seg_5019" s="T168">Kɛtat/ napa, čʼeːlɨk ınna pintɨ:/ mat qaj tətɨn iːqɨt muntokt/ soma kuras, sɛrɨ ɛːnʼak/? </ta>
            <ta e="T198" id="Seg_5020" s="T182">Zerkalalʼa kətɨkkɨtɨ/: Tat, sarɨt ima, aša antak/, tat kes okɨr muntokt soma/ kurasa aj sɛrɨ ɛːnʼant/. </ta>
            <ta e="T212" id="Seg_5021" s="T198">Sarɨt ima laqqɨmɔːnna/, qəqtɨ_pɔːrɨm ipqalnɨtɨ/, saintɨsa rɨpkalʼčʼenʼɨt/, munɨntɨsa kašilʼčʼenɨt/, utɨp qenʼɨnt kompɨlɨmpa/, zerkalalʼant mannɨmpɨla/. </ta>
            <ta e="T232" id="Seg_5022" s="T212">Sarɨt nälʼa– čʼilʼalʼ nätäk/ lʼamɨk, čʼontɨk orɨmnanta/, nat sitɨ kočʼat orɨmnanta/, məčʼišinta, orɨmtana/, sərɨ məčʼik, sʼaː sain untɨk/, selmokɔːlɨk, tɔːtɨk puːtɨk/. </ta>
            <ta e="T238" id="Seg_5023" s="T232">Titalʼ qumtɨ peːqɨlʼisa/, korolʼt ija Elisej/. </ta>
            <ta e="T255" id="Seg_5024" s="T238">Tüsa qup– sar əːtɨp misɨt/, mɔːtqɨn ipsa mɨta ɛːnʼa/: seːlʼčʼi təmtɨtili qɛːtɨt/ aje toːn aj tɛːsarɨli mɔːtɨjät/. </ta>
            <ta e="T274" id="Seg_5025" s="T255">Nätatɨtkinɨ taqqɨliqo/, sarɨt ima toqtatkutɨl/ zerkalalʼantɨsa ontɨ/, əːtɨk tɛpɨn čʼattɨkkustɨ/: Mat qaj, kətat mäkkä, muntok/ kurassa aj sərɨ ɛːnʼak/? </ta>
            <ta e="T278" id="Seg_5026" s="T274">Qaj kat zerkalalʼa tompɨt/? </ta>
            <ta e="T290" id="Seg_5027" s="T278">Kurassa ɛːnʼant, aša antak/, sarɨt nälʼa qotəl muntokt/, kurassa aj sərɨ ɛːnʼa/. </ta>
            <ta e="T302" id="Seg_5028" s="T290">Sarɨt ima alpa pakta/ aj küratɔːlna utɨlʼantɨs/, zerkalalʼap pəqšatɨnʼɨt/, lʼakčʼinlʼantɨs čʼelʼčʼɔːlna nɨː/! </ta>
            <ta e="T306" id="Seg_5029" s="T302">Аh, tat nılʼčʼilʼ steklonʼontɨ/! </ta>
            <ta e="T311" id="Seg_5030" s="T306">Tat na moːlmɨtantɨ mat čʼɔːt/. </ta>
            <ta e="T315" id="Seg_5031" s="T311">Təp kutar mannɨmmänta matsa/? </ta>
            <ta e="T319" id="Seg_5032" s="T315">Mat təpɨt moːmɨt lʼamqaltantap/. </ta>
            <ta e="T322" id="Seg_5033" s="T319">Qontɨrnɔːlɨt, orɨmna qaj/! </ta>
            <ta e="T333" id="Seg_5034" s="T322">Kutar aša sərɨ ɛːnʼa/: əmɨt pɛrqɨlʼ tıːri ɔːmtsa,/ kekɨs sɨront mantalpɨsa/. </ta>
            <ta e="T343" id="Seg_5035" s="T333">Kɨsa kɛtat: qantɨk man nɔːn/ nɨːnɨ puːt təp kurassa ɛːnta/? </ta>
            <ta e="T348" id="Seg_5036" s="T343">İnna kətat: mat kes nʼančʼak/. </ta>
            <ta e="T355" id="Seg_5037" s="T348">Ɔːmtɨlʼ qoː mɨt kolʼaltatɨ,/ qos tətɨ muntɨk. </ta>
            <ta e="T359" id="Seg_5038" s="T355">Matsa čʼäːŋka/ nʼančʼitɨlʼ mɨ. </ta>
            <ta e="T361" id="Seg_5039" s="T359">Nılʼčʼik qɔːtɨ/? </ta>
            <ta e="T371" id="Seg_5040" s="T361">Zerkalalʼa kətɨnʼɨtɨ/: sarɨt nälʼa qotol, pija,/ kurassa aj sərɨ ɛːnʼa/. </ta>
            <ta e="T373" id="Seg_5041" s="T371">Kutar meːtal. </ta>
            <ta e="T378" id="Seg_5042" s="T373">Puːtɨmt amla/ zerkalalʼap konna čʼattɨt/. </ta>
            <ta e="T402" id="Seg_5043" s="T378">Čʼernavkamtɨ qərɨnʼɨtɨ/, nılʼčʼik təptɨ quraltɨnʼɨt/, (ontɨ sennɨlʼ nätak tɨna)/: Sarɨt nälʼap mačʼɨt puːtont/ qɛntɨrtaltɨl kurɨnʼɨmpɨj/ ilɨla aj qəːčʼinʼɨmpɨj/ näčʼčʼäːtɨnʼɨt čʼöːn ılnʼɨt/ čʼumpɨ neːet amnʼɨntitqo/. </ta>
            <ta e="T407" id="Seg_5044" s="T402">Loːsɨ montɨ antɨtanta/ nʼenʼnʼalʼ imas? </ta>
            <ta e="T413" id="Seg_5045" s="T407">Sarɨt nälʼas/ Čʼernovka qənta mačʼontɨ na/. </ta>
            <ta e="T421" id="Seg_5046" s="T413">Našak kuntak tɨna qəntoj/ sarɨt nälʼa nɨːna qənta/. </ta>
            <ta e="T425" id="Seg_5047" s="T421">Qukɨntɨšak nɨrkɨmɔːnna/, omtaltana: Ilɨptamɨ/! </ta>
            <ta e="T429" id="Seg_5048" s="T425">Qajqɨt, kɛtat, urop ɛːnʼa/? </ta>
            <ta e="T433" id="Seg_5049" s="T429">Nälɨqup, ɨkɨ sip talʼčʼolʼašik/! </ta>
            <ta e="T441" id="Seg_5050" s="T433">Kutar qumɨp qontap tašınt/, qajqo kɨkant– natqo meːtak/. </ta>
            <ta e="T457" id="Seg_5051" s="T441">Toːnnam puːtsa təptɨ kɨkal/ aša kurasɨt, aša qəssɨt/, uːtɨsɨtɨ, nılʼčʼik kətɨl/: Qɛnʼaš, nop sıntɨ na məret/. </ta>
            <ta e="T461" id="Seg_5052" s="T457">Nɨːnɨ ontɨ moqɨn tüssa/. </ta>
            <ta e="T462" id="Seg_5053" s="T461">Qajɨ? </ta>
            <ta e="T465" id="Seg_5054" s="T462">–Sarɨt ima kətɨnʼɨtɨ/. </ta>
            <ta e="T469" id="Seg_5055" s="T465">–Kurassɨmɨlʼ nälʼqup kun ɛːnʼ/? </ta>
            <ta e="T475" id="Seg_5056" s="T469">Pɛlɨkɔːlɨk mačʼot nɨŋa/,– təptɨ kətɨnʼɨtɨ təpɨn/. </ta>
            <ta e="T479" id="Seg_5057" s="T475">–Sɔːrɨmpa qɔːmɨk sɨnʼte neːttɨ/. </ta>
            <ta e="T491" id="Seg_5058" s="T479">Suːrɨt qatont olʼčʼačʼemmä/, qɔːnak təp na kəkkɨtanɨt/, qurmot aj na seːpɨ ɛːnnɨnt/. </ta>
            <ta e="T498" id="Seg_5059" s="T491">Əːtɨ qənna nɨːnɨ nılʼčʼilʼ/: sarɨt nälʼa urɨntana/! </ta>
            <ta e="T501" id="Seg_5060" s="T498">Paširna sar nälʼantɨqo/. </ta>
            <ta e="T518" id="Seg_5061" s="T501">Korolʼ ija Elisej/ nomtɨ omtɨtɨla puːlä/ toqtalta aj peːlʼa qənna/ kurassɨmat sıːčʼint olɨp/ tıːtap iːpsa imtɨ neːmtɨ/. </ta>
            <ta e="T531" id="Seg_5062" s="T518">A itɨ neːtɨ qarɨt təttɨ/ mačʼit puːtot urkɨmpɨla/ kurɨtäntɨ sitɨ kotät,/ mɔːtɨn iːntɨ tulʼčʼeıːmpa/. </ta>
            <ta e="T539" id="Seg_5063" s="T531">Čʼəssä läqa təpɨn muːttɨl/ läːjätɔːlta, lʼamkaltenta/, lüčʼimpɨla/ ɔːntalpɨla/. </ta>
            <ta e="T543" id="Seg_5064" s="T539">Vorotamɨt təp na seːra/. </ta>
            <ta e="T546" id="Seg_5065" s="T543">Etɨmantot lʼamɨk ɛːnʼa/. </ta>
            <ta e="T549" id="Seg_5066" s="T546">Läqa ɔːntalpɨlʼa nʼoːnʼɨt/. </ta>
            <ta e="T559" id="Seg_5067" s="T549">A sarɨt nälʼa kurɨtelʼa/ krɨlʼcot iːntɨ sɨːqɨlenta/, pürɨj utoːqɨntɨ iːntɨt/. </ta>
            <ta e="T567" id="Seg_5068" s="T559">Mɔːtat čʼontɨka na nʼüːntɨt/, mɔːttɨ čʼontɨka na seːra/. </ta>
            <ta e="T579" id="Seg_5069" s="T567">Qonempa (/qonnɨmpa, qonnänta) koversa lavkat/, ikonat ılqɨt dupɨlʼ lem/, nəkɨrpɨlʼ ippɨpsasa šoːqɨr/. </ta>
            <ta e="T586" id="Seg_5070" s="T579">Nälʼqup mannɨmpatɨ: tɨmtɨ/ qɔːtɨ soma qumɨt ilʼɔːt/. </ta>
            <ta e="T589" id="Seg_5071" s="T586">Tɛpɨn aša toːtelʼančʼɔːt/! </ta>
            <ta e="T595" id="Seg_5072" s="T589">Nɔːt sitɨ kotat ni kutɨ aš ata/. </ta>
            <ta e="T613" id="Seg_5073" s="T595">Mɔːtɨp tumɨt kolʼaltɨntɨt/, muntok somak täšaltɨstɨ/, ikonat sečʼčʼip ačʼaltɨntɨt/, šoːqɨrɨp pötpɨlla čʼetɨntɨtɨ/, polatit iːntɨ sɨːqɨlelʼa/ lʼamɨk ılla olʼčʼeıːnta/. </ta>
            <ta e="T617" id="Seg_5074" s="T613">Čʼeːlɨt čʼonnont tətčʼe ɛːsa/. </ta>
            <ta e="T627" id="Seg_5075" s="T617">Sümɨ untalɨnta poːqɨt/: seːlʼčʼi mɔːtɨrɨn na seːrɔːt/, seːlʼčʼit muntɨk untɨlʼ uːkɨk/. </ta>
            <ta e="T641" id="Seg_5076" s="T627">Mərqɨ neːtɨt nılʼčʼik ɛsa/: Na lʼa qaj ɛsʼ kutar some/, kutar muntɨk kurass ɛːnʼa/. </ta>
            <ta e="T649" id="Seg_5077" s="T641">Kutɨ qos mɔːnmɨt təšanʼpatɨ/ mitɨ meːšımɨt ətakkɔːtɨj (/ätɨmpɔːtɨn)/. </ta>
            <ta e="T650" id="Seg_5078" s="T649">Kutontʼ? </ta>
            <ta e="T656" id="Seg_5079" s="T650">Tantaš atɨltašik/, meːšım toːnɨ qotak čʼɛːtäš/. </ta>
            <ta e="T663" id="Seg_5080" s="T656">Qatamol tat mɛrqɨ qumont/, ilʼčʼanɨtqo meːltɨ ɛːnnant/. </ta>
            <ta e="T670" id="Seg_5081" s="T663">Qatamol tat ilʼmat nʼoːntɨ/, meːqin čʼopanɨtqo ɛːnnat/. </ta>
            <ta e="T675" id="Seg_5082" s="T670">Qatamol tat imaqota–/ qɛrʼantɨmɨt ämɨnɨtqo/. </ta>
            <ta e="T682" id="Seg_5083" s="T675">Qatamol tat nälʼ qumontɨ–/ meːqin nʼenʼnʼanɨtqo ɛːnnat/. </ta>
            <ta e="T694" id="Seg_5084" s="T682">Sarɨt nälʼa ılla pančʼa (/ılʼlʼɨmpanɨ)/, torowatɨnt təpɨtɨsa/, čʼüːntɨt tättɨ mušqaıːtɨla (/nʼuːtɨ sočʼ)/. </ta>
            <ta e="T706" id="Seg_5085" s="T694">Nʼertarɨs, nılʼ ɛsɨt/: Mɔːtqɨntit mol seːrsak mat tiː/, qos sıt qɛrɨptälɨt čʼäːnʼa/. </ta>
            <ta e="T714" id="Seg_5086" s="T706">Nɨːnɨ ɛːtomɨntɨ qəntɔːtɨt:/ tam aš sarɨt nälʼa ɛːsa/. </ta>
            <ta e="T725" id="Seg_5087" s="T714">Sänʼälʼantɨ omtɨltɨsɔːt/, nʼanʼilʼasa (/nʼänʼilʼäsa) milʼčʼečʼesɔːt/, rʼumkaj tıːrla qamtečʼesɔːt,/ ilit iːqɨt taːtäčʼesɔːtɨ/. </ta>
            <ta e="T742" id="Seg_5088" s="T725">Tapɨlʼ utɨp aša iːstɨ/, nʼanʼilʼap kes sepɨtela/, kɨpɛ lıːpɨmt ɔːkɨnt iːstɨ/, wɛtɨn nɔːnɨ nʼɨšqɨntoːqo/ koptɨtqo no mɔːtɨrana/. </ta>
            <ta e="T758" id="Seg_5089" s="T742">Qɛntɨsɔːtɨt tɛpɨt nätap/ ınna čʼeːlɨnʼpɨtɨlʼ sünnöt/ aj ontɨqɨt na (/nɨmtɨ) qəːčʼɨntɔːtɨt (/qəːčʼɨsɔːtɨt) təpɨp/ ɔːŋkɨtitɨl pɛlɨkɔːlɨk / ((…)). </ta>
            <ta e="T767" id="Seg_5090" s="T758">Nʼarqɨ čʼeːlon (/čʼeːlʼi) moːroqɨntɨ/ ilʼmat koraltɨpsa/: Čʼeːlɨnʼpɨta čʼeːlɨmɨt meː! / </ta>
            <ta e="T783" id="Seg_5091" s="T767">Tat karanʼnʼantɨ (/kutornantɨ)/ poːntɨ kuntɨ nuːt nekmɨ/, totaltukkal kəp petpɨlʼ utɨsa/ mutokt meːsım onant ılqɨt qontɨrnantɨ/. </ta>
            <ta e="T787" id="Seg_5092" s="T783">Montɨ əːtop toː čʼatantal/? </ta>
            <ta e="T797" id="Seg_5093" s="T787">Qontɨrsal qaj kun ɛːm tətot/ sarɨt nälʼap ilʼmatɨlʼ tat?/ ((…))</ta>
            <ta e="T803" id="Seg_5094" s="T797">Irät, irät, onak neːmɨ/, zoločʼenɨlʼ ɔːttɨlʼa/! </ta>
            <ta e="T811" id="Seg_5095" s="T803">Məšikkantɨ korel pit tat/, pürɨlʼ mɛntɨlʼ, čʼeːlɨlʼ sailʼ/. </ta>
            <ta e="T819" id="Seg_5096" s="T811">Aj mat serlɨ kɨkɨla/ qıšqat tantɨ mannɨmpɔːtɨt / ((…)). </ta>
            <ta e="T830" id="Seg_5097" s="T819">Näčʼät čʼontɨ künɨlʼ kɨqat toap/ ɛːnʼa pirqɨ toanʼ,/ nɨmtɨ korɨ mü/. </ta>
            <ta e="T834" id="Seg_5098" s="T830">Pitɨ matɨ tətɨ / ((…)) </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5099" s="T0">sar</ta>
            <ta e="T2" id="Seg_5100" s="T1">ima-ntɨ-sa</ta>
            <ta e="T3" id="Seg_5101" s="T2">nɛtɨ-r-sɨ-tɨ</ta>
            <ta e="T4" id="Seg_5102" s="T3">nɛkä-r-sɨ</ta>
            <ta e="T5" id="Seg_5103" s="T4">kuntak</ta>
            <ta e="T6" id="Seg_5104" s="T5">wətto-nt</ta>
            <ta e="T7" id="Seg_5105" s="T6">toqta-ltɨ-ŋa</ta>
            <ta e="T8" id="Seg_5106" s="T7">okno-t</ta>
            <ta e="T9" id="Seg_5107" s="T8">qan-tɨ</ta>
            <ta e="T10" id="Seg_5108" s="T9">ima-tɨ</ta>
            <ta e="T11" id="Seg_5109" s="T10">tɛp-ɨ-p</ta>
            <ta e="T12" id="Seg_5110" s="T11">ɛtɨ-qo</ta>
            <ta e="T13" id="Seg_5111" s="T12">ontɨ</ta>
            <ta e="T14" id="Seg_5112" s="T13">omta</ta>
            <ta e="T15" id="Seg_5113" s="T14">qarɨ-n</ta>
            <ta e="T16" id="Seg_5114" s="T15">nɔː-nɨ</ta>
            <ta e="T17" id="Seg_5115" s="T16">üːtɨ-t</ta>
            <ta e="T18" id="Seg_5116" s="T17">tɛtɨ</ta>
            <ta e="T19" id="Seg_5117" s="T18">ɛtɨ-kkɨ-tɨ</ta>
            <ta e="T20" id="Seg_5118" s="T19">ɛtɨ-kkɨ-tɨ</ta>
            <ta e="T21" id="Seg_5119" s="T20">meːltɨ</ta>
            <ta e="T22" id="Seg_5120" s="T21">limo-nt</ta>
            <ta e="T23" id="Seg_5121" s="T22">mannɨ-mpa</ta>
            <ta e="T24" id="Seg_5122" s="T23">tɛp</ta>
            <ta e="T25" id="Seg_5123" s="T24">sai-lʼ</ta>
            <ta e="T26" id="Seg_5124" s="T25">tɔːk-tɨ</ta>
            <ta e="T27" id="Seg_5125" s="T26">mant-al-pɨ-lʼa</ta>
            <ta e="T28" id="Seg_5126" s="T27">čʼüša-lɨ-sɔː-t</ta>
            <ta e="T29" id="Seg_5127" s="T28">kutɨ-lʼ</ta>
            <ta e="T30" id="Seg_5128" s="T29">ɛːmɨ-lʼa</ta>
            <ta e="T31" id="Seg_5129" s="T30">qarɨ-lʼ</ta>
            <ta e="T32" id="Seg_5130" s="T31">kɛnnɨ-n-t</ta>
            <ta e="T33" id="Seg_5131" s="T32">uːkoː-qɨnɨ</ta>
            <ta e="T34" id="Seg_5132" s="T33">pi-t</ta>
            <ta e="T35" id="Seg_5133" s="T34">tɛt</ta>
            <ta e="T36" id="Seg_5134" s="T35">ɔːmɨ</ta>
            <ta e="T37" id="Seg_5135" s="T36">neː-tɨ</ta>
            <ta e="T38" id="Seg_5136" s="T37">aša</ta>
            <ta e="T39" id="Seg_5137" s="T38">ata</ta>
            <ta e="T40" id="Seg_5138" s="T39">kekɨsä</ta>
            <ta e="T41" id="Seg_5139" s="T40">təp</ta>
            <ta e="T42" id="Seg_5140" s="T41">mannɨ-mpa-tɨ</ta>
            <ta e="T43" id="Seg_5141" s="T42">mɛrkɨ-lʼ</ta>
            <ta e="T44" id="Seg_5142" s="T43">palʼčʼa</ta>
            <ta e="T45" id="Seg_5143" s="T44">küčʼini-mpa</ta>
            <ta e="T46" id="Seg_5144" s="T45">sɨrɨ</ta>
            <ta e="T47" id="Seg_5145" s="T46">čʼom-na</ta>
            <ta e="T48" id="Seg_5146" s="T47">limɨ-t</ta>
            <ta e="T49" id="Seg_5147" s="T48">iː-ntɨ</ta>
            <ta e="T50" id="Seg_5148" s="T49">təttɨ</ta>
            <ta e="T51" id="Seg_5149" s="T50">muntɨk</ta>
            <ta e="T52" id="Seg_5150" s="T51">sɛrɨ</ta>
            <ta e="T53" id="Seg_5151" s="T52">ɛsa</ta>
            <ta e="T54" id="Seg_5152" s="T53">qən-na</ta>
            <ta e="T55" id="Seg_5153" s="T54">qän-na</ta>
            <ta e="T56" id="Seg_5154" s="T55">okkɨr</ta>
            <ta e="T57" id="Seg_5155" s="T56">čʼäː</ta>
            <ta e="T58" id="Seg_5156" s="T57">köt</ta>
            <ta e="T59" id="Seg_5157" s="T58">irä-t</ta>
            <ta e="T60" id="Seg_5158" s="T59">limoː-qɨn</ta>
            <ta e="T61" id="Seg_5159" s="T60">sai-m-tɨ</ta>
            <ta e="T62" id="Seg_5160" s="T61">alpa</ta>
            <ta e="T63" id="Seg_5161" s="T62">ašša</ta>
            <ta e="T64" id="Seg_5162" s="T63">iː-tɨ-t</ta>
            <ta e="T65" id="Seg_5163" s="T64">iː-tɨ-ŋɨ-t</ta>
            <ta e="T66" id="Seg_5164" s="T65">nʼɔːt</ta>
            <ta e="T67" id="Seg_5165" s="T66">sočʼelʼnik-a-qɨt</ta>
            <ta e="T68" id="Seg_5166" s="T67">pi-t</ta>
            <ta e="T69" id="Seg_5167" s="T68">təp-ɨ-n</ta>
            <ta e="T70" id="Seg_5168" s="T69">mi-nɨ-t</ta>
            <ta e="T71" id="Seg_5169" s="T70">nälʼa-m</ta>
            <ta e="T72" id="Seg_5170" s="T71">nälʼa-m-tɨ</ta>
            <ta e="T73" id="Seg_5171" s="T72">nom</ta>
            <ta e="T74" id="Seg_5172" s="T73">nom-tɨ</ta>
            <ta e="T75" id="Seg_5173" s="T74">rɛmkɨ</ta>
            <ta e="T76" id="Seg_5174" s="T75">qarɨ-t</ta>
            <ta e="T77" id="Seg_5175" s="T76">nɔːtnɨ-lʼ</ta>
            <ta e="T78" id="Seg_5176" s="T77">gostʼ</ta>
            <ta e="T79" id="Seg_5177" s="T78">pi-sa</ta>
            <ta e="T80" id="Seg_5178" s="T79">čʼeːlɨ-sa</ta>
            <ta e="T81" id="Seg_5179" s="T80">kuntɨ</ta>
            <ta e="T82" id="Seg_5180" s="T81">ətɨ-pɨlʼ</ta>
            <ta e="T83" id="Seg_5181" s="T82">kunta-qɨnɨ</ta>
            <ta e="T84" id="Seg_5182" s="T83">nʼɔːtɨ</ta>
            <ta e="T85" id="Seg_5183" s="T84">qes-s-ɨ-s</ta>
            <ta e="T86" id="Seg_5184" s="T85">mɔːt-qɨn</ta>
            <ta e="T87" id="Seg_5185" s="T86">tü-sa</ta>
            <ta e="T88" id="Seg_5186" s="T87">sar-əsɨ-tɨ</ta>
            <ta e="T89" id="Seg_5187" s="T88">mant-ɛčʼe-kku-s</ta>
            <ta e="T90" id="Seg_5188" s="T89">təp-ɨ-tkinɨ</ta>
            <ta e="T91" id="Seg_5189" s="T90">sɛttɨm-e-la</ta>
            <ta e="T92" id="Seg_5190" s="T91">kɛšqɨlʼe-sa</ta>
            <ta e="T93" id="Seg_5191" s="T92">ɔːnnɨ-m-t</ta>
            <ta e="T94" id="Seg_5192" s="T93">aša</ta>
            <ta e="T95" id="Seg_5193" s="T94">sʼeːpɨ-r-e-l-sɨ-t</ta>
            <ta e="T96" id="Seg_5194" s="T95">seːpɨ-r-sɨ</ta>
            <ta e="T97" id="Seg_5195" s="T96">aje</ta>
            <ta e="T98" id="Seg_5196" s="T97">čʼeːlɨ</ta>
            <ta e="T99" id="Seg_5197" s="T98">čʼonto-t</ta>
            <ta e="T100" id="Seg_5198" s="T99">urr-ɛː-čʼe-sa</ta>
            <ta e="T101" id="Seg_5199" s="T100">kekk-al-pɨ-sa</ta>
            <ta e="T102" id="Seg_5200" s="T101">nakka-l-pɨ-s</ta>
            <ta e="T103" id="Seg_5201" s="T102">kuntɨ</ta>
            <ta e="T104" id="Seg_5202" s="T103">sar</ta>
            <ta e="T105" id="Seg_5203" s="T104">kutar</ta>
            <ta e="T106" id="Seg_5204" s="T105">ɛː-qo</ta>
            <ta e="T107" id="Seg_5205" s="T106">grešnɨk</ta>
            <ta e="T108" id="Seg_5206" s="T107">ɛː-ppa</ta>
            <ta e="T109" id="Seg_5207" s="T108">poː-tɨ</ta>
            <ta e="T110" id="Seg_5208" s="T109">qɛn-na</ta>
            <ta e="T111" id="Seg_5209" s="T110">qän-nɨ</ta>
            <ta e="T112" id="Seg_5210" s="T111">mitɨ</ta>
            <ta e="T113" id="Seg_5211" s="T112">ɔːnʼkɨ</ta>
            <ta e="T114" id="Seg_5212" s="T113">mənɨlʼ</ta>
            <ta e="T115" id="Seg_5213" s="T114">wänɨlʼ</ta>
            <ta e="T116" id="Seg_5214" s="T115">ima-p</ta>
            <ta e="T117" id="Seg_5215" s="T116">ɨː-nʼɨ-t</ta>
            <ta e="T118" id="Seg_5216" s="T117">sar</ta>
            <ta e="T119" id="Seg_5217" s="T118">kutar</ta>
            <ta e="T120" id="Seg_5218" s="T119">kɛtɨ-qo</ta>
            <ta e="T121" id="Seg_5219" s="T120">nälʼ-qup</ta>
            <ta e="T122" id="Seg_5220" s="T121">ɛː-sa</ta>
            <ta e="T123" id="Seg_5221" s="T122">on-ɨ-lʼ</ta>
            <ta e="T124" id="Seg_5222" s="T123">mitɨ</ta>
            <ta e="T125" id="Seg_5223" s="T124">sar-ɨ-lʼ</ta>
            <ta e="T126" id="Seg_5224" s="T125">ima</ta>
            <ta e="T127" id="Seg_5225" s="T126">pirqɨ</ta>
            <ta e="T128" id="Seg_5226" s="T127">tašik</ta>
            <ta e="T129" id="Seg_5227" s="T128">sərɨ</ta>
            <ta e="T130" id="Seg_5228" s="T129">ɛː-sa</ta>
            <ta e="T131" id="Seg_5229" s="T130">tɛnɨ-l</ta>
            <ta e="T132" id="Seg_5230" s="T131">tɔːk-sa</ta>
            <ta e="T133" id="Seg_5231" s="T132">muntok-t</ta>
            <ta e="T134" id="Seg_5232" s="T133">iː-sɨ-t</ta>
            <ta e="T135" id="Seg_5233" s="T134">nɨːn</ta>
            <ta e="T136" id="Seg_5234" s="T135">aj</ta>
            <ta e="T137" id="Seg_5235" s="T136">nʼɔːtɨ</ta>
            <ta e="T138" id="Seg_5236" s="T137">soma-lʼčʼɨ-mčʼa-t</ta>
            <ta e="T139" id="Seg_5237" s="T138">qɔːt-ol</ta>
            <ta e="T140" id="Seg_5238" s="T139">nʼɔːtɨ</ta>
            <ta e="T141" id="Seg_5239" s="T140">jarɨk</ta>
            <ta e="T142" id="Seg_5240" s="T141">ɛsa</ta>
            <ta e="T143" id="Seg_5241" s="T142">mɔːt-tɨ</ta>
            <ta e="T144" id="Seg_5242" s="T143">nɔː-nɨ</ta>
            <ta e="T145" id="Seg_5243" s="T144">taːtɨ-mpɨ-s-tɨ</ta>
            <ta e="T146" id="Seg_5244" s="T145">kekɨs</ta>
            <ta e="T147" id="Seg_5245" s="T146">zerkala-lʼa-m</ta>
            <ta e="T148" id="Seg_5246" s="T147">okɨr</ta>
            <ta e="T149" id="Seg_5247" s="T148">zerkala-lʼa</ta>
            <ta e="T150" id="Seg_5248" s="T149">nılʼčʼi-lʼ</ta>
            <ta e="T151" id="Seg_5249" s="T150">ɛː-sa</ta>
            <ta e="T152" id="Seg_5250" s="T151">konʼɨ-mpɨ-t-qo</ta>
            <ta e="T153" id="Seg_5251" s="T152">tɛnɨmɨ-mpa</ta>
            <ta e="T154" id="Seg_5252" s="T153">təp-sa</ta>
            <ta e="T155" id="Seg_5253" s="T154">okɨr</ta>
            <ta e="T156" id="Seg_5254" s="T155">təp</ta>
            <ta e="T157" id="Seg_5255" s="T156">qan</ta>
            <ta e="T158" id="Seg_5256" s="T157">ɛː-kka</ta>
            <ta e="T159" id="Seg_5257" s="T158">soma</ta>
            <ta e="T160" id="Seg_5258" s="T159">puːtɨ-k</ta>
            <ta e="T161" id="Seg_5259" s="T160">ɔːnt-al-pɨ-kka</ta>
            <ta e="T162" id="Seg_5260" s="T161">təp-sa</ta>
            <ta e="T163" id="Seg_5261" s="T162">šoqqa-k</ta>
            <ta e="T164" id="Seg_5262" s="T163">kara-mpɨ-kka</ta>
            <ta e="T165" id="Seg_5263" s="T164">soma-lʼčʼi-mpɨl</ta>
            <ta e="T166" id="Seg_5264" s="T165">konʼɨ-mpɨ-kka</ta>
            <ta e="T167" id="Seg_5265" s="T166">čʼeːlo-m</ta>
            <ta e="T168" id="Seg_5266" s="T167">zerkala-lʼa-m</ta>
            <ta e="T169" id="Seg_5267" s="T168">kɛt-at</ta>
            <ta e="T170" id="Seg_5268" s="T169">napa</ta>
            <ta e="T171" id="Seg_5269" s="T170">čʼeːlɨ-k</ta>
            <ta e="T172" id="Seg_5270" s="T171">ınna</ta>
            <ta e="T173" id="Seg_5271" s="T172">pin-tɨ</ta>
            <ta e="T174" id="Seg_5272" s="T173">mat</ta>
            <ta e="T175" id="Seg_5273" s="T174">qaj</ta>
            <ta e="T176" id="Seg_5274" s="T175">tətɨ-n</ta>
            <ta e="T177" id="Seg_5275" s="T176">iː-qɨt</ta>
            <ta e="T178" id="Seg_5276" s="T177">muntok-t</ta>
            <ta e="T179" id="Seg_5277" s="T178">soma</ta>
            <ta e="T180" id="Seg_5278" s="T179">kuras</ta>
            <ta e="T181" id="Seg_5279" s="T180">sɛrɨ</ta>
            <ta e="T182" id="Seg_5280" s="T181">ɛː-nʼa-k</ta>
            <ta e="T183" id="Seg_5281" s="T182">zerkala-lʼa</ta>
            <ta e="T184" id="Seg_5282" s="T183">kətɨ-kkɨ-tɨ</ta>
            <ta e="T185" id="Seg_5283" s="T184">tat</ta>
            <ta e="T186" id="Seg_5284" s="T185">sar-ɨ-t</ta>
            <ta e="T187" id="Seg_5285" s="T186">ima</ta>
            <ta e="T188" id="Seg_5286" s="T187">aša</ta>
            <ta e="T189" id="Seg_5287" s="T188">anta-k</ta>
            <ta e="T190" id="Seg_5288" s="T189">tan</ta>
            <ta e="T191" id="Seg_5289" s="T190">kes</ta>
            <ta e="T192" id="Seg_5290" s="T191">okɨr</ta>
            <ta e="T193" id="Seg_5291" s="T192">muntok-t</ta>
            <ta e="T194" id="Seg_5292" s="T193">soma</ta>
            <ta e="T195" id="Seg_5293" s="T194">kurasa</ta>
            <ta e="T196" id="Seg_5294" s="T195">aj</ta>
            <ta e="T197" id="Seg_5295" s="T196">sɛrɨ</ta>
            <ta e="T198" id="Seg_5296" s="T197">ɛː-nʼa-nt</ta>
            <ta e="T199" id="Seg_5297" s="T198">sar-ɨ-t</ta>
            <ta e="T200" id="Seg_5298" s="T199">ima</ta>
            <ta e="T201" id="Seg_5299" s="T200">laqqɨ-mɔːn-na</ta>
            <ta e="T202" id="Seg_5300" s="T201">qəqtɨ_pɔːrɨ-m</ta>
            <ta e="T203" id="Seg_5301" s="T202">ip-qal-nɨ-tɨ</ta>
            <ta e="T204" id="Seg_5302" s="T203">sai-ntɨ-sa</ta>
            <ta e="T205" id="Seg_5303" s="T204">rɨpka-lʼčʼe-nʼɨ-t</ta>
            <ta e="T206" id="Seg_5304" s="T205">munɨ-ntɨ-sa</ta>
            <ta e="T207" id="Seg_5305" s="T206">kaši-lʼčʼe-nɨ-t</ta>
            <ta e="T208" id="Seg_5306" s="T207">utɨ-p</ta>
            <ta e="T209" id="Seg_5307" s="T208">qenʼɨ-nt</ta>
            <ta e="T210" id="Seg_5308" s="T209">kompɨlɨ-mpa</ta>
            <ta e="T211" id="Seg_5309" s="T210">zerkala-lʼa-nt</ta>
            <ta e="T212" id="Seg_5310" s="T211">mannɨ-mpɨ-la</ta>
            <ta e="T213" id="Seg_5311" s="T212">sar-ɨ-t</ta>
            <ta e="T214" id="Seg_5312" s="T213">nälʼa</ta>
            <ta e="T215" id="Seg_5313" s="T214">čʼilʼa-lʼ</ta>
            <ta e="T216" id="Seg_5314" s="T215">nätäk</ta>
            <ta e="T217" id="Seg_5315" s="T216">lʼamɨ-k</ta>
            <ta e="T218" id="Seg_5316" s="T217">čʼontɨ-k</ta>
            <ta e="T219" id="Seg_5317" s="T218">orɨ-m-nanta</ta>
            <ta e="T220" id="Seg_5318" s="T219">na-t</ta>
            <ta e="T221" id="Seg_5319" s="T220">sitɨ kočʼa-t</ta>
            <ta e="T222" id="Seg_5320" s="T221">orɨ-m-nanta</ta>
            <ta e="T223" id="Seg_5321" s="T222">məčʼi-š-i-nta</ta>
            <ta e="T224" id="Seg_5322" s="T223">orɨ-m-ta-na</ta>
            <ta e="T225" id="Seg_5323" s="T224">sərɨ</ta>
            <ta e="T226" id="Seg_5324" s="T225">məčʼi-k</ta>
            <ta e="T227" id="Seg_5325" s="T226">sʼaː</ta>
            <ta e="T228" id="Seg_5326" s="T227">sai-n</ta>
            <ta e="T229" id="Seg_5327" s="T228">untɨ-k</ta>
            <ta e="T230" id="Seg_5328" s="T229">selmo-kɔːlɨ-k</ta>
            <ta e="T231" id="Seg_5329" s="T230">tɔːt-ɨ-k</ta>
            <ta e="T232" id="Seg_5330" s="T231">puːtɨ-k</ta>
            <ta e="T233" id="Seg_5331" s="T232">tita-lʼ</ta>
            <ta e="T234" id="Seg_5332" s="T233">qum-tɨ</ta>
            <ta e="T235" id="Seg_5333" s="T234">peː-qɨlʼ-i-sa</ta>
            <ta e="T236" id="Seg_5334" s="T235">korolʼ-t</ta>
            <ta e="T237" id="Seg_5335" s="T236">ija</ta>
            <ta e="T238" id="Seg_5336" s="T237">Elisej</ta>
            <ta e="T239" id="Seg_5337" s="T238">tü-sa</ta>
            <ta e="T240" id="Seg_5338" s="T239">qup</ta>
            <ta e="T241" id="Seg_5339" s="T240">sar</ta>
            <ta e="T242" id="Seg_5340" s="T241">əːtɨ-p</ta>
            <ta e="T243" id="Seg_5341" s="T242">mi-sɨ-t</ta>
            <ta e="T244" id="Seg_5342" s="T243">mɔːt-qɨn</ta>
            <ta e="T245" id="Seg_5343" s="T244">i-psa</ta>
            <ta e="T246" id="Seg_5344" s="T245">mɨta</ta>
            <ta e="T247" id="Seg_5345" s="T246">ɛː-nʼa</ta>
            <ta e="T248" id="Seg_5346" s="T247">seːlʼčʼi</ta>
            <ta e="T249" id="Seg_5347" s="T248">təm-tɨ-tili</ta>
            <ta e="T250" id="Seg_5348" s="T249">qɛːtɨ-t</ta>
            <ta e="T251" id="Seg_5349" s="T250">aje</ta>
            <ta e="T252" id="Seg_5350" s="T251">toːn</ta>
            <ta e="T253" id="Seg_5351" s="T252">aj</ta>
            <ta e="T254" id="Seg_5352" s="T253">tɛː-sar-ɨ-li</ta>
            <ta e="T255" id="Seg_5353" s="T254">mɔːt-ɨ-jä-t</ta>
            <ta e="T256" id="Seg_5354" s="T255">näta-t-ɨ-tkinɨ</ta>
            <ta e="T257" id="Seg_5355" s="T256">taq-qɨli-qo</ta>
            <ta e="T258" id="Seg_5356" s="T257">sar-ɨ-t</ta>
            <ta e="T259" id="Seg_5357" s="T258">ima</ta>
            <ta e="T260" id="Seg_5358" s="T259">toqta-t-ku-tɨl</ta>
            <ta e="T261" id="Seg_5359" s="T260">zerkala-lʼa-ntɨ-sa</ta>
            <ta e="T262" id="Seg_5360" s="T261">ontɨ</ta>
            <ta e="T263" id="Seg_5361" s="T262">əːtɨ-k</ta>
            <ta e="T264" id="Seg_5362" s="T263">tɛp-ɨ-n</ta>
            <ta e="T265" id="Seg_5363" s="T264">čʼattɨ-kku-s-tɨ</ta>
            <ta e="T266" id="Seg_5364" s="T265">Mat</ta>
            <ta e="T267" id="Seg_5365" s="T266">qaj</ta>
            <ta e="T268" id="Seg_5366" s="T267">kət-at</ta>
            <ta e="T269" id="Seg_5367" s="T268">mäkkä</ta>
            <ta e="T270" id="Seg_5368" s="T269">muntok</ta>
            <ta e="T271" id="Seg_5369" s="T270">kuras-sa</ta>
            <ta e="T272" id="Seg_5370" s="T271">aj</ta>
            <ta e="T273" id="Seg_5371" s="T272">sərɨ</ta>
            <ta e="T274" id="Seg_5372" s="T273">ɛː-nʼa-k</ta>
            <ta e="T275" id="Seg_5373" s="T274">qaj</ta>
            <ta e="T276" id="Seg_5374" s="T275">kat</ta>
            <ta e="T277" id="Seg_5375" s="T276">zerkala-lʼa</ta>
            <ta e="T278" id="Seg_5376" s="T277">tom-pɨ-t</ta>
            <ta e="T279" id="Seg_5377" s="T278">kuras-sa</ta>
            <ta e="T280" id="Seg_5378" s="T279">ɛː-nʼa-nt</ta>
            <ta e="T281" id="Seg_5379" s="T280">aša</ta>
            <ta e="T282" id="Seg_5380" s="T281">anta-k</ta>
            <ta e="T283" id="Seg_5381" s="T282">sar-ɨ-t</ta>
            <ta e="T284" id="Seg_5382" s="T283">nälʼa</ta>
            <ta e="T285" id="Seg_5383" s="T284">qotəl</ta>
            <ta e="T286" id="Seg_5384" s="T285">muntok-t</ta>
            <ta e="T287" id="Seg_5385" s="T286">kuras-sa</ta>
            <ta e="T288" id="Seg_5386" s="T287">aj</ta>
            <ta e="T289" id="Seg_5387" s="T288">sərɨ</ta>
            <ta e="T290" id="Seg_5388" s="T289">ɛː-nʼa</ta>
            <ta e="T291" id="Seg_5389" s="T290">sar-ɨ-t</ta>
            <ta e="T292" id="Seg_5390" s="T291">ima</ta>
            <ta e="T293" id="Seg_5391" s="T292">alpa</ta>
            <ta e="T294" id="Seg_5392" s="T293">pakta</ta>
            <ta e="T295" id="Seg_5393" s="T294">aj</ta>
            <ta e="T296" id="Seg_5394" s="T295">küra-tɔːl-na</ta>
            <ta e="T297" id="Seg_5395" s="T296">utɨ-lʼa-ntɨ-s</ta>
            <ta e="T298" id="Seg_5396" s="T297">zerkala-lʼa-p</ta>
            <ta e="T299" id="Seg_5397" s="T298">pəqša-tɨ-nʼɨ-t</ta>
            <ta e="T300" id="Seg_5398" s="T299">lʼakčʼin-lʼa-ntɨ-s</ta>
            <ta e="T301" id="Seg_5399" s="T300">čʼelʼčʼ-ɔːl-na</ta>
            <ta e="T302" id="Seg_5400" s="T301">nɨː</ta>
            <ta e="T303" id="Seg_5401" s="T302">аh</ta>
            <ta e="T304" id="Seg_5402" s="T303">tan</ta>
            <ta e="T305" id="Seg_5403" s="T304">nılʼčʼi-lʼ</ta>
            <ta e="T306" id="Seg_5404" s="T305">steklo-nʼo-ntɨ</ta>
            <ta e="T307" id="Seg_5405" s="T306">tan</ta>
            <ta e="T308" id="Seg_5406" s="T307">na</ta>
            <ta e="T309" id="Seg_5407" s="T308">moːlmɨ-ta-ntɨ</ta>
            <ta e="T310" id="Seg_5408" s="T309">mat</ta>
            <ta e="T311" id="Seg_5409" s="T310">čʼɔːt</ta>
            <ta e="T312" id="Seg_5410" s="T311">təp</ta>
            <ta e="T313" id="Seg_5411" s="T312">kutar</ta>
            <ta e="T314" id="Seg_5412" s="T313">mannɨ-mmä-nta</ta>
            <ta e="T315" id="Seg_5413" s="T314">matsa</ta>
            <ta e="T316" id="Seg_5414" s="T315">mat</ta>
            <ta e="T317" id="Seg_5415" s="T316">təp-ɨ-t</ta>
            <ta e="T318" id="Seg_5416" s="T317">moːmɨ-t</ta>
            <ta e="T319" id="Seg_5417" s="T318">lʼam-qal-tanta-p</ta>
            <ta e="T320" id="Seg_5418" s="T319">qo-ntɨr-nɔː-lɨt</ta>
            <ta e="T321" id="Seg_5419" s="T320">orɨ-m-na</ta>
            <ta e="T322" id="Seg_5420" s="T321">qaj</ta>
            <ta e="T323" id="Seg_5421" s="T322">kutar</ta>
            <ta e="T324" id="Seg_5422" s="T323">aša</ta>
            <ta e="T325" id="Seg_5423" s="T324">sərɨ</ta>
            <ta e="T326" id="Seg_5424" s="T325">ɛː-nʼa</ta>
            <ta e="T327" id="Seg_5425" s="T326">əmɨ-t</ta>
            <ta e="T328" id="Seg_5426" s="T327">pɛrqɨ-lʼ</ta>
            <ta e="T329" id="Seg_5427" s="T328">tıːri</ta>
            <ta e="T330" id="Seg_5428" s="T329">ɔːmt-sa</ta>
            <ta e="T331" id="Seg_5429" s="T330">kekɨs</ta>
            <ta e="T332" id="Seg_5430" s="T331">sɨro-nt</ta>
            <ta e="T333" id="Seg_5431" s="T332">mant-al-pɨ-sa</ta>
            <ta e="T334" id="Seg_5432" s="T333">kɨsa</ta>
            <ta e="T335" id="Seg_5433" s="T334">kɛt-at</ta>
            <ta e="T336" id="Seg_5434" s="T335">qantɨk</ta>
            <ta e="T337" id="Seg_5435" s="T336">man</ta>
            <ta e="T338" id="Seg_5436" s="T337">nɔː-n</ta>
            <ta e="T339" id="Seg_5437" s="T338">nɨːnɨ</ta>
            <ta e="T340" id="Seg_5438" s="T339">puːt</ta>
            <ta e="T341" id="Seg_5439" s="T340">təp</ta>
            <ta e="T342" id="Seg_5440" s="T341">kuras-sa</ta>
            <ta e="T343" id="Seg_5441" s="T342">ɛː-nta</ta>
            <ta e="T344" id="Seg_5442" s="T343">i̇nna</ta>
            <ta e="T345" id="Seg_5443" s="T344">kət-at</ta>
            <ta e="T346" id="Seg_5444" s="T345">mat</ta>
            <ta e="T347" id="Seg_5445" s="T346">kes</ta>
            <ta e="T348" id="Seg_5446" s="T347">nʼančʼa-k</ta>
            <ta e="T349" id="Seg_5447" s="T348">ɔːmtɨ-lʼ</ta>
            <ta e="T350" id="Seg_5448" s="T349">qoː</ta>
            <ta e="T351" id="Seg_5449" s="T350">mɨ-t</ta>
            <ta e="T352" id="Seg_5450" s="T351">kolʼ-alt-atɨ</ta>
            <ta e="T353" id="Seg_5451" s="T352">qos</ta>
            <ta e="T354" id="Seg_5452" s="T353">tətɨ</ta>
            <ta e="T355" id="Seg_5453" s="T354">muntɨk</ta>
            <ta e="T356" id="Seg_5454" s="T355">matsa</ta>
            <ta e="T357" id="Seg_5455" s="T356">čʼäːŋka</ta>
            <ta e="T358" id="Seg_5456" s="T357">nʼančʼi-tɨlʼ</ta>
            <ta e="T359" id="Seg_5457" s="T358">mɨ</ta>
            <ta e="T360" id="Seg_5458" s="T359">nılʼčʼi-k</ta>
            <ta e="T361" id="Seg_5459" s="T360">qɔːtɨ</ta>
            <ta e="T362" id="Seg_5460" s="T361">zerkala-lʼa</ta>
            <ta e="T363" id="Seg_5461" s="T362">kətɨ-nʼɨ-tɨ</ta>
            <ta e="T364" id="Seg_5462" s="T363">sar-ɨ-t</ta>
            <ta e="T365" id="Seg_5463" s="T364">nälʼa</ta>
            <ta e="T366" id="Seg_5464" s="T365">qotol</ta>
            <ta e="T367" id="Seg_5465" s="T366">pija</ta>
            <ta e="T368" id="Seg_5466" s="T367">kuras-sa</ta>
            <ta e="T369" id="Seg_5467" s="T368">aj</ta>
            <ta e="T370" id="Seg_5468" s="T369">sərɨ</ta>
            <ta e="T371" id="Seg_5469" s="T370">ɛː-nʼa</ta>
            <ta e="T372" id="Seg_5470" s="T371">kutar</ta>
            <ta e="T373" id="Seg_5471" s="T372">meː-ta-l</ta>
            <ta e="T374" id="Seg_5472" s="T373">puːtɨ-m-t</ta>
            <ta e="T375" id="Seg_5473" s="T374">am-la</ta>
            <ta e="T376" id="Seg_5474" s="T375">zerkala-lʼa-p</ta>
            <ta e="T377" id="Seg_5475" s="T376">konna</ta>
            <ta e="T378" id="Seg_5476" s="T377">čʼattɨ-t</ta>
            <ta e="T379" id="Seg_5477" s="T378">čʼernavka-m-tɨ</ta>
            <ta e="T380" id="Seg_5478" s="T379">qərɨ-nʼɨ-tɨ</ta>
            <ta e="T381" id="Seg_5479" s="T380">nılʼčʼi-k</ta>
            <ta e="T382" id="Seg_5480" s="T381">təp-tɨ</ta>
            <ta e="T383" id="Seg_5481" s="T382">*qur-altɨ-nʼɨ-t</ta>
            <ta e="T384" id="Seg_5482" s="T383">ontɨ</ta>
            <ta e="T385" id="Seg_5483" s="T384">sennɨ-lʼ</ta>
            <ta e="T386" id="Seg_5484" s="T385">nätak</ta>
            <ta e="T387" id="Seg_5485" s="T386">tɨna</ta>
            <ta e="T388" id="Seg_5486" s="T387">sar-ɨ-t</ta>
            <ta e="T389" id="Seg_5487" s="T388">nälʼa-p</ta>
            <ta e="T390" id="Seg_5488" s="T389">mačʼɨ-t</ta>
            <ta e="T391" id="Seg_5489" s="T390">puːto-nt</ta>
            <ta e="T392" id="Seg_5490" s="T391">qɛn-tɨr-ta-ltɨ-l</ta>
            <ta e="T393" id="Seg_5491" s="T392">kurɨ-nʼɨ-mpɨj</ta>
            <ta e="T394" id="Seg_5492" s="T393">ilɨ-la</ta>
            <ta e="T395" id="Seg_5493" s="T394">aj</ta>
            <ta e="T396" id="Seg_5494" s="T395">qəːčʼi-nʼɨ-mpɨj</ta>
            <ta e="T397" id="Seg_5495" s="T396">näčʼčʼäːtɨ-nʼɨt</ta>
            <ta e="T398" id="Seg_5496" s="T397">čʼöː-n</ta>
            <ta e="T399" id="Seg_5497" s="T398">ıl-nʼɨt</ta>
            <ta e="T400" id="Seg_5498" s="T399">čʼumpɨ</ta>
            <ta e="T401" id="Seg_5499" s="T400">neː-e-t</ta>
            <ta e="T402" id="Seg_5500" s="T401">am-nʼɨntitqo</ta>
            <ta e="T403" id="Seg_5501" s="T402">loːsɨ</ta>
            <ta e="T404" id="Seg_5502" s="T403">montɨ</ta>
            <ta e="T405" id="Seg_5503" s="T404">antɨ-tanta</ta>
            <ta e="T406" id="Seg_5504" s="T405">nʼenʼnʼ-alʼ</ta>
            <ta e="T407" id="Seg_5505" s="T406">ima-s</ta>
            <ta e="T408" id="Seg_5506" s="T407">sar-ɨ-t</ta>
            <ta e="T409" id="Seg_5507" s="T408">nälʼa-s</ta>
            <ta e="T410" id="Seg_5508" s="T409">Čʼernovka</ta>
            <ta e="T411" id="Seg_5509" s="T410">qən-ta</ta>
            <ta e="T412" id="Seg_5510" s="T411">mačʼo-ntɨ</ta>
            <ta e="T413" id="Seg_5511" s="T412">na</ta>
            <ta e="T414" id="Seg_5512" s="T413">našak</ta>
            <ta e="T415" id="Seg_5513" s="T414">kuntak</ta>
            <ta e="T416" id="Seg_5514" s="T415">tɨna</ta>
            <ta e="T417" id="Seg_5515" s="T416">qən-t-oj</ta>
            <ta e="T418" id="Seg_5516" s="T417">sar-ɨ-t</ta>
            <ta e="T419" id="Seg_5517" s="T418">nälʼa</ta>
            <ta e="T420" id="Seg_5518" s="T419">nɨːna</ta>
            <ta e="T421" id="Seg_5519" s="T420">qən-ta</ta>
            <ta e="T422" id="Seg_5520" s="T421">qu-kɨ-ntɨ-šak</ta>
            <ta e="T423" id="Seg_5521" s="T422">nɨrkɨ-mɔːn-na</ta>
            <ta e="T424" id="Seg_5522" s="T423">omt-alta-na</ta>
            <ta e="T425" id="Seg_5523" s="T424">ilɨ-pta-mɨ</ta>
            <ta e="T426" id="Seg_5524" s="T425">qaj-qɨt</ta>
            <ta e="T427" id="Seg_5525" s="T426">kɛt-at</ta>
            <ta e="T428" id="Seg_5526" s="T427">uro-p</ta>
            <ta e="T429" id="Seg_5527" s="T428">ɛː-nʼa</ta>
            <ta e="T430" id="Seg_5528" s="T429">nälɨ-qup</ta>
            <ta e="T431" id="Seg_5529" s="T430">ɨkɨ</ta>
            <ta e="T432" id="Seg_5530" s="T431">sip</ta>
            <ta e="T433" id="Seg_5531" s="T432">talʼ-čʼ-olʼ-ašik</ta>
            <ta e="T434" id="Seg_5532" s="T433">kutar</ta>
            <ta e="T435" id="Seg_5533" s="T434">qum-ɨ-p</ta>
            <ta e="T436" id="Seg_5534" s="T435">qo-nta-p</ta>
            <ta e="T437" id="Seg_5535" s="T436">tašınt</ta>
            <ta e="T438" id="Seg_5536" s="T437">qaj-qo</ta>
            <ta e="T439" id="Seg_5537" s="T438">kɨka-nt</ta>
            <ta e="T440" id="Seg_5538" s="T439">na-tqo</ta>
            <ta e="T441" id="Seg_5539" s="T440">meː-ta-k</ta>
            <ta e="T442" id="Seg_5540" s="T441">toːnna-m</ta>
            <ta e="T443" id="Seg_5541" s="T442">puːt-sa</ta>
            <ta e="T444" id="Seg_5542" s="T443">təp-tɨ</ta>
            <ta e="T445" id="Seg_5543" s="T444">kɨka-l</ta>
            <ta e="T446" id="Seg_5544" s="T445">aša</ta>
            <ta e="T447" id="Seg_5545" s="T446">kura-sɨ-t</ta>
            <ta e="T448" id="Seg_5546" s="T447">aša</ta>
            <ta e="T449" id="Seg_5547" s="T448">qəs-sɨ-t</ta>
            <ta e="T450" id="Seg_5548" s="T449">uːtɨ-sɨ-tɨ</ta>
            <ta e="T451" id="Seg_5549" s="T450">nılʼčʼi-k</ta>
            <ta e="T452" id="Seg_5550" s="T451">kətɨ-l</ta>
            <ta e="T453" id="Seg_5551" s="T452">Qɛnʼ-aš</ta>
            <ta e="T454" id="Seg_5552" s="T453">nop</ta>
            <ta e="T455" id="Seg_5553" s="T454">sıntɨ</ta>
            <ta e="T456" id="Seg_5554" s="T455">na</ta>
            <ta e="T457" id="Seg_5555" s="T456">məre-t</ta>
            <ta e="T458" id="Seg_5556" s="T457">nɨːnɨ</ta>
            <ta e="T459" id="Seg_5557" s="T458">ontɨ</ta>
            <ta e="T460" id="Seg_5558" s="T459">moqɨn</ta>
            <ta e="T461" id="Seg_5559" s="T460">tü-ssa</ta>
            <ta e="T462" id="Seg_5560" s="T461">qajɨ</ta>
            <ta e="T463" id="Seg_5561" s="T462">sar-ɨ-t</ta>
            <ta e="T464" id="Seg_5562" s="T463">ima</ta>
            <ta e="T465" id="Seg_5563" s="T464">kətɨ-nʼɨ-tɨ</ta>
            <ta e="T466" id="Seg_5564" s="T465">kuras-sɨmɨ-lʼ</ta>
            <ta e="T467" id="Seg_5565" s="T466">nälʼ-qup</ta>
            <ta e="T468" id="Seg_5566" s="T467">kun</ta>
            <ta e="T469" id="Seg_5567" s="T468">ɛː-nʼ</ta>
            <ta e="T470" id="Seg_5568" s="T469">pɛlɨ-kɔːlɨ-k</ta>
            <ta e="T471" id="Seg_5569" s="T470">mačʼo-t</ta>
            <ta e="T472" id="Seg_5570" s="T471">nɨŋa</ta>
            <ta e="T473" id="Seg_5571" s="T472">təp-tɨ</ta>
            <ta e="T474" id="Seg_5572" s="T473">kətɨ-nʼɨ-tɨ</ta>
            <ta e="T475" id="Seg_5573" s="T474">təp-ɨ-n</ta>
            <ta e="T476" id="Seg_5574" s="T475">sɔːrɨ-mpa</ta>
            <ta e="T477" id="Seg_5575" s="T476">qɔːmɨ-k</ta>
            <ta e="T478" id="Seg_5576" s="T477">sɨnʼte</ta>
            <ta e="T479" id="Seg_5577" s="T478">neː-t-tɨ</ta>
            <ta e="T480" id="Seg_5578" s="T479">suːrɨ-t</ta>
            <ta e="T481" id="Seg_5579" s="T480">qato-nt</ta>
            <ta e="T482" id="Seg_5580" s="T481">olʼčʼa-čʼe-mmä</ta>
            <ta e="T483" id="Seg_5581" s="T482">qɔːna-k</ta>
            <ta e="T484" id="Seg_5582" s="T483">təp</ta>
            <ta e="T485" id="Seg_5583" s="T484">na</ta>
            <ta e="T486" id="Seg_5584" s="T485">kə-kkɨ-ta-nɨ-t</ta>
            <ta e="T487" id="Seg_5585" s="T486">qu-r-mo-t</ta>
            <ta e="T488" id="Seg_5586" s="T487">aj</ta>
            <ta e="T489" id="Seg_5587" s="T488">na</ta>
            <ta e="T490" id="Seg_5588" s="T489">seːpɨ</ta>
            <ta e="T491" id="Seg_5589" s="T490">ɛː-nnɨ-nt</ta>
            <ta e="T492" id="Seg_5590" s="T491">əːtɨ</ta>
            <ta e="T493" id="Seg_5591" s="T492">qən-na</ta>
            <ta e="T494" id="Seg_5592" s="T493">nɨːnɨ</ta>
            <ta e="T495" id="Seg_5593" s="T494">nılʼčʼi-lʼ</ta>
            <ta e="T496" id="Seg_5594" s="T495">sar-ɨ-t</ta>
            <ta e="T497" id="Seg_5595" s="T496">nälʼa</ta>
            <ta e="T498" id="Seg_5596" s="T497">urɨ-nta-na</ta>
            <ta e="T499" id="Seg_5597" s="T498">paši-r-na</ta>
            <ta e="T500" id="Seg_5598" s="T499">sar</ta>
            <ta e="T501" id="Seg_5599" s="T500">nälʼa-ntɨ-qo</ta>
            <ta e="T502" id="Seg_5600" s="T501">korolʼ</ta>
            <ta e="T503" id="Seg_5601" s="T502">ija</ta>
            <ta e="T504" id="Seg_5602" s="T503">Elisej</ta>
            <ta e="T505" id="Seg_5603" s="T504">nom-tɨ</ta>
            <ta e="T506" id="Seg_5604" s="T505">omtɨ-tɨ-la</ta>
            <ta e="T507" id="Seg_5605" s="T506">puːlä</ta>
            <ta e="T508" id="Seg_5606" s="T507">toqt-alta</ta>
            <ta e="T509" id="Seg_5607" s="T508">aj</ta>
            <ta e="T510" id="Seg_5608" s="T509">peː-lʼa</ta>
            <ta e="T511" id="Seg_5609" s="T510">qən-na</ta>
            <ta e="T512" id="Seg_5610" s="T511">kuras-sɨma-t</ta>
            <ta e="T513" id="Seg_5611" s="T512">sıːčʼi-n-t</ta>
            <ta e="T514" id="Seg_5612" s="T513">olɨ-p</ta>
            <ta e="T515" id="Seg_5613" s="T514">tıːtap</ta>
            <ta e="T516" id="Seg_5614" s="T515">iː-psa</ta>
            <ta e="T517" id="Seg_5615" s="T516">im-tɨ</ta>
            <ta e="T518" id="Seg_5616" s="T517">neː-m-tɨ</ta>
            <ta e="T519" id="Seg_5617" s="T518">a</ta>
            <ta e="T520" id="Seg_5618" s="T519">itɨ</ta>
            <ta e="T521" id="Seg_5619" s="T520">neː-tɨ</ta>
            <ta e="T522" id="Seg_5620" s="T521">qarɨ-t</ta>
            <ta e="T523" id="Seg_5621" s="T522">təttɨ</ta>
            <ta e="T524" id="Seg_5622" s="T523">mačʼi-t</ta>
            <ta e="T525" id="Seg_5623" s="T524">puːto-t</ta>
            <ta e="T526" id="Seg_5624" s="T525">ur-kɨ-mpɨ-la</ta>
            <ta e="T527" id="Seg_5625" s="T526">kurɨ-tä-ntɨ</ta>
            <ta e="T528" id="Seg_5626" s="T527">sitɨ kotä-t</ta>
            <ta e="T529" id="Seg_5627" s="T528">mɔːt-ɨ-n</ta>
            <ta e="T530" id="Seg_5628" s="T529">iː-ntɨ</ta>
            <ta e="T531" id="Seg_5629" s="T530">tu-lʼčʼe-ıː-mpa</ta>
            <ta e="T532" id="Seg_5630" s="T531">čʼəssä</ta>
            <ta e="T533" id="Seg_5631" s="T532">läqa</ta>
            <ta e="T534" id="Seg_5632" s="T533">təp-ɨ-n</ta>
            <ta e="T535" id="Seg_5633" s="T534">muːt-tɨl</ta>
            <ta e="T536" id="Seg_5634" s="T535">läːj-ätɔːl-ta</ta>
            <ta e="T537" id="Seg_5635" s="T536">lʼam-k-alte-nta</ta>
            <ta e="T538" id="Seg_5636" s="T537">lüčʼi-mpɨ-la</ta>
            <ta e="T539" id="Seg_5637" s="T538">ɔːnt-al-pɨ-la</ta>
            <ta e="T540" id="Seg_5638" s="T539">vorota-mɨt</ta>
            <ta e="T541" id="Seg_5639" s="T540">təp</ta>
            <ta e="T542" id="Seg_5640" s="T541">na</ta>
            <ta e="T543" id="Seg_5641" s="T542">seːra</ta>
            <ta e="T544" id="Seg_5642" s="T543">etɨmanto-t</ta>
            <ta e="T545" id="Seg_5643" s="T544">lʼamɨ-k</ta>
            <ta e="T546" id="Seg_5644" s="T545">ɛː-nʼa</ta>
            <ta e="T547" id="Seg_5645" s="T546">läqa</ta>
            <ta e="T548" id="Seg_5646" s="T547">ɔːnt-al-pɨ-lʼa</ta>
            <ta e="T549" id="Seg_5647" s="T548">nʼoː-nʼɨ-t</ta>
            <ta e="T550" id="Seg_5648" s="T549">a</ta>
            <ta e="T551" id="Seg_5649" s="T550">sar-ɨ-t</ta>
            <ta e="T552" id="Seg_5650" s="T551">nälʼa</ta>
            <ta e="T553" id="Seg_5651" s="T552">kurɨ-te-lʼa</ta>
            <ta e="T554" id="Seg_5652" s="T553">krɨlʼco-t</ta>
            <ta e="T555" id="Seg_5653" s="T554">iː-ntɨ</ta>
            <ta e="T556" id="Seg_5654" s="T555">sɨːqɨle-nta</ta>
            <ta e="T557" id="Seg_5655" s="T556">pürɨ-j</ta>
            <ta e="T558" id="Seg_5656" s="T557">utoː-qɨn-tɨ</ta>
            <ta e="T559" id="Seg_5657" s="T558">iː-ntɨ-t</ta>
            <ta e="T560" id="Seg_5658" s="T559">mɔːta-t</ta>
            <ta e="T561" id="Seg_5659" s="T560">čʼontɨ-ka</ta>
            <ta e="T562" id="Seg_5660" s="T561">na</ta>
            <ta e="T563" id="Seg_5661" s="T562">nʼüː-ntɨ-t</ta>
            <ta e="T564" id="Seg_5662" s="T563">mɔːt-tɨ</ta>
            <ta e="T565" id="Seg_5663" s="T564">čʼontɨ-ka</ta>
            <ta e="T566" id="Seg_5664" s="T565">na</ta>
            <ta e="T567" id="Seg_5665" s="T566">seːra</ta>
            <ta e="T568" id="Seg_5666" s="T567">qo-ne-mpa</ta>
            <ta e="T569" id="Seg_5667" s="T568">qo-nnɨ-mpa</ta>
            <ta e="T570" id="Seg_5668" s="T569">qo-nnä-nta</ta>
            <ta e="T571" id="Seg_5669" s="T570">kover-sa</ta>
            <ta e="T572" id="Seg_5670" s="T571">lavka-t</ta>
            <ta e="T573" id="Seg_5671" s="T572">ikona-t</ta>
            <ta e="T574" id="Seg_5672" s="T573">ıl-qɨt</ta>
            <ta e="T575" id="Seg_5673" s="T574">dup-ɨ-lʼ</ta>
            <ta e="T576" id="Seg_5674" s="T575">lem</ta>
            <ta e="T577" id="Seg_5675" s="T576">nəkɨr-pɨlʼ</ta>
            <ta e="T578" id="Seg_5676" s="T577">ippɨ-psa-sa</ta>
            <ta e="T579" id="Seg_5677" s="T578">šoːqɨr</ta>
            <ta e="T580" id="Seg_5678" s="T579">nälʼ-qup</ta>
            <ta e="T581" id="Seg_5679" s="T580">mannɨ-mpa-tɨ</ta>
            <ta e="T582" id="Seg_5680" s="T581">tɨmtɨ</ta>
            <ta e="T583" id="Seg_5681" s="T582">qɔːtɨ</ta>
            <ta e="T584" id="Seg_5682" s="T583">soma</ta>
            <ta e="T585" id="Seg_5683" s="T584">qum-ɨ-t</ta>
            <ta e="T586" id="Seg_5684" s="T585">ilʼɔː-t</ta>
            <ta e="T587" id="Seg_5685" s="T586">tɛp-ɨ-n</ta>
            <ta e="T588" id="Seg_5686" s="T587">aša</ta>
            <ta e="T589" id="Seg_5687" s="T588">toːte-lʼ-ančʼɔː-t</ta>
            <ta e="T590" id="Seg_5688" s="T589">nɔːt</ta>
            <ta e="T591" id="Seg_5689" s="T590">sitɨ kota-t</ta>
            <ta e="T592" id="Seg_5690" s="T591">ni</ta>
            <ta e="T593" id="Seg_5691" s="T592">kutɨ</ta>
            <ta e="T594" id="Seg_5692" s="T593">aš</ta>
            <ta e="T595" id="Seg_5693" s="T594">ata</ta>
            <ta e="T596" id="Seg_5694" s="T595">mɔːt-ɨ-p</ta>
            <ta e="T597" id="Seg_5695" s="T596">tu-mɨt</ta>
            <ta e="T598" id="Seg_5696" s="T597">kolʼa-ltɨ-ntɨ-t</ta>
            <ta e="T599" id="Seg_5697" s="T598">muntok</ta>
            <ta e="T600" id="Seg_5698" s="T599">soma-k</ta>
            <ta e="T601" id="Seg_5699" s="T600">täš-altɨ-s-tɨ</ta>
            <ta e="T602" id="Seg_5700" s="T601">ikona-t</ta>
            <ta e="T603" id="Seg_5701" s="T602">sečʼčʼi-p</ta>
            <ta e="T604" id="Seg_5702" s="T603">ačʼ-altɨ-ntɨ-t</ta>
            <ta e="T605" id="Seg_5703" s="T604">šoːqɨr-ɨ-p</ta>
            <ta e="T606" id="Seg_5704" s="T605">pötpɨl-la</ta>
            <ta e="T607" id="Seg_5705" s="T606">čʼetɨ-ntɨ-tɨ</ta>
            <ta e="T608" id="Seg_5706" s="T607">polati-t</ta>
            <ta e="T609" id="Seg_5707" s="T608">iː-ntɨ</ta>
            <ta e="T610" id="Seg_5708" s="T609">sɨːqɨle-lʼa</ta>
            <ta e="T611" id="Seg_5709" s="T610">lʼamɨ-k</ta>
            <ta e="T612" id="Seg_5710" s="T611">ılla</ta>
            <ta e="T613" id="Seg_5711" s="T612">olʼčʼe-ıː-nta</ta>
            <ta e="T614" id="Seg_5712" s="T613">čʼeːlɨ-t</ta>
            <ta e="T615" id="Seg_5713" s="T614">čʼonno-nt</ta>
            <ta e="T616" id="Seg_5714" s="T615">tətčʼe</ta>
            <ta e="T617" id="Seg_5715" s="T616">ɛː-sa</ta>
            <ta e="T618" id="Seg_5716" s="T617">sümɨ</ta>
            <ta e="T619" id="Seg_5717" s="T618">unt-al-ɨ-nta</ta>
            <ta e="T620" id="Seg_5718" s="T619">poː-qɨt</ta>
            <ta e="T621" id="Seg_5719" s="T620">seːlʼčʼi</ta>
            <ta e="T622" id="Seg_5720" s="T621">mɔːtɨr-ɨ-n</ta>
            <ta e="T623" id="Seg_5721" s="T622">na</ta>
            <ta e="T624" id="Seg_5722" s="T623">seːrɔː-t</ta>
            <ta e="T625" id="Seg_5723" s="T624">seːlʼčʼi-t</ta>
            <ta e="T626" id="Seg_5724" s="T625">muntɨk</ta>
            <ta e="T627" id="Seg_5725" s="T626">untɨlʼ uːkɨ-k</ta>
            <ta e="T628" id="Seg_5726" s="T627">mərqɨ</ta>
            <ta e="T629" id="Seg_5727" s="T628">neː-tɨt</ta>
            <ta e="T630" id="Seg_5728" s="T629">nılʼčʼi-k</ta>
            <ta e="T631" id="Seg_5729" s="T630">ɛsa</ta>
            <ta e="T632" id="Seg_5730" s="T631">Na</ta>
            <ta e="T633" id="Seg_5731" s="T632">lʼa</ta>
            <ta e="T634" id="Seg_5732" s="T633">qaj</ta>
            <ta e="T635" id="Seg_5733" s="T634">ɛsʼ</ta>
            <ta e="T636" id="Seg_5734" s="T635">kutar</ta>
            <ta e="T637" id="Seg_5735" s="T636">some</ta>
            <ta e="T638" id="Seg_5736" s="T637">kutar</ta>
            <ta e="T639" id="Seg_5737" s="T638">muntɨk</ta>
            <ta e="T640" id="Seg_5738" s="T639">kuras-s</ta>
            <ta e="T641" id="Seg_5739" s="T640">ɛː-nʼa</ta>
            <ta e="T642" id="Seg_5740" s="T641">kutɨ</ta>
            <ta e="T643" id="Seg_5741" s="T642">qos</ta>
            <ta e="T644" id="Seg_5742" s="T643">mɔːn-mɨt</ta>
            <ta e="T645" id="Seg_5743" s="T644">təšanʼ-pa-tɨ</ta>
            <ta e="T646" id="Seg_5744" s="T645">mitɨ</ta>
            <ta e="T647" id="Seg_5745" s="T646">meːšımɨt</ta>
            <ta e="T648" id="Seg_5746" s="T647">əta-kkɔː-tɨj</ta>
            <ta e="T649" id="Seg_5747" s="T648">ätɨ-mpɔː-tɨn</ta>
            <ta e="T650" id="Seg_5748" s="T649">kuto-ntʼ</ta>
            <ta e="T651" id="Seg_5749" s="T650">tant-aš</ta>
            <ta e="T652" id="Seg_5750" s="T651">atɨ-lt-ašik</ta>
            <ta e="T653" id="Seg_5751" s="T652">meːšım</ta>
            <ta e="T654" id="Seg_5752" s="T653">toː-nɨ</ta>
            <ta e="T655" id="Seg_5753" s="T654">qota-k</ta>
            <ta e="T656" id="Seg_5754" s="T655">čʼɛːt-äš</ta>
            <ta e="T657" id="Seg_5755" s="T656">qatamol</ta>
            <ta e="T658" id="Seg_5756" s="T657">tan</ta>
            <ta e="T659" id="Seg_5757" s="T658">mɛrqɨ</ta>
            <ta e="T660" id="Seg_5758" s="T659">qum-o-nt</ta>
            <ta e="T661" id="Seg_5759" s="T660">ilʼčʼa-nɨt-qo</ta>
            <ta e="T662" id="Seg_5760" s="T661">meːltɨ</ta>
            <ta e="T663" id="Seg_5761" s="T662">ɛː-nna-nt</ta>
            <ta e="T664" id="Seg_5762" s="T663">qatamol</ta>
            <ta e="T665" id="Seg_5763" s="T664">tan</ta>
            <ta e="T666" id="Seg_5764" s="T665">ilʼmat</ta>
            <ta e="T667" id="Seg_5765" s="T666">nʼoː-ntɨ</ta>
            <ta e="T668" id="Seg_5766" s="T667">meːqin</ta>
            <ta e="T669" id="Seg_5767" s="T668">čʼopa-nɨt-qo</ta>
            <ta e="T670" id="Seg_5768" s="T669">ɛː-nna-t</ta>
            <ta e="T671" id="Seg_5769" s="T670">qatamol</ta>
            <ta e="T672" id="Seg_5770" s="T671">tan</ta>
            <ta e="T673" id="Seg_5771" s="T672">imaqota</ta>
            <ta e="T674" id="Seg_5772" s="T673">qɛrʼ-antɨ-mɨt</ta>
            <ta e="T675" id="Seg_5773" s="T674">ämɨ-nɨt-qo</ta>
            <ta e="T676" id="Seg_5774" s="T675">qatamol</ta>
            <ta e="T677" id="Seg_5775" s="T676">tan</ta>
            <ta e="T678" id="Seg_5776" s="T677">nälʼ</ta>
            <ta e="T679" id="Seg_5777" s="T678">qum-o-ntɨ</ta>
            <ta e="T680" id="Seg_5778" s="T679">meːqin</ta>
            <ta e="T681" id="Seg_5779" s="T680">nʼenʼnʼa-nɨt-qo</ta>
            <ta e="T682" id="Seg_5780" s="T681">ɛː-nna-t</ta>
            <ta e="T683" id="Seg_5781" s="T682">sar-ɨ-t</ta>
            <ta e="T684" id="Seg_5782" s="T683">nälʼa</ta>
            <ta e="T685" id="Seg_5783" s="T684">ılla</ta>
            <ta e="T686" id="Seg_5784" s="T685">pan-čʼa</ta>
            <ta e="T687" id="Seg_5785" s="T686">ılʼlʼɨ-mpa-nɨ</ta>
            <ta e="T688" id="Seg_5786" s="T687">torowatɨ-nt</ta>
            <ta e="T689" id="Seg_5787" s="T688">təp-ɨ-t-ɨ-sa</ta>
            <ta e="T690" id="Seg_5788" s="T689">čʼüː-n-tɨt</ta>
            <ta e="T691" id="Seg_5789" s="T690">tättɨ</ta>
            <ta e="T692" id="Seg_5790" s="T691">mušq-aıː-tɨ-la</ta>
            <ta e="T693" id="Seg_5791" s="T692">nʼuːtɨ</ta>
            <ta e="T694" id="Seg_5792" s="T693">sočʼ</ta>
            <ta e="T695" id="Seg_5793" s="T694">nʼer-ta-r-ɨ-s</ta>
            <ta e="T696" id="Seg_5794" s="T695">nı-lʼ</ta>
            <ta e="T697" id="Seg_5795" s="T696">ɛsɨ-t</ta>
            <ta e="T698" id="Seg_5796" s="T697">mɔːt-qɨn-tit</ta>
            <ta e="T699" id="Seg_5797" s="T698">mol</ta>
            <ta e="T700" id="Seg_5798" s="T699">seːr-sa-k</ta>
            <ta e="T701" id="Seg_5799" s="T700">mat</ta>
            <ta e="T702" id="Seg_5800" s="T701">tiː</ta>
            <ta e="T703" id="Seg_5801" s="T702">qos</ta>
            <ta e="T704" id="Seg_5802" s="T703">sıt</ta>
            <ta e="T705" id="Seg_5803" s="T704">qɛrɨ-ptä-lɨt</ta>
            <ta e="T706" id="Seg_5804" s="T705">čʼäːnʼa</ta>
            <ta e="T707" id="Seg_5805" s="T706">nɨːnɨ</ta>
            <ta e="T708" id="Seg_5806" s="T707">ɛːto-mɨn-tɨ</ta>
            <ta e="T709" id="Seg_5807" s="T708">qə-ntɔː-tɨt</ta>
            <ta e="T710" id="Seg_5808" s="T709">tam</ta>
            <ta e="T711" id="Seg_5809" s="T710">aš</ta>
            <ta e="T712" id="Seg_5810" s="T711">sar-ɨ-t</ta>
            <ta e="T713" id="Seg_5811" s="T712">nälʼa</ta>
            <ta e="T714" id="Seg_5812" s="T713">ɛː-sa</ta>
            <ta e="T715" id="Seg_5813" s="T714">sänʼä-lʼa-ntɨ</ta>
            <ta e="T716" id="Seg_5814" s="T715">omtɨ-ltɨ-sɔː-t</ta>
            <ta e="T717" id="Seg_5815" s="T716">nʼanʼ-i-lʼa-sa</ta>
            <ta e="T718" id="Seg_5816" s="T717">nʼänʼ-i-lʼä-sa</ta>
            <ta e="T719" id="Seg_5817" s="T718">mi-lʼčʼe-čʼe-sɔː-t</ta>
            <ta e="T720" id="Seg_5818" s="T719">rʼumka-j</ta>
            <ta e="T721" id="Seg_5819" s="T720">tıːr-la</ta>
            <ta e="T722" id="Seg_5820" s="T721">qam-te-čʼe-sɔː-t</ta>
            <ta e="T723" id="Seg_5821" s="T722">ili-t</ta>
            <ta e="T724" id="Seg_5822" s="T723">iː-qɨt</ta>
            <ta e="T725" id="Seg_5823" s="T724">taːtä-čʼe-sɔː-tɨ</ta>
            <ta e="T726" id="Seg_5824" s="T725">tap-ɨ-lʼ</ta>
            <ta e="T727" id="Seg_5825" s="T726">ut-ɨ-p</ta>
            <ta e="T728" id="Seg_5826" s="T727">aša</ta>
            <ta e="T729" id="Seg_5827" s="T728">iː-s-tɨ</ta>
            <ta e="T730" id="Seg_5828" s="T729">nʼanʼ-i-lʼa-p</ta>
            <ta e="T731" id="Seg_5829" s="T730">kes</ta>
            <ta e="T732" id="Seg_5830" s="T731">sepɨ-te-la</ta>
            <ta e="T733" id="Seg_5831" s="T732">kɨpɛ</ta>
            <ta e="T734" id="Seg_5832" s="T733">lıːpɨ-m-t</ta>
            <ta e="T735" id="Seg_5833" s="T734">ɔːk-ɨ-nt</ta>
            <ta e="T736" id="Seg_5834" s="T735">iː-s-tɨ</ta>
            <ta e="T737" id="Seg_5835" s="T736">wɛtɨ-n</ta>
            <ta e="T738" id="Seg_5836" s="T737">nɔː-nɨ</ta>
            <ta e="T739" id="Seg_5837" s="T738">nʼɨš-qɨntoːqo</ta>
            <ta e="T740" id="Seg_5838" s="T739">koptɨ-tqo</ta>
            <ta e="T741" id="Seg_5839" s="T740">no</ta>
            <ta e="T742" id="Seg_5840" s="T741">mɔːtɨ-r-a-na</ta>
            <ta e="T743" id="Seg_5841" s="T742">qɛn-tɨ-sɔː-tɨt</ta>
            <ta e="T744" id="Seg_5842" s="T743">tɛp-ɨ-t</ta>
            <ta e="T745" id="Seg_5843" s="T744">näta-p</ta>
            <ta e="T746" id="Seg_5844" s="T745">ınna</ta>
            <ta e="T747" id="Seg_5845" s="T746">čʼeːlɨ-nʼ-pɨ-tɨlʼ</ta>
            <ta e="T748" id="Seg_5846" s="T747">sünnö-t</ta>
            <ta e="T749" id="Seg_5847" s="T748">aj</ta>
            <ta e="T750" id="Seg_5848" s="T749">ontɨ-qɨt</ta>
            <ta e="T751" id="Seg_5849" s="T750">na</ta>
            <ta e="T752" id="Seg_5850" s="T751">nɨmtɨ</ta>
            <ta e="T753" id="Seg_5851" s="T752">qəːčʼɨ-ntɔː-tɨt</ta>
            <ta e="T754" id="Seg_5852" s="T753">qəːčʼɨ-sɔː-tɨt</ta>
            <ta e="T755" id="Seg_5853" s="T754">təp-ɨ-p</ta>
            <ta e="T756" id="Seg_5854" s="T755">ɔːŋkɨ-ti-tɨl</ta>
            <ta e="T757" id="Seg_5855" s="T756">pɛlɨ-kɔːlɨ-k</ta>
            <ta e="T759" id="Seg_5856" s="T758">nʼarqɨ</ta>
            <ta e="T760" id="Seg_5857" s="T759">čʼeːlo-n</ta>
            <ta e="T761" id="Seg_5858" s="T760">čʼeːlʼi</ta>
            <ta e="T762" id="Seg_5859" s="T761">moːro-qɨn-tɨ</ta>
            <ta e="T763" id="Seg_5860" s="T762">ilʼmat</ta>
            <ta e="T764" id="Seg_5861" s="T763">kor-altɨ-p-sa</ta>
            <ta e="T765" id="Seg_5862" s="T764">čʼeːlɨ-nʼ-pɨ-ta</ta>
            <ta e="T766" id="Seg_5863" s="T765">čʼeːlɨ-mɨt</ta>
            <ta e="T767" id="Seg_5864" s="T766">meː</ta>
            <ta e="T768" id="Seg_5865" s="T767">tan</ta>
            <ta e="T769" id="Seg_5866" s="T768">kara-nʼ-nʼa-ntɨ</ta>
            <ta e="T770" id="Seg_5867" s="T769">kuto-r-na-ntɨ</ta>
            <ta e="T771" id="Seg_5868" s="T770">poː-n-tɨ</ta>
            <ta e="T772" id="Seg_5869" s="T771">kuntɨ</ta>
            <ta e="T773" id="Seg_5870" s="T772">nuː-t</ta>
            <ta e="T774" id="Seg_5871" s="T773">nekmɨ</ta>
            <ta e="T775" id="Seg_5872" s="T774">tot-altu-kka-l</ta>
            <ta e="T776" id="Seg_5873" s="T775">kə-p</ta>
            <ta e="T777" id="Seg_5874" s="T776">petpɨlʼ</ta>
            <ta e="T778" id="Seg_5875" s="T777">utɨ-sa</ta>
            <ta e="T779" id="Seg_5876" s="T778">mutok-t</ta>
            <ta e="T780" id="Seg_5877" s="T779">meːsım</ta>
            <ta e="T781" id="Seg_5878" s="T780">onant</ta>
            <ta e="T782" id="Seg_5879" s="T781">ılqɨt</ta>
            <ta e="T783" id="Seg_5880" s="T782">qo-ntɨr-na-ntɨ</ta>
            <ta e="T784" id="Seg_5881" s="T783">montɨ</ta>
            <ta e="T785" id="Seg_5882" s="T784">əːto-p</ta>
            <ta e="T786" id="Seg_5883" s="T785">toː</ta>
            <ta e="T787" id="Seg_5884" s="T786">čʼat-anta-l</ta>
            <ta e="T788" id="Seg_5885" s="T787">qo-ntɨr-sa-l</ta>
            <ta e="T789" id="Seg_5886" s="T788">qaj</ta>
            <ta e="T790" id="Seg_5887" s="T789">kun</ta>
            <ta e="T791" id="Seg_5888" s="T790">ɛːm</ta>
            <ta e="T792" id="Seg_5889" s="T791">təto-t</ta>
            <ta e="T793" id="Seg_5890" s="T792">sar-ɨ-t</ta>
            <ta e="T794" id="Seg_5891" s="T793">nälʼa-p</ta>
            <ta e="T795" id="Seg_5892" s="T794">ilʼmatɨlʼ</ta>
            <ta e="T796" id="Seg_5893" s="T795">tan</ta>
            <ta e="T798" id="Seg_5894" s="T797">irä-t</ta>
            <ta e="T799" id="Seg_5895" s="T798">irä-t</ta>
            <ta e="T800" id="Seg_5896" s="T799">onak</ta>
            <ta e="T801" id="Seg_5897" s="T800">neː-mɨ</ta>
            <ta e="T802" id="Seg_5898" s="T801">zoločʼenɨ-lʼ</ta>
            <ta e="T803" id="Seg_5899" s="T802">ɔːttɨ-lʼa</ta>
            <ta e="T804" id="Seg_5900" s="T803">məši-kka-ntɨ</ta>
            <ta e="T805" id="Seg_5901" s="T804">kore-l</ta>
            <ta e="T806" id="Seg_5902" s="T805">pi-t</ta>
            <ta e="T807" id="Seg_5903" s="T806">tan</ta>
            <ta e="T808" id="Seg_5904" s="T807">pürɨ-lʼ</ta>
            <ta e="T809" id="Seg_5905" s="T808">mɛntɨ-lʼ</ta>
            <ta e="T810" id="Seg_5906" s="T809">čʼeːlɨ-lʼ</ta>
            <ta e="T811" id="Seg_5907" s="T810">sai-lʼ</ta>
            <ta e="T812" id="Seg_5908" s="T811">aj</ta>
            <ta e="T813" id="Seg_5909" s="T812">mat</ta>
            <ta e="T814" id="Seg_5910" s="T813">serlɨ</ta>
            <ta e="T815" id="Seg_5911" s="T814">kɨkɨ-la</ta>
            <ta e="T816" id="Seg_5912" s="T815">qıšqa-t</ta>
            <ta e="T817" id="Seg_5913" s="T816">tantɨ</ta>
            <ta e="T818" id="Seg_5914" s="T817">mannɨ-mpɔː-tɨt</ta>
            <ta e="T820" id="Seg_5915" s="T819">näčʼä-t</ta>
            <ta e="T821" id="Seg_5916" s="T820">čʼontɨ</ta>
            <ta e="T822" id="Seg_5917" s="T821">künɨ-lʼ</ta>
            <ta e="T823" id="Seg_5918" s="T822">kɨ-qat</ta>
            <ta e="T824" id="Seg_5919" s="T823">toap</ta>
            <ta e="T825" id="Seg_5920" s="T824">ɛː-nʼa</ta>
            <ta e="T826" id="Seg_5921" s="T825">pirqɨ</ta>
            <ta e="T827" id="Seg_5922" s="T826">toanʼ</ta>
            <ta e="T828" id="Seg_5923" s="T827">nɨmtɨ</ta>
            <ta e="T829" id="Seg_5924" s="T828">korɨ</ta>
            <ta e="T830" id="Seg_5925" s="T829">mü</ta>
            <ta e="T831" id="Seg_5926" s="T830">pi-tɨ</ta>
            <ta e="T832" id="Seg_5927" s="T831">matɨ</ta>
            <ta e="T833" id="Seg_5928" s="T832">tətɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5929" s="T0">caːr</ta>
            <ta e="T2" id="Seg_5930" s="T1">ima-ntɨ-sä</ta>
            <ta e="T3" id="Seg_5931" s="T2">nıːtɨ-r-sɨ-tɨ</ta>
            <ta e="T4" id="Seg_5932" s="T3">nəkɨ-r-sɨ</ta>
            <ta e="T5" id="Seg_5933" s="T4">kuntak</ta>
            <ta e="T6" id="Seg_5934" s="T5">wəttɨ-ntɨ</ta>
            <ta e="T7" id="Seg_5935" s="T6">*taqtɨ-ltɨ-ŋɨ</ta>
            <ta e="T8" id="Seg_5936" s="T7">okno-n</ta>
            <ta e="T9" id="Seg_5937" s="T8">qan-ntɨ</ta>
            <ta e="T10" id="Seg_5938" s="T9">ima-tɨ</ta>
            <ta e="T11" id="Seg_5939" s="T10">təp-ɨ-m</ta>
            <ta e="T12" id="Seg_5940" s="T11">ətɨ-qo</ta>
            <ta e="T13" id="Seg_5941" s="T12">ontɨ</ta>
            <ta e="T14" id="Seg_5942" s="T13">omtɨ</ta>
            <ta e="T15" id="Seg_5943" s="T14">qarɨ-n</ta>
            <ta e="T16" id="Seg_5944" s="T15">*nɔː-nɨ</ta>
            <ta e="T17" id="Seg_5945" s="T16">üːtɨ-n</ta>
            <ta e="T18" id="Seg_5946" s="T17">təttɨ</ta>
            <ta e="T19" id="Seg_5947" s="T18">ətɨ-kkɨ-tɨ</ta>
            <ta e="T20" id="Seg_5948" s="T19">ətɨ-kkɨ-tɨ</ta>
            <ta e="T21" id="Seg_5949" s="T20">meːltɨ</ta>
            <ta e="T22" id="Seg_5950" s="T21">limɨ-ntɨ</ta>
            <ta e="T23" id="Seg_5951" s="T22">mantɨ-mpɨ</ta>
            <ta e="T24" id="Seg_5952" s="T23">təp</ta>
            <ta e="T25" id="Seg_5953" s="T24">sajɨ-lʼ</ta>
            <ta e="T26" id="Seg_5954" s="T25">tɔːŋ-tɨ</ta>
            <ta e="T27" id="Seg_5955" s="T26">mantɨ-alʼ-mpɨ-lä</ta>
            <ta e="T28" id="Seg_5956" s="T27">čʼüšɨ-lɨ-sɨ-tɨt</ta>
            <ta e="T29" id="Seg_5957" s="T28">kutɨ-lʼ</ta>
            <ta e="T30" id="Seg_5958" s="T29">ɛːmä-lʼ</ta>
            <ta e="T31" id="Seg_5959" s="T30">qarɨ-lʼ</ta>
            <ta e="T32" id="Seg_5960" s="T31">kəntɨ-n-tɨ</ta>
            <ta e="T33" id="Seg_5961" s="T32">uːkɨ-qɨnɨ</ta>
            <ta e="T34" id="Seg_5962" s="T33">pi-n</ta>
            <ta e="T35" id="Seg_5963" s="T34">təttɨ</ta>
            <ta e="T36" id="Seg_5964" s="T35">ɔːmɨ</ta>
            <ta e="T37" id="Seg_5965" s="T36">neː-tɨ</ta>
            <ta e="T38" id="Seg_5966" s="T37">ašša</ta>
            <ta e="T39" id="Seg_5967" s="T38">atɨ</ta>
            <ta e="T40" id="Seg_5968" s="T39">kekkɨsä</ta>
            <ta e="T41" id="Seg_5969" s="T40">təp</ta>
            <ta e="T42" id="Seg_5970" s="T41">mantɨ-mpɨ-tɨ</ta>
            <ta e="T43" id="Seg_5971" s="T42">mɛrkɨ-lʼ</ta>
            <ta e="T44" id="Seg_5972" s="T43">palʼčʼa</ta>
            <ta e="T45" id="Seg_5973" s="T44">küčʼɨnɨ-mpɨ</ta>
            <ta e="T46" id="Seg_5974" s="T45">sɨrɨ</ta>
            <ta e="T47" id="Seg_5975" s="T46">čʼom-ŋɨ</ta>
            <ta e="T48" id="Seg_5976" s="T47">limɨ-n</ta>
            <ta e="T49" id="Seg_5977" s="T48">*iː-ntɨ</ta>
            <ta e="T50" id="Seg_5978" s="T49">təttɨ</ta>
            <ta e="T51" id="Seg_5979" s="T50">muntɨk</ta>
            <ta e="T52" id="Seg_5980" s="T51">sərɨ</ta>
            <ta e="T53" id="Seg_5981" s="T52">ɛsɨ</ta>
            <ta e="T54" id="Seg_5982" s="T53">qən-ŋɨ</ta>
            <ta e="T55" id="Seg_5983" s="T54">qən-ŋɨ</ta>
            <ta e="T56" id="Seg_5984" s="T55">ukkɨr</ta>
            <ta e="T57" id="Seg_5985" s="T56">čʼäː</ta>
            <ta e="T58" id="Seg_5986" s="T57">köt</ta>
            <ta e="T59" id="Seg_5987" s="T58">irä-tɨ</ta>
            <ta e="T60" id="Seg_5988" s="T59">limɨ-qɨn</ta>
            <ta e="T61" id="Seg_5989" s="T60">sajɨ-m-tɨ</ta>
            <ta e="T62" id="Seg_5990" s="T61">älpä</ta>
            <ta e="T63" id="Seg_5991" s="T62">ašša</ta>
            <ta e="T64" id="Seg_5992" s="T63">iː-tɨ-tɨ</ta>
            <ta e="T65" id="Seg_5993" s="T64">iː-tɨ-ŋɨ-tɨ</ta>
            <ta e="T66" id="Seg_5994" s="T65">nɔːtɨ</ta>
            <ta e="T67" id="Seg_5995" s="T66">sočʼelʼnik-ɨ-qɨn</ta>
            <ta e="T68" id="Seg_5996" s="T67">pi-n</ta>
            <ta e="T69" id="Seg_5997" s="T68">təp-ɨ-n</ta>
            <ta e="T70" id="Seg_5998" s="T69">mi-ŋɨ-tɨ</ta>
            <ta e="T71" id="Seg_5999" s="T70">nälʼa-m</ta>
            <ta e="T72" id="Seg_6000" s="T71">nälʼa-m-tɨ</ta>
            <ta e="T73" id="Seg_6001" s="T72">nom</ta>
            <ta e="T74" id="Seg_6002" s="T73">nom-tɨ</ta>
            <ta e="T75" id="Seg_6003" s="T74">rɛmɨkɨ</ta>
            <ta e="T76" id="Seg_6004" s="T75">qarɨ-ntɨ</ta>
            <ta e="T77" id="Seg_6005" s="T76">nɔːtna-lʼ</ta>
            <ta e="T78" id="Seg_6006" s="T77">gostʼ</ta>
            <ta e="T79" id="Seg_6007" s="T78">pi-sä</ta>
            <ta e="T80" id="Seg_6008" s="T79">čʼeːlɨ-sä</ta>
            <ta e="T81" id="Seg_6009" s="T80">kuntɨ</ta>
            <ta e="T82" id="Seg_6010" s="T81">ətɨ-mpɨlʼ</ta>
            <ta e="T83" id="Seg_6011" s="T82">kuntak-qɨnɨ</ta>
            <ta e="T84" id="Seg_6012" s="T83">nɔːtɨ</ta>
            <ta e="T85" id="Seg_6013" s="T84">qən-š-ɨ-sɨ</ta>
            <ta e="T86" id="Seg_6014" s="T85">mɔːt-qɨn</ta>
            <ta e="T87" id="Seg_6015" s="T86">tü-sɨ</ta>
            <ta e="T88" id="Seg_6016" s="T87">caːr-əsɨ-tɨ</ta>
            <ta e="T89" id="Seg_6017" s="T88">mantɨ-ɛčʼɨ-kkɨ-sɨ</ta>
            <ta e="T90" id="Seg_6018" s="T89">təp-ɨ-nkinı</ta>
            <ta e="T91" id="Seg_6019" s="T90">səttɨm-ɨ-lʼa</ta>
            <ta e="T92" id="Seg_6020" s="T91">kəšqɨlɛj-sɨ</ta>
            <ta e="T93" id="Seg_6021" s="T92">ɔːntɨ-m-tɨ</ta>
            <ta e="T94" id="Seg_6022" s="T93">ašša</ta>
            <ta e="T95" id="Seg_6023" s="T94">seːpɨ-r-ɨ-lɨ-sɨ-tɨ</ta>
            <ta e="T96" id="Seg_6024" s="T95">seːpɨ-r-sɨ</ta>
            <ta e="T97" id="Seg_6025" s="T96">aj</ta>
            <ta e="T98" id="Seg_6026" s="T97">čʼeːlɨ</ta>
            <ta e="T99" id="Seg_6027" s="T98">čʼontɨ-n</ta>
            <ta e="T100" id="Seg_6028" s="T99">ürɨ-ɛː-čʼɨ-sɨ</ta>
            <ta e="T101" id="Seg_6029" s="T100">kekkɨ-alʼ-mpɨ-sɨ</ta>
            <ta e="T102" id="Seg_6030" s="T101">näkä-lɨ-mpɨ-sɨ</ta>
            <ta e="T103" id="Seg_6031" s="T102">kuntɨ</ta>
            <ta e="T104" id="Seg_6032" s="T103">caːr</ta>
            <ta e="T105" id="Seg_6033" s="T104">kuttar</ta>
            <ta e="T106" id="Seg_6034" s="T105">ɛː-qo</ta>
            <ta e="T107" id="Seg_6035" s="T106">grešnɨk</ta>
            <ta e="T108" id="Seg_6036" s="T107">ɛː-mpɨ</ta>
            <ta e="T109" id="Seg_6037" s="T108">poː-tɨ</ta>
            <ta e="T110" id="Seg_6038" s="T109">qən-ŋɨ</ta>
            <ta e="T111" id="Seg_6039" s="T110">qən-ŋɨ</ta>
            <ta e="T112" id="Seg_6040" s="T111">mitɨ</ta>
            <ta e="T113" id="Seg_6041" s="T112">ɔːŋkɨ</ta>
            <ta e="T114" id="Seg_6042" s="T113">mənɨlʼ</ta>
            <ta e="T115" id="Seg_6043" s="T114">mənɨlʼ</ta>
            <ta e="T116" id="Seg_6044" s="T115">ima-m</ta>
            <ta e="T117" id="Seg_6045" s="T116">iː-ŋɨ-tɨ</ta>
            <ta e="T118" id="Seg_6046" s="T117">caːr</ta>
            <ta e="T119" id="Seg_6047" s="T118">kuttar</ta>
            <ta e="T120" id="Seg_6048" s="T119">kətɨ-qo</ta>
            <ta e="T121" id="Seg_6049" s="T120">nälʼa-qum</ta>
            <ta e="T122" id="Seg_6050" s="T121">ɛː-sɨ</ta>
            <ta e="T123" id="Seg_6051" s="T122">ontɨ-ɨ-lʼ</ta>
            <ta e="T124" id="Seg_6052" s="T123">mitɨ</ta>
            <ta e="T125" id="Seg_6053" s="T124">caːr-ɨ-lʼ</ta>
            <ta e="T126" id="Seg_6054" s="T125">ima</ta>
            <ta e="T127" id="Seg_6055" s="T126">pirqɨ</ta>
            <ta e="T128" id="Seg_6056" s="T127">tɔːšan</ta>
            <ta e="T129" id="Seg_6057" s="T128">sərɨ</ta>
            <ta e="T130" id="Seg_6058" s="T129">ɛː-sɨ</ta>
            <ta e="T131" id="Seg_6059" s="T130">tɛnɨ-lʼ</ta>
            <ta e="T132" id="Seg_6060" s="T131">tɔːŋ-sä</ta>
            <ta e="T133" id="Seg_6061" s="T132">muntɨk-n</ta>
            <ta e="T134" id="Seg_6062" s="T133">iː-sɨ-tɨ</ta>
            <ta e="T135" id="Seg_6063" s="T134">nɨːnɨ</ta>
            <ta e="T136" id="Seg_6064" s="T135">aj</ta>
            <ta e="T137" id="Seg_6065" s="T136">nɔːtɨ</ta>
            <ta e="T138" id="Seg_6066" s="T137">soma-lʼčʼɨ-mtɨ-tɨ</ta>
            <ta e="T139" id="Seg_6067" s="T138">qɔːtɨ-olä</ta>
            <ta e="T140" id="Seg_6068" s="T139">nɔːtɨ</ta>
            <ta e="T141" id="Seg_6069" s="T140">jarɨk</ta>
            <ta e="T142" id="Seg_6070" s="T141">ɛsɨ</ta>
            <ta e="T143" id="Seg_6071" s="T142">mɔːt-tɨ</ta>
            <ta e="T144" id="Seg_6072" s="T143">*nɔː-nɨ</ta>
            <ta e="T145" id="Seg_6073" s="T144">taːtɨ-mpɨ-sɨ-tɨ</ta>
            <ta e="T146" id="Seg_6074" s="T145">kekkɨsä</ta>
            <ta e="T147" id="Seg_6075" s="T146">zerkala-lʼa-m</ta>
            <ta e="T148" id="Seg_6076" s="T147">ukkɨr</ta>
            <ta e="T149" id="Seg_6077" s="T148">zerkala-lʼa</ta>
            <ta e="T150" id="Seg_6078" s="T149">nılʼčʼɨ-lʼ</ta>
            <ta e="T151" id="Seg_6079" s="T150">ɛː-sɨ</ta>
            <ta e="T152" id="Seg_6080" s="T151">*koŋɨ-mpɨ-tɨ-qo</ta>
            <ta e="T153" id="Seg_6081" s="T152">tɛnɨmɨ-mpɨ</ta>
            <ta e="T154" id="Seg_6082" s="T153">təp-sä</ta>
            <ta e="T155" id="Seg_6083" s="T154">ukkɨr</ta>
            <ta e="T156" id="Seg_6084" s="T155">təp</ta>
            <ta e="T157" id="Seg_6085" s="T156">qan</ta>
            <ta e="T158" id="Seg_6086" s="T157">ɛː-kkɨ</ta>
            <ta e="T159" id="Seg_6087" s="T158">soma</ta>
            <ta e="T160" id="Seg_6088" s="T159">puːtɨ-k</ta>
            <ta e="T161" id="Seg_6089" s="T160">ɔːntɨ-al-mpɨ-kkɨ</ta>
            <ta e="T162" id="Seg_6090" s="T161">təp-sä</ta>
            <ta e="T163" id="Seg_6091" s="T162">šoqqa-k</ta>
            <ta e="T164" id="Seg_6092" s="T163">kara-mpɨ-kkɨ</ta>
            <ta e="T165" id="Seg_6093" s="T164">soma-lʼčʼɨ-mpɨlʼ</ta>
            <ta e="T166" id="Seg_6094" s="T165">*koŋɨ-mpɨ-kkɨ</ta>
            <ta e="T167" id="Seg_6095" s="T166">čʼeːlɨ-mɨ</ta>
            <ta e="T168" id="Seg_6096" s="T167">zerkala-lʼa-mɨ</ta>
            <ta e="T169" id="Seg_6097" s="T168">kətɨ-ätɨ</ta>
            <ta e="T170" id="Seg_6098" s="T169">napa</ta>
            <ta e="T171" id="Seg_6099" s="T170">čʼeːlɨ-k</ta>
            <ta e="T172" id="Seg_6100" s="T171">ınnä</ta>
            <ta e="T173" id="Seg_6101" s="T172">pin-ätɨ</ta>
            <ta e="T174" id="Seg_6102" s="T173">man</ta>
            <ta e="T175" id="Seg_6103" s="T174">qaj</ta>
            <ta e="T176" id="Seg_6104" s="T175">təttɨ-n</ta>
            <ta e="T177" id="Seg_6105" s="T176">*iː-qɨn</ta>
            <ta e="T178" id="Seg_6106" s="T177">muntɨk-n</ta>
            <ta e="T179" id="Seg_6107" s="T178">soma</ta>
            <ta e="T180" id="Seg_6108" s="T179">kuras</ta>
            <ta e="T181" id="Seg_6109" s="T180">sərɨ</ta>
            <ta e="T182" id="Seg_6110" s="T181">ɛː-ŋɨ-k</ta>
            <ta e="T183" id="Seg_6111" s="T182">zerkala-lʼa</ta>
            <ta e="T184" id="Seg_6112" s="T183">kətɨ-kkɨ-tɨ</ta>
            <ta e="T185" id="Seg_6113" s="T184">tan</ta>
            <ta e="T186" id="Seg_6114" s="T185">caːr-ɨ-n</ta>
            <ta e="T187" id="Seg_6115" s="T186">ima</ta>
            <ta e="T188" id="Seg_6116" s="T187">ašša</ta>
            <ta e="T189" id="Seg_6117" s="T188">antɨ-k</ta>
            <ta e="T190" id="Seg_6118" s="T189">tan</ta>
            <ta e="T191" id="Seg_6119" s="T190">kes</ta>
            <ta e="T192" id="Seg_6120" s="T191">ukkɨr</ta>
            <ta e="T193" id="Seg_6121" s="T192">muntɨk-n</ta>
            <ta e="T194" id="Seg_6122" s="T193">soma</ta>
            <ta e="T195" id="Seg_6123" s="T194">kuras</ta>
            <ta e="T196" id="Seg_6124" s="T195">aj</ta>
            <ta e="T197" id="Seg_6125" s="T196">sərɨ</ta>
            <ta e="T198" id="Seg_6126" s="T197">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T199" id="Seg_6127" s="T198">caːr-ɨ-n</ta>
            <ta e="T200" id="Seg_6128" s="T199">ima</ta>
            <ta e="T201" id="Seg_6129" s="T200">laqɨ-mɔːt-ŋɨ</ta>
            <ta e="T202" id="Seg_6130" s="T201">qəqtɨ_pɔːrɨ-m</ta>
            <ta e="T203" id="Seg_6131" s="T202">ip-qɨl-ŋɨ-tɨ</ta>
            <ta e="T204" id="Seg_6132" s="T203">sajɨ-ntɨ-sä</ta>
            <ta e="T205" id="Seg_6133" s="T204">rɨpčʼɨ-lʼčʼɨ-ŋɨ-n</ta>
            <ta e="T206" id="Seg_6134" s="T205">munɨ-ntɨ-sä</ta>
            <ta e="T207" id="Seg_6135" s="T206">kašɨ-lʼčʼɨ-ŋɨ-tɨ</ta>
            <ta e="T208" id="Seg_6136" s="T207">utɨ-m</ta>
            <ta e="T209" id="Seg_6137" s="T208">qenʼɨ-ntɨ</ta>
            <ta e="T210" id="Seg_6138" s="T209">kompɨlɨ-mpɨ</ta>
            <ta e="T211" id="Seg_6139" s="T210">zerkala-lʼa-ntɨ</ta>
            <ta e="T212" id="Seg_6140" s="T211">mantɨ-mpɨ-lä</ta>
            <ta e="T213" id="Seg_6141" s="T212">caːr-ɨ-n</ta>
            <ta e="T214" id="Seg_6142" s="T213">nälʼa</ta>
            <ta e="T215" id="Seg_6143" s="T214">*čʼilʼa-lʼ</ta>
            <ta e="T216" id="Seg_6144" s="T215">nätäk</ta>
            <ta e="T217" id="Seg_6145" s="T216">*lʼamɨ-k</ta>
            <ta e="T218" id="Seg_6146" s="T217">čʼontɨ-k</ta>
            <ta e="T219" id="Seg_6147" s="T218">orɨ-m-ntɨ</ta>
            <ta e="T220" id="Seg_6148" s="T219">na-n</ta>
            <ta e="T221" id="Seg_6149" s="T220">šittɨ kotä-n</ta>
            <ta e="T222" id="Seg_6150" s="T221">orɨ-m-ntɨ</ta>
            <ta e="T223" id="Seg_6151" s="T222">wəčʼčʼɨ-š-ɨ-ntɨ</ta>
            <ta e="T224" id="Seg_6152" s="T223">orɨ-m-ntɨ-ŋɨ</ta>
            <ta e="T225" id="Seg_6153" s="T224">sərɨ</ta>
            <ta e="T226" id="Seg_6154" s="T225">wəntɨ-k</ta>
            <ta e="T227" id="Seg_6155" s="T226">säːqɨ</ta>
            <ta e="T228" id="Seg_6156" s="T227">sajɨ-n</ta>
            <ta e="T229" id="Seg_6157" s="T228">üntɨ-k</ta>
            <ta e="T230" id="Seg_6158" s="T229">selmo-kɔːlɨ-k</ta>
            <ta e="T231" id="Seg_6159" s="T230">tɔːt-ɨ-k</ta>
            <ta e="T232" id="Seg_6160" s="T231">puːtɨ-k</ta>
            <ta e="T233" id="Seg_6161" s="T232">tətta-lʼ</ta>
            <ta e="T234" id="Seg_6162" s="T233">qum-tɨ</ta>
            <ta e="T235" id="Seg_6163" s="T234">peː-qɨl-ɨ-sɨ</ta>
            <ta e="T236" id="Seg_6164" s="T235">korolʼ-n</ta>
            <ta e="T237" id="Seg_6165" s="T236">iːja</ta>
            <ta e="T238" id="Seg_6166" s="T237">Elisej</ta>
            <ta e="T239" id="Seg_6167" s="T238">tü-sɨ</ta>
            <ta e="T240" id="Seg_6168" s="T239">qum</ta>
            <ta e="T241" id="Seg_6169" s="T240">caːr</ta>
            <ta e="T242" id="Seg_6170" s="T241">əːtɨ-m</ta>
            <ta e="T243" id="Seg_6171" s="T242">mi-sɨ-tɨ</ta>
            <ta e="T244" id="Seg_6172" s="T243">mɔːt-qɨn</ta>
            <ta e="T245" id="Seg_6173" s="T244">iː-pso</ta>
            <ta e="T246" id="Seg_6174" s="T245">mɨta</ta>
            <ta e="T247" id="Seg_6175" s="T246">ɛː-ŋɨ</ta>
            <ta e="T248" id="Seg_6176" s="T247">seːlʼčʼɨ</ta>
            <ta e="T249" id="Seg_6177" s="T248">təmɨ-tɨ-tɨlʼ</ta>
            <ta e="T250" id="Seg_6178" s="T249">qəːttɨ-t</ta>
            <ta e="T251" id="Seg_6179" s="T250">aj</ta>
            <ta e="T252" id="Seg_6180" s="T251">toːn</ta>
            <ta e="T253" id="Seg_6181" s="T252">aj</ta>
            <ta e="T254" id="Seg_6182" s="T253">tɛːttɨ-sar-ɨ-lʼ</ta>
            <ta e="T255" id="Seg_6183" s="T254">mɔːt-ɨ-ja-t</ta>
            <ta e="T256" id="Seg_6184" s="T255">nätäk-t-ɨ-nkinı</ta>
            <ta e="T257" id="Seg_6185" s="T256">taqɨ-qɨlɨ-qo</ta>
            <ta e="T258" id="Seg_6186" s="T257">caːr-ɨ-n</ta>
            <ta e="T259" id="Seg_6187" s="T258">ima</ta>
            <ta e="T260" id="Seg_6188" s="T259">*taqtɨ-ntɨ-kkɨ-ntɨlʼ</ta>
            <ta e="T261" id="Seg_6189" s="T260">zerkala-lʼa-ntɨ-sä</ta>
            <ta e="T262" id="Seg_6190" s="T261">ontɨ</ta>
            <ta e="T263" id="Seg_6191" s="T262">əːtɨ-k</ta>
            <ta e="T264" id="Seg_6192" s="T263">təp-ɨ-n</ta>
            <ta e="T265" id="Seg_6193" s="T264">čʼattɨ-kkɨ-sɨ-tɨ</ta>
            <ta e="T266" id="Seg_6194" s="T265">man</ta>
            <ta e="T267" id="Seg_6195" s="T266">qaj</ta>
            <ta e="T268" id="Seg_6196" s="T267">kətɨ-ätɨ</ta>
            <ta e="T269" id="Seg_6197" s="T268">mäkkä</ta>
            <ta e="T270" id="Seg_6198" s="T269">muntɨk</ta>
            <ta e="T271" id="Seg_6199" s="T270">kuras-sä</ta>
            <ta e="T272" id="Seg_6200" s="T271">aj</ta>
            <ta e="T273" id="Seg_6201" s="T272">sərɨ</ta>
            <ta e="T274" id="Seg_6202" s="T273">ɛː-ŋɨ-k</ta>
            <ta e="T275" id="Seg_6203" s="T274">qaj</ta>
            <ta e="T276" id="Seg_6204" s="T275">ket</ta>
            <ta e="T277" id="Seg_6205" s="T276">zerkala-lʼa</ta>
            <ta e="T278" id="Seg_6206" s="T277">tom-mpɨ-tɨ</ta>
            <ta e="T279" id="Seg_6207" s="T278">kuras-sä</ta>
            <ta e="T280" id="Seg_6208" s="T279">ɛː-ŋɨ-ntɨ</ta>
            <ta e="T281" id="Seg_6209" s="T280">ašša</ta>
            <ta e="T282" id="Seg_6210" s="T281">antɨ-k</ta>
            <ta e="T283" id="Seg_6211" s="T282">caːr-ɨ-n</ta>
            <ta e="T284" id="Seg_6212" s="T283">nälʼa</ta>
            <ta e="T285" id="Seg_6213" s="T284">qotəl</ta>
            <ta e="T286" id="Seg_6214" s="T285">muntɨk-n</ta>
            <ta e="T287" id="Seg_6215" s="T286">kuras-sä</ta>
            <ta e="T288" id="Seg_6216" s="T287">aj</ta>
            <ta e="T289" id="Seg_6217" s="T288">sərɨ</ta>
            <ta e="T290" id="Seg_6218" s="T289">ɛː-ŋɨ</ta>
            <ta e="T291" id="Seg_6219" s="T290">caːr-ɨ-n</ta>
            <ta e="T292" id="Seg_6220" s="T291">ima</ta>
            <ta e="T293" id="Seg_6221" s="T292">älpä</ta>
            <ta e="T294" id="Seg_6222" s="T293">paktɨ</ta>
            <ta e="T295" id="Seg_6223" s="T294">aj</ta>
            <ta e="T296" id="Seg_6224" s="T295">kürä-ätɔːl-ŋɨ</ta>
            <ta e="T297" id="Seg_6225" s="T296">utɨ-lʼa-ntɨ-sä</ta>
            <ta e="T298" id="Seg_6226" s="T297">zerkala-lʼa-m</ta>
            <ta e="T299" id="Seg_6227" s="T298">pəššɨ-tɨ-ŋɨ-tɨ</ta>
            <ta e="T300" id="Seg_6228" s="T299">lʼakčʼɨn-lʼa-ntɨ-sä</ta>
            <ta e="T301" id="Seg_6229" s="T300">čʼelʼčʼɨ-ɔːl-ŋɨ</ta>
            <ta e="T302" id="Seg_6230" s="T301">nɨː</ta>
            <ta e="T303" id="Seg_6231" s="T302">аh</ta>
            <ta e="T304" id="Seg_6232" s="T303">tan</ta>
            <ta e="T305" id="Seg_6233" s="T304">nılʼčʼɨ-lʼ</ta>
            <ta e="T306" id="Seg_6234" s="T305">steklo-ŋɨ-ntɨ</ta>
            <ta e="T307" id="Seg_6235" s="T306">tan</ta>
            <ta e="T308" id="Seg_6236" s="T307">na</ta>
            <ta e="T309" id="Seg_6237" s="T308">moːlmɨ-tɨ-ntɨ</ta>
            <ta e="T310" id="Seg_6238" s="T309">man</ta>
            <ta e="T311" id="Seg_6239" s="T310">čʼɔːtɨ</ta>
            <ta e="T312" id="Seg_6240" s="T311">təp</ta>
            <ta e="T313" id="Seg_6241" s="T312">kuttar</ta>
            <ta e="T314" id="Seg_6242" s="T313">mantɨ-mmä-ntɨ</ta>
            <ta e="T315" id="Seg_6243" s="T314">massä</ta>
            <ta e="T316" id="Seg_6244" s="T315">man</ta>
            <ta e="T317" id="Seg_6245" s="T316">təp-ɨ-n</ta>
            <ta e="T318" id="Seg_6246" s="T317">moːlmɨ-t</ta>
            <ta e="T319" id="Seg_6247" s="T318">*lʼamɨ-qɨl-ɛntɨ-m</ta>
            <ta e="T320" id="Seg_6248" s="T319">qo-ntɨr-ŋɨ-lɨt</ta>
            <ta e="T321" id="Seg_6249" s="T320">orɨ-m-ŋɨ</ta>
            <ta e="T322" id="Seg_6250" s="T321">qaj</ta>
            <ta e="T323" id="Seg_6251" s="T322">kuttar</ta>
            <ta e="T324" id="Seg_6252" s="T323">ašša</ta>
            <ta e="T325" id="Seg_6253" s="T324">sərɨ</ta>
            <ta e="T326" id="Seg_6254" s="T325">ɛː-ŋɨ</ta>
            <ta e="T327" id="Seg_6255" s="T326">ama-tɨ</ta>
            <ta e="T328" id="Seg_6256" s="T327">pɛrqɨ-lʼ</ta>
            <ta e="T329" id="Seg_6257" s="T328">tıːrɨ</ta>
            <ta e="T330" id="Seg_6258" s="T329">ɔːmtɨ-sɨ</ta>
            <ta e="T331" id="Seg_6259" s="T330">kekkɨsä</ta>
            <ta e="T332" id="Seg_6260" s="T331">sɨrɨ-ntɨ</ta>
            <ta e="T333" id="Seg_6261" s="T332">mantɨ-ätɔːl-mpɨ-sɨ</ta>
            <ta e="T334" id="Seg_6262" s="T333">kɨssa</ta>
            <ta e="T335" id="Seg_6263" s="T334">kətɨ-ätɨ</ta>
            <ta e="T336" id="Seg_6264" s="T335">qäntɨk</ta>
            <ta e="T337" id="Seg_6265" s="T336">man</ta>
            <ta e="T338" id="Seg_6266" s="T337">*nɔː-nɨ</ta>
            <ta e="T339" id="Seg_6267" s="T338">nɨːnɨ</ta>
            <ta e="T340" id="Seg_6268" s="T339">puːn</ta>
            <ta e="T341" id="Seg_6269" s="T340">təp</ta>
            <ta e="T342" id="Seg_6270" s="T341">kuras-sä</ta>
            <ta e="T343" id="Seg_6271" s="T342">ɛː-ɛntɨ</ta>
            <ta e="T344" id="Seg_6272" s="T343">ınnä</ta>
            <ta e="T345" id="Seg_6273" s="T344">kətɨ-ätɨ</ta>
            <ta e="T346" id="Seg_6274" s="T345">man</ta>
            <ta e="T347" id="Seg_6275" s="T346">kes</ta>
            <ta e="T348" id="Seg_6276" s="T347">nʼančʼa-k</ta>
            <ta e="T349" id="Seg_6277" s="T348">ɔːmtɨ-lʼ</ta>
            <ta e="T350" id="Seg_6278" s="T349">qok</ta>
            <ta e="T351" id="Seg_6279" s="T350">mɨ-tɨ</ta>
            <ta e="T352" id="Seg_6280" s="T351">kolʼɨ-altɨ-ätɨ</ta>
            <ta e="T353" id="Seg_6281" s="T352">kos</ta>
            <ta e="T354" id="Seg_6282" s="T353">təttɨ</ta>
            <ta e="T355" id="Seg_6283" s="T354">muntɨk</ta>
            <ta e="T356" id="Seg_6284" s="T355">massä</ta>
            <ta e="T357" id="Seg_6285" s="T356">čʼäːŋkɨ</ta>
            <ta e="T358" id="Seg_6286" s="T357">nʼančʼa-tɨlʼ</ta>
            <ta e="T359" id="Seg_6287" s="T358">mɨ</ta>
            <ta e="T360" id="Seg_6288" s="T359">nılʼčʼɨ-k</ta>
            <ta e="T361" id="Seg_6289" s="T360">qɔːtɨ</ta>
            <ta e="T362" id="Seg_6290" s="T361">zerkala-lʼa</ta>
            <ta e="T363" id="Seg_6291" s="T362">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T364" id="Seg_6292" s="T363">caːr-ɨ-n</ta>
            <ta e="T365" id="Seg_6293" s="T364">nälʼa</ta>
            <ta e="T366" id="Seg_6294" s="T365">qotəl</ta>
            <ta e="T367" id="Seg_6295" s="T366">pija</ta>
            <ta e="T368" id="Seg_6296" s="T367">kuras-sä</ta>
            <ta e="T369" id="Seg_6297" s="T368">aj</ta>
            <ta e="T370" id="Seg_6298" s="T369">sərɨ</ta>
            <ta e="T371" id="Seg_6299" s="T370">ɛː-ŋɨ</ta>
            <ta e="T372" id="Seg_6300" s="T371">kuttar</ta>
            <ta e="T373" id="Seg_6301" s="T372">meː-tɨ-l</ta>
            <ta e="T374" id="Seg_6302" s="T373">puːtɨ-m-tɨ</ta>
            <ta e="T375" id="Seg_6303" s="T374">am-lä</ta>
            <ta e="T376" id="Seg_6304" s="T375">zerkala-lʼa-m</ta>
            <ta e="T377" id="Seg_6305" s="T376">konnä</ta>
            <ta e="T378" id="Seg_6306" s="T377">čʼattɨ-tɨ</ta>
            <ta e="T379" id="Seg_6307" s="T378">Čʼernavka-m-tɨ</ta>
            <ta e="T380" id="Seg_6308" s="T379">qərɨ-ŋɨ-tɨ</ta>
            <ta e="T381" id="Seg_6309" s="T380">nılʼčʼɨ-k</ta>
            <ta e="T382" id="Seg_6310" s="T381">təp-ntɨ</ta>
            <ta e="T383" id="Seg_6311" s="T382">*kurɨ-altɨ-ŋɨ-tɨ</ta>
            <ta e="T384" id="Seg_6312" s="T383">ontɨ</ta>
            <ta e="T385" id="Seg_6313" s="T384">sennɨ-lʼ</ta>
            <ta e="T386" id="Seg_6314" s="T385">nätäk</ta>
            <ta e="T387" id="Seg_6315" s="T386">tına</ta>
            <ta e="T388" id="Seg_6316" s="T387">caːr-ɨ-n</ta>
            <ta e="T389" id="Seg_6317" s="T388">nälʼa-m</ta>
            <ta e="T390" id="Seg_6318" s="T389">mačʼɨ-n</ta>
            <ta e="T391" id="Seg_6319" s="T390">puːtɨ-ntɨ</ta>
            <ta e="T392" id="Seg_6320" s="T391">qən-ntɨr-tɨ-ltɨ-l</ta>
            <ta e="T393" id="Seg_6321" s="T392">kurɨ-ntɨ-mpɨlʼ</ta>
            <ta e="T394" id="Seg_6322" s="T393">ilɨ-lä</ta>
            <ta e="T395" id="Seg_6323" s="T394">aj</ta>
            <ta e="T396" id="Seg_6324" s="T395">qəːčʼɨ-ntɨ-mpɨlʼ</ta>
            <ta e="T397" id="Seg_6325" s="T396">näčʼčʼä-qɨn</ta>
            <ta e="T398" id="Seg_6326" s="T397">čʼöː-n</ta>
            <ta e="T399" id="Seg_6327" s="T398">ıllä-qɨn</ta>
            <ta e="T400" id="Seg_6328" s="T399">čʼumpɨ</ta>
            <ta e="T401" id="Seg_6329" s="T400">neː-ɨ-t</ta>
            <ta e="T402" id="Seg_6330" s="T401">am-qɨntɨtqo</ta>
            <ta e="T403" id="Seg_6331" s="T402">lоːsɨ</ta>
            <ta e="T404" id="Seg_6332" s="T403">montɨ</ta>
            <ta e="T405" id="Seg_6333" s="T404">antɨ-ɛntɨ</ta>
            <ta e="T406" id="Seg_6334" s="T405">nʼenʼnʼɨ-ntɨlʼ</ta>
            <ta e="T407" id="Seg_6335" s="T406">ima-sä</ta>
            <ta e="T408" id="Seg_6336" s="T407">caːr-ɨ-n</ta>
            <ta e="T409" id="Seg_6337" s="T408">nälʼa-sä</ta>
            <ta e="T410" id="Seg_6338" s="T409">Čʼernavka</ta>
            <ta e="T411" id="Seg_6339" s="T410">qən-ntɨ</ta>
            <ta e="T412" id="Seg_6340" s="T411">mačʼɨ-ntɨ</ta>
            <ta e="T413" id="Seg_6341" s="T412">na</ta>
            <ta e="T414" id="Seg_6342" s="T413">naššak</ta>
            <ta e="T415" id="Seg_6343" s="T414">kuntak</ta>
            <ta e="T416" id="Seg_6344" s="T415">tına</ta>
            <ta e="T417" id="Seg_6345" s="T416">qən-tɨ-ntɨlʼ</ta>
            <ta e="T418" id="Seg_6346" s="T417">caːr-ɨ-n</ta>
            <ta e="T419" id="Seg_6347" s="T418">nälʼa</ta>
            <ta e="T420" id="Seg_6348" s="T419">nɨːnɨ</ta>
            <ta e="T421" id="Seg_6349" s="T420">qən-tɨ</ta>
            <ta e="T422" id="Seg_6350" s="T421">qu-ku-ntɨ-ššak</ta>
            <ta e="T423" id="Seg_6351" s="T422">nɨrkɨ-mɔːt-ŋɨ</ta>
            <ta e="T424" id="Seg_6352" s="T423">omtɨ-altɨ-ŋɨ</ta>
            <ta e="T425" id="Seg_6353" s="T424">ilɨ-ptäː-mɨ</ta>
            <ta e="T426" id="Seg_6354" s="T425">qaj-qɨn</ta>
            <ta e="T427" id="Seg_6355" s="T426">kətɨ-ätɨ</ta>
            <ta e="T428" id="Seg_6356" s="T427">ürɨ-mɨ</ta>
            <ta e="T429" id="Seg_6357" s="T428">ɛː-ŋɨ</ta>
            <ta e="T430" id="Seg_6358" s="T429">nälʼa-qum</ta>
            <ta e="T431" id="Seg_6359" s="T430">ɨkɨ</ta>
            <ta e="T432" id="Seg_6360" s="T431">mašım</ta>
            <ta e="T433" id="Seg_6361" s="T432">talʼ-š-olʼ-äšɨk</ta>
            <ta e="T434" id="Seg_6362" s="T433">kuttar</ta>
            <ta e="T435" id="Seg_6363" s="T434">qum-ɨ-m</ta>
            <ta e="T436" id="Seg_6364" s="T435">qo-ɛntɨ-m</ta>
            <ta e="T437" id="Seg_6365" s="T436">tašıntɨ</ta>
            <ta e="T438" id="Seg_6366" s="T437">qaj-tqo</ta>
            <ta e="T439" id="Seg_6367" s="T438">kɨkɨ-ntɨ</ta>
            <ta e="T440" id="Seg_6368" s="T439">na-tqo</ta>
            <ta e="T441" id="Seg_6369" s="T440">meː-ɛntɨ-k</ta>
            <ta e="T442" id="Seg_6370" s="T441">toːnna-m</ta>
            <ta e="T443" id="Seg_6371" s="T442">puːtɨ-sä</ta>
            <ta e="T444" id="Seg_6372" s="T443">təp-tɨ</ta>
            <ta e="T445" id="Seg_6373" s="T444">kɨkɨ-lä</ta>
            <ta e="T446" id="Seg_6374" s="T445">ašša</ta>
            <ta e="T447" id="Seg_6375" s="T446">kurɨ-sɨ-tɨ</ta>
            <ta e="T448" id="Seg_6376" s="T447">ašša</ta>
            <ta e="T449" id="Seg_6377" s="T448">qət-sɨ-tɨ</ta>
            <ta e="T450" id="Seg_6378" s="T449">üːtɨ-sɨ-tɨ</ta>
            <ta e="T451" id="Seg_6379" s="T450">nılʼčʼɨ-k</ta>
            <ta e="T452" id="Seg_6380" s="T451">kətɨ-lä</ta>
            <ta e="T453" id="Seg_6381" s="T452">qən-äšɨk</ta>
            <ta e="T454" id="Seg_6382" s="T453">nom</ta>
            <ta e="T455" id="Seg_6383" s="T454">tašıntɨ</ta>
            <ta e="T456" id="Seg_6384" s="T455">na</ta>
            <ta e="T457" id="Seg_6385" s="T456">wərɨ-tɨ</ta>
            <ta e="T458" id="Seg_6386" s="T457">nɨːnɨ</ta>
            <ta e="T459" id="Seg_6387" s="T458">ontɨ</ta>
            <ta e="T460" id="Seg_6388" s="T459">moqɨnä</ta>
            <ta e="T461" id="Seg_6389" s="T460">tü-sɨ</ta>
            <ta e="T462" id="Seg_6390" s="T461">qaj</ta>
            <ta e="T463" id="Seg_6391" s="T462">caːr-ɨ-n</ta>
            <ta e="T464" id="Seg_6392" s="T463">ima</ta>
            <ta e="T465" id="Seg_6393" s="T464">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T466" id="Seg_6394" s="T465">kuras-sɨma-lʼ</ta>
            <ta e="T467" id="Seg_6395" s="T466">nälʼa-qum</ta>
            <ta e="T468" id="Seg_6396" s="T467">kun</ta>
            <ta e="T469" id="Seg_6397" s="T468">ɛː-ŋɨ</ta>
            <ta e="T470" id="Seg_6398" s="T469">pɛlɨ-kɔːlɨ-k</ta>
            <ta e="T471" id="Seg_6399" s="T470">mačʼɨ-n</ta>
            <ta e="T472" id="Seg_6400" s="T471">nɨŋ</ta>
            <ta e="T473" id="Seg_6401" s="T472">təp-tɨ</ta>
            <ta e="T474" id="Seg_6402" s="T473">kətɨ-ŋɨ-tɨ</ta>
            <ta e="T475" id="Seg_6403" s="T474">təp-ɨ-n</ta>
            <ta e="T476" id="Seg_6404" s="T475">sɔːrɨ-mpɨ</ta>
            <ta e="T477" id="Seg_6405" s="T476">qɔːmɨčʼä-k</ta>
            <ta e="T478" id="Seg_6406" s="T477">səntɨ</ta>
            <ta e="T479" id="Seg_6407" s="T478">neː-n-tɨ</ta>
            <ta e="T480" id="Seg_6408" s="T479">suːrɨm-n</ta>
            <ta e="T481" id="Seg_6409" s="T480">qatɨ-ntɨ</ta>
            <ta e="T482" id="Seg_6410" s="T481">alʼčʼɨ-čʼɨ-mmä</ta>
            <ta e="T483" id="Seg_6411" s="T482">qɔːna-k</ta>
            <ta e="T484" id="Seg_6412" s="T483">təp</ta>
            <ta e="T485" id="Seg_6413" s="T484">na</ta>
            <ta e="T486" id="Seg_6414" s="T485">kə-kkɨ-tɨ-ŋɨ-tɨ</ta>
            <ta e="T487" id="Seg_6415" s="T486">qu-r-mə-n</ta>
            <ta e="T488" id="Seg_6416" s="T487">aj</ta>
            <ta e="T489" id="Seg_6417" s="T488">na</ta>
            <ta e="T490" id="Seg_6418" s="T489">seːpɨ</ta>
            <ta e="T491" id="Seg_6419" s="T490">ɛː-ntɨ-ntɨ</ta>
            <ta e="T492" id="Seg_6420" s="T491">əːtɨ</ta>
            <ta e="T493" id="Seg_6421" s="T492">qən-ŋɨ</ta>
            <ta e="T494" id="Seg_6422" s="T493">nɨːnɨ</ta>
            <ta e="T495" id="Seg_6423" s="T494">nılʼčʼɨ-lʼ</ta>
            <ta e="T496" id="Seg_6424" s="T495">caːr-ɨ-n</ta>
            <ta e="T497" id="Seg_6425" s="T496">nälʼa</ta>
            <ta e="T498" id="Seg_6426" s="T497">ürɨ-ntɨ-ŋɨ</ta>
            <ta e="T499" id="Seg_6427" s="T498">pəšɨ-r-ŋɨ</ta>
            <ta e="T500" id="Seg_6428" s="T499">caːr</ta>
            <ta e="T501" id="Seg_6429" s="T500">nälʼa-ntɨ-tqo</ta>
            <ta e="T502" id="Seg_6430" s="T501">korolʼ</ta>
            <ta e="T503" id="Seg_6431" s="T502">iːja</ta>
            <ta e="T504" id="Seg_6432" s="T503">Elisej</ta>
            <ta e="T505" id="Seg_6433" s="T504">nom-ntɨ</ta>
            <ta e="T506" id="Seg_6434" s="T505">omtɨ-tɨ-lä</ta>
            <ta e="T507" id="Seg_6435" s="T506">puːlä</ta>
            <ta e="T508" id="Seg_6436" s="T507">*taqtɨ-altɨ</ta>
            <ta e="T509" id="Seg_6437" s="T508">aj</ta>
            <ta e="T510" id="Seg_6438" s="T509">peː-lä</ta>
            <ta e="T511" id="Seg_6439" s="T510">qən-ŋɨ</ta>
            <ta e="T512" id="Seg_6440" s="T511">kuras-sɨma-n</ta>
            <ta e="T513" id="Seg_6441" s="T512">sıːčʼɨ-n-tɨ</ta>
            <ta e="T514" id="Seg_6442" s="T513">olɨ-m</ta>
            <ta e="T515" id="Seg_6443" s="T514">tıːtap</ta>
            <ta e="T516" id="Seg_6444" s="T515">iː-pso</ta>
            <ta e="T517" id="Seg_6445" s="T516">ima-tɨ</ta>
            <ta e="T518" id="Seg_6446" s="T517">neː-m-tɨ</ta>
            <ta e="T519" id="Seg_6447" s="T518">a</ta>
            <ta e="T520" id="Seg_6448" s="T519">itɨ</ta>
            <ta e="T521" id="Seg_6449" s="T520">neː-tɨ</ta>
            <ta e="T522" id="Seg_6450" s="T521">qarɨ-n</ta>
            <ta e="T523" id="Seg_6451" s="T522">təttɨ</ta>
            <ta e="T524" id="Seg_6452" s="T523">mačʼɨ-n</ta>
            <ta e="T525" id="Seg_6453" s="T524">puːtɨ-n</ta>
            <ta e="T526" id="Seg_6454" s="T525">ürɨ-kkɨ-mpɨ-lä</ta>
            <ta e="T527" id="Seg_6455" s="T526">*kurɨ-ntɨ-ntɨ</ta>
            <ta e="T528" id="Seg_6456" s="T527">šittɨ kotä-n</ta>
            <ta e="T529" id="Seg_6457" s="T528">mɔːt-ɨ-n</ta>
            <ta e="T530" id="Seg_6458" s="T529">*iː-ntɨ</ta>
            <ta e="T531" id="Seg_6459" s="T530">tü-lʼčʼɨ-ıː-mpɨ</ta>
            <ta e="T532" id="Seg_6460" s="T531">čʼəssä</ta>
            <ta e="T533" id="Seg_6461" s="T532">läq</ta>
            <ta e="T534" id="Seg_6462" s="T533">təp-ɨ-n</ta>
            <ta e="T535" id="Seg_6463" s="T534">muːt-ntɨlʼ</ta>
            <ta e="T536" id="Seg_6464" s="T535">läːqɨ-ätɔːl-tɨ</ta>
            <ta e="T537" id="Seg_6465" s="T536">*lʼamɨ-k-altɨ-ntɨ</ta>
            <ta e="T538" id="Seg_6466" s="T537">lüčʼi-mpɨ-lä</ta>
            <ta e="T539" id="Seg_6467" s="T538">ɔːntɨ-al-mpɨ-lä</ta>
            <ta e="T540" id="Seg_6468" s="T539">vorota-mɨn</ta>
            <ta e="T541" id="Seg_6469" s="T540">təp</ta>
            <ta e="T542" id="Seg_6470" s="T541">na</ta>
            <ta e="T543" id="Seg_6471" s="T542">šeːr</ta>
            <ta e="T544" id="Seg_6472" s="T543">ətɨmantɨ-n</ta>
            <ta e="T545" id="Seg_6473" s="T544">*lʼamɨ-k</ta>
            <ta e="T546" id="Seg_6474" s="T545">ɛː-ŋɨ</ta>
            <ta e="T547" id="Seg_6475" s="T546">läq</ta>
            <ta e="T548" id="Seg_6476" s="T547">ɔːntɨ-al-mpɨ-lä</ta>
            <ta e="T549" id="Seg_6477" s="T548">nʼoː-ŋɨ-tɨ</ta>
            <ta e="T550" id="Seg_6478" s="T549">a</ta>
            <ta e="T551" id="Seg_6479" s="T550">caːr-ɨ-n</ta>
            <ta e="T552" id="Seg_6480" s="T551">nälʼa</ta>
            <ta e="T553" id="Seg_6481" s="T552">*kurɨ-tɨ-lä</ta>
            <ta e="T554" id="Seg_6482" s="T553">krɨlʼco-n</ta>
            <ta e="T555" id="Seg_6483" s="T554">*iː-ntɨ</ta>
            <ta e="T556" id="Seg_6484" s="T555">sɨːqɨl-ntɨ</ta>
            <ta e="T557" id="Seg_6485" s="T556">pürɨ-lʼ</ta>
            <ta e="T558" id="Seg_6486" s="T557">utɨ-qɨn-ntɨ</ta>
            <ta e="T559" id="Seg_6487" s="T558">iː-ntɨ-tɨ</ta>
            <ta e="T560" id="Seg_6488" s="T559">mɔːta-t</ta>
            <ta e="T561" id="Seg_6489" s="T560">čʼontɨ-ka</ta>
            <ta e="T562" id="Seg_6490" s="T561">na</ta>
            <ta e="T563" id="Seg_6491" s="T562">nüː-ntɨ-tɨ</ta>
            <ta e="T564" id="Seg_6492" s="T563">mɔːt-ntɨ</ta>
            <ta e="T565" id="Seg_6493" s="T564">čʼontɨ-ka</ta>
            <ta e="T566" id="Seg_6494" s="T565">na</ta>
            <ta e="T567" id="Seg_6495" s="T566">šeːr</ta>
            <ta e="T568" id="Seg_6496" s="T567">qo-ntɨ-mpɨ</ta>
            <ta e="T569" id="Seg_6497" s="T568">qo-ntɨ-mpɨ</ta>
            <ta e="T570" id="Seg_6498" s="T569">qo-ntɨ-ntɨ</ta>
            <ta e="T571" id="Seg_6499" s="T570">kovʼor-sä</ta>
            <ta e="T572" id="Seg_6500" s="T571">lavka-t</ta>
            <ta e="T573" id="Seg_6501" s="T572">ikona-n</ta>
            <ta e="T574" id="Seg_6502" s="T573">ıllä-qɨn</ta>
            <ta e="T575" id="Seg_6503" s="T574">dup-ɨ-lʼ</ta>
            <ta e="T576" id="Seg_6504" s="T575">lem</ta>
            <ta e="T577" id="Seg_6505" s="T576">nəkɨr-mpɨlʼ</ta>
            <ta e="T578" id="Seg_6506" s="T577">ippɨ-ptäː-sä</ta>
            <ta e="T579" id="Seg_6507" s="T578">šoːqɨr</ta>
            <ta e="T580" id="Seg_6508" s="T579">nälʼa-qum</ta>
            <ta e="T581" id="Seg_6509" s="T580">mantɨ-mpɨ-tɨ</ta>
            <ta e="T582" id="Seg_6510" s="T581">tɨmtɨ</ta>
            <ta e="T583" id="Seg_6511" s="T582">qɔːtɨ</ta>
            <ta e="T584" id="Seg_6512" s="T583">soma</ta>
            <ta e="T585" id="Seg_6513" s="T584">qum-ɨ-t</ta>
            <ta e="T586" id="Seg_6514" s="T585">ilɨ-tɨt</ta>
            <ta e="T587" id="Seg_6515" s="T586">təp-ɨ-t</ta>
            <ta e="T588" id="Seg_6516" s="T587">ašša</ta>
            <ta e="T589" id="Seg_6517" s="T588">toːtɨ-lɨ-ɛntɨ-tɨt</ta>
            <ta e="T590" id="Seg_6518" s="T589">nɔːtɨ</ta>
            <ta e="T591" id="Seg_6519" s="T590">šittɨ kotä-k</ta>
            <ta e="T592" id="Seg_6520" s="T591">nʼi</ta>
            <ta e="T593" id="Seg_6521" s="T592">kutɨ</ta>
            <ta e="T594" id="Seg_6522" s="T593">ašša</ta>
            <ta e="T595" id="Seg_6523" s="T594">atɨ</ta>
            <ta e="T596" id="Seg_6524" s="T595">mɔːt-ɨ-m</ta>
            <ta e="T597" id="Seg_6525" s="T596">toː-mɨn</ta>
            <ta e="T598" id="Seg_6526" s="T597">kolʼɨ-ltɨ-ntɨ-tɨ</ta>
            <ta e="T599" id="Seg_6527" s="T598">muntɨk</ta>
            <ta e="T600" id="Seg_6528" s="T599">soma-k</ta>
            <ta e="T601" id="Seg_6529" s="T600">täš-altɨ-sɨ-tɨ</ta>
            <ta e="T602" id="Seg_6530" s="T601">ikona-n</ta>
            <ta e="T603" id="Seg_6531" s="T602">sečʼčʼi-m</ta>
            <ta e="T604" id="Seg_6532" s="T603">ačʼɨ-altɨ-ntɨ-tɨ</ta>
            <ta e="T605" id="Seg_6533" s="T604">šoːqɨr-ɨ-m</ta>
            <ta e="T606" id="Seg_6534" s="T605">pötpɨlʼ-lʼa</ta>
            <ta e="T607" id="Seg_6535" s="T606">čʼɔːtɨ-ntɨ-tɨ</ta>
            <ta e="T608" id="Seg_6536" s="T607">polati-n</ta>
            <ta e="T609" id="Seg_6537" s="T608">*iː-ntɨ</ta>
            <ta e="T610" id="Seg_6538" s="T609">sɨːqɨl-lä</ta>
            <ta e="T611" id="Seg_6539" s="T610">*lʼamɨ-k</ta>
            <ta e="T612" id="Seg_6540" s="T611">ıllä</ta>
            <ta e="T613" id="Seg_6541" s="T612">alʼčʼɨ-ıː-ntɨ</ta>
            <ta e="T614" id="Seg_6542" s="T613">čʼeːlɨ-n</ta>
            <ta e="T615" id="Seg_6543" s="T614">čʼontɨ-ntɨ</ta>
            <ta e="T616" id="Seg_6544" s="T615">təttɨčʼa</ta>
            <ta e="T617" id="Seg_6545" s="T616">ɛː-sɨ</ta>
            <ta e="T618" id="Seg_6546" s="T617">sümɨ</ta>
            <ta e="T619" id="Seg_6547" s="T618">üntɨ-alʼ-ɨ-ntɨ</ta>
            <ta e="T620" id="Seg_6548" s="T619">poː-qɨn</ta>
            <ta e="T621" id="Seg_6549" s="T620">seːlʼčʼɨ</ta>
            <ta e="T622" id="Seg_6550" s="T621">mɔːtɨr-ɨ-t</ta>
            <ta e="T623" id="Seg_6551" s="T622">na</ta>
            <ta e="T624" id="Seg_6552" s="T623">šeːr-tɨt</ta>
            <ta e="T625" id="Seg_6553" s="T624">seːlʼčʼɨ-t</ta>
            <ta e="T626" id="Seg_6554" s="T625">muntɨk</ta>
            <ta e="T627" id="Seg_6555" s="T626">untɨlʼ uːkɨ-k</ta>
            <ta e="T628" id="Seg_6556" s="T627">wərqɨ</ta>
            <ta e="T629" id="Seg_6557" s="T628">neː-tɨt</ta>
            <ta e="T630" id="Seg_6558" s="T629">nılʼčʼɨ-k</ta>
            <ta e="T631" id="Seg_6559" s="T630">ɛsɨ</ta>
            <ta e="T632" id="Seg_6560" s="T631">na</ta>
            <ta e="T633" id="Seg_6561" s="T632">lʼa</ta>
            <ta e="T634" id="Seg_6562" s="T633">qaj</ta>
            <ta e="T635" id="Seg_6563" s="T634">ɛsɨ</ta>
            <ta e="T636" id="Seg_6564" s="T635">kuttar</ta>
            <ta e="T637" id="Seg_6565" s="T636">soma</ta>
            <ta e="T638" id="Seg_6566" s="T637">kuttar</ta>
            <ta e="T639" id="Seg_6567" s="T638">muntɨk</ta>
            <ta e="T640" id="Seg_6568" s="T639">kuras-sä</ta>
            <ta e="T641" id="Seg_6569" s="T640">ɛː-ŋɨ</ta>
            <ta e="T642" id="Seg_6570" s="T641">kutɨ</ta>
            <ta e="T643" id="Seg_6571" s="T642">kos</ta>
            <ta e="T644" id="Seg_6572" s="T643">mɔːt-mɨt</ta>
            <ta e="T645" id="Seg_6573" s="T644">täš-mpɨ-tɨ</ta>
            <ta e="T646" id="Seg_6574" s="T645">mɨta</ta>
            <ta e="T647" id="Seg_6575" s="T646">meːšımɨt</ta>
            <ta e="T648" id="Seg_6576" s="T647">ətɨ-kkɨ-tɨt</ta>
            <ta e="T649" id="Seg_6577" s="T648">ətɨ-mpɨ-tɨt</ta>
            <ta e="T650" id="Seg_6578" s="T649">kutɨ-ntɨ</ta>
            <ta e="T651" id="Seg_6579" s="T650">tantɨ-äšɨk</ta>
            <ta e="T652" id="Seg_6580" s="T651">atɨ-ltɨ-äšɨk</ta>
            <ta e="T653" id="Seg_6581" s="T652">meːšımɨt</ta>
            <ta e="T654" id="Seg_6582" s="T653">toː-nɨ</ta>
            <ta e="T655" id="Seg_6583" s="T654">qotəl-k</ta>
            <ta e="T656" id="Seg_6584" s="T655">čʼəːtɨ-äšɨk</ta>
            <ta e="T657" id="Seg_6585" s="T656">qatamol</ta>
            <ta e="T658" id="Seg_6586" s="T657">tan</ta>
            <ta e="T659" id="Seg_6587" s="T658">wərqɨ</ta>
            <ta e="T660" id="Seg_6588" s="T659">qum-ɨ-ntɨ</ta>
            <ta e="T661" id="Seg_6589" s="T660">ilʼčʼa-nɨt-tqo</ta>
            <ta e="T662" id="Seg_6590" s="T661">meːltɨ</ta>
            <ta e="T663" id="Seg_6591" s="T662">ɛː-ɛntɨ-ntɨ</ta>
            <ta e="T664" id="Seg_6592" s="T663">qatamol</ta>
            <ta e="T665" id="Seg_6593" s="T664">tan</ta>
            <ta e="T666" id="Seg_6594" s="T665">ilʼmatɨlʼ</ta>
            <ta e="T667" id="Seg_6595" s="T666">nʼeː-ntɨ</ta>
            <ta e="T668" id="Seg_6596" s="T667">meːqɨnʼɨn</ta>
            <ta e="T669" id="Seg_6597" s="T668">čʼopa-nɨt-tqo</ta>
            <ta e="T670" id="Seg_6598" s="T669">ɛː-ɛntɨ-tɨ</ta>
            <ta e="T671" id="Seg_6599" s="T670">qatamol</ta>
            <ta e="T672" id="Seg_6600" s="T671">tan</ta>
            <ta e="T673" id="Seg_6601" s="T672">imaqota</ta>
            <ta e="T674" id="Seg_6602" s="T673">qərɨ-ɛntɨ-mɨt</ta>
            <ta e="T675" id="Seg_6603" s="T674">ama-nɨt-tqo</ta>
            <ta e="T676" id="Seg_6604" s="T675">qatamol</ta>
            <ta e="T677" id="Seg_6605" s="T676">tan</ta>
            <ta e="T678" id="Seg_6606" s="T677">nälʼa</ta>
            <ta e="T679" id="Seg_6607" s="T678">qum-ɨ-ntɨ</ta>
            <ta e="T680" id="Seg_6608" s="T679">meːqɨnʼɨn</ta>
            <ta e="T681" id="Seg_6609" s="T680">nʼenʼnʼa-nɨt-tqo</ta>
            <ta e="T682" id="Seg_6610" s="T681">ɛː-ɛntɨ-tɨ</ta>
            <ta e="T683" id="Seg_6611" s="T682">caːr-ɨ-n</ta>
            <ta e="T684" id="Seg_6612" s="T683">nälʼa</ta>
            <ta e="T685" id="Seg_6613" s="T684">ıllä</ta>
            <ta e="T686" id="Seg_6614" s="T685">*panı-ntɨ</ta>
            <ta e="T687" id="Seg_6615" s="T686">ıllä-mpɨ-ŋɨ</ta>
            <ta e="T688" id="Seg_6616" s="T687">torowajtɨ-ntɨ</ta>
            <ta e="T689" id="Seg_6617" s="T688">təp-ɨ-t-ɨ-sä</ta>
            <ta e="T690" id="Seg_6618" s="T689">čʼüː-n-tɨt</ta>
            <ta e="T691" id="Seg_6619" s="T690">təttɨ</ta>
            <ta e="T692" id="Seg_6620" s="T691">mušqɨ-ɛː-ntɨ-lä</ta>
            <ta e="T693" id="Seg_6621" s="T692">nʼuːtɨ</ta>
            <ta e="T694" id="Seg_6622" s="T693">sočʼɨ</ta>
            <ta e="T695" id="Seg_6623" s="T694">nʼarqɨ-ttɨ-r-ɨ-sɨ</ta>
            <ta e="T696" id="Seg_6624" s="T695">nılʼčʼɨ-lʼ</ta>
            <ta e="T697" id="Seg_6625" s="T696">ɛsɨ-tɨ</ta>
            <ta e="T698" id="Seg_6626" s="T697">mɔːt-qɨn-tɨt</ta>
            <ta e="T699" id="Seg_6627" s="T698">mol</ta>
            <ta e="T700" id="Seg_6628" s="T699">šeːr-sɨ-k</ta>
            <ta e="T701" id="Seg_6629" s="T700">man</ta>
            <ta e="T702" id="Seg_6630" s="T701">tɨː</ta>
            <ta e="T703" id="Seg_6631" s="T702">kos</ta>
            <ta e="T704" id="Seg_6632" s="T703">mašım</ta>
            <ta e="T705" id="Seg_6633" s="T704">qərɨ-äptɨ-lɨt</ta>
            <ta e="T706" id="Seg_6634" s="T705">čʼäːŋkɨ</ta>
            <ta e="T707" id="Seg_6635" s="T706">nɨːnɨ</ta>
            <ta e="T708" id="Seg_6636" s="T707">əːtɨ-mɨn-tɨ</ta>
            <ta e="T709" id="Seg_6637" s="T708">qo-ntɨ-tɨt</ta>
            <ta e="T710" id="Seg_6638" s="T709">tam</ta>
            <ta e="T711" id="Seg_6639" s="T710">aš</ta>
            <ta e="T712" id="Seg_6640" s="T711">caːr-ɨ-n</ta>
            <ta e="T713" id="Seg_6641" s="T712">nälʼa</ta>
            <ta e="T714" id="Seg_6642" s="T713">ɛː-sɨ</ta>
            <ta e="T715" id="Seg_6643" s="T714">säŋä-lʼa-ntɨ</ta>
            <ta e="T716" id="Seg_6644" s="T715">omtɨ-ltɨ-sɨ-tɨt</ta>
            <ta e="T717" id="Seg_6645" s="T716">nʼanʼ-ɨ-lʼa-sä</ta>
            <ta e="T718" id="Seg_6646" s="T717">nʼanʼ-ɨ-lʼa-sä</ta>
            <ta e="T719" id="Seg_6647" s="T718">mi-lʼčʼɨ-tɨ-sɨ-tɨt</ta>
            <ta e="T720" id="Seg_6648" s="T719">rʼumka-lʼ</ta>
            <ta e="T721" id="Seg_6649" s="T720">tıːrɨ-lʼa</ta>
            <ta e="T722" id="Seg_6650" s="T721">*qam-tɨ-tɨ-sɨ-tɨt</ta>
            <ta e="T723" id="Seg_6651" s="T722">ili-n</ta>
            <ta e="T724" id="Seg_6652" s="T723">*iː-qɨn</ta>
            <ta e="T725" id="Seg_6653" s="T724">taːtɨ-tɨ-sɨ-tɨ</ta>
            <ta e="T726" id="Seg_6654" s="T725">təp-ɨ-lʼ</ta>
            <ta e="T727" id="Seg_6655" s="T726">üt-ɨ-m</ta>
            <ta e="T728" id="Seg_6656" s="T727">ašša</ta>
            <ta e="T729" id="Seg_6657" s="T728">iː-sɨ-tɨ</ta>
            <ta e="T730" id="Seg_6658" s="T729">nʼanʼ-ɨ-lʼa-m</ta>
            <ta e="T731" id="Seg_6659" s="T730">kes</ta>
            <ta e="T732" id="Seg_6660" s="T731">səpɨ-tɨ-lä</ta>
            <ta e="T733" id="Seg_6661" s="T732">kɨpa</ta>
            <ta e="T734" id="Seg_6662" s="T733">lıːpɨ-m-tɨ</ta>
            <ta e="T735" id="Seg_6663" s="T734">ɔːŋ-ɨ-ntɨ</ta>
            <ta e="T736" id="Seg_6664" s="T735">iː-sɨ-tɨ</ta>
            <ta e="T737" id="Seg_6665" s="T736">wəttɨ-n</ta>
            <ta e="T738" id="Seg_6666" s="T737">*nɔː-nɨ</ta>
            <ta e="T739" id="Seg_6667" s="T738">*nıšqɨ-qɨntoːqo</ta>
            <ta e="T740" id="Seg_6668" s="T739">koptɨ-tqo</ta>
            <ta e="T741" id="Seg_6669" s="T740">na</ta>
            <ta e="T742" id="Seg_6670" s="T741">mɔːtɨ-r-ɨ-ŋɨ</ta>
            <ta e="T743" id="Seg_6671" s="T742">qən-tɨ-sɨ-tɨt</ta>
            <ta e="T744" id="Seg_6672" s="T743">təp-ɨ-t</ta>
            <ta e="T745" id="Seg_6673" s="T744">nätäk-m</ta>
            <ta e="T746" id="Seg_6674" s="T745">ınnä</ta>
            <ta e="T747" id="Seg_6675" s="T746">čʼeːlɨ-k-mpɨ-ntɨlʼ</ta>
            <ta e="T748" id="Seg_6676" s="T747">šünʼčʼɨ-n</ta>
            <ta e="T749" id="Seg_6677" s="T748">aj</ta>
            <ta e="T750" id="Seg_6678" s="T749">ontɨ-qɨn</ta>
            <ta e="T751" id="Seg_6679" s="T750">na</ta>
            <ta e="T752" id="Seg_6680" s="T751">nɨmtɨ</ta>
            <ta e="T753" id="Seg_6681" s="T752">qəːčʼɨ-ntɨ-tɨt</ta>
            <ta e="T754" id="Seg_6682" s="T753">qəːčʼɨ-sɨ-tɨt</ta>
            <ta e="T755" id="Seg_6683" s="T754">təp-ɨ-m</ta>
            <ta e="T756" id="Seg_6684" s="T755">ɔːŋkɨ-tɨ-ntɨlʼ</ta>
            <ta e="T757" id="Seg_6685" s="T756">pɛlɨ-kɔːlɨ-k</ta>
            <ta e="T759" id="Seg_6686" s="T758">nʼarqɨ</ta>
            <ta e="T760" id="Seg_6687" s="T759">čʼeːlɨ-n</ta>
            <ta e="T761" id="Seg_6688" s="T760">čʼeːlɨ</ta>
            <ta e="T762" id="Seg_6689" s="T761">moːrɨ-qɨn-tɨ</ta>
            <ta e="T763" id="Seg_6690" s="T762">ilʼmatɨlʼ</ta>
            <ta e="T764" id="Seg_6691" s="T763">*korɨ-altɨ-mpɨ-sɨ</ta>
            <ta e="T765" id="Seg_6692" s="T764">čʼeːlɨ-š-mpɨ-ntɨlʼ</ta>
            <ta e="T766" id="Seg_6693" s="T765">čʼeːlɨ-mɨt</ta>
            <ta e="T767" id="Seg_6694" s="T766">meː</ta>
            <ta e="T768" id="Seg_6695" s="T767">tan</ta>
            <ta e="T769" id="Seg_6696" s="T768">*korɨ-š-ŋɨ-ntɨ</ta>
            <ta e="T770" id="Seg_6697" s="T769">kutta-r-ŋɨ-ntɨ</ta>
            <ta e="T771" id="Seg_6698" s="T770">poː-n-tɨ</ta>
            <ta e="T772" id="Seg_6699" s="T771">kuntɨ</ta>
            <ta e="T773" id="Seg_6700" s="T772">nom-n</ta>
            <ta e="T774" id="Seg_6701" s="T773">nekmɨ</ta>
            <ta e="T775" id="Seg_6702" s="T774">tottɨ-altɨ-kkɨ-l</ta>
            <ta e="T776" id="Seg_6703" s="T775">kə-m</ta>
            <ta e="T777" id="Seg_6704" s="T776">pötpɨlʼ</ta>
            <ta e="T778" id="Seg_6705" s="T777">üttɨ-sä</ta>
            <ta e="T779" id="Seg_6706" s="T778">muntɨk-t</ta>
            <ta e="T780" id="Seg_6707" s="T779">meːšımɨt</ta>
            <ta e="T781" id="Seg_6708" s="T780">onäntɨ</ta>
            <ta e="T782" id="Seg_6709" s="T781">ılqɨn</ta>
            <ta e="T783" id="Seg_6710" s="T782">qo-ntɨr-ŋɨ-ntɨ</ta>
            <ta e="T784" id="Seg_6711" s="T783">montɨ</ta>
            <ta e="T785" id="Seg_6712" s="T784">əːtɨ-m</ta>
            <ta e="T786" id="Seg_6713" s="T785">toː</ta>
            <ta e="T787" id="Seg_6714" s="T786">čʼattɨ-ɛntɨ-l</ta>
            <ta e="T788" id="Seg_6715" s="T787">qo-ntɨr-sɨ-l</ta>
            <ta e="T789" id="Seg_6716" s="T788">qaj</ta>
            <ta e="T790" id="Seg_6717" s="T789">kun</ta>
            <ta e="T791" id="Seg_6718" s="T790">ɛːmä</ta>
            <ta e="T792" id="Seg_6719" s="T791">təttɨ-k</ta>
            <ta e="T793" id="Seg_6720" s="T792">caːr-ɨ-n</ta>
            <ta e="T794" id="Seg_6721" s="T793">nälʼa-m</ta>
            <ta e="T795" id="Seg_6722" s="T794">ilʼmatɨlʼ</ta>
            <ta e="T796" id="Seg_6723" s="T795">tan</ta>
            <ta e="T798" id="Seg_6724" s="T797">irä-tɨ</ta>
            <ta e="T799" id="Seg_6725" s="T798">irä-tɨ</ta>
            <ta e="T800" id="Seg_6726" s="T799">onäk</ta>
            <ta e="T801" id="Seg_6727" s="T800">neː-mɨ</ta>
            <ta e="T802" id="Seg_6728" s="T801">zoločʼenɨ-lʼ</ta>
            <ta e="T803" id="Seg_6729" s="T802">ɔːmtɨ-lʼa</ta>
            <ta e="T804" id="Seg_6730" s="T803">wəšɨ-kkɨ-ntɨ</ta>
            <ta e="T805" id="Seg_6731" s="T804">korɨ-lʼ</ta>
            <ta e="T806" id="Seg_6732" s="T805">pi-n</ta>
            <ta e="T807" id="Seg_6733" s="T806">tan</ta>
            <ta e="T808" id="Seg_6734" s="T807">pürɨ-lʼ</ta>
            <ta e="T809" id="Seg_6735" s="T808">wəntɨ-lʼ</ta>
            <ta e="T810" id="Seg_6736" s="T809">čʼeːlɨ-lʼ</ta>
            <ta e="T811" id="Seg_6737" s="T810">sajɨ-lʼ</ta>
            <ta e="T812" id="Seg_6738" s="T811">aj</ta>
            <ta e="T813" id="Seg_6739" s="T812">man</ta>
            <ta e="T814" id="Seg_6740" s="T813">serlɨ</ta>
            <ta e="T815" id="Seg_6741" s="T814">kɨkɨ-lä</ta>
            <ta e="T816" id="Seg_6742" s="T815">qıšqä-t</ta>
            <ta e="T817" id="Seg_6743" s="T816">täntɨ</ta>
            <ta e="T818" id="Seg_6744" s="T817">mantɨ-mpɨ-tɨt</ta>
            <ta e="T820" id="Seg_6745" s="T819">näčʼčʼä-n</ta>
            <ta e="T821" id="Seg_6746" s="T820">čʼontɨ</ta>
            <ta e="T822" id="Seg_6747" s="T821">küŋɨ-lʼ</ta>
            <ta e="T823" id="Seg_6748" s="T822">kɨ-qɨn</ta>
            <ta e="T824" id="Seg_6749" s="T823">toːp</ta>
            <ta e="T825" id="Seg_6750" s="T824">ɛː-ŋɨ</ta>
            <ta e="T826" id="Seg_6751" s="T825">pirqɨ</ta>
            <ta e="T827" id="Seg_6752" s="T826">toanʼ</ta>
            <ta e="T828" id="Seg_6753" s="T827">nɨmtɨ</ta>
            <ta e="T829" id="Seg_6754" s="T828">korɨ</ta>
            <ta e="T830" id="Seg_6755" s="T829">mü</ta>
            <ta e="T831" id="Seg_6756" s="T830">pi-tɨ</ta>
            <ta e="T832" id="Seg_6757" s="T831">matɨ</ta>
            <ta e="T833" id="Seg_6758" s="T832">təttɨ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6759" s="T0">tsar.[NOM]</ta>
            <ta e="T2" id="Seg_6760" s="T1">wife-OBL.3SG-COM</ta>
            <ta e="T3" id="Seg_6761" s="T2">kiss-FRQ-PST-3SG.O</ta>
            <ta e="T4" id="Seg_6762" s="T3">write-FRQ-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_6763" s="T4">far</ta>
            <ta e="T6" id="Seg_6764" s="T5">road-ILL</ta>
            <ta e="T7" id="Seg_6765" s="T6">dress-TR-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_6766" s="T7">window-GEN</ta>
            <ta e="T9" id="Seg_6767" s="T8">near-ILL</ta>
            <ta e="T10" id="Seg_6768" s="T9">wife.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_6769" s="T10">(s)he-EP-ACC</ta>
            <ta e="T12" id="Seg_6770" s="T11">wait-INF</ta>
            <ta e="T13" id="Seg_6771" s="T12">oneself.3SG.[NOM]</ta>
            <ta e="T14" id="Seg_6772" s="T13">sit.down.[3SG.S]</ta>
            <ta e="T15" id="Seg_6773" s="T14">morning-GEN</ta>
            <ta e="T16" id="Seg_6774" s="T15">out-ADV.EL</ta>
            <ta e="T17" id="Seg_6775" s="T16">evening-GEN</ta>
            <ta e="T18" id="Seg_6776" s="T17">up.to</ta>
            <ta e="T19" id="Seg_6777" s="T18">wait-HAB-3SG.O</ta>
            <ta e="T20" id="Seg_6778" s="T19">wait-HAB-3SG.O</ta>
            <ta e="T21" id="Seg_6779" s="T20">always</ta>
            <ta e="T22" id="Seg_6780" s="T21">%%-ILL</ta>
            <ta e="T23" id="Seg_6781" s="T22">give.a.look-DUR.[3SG.S]</ta>
            <ta e="T24" id="Seg_6782" s="T23">(s)he.[NOM]</ta>
            <ta e="T25" id="Seg_6783" s="T24">eye-ADJZ</ta>
            <ta e="T26" id="Seg_6784" s="T25">all.the.rest.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_6785" s="T26">give.a.look-INCH-DUR-CVB</ta>
            <ta e="T28" id="Seg_6786" s="T27">hurt-INCH-PST-3PL</ta>
            <ta e="T29" id="Seg_6787" s="T28">who-ADJZ</ta>
            <ta e="T30" id="Seg_6788" s="T29">INDEF2-ADJZ</ta>
            <ta e="T31" id="Seg_6789" s="T30">morning-ADJZ</ta>
            <ta e="T32" id="Seg_6790" s="T31">dawn-GEN-3SG</ta>
            <ta e="T33" id="Seg_6791" s="T32">before-EL</ta>
            <ta e="T34" id="Seg_6792" s="T33">night-GEN</ta>
            <ta e="T35" id="Seg_6793" s="T34">up.to</ta>
            <ta e="T36" id="Seg_6794" s="T35">other</ta>
            <ta e="T37" id="Seg_6795" s="T36">living.being.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_6796" s="T37">NEG</ta>
            <ta e="T39" id="Seg_6797" s="T38">be.visible.[3SG.S]</ta>
            <ta e="T40" id="Seg_6798" s="T39">only</ta>
            <ta e="T41" id="Seg_6799" s="T40">(s)he.[NOM]</ta>
            <ta e="T42" id="Seg_6800" s="T41">give.a.look-DUR-3SG.O</ta>
            <ta e="T43" id="Seg_6801" s="T42">wind-ADJZ</ta>
            <ta e="T44" id="Seg_6802" s="T43">twirl.[NOM]</ta>
            <ta e="T45" id="Seg_6803" s="T44">curl-DUR.[3SG.S]</ta>
            <ta e="T46" id="Seg_6804" s="T45">snow.[NOM]</ta>
            <ta e="T47" id="Seg_6805" s="T46">snow-CO.[3SG.S]</ta>
            <ta e="T48" id="Seg_6806" s="T47">%%-GEN</ta>
            <ta e="T49" id="Seg_6807" s="T48">on-ILL</ta>
            <ta e="T50" id="Seg_6808" s="T49">earth.[NOM]</ta>
            <ta e="T51" id="Seg_6809" s="T50">all</ta>
            <ta e="T52" id="Seg_6810" s="T51">white</ta>
            <ta e="T53" id="Seg_6811" s="T52">become.[3SG.S]</ta>
            <ta e="T54" id="Seg_6812" s="T53">go.away-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_6813" s="T54">leave-CO.[3SG.S]</ta>
            <ta e="T56" id="Seg_6814" s="T55">one</ta>
            <ta e="T57" id="Seg_6815" s="T56">no(t)</ta>
            <ta e="T58" id="Seg_6816" s="T57">ten</ta>
            <ta e="T59" id="Seg_6817" s="T58">month.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_6818" s="T59">%%-LOC</ta>
            <ta e="T61" id="Seg_6819" s="T60">eye-ACC-3SG</ta>
            <ta e="T62" id="Seg_6820" s="T61">away</ta>
            <ta e="T63" id="Seg_6821" s="T62">NEG</ta>
            <ta e="T64" id="Seg_6822" s="T63">take-TR-3SG.O</ta>
            <ta e="T65" id="Seg_6823" s="T64">take-TR-CO-3SG.O</ta>
            <ta e="T66" id="Seg_6824" s="T65">then</ta>
            <ta e="T67" id="Seg_6825" s="T66">Christmas.Eve-EP-LOC</ta>
            <ta e="T68" id="Seg_6826" s="T67">night-ADV.LOC</ta>
            <ta e="T69" id="Seg_6827" s="T68">(s)he-EP-GEN</ta>
            <ta e="T70" id="Seg_6828" s="T69">give-CO-3SG.O</ta>
            <ta e="T71" id="Seg_6829" s="T70">daughter-ACC</ta>
            <ta e="T72" id="Seg_6830" s="T71">daughter-ACC-3SG</ta>
            <ta e="T73" id="Seg_6831" s="T72">god.[NOM]</ta>
            <ta e="T74" id="Seg_6832" s="T73">god.[NOM]-3SG</ta>
            <ta e="T75" id="Seg_6833" s="T74">gloomy</ta>
            <ta e="T76" id="Seg_6834" s="T75">morning-ADV.LOC</ta>
            <ta e="T77" id="Seg_6835" s="T76">one.needs-ADJZ</ta>
            <ta e="T78" id="Seg_6836" s="T77">guest.[NOM]</ta>
            <ta e="T79" id="Seg_6837" s="T78">night-INSTR</ta>
            <ta e="T80" id="Seg_6838" s="T79">day-INSTR</ta>
            <ta e="T81" id="Seg_6839" s="T80">long</ta>
            <ta e="T82" id="Seg_6840" s="T81">wait-PTCP.PST</ta>
            <ta e="T83" id="Seg_6841" s="T82">far-ADV.EL</ta>
            <ta e="T84" id="Seg_6842" s="T83">further</ta>
            <ta e="T85" id="Seg_6843" s="T84">leave-US-EP-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_6844" s="T85">house-LOC</ta>
            <ta e="T87" id="Seg_6845" s="T86">come-PST.[3SG.S]</ta>
            <ta e="T88" id="Seg_6846" s="T87">tsar.[NOM]-father.[NOM]-3SG</ta>
            <ta e="T89" id="Seg_6847" s="T88">give.a.look-ATTEN-HAB-PST.[3SG.S]</ta>
            <ta e="T90" id="Seg_6848" s="T89">(s)he-EP-ALL</ta>
            <ta e="T91" id="Seg_6849" s="T90">difficult-EP-DIM</ta>
            <ta e="T92" id="Seg_6850" s="T91">come.to.oneself-PST.[3SG.S]</ta>
            <ta e="T93" id="Seg_6851" s="T92">happiness-ACC-3SG</ta>
            <ta e="T94" id="Seg_6852" s="T93">NEG</ta>
            <ta e="T95" id="Seg_6853" s="T94">bear-FRQ-EP-RES-PST-3SG.O</ta>
            <ta e="T96" id="Seg_6854" s="T95">bear-FRQ-PST.[3SG.S]</ta>
            <ta e="T97" id="Seg_6855" s="T96">and</ta>
            <ta e="T98" id="Seg_6856" s="T97">day.[NOM]</ta>
            <ta e="T99" id="Seg_6857" s="T98">middle-ADV.LOC</ta>
            <ta e="T100" id="Seg_6858" s="T99">disappear-PFV-RFL-PST.[3SG.S]</ta>
            <ta e="T101" id="Seg_6859" s="T100">suffer-INCH-DUR-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_6860" s="T101">smoke-INCH-DUR-PST.[3SG.S]</ta>
            <ta e="T103" id="Seg_6861" s="T102">long</ta>
            <ta e="T104" id="Seg_6862" s="T103">tsar.[NOM]</ta>
            <ta e="T105" id="Seg_6863" s="T104">how</ta>
            <ta e="T106" id="Seg_6864" s="T105">be-INF</ta>
            <ta e="T107" id="Seg_6865" s="T106">sinner.[NOM]</ta>
            <ta e="T108" id="Seg_6866" s="T107">be-PST.NAR.[3SG.S]</ta>
            <ta e="T109" id="Seg_6867" s="T108">year.[NOM]-3SG</ta>
            <ta e="T110" id="Seg_6868" s="T109">go.away-CO.[3SG.S]</ta>
            <ta e="T111" id="Seg_6869" s="T110">go.away-CO.[3SG.S]</ta>
            <ta e="T112" id="Seg_6870" s="T111">like</ta>
            <ta e="T113" id="Seg_6871" s="T112">sleeping.[NOM]</ta>
            <ta e="T114" id="Seg_6872" s="T113">new</ta>
            <ta e="T115" id="Seg_6873" s="T114">new</ta>
            <ta e="T116" id="Seg_6874" s="T115">wife-ACC</ta>
            <ta e="T117" id="Seg_6875" s="T116">take-CO-3SG.O</ta>
            <ta e="T118" id="Seg_6876" s="T117">tsar.[NOM]</ta>
            <ta e="T119" id="Seg_6877" s="T118">how</ta>
            <ta e="T120" id="Seg_6878" s="T119">say-INF</ta>
            <ta e="T121" id="Seg_6879" s="T120">daughter-human.being.[NOM]</ta>
            <ta e="T122" id="Seg_6880" s="T121">be-PST.[3SG.S]</ta>
            <ta e="T123" id="Seg_6881" s="T122">oneself.3SG-EP-ADJZ</ta>
            <ta e="T124" id="Seg_6882" s="T123">like</ta>
            <ta e="T125" id="Seg_6883" s="T124">tsar-EP-ADJZ</ta>
            <ta e="T126" id="Seg_6884" s="T125">wife.[NOM]</ta>
            <ta e="T127" id="Seg_6885" s="T126">high</ta>
            <ta e="T128" id="Seg_6886" s="T127">neck.[NOM]</ta>
            <ta e="T129" id="Seg_6887" s="T128">white</ta>
            <ta e="T130" id="Seg_6888" s="T129">be-PST.[3SG.S]</ta>
            <ta e="T131" id="Seg_6889" s="T130">mind-ADJZ</ta>
            <ta e="T132" id="Seg_6890" s="T131">all.the.rest-COM</ta>
            <ta e="T133" id="Seg_6891" s="T132">all-GEN</ta>
            <ta e="T134" id="Seg_6892" s="T133">take-PST-3SG.O</ta>
            <ta e="T135" id="Seg_6893" s="T134">then</ta>
            <ta e="T136" id="Seg_6894" s="T135">and</ta>
            <ta e="T137" id="Seg_6895" s="T136">further</ta>
            <ta e="T138" id="Seg_6896" s="T137">good.VBLZ-PFV-TR-3SG.O</ta>
            <ta e="T139" id="Seg_6897" s="T138">probably-only</ta>
            <ta e="T140" id="Seg_6898" s="T139">further</ta>
            <ta e="T141" id="Seg_6899" s="T140">other</ta>
            <ta e="T142" id="Seg_6900" s="T141">become.[3SG.S]</ta>
            <ta e="T143" id="Seg_6901" s="T142">tent-GEN.3SG</ta>
            <ta e="T144" id="Seg_6902" s="T143">out-ADV.EL</ta>
            <ta e="T145" id="Seg_6903" s="T144">bring-DUR-PST-3SG.O</ta>
            <ta e="T146" id="Seg_6904" s="T145">only</ta>
            <ta e="T147" id="Seg_6905" s="T146">mirror-DIM-ACC</ta>
            <ta e="T148" id="Seg_6906" s="T147">one</ta>
            <ta e="T149" id="Seg_6907" s="T148">mirror-DIM.[NOM]</ta>
            <ta e="T150" id="Seg_6908" s="T149">such-ADJZ</ta>
            <ta e="T151" id="Seg_6909" s="T150">be-PST.[3SG.S]</ta>
            <ta e="T152" id="Seg_6910" s="T151">say-DUR-TR-INF</ta>
            <ta e="T153" id="Seg_6911" s="T152">can-PST.NAR.[3SG.S]</ta>
            <ta e="T154" id="Seg_6912" s="T153">(s)he-COM</ta>
            <ta e="T155" id="Seg_6913" s="T154">one</ta>
            <ta e="T156" id="Seg_6914" s="T155">(s)he.[NOM]</ta>
            <ta e="T157" id="Seg_6915" s="T156">near</ta>
            <ta e="T158" id="Seg_6916" s="T157">be-HAB.[3SG.S]</ta>
            <ta e="T159" id="Seg_6917" s="T158">good</ta>
            <ta e="T160" id="Seg_6918" s="T159">inside-ADVZ</ta>
            <ta e="T161" id="Seg_6919" s="T160">happiness-TR-DUR-HAB.[3SG.S]</ta>
            <ta e="T162" id="Seg_6920" s="T161">(s)he-COM</ta>
            <ta e="T163" id="Seg_6921" s="T162">something.funny-ADVZ</ta>
            <ta e="T164" id="Seg_6922" s="T163">joke-DUR-HAB.[3SG.S]</ta>
            <ta e="T165" id="Seg_6923" s="T164">good.VBLZ-PFV-PTCP.PST</ta>
            <ta e="T166" id="Seg_6924" s="T165">say-DUR-HAB.[3SG.S]</ta>
            <ta e="T167" id="Seg_6925" s="T166">day.[NOM]-1SG</ta>
            <ta e="T168" id="Seg_6926" s="T167">mirror-DIM.[NOM]-1SG</ta>
            <ta e="T169" id="Seg_6927" s="T168">say-IMP.2SG.O</ta>
            <ta e="T170" id="Seg_6928" s="T169">here</ta>
            <ta e="T171" id="Seg_6929" s="T170">day-ADVZ</ta>
            <ta e="T172" id="Seg_6930" s="T171">up</ta>
            <ta e="T173" id="Seg_6931" s="T172">put-IMP.2SG.O</ta>
            <ta e="T174" id="Seg_6932" s="T173">I.NOM</ta>
            <ta e="T175" id="Seg_6933" s="T174">whether</ta>
            <ta e="T176" id="Seg_6934" s="T175">earth-GEN</ta>
            <ta e="T177" id="Seg_6935" s="T176">on-LOC</ta>
            <ta e="T178" id="Seg_6936" s="T177">all-GEN</ta>
            <ta e="T179" id="Seg_6937" s="T178">good</ta>
            <ta e="T180" id="Seg_6938" s="T179">appearance.[NOM]</ta>
            <ta e="T181" id="Seg_6939" s="T180">white</ta>
            <ta e="T182" id="Seg_6940" s="T181">be-CO-1SG.S</ta>
            <ta e="T183" id="Seg_6941" s="T182">mirror-DIM.[NOM]</ta>
            <ta e="T184" id="Seg_6942" s="T183">say-HAB-3SG.O</ta>
            <ta e="T185" id="Seg_6943" s="T184">you.SG.NOM</ta>
            <ta e="T186" id="Seg_6944" s="T185">tsar-EP-GEN</ta>
            <ta e="T187" id="Seg_6945" s="T186">wife.[NOM]</ta>
            <ta e="T188" id="Seg_6946" s="T187">NEG</ta>
            <ta e="T189" id="Seg_6947" s="T188">dispute-1SG.S</ta>
            <ta e="T190" id="Seg_6948" s="T189">you.SG.NOM</ta>
            <ta e="T191" id="Seg_6949" s="T190">%%</ta>
            <ta e="T192" id="Seg_6950" s="T191">one</ta>
            <ta e="T193" id="Seg_6951" s="T192">all-GEN</ta>
            <ta e="T194" id="Seg_6952" s="T193">good</ta>
            <ta e="T195" id="Seg_6953" s="T194">appearance.[NOM]</ta>
            <ta e="T196" id="Seg_6954" s="T195">and</ta>
            <ta e="T197" id="Seg_6955" s="T196">white</ta>
            <ta e="T198" id="Seg_6956" s="T197">be-CO-2SG.S</ta>
            <ta e="T199" id="Seg_6957" s="T198">tsar-EP-GEN</ta>
            <ta e="T200" id="Seg_6958" s="T199">wife.[NOM]</ta>
            <ta e="T201" id="Seg_6959" s="T200">laugh-DRV-CO.[3SG.S]</ta>
            <ta e="T202" id="Seg_6960" s="T201">shoulder-ACC</ta>
            <ta e="T203" id="Seg_6961" s="T202">%%-MULO-CO-3SG.O</ta>
            <ta e="T204" id="Seg_6962" s="T203">eye-OBL.3SG-INSTR</ta>
            <ta e="T205" id="Seg_6963" s="T204">blink-PFV-CO-GEN</ta>
            <ta e="T206" id="Seg_6964" s="T205">finger-OBL.3SG-INSTR</ta>
            <ta e="T207" id="Seg_6965" s="T206">snap.fingers-PFV-CO-3SG.O</ta>
            <ta e="T208" id="Seg_6966" s="T207">hand-ACC</ta>
            <ta e="T209" id="Seg_6967" s="T208">%%-ILL</ta>
            <ta e="T210" id="Seg_6968" s="T209">turn-DUR.[3SG.S]</ta>
            <ta e="T211" id="Seg_6969" s="T210">mirror-DIM-ILL</ta>
            <ta e="T212" id="Seg_6970" s="T211">give.a.look-DUR-CVB</ta>
            <ta e="T213" id="Seg_6971" s="T212">tsar-EP-GEN</ta>
            <ta e="T214" id="Seg_6972" s="T213">daughter.[NOM]</ta>
            <ta e="T215" id="Seg_6973" s="T214">orphan-ADJZ</ta>
            <ta e="T216" id="Seg_6974" s="T215">girl.[NOM]</ta>
            <ta e="T217" id="Seg_6975" s="T216">quiet-ADVZ</ta>
            <ta e="T218" id="Seg_6976" s="T217">middle-ADVZ</ta>
            <ta e="T219" id="Seg_6977" s="T218">force-TRL-IPFV.[3SG.S]</ta>
            <ta e="T220" id="Seg_6978" s="T219">this-GEN</ta>
            <ta e="T221" id="Seg_6979" s="T220">between-ADV.LOC</ta>
            <ta e="T222" id="Seg_6980" s="T221">force-TRL-IPFV.[3SG.S]</ta>
            <ta e="T223" id="Seg_6981" s="T222">lift-US-EP-IPFV.[3SG.S]</ta>
            <ta e="T224" id="Seg_6982" s="T223">force-TRL-INFER-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_6983" s="T224">white</ta>
            <ta e="T226" id="Seg_6984" s="T225">face-ADVZ</ta>
            <ta e="T227" id="Seg_6985" s="T226">black</ta>
            <ta e="T228" id="Seg_6986" s="T227">eye-GEN</ta>
            <ta e="T229" id="Seg_6987" s="T228">arch-ADVZ</ta>
            <ta e="T230" id="Seg_6988" s="T229">%%-CAR-ADVZ</ta>
            <ta e="T231" id="Seg_6989" s="T230">whole-EP-ADVZ</ta>
            <ta e="T232" id="Seg_6990" s="T231">inside-ADVZ</ta>
            <ta e="T233" id="Seg_6991" s="T232">rich.man-ADJZ</ta>
            <ta e="T234" id="Seg_6992" s="T233">human.being.[NOM]-3SG</ta>
            <ta e="T235" id="Seg_6993" s="T234">look.for-MULO-EP-PST.[3SG.S]</ta>
            <ta e="T236" id="Seg_6994" s="T235">king-GEN</ta>
            <ta e="T237" id="Seg_6995" s="T236">son.[NOM]</ta>
            <ta e="T238" id="Seg_6996" s="T237">Yelisej.[NOM]</ta>
            <ta e="T239" id="Seg_6997" s="T238">come-PST.[3SG.S]</ta>
            <ta e="T240" id="Seg_6998" s="T239">human.being.[NOM]</ta>
            <ta e="T241" id="Seg_6999" s="T240">tsar.[NOM]</ta>
            <ta e="T242" id="Seg_7000" s="T241">word-ACC</ta>
            <ta e="T243" id="Seg_7001" s="T242">give-PST-3SG.O</ta>
            <ta e="T244" id="Seg_7002" s="T243">house-LOC</ta>
            <ta e="T245" id="Seg_7003" s="T244">take-PTCP.NEC</ta>
            <ta e="T246" id="Seg_7004" s="T245">as.if</ta>
            <ta e="T247" id="Seg_7005" s="T246">be-CO.[3SG.S]</ta>
            <ta e="T248" id="Seg_7006" s="T247">seven</ta>
            <ta e="T249" id="Seg_7007" s="T248">buy-ABST-ADJZ</ta>
            <ta e="T250" id="Seg_7008" s="T249">town-PL.[NOM]</ta>
            <ta e="T251" id="Seg_7009" s="T250">and</ta>
            <ta e="T252" id="Seg_7010" s="T251">hundred</ta>
            <ta e="T253" id="Seg_7011" s="T252">and</ta>
            <ta e="T254" id="Seg_7012" s="T253">four-ten-EP-ADJZ</ta>
            <ta e="T255" id="Seg_7013" s="T254">house-EP-DIM-PL.[NOM]</ta>
            <ta e="T256" id="Seg_7014" s="T255">girl-PL-EP-ALL</ta>
            <ta e="T257" id="Seg_7015" s="T256">gather-MULS-INF</ta>
            <ta e="T258" id="Seg_7016" s="T257">tsar-EP-GEN</ta>
            <ta e="T259" id="Seg_7017" s="T258">wife.[NOM]</ta>
            <ta e="T260" id="Seg_7018" s="T259">dress-IPFV-HAB-PTCP.PRS</ta>
            <ta e="T261" id="Seg_7019" s="T260">mirror-DIM-OBL.3SG-COM</ta>
            <ta e="T262" id="Seg_7020" s="T261">own.3SG</ta>
            <ta e="T263" id="Seg_7021" s="T262">word-1SG.S.[NOM]</ta>
            <ta e="T264" id="Seg_7022" s="T263">(s)he-EP-GEN</ta>
            <ta e="T265" id="Seg_7023" s="T264">throw-HAB-PST-3SG.O</ta>
            <ta e="T266" id="Seg_7024" s="T265">I.NOM</ta>
            <ta e="T267" id="Seg_7025" s="T266">whether</ta>
            <ta e="T268" id="Seg_7026" s="T267">say-IMP.2SG.O</ta>
            <ta e="T269" id="Seg_7027" s="T268">I.ALL</ta>
            <ta e="T270" id="Seg_7028" s="T269">all</ta>
            <ta e="T271" id="Seg_7029" s="T270">appearance-INSTR</ta>
            <ta e="T272" id="Seg_7030" s="T271">and</ta>
            <ta e="T273" id="Seg_7031" s="T272">white</ta>
            <ta e="T274" id="Seg_7032" s="T273">be-CO-1SG.S</ta>
            <ta e="T275" id="Seg_7033" s="T274">what.[NOM]</ta>
            <ta e="T276" id="Seg_7034" s="T275">%%</ta>
            <ta e="T277" id="Seg_7035" s="T276">mirror-DIM.[NOM]</ta>
            <ta e="T278" id="Seg_7036" s="T277">speak-DUR-3SG.O</ta>
            <ta e="T279" id="Seg_7037" s="T278">appearance-INSTR</ta>
            <ta e="T280" id="Seg_7038" s="T279">be-CO-2SG.S</ta>
            <ta e="T281" id="Seg_7039" s="T280">NEG</ta>
            <ta e="T282" id="Seg_7040" s="T281">dispute-1SG.S</ta>
            <ta e="T283" id="Seg_7041" s="T282">tsar-EP-GEN</ta>
            <ta e="T284" id="Seg_7042" s="T283">daughter.[NOM]</ta>
            <ta e="T285" id="Seg_7043" s="T284">%%</ta>
            <ta e="T286" id="Seg_7044" s="T285">all-GEN</ta>
            <ta e="T287" id="Seg_7045" s="T286">appearance-INSTR</ta>
            <ta e="T288" id="Seg_7046" s="T287">and</ta>
            <ta e="T289" id="Seg_7047" s="T288">white</ta>
            <ta e="T290" id="Seg_7048" s="T289">be-CO.[3SG.S]</ta>
            <ta e="T291" id="Seg_7049" s="T290">tsar-EP-GEN</ta>
            <ta e="T292" id="Seg_7050" s="T291">wife.[NOM]</ta>
            <ta e="T293" id="Seg_7051" s="T292">away</ta>
            <ta e="T294" id="Seg_7052" s="T293">jump.[3SG.S]</ta>
            <ta e="T295" id="Seg_7053" s="T294">and</ta>
            <ta e="T296" id="Seg_7054" s="T295">swing-MOM-CO.[3SG.S]</ta>
            <ta e="T297" id="Seg_7055" s="T296">hand-DIM-OBL.3SG-INSTR</ta>
            <ta e="T298" id="Seg_7056" s="T297">mirror-DIM-ACC</ta>
            <ta e="T299" id="Seg_7057" s="T298">slap-TR-CO-3SG.O</ta>
            <ta e="T300" id="Seg_7058" s="T299">heel-DIM-OBL.3SG-INSTR</ta>
            <ta e="T301" id="Seg_7059" s="T300">stamp-MOM-CO.[3SG.S]</ta>
            <ta e="T302" id="Seg_7060" s="T301">there</ta>
            <ta e="T303" id="Seg_7061" s="T302">ah</ta>
            <ta e="T304" id="Seg_7062" s="T303">you.SG.NOM</ta>
            <ta e="T305" id="Seg_7063" s="T304">such-ADJZ</ta>
            <ta e="T306" id="Seg_7064" s="T305">glass-CO-2SG.S</ta>
            <ta e="T307" id="Seg_7065" s="T306">you.SG.NOM</ta>
            <ta e="T308" id="Seg_7066" s="T307">here</ta>
            <ta e="T309" id="Seg_7067" s="T308">lies-TR-2SG.S</ta>
            <ta e="T310" id="Seg_7068" s="T309">I.GEN</ta>
            <ta e="T311" id="Seg_7069" s="T310">about</ta>
            <ta e="T312" id="Seg_7070" s="T311">(s)he.[NOM]</ta>
            <ta e="T313" id="Seg_7071" s="T312">how</ta>
            <ta e="T314" id="Seg_7072" s="T313">give.a.look-COND-2SG.S</ta>
            <ta e="T315" id="Seg_7073" s="T314">I.COM</ta>
            <ta e="T316" id="Seg_7074" s="T315">I.NOM</ta>
            <ta e="T317" id="Seg_7075" s="T316">(s)he-EP-GEN</ta>
            <ta e="T318" id="Seg_7076" s="T317">lies-PL.[NOM]</ta>
            <ta e="T319" id="Seg_7077" s="T318">quiet.VBLZ-MULO-FUT-1SG.O</ta>
            <ta e="T320" id="Seg_7078" s="T319">see-DRV-CO-2PL</ta>
            <ta e="T321" id="Seg_7079" s="T320">force-TRL-CO.[3SG.S]</ta>
            <ta e="T322" id="Seg_7080" s="T321">whether</ta>
            <ta e="T323" id="Seg_7081" s="T322">how</ta>
            <ta e="T324" id="Seg_7082" s="T323">NEG</ta>
            <ta e="T325" id="Seg_7083" s="T324">white</ta>
            <ta e="T326" id="Seg_7084" s="T325">be-CO.[3SG.S]</ta>
            <ta e="T327" id="Seg_7085" s="T326">mother.[NOM]-3SG</ta>
            <ta e="T328" id="Seg_7086" s="T327">stomach-ADJZ</ta>
            <ta e="T329" id="Seg_7087" s="T328">amount.[NOM]</ta>
            <ta e="T330" id="Seg_7088" s="T329">sit-PST.[3SG.S]</ta>
            <ta e="T331" id="Seg_7089" s="T330">only</ta>
            <ta e="T332" id="Seg_7090" s="T331">snow-ILL</ta>
            <ta e="T333" id="Seg_7091" s="T332">give.a.look-MOM-DUR-PST.[3SG.S]</ta>
            <ta e="T334" id="Seg_7092" s="T333">come.on</ta>
            <ta e="T335" id="Seg_7093" s="T334">say-IMP.2SG.O</ta>
            <ta e="T336" id="Seg_7094" s="T335">how</ta>
            <ta e="T337" id="Seg_7095" s="T336">I.GEN</ta>
            <ta e="T338" id="Seg_7096" s="T337">out-ADV.EL</ta>
            <ta e="T339" id="Seg_7097" s="T338">then</ta>
            <ta e="T340" id="Seg_7098" s="T339">afterwards</ta>
            <ta e="T341" id="Seg_7099" s="T340">(s)he.[NOM]</ta>
            <ta e="T342" id="Seg_7100" s="T341">appearance-INSTR</ta>
            <ta e="T343" id="Seg_7101" s="T342">be-FUT.[3SG.S]</ta>
            <ta e="T344" id="Seg_7102" s="T343">up</ta>
            <ta e="T345" id="Seg_7103" s="T344">say-IMP.2SG.O</ta>
            <ta e="T346" id="Seg_7104" s="T345">I.NOM</ta>
            <ta e="T347" id="Seg_7105" s="T346">%%</ta>
            <ta e="T348" id="Seg_7106" s="T347">%%-1SG.S</ta>
            <ta e="T349" id="Seg_7107" s="T348">horn-ADJZ</ta>
            <ta e="T350" id="Seg_7108" s="T349">head.[NOM]</ta>
            <ta e="T351" id="Seg_7109" s="T350">something.[NOM]-3SG</ta>
            <ta e="T352" id="Seg_7110" s="T351">turn-TR-IMP.2SG.O</ta>
            <ta e="T353" id="Seg_7111" s="T352">INDEF3</ta>
            <ta e="T354" id="Seg_7112" s="T353">earth.[NOM]</ta>
            <ta e="T355" id="Seg_7113" s="T354">all</ta>
            <ta e="T356" id="Seg_7114" s="T355">I.COM</ta>
            <ta e="T357" id="Seg_7115" s="T356">NEG.EX.[3SG.S]</ta>
            <ta e="T358" id="Seg_7116" s="T357">%%-ADJZ</ta>
            <ta e="T359" id="Seg_7117" s="T358">something.[NOM]</ta>
            <ta e="T360" id="Seg_7118" s="T359">such-ADVZ</ta>
            <ta e="T361" id="Seg_7119" s="T360">probably</ta>
            <ta e="T362" id="Seg_7120" s="T361">mirror-DIM.[NOM]</ta>
            <ta e="T363" id="Seg_7121" s="T362">say-CO-3SG.O</ta>
            <ta e="T364" id="Seg_7122" s="T363">tsar-EP-GEN</ta>
            <ta e="T365" id="Seg_7123" s="T364">daughter.[NOM]</ta>
            <ta e="T366" id="Seg_7124" s="T365">%%</ta>
            <ta e="T367" id="Seg_7125" s="T366">%%</ta>
            <ta e="T368" id="Seg_7126" s="T367">appearance-INSTR</ta>
            <ta e="T369" id="Seg_7127" s="T368">and</ta>
            <ta e="T370" id="Seg_7128" s="T369">white</ta>
            <ta e="T371" id="Seg_7129" s="T370">be-CO.[3SG.S]</ta>
            <ta e="T372" id="Seg_7130" s="T371">how</ta>
            <ta e="T373" id="Seg_7131" s="T372">make-TR-2SG.O</ta>
            <ta e="T374" id="Seg_7132" s="T373">soul-ACC-3SG</ta>
            <ta e="T375" id="Seg_7133" s="T374">eat-CVB</ta>
            <ta e="T376" id="Seg_7134" s="T375">mirror-DIM-ACC</ta>
            <ta e="T377" id="Seg_7135" s="T376">upwards</ta>
            <ta e="T378" id="Seg_7136" s="T377">throw-3SG.O</ta>
            <ta e="T379" id="Seg_7137" s="T378">Chernavka-ACC-3SG</ta>
            <ta e="T380" id="Seg_7138" s="T379">call-CO-3SG.O</ta>
            <ta e="T381" id="Seg_7139" s="T380">such-ADVZ</ta>
            <ta e="T382" id="Seg_7140" s="T381">(s)he-ILL</ta>
            <ta e="T383" id="Seg_7141" s="T382">go-CAUS-CO-3SG.O</ta>
            <ta e="T384" id="Seg_7142" s="T383">oneself.3SG.[NOM]</ta>
            <ta e="T385" id="Seg_7143" s="T384">of.mudroom-ADJZ</ta>
            <ta e="T386" id="Seg_7144" s="T385">girl.[NOM]</ta>
            <ta e="T387" id="Seg_7145" s="T386">that</ta>
            <ta e="T388" id="Seg_7146" s="T387">tsar-EP-GEN</ta>
            <ta e="T389" id="Seg_7147" s="T388">daughter-ACC</ta>
            <ta e="T390" id="Seg_7148" s="T389">forest-GEN</ta>
            <ta e="T391" id="Seg_7149" s="T390">inside-ILL</ta>
            <ta e="T392" id="Seg_7150" s="T391">leave-DRV-TR-TR-2SG.O</ta>
            <ta e="T393" id="Seg_7151" s="T392">wind.round-IPFV-PTCP.PST</ta>
            <ta e="T394" id="Seg_7152" s="T393">live-CVB</ta>
            <ta e="T395" id="Seg_7153" s="T394">and</ta>
            <ta e="T396" id="Seg_7154" s="T395">leave-IPFV-PTCP.PST</ta>
            <ta e="T397" id="Seg_7155" s="T396">there-ADV.LOC</ta>
            <ta e="T398" id="Seg_7156" s="T397">pine-GEN</ta>
            <ta e="T399" id="Seg_7157" s="T398">down-ADV.LOC</ta>
            <ta e="T400" id="Seg_7158" s="T399">long</ta>
            <ta e="T401" id="Seg_7159" s="T400">living.being-EP-PL.[NOM]</ta>
            <ta e="T402" id="Seg_7160" s="T401">eat-SUP.2/3PL</ta>
            <ta e="T403" id="Seg_7161" s="T402">devil.[NOM]</ta>
            <ta e="T404" id="Seg_7162" s="T403">apparently</ta>
            <ta e="T405" id="Seg_7163" s="T404">dispute-FUT.[3SG.S]</ta>
            <ta e="T406" id="Seg_7164" s="T405">get.angry-PTCP.PRS</ta>
            <ta e="T407" id="Seg_7165" s="T406">woman-COM</ta>
            <ta e="T408" id="Seg_7166" s="T407">tsar-EP-GEN</ta>
            <ta e="T409" id="Seg_7167" s="T408">daughter-COM</ta>
            <ta e="T410" id="Seg_7168" s="T409">Chernavka.[NOM]</ta>
            <ta e="T411" id="Seg_7169" s="T410">go.away-IPFV.[3SG.S]</ta>
            <ta e="T412" id="Seg_7170" s="T411">forest-ILL</ta>
            <ta e="T413" id="Seg_7171" s="T412">here</ta>
            <ta e="T414" id="Seg_7172" s="T413">so.much</ta>
            <ta e="T415" id="Seg_7173" s="T414">far</ta>
            <ta e="T416" id="Seg_7174" s="T415">that</ta>
            <ta e="T417" id="Seg_7175" s="T416">leave-TR-PTCP.PRS</ta>
            <ta e="T418" id="Seg_7176" s="T417">tsar-EP-GEN</ta>
            <ta e="T419" id="Seg_7177" s="T418">daughter.[NOM]</ta>
            <ta e="T420" id="Seg_7178" s="T419">then</ta>
            <ta e="T421" id="Seg_7179" s="T420">go.away-TR.[3SG.S]</ta>
            <ta e="T422" id="Seg_7180" s="T421">die-TEMPN-OBL.3SG-COR</ta>
            <ta e="T423" id="Seg_7181" s="T422">be.frightened-DRV-CO.[3SG.S]</ta>
            <ta e="T424" id="Seg_7182" s="T423">pray-TR-CO.[3SG.S]</ta>
            <ta e="T425" id="Seg_7183" s="T424">live-ACTN.[NOM]-1SG</ta>
            <ta e="T426" id="Seg_7184" s="T425">what-LOC</ta>
            <ta e="T427" id="Seg_7185" s="T426">say-IMP.2SG.O</ta>
            <ta e="T428" id="Seg_7186" s="T427">affair.[NOM]-1SG</ta>
            <ta e="T429" id="Seg_7187" s="T428">be-CO.[3SG.S]</ta>
            <ta e="T430" id="Seg_7188" s="T429">daughter-human.being.[NOM]</ta>
            <ta e="T431" id="Seg_7189" s="T430">NEG.IMP</ta>
            <ta e="T432" id="Seg_7190" s="T431">I.ACC</ta>
            <ta e="T433" id="Seg_7191" s="T432">break-US-FRQ-IMP.2SG.S</ta>
            <ta e="T434" id="Seg_7192" s="T433">how</ta>
            <ta e="T435" id="Seg_7193" s="T434">human.being-EP-ACC</ta>
            <ta e="T436" id="Seg_7194" s="T435">see-FUT-1SG.O</ta>
            <ta e="T437" id="Seg_7195" s="T436">you.SG.ACC</ta>
            <ta e="T438" id="Seg_7196" s="T437">what-TRL</ta>
            <ta e="T439" id="Seg_7197" s="T438">want-2SG.S</ta>
            <ta e="T440" id="Seg_7198" s="T439">this-TRL</ta>
            <ta e="T441" id="Seg_7199" s="T440">make-FUT-1SG.S</ta>
            <ta e="T442" id="Seg_7200" s="T441">that-ACC</ta>
            <ta e="T443" id="Seg_7201" s="T442">soul-INSTR</ta>
            <ta e="T444" id="Seg_7202" s="T443">(s)he.[NOM]-3SG</ta>
            <ta e="T445" id="Seg_7203" s="T444">like-CVB</ta>
            <ta e="T446" id="Seg_7204" s="T445">NEG</ta>
            <ta e="T447" id="Seg_7205" s="T446">wind.round-PST-3SG.O</ta>
            <ta e="T448" id="Seg_7206" s="T447">NEG</ta>
            <ta e="T449" id="Seg_7207" s="T448">kill-PST-3SG.O</ta>
            <ta e="T450" id="Seg_7208" s="T449">let.go-PST-3SG.O</ta>
            <ta e="T451" id="Seg_7209" s="T450">such-ADVZ</ta>
            <ta e="T452" id="Seg_7210" s="T451">say-CVB</ta>
            <ta e="T453" id="Seg_7211" s="T452">leave-IMP.2SG.S</ta>
            <ta e="T454" id="Seg_7212" s="T453">god.[NOM]</ta>
            <ta e="T455" id="Seg_7213" s="T454">you.SG.ACC</ta>
            <ta e="T456" id="Seg_7214" s="T455">here</ta>
            <ta e="T457" id="Seg_7215" s="T456">keep-3SG.O</ta>
            <ta e="T458" id="Seg_7216" s="T457">then</ta>
            <ta e="T459" id="Seg_7217" s="T458">oneself.3SG.[NOM]</ta>
            <ta e="T460" id="Seg_7218" s="T459">home</ta>
            <ta e="T461" id="Seg_7219" s="T460">come-PST.[3SG.S]</ta>
            <ta e="T462" id="Seg_7220" s="T461">what.[NOM]</ta>
            <ta e="T463" id="Seg_7221" s="T462">tsar-EP-GEN</ta>
            <ta e="T464" id="Seg_7222" s="T463">wife.[NOM]</ta>
            <ta e="T465" id="Seg_7223" s="T464">say-CO-3SG.O</ta>
            <ta e="T466" id="Seg_7224" s="T465">appearance-PROPR-ADJZ</ta>
            <ta e="T467" id="Seg_7225" s="T466">daughter-human.being.[NOM]</ta>
            <ta e="T468" id="Seg_7226" s="T467">where</ta>
            <ta e="T469" id="Seg_7227" s="T468">be-CO.[3SG.S]</ta>
            <ta e="T470" id="Seg_7228" s="T469">friend-CAR-ADVZ</ta>
            <ta e="T471" id="Seg_7229" s="T470">forest-ADV.LOC</ta>
            <ta e="T472" id="Seg_7230" s="T471">stand.[3SG.S]</ta>
            <ta e="T473" id="Seg_7231" s="T472">(s)he.[NOM]-3SG</ta>
            <ta e="T474" id="Seg_7232" s="T473">say-CO-3SG.O</ta>
            <ta e="T475" id="Seg_7233" s="T474">(s)he-EP-GEN</ta>
            <ta e="T476" id="Seg_7234" s="T475">bind-DUR.[3SG.S]</ta>
            <ta e="T477" id="Seg_7235" s="T476">short-ADVZ</ta>
            <ta e="T478" id="Seg_7236" s="T477">elbow.[NOM]</ta>
            <ta e="T479" id="Seg_7237" s="T478">living.being-GEN-3SG</ta>
            <ta e="T480" id="Seg_7238" s="T479">wild.animal-GEN</ta>
            <ta e="T481" id="Seg_7239" s="T480">claw-ILL</ta>
            <ta e="T482" id="Seg_7240" s="T481">get.into-RFL-COND.[3SG.S]</ta>
            <ta e="T483" id="Seg_7241" s="T482">few-ADVZ</ta>
            <ta e="T484" id="Seg_7242" s="T483">(s)he.[NOM]</ta>
            <ta e="T485" id="Seg_7243" s="T484">here</ta>
            <ta e="T486" id="Seg_7244" s="T485">feel-HAB-TR-CO-3SG.O</ta>
            <ta e="T487" id="Seg_7245" s="T486">die-FRQ-ABST-GEN</ta>
            <ta e="T488" id="Seg_7246" s="T487">also</ta>
            <ta e="T489" id="Seg_7247" s="T488">INFER</ta>
            <ta e="T490" id="Seg_7248" s="T489">enough.quantity.[NOM]</ta>
            <ta e="T491" id="Seg_7249" s="T490">be-IPFV-INFER.[3SG.S]</ta>
            <ta e="T492" id="Seg_7250" s="T491">speech.[NOM]</ta>
            <ta e="T493" id="Seg_7251" s="T492">go.away-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_7252" s="T493">then</ta>
            <ta e="T495" id="Seg_7253" s="T494">such-ADJZ</ta>
            <ta e="T496" id="Seg_7254" s="T495">tsar-EP-GEN</ta>
            <ta e="T497" id="Seg_7255" s="T496">daughter.[NOM]</ta>
            <ta e="T498" id="Seg_7256" s="T497">get.lost-IPFV-CO.[3SG.S]</ta>
            <ta e="T499" id="Seg_7257" s="T498">ache-FRQ-CO.[3SG.S]</ta>
            <ta e="T500" id="Seg_7258" s="T499">tsar.[NOM]</ta>
            <ta e="T501" id="Seg_7259" s="T500">daughter-OBL.3SG-TRL</ta>
            <ta e="T502" id="Seg_7260" s="T501">king.[NOM]</ta>
            <ta e="T503" id="Seg_7261" s="T502">son.[NOM]</ta>
            <ta e="T504" id="Seg_7262" s="T503">Yelisej.[NOM]</ta>
            <ta e="T505" id="Seg_7263" s="T504">god-ILL</ta>
            <ta e="T506" id="Seg_7264" s="T505">pray-TR-CVB</ta>
            <ta e="T507" id="Seg_7265" s="T506">after</ta>
            <ta e="T508" id="Seg_7266" s="T507">dress-TR.[3SG.S]</ta>
            <ta e="T509" id="Seg_7267" s="T508">and</ta>
            <ta e="T510" id="Seg_7268" s="T509">look.for-CVB</ta>
            <ta e="T511" id="Seg_7269" s="T510">go.away-CO.[3SG.S]</ta>
            <ta e="T512" id="Seg_7270" s="T511">appearance-PROPR-GEN</ta>
            <ta e="T513" id="Seg_7271" s="T512">heart-GEN-3SG</ta>
            <ta e="T514" id="Seg_7272" s="T513">head-ACC</ta>
            <ta e="T515" id="Seg_7273" s="T514">later</ta>
            <ta e="T516" id="Seg_7274" s="T515">take-PTCP.NEC</ta>
            <ta e="T517" id="Seg_7275" s="T516">woman.[NOM]-3SG</ta>
            <ta e="T518" id="Seg_7276" s="T517">living.being-ACC-3SG</ta>
            <ta e="T519" id="Seg_7277" s="T518">and</ta>
            <ta e="T520" id="Seg_7278" s="T519">%%.[NOM]</ta>
            <ta e="T521" id="Seg_7279" s="T520">living.being.[NOM]-3SG</ta>
            <ta e="T522" id="Seg_7280" s="T521">morning-GEN</ta>
            <ta e="T523" id="Seg_7281" s="T522">up.to</ta>
            <ta e="T524" id="Seg_7282" s="T523">forest-GEN</ta>
            <ta e="T525" id="Seg_7283" s="T524">inside-ADV.LOC</ta>
            <ta e="T526" id="Seg_7284" s="T525">get.lost-HAB-DUR-CVB</ta>
            <ta e="T527" id="Seg_7285" s="T526">go-IPFV-INFER.[3SG.S]</ta>
            <ta e="T528" id="Seg_7286" s="T527">between-ADV.LOC</ta>
            <ta e="T529" id="Seg_7287" s="T528">house-EP-GEN</ta>
            <ta e="T530" id="Seg_7288" s="T529">on-ILL</ta>
            <ta e="T531" id="Seg_7289" s="T530">come-PFV-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T532" id="Seg_7290" s="T531">towards</ta>
            <ta e="T533" id="Seg_7291" s="T532">dogs.yelp.[NOM]</ta>
            <ta e="T534" id="Seg_7292" s="T533">(s)he-EP-GEN</ta>
            <ta e="T535" id="Seg_7293" s="T534">bark-PTCP.PRS</ta>
            <ta e="T536" id="Seg_7294" s="T535">yell-MOM-DUR.[3SG.S]</ta>
            <ta e="T537" id="Seg_7295" s="T536">quiet-VBLZ-TR-IPFV.[3SG.S]</ta>
            <ta e="T538" id="Seg_7296" s="T537">fawn-DUR-CVB</ta>
            <ta e="T539" id="Seg_7297" s="T538">happiness-TR-DUR-CVB</ta>
            <ta e="T540" id="Seg_7298" s="T539">gate-PROL</ta>
            <ta e="T541" id="Seg_7299" s="T540">(s)he.[NOM]</ta>
            <ta e="T542" id="Seg_7300" s="T541">here</ta>
            <ta e="T543" id="Seg_7301" s="T542">come.in.[3SG.S]</ta>
            <ta e="T544" id="Seg_7302" s="T543">outdoors-ADV.LOC</ta>
            <ta e="T545" id="Seg_7303" s="T544">quiet-ADVZ</ta>
            <ta e="T546" id="Seg_7304" s="T545">be-CO.[3SG.S]</ta>
            <ta e="T547" id="Seg_7305" s="T546">dogs.yelp.[NOM]</ta>
            <ta e="T548" id="Seg_7306" s="T547">happiness-TR-DUR-CVB</ta>
            <ta e="T549" id="Seg_7307" s="T548">catch.up-CO-3SG.O</ta>
            <ta e="T550" id="Seg_7308" s="T549">and</ta>
            <ta e="T551" id="Seg_7309" s="T550">tsar-EP-GEN</ta>
            <ta e="T552" id="Seg_7310" s="T551">daughter.[NOM]</ta>
            <ta e="T553" id="Seg_7311" s="T552">go-%%-CVB</ta>
            <ta e="T554" id="Seg_7312" s="T553">porch-GEN</ta>
            <ta e="T555" id="Seg_7313" s="T554">on-ILL</ta>
            <ta e="T556" id="Seg_7314" s="T555">climb-IPFV.[3SG.S]</ta>
            <ta e="T557" id="Seg_7315" s="T556">circle-ADJZ</ta>
            <ta e="T558" id="Seg_7316" s="T557">hand-ILL-OBL.3SG</ta>
            <ta e="T559" id="Seg_7317" s="T558">take-INFER-3SG.O</ta>
            <ta e="T560" id="Seg_7318" s="T559">door-PL.[NOM]</ta>
            <ta e="T561" id="Seg_7319" s="T560">middle-DIM.[NOM]</ta>
            <ta e="T562" id="Seg_7320" s="T561">INFER</ta>
            <ta e="T563" id="Seg_7321" s="T562">open-INFER-3SG.O</ta>
            <ta e="T564" id="Seg_7322" s="T563">house-ILL</ta>
            <ta e="T565" id="Seg_7323" s="T564">middle-DIM.[NOM]</ta>
            <ta e="T566" id="Seg_7324" s="T565">here</ta>
            <ta e="T567" id="Seg_7325" s="T566">come.in.[3SG.S]</ta>
            <ta e="T568" id="Seg_7326" s="T567">see-IPFV-DUR.[3SG.S]</ta>
            <ta e="T569" id="Seg_7327" s="T568">see-IPFV-DUR.[3SG.S]</ta>
            <ta e="T570" id="Seg_7328" s="T569">see-IPFV-IPFV.[3SG.S]</ta>
            <ta e="T571" id="Seg_7329" s="T570">carpet-COM</ta>
            <ta e="T572" id="Seg_7330" s="T571">bank-PL.[NOM]</ta>
            <ta e="T573" id="Seg_7331" s="T572">icon-GEN</ta>
            <ta e="T574" id="Seg_7332" s="T573">down-ADV.LOC</ta>
            <ta e="T575" id="Seg_7333" s="T574">oak-EP-ADJZ</ta>
            <ta e="T576" id="Seg_7334" s="T575">table.in.a.tent.[NOM]</ta>
            <ta e="T577" id="Seg_7335" s="T576">ornament.VBLZ-PTCP.PST</ta>
            <ta e="T578" id="Seg_7336" s="T577">lie-ACTN-COM</ta>
            <ta e="T579" id="Seg_7337" s="T578">oven.[NOM]</ta>
            <ta e="T580" id="Seg_7338" s="T579">daughter-human.being.[NOM]</ta>
            <ta e="T581" id="Seg_7339" s="T580">give.a.look-DUR-3SG.O</ta>
            <ta e="T582" id="Seg_7340" s="T581">here</ta>
            <ta e="T583" id="Seg_7341" s="T582">probably</ta>
            <ta e="T584" id="Seg_7342" s="T583">good</ta>
            <ta e="T585" id="Seg_7343" s="T584">human.being-EP-PL.[NOM]</ta>
            <ta e="T586" id="Seg_7344" s="T585">live-3PL</ta>
            <ta e="T587" id="Seg_7345" s="T586">(s)he-EP-PL.[NOM]</ta>
            <ta e="T588" id="Seg_7346" s="T587">NEG</ta>
            <ta e="T589" id="Seg_7347" s="T588">swear-INCH-FUT-3PL</ta>
            <ta e="T590" id="Seg_7348" s="T589">then</ta>
            <ta e="T591" id="Seg_7349" s="T590">between-ADVZ</ta>
            <ta e="T592" id="Seg_7350" s="T591">NEG</ta>
            <ta e="T593" id="Seg_7351" s="T592">who.[NOM]</ta>
            <ta e="T594" id="Seg_7352" s="T593">NEG</ta>
            <ta e="T595" id="Seg_7353" s="T594">be.visible.[3SG.S]</ta>
            <ta e="T596" id="Seg_7354" s="T595">house-EP-ACC</ta>
            <ta e="T597" id="Seg_7355" s="T596">there-PROL</ta>
            <ta e="T598" id="Seg_7356" s="T597">turn-TR-INFER-3SG.O</ta>
            <ta e="T599" id="Seg_7357" s="T598">all</ta>
            <ta e="T600" id="Seg_7358" s="T599">good-ADVZ</ta>
            <ta e="T601" id="Seg_7359" s="T600">%%-TR-PST-3SG.O</ta>
            <ta e="T602" id="Seg_7360" s="T601">icon-GEN</ta>
            <ta e="T603" id="Seg_7361" s="T602">candle-ACC</ta>
            <ta e="T604" id="Seg_7362" s="T603">smoulder-CAUS-INFER-3SG.O</ta>
            <ta e="T605" id="Seg_7363" s="T604">oven-EP-ACC</ta>
            <ta e="T606" id="Seg_7364" s="T605">hot-DIM</ta>
            <ta e="T607" id="Seg_7365" s="T606">set.fire-INFER-3SG.O</ta>
            <ta e="T608" id="Seg_7366" s="T607">sleeping.bench-GEN</ta>
            <ta e="T609" id="Seg_7367" s="T608">on-ILL</ta>
            <ta e="T610" id="Seg_7368" s="T609">climb-CVB</ta>
            <ta e="T611" id="Seg_7369" s="T610">quiet-ADVZ</ta>
            <ta e="T612" id="Seg_7370" s="T611">down</ta>
            <ta e="T613" id="Seg_7371" s="T612">fall-RFL.PFV-INFER.[3SG.S]</ta>
            <ta e="T614" id="Seg_7372" s="T613">day-GEN</ta>
            <ta e="T615" id="Seg_7373" s="T614">middle-ILL</ta>
            <ta e="T616" id="Seg_7374" s="T615">relative.[NOM]</ta>
            <ta e="T617" id="Seg_7375" s="T616">be-PST.[3SG.S]</ta>
            <ta e="T618" id="Seg_7376" s="T617">noise.[NOM]</ta>
            <ta e="T619" id="Seg_7377" s="T618">be.heard-INCH-EP-IPFV.[3SG.S]</ta>
            <ta e="T620" id="Seg_7378" s="T619">space.outside-LOC</ta>
            <ta e="T621" id="Seg_7379" s="T620">seven</ta>
            <ta e="T622" id="Seg_7380" s="T621">strongman-EP-PL.[NOM]</ta>
            <ta e="T623" id="Seg_7381" s="T622">here</ta>
            <ta e="T624" id="Seg_7382" s="T623">come.in-3PL</ta>
            <ta e="T625" id="Seg_7383" s="T624">seven-PL</ta>
            <ta e="T626" id="Seg_7384" s="T625">all</ta>
            <ta e="T627" id="Seg_7385" s="T626">moustache-ADVZ</ta>
            <ta e="T628" id="Seg_7386" s="T627">elder</ta>
            <ta e="T629" id="Seg_7387" s="T628">living.being.[NOM]-3PL</ta>
            <ta e="T630" id="Seg_7388" s="T629">such-ADVZ</ta>
            <ta e="T631" id="Seg_7389" s="T630">say.[3SG.S]</ta>
            <ta e="T632" id="Seg_7390" s="T631">here</ta>
            <ta e="T633" id="Seg_7391" s="T632">hey</ta>
            <ta e="T634" id="Seg_7392" s="T633">whether</ta>
            <ta e="T635" id="Seg_7393" s="T634">become.[3SG.S]</ta>
            <ta e="T636" id="Seg_7394" s="T635">how</ta>
            <ta e="T637" id="Seg_7395" s="T636">good</ta>
            <ta e="T638" id="Seg_7396" s="T637">how</ta>
            <ta e="T639" id="Seg_7397" s="T638">all</ta>
            <ta e="T640" id="Seg_7398" s="T639">appearance-INSTR</ta>
            <ta e="T641" id="Seg_7399" s="T640">be-CO.[3SG.S]</ta>
            <ta e="T642" id="Seg_7400" s="T641">who.[NOM]</ta>
            <ta e="T643" id="Seg_7401" s="T642">INDEF3</ta>
            <ta e="T644" id="Seg_7402" s="T643">house.[NOM]-1PL</ta>
            <ta e="T645" id="Seg_7403" s="T644">%%-PST.NAR-3SG.O</ta>
            <ta e="T646" id="Seg_7404" s="T645">as.if</ta>
            <ta e="T647" id="Seg_7405" s="T646">we.PL.ACC</ta>
            <ta e="T648" id="Seg_7406" s="T647">wait-HAB-3PL</ta>
            <ta e="T649" id="Seg_7407" s="T648">wait-PST.NAR-3PL</ta>
            <ta e="T650" id="Seg_7408" s="T649">who-2SG.S</ta>
            <ta e="T651" id="Seg_7409" s="T650">go.out-IMP.2SG.S</ta>
            <ta e="T652" id="Seg_7410" s="T651">be.visible-TR-IMP.2SG.S</ta>
            <ta e="T653" id="Seg_7411" s="T652">we.PL.ACC</ta>
            <ta e="T654" id="Seg_7412" s="T653">there-ADV.EL</ta>
            <ta e="T655" id="Seg_7413" s="T654">%%-ADVZ</ta>
            <ta e="T656" id="Seg_7414" s="T655">meet-IMP.2SG.S</ta>
            <ta e="T657" id="Seg_7415" s="T656">if</ta>
            <ta e="T658" id="Seg_7416" s="T657">you.SG.NOM</ta>
            <ta e="T659" id="Seg_7417" s="T658">elder</ta>
            <ta e="T660" id="Seg_7418" s="T659">human.being-EP-2SG.S</ta>
            <ta e="T661" id="Seg_7419" s="T660">grandfather-OBL.1PL-TRL</ta>
            <ta e="T662" id="Seg_7420" s="T661">always</ta>
            <ta e="T663" id="Seg_7421" s="T662">be-FUT-2SG.S</ta>
            <ta e="T664" id="Seg_7422" s="T663">if</ta>
            <ta e="T665" id="Seg_7423" s="T664">you.SG.NOM</ta>
            <ta e="T666" id="Seg_7424" s="T665">young</ta>
            <ta e="T667" id="Seg_7425" s="T666">somebody-2SG.S</ta>
            <ta e="T668" id="Seg_7426" s="T667">we.PL.ALL</ta>
            <ta e="T669" id="Seg_7427" s="T668">brother-OBL.1PL-TRL</ta>
            <ta e="T670" id="Seg_7428" s="T669">be-FUT-3SG.O</ta>
            <ta e="T671" id="Seg_7429" s="T670">if</ta>
            <ta e="T672" id="Seg_7430" s="T671">you.SG.NOM</ta>
            <ta e="T673" id="Seg_7431" s="T672">old.woman.[NOM]</ta>
            <ta e="T674" id="Seg_7432" s="T673">invite-FUT-1PL</ta>
            <ta e="T675" id="Seg_7433" s="T674">mother-OBL.1PL-TRL</ta>
            <ta e="T676" id="Seg_7434" s="T675">if</ta>
            <ta e="T677" id="Seg_7435" s="T676">you.SG.NOM</ta>
            <ta e="T678" id="Seg_7436" s="T677">daughter.[NOM]</ta>
            <ta e="T679" id="Seg_7437" s="T678">human.being-EP-2SG.S</ta>
            <ta e="T680" id="Seg_7438" s="T679">we.PL.ALL</ta>
            <ta e="T681" id="Seg_7439" s="T680">sister-OBL.1PL-TRL</ta>
            <ta e="T682" id="Seg_7440" s="T681">be-FUT-3SG.O</ta>
            <ta e="T683" id="Seg_7441" s="T682">tsar-EP-GEN</ta>
            <ta e="T684" id="Seg_7442" s="T683">daughter.[NOM]</ta>
            <ta e="T685" id="Seg_7443" s="T684">down</ta>
            <ta e="T686" id="Seg_7444" s="T685">come.down-IPFV.[3SG.S]</ta>
            <ta e="T687" id="Seg_7445" s="T686">down.VBLZ-DUR-CO.[3SG.S]</ta>
            <ta e="T688" id="Seg_7446" s="T687">greet-IPFV.[3SG.S]</ta>
            <ta e="T689" id="Seg_7447" s="T688">(s)he-EP-PL-EP-COM</ta>
            <ta e="T690" id="Seg_7448" s="T689">waist-GEN-3PL</ta>
            <ta e="T691" id="Seg_7449" s="T690">up.to</ta>
            <ta e="T692" id="Seg_7450" s="T691">incline-PFV-IPFV-CVB</ta>
            <ta e="T693" id="Seg_7451" s="T692">grass.[NOM]</ta>
            <ta e="T694" id="Seg_7452" s="T693">grow.[3SG.S]</ta>
            <ta e="T695" id="Seg_7453" s="T694">red-VBLZ-FRQ-EP-PST.[3SG.S]</ta>
            <ta e="T696" id="Seg_7454" s="T695">such-ADJZ</ta>
            <ta e="T697" id="Seg_7455" s="T696">say-3SG.O</ta>
            <ta e="T698" id="Seg_7456" s="T697">house-ILL-3PL</ta>
            <ta e="T699" id="Seg_7457" s="T698">he.says</ta>
            <ta e="T700" id="Seg_7458" s="T699">come.in-PST-1SG.S</ta>
            <ta e="T701" id="Seg_7459" s="T700">I.NOM</ta>
            <ta e="T702" id="Seg_7460" s="T701">here</ta>
            <ta e="T703" id="Seg_7461" s="T702">INDEF3</ta>
            <ta e="T704" id="Seg_7462" s="T703">I.ACC</ta>
            <ta e="T705" id="Seg_7463" s="T704">invite-ATTEN-2PL</ta>
            <ta e="T706" id="Seg_7464" s="T705">NEG</ta>
            <ta e="T707" id="Seg_7465" s="T706">then</ta>
            <ta e="T708" id="Seg_7466" s="T707">word-PROL-3SG</ta>
            <ta e="T709" id="Seg_7467" s="T708">see-IPFV-3PL</ta>
            <ta e="T710" id="Seg_7468" s="T709">this</ta>
            <ta e="T711" id="Seg_7469" s="T710">but</ta>
            <ta e="T712" id="Seg_7470" s="T711">tsar-EP-GEN</ta>
            <ta e="T713" id="Seg_7471" s="T712">daughter.[NOM]</ta>
            <ta e="T714" id="Seg_7472" s="T713">be-PST.[3SG.S]</ta>
            <ta e="T715" id="Seg_7473" s="T714">corner-DIM-ILL</ta>
            <ta e="T716" id="Seg_7474" s="T715">sit.down-TR-PST-3PL</ta>
            <ta e="T717" id="Seg_7475" s="T716">bread-EP-DIM-INSTR</ta>
            <ta e="T718" id="Seg_7476" s="T717">bread-EP-DIM-INSTR</ta>
            <ta e="T719" id="Seg_7477" s="T718">give-PFV-TR-PST-3PL</ta>
            <ta e="T720" id="Seg_7478" s="T719">shot.glass-ADJZ</ta>
            <ta e="T721" id="Seg_7479" s="T720">amount-DIM.[NOM]</ta>
            <ta e="T722" id="Seg_7480" s="T721">pour-TR-%%-PST-3PL</ta>
            <ta e="T723" id="Seg_7481" s="T722">%%-GEN</ta>
            <ta e="T724" id="Seg_7482" s="T723">on-LOC</ta>
            <ta e="T725" id="Seg_7483" s="T724">bring-%%-PST-3SG.O</ta>
            <ta e="T726" id="Seg_7484" s="T725">(s)he-EP-ADJZ</ta>
            <ta e="T727" id="Seg_7485" s="T726">spirit-EP-ACC</ta>
            <ta e="T728" id="Seg_7486" s="T727">NEG</ta>
            <ta e="T729" id="Seg_7487" s="T728">take-PST-3SG.O</ta>
            <ta e="T730" id="Seg_7488" s="T729">bread-EP-DIM-ACC</ta>
            <ta e="T731" id="Seg_7489" s="T730">%%</ta>
            <ta e="T732" id="Seg_7490" s="T731">break-TR-CVB</ta>
            <ta e="T733" id="Seg_7491" s="T732">small</ta>
            <ta e="T734" id="Seg_7492" s="T733">piece-ACC-3SG</ta>
            <ta e="T735" id="Seg_7493" s="T734">mouth-EP-ILL</ta>
            <ta e="T736" id="Seg_7494" s="T735">take-PST-3SG.O</ta>
            <ta e="T737" id="Seg_7495" s="T736">trace-GEN</ta>
            <ta e="T738" id="Seg_7496" s="T737">out-ADV.EL</ta>
            <ta e="T739" id="Seg_7497" s="T738">relax-SUP.2/3SG</ta>
            <ta e="T740" id="Seg_7498" s="T739">place-TRL</ta>
            <ta e="T741" id="Seg_7499" s="T740">here</ta>
            <ta e="T742" id="Seg_7500" s="T741">ask.for-FRQ-EP-CO.[3SG.S]</ta>
            <ta e="T743" id="Seg_7501" s="T742">leave-TR-PST-3PL</ta>
            <ta e="T744" id="Seg_7502" s="T743">(s)he-EP-PL.[NOM]</ta>
            <ta e="T745" id="Seg_7503" s="T744">girl-ACC</ta>
            <ta e="T746" id="Seg_7504" s="T745">up</ta>
            <ta e="T747" id="Seg_7505" s="T746">day-VBLZ-DUR-PTCP.PRS</ta>
            <ta e="T748" id="Seg_7506" s="T747">inside-ADV.LOC</ta>
            <ta e="T749" id="Seg_7507" s="T748">and</ta>
            <ta e="T750" id="Seg_7508" s="T749">oneself.3SG-ADV.LOC</ta>
            <ta e="T751" id="Seg_7509" s="T750">INFER</ta>
            <ta e="T752" id="Seg_7510" s="T751">there</ta>
            <ta e="T753" id="Seg_7511" s="T752">leave-INFER-3PL</ta>
            <ta e="T754" id="Seg_7512" s="T753">leave-PST-3PL</ta>
            <ta e="T755" id="Seg_7513" s="T754">(s)he-EP-ACC</ta>
            <ta e="T756" id="Seg_7514" s="T755">sleeping-TR-PTCP.PRS</ta>
            <ta e="T757" id="Seg_7515" s="T756">friend-CAR-ADVZ</ta>
            <ta e="T759" id="Seg_7516" s="T758">red</ta>
            <ta e="T760" id="Seg_7517" s="T759">sun-GEN</ta>
            <ta e="T761" id="Seg_7518" s="T760">sun.[NOM]</ta>
            <ta e="T762" id="Seg_7519" s="T761">end-LOC-3SG</ta>
            <ta e="T763" id="Seg_7520" s="T762">young</ta>
            <ta e="T764" id="Seg_7521" s="T763">turn-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T765" id="Seg_7522" s="T764">day-VBLZ-DUR-PTCP.PRS</ta>
            <ta e="T766" id="Seg_7523" s="T765">sun.[NOM]-1PL</ta>
            <ta e="T767" id="Seg_7524" s="T766">we.PL.GEN</ta>
            <ta e="T768" id="Seg_7525" s="T767">you.SG.NOM</ta>
            <ta e="T769" id="Seg_7526" s="T768">turn-US-CO-2SG.S</ta>
            <ta e="T770" id="Seg_7527" s="T769">go-FRQ-CO-2SG.S</ta>
            <ta e="T771" id="Seg_7528" s="T770">year-GEN-3SG</ta>
            <ta e="T772" id="Seg_7529" s="T771">during</ta>
            <ta e="T773" id="Seg_7530" s="T772">sky-GEN</ta>
            <ta e="T774" id="Seg_7531" s="T773">%%</ta>
            <ta e="T775" id="Seg_7532" s="T774">stand-CAUS-HAB-2SG.O</ta>
            <ta e="T776" id="Seg_7533" s="T775">winter-ACC</ta>
            <ta e="T777" id="Seg_7534" s="T776">warm</ta>
            <ta e="T778" id="Seg_7535" s="T777">spring-COM</ta>
            <ta e="T779" id="Seg_7536" s="T778">all-PL.[NOM]</ta>
            <ta e="T780" id="Seg_7537" s="T779">we.PL.ACC</ta>
            <ta e="T781" id="Seg_7538" s="T780">oneself.2SG.[NOM]</ta>
            <ta e="T782" id="Seg_7539" s="T781">below</ta>
            <ta e="T783" id="Seg_7540" s="T782">see-DRV-CO-2SG.S</ta>
            <ta e="T784" id="Seg_7541" s="T783">apparently</ta>
            <ta e="T785" id="Seg_7542" s="T784">word-ACC</ta>
            <ta e="T786" id="Seg_7543" s="T785">away</ta>
            <ta e="T787" id="Seg_7544" s="T786">throw-FUT-2SG.O</ta>
            <ta e="T788" id="Seg_7545" s="T787">see-DRV-PST-2SG.O</ta>
            <ta e="T789" id="Seg_7546" s="T788">whether</ta>
            <ta e="T790" id="Seg_7547" s="T789">where</ta>
            <ta e="T791" id="Seg_7548" s="T790">INDEF2</ta>
            <ta e="T792" id="Seg_7549" s="T791">earth-ADVZ</ta>
            <ta e="T793" id="Seg_7550" s="T792">tsar-EP-GEN</ta>
            <ta e="T794" id="Seg_7551" s="T793">daughter-ACC</ta>
            <ta e="T795" id="Seg_7552" s="T794">young</ta>
            <ta e="T796" id="Seg_7553" s="T795">you.SG.NOM</ta>
            <ta e="T798" id="Seg_7554" s="T797">moon.[NOM]-3SG</ta>
            <ta e="T799" id="Seg_7555" s="T798">moon.[NOM]-3SG</ta>
            <ta e="T800" id="Seg_7556" s="T799">oneself.1SG</ta>
            <ta e="T801" id="Seg_7557" s="T800">living.being.[NOM]-1SG</ta>
            <ta e="T802" id="Seg_7558" s="T801">gilt-ADJZ</ta>
            <ta e="T803" id="Seg_7559" s="T802">horn-DIM.[NOM]</ta>
            <ta e="T804" id="Seg_7560" s="T803">get.up-HAB-2SG.S</ta>
            <ta e="T805" id="Seg_7561" s="T804">deep-ADJZ</ta>
            <ta e="T806" id="Seg_7562" s="T805">night-ADV.LOC</ta>
            <ta e="T807" id="Seg_7563" s="T806">you.SG.NOM</ta>
            <ta e="T808" id="Seg_7564" s="T807">circle-ADJZ</ta>
            <ta e="T809" id="Seg_7565" s="T808">face-ADJZ</ta>
            <ta e="T810" id="Seg_7566" s="T809">day-ADJZ</ta>
            <ta e="T811" id="Seg_7567" s="T810">eye-ADJZ</ta>
            <ta e="T812" id="Seg_7568" s="T811">and</ta>
            <ta e="T813" id="Seg_7569" s="T812">I.GEN</ta>
            <ta e="T814" id="Seg_7570" s="T813">%%.[NOM]</ta>
            <ta e="T815" id="Seg_7571" s="T814">like-CVB</ta>
            <ta e="T816" id="Seg_7572" s="T815">star-PL.[NOM]</ta>
            <ta e="T817" id="Seg_7573" s="T816">you.SG.ALL</ta>
            <ta e="T818" id="Seg_7574" s="T817">give.a.look-DUR-3PL</ta>
            <ta e="T820" id="Seg_7575" s="T819">there-ADV.LOC</ta>
            <ta e="T821" id="Seg_7576" s="T820">middle.[NOM]</ta>
            <ta e="T822" id="Seg_7577" s="T821">stream-ADJZ</ta>
            <ta e="T823" id="Seg_7578" s="T822">river-LOC</ta>
            <ta e="T824" id="Seg_7579" s="T823">edge.[3SG.S]</ta>
            <ta e="T825" id="Seg_7580" s="T824">be-CO.[3SG.S]</ta>
            <ta e="T826" id="Seg_7581" s="T825">high</ta>
            <ta e="T827" id="Seg_7582" s="T826">%%.[NOM]</ta>
            <ta e="T828" id="Seg_7583" s="T827">there</ta>
            <ta e="T829" id="Seg_7584" s="T828">deep</ta>
            <ta e="T830" id="Seg_7585" s="T829">hole.[NOM]</ta>
            <ta e="T831" id="Seg_7586" s="T830">night.[NOM]-3SG</ta>
            <ta e="T832" id="Seg_7587" s="T831">cliff.[3SG.S]</ta>
            <ta e="T833" id="Seg_7588" s="T832">earth.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7589" s="T0">царь.[NOM]</ta>
            <ta e="T2" id="Seg_7590" s="T1">жена-OBL.3SG-COM</ta>
            <ta e="T3" id="Seg_7591" s="T2">целовать-FRQ-PST-3SG.O</ta>
            <ta e="T4" id="Seg_7592" s="T3">писать-FRQ-PST.[3SG.S]</ta>
            <ta e="T5" id="Seg_7593" s="T4">далеко</ta>
            <ta e="T6" id="Seg_7594" s="T5">дорога-ILL</ta>
            <ta e="T7" id="Seg_7595" s="T6">одеть(ся)-TR-CO.[3SG.S]</ta>
            <ta e="T8" id="Seg_7596" s="T7">окно-GEN</ta>
            <ta e="T9" id="Seg_7597" s="T8">рядом-ILL</ta>
            <ta e="T10" id="Seg_7598" s="T9">жена.[NOM]-3SG</ta>
            <ta e="T11" id="Seg_7599" s="T10">он(а)-EP-ACC</ta>
            <ta e="T12" id="Seg_7600" s="T11">подождать-INF</ta>
            <ta e="T13" id="Seg_7601" s="T12">сам.3SG.[NOM]</ta>
            <ta e="T14" id="Seg_7602" s="T13">сесть.[3SG.S]</ta>
            <ta e="T15" id="Seg_7603" s="T14">утро-GEN</ta>
            <ta e="T16" id="Seg_7604" s="T15">из-ADV.EL</ta>
            <ta e="T17" id="Seg_7605" s="T16">вечер-GEN</ta>
            <ta e="T18" id="Seg_7606" s="T17">до</ta>
            <ta e="T19" id="Seg_7607" s="T18">подождать-HAB-3SG.O</ta>
            <ta e="T20" id="Seg_7608" s="T19">подождать-HAB-3SG.O</ta>
            <ta e="T21" id="Seg_7609" s="T20">всегда</ta>
            <ta e="T22" id="Seg_7610" s="T21">%%-ILL</ta>
            <ta e="T23" id="Seg_7611" s="T22">взглянуть-DUR.[3SG.S]</ta>
            <ta e="T24" id="Seg_7612" s="T23">он(а).[NOM]</ta>
            <ta e="T25" id="Seg_7613" s="T24">глаз-ADJZ</ta>
            <ta e="T26" id="Seg_7614" s="T25">всё.прочее.[NOM]-3SG</ta>
            <ta e="T27" id="Seg_7615" s="T26">взглянуть-INCH-DUR-CVB</ta>
            <ta e="T28" id="Seg_7616" s="T27">болеть-INCH-PST-3PL</ta>
            <ta e="T29" id="Seg_7617" s="T28">кто-ADJZ</ta>
            <ta e="T30" id="Seg_7618" s="T29">INDEF2-ADJZ</ta>
            <ta e="T31" id="Seg_7619" s="T30">утро-ADJZ</ta>
            <ta e="T32" id="Seg_7620" s="T31">заря-GEN-3SG</ta>
            <ta e="T33" id="Seg_7621" s="T32">перед-EL</ta>
            <ta e="T34" id="Seg_7622" s="T33">ночь-GEN</ta>
            <ta e="T35" id="Seg_7623" s="T34">до</ta>
            <ta e="T36" id="Seg_7624" s="T35">остальной</ta>
            <ta e="T37" id="Seg_7625" s="T36">живое.существо.[NOM]-3SG</ta>
            <ta e="T38" id="Seg_7626" s="T37">NEG</ta>
            <ta e="T39" id="Seg_7627" s="T38">виднеться.[3SG.S]</ta>
            <ta e="T40" id="Seg_7628" s="T39">только</ta>
            <ta e="T41" id="Seg_7629" s="T40">он(а).[NOM]</ta>
            <ta e="T42" id="Seg_7630" s="T41">взглянуть-DUR-3SG.O</ta>
            <ta e="T43" id="Seg_7631" s="T42">ветер-ADJZ</ta>
            <ta e="T44" id="Seg_7632" s="T43">кручение.[NOM]</ta>
            <ta e="T45" id="Seg_7633" s="T44">виться-DUR.[3SG.S]</ta>
            <ta e="T46" id="Seg_7634" s="T45">снег.[NOM]</ta>
            <ta e="T47" id="Seg_7635" s="T46">снежить-CO.[3SG.S]</ta>
            <ta e="T48" id="Seg_7636" s="T47">%%-GEN</ta>
            <ta e="T49" id="Seg_7637" s="T48">на-ILL</ta>
            <ta e="T50" id="Seg_7638" s="T49">земля.[NOM]</ta>
            <ta e="T51" id="Seg_7639" s="T50">всё</ta>
            <ta e="T52" id="Seg_7640" s="T51">белый</ta>
            <ta e="T53" id="Seg_7641" s="T52">стать.[3SG.S]</ta>
            <ta e="T54" id="Seg_7642" s="T53">уйти-CO.[3SG.S]</ta>
            <ta e="T55" id="Seg_7643" s="T54">отправиться-CO.[3SG.S]</ta>
            <ta e="T56" id="Seg_7644" s="T55">один</ta>
            <ta e="T57" id="Seg_7645" s="T56">нет</ta>
            <ta e="T58" id="Seg_7646" s="T57">десять</ta>
            <ta e="T59" id="Seg_7647" s="T58">месяц.[NOM]-3SG</ta>
            <ta e="T60" id="Seg_7648" s="T59">%%-LOC</ta>
            <ta e="T61" id="Seg_7649" s="T60">глаз-ACC-3SG</ta>
            <ta e="T62" id="Seg_7650" s="T61">прочь</ta>
            <ta e="T63" id="Seg_7651" s="T62">NEG</ta>
            <ta e="T64" id="Seg_7652" s="T63">взять-TR-3SG.O</ta>
            <ta e="T65" id="Seg_7653" s="T64">взять-TR-CO-3SG.O</ta>
            <ta e="T66" id="Seg_7654" s="T65">затем</ta>
            <ta e="T67" id="Seg_7655" s="T66">сочельник-EP-LOC</ta>
            <ta e="T68" id="Seg_7656" s="T67">ночь-ADV.LOC</ta>
            <ta e="T69" id="Seg_7657" s="T68">он(а)-EP-GEN</ta>
            <ta e="T70" id="Seg_7658" s="T69">дать-CO-3SG.O</ta>
            <ta e="T71" id="Seg_7659" s="T70">дочь-ACC</ta>
            <ta e="T72" id="Seg_7660" s="T71">дочь-ACC-3SG</ta>
            <ta e="T73" id="Seg_7661" s="T72">бог.[NOM]</ta>
            <ta e="T74" id="Seg_7662" s="T73">бог.[NOM]-3SG</ta>
            <ta e="T75" id="Seg_7663" s="T74">сумрачный</ta>
            <ta e="T76" id="Seg_7664" s="T75">утро-ADV.LOC</ta>
            <ta e="T77" id="Seg_7665" s="T76">нужно-ADJZ</ta>
            <ta e="T78" id="Seg_7666" s="T77">гость.[NOM]</ta>
            <ta e="T79" id="Seg_7667" s="T78">ночь-INSTR</ta>
            <ta e="T80" id="Seg_7668" s="T79">день-INSTR</ta>
            <ta e="T81" id="Seg_7669" s="T80">долго</ta>
            <ta e="T82" id="Seg_7670" s="T81">подождать-PTCP.PST</ta>
            <ta e="T83" id="Seg_7671" s="T82">далеко-ADV.EL</ta>
            <ta e="T84" id="Seg_7672" s="T83">дальше</ta>
            <ta e="T85" id="Seg_7673" s="T84">отправиться-US-EP-PST.[3SG.S]</ta>
            <ta e="T86" id="Seg_7674" s="T85">дом-LOC</ta>
            <ta e="T87" id="Seg_7675" s="T86">прийти-PST.[3SG.S]</ta>
            <ta e="T88" id="Seg_7676" s="T87">царь.[NOM]-отец.[NOM]-3SG</ta>
            <ta e="T89" id="Seg_7677" s="T88">взглянуть-ATTEN-HAB-PST.[3SG.S]</ta>
            <ta e="T90" id="Seg_7678" s="T89">он(а)-EP-ALL</ta>
            <ta e="T91" id="Seg_7679" s="T90">трудный-EP-DIM</ta>
            <ta e="T92" id="Seg_7680" s="T91">очнуться-PST.[3SG.S]</ta>
            <ta e="T93" id="Seg_7681" s="T92">радость-ACC-3SG</ta>
            <ta e="T94" id="Seg_7682" s="T93">NEG</ta>
            <ta e="T95" id="Seg_7683" s="T94">стерпеть-FRQ-EP-RES-PST-3SG.O</ta>
            <ta e="T96" id="Seg_7684" s="T95">стерпеть-FRQ-PST.[3SG.S]</ta>
            <ta e="T97" id="Seg_7685" s="T96">и</ta>
            <ta e="T98" id="Seg_7686" s="T97">день.[NOM]</ta>
            <ta e="T99" id="Seg_7687" s="T98">середина-ADV.LOC</ta>
            <ta e="T100" id="Seg_7688" s="T99">исчезнуть-PFV-RFL-PST.[3SG.S]</ta>
            <ta e="T101" id="Seg_7689" s="T100">мучиться-INCH-DUR-PST.[3SG.S]</ta>
            <ta e="T102" id="Seg_7690" s="T101">курить-INCH-DUR-PST.[3SG.S]</ta>
            <ta e="T103" id="Seg_7691" s="T102">долго</ta>
            <ta e="T104" id="Seg_7692" s="T103">царь.[NOM]</ta>
            <ta e="T105" id="Seg_7693" s="T104">как</ta>
            <ta e="T106" id="Seg_7694" s="T105">быть-INF</ta>
            <ta e="T107" id="Seg_7695" s="T106">грешник.[NOM]</ta>
            <ta e="T108" id="Seg_7696" s="T107">быть-PST.NAR.[3SG.S]</ta>
            <ta e="T109" id="Seg_7697" s="T108">год.[NOM]-3SG</ta>
            <ta e="T110" id="Seg_7698" s="T109">уйти-CO.[3SG.S]</ta>
            <ta e="T111" id="Seg_7699" s="T110">уйти-CO.[3SG.S]</ta>
            <ta e="T112" id="Seg_7700" s="T111">словно</ta>
            <ta e="T113" id="Seg_7701" s="T112">сон.[NOM]</ta>
            <ta e="T114" id="Seg_7702" s="T113">новый</ta>
            <ta e="T115" id="Seg_7703" s="T114">новый</ta>
            <ta e="T116" id="Seg_7704" s="T115">жена-ACC</ta>
            <ta e="T117" id="Seg_7705" s="T116">взять-CO-3SG.O</ta>
            <ta e="T118" id="Seg_7706" s="T117">царь.[NOM]</ta>
            <ta e="T119" id="Seg_7707" s="T118">как</ta>
            <ta e="T120" id="Seg_7708" s="T119">сказать-INF</ta>
            <ta e="T121" id="Seg_7709" s="T120">дочь-человек.[NOM]</ta>
            <ta e="T122" id="Seg_7710" s="T121">быть-PST.[3SG.S]</ta>
            <ta e="T123" id="Seg_7711" s="T122">сам.3SG-EP-ADJZ</ta>
            <ta e="T124" id="Seg_7712" s="T123">словно</ta>
            <ta e="T125" id="Seg_7713" s="T124">царь-EP-ADJZ</ta>
            <ta e="T126" id="Seg_7714" s="T125">жена.[NOM]</ta>
            <ta e="T127" id="Seg_7715" s="T126">высокий</ta>
            <ta e="T128" id="Seg_7716" s="T127">шея.[NOM]</ta>
            <ta e="T129" id="Seg_7717" s="T128">белый</ta>
            <ta e="T130" id="Seg_7718" s="T129">быть-PST.[3SG.S]</ta>
            <ta e="T131" id="Seg_7719" s="T130">ум-ADJZ</ta>
            <ta e="T132" id="Seg_7720" s="T131">всё.прочее-COM</ta>
            <ta e="T133" id="Seg_7721" s="T132">всё-GEN</ta>
            <ta e="T134" id="Seg_7722" s="T133">взять-PST-3SG.O</ta>
            <ta e="T135" id="Seg_7723" s="T134">потом</ta>
            <ta e="T136" id="Seg_7724" s="T135">и</ta>
            <ta e="T137" id="Seg_7725" s="T136">дальше</ta>
            <ta e="T138" id="Seg_7726" s="T137">хороший.VBLZ-PFV-TR-3SG.O</ta>
            <ta e="T139" id="Seg_7727" s="T138">наверно-только</ta>
            <ta e="T140" id="Seg_7728" s="T139">дальше</ta>
            <ta e="T141" id="Seg_7729" s="T140">другой</ta>
            <ta e="T142" id="Seg_7730" s="T141">стать.[3SG.S]</ta>
            <ta e="T143" id="Seg_7731" s="T142">чум-GEN.3SG</ta>
            <ta e="T144" id="Seg_7732" s="T143">из-ADV.EL</ta>
            <ta e="T145" id="Seg_7733" s="T144">принести-DUR-PST-3SG.O</ta>
            <ta e="T146" id="Seg_7734" s="T145">только</ta>
            <ta e="T147" id="Seg_7735" s="T146">зеркало-DIM-ACC</ta>
            <ta e="T148" id="Seg_7736" s="T147">один</ta>
            <ta e="T149" id="Seg_7737" s="T148">зеркало-DIM.[NOM]</ta>
            <ta e="T150" id="Seg_7738" s="T149">такой-ADJZ</ta>
            <ta e="T151" id="Seg_7739" s="T150">быть-PST.[3SG.S]</ta>
            <ta e="T152" id="Seg_7740" s="T151">сказать-DUR-TR-INF</ta>
            <ta e="T153" id="Seg_7741" s="T152">уметь-PST.NAR.[3SG.S]</ta>
            <ta e="T154" id="Seg_7742" s="T153">он(а)-COM</ta>
            <ta e="T155" id="Seg_7743" s="T154">один</ta>
            <ta e="T156" id="Seg_7744" s="T155">он(а).[NOM]</ta>
            <ta e="T157" id="Seg_7745" s="T156">рядом</ta>
            <ta e="T158" id="Seg_7746" s="T157">быть-HAB.[3SG.S]</ta>
            <ta e="T159" id="Seg_7747" s="T158">хороший</ta>
            <ta e="T160" id="Seg_7748" s="T159">внутренность-ADVZ</ta>
            <ta e="T161" id="Seg_7749" s="T160">радость-TR-DUR-HAB.[3SG.S]</ta>
            <ta e="T162" id="Seg_7750" s="T161">он(а)-COM</ta>
            <ta e="T163" id="Seg_7751" s="T162">нечто.смешное-ADVZ</ta>
            <ta e="T164" id="Seg_7752" s="T163">шутить-DUR-HAB.[3SG.S]</ta>
            <ta e="T165" id="Seg_7753" s="T164">хороший.VBLZ-PFV-PTCP.PST</ta>
            <ta e="T166" id="Seg_7754" s="T165">сказать-DUR-HAB.[3SG.S]</ta>
            <ta e="T167" id="Seg_7755" s="T166">день.[NOM]-1SG</ta>
            <ta e="T168" id="Seg_7756" s="T167">зеркало-DIM.[NOM]-1SG</ta>
            <ta e="T169" id="Seg_7757" s="T168">сказать-IMP.2SG.O</ta>
            <ta e="T170" id="Seg_7758" s="T169">вот</ta>
            <ta e="T171" id="Seg_7759" s="T170">день-ADVZ</ta>
            <ta e="T172" id="Seg_7760" s="T171">вверх</ta>
            <ta e="T173" id="Seg_7761" s="T172">положить-IMP.2SG.O</ta>
            <ta e="T174" id="Seg_7762" s="T173">я.NOM</ta>
            <ta e="T175" id="Seg_7763" s="T174">что.ли</ta>
            <ta e="T176" id="Seg_7764" s="T175">земля-GEN</ta>
            <ta e="T177" id="Seg_7765" s="T176">на-LOC</ta>
            <ta e="T178" id="Seg_7766" s="T177">всё-GEN</ta>
            <ta e="T179" id="Seg_7767" s="T178">хороший</ta>
            <ta e="T180" id="Seg_7768" s="T179">вид.[NOM]</ta>
            <ta e="T181" id="Seg_7769" s="T180">белый</ta>
            <ta e="T182" id="Seg_7770" s="T181">быть-CO-1SG.S</ta>
            <ta e="T183" id="Seg_7771" s="T182">зеркало-DIM.[NOM]</ta>
            <ta e="T184" id="Seg_7772" s="T183">сказать-HAB-3SG.O</ta>
            <ta e="T185" id="Seg_7773" s="T184">ты.NOM</ta>
            <ta e="T186" id="Seg_7774" s="T185">царь-EP-GEN</ta>
            <ta e="T187" id="Seg_7775" s="T186">жена.[NOM]</ta>
            <ta e="T188" id="Seg_7776" s="T187">NEG</ta>
            <ta e="T189" id="Seg_7777" s="T188">спорить-1SG.S</ta>
            <ta e="T190" id="Seg_7778" s="T189">ты.NOM</ta>
            <ta e="T191" id="Seg_7779" s="T190">%%</ta>
            <ta e="T192" id="Seg_7780" s="T191">один</ta>
            <ta e="T193" id="Seg_7781" s="T192">всё-GEN</ta>
            <ta e="T194" id="Seg_7782" s="T193">хороший</ta>
            <ta e="T195" id="Seg_7783" s="T194">вид.[NOM]</ta>
            <ta e="T196" id="Seg_7784" s="T195">и</ta>
            <ta e="T197" id="Seg_7785" s="T196">белый</ta>
            <ta e="T198" id="Seg_7786" s="T197">быть-CO-2SG.S</ta>
            <ta e="T199" id="Seg_7787" s="T198">царь-EP-GEN</ta>
            <ta e="T200" id="Seg_7788" s="T199">жена.[NOM]</ta>
            <ta e="T201" id="Seg_7789" s="T200">хохотать-DRV-CO.[3SG.S]</ta>
            <ta e="T202" id="Seg_7790" s="T201">плечо-ACC</ta>
            <ta e="T203" id="Seg_7791" s="T202">%%-MULO-CO-3SG.O</ta>
            <ta e="T204" id="Seg_7792" s="T203">глаз-OBL.3SG-INSTR</ta>
            <ta e="T205" id="Seg_7793" s="T204">мигать-PFV-CO-GEN</ta>
            <ta e="T206" id="Seg_7794" s="T205">палец-OBL.3SG-INSTR</ta>
            <ta e="T207" id="Seg_7795" s="T206">щёлкать.пальцами-PFV-CO-3SG.O</ta>
            <ta e="T208" id="Seg_7796" s="T207">рука-ACC</ta>
            <ta e="T209" id="Seg_7797" s="T208">%%-ILL</ta>
            <ta e="T210" id="Seg_7798" s="T209">вертеть-DUR.[3SG.S]</ta>
            <ta e="T211" id="Seg_7799" s="T210">зеркало-DIM-ILL</ta>
            <ta e="T212" id="Seg_7800" s="T211">взглянуть-DUR-CVB</ta>
            <ta e="T213" id="Seg_7801" s="T212">царь-EP-GEN</ta>
            <ta e="T214" id="Seg_7802" s="T213">дочь.[NOM]</ta>
            <ta e="T215" id="Seg_7803" s="T214">сирота-ADJZ</ta>
            <ta e="T216" id="Seg_7804" s="T215">девушка.[NOM]</ta>
            <ta e="T217" id="Seg_7805" s="T216">тихий-ADVZ</ta>
            <ta e="T218" id="Seg_7806" s="T217">середина-ADVZ</ta>
            <ta e="T219" id="Seg_7807" s="T218">сила-TRL-IPFV.[3SG.S]</ta>
            <ta e="T220" id="Seg_7808" s="T219">это-GEN</ta>
            <ta e="T221" id="Seg_7809" s="T220">между-ADV.LOC</ta>
            <ta e="T222" id="Seg_7810" s="T221">сила-TRL-IPFV.[3SG.S]</ta>
            <ta e="T223" id="Seg_7811" s="T222">поднять-US-EP-IPFV.[3SG.S]</ta>
            <ta e="T224" id="Seg_7812" s="T223">сила-TRL-INFER-CO.[3SG.S]</ta>
            <ta e="T225" id="Seg_7813" s="T224">белый</ta>
            <ta e="T226" id="Seg_7814" s="T225">лицо-ADVZ</ta>
            <ta e="T227" id="Seg_7815" s="T226">чёрный</ta>
            <ta e="T228" id="Seg_7816" s="T227">глаз-GEN</ta>
            <ta e="T229" id="Seg_7817" s="T228">дуга-ADVZ</ta>
            <ta e="T230" id="Seg_7818" s="T229">%%-CAR-ADVZ</ta>
            <ta e="T231" id="Seg_7819" s="T230">целый-EP-ADVZ</ta>
            <ta e="T232" id="Seg_7820" s="T231">внутренность-ADVZ</ta>
            <ta e="T233" id="Seg_7821" s="T232">богач-ADJZ</ta>
            <ta e="T234" id="Seg_7822" s="T233">человек.[NOM]-3SG</ta>
            <ta e="T235" id="Seg_7823" s="T234">искать-MULO-EP-PST.[3SG.S]</ta>
            <ta e="T236" id="Seg_7824" s="T235">король-GEN</ta>
            <ta e="T237" id="Seg_7825" s="T236">сын.[NOM]</ta>
            <ta e="T238" id="Seg_7826" s="T237">Елисей.[NOM]</ta>
            <ta e="T239" id="Seg_7827" s="T238">прийти-PST.[3SG.S]</ta>
            <ta e="T240" id="Seg_7828" s="T239">человек.[NOM]</ta>
            <ta e="T241" id="Seg_7829" s="T240">царь.[NOM]</ta>
            <ta e="T242" id="Seg_7830" s="T241">слово-ACC</ta>
            <ta e="T243" id="Seg_7831" s="T242">дать-PST-3SG.O</ta>
            <ta e="T244" id="Seg_7832" s="T243">дом-LOC</ta>
            <ta e="T245" id="Seg_7833" s="T244">взять-PTCP.NEC</ta>
            <ta e="T246" id="Seg_7834" s="T245">будто</ta>
            <ta e="T247" id="Seg_7835" s="T246">быть-CO.[3SG.S]</ta>
            <ta e="T248" id="Seg_7836" s="T247">семь</ta>
            <ta e="T249" id="Seg_7837" s="T248">купить-ABST-ADJZ</ta>
            <ta e="T250" id="Seg_7838" s="T249">город-PL.[NOM]</ta>
            <ta e="T251" id="Seg_7839" s="T250">и</ta>
            <ta e="T252" id="Seg_7840" s="T251">сто</ta>
            <ta e="T253" id="Seg_7841" s="T252">и</ta>
            <ta e="T254" id="Seg_7842" s="T253">четыре-десять-EP-ADJZ</ta>
            <ta e="T255" id="Seg_7843" s="T254">дом-EP-DIM-PL.[NOM]</ta>
            <ta e="T256" id="Seg_7844" s="T255">девушка-PL-EP-ALL</ta>
            <ta e="T257" id="Seg_7845" s="T256">собирать-MULS-INF</ta>
            <ta e="T258" id="Seg_7846" s="T257">царь-EP-GEN</ta>
            <ta e="T259" id="Seg_7847" s="T258">жена.[NOM]</ta>
            <ta e="T260" id="Seg_7848" s="T259">одеть(ся)-IPFV-HAB-PTCP.PRS</ta>
            <ta e="T261" id="Seg_7849" s="T260">зеркало-DIM-OBL.3SG-COM</ta>
            <ta e="T262" id="Seg_7850" s="T261">свой.3SG</ta>
            <ta e="T263" id="Seg_7851" s="T262">слово-1SG.S.[NOM]</ta>
            <ta e="T264" id="Seg_7852" s="T263">он(а)-EP-GEN</ta>
            <ta e="T265" id="Seg_7853" s="T264">бросать-HAB-PST-3SG.O</ta>
            <ta e="T266" id="Seg_7854" s="T265">я.NOM</ta>
            <ta e="T267" id="Seg_7855" s="T266">что.ли</ta>
            <ta e="T268" id="Seg_7856" s="T267">сказать-IMP.2SG.O</ta>
            <ta e="T269" id="Seg_7857" s="T268">я.ALL</ta>
            <ta e="T270" id="Seg_7858" s="T269">всё</ta>
            <ta e="T271" id="Seg_7859" s="T270">вид-INSTR</ta>
            <ta e="T272" id="Seg_7860" s="T271">и</ta>
            <ta e="T273" id="Seg_7861" s="T272">белый</ta>
            <ta e="T274" id="Seg_7862" s="T273">быть-CO-1SG.S</ta>
            <ta e="T275" id="Seg_7863" s="T274">что.[NOM]</ta>
            <ta e="T276" id="Seg_7864" s="T275">%%</ta>
            <ta e="T277" id="Seg_7865" s="T276">зеркало-DIM.[NOM]</ta>
            <ta e="T278" id="Seg_7866" s="T277">сказать-DUR-3SG.O</ta>
            <ta e="T279" id="Seg_7867" s="T278">вид-INSTR</ta>
            <ta e="T280" id="Seg_7868" s="T279">быть-CO-2SG.S</ta>
            <ta e="T281" id="Seg_7869" s="T280">NEG</ta>
            <ta e="T282" id="Seg_7870" s="T281">спорить-1SG.S</ta>
            <ta e="T283" id="Seg_7871" s="T282">царь-EP-GEN</ta>
            <ta e="T284" id="Seg_7872" s="T283">дочь.[NOM]</ta>
            <ta e="T285" id="Seg_7873" s="T284">%%</ta>
            <ta e="T286" id="Seg_7874" s="T285">всё-GEN</ta>
            <ta e="T287" id="Seg_7875" s="T286">вид-INSTR</ta>
            <ta e="T288" id="Seg_7876" s="T287">и</ta>
            <ta e="T289" id="Seg_7877" s="T288">белый</ta>
            <ta e="T290" id="Seg_7878" s="T289">быть-CO.[3SG.S]</ta>
            <ta e="T291" id="Seg_7879" s="T290">царь-EP-GEN</ta>
            <ta e="T292" id="Seg_7880" s="T291">жена.[NOM]</ta>
            <ta e="T293" id="Seg_7881" s="T292">прочь</ta>
            <ta e="T294" id="Seg_7882" s="T293">прыгнуть.[3SG.S]</ta>
            <ta e="T295" id="Seg_7883" s="T294">и</ta>
            <ta e="T296" id="Seg_7884" s="T295">махать-MOM-CO.[3SG.S]</ta>
            <ta e="T297" id="Seg_7885" s="T296">рука-DIM-OBL.3SG-INSTR</ta>
            <ta e="T298" id="Seg_7886" s="T297">зеркало-DIM-ACC</ta>
            <ta e="T299" id="Seg_7887" s="T298">шлёпнуть-TR-CO-3SG.O</ta>
            <ta e="T300" id="Seg_7888" s="T299">пятка-DIM-OBL.3SG-INSTR</ta>
            <ta e="T301" id="Seg_7889" s="T300">топтать-MOM-CO.[3SG.S]</ta>
            <ta e="T302" id="Seg_7890" s="T301">туда</ta>
            <ta e="T303" id="Seg_7891" s="T302">ах</ta>
            <ta e="T304" id="Seg_7892" s="T303">ты.NOM</ta>
            <ta e="T305" id="Seg_7893" s="T304">такой-ADJZ</ta>
            <ta e="T306" id="Seg_7894" s="T305">стекло-CO-2SG.S</ta>
            <ta e="T307" id="Seg_7895" s="T306">ты.NOM</ta>
            <ta e="T308" id="Seg_7896" s="T307">вот</ta>
            <ta e="T309" id="Seg_7897" s="T308">ложь-TR-2SG.S</ta>
            <ta e="T310" id="Seg_7898" s="T309">я.GEN</ta>
            <ta e="T311" id="Seg_7899" s="T310">о</ta>
            <ta e="T312" id="Seg_7900" s="T311">он(а).[NOM]</ta>
            <ta e="T313" id="Seg_7901" s="T312">как</ta>
            <ta e="T314" id="Seg_7902" s="T313">взглянуть-COND-2SG.S</ta>
            <ta e="T315" id="Seg_7903" s="T314">я.COM</ta>
            <ta e="T316" id="Seg_7904" s="T315">я.NOM</ta>
            <ta e="T317" id="Seg_7905" s="T316">он(а)-EP-GEN</ta>
            <ta e="T318" id="Seg_7906" s="T317">ложь-PL.[NOM]</ta>
            <ta e="T319" id="Seg_7907" s="T318">тихий.VBLZ-MULO-FUT-1SG.O</ta>
            <ta e="T320" id="Seg_7908" s="T319">увидеть-DRV-CO-2PL</ta>
            <ta e="T321" id="Seg_7909" s="T320">сила-TRL-CO.[3SG.S]</ta>
            <ta e="T322" id="Seg_7910" s="T321">что.ли</ta>
            <ta e="T323" id="Seg_7911" s="T322">как</ta>
            <ta e="T324" id="Seg_7912" s="T323">NEG</ta>
            <ta e="T325" id="Seg_7913" s="T324">белый</ta>
            <ta e="T326" id="Seg_7914" s="T325">быть-CO.[3SG.S]</ta>
            <ta e="T327" id="Seg_7915" s="T326">мать.[NOM]-3SG</ta>
            <ta e="T328" id="Seg_7916" s="T327">живот-ADJZ</ta>
            <ta e="T329" id="Seg_7917" s="T328">совокупность.[NOM]</ta>
            <ta e="T330" id="Seg_7918" s="T329">сидеть-PST.[3SG.S]</ta>
            <ta e="T331" id="Seg_7919" s="T330">только</ta>
            <ta e="T332" id="Seg_7920" s="T331">снег-ILL</ta>
            <ta e="T333" id="Seg_7921" s="T332">взглянуть-MOM-DUR-PST.[3SG.S]</ta>
            <ta e="T334" id="Seg_7922" s="T333">ну.ка</ta>
            <ta e="T335" id="Seg_7923" s="T334">сказать-IMP.2SG.O</ta>
            <ta e="T336" id="Seg_7924" s="T335">как</ta>
            <ta e="T337" id="Seg_7925" s="T336">я.GEN</ta>
            <ta e="T338" id="Seg_7926" s="T337">из-ADV.EL</ta>
            <ta e="T339" id="Seg_7927" s="T338">потом</ta>
            <ta e="T340" id="Seg_7928" s="T339">потом</ta>
            <ta e="T341" id="Seg_7929" s="T340">он(а).[NOM]</ta>
            <ta e="T342" id="Seg_7930" s="T341">вид-INSTR</ta>
            <ta e="T343" id="Seg_7931" s="T342">быть-FUT.[3SG.S]</ta>
            <ta e="T344" id="Seg_7932" s="T343">вверх</ta>
            <ta e="T345" id="Seg_7933" s="T344">сказать-IMP.2SG.O</ta>
            <ta e="T346" id="Seg_7934" s="T345">я.NOM</ta>
            <ta e="T347" id="Seg_7935" s="T346">%%</ta>
            <ta e="T348" id="Seg_7936" s="T347">%%-1SG.S</ta>
            <ta e="T349" id="Seg_7937" s="T348">рог-ADJZ</ta>
            <ta e="T350" id="Seg_7938" s="T349">начальник.[NOM]</ta>
            <ta e="T351" id="Seg_7939" s="T350">нечто.[NOM]-3SG</ta>
            <ta e="T352" id="Seg_7940" s="T351">повернуться-TR-IMP.2SG.O</ta>
            <ta e="T353" id="Seg_7941" s="T352">INDEF3</ta>
            <ta e="T354" id="Seg_7942" s="T353">земля.[NOM]</ta>
            <ta e="T355" id="Seg_7943" s="T354">всё</ta>
            <ta e="T356" id="Seg_7944" s="T355">я.COM</ta>
            <ta e="T357" id="Seg_7945" s="T356">NEG.EX.[3SG.S]</ta>
            <ta e="T358" id="Seg_7946" s="T357">%%-ADJZ</ta>
            <ta e="T359" id="Seg_7947" s="T358">нечто.[NOM]</ta>
            <ta e="T360" id="Seg_7948" s="T359">такой-ADVZ</ta>
            <ta e="T361" id="Seg_7949" s="T360">наверно</ta>
            <ta e="T362" id="Seg_7950" s="T361">зеркало-DIM.[NOM]</ta>
            <ta e="T363" id="Seg_7951" s="T362">сказать-CO-3SG.O</ta>
            <ta e="T364" id="Seg_7952" s="T363">царь-EP-GEN</ta>
            <ta e="T365" id="Seg_7953" s="T364">дочь.[NOM]</ta>
            <ta e="T366" id="Seg_7954" s="T365">%%</ta>
            <ta e="T367" id="Seg_7955" s="T366">%%</ta>
            <ta e="T368" id="Seg_7956" s="T367">вид-INSTR</ta>
            <ta e="T369" id="Seg_7957" s="T368">и</ta>
            <ta e="T370" id="Seg_7958" s="T369">белый</ta>
            <ta e="T371" id="Seg_7959" s="T370">быть-CO.[3SG.S]</ta>
            <ta e="T372" id="Seg_7960" s="T371">как</ta>
            <ta e="T373" id="Seg_7961" s="T372">сделать-TR-2SG.O</ta>
            <ta e="T374" id="Seg_7962" s="T373">душа-ACC-3SG</ta>
            <ta e="T375" id="Seg_7963" s="T374">съесть-CVB</ta>
            <ta e="T376" id="Seg_7964" s="T375">зеркало-DIM-ACC</ta>
            <ta e="T377" id="Seg_7965" s="T376">вверх</ta>
            <ta e="T378" id="Seg_7966" s="T377">бросать-3SG.O</ta>
            <ta e="T379" id="Seg_7967" s="T378">Чернавка-ACC-3SG</ta>
            <ta e="T380" id="Seg_7968" s="T379">позвать-CO-3SG.O</ta>
            <ta e="T381" id="Seg_7969" s="T380">такой-ADVZ</ta>
            <ta e="T382" id="Seg_7970" s="T381">он(а)-ILL</ta>
            <ta e="T383" id="Seg_7971" s="T382">идти-CAUS-CO-3SG.O</ta>
            <ta e="T384" id="Seg_7972" s="T383">сам.3SG.[NOM]</ta>
            <ta e="T385" id="Seg_7973" s="T384">сенный-ADJZ</ta>
            <ta e="T386" id="Seg_7974" s="T385">девушка.[NOM]</ta>
            <ta e="T387" id="Seg_7975" s="T386">тот</ta>
            <ta e="T388" id="Seg_7976" s="T387">царь-EP-GEN</ta>
            <ta e="T389" id="Seg_7977" s="T388">дочь-ACC</ta>
            <ta e="T390" id="Seg_7978" s="T389">лес-GEN</ta>
            <ta e="T391" id="Seg_7979" s="T390">внутри-ILL</ta>
            <ta e="T392" id="Seg_7980" s="T391">отправиться-DRV-TR-TR-2SG.O</ta>
            <ta e="T393" id="Seg_7981" s="T392">замотать-IPFV-PTCP.PST</ta>
            <ta e="T394" id="Seg_7982" s="T393">жить-CVB</ta>
            <ta e="T395" id="Seg_7983" s="T394">и</ta>
            <ta e="T396" id="Seg_7984" s="T395">оставить-IPFV-PTCP.PST</ta>
            <ta e="T397" id="Seg_7985" s="T396">туда-ADV.LOC</ta>
            <ta e="T398" id="Seg_7986" s="T397">сосна-GEN</ta>
            <ta e="T399" id="Seg_7987" s="T398">вниз-ADV.LOC</ta>
            <ta e="T400" id="Seg_7988" s="T399">длинный</ta>
            <ta e="T401" id="Seg_7989" s="T400">живое.существо-EP-PL.[NOM]</ta>
            <ta e="T402" id="Seg_7990" s="T401">съесть-SUP.2/3PL</ta>
            <ta e="T403" id="Seg_7991" s="T402">чёрт.[NOM]</ta>
            <ta e="T404" id="Seg_7992" s="T403">видать</ta>
            <ta e="T405" id="Seg_7993" s="T404">спорить-FUT.[3SG.S]</ta>
            <ta e="T406" id="Seg_7994" s="T405">сердиться-PTCP.PRS</ta>
            <ta e="T407" id="Seg_7995" s="T406">женщина-COM</ta>
            <ta e="T408" id="Seg_7996" s="T407">царь-EP-GEN</ta>
            <ta e="T409" id="Seg_7997" s="T408">дочь-COM</ta>
            <ta e="T410" id="Seg_7998" s="T409">Чернавка.[NOM]</ta>
            <ta e="T411" id="Seg_7999" s="T410">уйти-IPFV.[3SG.S]</ta>
            <ta e="T412" id="Seg_8000" s="T411">лес-ILL</ta>
            <ta e="T413" id="Seg_8001" s="T412">вот</ta>
            <ta e="T414" id="Seg_8002" s="T413">столько</ta>
            <ta e="T415" id="Seg_8003" s="T414">далеко</ta>
            <ta e="T416" id="Seg_8004" s="T415">тот</ta>
            <ta e="T417" id="Seg_8005" s="T416">отправиться-TR-PTCP.PRS</ta>
            <ta e="T418" id="Seg_8006" s="T417">царь-EP-GEN</ta>
            <ta e="T419" id="Seg_8007" s="T418">дочь.[NOM]</ta>
            <ta e="T420" id="Seg_8008" s="T419">потом</ta>
            <ta e="T421" id="Seg_8009" s="T420">уйти-TR.[3SG.S]</ta>
            <ta e="T422" id="Seg_8010" s="T421">умереть-TEMPN-OBL.3SG-COR</ta>
            <ta e="T423" id="Seg_8011" s="T422">испугаться-DRV-CO.[3SG.S]</ta>
            <ta e="T424" id="Seg_8012" s="T423">молиться-TR-CO.[3SG.S]</ta>
            <ta e="T425" id="Seg_8013" s="T424">жить-ACTN.[NOM]-1SG</ta>
            <ta e="T426" id="Seg_8014" s="T425">что-LOC</ta>
            <ta e="T427" id="Seg_8015" s="T426">сказать-IMP.2SG.O</ta>
            <ta e="T428" id="Seg_8016" s="T427">дело.[NOM]-1SG</ta>
            <ta e="T429" id="Seg_8017" s="T428">быть-CO.[3SG.S]</ta>
            <ta e="T430" id="Seg_8018" s="T429">дочь-человек.[NOM]</ta>
            <ta e="T431" id="Seg_8019" s="T430">NEG.IMP</ta>
            <ta e="T432" id="Seg_8020" s="T431">я.ACC</ta>
            <ta e="T433" id="Seg_8021" s="T432">сломать-US-FRQ-IMP.2SG.S</ta>
            <ta e="T434" id="Seg_8022" s="T433">как</ta>
            <ta e="T435" id="Seg_8023" s="T434">человек-EP-ACC</ta>
            <ta e="T436" id="Seg_8024" s="T435">увидеть-FUT-1SG.O</ta>
            <ta e="T437" id="Seg_8025" s="T436">ты.ACC</ta>
            <ta e="T438" id="Seg_8026" s="T437">что-TRL</ta>
            <ta e="T439" id="Seg_8027" s="T438">хотеть-2SG.S</ta>
            <ta e="T440" id="Seg_8028" s="T439">это-TRL</ta>
            <ta e="T441" id="Seg_8029" s="T440">сделать-FUT-1SG.S</ta>
            <ta e="T442" id="Seg_8030" s="T441">тот-ACC</ta>
            <ta e="T443" id="Seg_8031" s="T442">душа-INSTR</ta>
            <ta e="T444" id="Seg_8032" s="T443">он(а).[NOM]-3SG</ta>
            <ta e="T445" id="Seg_8033" s="T444">любить-CVB</ta>
            <ta e="T446" id="Seg_8034" s="T445">NEG</ta>
            <ta e="T447" id="Seg_8035" s="T446">замотать-PST-3SG.O</ta>
            <ta e="T448" id="Seg_8036" s="T447">NEG</ta>
            <ta e="T449" id="Seg_8037" s="T448">убить-PST-3SG.O</ta>
            <ta e="T450" id="Seg_8038" s="T449">пустить-PST-3SG.O</ta>
            <ta e="T451" id="Seg_8039" s="T450">такой-ADVZ</ta>
            <ta e="T452" id="Seg_8040" s="T451">сказать-CVB</ta>
            <ta e="T453" id="Seg_8041" s="T452">отправиться-IMP.2SG.S</ta>
            <ta e="T454" id="Seg_8042" s="T453">бог.[NOM]</ta>
            <ta e="T455" id="Seg_8043" s="T454">ты.ACC</ta>
            <ta e="T456" id="Seg_8044" s="T455">вот</ta>
            <ta e="T457" id="Seg_8045" s="T456">держать-3SG.O</ta>
            <ta e="T458" id="Seg_8046" s="T457">потом</ta>
            <ta e="T459" id="Seg_8047" s="T458">сам.3SG.[NOM]</ta>
            <ta e="T460" id="Seg_8048" s="T459">домой</ta>
            <ta e="T461" id="Seg_8049" s="T460">прийти-PST.[3SG.S]</ta>
            <ta e="T462" id="Seg_8050" s="T461">что.[NOM]</ta>
            <ta e="T463" id="Seg_8051" s="T462">царь-EP-GEN</ta>
            <ta e="T464" id="Seg_8052" s="T463">жена.[NOM]</ta>
            <ta e="T465" id="Seg_8053" s="T464">сказать-CO-3SG.O</ta>
            <ta e="T466" id="Seg_8054" s="T465">вид-PROPR-ADJZ</ta>
            <ta e="T467" id="Seg_8055" s="T466">дочь-человек.[NOM]</ta>
            <ta e="T468" id="Seg_8056" s="T467">где</ta>
            <ta e="T469" id="Seg_8057" s="T468">быть-CO.[3SG.S]</ta>
            <ta e="T470" id="Seg_8058" s="T469">друг-CAR-ADVZ</ta>
            <ta e="T471" id="Seg_8059" s="T470">лес-ADV.LOC</ta>
            <ta e="T472" id="Seg_8060" s="T471">стоять.[3SG.S]</ta>
            <ta e="T473" id="Seg_8061" s="T472">он(а).[NOM]-3SG</ta>
            <ta e="T474" id="Seg_8062" s="T473">сказать-CO-3SG.O</ta>
            <ta e="T475" id="Seg_8063" s="T474">он(а)-EP-GEN</ta>
            <ta e="T476" id="Seg_8064" s="T475">привязать-DUR.[3SG.S]</ta>
            <ta e="T477" id="Seg_8065" s="T476">короткий-ADVZ</ta>
            <ta e="T478" id="Seg_8066" s="T477">локоть.[NOM]</ta>
            <ta e="T479" id="Seg_8067" s="T478">живое.существо-GEN-3SG</ta>
            <ta e="T480" id="Seg_8068" s="T479">зверь-GEN</ta>
            <ta e="T481" id="Seg_8069" s="T480">коготь-ILL</ta>
            <ta e="T482" id="Seg_8070" s="T481">попасть-RFL-COND.[3SG.S]</ta>
            <ta e="T483" id="Seg_8071" s="T482">мало-ADVZ</ta>
            <ta e="T484" id="Seg_8072" s="T483">он(а).[NOM]</ta>
            <ta e="T485" id="Seg_8073" s="T484">вот</ta>
            <ta e="T486" id="Seg_8074" s="T485">почувствовать-HAB-TR-CO-3SG.O</ta>
            <ta e="T487" id="Seg_8075" s="T486">умереть-FRQ-ABST-GEN</ta>
            <ta e="T488" id="Seg_8076" s="T487">тоже</ta>
            <ta e="T489" id="Seg_8077" s="T488">INFER</ta>
            <ta e="T490" id="Seg_8078" s="T489">достаточное.количество.[NOM]</ta>
            <ta e="T491" id="Seg_8079" s="T490">быть-IPFV-INFER.[3SG.S]</ta>
            <ta e="T492" id="Seg_8080" s="T491">речь.[NOM]</ta>
            <ta e="T493" id="Seg_8081" s="T492">уйти-CO.[3SG.S]</ta>
            <ta e="T494" id="Seg_8082" s="T493">потом</ta>
            <ta e="T495" id="Seg_8083" s="T494">такой-ADJZ</ta>
            <ta e="T496" id="Seg_8084" s="T495">царь-EP-GEN</ta>
            <ta e="T497" id="Seg_8085" s="T496">дочь.[NOM]</ta>
            <ta e="T498" id="Seg_8086" s="T497">потеряться-IPFV-CO.[3SG.S]</ta>
            <ta e="T499" id="Seg_8087" s="T498">болеть-FRQ-CO.[3SG.S]</ta>
            <ta e="T500" id="Seg_8088" s="T499">царь.[NOM]</ta>
            <ta e="T501" id="Seg_8089" s="T500">дочь-OBL.3SG-TRL</ta>
            <ta e="T502" id="Seg_8090" s="T501">король.[NOM]</ta>
            <ta e="T503" id="Seg_8091" s="T502">сын.[NOM]</ta>
            <ta e="T504" id="Seg_8092" s="T503">Елисей.[NOM]</ta>
            <ta e="T505" id="Seg_8093" s="T504">бог-ILL</ta>
            <ta e="T506" id="Seg_8094" s="T505">молиться-TR-CVB</ta>
            <ta e="T507" id="Seg_8095" s="T506">после</ta>
            <ta e="T508" id="Seg_8096" s="T507">одеть(ся)-TR.[3SG.S]</ta>
            <ta e="T509" id="Seg_8097" s="T508">и</ta>
            <ta e="T510" id="Seg_8098" s="T509">искать-CVB</ta>
            <ta e="T511" id="Seg_8099" s="T510">уйти-CO.[3SG.S]</ta>
            <ta e="T512" id="Seg_8100" s="T511">вид-PROPR-GEN</ta>
            <ta e="T513" id="Seg_8101" s="T512">сердце-GEN-3SG</ta>
            <ta e="T514" id="Seg_8102" s="T513">голова-ACC</ta>
            <ta e="T515" id="Seg_8103" s="T514">позже</ta>
            <ta e="T516" id="Seg_8104" s="T515">взять-PTCP.NEC</ta>
            <ta e="T517" id="Seg_8105" s="T516">женщина.[NOM]-3SG</ta>
            <ta e="T518" id="Seg_8106" s="T517">живое.существо-ACC-3SG</ta>
            <ta e="T519" id="Seg_8107" s="T518">а</ta>
            <ta e="T520" id="Seg_8108" s="T519">%%.[NOM]</ta>
            <ta e="T521" id="Seg_8109" s="T520">живое.существо.[NOM]-3SG</ta>
            <ta e="T522" id="Seg_8110" s="T521">утро-GEN</ta>
            <ta e="T523" id="Seg_8111" s="T522">до</ta>
            <ta e="T524" id="Seg_8112" s="T523">лес-GEN</ta>
            <ta e="T525" id="Seg_8113" s="T524">внутри-ADV.LOC</ta>
            <ta e="T526" id="Seg_8114" s="T525">потеряться-HAB-DUR-CVB</ta>
            <ta e="T527" id="Seg_8115" s="T526">идти-IPFV-INFER.[3SG.S]</ta>
            <ta e="T528" id="Seg_8116" s="T527">между-ADV.LOC</ta>
            <ta e="T529" id="Seg_8117" s="T528">дом-EP-GEN</ta>
            <ta e="T530" id="Seg_8118" s="T529">на-ILL</ta>
            <ta e="T531" id="Seg_8119" s="T530">прийти-PFV-RFL.PFV-PST.NAR.[3SG.S]</ta>
            <ta e="T532" id="Seg_8120" s="T531">навстречу</ta>
            <ta e="T533" id="Seg_8121" s="T532">визг.собаки.[NOM]</ta>
            <ta e="T534" id="Seg_8122" s="T533">он(а)-EP-GEN</ta>
            <ta e="T535" id="Seg_8123" s="T534">лаять-PTCP.PRS</ta>
            <ta e="T536" id="Seg_8124" s="T535">визжать-MOM-DUR.[3SG.S]</ta>
            <ta e="T537" id="Seg_8125" s="T536">тихий-VBLZ-TR-IPFV.[3SG.S]</ta>
            <ta e="T538" id="Seg_8126" s="T537">ласкаться-DUR-CVB</ta>
            <ta e="T539" id="Seg_8127" s="T538">радость-TR-DUR-CVB</ta>
            <ta e="T540" id="Seg_8128" s="T539">ворота-PROL</ta>
            <ta e="T541" id="Seg_8129" s="T540">он(а).[NOM]</ta>
            <ta e="T542" id="Seg_8130" s="T541">вот</ta>
            <ta e="T543" id="Seg_8131" s="T542">войти.[3SG.S]</ta>
            <ta e="T544" id="Seg_8132" s="T543">улица-ADV.LOC</ta>
            <ta e="T545" id="Seg_8133" s="T544">тихий-ADVZ</ta>
            <ta e="T546" id="Seg_8134" s="T545">быть-CO.[3SG.S]</ta>
            <ta e="T547" id="Seg_8135" s="T546">визг.собаки.[NOM]</ta>
            <ta e="T548" id="Seg_8136" s="T547">радость-TR-DUR-CVB</ta>
            <ta e="T549" id="Seg_8137" s="T548">догонять-CO-3SG.O</ta>
            <ta e="T550" id="Seg_8138" s="T549">а</ta>
            <ta e="T551" id="Seg_8139" s="T550">царь-EP-GEN</ta>
            <ta e="T552" id="Seg_8140" s="T551">дочь.[NOM]</ta>
            <ta e="T553" id="Seg_8141" s="T552">идти-%%-CVB</ta>
            <ta e="T554" id="Seg_8142" s="T553">крыльцо-GEN</ta>
            <ta e="T555" id="Seg_8143" s="T554">на-ILL</ta>
            <ta e="T556" id="Seg_8144" s="T555">взбираться-IPFV.[3SG.S]</ta>
            <ta e="T557" id="Seg_8145" s="T556">круг-ADJZ</ta>
            <ta e="T558" id="Seg_8146" s="T557">рука-ILL-OBL.3SG</ta>
            <ta e="T559" id="Seg_8147" s="T558">взять-INFER-3SG.O</ta>
            <ta e="T560" id="Seg_8148" s="T559">дверь-PL.[NOM]</ta>
            <ta e="T561" id="Seg_8149" s="T560">середина-DIM.[NOM]</ta>
            <ta e="T562" id="Seg_8150" s="T561">INFER</ta>
            <ta e="T563" id="Seg_8151" s="T562">открыть-INFER-3SG.O</ta>
            <ta e="T564" id="Seg_8152" s="T563">дом-ILL</ta>
            <ta e="T565" id="Seg_8153" s="T564">середина-DIM.[NOM]</ta>
            <ta e="T566" id="Seg_8154" s="T565">вот</ta>
            <ta e="T567" id="Seg_8155" s="T566">войти.[3SG.S]</ta>
            <ta e="T568" id="Seg_8156" s="T567">увидеть-IPFV-DUR.[3SG.S]</ta>
            <ta e="T569" id="Seg_8157" s="T568">увидеть-IPFV-DUR.[3SG.S]</ta>
            <ta e="T570" id="Seg_8158" s="T569">увидеть-IPFV-IPFV.[3SG.S]</ta>
            <ta e="T571" id="Seg_8159" s="T570">ковёр-COM</ta>
            <ta e="T572" id="Seg_8160" s="T571">лавка-PL.[NOM]</ta>
            <ta e="T573" id="Seg_8161" s="T572">икона-GEN</ta>
            <ta e="T574" id="Seg_8162" s="T573">вниз-ADV.LOC</ta>
            <ta e="T575" id="Seg_8163" s="T574">дуб-EP-ADJZ</ta>
            <ta e="T576" id="Seg_8164" s="T575">столик.в.чуме.[NOM]</ta>
            <ta e="T577" id="Seg_8165" s="T576">узор.VBLZ-PTCP.PST</ta>
            <ta e="T578" id="Seg_8166" s="T577">лежать-ACTN-COM</ta>
            <ta e="T579" id="Seg_8167" s="T578">печь.[NOM]</ta>
            <ta e="T580" id="Seg_8168" s="T579">дочь-человек.[NOM]</ta>
            <ta e="T581" id="Seg_8169" s="T580">взглянуть-DUR-3SG.O</ta>
            <ta e="T582" id="Seg_8170" s="T581">здесь</ta>
            <ta e="T583" id="Seg_8171" s="T582">наверно</ta>
            <ta e="T584" id="Seg_8172" s="T583">хороший</ta>
            <ta e="T585" id="Seg_8173" s="T584">человек-EP-PL.[NOM]</ta>
            <ta e="T586" id="Seg_8174" s="T585">жить-3PL</ta>
            <ta e="T587" id="Seg_8175" s="T586">он(а)-EP-PL.[NOM]</ta>
            <ta e="T588" id="Seg_8176" s="T587">NEG</ta>
            <ta e="T589" id="Seg_8177" s="T588">ругаться-INCH-FUT-3PL</ta>
            <ta e="T590" id="Seg_8178" s="T589">затем</ta>
            <ta e="T591" id="Seg_8179" s="T590">между-ADVZ</ta>
            <ta e="T592" id="Seg_8180" s="T591">NEG</ta>
            <ta e="T593" id="Seg_8181" s="T592">кто.[NOM]</ta>
            <ta e="T594" id="Seg_8182" s="T593">NEG</ta>
            <ta e="T595" id="Seg_8183" s="T594">виднеться.[3SG.S]</ta>
            <ta e="T596" id="Seg_8184" s="T595">дом-EP-ACC</ta>
            <ta e="T597" id="Seg_8185" s="T596">туда-PROL</ta>
            <ta e="T598" id="Seg_8186" s="T597">повернуться-TR-INFER-3SG.O</ta>
            <ta e="T599" id="Seg_8187" s="T598">всё</ta>
            <ta e="T600" id="Seg_8188" s="T599">хороший-ADVZ</ta>
            <ta e="T601" id="Seg_8189" s="T600">%%-TR-PST-3SG.O</ta>
            <ta e="T602" id="Seg_8190" s="T601">икона-GEN</ta>
            <ta e="T603" id="Seg_8191" s="T602">свеча-ACC</ta>
            <ta e="T604" id="Seg_8192" s="T603">тлеть-CAUS-INFER-3SG.O</ta>
            <ta e="T605" id="Seg_8193" s="T604">печь-EP-ACC</ta>
            <ta e="T606" id="Seg_8194" s="T605">горячий-DIM</ta>
            <ta e="T607" id="Seg_8195" s="T606">зажечь-INFER-3SG.O</ta>
            <ta e="T608" id="Seg_8196" s="T607">полати-GEN</ta>
            <ta e="T609" id="Seg_8197" s="T608">на-ILL</ta>
            <ta e="T610" id="Seg_8198" s="T609">взбираться-CVB</ta>
            <ta e="T611" id="Seg_8199" s="T610">тихий-ADVZ</ta>
            <ta e="T612" id="Seg_8200" s="T611">вниз</ta>
            <ta e="T613" id="Seg_8201" s="T612">упасть-RFL.PFV-INFER.[3SG.S]</ta>
            <ta e="T614" id="Seg_8202" s="T613">день-GEN</ta>
            <ta e="T615" id="Seg_8203" s="T614">середина-ILL</ta>
            <ta e="T616" id="Seg_8204" s="T615">родня.[NOM]</ta>
            <ta e="T617" id="Seg_8205" s="T616">быть-PST.[3SG.S]</ta>
            <ta e="T618" id="Seg_8206" s="T617">шум.[NOM]</ta>
            <ta e="T619" id="Seg_8207" s="T618">слышаться-INCH-EP-IPFV.[3SG.S]</ta>
            <ta e="T620" id="Seg_8208" s="T619">пространство.снаружи-LOC</ta>
            <ta e="T621" id="Seg_8209" s="T620">семь</ta>
            <ta e="T622" id="Seg_8210" s="T621">богатырь-EP-PL.[NOM]</ta>
            <ta e="T623" id="Seg_8211" s="T622">вот</ta>
            <ta e="T624" id="Seg_8212" s="T623">войти-3PL</ta>
            <ta e="T625" id="Seg_8213" s="T624">семь-PL</ta>
            <ta e="T626" id="Seg_8214" s="T625">всё</ta>
            <ta e="T627" id="Seg_8215" s="T626">усы-ADVZ</ta>
            <ta e="T628" id="Seg_8216" s="T627">старший</ta>
            <ta e="T629" id="Seg_8217" s="T628">живое.существо.[NOM]-3PL</ta>
            <ta e="T630" id="Seg_8218" s="T629">такой-ADVZ</ta>
            <ta e="T631" id="Seg_8219" s="T630">сказать.[3SG.S]</ta>
            <ta e="T632" id="Seg_8220" s="T631">вот</ta>
            <ta e="T633" id="Seg_8221" s="T632">эй</ta>
            <ta e="T634" id="Seg_8222" s="T633">что.ли</ta>
            <ta e="T635" id="Seg_8223" s="T634">стать.[3SG.S]</ta>
            <ta e="T636" id="Seg_8224" s="T635">как</ta>
            <ta e="T637" id="Seg_8225" s="T636">хороший</ta>
            <ta e="T638" id="Seg_8226" s="T637">как</ta>
            <ta e="T639" id="Seg_8227" s="T638">всё</ta>
            <ta e="T640" id="Seg_8228" s="T639">вид-INSTR</ta>
            <ta e="T641" id="Seg_8229" s="T640">быть-CO.[3SG.S]</ta>
            <ta e="T642" id="Seg_8230" s="T641">кто.[NOM]</ta>
            <ta e="T643" id="Seg_8231" s="T642">INDEF3</ta>
            <ta e="T644" id="Seg_8232" s="T643">дом.[NOM]-1PL</ta>
            <ta e="T645" id="Seg_8233" s="T644">%%-PST.NAR-3SG.O</ta>
            <ta e="T646" id="Seg_8234" s="T645">будто</ta>
            <ta e="T647" id="Seg_8235" s="T646">мы.PL.ACC</ta>
            <ta e="T648" id="Seg_8236" s="T647">подождать-HAB-3PL</ta>
            <ta e="T649" id="Seg_8237" s="T648">подождать-PST.NAR-3PL</ta>
            <ta e="T650" id="Seg_8238" s="T649">кто-2SG.S</ta>
            <ta e="T651" id="Seg_8239" s="T650">выйти-IMP.2SG.S</ta>
            <ta e="T652" id="Seg_8240" s="T651">виднеться-TR-IMP.2SG.S</ta>
            <ta e="T653" id="Seg_8241" s="T652">мы.PL.ACC</ta>
            <ta e="T654" id="Seg_8242" s="T653">туда-ADV.EL</ta>
            <ta e="T655" id="Seg_8243" s="T654">%%-ADVZ</ta>
            <ta e="T656" id="Seg_8244" s="T655">встретить-IMP.2SG.S</ta>
            <ta e="T657" id="Seg_8245" s="T656">если</ta>
            <ta e="T658" id="Seg_8246" s="T657">ты.NOM</ta>
            <ta e="T659" id="Seg_8247" s="T658">старший</ta>
            <ta e="T660" id="Seg_8248" s="T659">человек-EP-2SG.S</ta>
            <ta e="T661" id="Seg_8249" s="T660">дедушка-OBL.1PL-TRL</ta>
            <ta e="T662" id="Seg_8250" s="T661">всегда</ta>
            <ta e="T663" id="Seg_8251" s="T662">быть-FUT-2SG.S</ta>
            <ta e="T664" id="Seg_8252" s="T663">если</ta>
            <ta e="T665" id="Seg_8253" s="T664">ты.NOM</ta>
            <ta e="T666" id="Seg_8254" s="T665">молодой</ta>
            <ta e="T667" id="Seg_8255" s="T666">некто-2SG.S</ta>
            <ta e="T668" id="Seg_8256" s="T667">мы.PL.ALL</ta>
            <ta e="T669" id="Seg_8257" s="T668">брат-OBL.1PL-TRL</ta>
            <ta e="T670" id="Seg_8258" s="T669">быть-FUT-3SG.O</ta>
            <ta e="T671" id="Seg_8259" s="T670">если</ta>
            <ta e="T672" id="Seg_8260" s="T671">ты.NOM</ta>
            <ta e="T673" id="Seg_8261" s="T672">старуха.[NOM]</ta>
            <ta e="T674" id="Seg_8262" s="T673">позвать-FUT-1PL</ta>
            <ta e="T675" id="Seg_8263" s="T674">мать-OBL.1PL-TRL</ta>
            <ta e="T676" id="Seg_8264" s="T675">если</ta>
            <ta e="T677" id="Seg_8265" s="T676">ты.NOM</ta>
            <ta e="T678" id="Seg_8266" s="T677">дочь.[NOM]</ta>
            <ta e="T679" id="Seg_8267" s="T678">человек-EP-2SG.S</ta>
            <ta e="T680" id="Seg_8268" s="T679">мы.PL.ALL</ta>
            <ta e="T681" id="Seg_8269" s="T680">сестра-OBL.1PL-TRL</ta>
            <ta e="T682" id="Seg_8270" s="T681">быть-FUT-3SG.O</ta>
            <ta e="T683" id="Seg_8271" s="T682">царь-EP-GEN</ta>
            <ta e="T684" id="Seg_8272" s="T683">дочь.[NOM]</ta>
            <ta e="T685" id="Seg_8273" s="T684">вниз</ta>
            <ta e="T686" id="Seg_8274" s="T685">спускаться-IPFV.[3SG.S]</ta>
            <ta e="T687" id="Seg_8275" s="T686">вниз.VBLZ-DUR-CO.[3SG.S]</ta>
            <ta e="T688" id="Seg_8276" s="T687">здороваться-IPFV.[3SG.S]</ta>
            <ta e="T689" id="Seg_8277" s="T688">он(а)-EP-PL-EP-COM</ta>
            <ta e="T690" id="Seg_8278" s="T689">пояс-GEN-3PL</ta>
            <ta e="T691" id="Seg_8279" s="T690">до</ta>
            <ta e="T692" id="Seg_8280" s="T691">наклониться-PFV-IPFV-CVB</ta>
            <ta e="T693" id="Seg_8281" s="T692">трава.[NOM]</ta>
            <ta e="T694" id="Seg_8282" s="T693">вырасти.[3SG.S]</ta>
            <ta e="T695" id="Seg_8283" s="T694">красный-VBLZ-FRQ-EP-PST.[3SG.S]</ta>
            <ta e="T696" id="Seg_8284" s="T695">такой-ADJZ</ta>
            <ta e="T697" id="Seg_8285" s="T696">сказать-3SG.O</ta>
            <ta e="T698" id="Seg_8286" s="T697">дом-ILL-3PL</ta>
            <ta e="T699" id="Seg_8287" s="T698">мол</ta>
            <ta e="T700" id="Seg_8288" s="T699">войти-PST-1SG.S</ta>
            <ta e="T701" id="Seg_8289" s="T700">я.NOM</ta>
            <ta e="T702" id="Seg_8290" s="T701">сюда</ta>
            <ta e="T703" id="Seg_8291" s="T702">INDEF3</ta>
            <ta e="T704" id="Seg_8292" s="T703">я.ACC</ta>
            <ta e="T705" id="Seg_8293" s="T704">позвать-ATTEN-2PL</ta>
            <ta e="T706" id="Seg_8294" s="T705">NEG</ta>
            <ta e="T707" id="Seg_8295" s="T706">потом</ta>
            <ta e="T708" id="Seg_8296" s="T707">слово-PROL-3SG</ta>
            <ta e="T709" id="Seg_8297" s="T708">увидеть-IPFV-3PL</ta>
            <ta e="T710" id="Seg_8298" s="T709">этот</ta>
            <ta e="T711" id="Seg_8299" s="T710">же</ta>
            <ta e="T712" id="Seg_8300" s="T711">царь-EP-GEN</ta>
            <ta e="T713" id="Seg_8301" s="T712">дочь.[NOM]</ta>
            <ta e="T714" id="Seg_8302" s="T713">быть-PST.[3SG.S]</ta>
            <ta e="T715" id="Seg_8303" s="T714">угол-DIM-ILL</ta>
            <ta e="T716" id="Seg_8304" s="T715">сесть-TR-PST-3PL</ta>
            <ta e="T717" id="Seg_8305" s="T716">хлеб-EP-DIM-INSTR</ta>
            <ta e="T718" id="Seg_8306" s="T717">хлеб-EP-DIM-INSTR</ta>
            <ta e="T719" id="Seg_8307" s="T718">дать-PFV-TR-PST-3PL</ta>
            <ta e="T720" id="Seg_8308" s="T719">рюмка-ADJZ</ta>
            <ta e="T721" id="Seg_8309" s="T720">совокупность-DIM.[NOM]</ta>
            <ta e="T722" id="Seg_8310" s="T721">лить-TR-%%-PST-3PL</ta>
            <ta e="T723" id="Seg_8311" s="T722">%%-GEN</ta>
            <ta e="T724" id="Seg_8312" s="T723">на-LOC</ta>
            <ta e="T725" id="Seg_8313" s="T724">принести-%%-PST-3SG.O</ta>
            <ta e="T726" id="Seg_8314" s="T725">он(а)-EP-ADJZ</ta>
            <ta e="T727" id="Seg_8315" s="T726">алкоголь-EP-ACC</ta>
            <ta e="T728" id="Seg_8316" s="T727">NEG</ta>
            <ta e="T729" id="Seg_8317" s="T728">взять-PST-3SG.O</ta>
            <ta e="T730" id="Seg_8318" s="T729">хлеб-EP-DIM-ACC</ta>
            <ta e="T731" id="Seg_8319" s="T730">%%</ta>
            <ta e="T732" id="Seg_8320" s="T731">ломать-TR-CVB</ta>
            <ta e="T733" id="Seg_8321" s="T732">маленький</ta>
            <ta e="T734" id="Seg_8322" s="T733">кусок-ACC-3SG</ta>
            <ta e="T735" id="Seg_8323" s="T734">рот-EP-ILL</ta>
            <ta e="T736" id="Seg_8324" s="T735">взять-PST-3SG.O</ta>
            <ta e="T737" id="Seg_8325" s="T736">след-GEN</ta>
            <ta e="T738" id="Seg_8326" s="T737">из-ADV.EL</ta>
            <ta e="T739" id="Seg_8327" s="T738">отдыхать-SUP.2/3SG</ta>
            <ta e="T740" id="Seg_8328" s="T739">место-TRL</ta>
            <ta e="T741" id="Seg_8329" s="T740">вот</ta>
            <ta e="T742" id="Seg_8330" s="T741">просить-FRQ-EP-CO.[3SG.S]</ta>
            <ta e="T743" id="Seg_8331" s="T742">отправиться-TR-PST-3PL</ta>
            <ta e="T744" id="Seg_8332" s="T743">он(а)-EP-PL.[NOM]</ta>
            <ta e="T745" id="Seg_8333" s="T744">девушка-ACC</ta>
            <ta e="T746" id="Seg_8334" s="T745">вверх</ta>
            <ta e="T747" id="Seg_8335" s="T746">день-VBLZ-DUR-PTCP.PRS</ta>
            <ta e="T748" id="Seg_8336" s="T747">нутро-ADV.LOC</ta>
            <ta e="T749" id="Seg_8337" s="T748">и</ta>
            <ta e="T750" id="Seg_8338" s="T749">сам.3SG-ADV.LOC</ta>
            <ta e="T751" id="Seg_8339" s="T750">INFER</ta>
            <ta e="T752" id="Seg_8340" s="T751">там</ta>
            <ta e="T753" id="Seg_8341" s="T752">оставить-INFER-3PL</ta>
            <ta e="T754" id="Seg_8342" s="T753">оставить-PST-3PL</ta>
            <ta e="T755" id="Seg_8343" s="T754">он(а)-EP-ACC</ta>
            <ta e="T756" id="Seg_8344" s="T755">сон-TR-PTCP.PRS</ta>
            <ta e="T757" id="Seg_8345" s="T756">друг-CAR-ADVZ</ta>
            <ta e="T759" id="Seg_8346" s="T758">красный</ta>
            <ta e="T760" id="Seg_8347" s="T759">солнце-GEN</ta>
            <ta e="T761" id="Seg_8348" s="T760">солнце.[NOM]</ta>
            <ta e="T762" id="Seg_8349" s="T761">конец-LOC-3SG</ta>
            <ta e="T763" id="Seg_8350" s="T762">молодой</ta>
            <ta e="T764" id="Seg_8351" s="T763">повернуть-TR-DUR-PST.[3SG.S]</ta>
            <ta e="T765" id="Seg_8352" s="T764">день-VBLZ-DUR-PTCP.PRS</ta>
            <ta e="T766" id="Seg_8353" s="T765">солнце.[NOM]-1PL</ta>
            <ta e="T767" id="Seg_8354" s="T766">мы.PL.GEN</ta>
            <ta e="T768" id="Seg_8355" s="T767">ты.NOM</ta>
            <ta e="T769" id="Seg_8356" s="T768">повернуть-US-CO-2SG.S</ta>
            <ta e="T770" id="Seg_8357" s="T769">ходить-FRQ-CO-2SG.S</ta>
            <ta e="T771" id="Seg_8358" s="T770">год-GEN-3SG</ta>
            <ta e="T772" id="Seg_8359" s="T771">в.течение</ta>
            <ta e="T773" id="Seg_8360" s="T772">небо-GEN</ta>
            <ta e="T774" id="Seg_8361" s="T773">%%</ta>
            <ta e="T775" id="Seg_8362" s="T774">стоять-CAUS-HAB-2SG.O</ta>
            <ta e="T776" id="Seg_8363" s="T775">зима-ACC</ta>
            <ta e="T777" id="Seg_8364" s="T776">тёплый</ta>
            <ta e="T778" id="Seg_8365" s="T777">весна-COM</ta>
            <ta e="T779" id="Seg_8366" s="T778">всё-PL.[NOM]</ta>
            <ta e="T780" id="Seg_8367" s="T779">мы.PL.ACC</ta>
            <ta e="T781" id="Seg_8368" s="T780">сам.2SG.[NOM]</ta>
            <ta e="T782" id="Seg_8369" s="T781">внизу</ta>
            <ta e="T783" id="Seg_8370" s="T782">увидеть-DRV-CO-2SG.S</ta>
            <ta e="T784" id="Seg_8371" s="T783">видать</ta>
            <ta e="T785" id="Seg_8372" s="T784">слово-ACC</ta>
            <ta e="T786" id="Seg_8373" s="T785">прочь</ta>
            <ta e="T787" id="Seg_8374" s="T786">бросать-FUT-2SG.O</ta>
            <ta e="T788" id="Seg_8375" s="T787">увидеть-DRV-PST-2SG.O</ta>
            <ta e="T789" id="Seg_8376" s="T788">что.ли</ta>
            <ta e="T790" id="Seg_8377" s="T789">где</ta>
            <ta e="T791" id="Seg_8378" s="T790">INDEF2</ta>
            <ta e="T792" id="Seg_8379" s="T791">земля-ADVZ</ta>
            <ta e="T793" id="Seg_8380" s="T792">царь-EP-GEN</ta>
            <ta e="T794" id="Seg_8381" s="T793">дочь-ACC</ta>
            <ta e="T795" id="Seg_8382" s="T794">молодой</ta>
            <ta e="T796" id="Seg_8383" s="T795">ты.NOM</ta>
            <ta e="T798" id="Seg_8384" s="T797">луна.[NOM]-3SG</ta>
            <ta e="T799" id="Seg_8385" s="T798">луна.[NOM]-3SG</ta>
            <ta e="T800" id="Seg_8386" s="T799">сам.1SG</ta>
            <ta e="T801" id="Seg_8387" s="T800">живое.существо.[NOM]-1SG</ta>
            <ta e="T802" id="Seg_8388" s="T801">позолоченный-ADJZ</ta>
            <ta e="T803" id="Seg_8389" s="T802">рог-DIM.[NOM]</ta>
            <ta e="T804" id="Seg_8390" s="T803">встать-HAB-2SG.S</ta>
            <ta e="T805" id="Seg_8391" s="T804">глубокий-ADJZ</ta>
            <ta e="T806" id="Seg_8392" s="T805">ночь-ADV.LOC</ta>
            <ta e="T807" id="Seg_8393" s="T806">ты.NOM</ta>
            <ta e="T808" id="Seg_8394" s="T807">круг-ADJZ</ta>
            <ta e="T809" id="Seg_8395" s="T808">лицо-ADJZ</ta>
            <ta e="T810" id="Seg_8396" s="T809">день-ADJZ</ta>
            <ta e="T811" id="Seg_8397" s="T810">глаз-ADJZ</ta>
            <ta e="T812" id="Seg_8398" s="T811">и</ta>
            <ta e="T813" id="Seg_8399" s="T812">я.GEN</ta>
            <ta e="T814" id="Seg_8400" s="T813">%%.[NOM]</ta>
            <ta e="T815" id="Seg_8401" s="T814">любить-CVB</ta>
            <ta e="T816" id="Seg_8402" s="T815">звезда-PL.[NOM]</ta>
            <ta e="T817" id="Seg_8403" s="T816">ты.ALL</ta>
            <ta e="T818" id="Seg_8404" s="T817">взглянуть-DUR-3PL</ta>
            <ta e="T820" id="Seg_8405" s="T819">туда-ADV.LOC</ta>
            <ta e="T821" id="Seg_8406" s="T820">середина.[NOM]</ta>
            <ta e="T822" id="Seg_8407" s="T821">течение-ADJZ</ta>
            <ta e="T823" id="Seg_8408" s="T822">река-LOC</ta>
            <ta e="T824" id="Seg_8409" s="T823">край.[3SG.S]</ta>
            <ta e="T825" id="Seg_8410" s="T824">быть-CO.[3SG.S]</ta>
            <ta e="T826" id="Seg_8411" s="T825">высокий</ta>
            <ta e="T827" id="Seg_8412" s="T826">%%.[NOM]</ta>
            <ta e="T828" id="Seg_8413" s="T827">там</ta>
            <ta e="T829" id="Seg_8414" s="T828">глубокий</ta>
            <ta e="T830" id="Seg_8415" s="T829">дыра.[NOM]</ta>
            <ta e="T831" id="Seg_8416" s="T830">ночь.[NOM]-3SG</ta>
            <ta e="T832" id="Seg_8417" s="T831">обрыв.[3SG.S]</ta>
            <ta e="T833" id="Seg_8418" s="T832">земля.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8419" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_8420" s="T1">n-n:obl.poss-n:case</ta>
            <ta e="T3" id="Seg_8421" s="T2">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_8422" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_8423" s="T4">adv</ta>
            <ta e="T6" id="Seg_8424" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_8425" s="T6">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T8" id="Seg_8426" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_8427" s="T8">pp-n:case</ta>
            <ta e="T10" id="Seg_8428" s="T9">n-n:case-n:poss</ta>
            <ta e="T11" id="Seg_8429" s="T10">pers-n:ins-n:case</ta>
            <ta e="T12" id="Seg_8430" s="T11">v-v:inf</ta>
            <ta e="T13" id="Seg_8431" s="T12">emphpro-n:case</ta>
            <ta e="T14" id="Seg_8432" s="T13">v-v:pn</ta>
            <ta e="T15" id="Seg_8433" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_8434" s="T15">pp-adv:case</ta>
            <ta e="T17" id="Seg_8435" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_8436" s="T17">pp</ta>
            <ta e="T19" id="Seg_8437" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_8438" s="T19">v-v&gt;v-v:pn</ta>
            <ta e="T21" id="Seg_8439" s="T20">adv</ta>
            <ta e="T22" id="Seg_8440" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_8441" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_8442" s="T23">pers-n:case</ta>
            <ta e="T25" id="Seg_8443" s="T24">n-n&gt;adj</ta>
            <ta e="T26" id="Seg_8444" s="T25">n-n:case-n:poss</ta>
            <ta e="T27" id="Seg_8445" s="T26">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T28" id="Seg_8446" s="T27">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_8447" s="T28">interrog-n&gt;adj</ta>
            <ta e="T30" id="Seg_8448" s="T29">ptcl-n&gt;adj</ta>
            <ta e="T31" id="Seg_8449" s="T30">n-n&gt;adj</ta>
            <ta e="T32" id="Seg_8450" s="T31">n-n:case-n:poss</ta>
            <ta e="T33" id="Seg_8451" s="T32">pp-n:case</ta>
            <ta e="T34" id="Seg_8452" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_8453" s="T34">pp</ta>
            <ta e="T36" id="Seg_8454" s="T35">adj</ta>
            <ta e="T37" id="Seg_8455" s="T36">n-n:case-n:poss</ta>
            <ta e="T38" id="Seg_8456" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_8457" s="T38">v-v:pn</ta>
            <ta e="T40" id="Seg_8458" s="T39">adv</ta>
            <ta e="T41" id="Seg_8459" s="T40">pers-n:case</ta>
            <ta e="T42" id="Seg_8460" s="T41">v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_8461" s="T42">n-n&gt;adj</ta>
            <ta e="T44" id="Seg_8462" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_8463" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_8464" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_8465" s="T46">v-v:ins-v:pn</ta>
            <ta e="T48" id="Seg_8466" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_8467" s="T48">pp-n:case</ta>
            <ta e="T50" id="Seg_8468" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_8469" s="T50">quant</ta>
            <ta e="T52" id="Seg_8470" s="T51">adj</ta>
            <ta e="T53" id="Seg_8471" s="T52">v-v:pn</ta>
            <ta e="T54" id="Seg_8472" s="T53">v-v:ins-v:pn</ta>
            <ta e="T55" id="Seg_8473" s="T54">v-v:ins-v:pn</ta>
            <ta e="T56" id="Seg_8474" s="T55">num</ta>
            <ta e="T57" id="Seg_8475" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8476" s="T57">num</ta>
            <ta e="T59" id="Seg_8477" s="T58">n-n:case-n:poss</ta>
            <ta e="T60" id="Seg_8478" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_8479" s="T60">n-n:case-n:poss</ta>
            <ta e="T62" id="Seg_8480" s="T61">adv</ta>
            <ta e="T63" id="Seg_8481" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_8482" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_8483" s="T64">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T66" id="Seg_8484" s="T65">adv</ta>
            <ta e="T67" id="Seg_8485" s="T66">n-n:ins-n:case</ta>
            <ta e="T68" id="Seg_8486" s="T67">n-n&gt;adv</ta>
            <ta e="T69" id="Seg_8487" s="T68">pers-v:ins-n:case</ta>
            <ta e="T70" id="Seg_8488" s="T69">v-v:ins-v:pn</ta>
            <ta e="T71" id="Seg_8489" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_8490" s="T71">n-n:case-n:poss</ta>
            <ta e="T73" id="Seg_8491" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_8492" s="T73">n-n:case-n:poss</ta>
            <ta e="T75" id="Seg_8493" s="T74">adj</ta>
            <ta e="T76" id="Seg_8494" s="T75">n-adv:case</ta>
            <ta e="T77" id="Seg_8495" s="T76">ptcl-n&gt;adj</ta>
            <ta e="T78" id="Seg_8496" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_8497" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_8498" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_8499" s="T80">adv</ta>
            <ta e="T82" id="Seg_8500" s="T81">v-v&gt;ptcp</ta>
            <ta e="T83" id="Seg_8501" s="T82">adv-adv:case</ta>
            <ta e="T84" id="Seg_8502" s="T83">adv</ta>
            <ta e="T85" id="Seg_8503" s="T84">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_8504" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_8505" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_8506" s="T87">n-n:case-n-n:case-n:poss</ta>
            <ta e="T89" id="Seg_8507" s="T88">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_8508" s="T89">pers-n:ins-n:case</ta>
            <ta e="T91" id="Seg_8509" s="T90">adj-n:ins-adj&gt;adj</ta>
            <ta e="T92" id="Seg_8510" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_8511" s="T92">n-n:case-n:poss</ta>
            <ta e="T94" id="Seg_8512" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_8513" s="T94">v-v&gt;v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_8514" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_8515" s="T96">conj</ta>
            <ta e="T98" id="Seg_8516" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_8517" s="T98">n-n&gt;adv</ta>
            <ta e="T100" id="Seg_8518" s="T99">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_8519" s="T100">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_8520" s="T101">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_8521" s="T102">adv</ta>
            <ta e="T104" id="Seg_8522" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_8523" s="T104">interrog</ta>
            <ta e="T106" id="Seg_8524" s="T105">v-v:inf</ta>
            <ta e="T107" id="Seg_8525" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_8526" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_8527" s="T108">n-n:case-n:poss</ta>
            <ta e="T110" id="Seg_8528" s="T109">v-v:ins-v:pn</ta>
            <ta e="T111" id="Seg_8529" s="T110">v-v:ins-v:pn</ta>
            <ta e="T112" id="Seg_8530" s="T111">conj</ta>
            <ta e="T113" id="Seg_8531" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_8532" s="T113">adj</ta>
            <ta e="T115" id="Seg_8533" s="T114">adj</ta>
            <ta e="T116" id="Seg_8534" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_8535" s="T116">v-v:ins-v:pn</ta>
            <ta e="T118" id="Seg_8536" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_8537" s="T118">interrog</ta>
            <ta e="T120" id="Seg_8538" s="T119">v-v:inf</ta>
            <ta e="T121" id="Seg_8539" s="T120">n-n-n:case</ta>
            <ta e="T122" id="Seg_8540" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_8541" s="T122">emphpro-n:ins-n&gt;adj</ta>
            <ta e="T124" id="Seg_8542" s="T123">conj</ta>
            <ta e="T125" id="Seg_8543" s="T124">n-n:ins-n&gt;adj</ta>
            <ta e="T126" id="Seg_8544" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_8545" s="T126">adj</ta>
            <ta e="T128" id="Seg_8546" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_8547" s="T128">adj</ta>
            <ta e="T130" id="Seg_8548" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_8549" s="T130">n-n&gt;adj</ta>
            <ta e="T132" id="Seg_8550" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_8551" s="T132">quant-n:case</ta>
            <ta e="T134" id="Seg_8552" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_8553" s="T134">adv</ta>
            <ta e="T136" id="Seg_8554" s="T135">conj</ta>
            <ta e="T137" id="Seg_8555" s="T136">adv</ta>
            <ta e="T138" id="Seg_8556" s="T137">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_8557" s="T138">ptcl-ptcl</ta>
            <ta e="T140" id="Seg_8558" s="T139">adv</ta>
            <ta e="T141" id="Seg_8559" s="T140">adj</ta>
            <ta e="T142" id="Seg_8560" s="T141">v-v:pn</ta>
            <ta e="T143" id="Seg_8561" s="T142">n-n:case-n:poss</ta>
            <ta e="T144" id="Seg_8562" s="T143">pp-adv:case</ta>
            <ta e="T145" id="Seg_8563" s="T144">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_8564" s="T145">adv</ta>
            <ta e="T147" id="Seg_8565" s="T146">n-n&gt;n-n:case</ta>
            <ta e="T148" id="Seg_8566" s="T147">num</ta>
            <ta e="T149" id="Seg_8567" s="T148">n-n&gt;n-n:case</ta>
            <ta e="T150" id="Seg_8568" s="T149">dem-adj&gt;adj</ta>
            <ta e="T151" id="Seg_8569" s="T150">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_8570" s="T151">v-v&gt;v-v&gt;v-v:inf</ta>
            <ta e="T153" id="Seg_8571" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_8572" s="T153">pers-n:case</ta>
            <ta e="T155" id="Seg_8573" s="T154">num</ta>
            <ta e="T156" id="Seg_8574" s="T155">pers-n:case</ta>
            <ta e="T157" id="Seg_8575" s="T156">pp</ta>
            <ta e="T158" id="Seg_8576" s="T157">v-v&gt;v-v:pn</ta>
            <ta e="T159" id="Seg_8577" s="T158">adj</ta>
            <ta e="T160" id="Seg_8578" s="T159">n-n&gt;adv</ta>
            <ta e="T161" id="Seg_8579" s="T160">n-n&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_8580" s="T161">pers-n:case</ta>
            <ta e="T163" id="Seg_8581" s="T162">n-n&gt;adv</ta>
            <ta e="T164" id="Seg_8582" s="T163">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T165" id="Seg_8583" s="T164">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T166" id="Seg_8584" s="T165">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T167" id="Seg_8585" s="T166">n-n:case-n:poss</ta>
            <ta e="T168" id="Seg_8586" s="T167">n-n&gt;n-n:case-n:poss</ta>
            <ta e="T169" id="Seg_8587" s="T168">v-v:mood.pn</ta>
            <ta e="T170" id="Seg_8588" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_8589" s="T170">n-n&gt;adv</ta>
            <ta e="T172" id="Seg_8590" s="T171">preverb</ta>
            <ta e="T173" id="Seg_8591" s="T172">v-v:mood.pn</ta>
            <ta e="T174" id="Seg_8592" s="T173">pers</ta>
            <ta e="T175" id="Seg_8593" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_8594" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_8595" s="T176">pp-n:case</ta>
            <ta e="T178" id="Seg_8596" s="T177">quant-n:case</ta>
            <ta e="T179" id="Seg_8597" s="T178">adj</ta>
            <ta e="T180" id="Seg_8598" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_8599" s="T180">adj</ta>
            <ta e="T182" id="Seg_8600" s="T181">v-v:ins-v:pn</ta>
            <ta e="T183" id="Seg_8601" s="T182">n-n&gt;n-n:case</ta>
            <ta e="T184" id="Seg_8602" s="T183">v-v&gt;v-v:pn</ta>
            <ta e="T185" id="Seg_8603" s="T184">pers</ta>
            <ta e="T186" id="Seg_8604" s="T185">n-n:ins-n:case</ta>
            <ta e="T187" id="Seg_8605" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_8606" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_8607" s="T188">v-v:pn</ta>
            <ta e="T190" id="Seg_8608" s="T189">pers</ta>
            <ta e="T191" id="Seg_8609" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_8610" s="T191">num</ta>
            <ta e="T193" id="Seg_8611" s="T192">quant-n:case</ta>
            <ta e="T194" id="Seg_8612" s="T193">adj</ta>
            <ta e="T195" id="Seg_8613" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_8614" s="T195">conj</ta>
            <ta e="T197" id="Seg_8615" s="T196">adj</ta>
            <ta e="T198" id="Seg_8616" s="T197">v-v:ins-v:pn</ta>
            <ta e="T199" id="Seg_8617" s="T198">n-n:ins-n:case</ta>
            <ta e="T200" id="Seg_8618" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_8619" s="T200">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T202" id="Seg_8620" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_8621" s="T202">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T204" id="Seg_8622" s="T203">n-n:obl.poss-n:case</ta>
            <ta e="T205" id="Seg_8623" s="T204">v-v&gt;v-v:ins-n:case</ta>
            <ta e="T206" id="Seg_8624" s="T205">n-n:obl.poss-n:case</ta>
            <ta e="T207" id="Seg_8625" s="T206">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T208" id="Seg_8626" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_8627" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_8628" s="T209">v-v&gt;v-v:pn</ta>
            <ta e="T211" id="Seg_8629" s="T210">n-n&gt;n-n:case</ta>
            <ta e="T212" id="Seg_8630" s="T211">v-v&gt;v-v&gt;adv</ta>
            <ta e="T213" id="Seg_8631" s="T212">n-n:ins-n:case</ta>
            <ta e="T214" id="Seg_8632" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_8633" s="T214">n-n&gt;adj</ta>
            <ta e="T216" id="Seg_8634" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_8635" s="T216">adj-adj&gt;adv</ta>
            <ta e="T218" id="Seg_8636" s="T217">n-n&gt;adv</ta>
            <ta e="T219" id="Seg_8637" s="T218">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T220" id="Seg_8638" s="T219">pro-n:case</ta>
            <ta e="T221" id="Seg_8639" s="T220">pp-n&gt;adv</ta>
            <ta e="T222" id="Seg_8640" s="T221">n-n&gt;v-v&gt;v-v:pn</ta>
            <ta e="T223" id="Seg_8641" s="T222">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T224" id="Seg_8642" s="T223">n-n&gt;v-v:mood-v:ins-v:pn</ta>
            <ta e="T225" id="Seg_8643" s="T224">adj</ta>
            <ta e="T226" id="Seg_8644" s="T225">n-n&gt;adv</ta>
            <ta e="T227" id="Seg_8645" s="T226">adj</ta>
            <ta e="T228" id="Seg_8646" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_8647" s="T228">n-n&gt;adv</ta>
            <ta e="T230" id="Seg_8648" s="T229">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T231" id="Seg_8649" s="T230">adj-n:ins-adj&gt;adv</ta>
            <ta e="T232" id="Seg_8650" s="T231">n-n&gt;adv</ta>
            <ta e="T233" id="Seg_8651" s="T232">n-n&gt;adj</ta>
            <ta e="T234" id="Seg_8652" s="T233">n-n:case-n:poss</ta>
            <ta e="T235" id="Seg_8653" s="T234">v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_8654" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_8655" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_8656" s="T237">nprop-n:case</ta>
            <ta e="T239" id="Seg_8657" s="T238">v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_8658" s="T239">n-n:case</ta>
            <ta e="T241" id="Seg_8659" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_8660" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_8661" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_8662" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_8663" s="T244">v-v&gt;ptcp</ta>
            <ta e="T246" id="Seg_8664" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_8665" s="T246">v-v:ins-v:pn</ta>
            <ta e="T248" id="Seg_8666" s="T247">num</ta>
            <ta e="T249" id="Seg_8667" s="T248">v-v&gt;n-n&gt;adj</ta>
            <ta e="T250" id="Seg_8668" s="T249">n-n:num-n:case</ta>
            <ta e="T251" id="Seg_8669" s="T250">conj</ta>
            <ta e="T252" id="Seg_8670" s="T251">num</ta>
            <ta e="T253" id="Seg_8671" s="T252">conj</ta>
            <ta e="T254" id="Seg_8672" s="T253">num-num&gt;num-n:ins-num&gt;adj</ta>
            <ta e="T255" id="Seg_8673" s="T254">n-n:ins-n&gt;n-n:num-n:case</ta>
            <ta e="T256" id="Seg_8674" s="T255">n-n:num-n:ins-n:case</ta>
            <ta e="T257" id="Seg_8675" s="T256">v-v&gt;v-v:inf</ta>
            <ta e="T258" id="Seg_8676" s="T257">n-n:ins-n:case</ta>
            <ta e="T259" id="Seg_8677" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_8678" s="T259">v-v&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T261" id="Seg_8679" s="T260">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T262" id="Seg_8680" s="T261">emphpro</ta>
            <ta e="T263" id="Seg_8681" s="T262">n-v:pn-n:case</ta>
            <ta e="T264" id="Seg_8682" s="T263">pers-n:ins-n:case</ta>
            <ta e="T265" id="Seg_8683" s="T264">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_8684" s="T265">pers</ta>
            <ta e="T267" id="Seg_8685" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_8686" s="T267">v-v:mood.pn</ta>
            <ta e="T269" id="Seg_8687" s="T268">pers</ta>
            <ta e="T270" id="Seg_8688" s="T269">quant</ta>
            <ta e="T271" id="Seg_8689" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_8690" s="T271">conj</ta>
            <ta e="T273" id="Seg_8691" s="T272">adj</ta>
            <ta e="T274" id="Seg_8692" s="T273">v-v:ins-v:pn</ta>
            <ta e="T275" id="Seg_8693" s="T274">interrog-n:case</ta>
            <ta e="T276" id="Seg_8694" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_8695" s="T276">n-n&gt;n-n:case</ta>
            <ta e="T278" id="Seg_8696" s="T277">v-v&gt;v-v:pn</ta>
            <ta e="T279" id="Seg_8697" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_8698" s="T279">v-v:ins-v:pn</ta>
            <ta e="T281" id="Seg_8699" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_8700" s="T281">v-v:pn</ta>
            <ta e="T283" id="Seg_8701" s="T282">n-n:ins-n:case</ta>
            <ta e="T284" id="Seg_8702" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_8703" s="T284">adj</ta>
            <ta e="T286" id="Seg_8704" s="T285">quant-n:case</ta>
            <ta e="T287" id="Seg_8705" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_8706" s="T287">conj</ta>
            <ta e="T289" id="Seg_8707" s="T288">adj</ta>
            <ta e="T290" id="Seg_8708" s="T289">v-v:ins-v:pn</ta>
            <ta e="T291" id="Seg_8709" s="T290">n-n:ins-n:case</ta>
            <ta e="T292" id="Seg_8710" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_8711" s="T292">adv</ta>
            <ta e="T294" id="Seg_8712" s="T293">v-v:pn</ta>
            <ta e="T295" id="Seg_8713" s="T294">conj</ta>
            <ta e="T296" id="Seg_8714" s="T295">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T297" id="Seg_8715" s="T296">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T298" id="Seg_8716" s="T297">n-n&gt;n-n:case</ta>
            <ta e="T299" id="Seg_8717" s="T298">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T300" id="Seg_8718" s="T299">n-n&gt;n-n:obl.poss-n:case</ta>
            <ta e="T301" id="Seg_8719" s="T300">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T302" id="Seg_8720" s="T301">adv</ta>
            <ta e="T303" id="Seg_8721" s="T302">interj</ta>
            <ta e="T304" id="Seg_8722" s="T303">pers</ta>
            <ta e="T305" id="Seg_8723" s="T304">dem-adj&gt;adj</ta>
            <ta e="T306" id="Seg_8724" s="T305">n-v:ins-v:pn</ta>
            <ta e="T307" id="Seg_8725" s="T306">pers</ta>
            <ta e="T308" id="Seg_8726" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8727" s="T308">n-n&gt;v-v:pn</ta>
            <ta e="T310" id="Seg_8728" s="T309">pers</ta>
            <ta e="T311" id="Seg_8729" s="T310">pp</ta>
            <ta e="T312" id="Seg_8730" s="T311">pers-n:case</ta>
            <ta e="T313" id="Seg_8731" s="T312">interrog</ta>
            <ta e="T314" id="Seg_8732" s="T313">v-v:mood-v:pn</ta>
            <ta e="T315" id="Seg_8733" s="T314">pers</ta>
            <ta e="T316" id="Seg_8734" s="T315">pers</ta>
            <ta e="T317" id="Seg_8735" s="T316">pers-n:ins-n:case</ta>
            <ta e="T318" id="Seg_8736" s="T317">n-n:num-n:case</ta>
            <ta e="T319" id="Seg_8737" s="T318">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_8738" s="T319">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T321" id="Seg_8739" s="T320">n-n&gt;v-v:ins-v:pn</ta>
            <ta e="T322" id="Seg_8740" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_8741" s="T322">interrog</ta>
            <ta e="T324" id="Seg_8742" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_8743" s="T324">adj</ta>
            <ta e="T326" id="Seg_8744" s="T325">v-v:ins-v:pn</ta>
            <ta e="T327" id="Seg_8745" s="T326">n-n:case-n:poss</ta>
            <ta e="T328" id="Seg_8746" s="T327">n-n&gt;adj</ta>
            <ta e="T329" id="Seg_8747" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_8748" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_8749" s="T330">adv</ta>
            <ta e="T332" id="Seg_8750" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_8751" s="T332">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_8752" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_8753" s="T334">v-v:mood.pn</ta>
            <ta e="T336" id="Seg_8754" s="T335">interrog</ta>
            <ta e="T337" id="Seg_8755" s="T336">pers</ta>
            <ta e="T338" id="Seg_8756" s="T337">pp-n&gt;adv</ta>
            <ta e="T339" id="Seg_8757" s="T338">adv</ta>
            <ta e="T340" id="Seg_8758" s="T339">adv</ta>
            <ta e="T341" id="Seg_8759" s="T340">pers-n:case</ta>
            <ta e="T342" id="Seg_8760" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_8761" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_8762" s="T343">preverb</ta>
            <ta e="T345" id="Seg_8763" s="T344">v-v:mood.pn</ta>
            <ta e="T346" id="Seg_8764" s="T345">pers</ta>
            <ta e="T347" id="Seg_8765" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_8766" s="T347">v-v:pn</ta>
            <ta e="T349" id="Seg_8767" s="T348">n-n&gt;adj</ta>
            <ta e="T350" id="Seg_8768" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_8769" s="T350">n-n:case-n:poss</ta>
            <ta e="T352" id="Seg_8770" s="T351">v-v&gt;v-v:mood.pn</ta>
            <ta e="T353" id="Seg_8771" s="T352">clit</ta>
            <ta e="T354" id="Seg_8772" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_8773" s="T354">quant</ta>
            <ta e="T356" id="Seg_8774" s="T355">pers</ta>
            <ta e="T357" id="Seg_8775" s="T356">v-v:pn</ta>
            <ta e="T358" id="Seg_8776" s="T357">adj-n&gt;adj</ta>
            <ta e="T359" id="Seg_8777" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_8778" s="T359">dem-adj&gt;adv</ta>
            <ta e="T361" id="Seg_8779" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_8780" s="T361">n-n&gt;n-n:case</ta>
            <ta e="T363" id="Seg_8781" s="T362">v-v:ins-v:pn</ta>
            <ta e="T364" id="Seg_8782" s="T363">n-n:ins-n:case</ta>
            <ta e="T365" id="Seg_8783" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_8784" s="T365">adj</ta>
            <ta e="T367" id="Seg_8785" s="T366">adj</ta>
            <ta e="T368" id="Seg_8786" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_8787" s="T368">conj</ta>
            <ta e="T370" id="Seg_8788" s="T369">adj</ta>
            <ta e="T371" id="Seg_8789" s="T370">v-v:ins-v:pn</ta>
            <ta e="T372" id="Seg_8790" s="T371">interrog</ta>
            <ta e="T373" id="Seg_8791" s="T372">v-v&gt;v-v:pn</ta>
            <ta e="T374" id="Seg_8792" s="T373">n-n:case-n:poss</ta>
            <ta e="T375" id="Seg_8793" s="T374">v-v&gt;adv</ta>
            <ta e="T376" id="Seg_8794" s="T375">n-n&gt;n-n:case</ta>
            <ta e="T377" id="Seg_8795" s="T376">adv</ta>
            <ta e="T378" id="Seg_8796" s="T377">v-v:pn</ta>
            <ta e="T379" id="Seg_8797" s="T378">nprop-n:case-n:poss</ta>
            <ta e="T380" id="Seg_8798" s="T379">v-v:ins-v:pn</ta>
            <ta e="T381" id="Seg_8799" s="T380">dem-adj&gt;adv</ta>
            <ta e="T382" id="Seg_8800" s="T381">pers-n:case</ta>
            <ta e="T383" id="Seg_8801" s="T382">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T384" id="Seg_8802" s="T383">emphpro-n:case</ta>
            <ta e="T385" id="Seg_8803" s="T384">adj-n&gt;adj</ta>
            <ta e="T386" id="Seg_8804" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_8805" s="T386">dem</ta>
            <ta e="T388" id="Seg_8806" s="T387">n-n:ins-n:case</ta>
            <ta e="T389" id="Seg_8807" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_8808" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_8809" s="T390">pp-n:case</ta>
            <ta e="T392" id="Seg_8810" s="T391">v-v&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T393" id="Seg_8811" s="T392">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T394" id="Seg_8812" s="T393">v-v&gt;adv</ta>
            <ta e="T395" id="Seg_8813" s="T394">conj</ta>
            <ta e="T396" id="Seg_8814" s="T395">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T397" id="Seg_8815" s="T396">adv-adv&gt;adv</ta>
            <ta e="T398" id="Seg_8816" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_8817" s="T398">adv-adv&gt;adv</ta>
            <ta e="T400" id="Seg_8818" s="T399">adj</ta>
            <ta e="T401" id="Seg_8819" s="T400">n-n:ins-n:num-n:case</ta>
            <ta e="T402" id="Seg_8820" s="T401">v-v:inf.poss</ta>
            <ta e="T403" id="Seg_8821" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_8822" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_8823" s="T404">v-v:tense-v:pn</ta>
            <ta e="T406" id="Seg_8824" s="T405">v-v&gt;ptcp</ta>
            <ta e="T407" id="Seg_8825" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_8826" s="T407">n-n:ins-n:case</ta>
            <ta e="T409" id="Seg_8827" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_8828" s="T409">nprop-n:case</ta>
            <ta e="T411" id="Seg_8829" s="T410">v-v&gt;v-v:pn</ta>
            <ta e="T412" id="Seg_8830" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_8831" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_8832" s="T413">quant</ta>
            <ta e="T415" id="Seg_8833" s="T414">adv</ta>
            <ta e="T416" id="Seg_8834" s="T415">dem</ta>
            <ta e="T417" id="Seg_8835" s="T416">v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T418" id="Seg_8836" s="T417">n-n:ins-n:case</ta>
            <ta e="T419" id="Seg_8837" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_8838" s="T419">adv</ta>
            <ta e="T421" id="Seg_8839" s="T420">v-v&gt;v-v:pn</ta>
            <ta e="T422" id="Seg_8840" s="T421">v-v&gt;n-n:obl.poss-n:case</ta>
            <ta e="T423" id="Seg_8841" s="T422">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T424" id="Seg_8842" s="T423">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T425" id="Seg_8843" s="T424">v-v&gt;n-n:case-n:poss</ta>
            <ta e="T426" id="Seg_8844" s="T425">interrog-n:case</ta>
            <ta e="T427" id="Seg_8845" s="T426">v-v:mood.pn</ta>
            <ta e="T428" id="Seg_8846" s="T427">n-n:case-n:poss</ta>
            <ta e="T429" id="Seg_8847" s="T428">v-v:ins-v:pn</ta>
            <ta e="T430" id="Seg_8848" s="T429">n-n-n:case</ta>
            <ta e="T431" id="Seg_8849" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_8850" s="T431">pers</ta>
            <ta e="T433" id="Seg_8851" s="T432">v-v&gt;v-v&gt;v-v:mood.pn</ta>
            <ta e="T434" id="Seg_8852" s="T433">interrog</ta>
            <ta e="T435" id="Seg_8853" s="T434">n-n:ins-n:case</ta>
            <ta e="T436" id="Seg_8854" s="T435">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_8855" s="T436">pers</ta>
            <ta e="T438" id="Seg_8856" s="T437">interrog-n:case</ta>
            <ta e="T439" id="Seg_8857" s="T438">v-v:pn</ta>
            <ta e="T440" id="Seg_8858" s="T439">pro-n:case</ta>
            <ta e="T441" id="Seg_8859" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_8860" s="T441">pro-n:case</ta>
            <ta e="T443" id="Seg_8861" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8862" s="T443">pers-n:case-n:poss</ta>
            <ta e="T445" id="Seg_8863" s="T444">v-v&gt;adv</ta>
            <ta e="T446" id="Seg_8864" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8865" s="T446">v-v:tense-v:pn</ta>
            <ta e="T448" id="Seg_8866" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_8867" s="T448">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_8868" s="T449">v-v:tense-v:pn</ta>
            <ta e="T451" id="Seg_8869" s="T450">dem-adj&gt;adv</ta>
            <ta e="T452" id="Seg_8870" s="T451">v-v&gt;adv</ta>
            <ta e="T453" id="Seg_8871" s="T452">v-v:mood.pn</ta>
            <ta e="T454" id="Seg_8872" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_8873" s="T454">pers</ta>
            <ta e="T456" id="Seg_8874" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_8875" s="T456">v-v:pn</ta>
            <ta e="T458" id="Seg_8876" s="T457">adv</ta>
            <ta e="T459" id="Seg_8877" s="T458">emphpro-n:case</ta>
            <ta e="T460" id="Seg_8878" s="T459">adv</ta>
            <ta e="T461" id="Seg_8879" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_8880" s="T461">interrog-n:case</ta>
            <ta e="T463" id="Seg_8881" s="T462">n-n:ins-n:case</ta>
            <ta e="T464" id="Seg_8882" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_8883" s="T464">v-v:ins-v:pn</ta>
            <ta e="T466" id="Seg_8884" s="T465">n-n&gt;n-n&gt;adj</ta>
            <ta e="T467" id="Seg_8885" s="T466">n-n-n:case</ta>
            <ta e="T468" id="Seg_8886" s="T467">interrog</ta>
            <ta e="T469" id="Seg_8887" s="T468">v-v:ins-v:pn</ta>
            <ta e="T470" id="Seg_8888" s="T469">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T471" id="Seg_8889" s="T470">n-n&gt;adv</ta>
            <ta e="T472" id="Seg_8890" s="T471">v-v:pn</ta>
            <ta e="T473" id="Seg_8891" s="T472">pers-n:case-n:poss</ta>
            <ta e="T474" id="Seg_8892" s="T473">v-v:ins-v:pn</ta>
            <ta e="T475" id="Seg_8893" s="T474">pers-n:ins-n:case</ta>
            <ta e="T476" id="Seg_8894" s="T475">v-v&gt;v-v:pn</ta>
            <ta e="T477" id="Seg_8895" s="T476">adj-adj&gt;adv</ta>
            <ta e="T478" id="Seg_8896" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_8897" s="T478">n-n:case-n:poss</ta>
            <ta e="T480" id="Seg_8898" s="T479">n-n:case</ta>
            <ta e="T481" id="Seg_8899" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_8900" s="T481">v-v&gt;v-v:mood-v:pn</ta>
            <ta e="T483" id="Seg_8901" s="T482">quant-adj&gt;adv</ta>
            <ta e="T484" id="Seg_8902" s="T483">pers-n:case</ta>
            <ta e="T485" id="Seg_8903" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_8904" s="T485">v-v&gt;v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T487" id="Seg_8905" s="T486">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T488" id="Seg_8906" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_8907" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_8908" s="T489">n-n:case</ta>
            <ta e="T491" id="Seg_8909" s="T490">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T492" id="Seg_8910" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_8911" s="T492">v-v:ins-v:pn</ta>
            <ta e="T494" id="Seg_8912" s="T493">adv</ta>
            <ta e="T495" id="Seg_8913" s="T494">dem-adj&gt;adj</ta>
            <ta e="T496" id="Seg_8914" s="T495">n-n:ins-n:case</ta>
            <ta e="T497" id="Seg_8915" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_8916" s="T497">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T499" id="Seg_8917" s="T498">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T500" id="Seg_8918" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_8919" s="T500">n-n:obl.poss-n:case</ta>
            <ta e="T502" id="Seg_8920" s="T501">n-n:case</ta>
            <ta e="T503" id="Seg_8921" s="T502">n-n:case</ta>
            <ta e="T504" id="Seg_8922" s="T503">nprop-n:case</ta>
            <ta e="T505" id="Seg_8923" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_8924" s="T505">v-v&gt;v-v&gt;adv</ta>
            <ta e="T507" id="Seg_8925" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_8926" s="T507">v-v&gt;v-v:pn</ta>
            <ta e="T509" id="Seg_8927" s="T508">conj</ta>
            <ta e="T510" id="Seg_8928" s="T509">v-v&gt;adv</ta>
            <ta e="T511" id="Seg_8929" s="T510">v-v:ins-v:pn</ta>
            <ta e="T512" id="Seg_8930" s="T511">n-n&gt;n-n:case</ta>
            <ta e="T513" id="Seg_8931" s="T512">n-n:case-n:poss</ta>
            <ta e="T514" id="Seg_8932" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_8933" s="T514">adv</ta>
            <ta e="T516" id="Seg_8934" s="T515">v-v&gt;ptcp</ta>
            <ta e="T517" id="Seg_8935" s="T516">n-n:case-n:poss</ta>
            <ta e="T518" id="Seg_8936" s="T517">n-n:case-n:poss</ta>
            <ta e="T519" id="Seg_8937" s="T518">conj</ta>
            <ta e="T520" id="Seg_8938" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_8939" s="T520">n-n:case-n:poss</ta>
            <ta e="T522" id="Seg_8940" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_8941" s="T522">pp</ta>
            <ta e="T524" id="Seg_8942" s="T523">n-n:case</ta>
            <ta e="T525" id="Seg_8943" s="T524">pp-n&gt;adv</ta>
            <ta e="T526" id="Seg_8944" s="T525">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T527" id="Seg_8945" s="T526">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T528" id="Seg_8946" s="T527">pp-n&gt;adv</ta>
            <ta e="T529" id="Seg_8947" s="T528">n-v:ins-n:case</ta>
            <ta e="T530" id="Seg_8948" s="T529">pp-n:case</ta>
            <ta e="T531" id="Seg_8949" s="T530">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_8950" s="T531">adv</ta>
            <ta e="T533" id="Seg_8951" s="T532">interj-n:case</ta>
            <ta e="T534" id="Seg_8952" s="T533">pers-v:ins-n:case</ta>
            <ta e="T535" id="Seg_8953" s="T534">v-v&gt;ptcp</ta>
            <ta e="T536" id="Seg_8954" s="T535">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T537" id="Seg_8955" s="T536">adj-n&gt;v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T538" id="Seg_8956" s="T537">v-v&gt;v-v&gt;adv</ta>
            <ta e="T539" id="Seg_8957" s="T538">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T540" id="Seg_8958" s="T539">n-n:case</ta>
            <ta e="T541" id="Seg_8959" s="T540">pers-n:case</ta>
            <ta e="T542" id="Seg_8960" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_8961" s="T542">v-v:pn</ta>
            <ta e="T544" id="Seg_8962" s="T543">n-n&gt;adv</ta>
            <ta e="T545" id="Seg_8963" s="T544">adj-adj&gt;adv</ta>
            <ta e="T546" id="Seg_8964" s="T545">v-v:ins-v:pn</ta>
            <ta e="T547" id="Seg_8965" s="T546">interj-n:case</ta>
            <ta e="T548" id="Seg_8966" s="T547">n-n&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T549" id="Seg_8967" s="T548">v-v:ins-v:pn</ta>
            <ta e="T550" id="Seg_8968" s="T549">conj</ta>
            <ta e="T551" id="Seg_8969" s="T550">n-n:ins-n:case</ta>
            <ta e="T552" id="Seg_8970" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_8971" s="T552">v-v&gt;v-v&gt;adv</ta>
            <ta e="T554" id="Seg_8972" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_8973" s="T554">pp-n:case</ta>
            <ta e="T556" id="Seg_8974" s="T555">v-v&gt;v-v:pn</ta>
            <ta e="T557" id="Seg_8975" s="T556">n-n&gt;adj</ta>
            <ta e="T558" id="Seg_8976" s="T557">n-n:case-n:obl.poss</ta>
            <ta e="T559" id="Seg_8977" s="T558">v-v:tense.mood-v:pn</ta>
            <ta e="T560" id="Seg_8978" s="T559">n-n:num-n:case</ta>
            <ta e="T561" id="Seg_8979" s="T560">n-n&gt;n-n:case</ta>
            <ta e="T562" id="Seg_8980" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_8981" s="T562">v-v:tense.mood-v:pn</ta>
            <ta e="T564" id="Seg_8982" s="T563">n-n:case</ta>
            <ta e="T565" id="Seg_8983" s="T564">n-n&gt;n-n:case</ta>
            <ta e="T566" id="Seg_8984" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_8985" s="T566">v-v:pn</ta>
            <ta e="T568" id="Seg_8986" s="T567">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T569" id="Seg_8987" s="T568">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T570" id="Seg_8988" s="T569">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T571" id="Seg_8989" s="T570">n-n:case</ta>
            <ta e="T572" id="Seg_8990" s="T571">n-n:num-n:case</ta>
            <ta e="T573" id="Seg_8991" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_8992" s="T573">adv-adv&gt;adv</ta>
            <ta e="T575" id="Seg_8993" s="T574">n-n:ins-n&gt;adj</ta>
            <ta e="T576" id="Seg_8994" s="T575">n-n:case</ta>
            <ta e="T577" id="Seg_8995" s="T576">v-v&gt;ptcp</ta>
            <ta e="T578" id="Seg_8996" s="T577">v-v&gt;n-n:case</ta>
            <ta e="T579" id="Seg_8997" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_8998" s="T579">n-n-n:case</ta>
            <ta e="T581" id="Seg_8999" s="T580">v-v&gt;v-v:pn</ta>
            <ta e="T582" id="Seg_9000" s="T581">adv</ta>
            <ta e="T583" id="Seg_9001" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9002" s="T583">adj</ta>
            <ta e="T585" id="Seg_9003" s="T584">n-n:ins-n:num-n:case</ta>
            <ta e="T586" id="Seg_9004" s="T585">v-v:pn</ta>
            <ta e="T587" id="Seg_9005" s="T586">pers-n:ins-n:num-n:case</ta>
            <ta e="T588" id="Seg_9006" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_9007" s="T588">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_9008" s="T589">adv</ta>
            <ta e="T591" id="Seg_9009" s="T590">pp-n&gt;adv</ta>
            <ta e="T592" id="Seg_9010" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_9011" s="T592">interrog-n:case</ta>
            <ta e="T594" id="Seg_9012" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9013" s="T594">v-v:pn</ta>
            <ta e="T596" id="Seg_9014" s="T595">n-n:ins-n:case</ta>
            <ta e="T597" id="Seg_9015" s="T596">adv-n:case</ta>
            <ta e="T598" id="Seg_9016" s="T597">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T599" id="Seg_9017" s="T598">quant</ta>
            <ta e="T600" id="Seg_9018" s="T599">adj-adj&gt;adv</ta>
            <ta e="T601" id="Seg_9019" s="T600">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_9020" s="T601">n-n:case</ta>
            <ta e="T603" id="Seg_9021" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_9022" s="T603">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T605" id="Seg_9023" s="T604">n-n:ins-n:case</ta>
            <ta e="T606" id="Seg_9024" s="T605">adj-adj&gt;adj</ta>
            <ta e="T607" id="Seg_9025" s="T606">v-v:tense.mood-v:pn</ta>
            <ta e="T608" id="Seg_9026" s="T607">n-n:case</ta>
            <ta e="T609" id="Seg_9027" s="T608">pp-n:case</ta>
            <ta e="T610" id="Seg_9028" s="T609">v-v&gt;adv</ta>
            <ta e="T611" id="Seg_9029" s="T610">adj-adj&gt;adv</ta>
            <ta e="T612" id="Seg_9030" s="T611">preverb</ta>
            <ta e="T613" id="Seg_9031" s="T612">v-v&gt;v-v:tense.mood-v:pn</ta>
            <ta e="T614" id="Seg_9032" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_9033" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_9034" s="T615">n-n:case</ta>
            <ta e="T617" id="Seg_9035" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_9036" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_9037" s="T618">v-v&gt;v-v:ins-v&gt;v-v:pn</ta>
            <ta e="T620" id="Seg_9038" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_9039" s="T620">num</ta>
            <ta e="T622" id="Seg_9040" s="T621">n-n:ins-n:num-n:case</ta>
            <ta e="T623" id="Seg_9041" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_9042" s="T623">v-v:pn</ta>
            <ta e="T625" id="Seg_9043" s="T624">num-n:num</ta>
            <ta e="T626" id="Seg_9044" s="T625">quant</ta>
            <ta e="T627" id="Seg_9045" s="T626">n-n&gt;adv</ta>
            <ta e="T628" id="Seg_9046" s="T627">adj</ta>
            <ta e="T629" id="Seg_9047" s="T628">n-n:case-n:poss</ta>
            <ta e="T630" id="Seg_9048" s="T629">dem-adj&gt;adv</ta>
            <ta e="T631" id="Seg_9049" s="T630">v-v:pn</ta>
            <ta e="T632" id="Seg_9050" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_9051" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9052" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9053" s="T634">v-v:pn</ta>
            <ta e="T636" id="Seg_9054" s="T635">interrog</ta>
            <ta e="T637" id="Seg_9055" s="T636">adj</ta>
            <ta e="T638" id="Seg_9056" s="T637">interrog</ta>
            <ta e="T639" id="Seg_9057" s="T638">quant</ta>
            <ta e="T640" id="Seg_9058" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_9059" s="T640">v-v:ins-v:pn</ta>
            <ta e="T642" id="Seg_9060" s="T641">interrog-n:case</ta>
            <ta e="T643" id="Seg_9061" s="T642">clit</ta>
            <ta e="T644" id="Seg_9062" s="T643">n-n:case-n:poss</ta>
            <ta e="T645" id="Seg_9063" s="T644">v-v:tense-v:pn</ta>
            <ta e="T646" id="Seg_9064" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_9065" s="T646">pers</ta>
            <ta e="T648" id="Seg_9066" s="T647">v-v&gt;v-v:pn</ta>
            <ta e="T649" id="Seg_9067" s="T648">v-v:tense-v:pn</ta>
            <ta e="T650" id="Seg_9068" s="T649">interrog-v:pn</ta>
            <ta e="T651" id="Seg_9069" s="T650">v-v:mood.pn</ta>
            <ta e="T652" id="Seg_9070" s="T651">v-v&gt;v-v:mood.pn</ta>
            <ta e="T653" id="Seg_9071" s="T652">pers</ta>
            <ta e="T654" id="Seg_9072" s="T653">adv-adv:case</ta>
            <ta e="T655" id="Seg_9073" s="T654">adj-adj&gt;adv</ta>
            <ta e="T656" id="Seg_9074" s="T655">v-v:mood.pn</ta>
            <ta e="T657" id="Seg_9075" s="T656">conj</ta>
            <ta e="T658" id="Seg_9076" s="T657">pers</ta>
            <ta e="T659" id="Seg_9077" s="T658">adj</ta>
            <ta e="T660" id="Seg_9078" s="T659">n-n:ins-v:pn</ta>
            <ta e="T661" id="Seg_9079" s="T660">n-n:obl.poss-n:case</ta>
            <ta e="T662" id="Seg_9080" s="T661">adv</ta>
            <ta e="T663" id="Seg_9081" s="T662">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_9082" s="T663">conj</ta>
            <ta e="T665" id="Seg_9083" s="T664">pers</ta>
            <ta e="T666" id="Seg_9084" s="T665">adj</ta>
            <ta e="T667" id="Seg_9085" s="T666">n-v:pn</ta>
            <ta e="T668" id="Seg_9086" s="T667">pers</ta>
            <ta e="T669" id="Seg_9087" s="T668">n-n:obl.poss-n:case</ta>
            <ta e="T670" id="Seg_9088" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_9089" s="T670">conj</ta>
            <ta e="T672" id="Seg_9090" s="T671">pers</ta>
            <ta e="T673" id="Seg_9091" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_9092" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_9093" s="T674">n-n:obl.poss-n:case</ta>
            <ta e="T676" id="Seg_9094" s="T675">conj</ta>
            <ta e="T677" id="Seg_9095" s="T676">pers</ta>
            <ta e="T678" id="Seg_9096" s="T677">n-n:case</ta>
            <ta e="T679" id="Seg_9097" s="T678">n-n:ins-v:pn</ta>
            <ta e="T680" id="Seg_9098" s="T679">pers</ta>
            <ta e="T681" id="Seg_9099" s="T680">n-n:obl.poss-n:case</ta>
            <ta e="T682" id="Seg_9100" s="T681">v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_9101" s="T682">n-n:ins-n:case</ta>
            <ta e="T684" id="Seg_9102" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_9103" s="T684">preverb</ta>
            <ta e="T686" id="Seg_9104" s="T685">v-v&gt;v-v:pn</ta>
            <ta e="T687" id="Seg_9105" s="T686">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T688" id="Seg_9106" s="T687">v-v&gt;v-v:pn</ta>
            <ta e="T689" id="Seg_9107" s="T688">pers-n:ins-n:num-n:ins-n:case</ta>
            <ta e="T690" id="Seg_9108" s="T689">n-n:case-n:poss</ta>
            <ta e="T691" id="Seg_9109" s="T690">pp</ta>
            <ta e="T692" id="Seg_9110" s="T691">v-v&gt;v-v&gt;v-v&gt;adv</ta>
            <ta e="T693" id="Seg_9111" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_9112" s="T693">v-v:pn</ta>
            <ta e="T695" id="Seg_9113" s="T694">adj-adj&gt;v-v&gt;v-v:ins-v:tense-v:pn</ta>
            <ta e="T696" id="Seg_9114" s="T695">dem-n&gt;adj</ta>
            <ta e="T697" id="Seg_9115" s="T696">v-v:pn</ta>
            <ta e="T698" id="Seg_9116" s="T697">n-n:case-n:poss</ta>
            <ta e="T699" id="Seg_9117" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_9118" s="T699">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_9119" s="T700">pers</ta>
            <ta e="T702" id="Seg_9120" s="T701">adv</ta>
            <ta e="T703" id="Seg_9121" s="T702">clit</ta>
            <ta e="T704" id="Seg_9122" s="T703">pers</ta>
            <ta e="T705" id="Seg_9123" s="T704">v-v&gt;v-v:pn</ta>
            <ta e="T706" id="Seg_9124" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_9125" s="T706">adv</ta>
            <ta e="T708" id="Seg_9126" s="T707">n-n:case-n:poss</ta>
            <ta e="T709" id="Seg_9127" s="T708">v-v&gt;v-v:pn</ta>
            <ta e="T710" id="Seg_9128" s="T709">dem</ta>
            <ta e="T711" id="Seg_9129" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_9130" s="T711">n-n:ins-n:case</ta>
            <ta e="T713" id="Seg_9131" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_9132" s="T713">v-v:tense-v:pn</ta>
            <ta e="T715" id="Seg_9133" s="T714">n-n&gt;n-n:case</ta>
            <ta e="T716" id="Seg_9134" s="T715">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_9135" s="T716">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T718" id="Seg_9136" s="T717">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T719" id="Seg_9137" s="T718">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_9138" s="T719">n-n&gt;adj</ta>
            <ta e="T721" id="Seg_9139" s="T720">n-n&gt;n-n:case</ta>
            <ta e="T722" id="Seg_9140" s="T721">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T723" id="Seg_9141" s="T722">n-n:case</ta>
            <ta e="T724" id="Seg_9142" s="T723">pp-n:case</ta>
            <ta e="T725" id="Seg_9143" s="T724">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T726" id="Seg_9144" s="T725">pers-n:ins-n&gt;adj</ta>
            <ta e="T727" id="Seg_9145" s="T726">n-n:ins-n:case</ta>
            <ta e="T728" id="Seg_9146" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9147" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_9148" s="T729">n-n:ins-n&gt;n-n:case</ta>
            <ta e="T731" id="Seg_9149" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_9150" s="T731">v-v&gt;v-v&gt;adv</ta>
            <ta e="T733" id="Seg_9151" s="T732">adj</ta>
            <ta e="T734" id="Seg_9152" s="T733">n-n:case-n:poss</ta>
            <ta e="T735" id="Seg_9153" s="T734">n-n:ins-n:case</ta>
            <ta e="T736" id="Seg_9154" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_9155" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_9156" s="T737">pp-adv:case</ta>
            <ta e="T739" id="Seg_9157" s="T738">v-v:inf.poss</ta>
            <ta e="T740" id="Seg_9158" s="T739">n-n:case</ta>
            <ta e="T741" id="Seg_9159" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_9160" s="T741">v-v&gt;v-v:ins-v:ins-v:pn</ta>
            <ta e="T743" id="Seg_9161" s="T742">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T744" id="Seg_9162" s="T743">pers-n:ins-n:num-n:case</ta>
            <ta e="T745" id="Seg_9163" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_9164" s="T745">adv</ta>
            <ta e="T747" id="Seg_9165" s="T746">n-n&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T748" id="Seg_9166" s="T747">n-n&gt;adv</ta>
            <ta e="T749" id="Seg_9167" s="T748">conj</ta>
            <ta e="T750" id="Seg_9168" s="T749">emphpro-n&gt;adv</ta>
            <ta e="T751" id="Seg_9169" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_9170" s="T751">adv</ta>
            <ta e="T753" id="Seg_9171" s="T752">v-v:tense.mood-v:pn</ta>
            <ta e="T754" id="Seg_9172" s="T753">v-v:tense-v:pn</ta>
            <ta e="T755" id="Seg_9173" s="T754">pers-v:ins-n:case</ta>
            <ta e="T756" id="Seg_9174" s="T755">n-n&gt;v-v&gt;ptcp</ta>
            <ta e="T757" id="Seg_9175" s="T756">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T759" id="Seg_9176" s="T758">adj</ta>
            <ta e="T760" id="Seg_9177" s="T759">n-n:case</ta>
            <ta e="T761" id="Seg_9178" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_9179" s="T761">n-n:case-n:poss</ta>
            <ta e="T763" id="Seg_9180" s="T762">adj</ta>
            <ta e="T764" id="Seg_9181" s="T763">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T765" id="Seg_9182" s="T764">n-n&gt;v-v&gt;v-v&gt;ptcp</ta>
            <ta e="T766" id="Seg_9183" s="T765">n-n:case-n:poss</ta>
            <ta e="T767" id="Seg_9184" s="T766">pers</ta>
            <ta e="T768" id="Seg_9185" s="T767">pers</ta>
            <ta e="T769" id="Seg_9186" s="T768">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T770" id="Seg_9187" s="T769">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T771" id="Seg_9188" s="T770">n-n:case-n:poss</ta>
            <ta e="T772" id="Seg_9189" s="T771">pp</ta>
            <ta e="T773" id="Seg_9190" s="T772">n-n:case</ta>
            <ta e="T774" id="Seg_9191" s="T773">%%</ta>
            <ta e="T775" id="Seg_9192" s="T774">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T776" id="Seg_9193" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_9194" s="T776">adj</ta>
            <ta e="T778" id="Seg_9195" s="T777">n-n:case</ta>
            <ta e="T779" id="Seg_9196" s="T778">quant-n:num-n:case</ta>
            <ta e="T780" id="Seg_9197" s="T779">pers</ta>
            <ta e="T781" id="Seg_9198" s="T780">emphpro-n:case</ta>
            <ta e="T782" id="Seg_9199" s="T781">pp</ta>
            <ta e="T783" id="Seg_9200" s="T782">v-v&gt;v-v:ins-v:pn</ta>
            <ta e="T784" id="Seg_9201" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_9202" s="T784">n-n:case</ta>
            <ta e="T786" id="Seg_9203" s="T785">preverb</ta>
            <ta e="T787" id="Seg_9204" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_9205" s="T787">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T789" id="Seg_9206" s="T788">ptcl</ta>
            <ta e="T790" id="Seg_9207" s="T789">interrog</ta>
            <ta e="T791" id="Seg_9208" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_9209" s="T791">n-n&gt;adv</ta>
            <ta e="T793" id="Seg_9210" s="T792">n-n:ins-n:case</ta>
            <ta e="T794" id="Seg_9211" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_9212" s="T794">adj</ta>
            <ta e="T796" id="Seg_9213" s="T795">pers</ta>
            <ta e="T798" id="Seg_9214" s="T797">n-n:case-n:poss</ta>
            <ta e="T799" id="Seg_9215" s="T798">n-n:case-n:poss</ta>
            <ta e="T800" id="Seg_9216" s="T799">emphpro</ta>
            <ta e="T801" id="Seg_9217" s="T800">n-n:case-n:poss</ta>
            <ta e="T802" id="Seg_9218" s="T801">adj-n&gt;adj</ta>
            <ta e="T803" id="Seg_9219" s="T802">n-n&gt;n-n:case</ta>
            <ta e="T804" id="Seg_9220" s="T803">v-v&gt;v-v:pn</ta>
            <ta e="T805" id="Seg_9221" s="T804">adj-adj&gt;adj</ta>
            <ta e="T806" id="Seg_9222" s="T805">n-n&gt;adv</ta>
            <ta e="T807" id="Seg_9223" s="T806">pers</ta>
            <ta e="T808" id="Seg_9224" s="T807">n-n&gt;adj</ta>
            <ta e="T809" id="Seg_9225" s="T808">n-n&gt;adj</ta>
            <ta e="T810" id="Seg_9226" s="T809">n-n&gt;adj</ta>
            <ta e="T811" id="Seg_9227" s="T810">n-n&gt;adj</ta>
            <ta e="T812" id="Seg_9228" s="T811">conj</ta>
            <ta e="T813" id="Seg_9229" s="T812">pers</ta>
            <ta e="T814" id="Seg_9230" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_9231" s="T814">v-v&gt;adv</ta>
            <ta e="T816" id="Seg_9232" s="T815">n-n:num-n:case</ta>
            <ta e="T817" id="Seg_9233" s="T816">pers</ta>
            <ta e="T818" id="Seg_9234" s="T817">v-v&gt;v-v:pn</ta>
            <ta e="T820" id="Seg_9235" s="T819">adv-adv:case</ta>
            <ta e="T821" id="Seg_9236" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_9237" s="T821">n-n&gt;adj</ta>
            <ta e="T823" id="Seg_9238" s="T822">n-n:case</ta>
            <ta e="T824" id="Seg_9239" s="T823">n-v:pn</ta>
            <ta e="T825" id="Seg_9240" s="T824">v-v:ins-v:pn</ta>
            <ta e="T826" id="Seg_9241" s="T825">adj</ta>
            <ta e="T827" id="Seg_9242" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_9243" s="T827">adv</ta>
            <ta e="T829" id="Seg_9244" s="T828">adj</ta>
            <ta e="T830" id="Seg_9245" s="T829">n-n:case</ta>
            <ta e="T831" id="Seg_9246" s="T830">n-n:case-n:poss</ta>
            <ta e="T832" id="Seg_9247" s="T831">n-v:pn</ta>
            <ta e="T833" id="Seg_9248" s="T832">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9249" s="T0">n</ta>
            <ta e="T2" id="Seg_9250" s="T1">n</ta>
            <ta e="T3" id="Seg_9251" s="T2">v</ta>
            <ta e="T4" id="Seg_9252" s="T3">v</ta>
            <ta e="T5" id="Seg_9253" s="T4">adv</ta>
            <ta e="T6" id="Seg_9254" s="T5">n</ta>
            <ta e="T7" id="Seg_9255" s="T6">v</ta>
            <ta e="T8" id="Seg_9256" s="T7">n</ta>
            <ta e="T9" id="Seg_9257" s="T8">pp</ta>
            <ta e="T10" id="Seg_9258" s="T9">n</ta>
            <ta e="T11" id="Seg_9259" s="T10">pers</ta>
            <ta e="T12" id="Seg_9260" s="T11">v</ta>
            <ta e="T13" id="Seg_9261" s="T12">emphpro</ta>
            <ta e="T14" id="Seg_9262" s="T13">v</ta>
            <ta e="T15" id="Seg_9263" s="T14">n</ta>
            <ta e="T16" id="Seg_9264" s="T15">pp</ta>
            <ta e="T17" id="Seg_9265" s="T16">n</ta>
            <ta e="T18" id="Seg_9266" s="T17">pp</ta>
            <ta e="T19" id="Seg_9267" s="T18">v</ta>
            <ta e="T20" id="Seg_9268" s="T19">v</ta>
            <ta e="T21" id="Seg_9269" s="T20">adv</ta>
            <ta e="T22" id="Seg_9270" s="T21">n</ta>
            <ta e="T23" id="Seg_9271" s="T22">v</ta>
            <ta e="T24" id="Seg_9272" s="T23">pers</ta>
            <ta e="T25" id="Seg_9273" s="T24">adj</ta>
            <ta e="T26" id="Seg_9274" s="T25">n</ta>
            <ta e="T27" id="Seg_9275" s="T26">adv</ta>
            <ta e="T28" id="Seg_9276" s="T27">v</ta>
            <ta e="T29" id="Seg_9277" s="T28">interrog</ta>
            <ta e="T30" id="Seg_9278" s="T29">adj</ta>
            <ta e="T31" id="Seg_9279" s="T30">adj</ta>
            <ta e="T32" id="Seg_9280" s="T31">n</ta>
            <ta e="T33" id="Seg_9281" s="T32">pp</ta>
            <ta e="T34" id="Seg_9282" s="T33">n</ta>
            <ta e="T35" id="Seg_9283" s="T34">pp</ta>
            <ta e="T36" id="Seg_9284" s="T35">adj</ta>
            <ta e="T37" id="Seg_9285" s="T36">n</ta>
            <ta e="T38" id="Seg_9286" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_9287" s="T38">v</ta>
            <ta e="T40" id="Seg_9288" s="T39">adv</ta>
            <ta e="T41" id="Seg_9289" s="T40">pers</ta>
            <ta e="T42" id="Seg_9290" s="T41">v</ta>
            <ta e="T43" id="Seg_9291" s="T42">adj</ta>
            <ta e="T44" id="Seg_9292" s="T43">n</ta>
            <ta e="T45" id="Seg_9293" s="T44">v</ta>
            <ta e="T46" id="Seg_9294" s="T45">n</ta>
            <ta e="T47" id="Seg_9295" s="T46">v</ta>
            <ta e="T48" id="Seg_9296" s="T47">n</ta>
            <ta e="T49" id="Seg_9297" s="T48">pp</ta>
            <ta e="T50" id="Seg_9298" s="T49">n</ta>
            <ta e="T51" id="Seg_9299" s="T50">quant</ta>
            <ta e="T52" id="Seg_9300" s="T51">adj</ta>
            <ta e="T53" id="Seg_9301" s="T52">v</ta>
            <ta e="T54" id="Seg_9302" s="T53">v</ta>
            <ta e="T55" id="Seg_9303" s="T54">v</ta>
            <ta e="T56" id="Seg_9304" s="T55">num</ta>
            <ta e="T57" id="Seg_9305" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9306" s="T57">num</ta>
            <ta e="T59" id="Seg_9307" s="T58">n</ta>
            <ta e="T60" id="Seg_9308" s="T59">n</ta>
            <ta e="T61" id="Seg_9309" s="T60">n</ta>
            <ta e="T62" id="Seg_9310" s="T61">adv</ta>
            <ta e="T63" id="Seg_9311" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_9312" s="T63">v</ta>
            <ta e="T65" id="Seg_9313" s="T64">v</ta>
            <ta e="T66" id="Seg_9314" s="T65">adv</ta>
            <ta e="T67" id="Seg_9315" s="T66">n</ta>
            <ta e="T68" id="Seg_9316" s="T67">adv</ta>
            <ta e="T69" id="Seg_9317" s="T68">pers</ta>
            <ta e="T70" id="Seg_9318" s="T69">v</ta>
            <ta e="T71" id="Seg_9319" s="T70">n</ta>
            <ta e="T72" id="Seg_9320" s="T71">n</ta>
            <ta e="T73" id="Seg_9321" s="T72">n</ta>
            <ta e="T74" id="Seg_9322" s="T73">n</ta>
            <ta e="T75" id="Seg_9323" s="T74">adj</ta>
            <ta e="T76" id="Seg_9324" s="T75">adv</ta>
            <ta e="T77" id="Seg_9325" s="T76">adj</ta>
            <ta e="T78" id="Seg_9326" s="T77">n</ta>
            <ta e="T79" id="Seg_9327" s="T78">n</ta>
            <ta e="T80" id="Seg_9328" s="T79">n</ta>
            <ta e="T81" id="Seg_9329" s="T80">adv</ta>
            <ta e="T82" id="Seg_9330" s="T81">ptcp</ta>
            <ta e="T83" id="Seg_9331" s="T82">adv</ta>
            <ta e="T84" id="Seg_9332" s="T83">adv</ta>
            <ta e="T85" id="Seg_9333" s="T84">v</ta>
            <ta e="T86" id="Seg_9334" s="T85">n</ta>
            <ta e="T87" id="Seg_9335" s="T86">v</ta>
            <ta e="T88" id="Seg_9336" s="T87">n</ta>
            <ta e="T89" id="Seg_9337" s="T88">v</ta>
            <ta e="T90" id="Seg_9338" s="T89">pers</ta>
            <ta e="T91" id="Seg_9339" s="T90">adj</ta>
            <ta e="T92" id="Seg_9340" s="T91">v</ta>
            <ta e="T93" id="Seg_9341" s="T92">v</ta>
            <ta e="T94" id="Seg_9342" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_9343" s="T94">v</ta>
            <ta e="T96" id="Seg_9344" s="T95">v</ta>
            <ta e="T97" id="Seg_9345" s="T96">conj</ta>
            <ta e="T98" id="Seg_9346" s="T97">n</ta>
            <ta e="T99" id="Seg_9347" s="T98">adv</ta>
            <ta e="T100" id="Seg_9348" s="T99">v</ta>
            <ta e="T101" id="Seg_9349" s="T100">v</ta>
            <ta e="T102" id="Seg_9350" s="T101">v</ta>
            <ta e="T103" id="Seg_9351" s="T102">adv</ta>
            <ta e="T104" id="Seg_9352" s="T103">n</ta>
            <ta e="T105" id="Seg_9353" s="T104">interrog</ta>
            <ta e="T106" id="Seg_9354" s="T105">v</ta>
            <ta e="T107" id="Seg_9355" s="T106">n</ta>
            <ta e="T108" id="Seg_9356" s="T107">v</ta>
            <ta e="T109" id="Seg_9357" s="T108">n</ta>
            <ta e="T110" id="Seg_9358" s="T109">v</ta>
            <ta e="T111" id="Seg_9359" s="T110">v</ta>
            <ta e="T112" id="Seg_9360" s="T111">conj</ta>
            <ta e="T113" id="Seg_9361" s="T112">n</ta>
            <ta e="T114" id="Seg_9362" s="T113">adj</ta>
            <ta e="T115" id="Seg_9363" s="T114">adj</ta>
            <ta e="T116" id="Seg_9364" s="T115">n</ta>
            <ta e="T117" id="Seg_9365" s="T116">v</ta>
            <ta e="T118" id="Seg_9366" s="T117">n</ta>
            <ta e="T119" id="Seg_9367" s="T118">interrog</ta>
            <ta e="T120" id="Seg_9368" s="T119">v</ta>
            <ta e="T121" id="Seg_9369" s="T120">n</ta>
            <ta e="T122" id="Seg_9370" s="T121">v</ta>
            <ta e="T123" id="Seg_9371" s="T122">emphpro</ta>
            <ta e="T124" id="Seg_9372" s="T123">conj</ta>
            <ta e="T125" id="Seg_9373" s="T124">adj</ta>
            <ta e="T126" id="Seg_9374" s="T125">n</ta>
            <ta e="T127" id="Seg_9375" s="T126">adj</ta>
            <ta e="T128" id="Seg_9376" s="T127">n</ta>
            <ta e="T129" id="Seg_9377" s="T128">adj</ta>
            <ta e="T130" id="Seg_9378" s="T129">n</ta>
            <ta e="T131" id="Seg_9379" s="T130">adj</ta>
            <ta e="T132" id="Seg_9380" s="T131">n</ta>
            <ta e="T133" id="Seg_9381" s="T132">quant</ta>
            <ta e="T134" id="Seg_9382" s="T133">v</ta>
            <ta e="T135" id="Seg_9383" s="T134">adv</ta>
            <ta e="T136" id="Seg_9384" s="T135">conj</ta>
            <ta e="T137" id="Seg_9385" s="T136">adv</ta>
            <ta e="T138" id="Seg_9386" s="T137">v</ta>
            <ta e="T139" id="Seg_9387" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_9388" s="T139">adv</ta>
            <ta e="T141" id="Seg_9389" s="T140">adj</ta>
            <ta e="T142" id="Seg_9390" s="T141">v</ta>
            <ta e="T143" id="Seg_9391" s="T142">n</ta>
            <ta e="T144" id="Seg_9392" s="T143">pp</ta>
            <ta e="T145" id="Seg_9393" s="T144">v</ta>
            <ta e="T146" id="Seg_9394" s="T145">adv</ta>
            <ta e="T147" id="Seg_9395" s="T146">n</ta>
            <ta e="T148" id="Seg_9396" s="T147">num</ta>
            <ta e="T149" id="Seg_9397" s="T148">n</ta>
            <ta e="T150" id="Seg_9398" s="T149">adj</ta>
            <ta e="T151" id="Seg_9399" s="T150">n</ta>
            <ta e="T152" id="Seg_9400" s="T151">v</ta>
            <ta e="T153" id="Seg_9401" s="T152">v</ta>
            <ta e="T154" id="Seg_9402" s="T153">pers</ta>
            <ta e="T155" id="Seg_9403" s="T154">num</ta>
            <ta e="T156" id="Seg_9404" s="T155">pers</ta>
            <ta e="T157" id="Seg_9405" s="T156">pp</ta>
            <ta e="T158" id="Seg_9406" s="T157">v</ta>
            <ta e="T159" id="Seg_9407" s="T158">adj</ta>
            <ta e="T160" id="Seg_9408" s="T159">adv</ta>
            <ta e="T161" id="Seg_9409" s="T160">v</ta>
            <ta e="T162" id="Seg_9410" s="T161">pers</ta>
            <ta e="T163" id="Seg_9411" s="T162">adv</ta>
            <ta e="T164" id="Seg_9412" s="T163">v</ta>
            <ta e="T165" id="Seg_9413" s="T164">ptcp</ta>
            <ta e="T166" id="Seg_9414" s="T165">v</ta>
            <ta e="T167" id="Seg_9415" s="T166">n</ta>
            <ta e="T168" id="Seg_9416" s="T167">n</ta>
            <ta e="T169" id="Seg_9417" s="T168">v</ta>
            <ta e="T170" id="Seg_9418" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_9419" s="T170">adv</ta>
            <ta e="T172" id="Seg_9420" s="T171">preverb</ta>
            <ta e="T173" id="Seg_9421" s="T172">v</ta>
            <ta e="T174" id="Seg_9422" s="T173">pers</ta>
            <ta e="T175" id="Seg_9423" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_9424" s="T175">n</ta>
            <ta e="T177" id="Seg_9425" s="T176">pp</ta>
            <ta e="T178" id="Seg_9426" s="T177">quant</ta>
            <ta e="T179" id="Seg_9427" s="T178">adj</ta>
            <ta e="T180" id="Seg_9428" s="T179">n</ta>
            <ta e="T181" id="Seg_9429" s="T180">adj</ta>
            <ta e="T182" id="Seg_9430" s="T181">v</ta>
            <ta e="T183" id="Seg_9431" s="T182">n</ta>
            <ta e="T184" id="Seg_9432" s="T183">v</ta>
            <ta e="T185" id="Seg_9433" s="T184">pers</ta>
            <ta e="T186" id="Seg_9434" s="T185">n</ta>
            <ta e="T187" id="Seg_9435" s="T186">n</ta>
            <ta e="T188" id="Seg_9436" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_9437" s="T188">v</ta>
            <ta e="T190" id="Seg_9438" s="T189">pers</ta>
            <ta e="T191" id="Seg_9439" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9440" s="T191">num</ta>
            <ta e="T193" id="Seg_9441" s="T192">quant</ta>
            <ta e="T194" id="Seg_9442" s="T193">adj</ta>
            <ta e="T195" id="Seg_9443" s="T194">n</ta>
            <ta e="T196" id="Seg_9444" s="T195">conj</ta>
            <ta e="T197" id="Seg_9445" s="T196">adj</ta>
            <ta e="T198" id="Seg_9446" s="T197">v</ta>
            <ta e="T199" id="Seg_9447" s="T198">n</ta>
            <ta e="T200" id="Seg_9448" s="T199">n</ta>
            <ta e="T201" id="Seg_9449" s="T200">v</ta>
            <ta e="T202" id="Seg_9450" s="T201">n</ta>
            <ta e="T203" id="Seg_9451" s="T202">v</ta>
            <ta e="T204" id="Seg_9452" s="T203">n</ta>
            <ta e="T205" id="Seg_9453" s="T204">v</ta>
            <ta e="T206" id="Seg_9454" s="T205">n</ta>
            <ta e="T207" id="Seg_9455" s="T206">v</ta>
            <ta e="T208" id="Seg_9456" s="T207">n</ta>
            <ta e="T210" id="Seg_9457" s="T209">v</ta>
            <ta e="T211" id="Seg_9458" s="T210">n</ta>
            <ta e="T212" id="Seg_9459" s="T211">adv</ta>
            <ta e="T213" id="Seg_9460" s="T212">n</ta>
            <ta e="T214" id="Seg_9461" s="T213">n</ta>
            <ta e="T215" id="Seg_9462" s="T214">adj</ta>
            <ta e="T216" id="Seg_9463" s="T215">n</ta>
            <ta e="T217" id="Seg_9464" s="T216">adv</ta>
            <ta e="T218" id="Seg_9465" s="T217">adv</ta>
            <ta e="T219" id="Seg_9466" s="T218">v</ta>
            <ta e="T220" id="Seg_9467" s="T219">pro</ta>
            <ta e="T221" id="Seg_9468" s="T220">adv</ta>
            <ta e="T222" id="Seg_9469" s="T221">v</ta>
            <ta e="T223" id="Seg_9470" s="T222">v</ta>
            <ta e="T224" id="Seg_9471" s="T223">v</ta>
            <ta e="T225" id="Seg_9472" s="T224">adj</ta>
            <ta e="T226" id="Seg_9473" s="T225">adv</ta>
            <ta e="T227" id="Seg_9474" s="T226">adj</ta>
            <ta e="T228" id="Seg_9475" s="T227">n</ta>
            <ta e="T229" id="Seg_9476" s="T228">n</ta>
            <ta e="T230" id="Seg_9477" s="T229">adv</ta>
            <ta e="T231" id="Seg_9478" s="T230">adv</ta>
            <ta e="T232" id="Seg_9479" s="T231">adv</ta>
            <ta e="T233" id="Seg_9480" s="T232">adj</ta>
            <ta e="T234" id="Seg_9481" s="T233">n</ta>
            <ta e="T235" id="Seg_9482" s="T234">v</ta>
            <ta e="T236" id="Seg_9483" s="T235">n</ta>
            <ta e="T237" id="Seg_9484" s="T236">n</ta>
            <ta e="T238" id="Seg_9485" s="T237">nprop</ta>
            <ta e="T239" id="Seg_9486" s="T238">v</ta>
            <ta e="T240" id="Seg_9487" s="T239">n</ta>
            <ta e="T241" id="Seg_9488" s="T240">n</ta>
            <ta e="T242" id="Seg_9489" s="T241">n</ta>
            <ta e="T243" id="Seg_9490" s="T242">v</ta>
            <ta e="T244" id="Seg_9491" s="T243">n</ta>
            <ta e="T245" id="Seg_9492" s="T244">ptcp</ta>
            <ta e="T246" id="Seg_9493" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9494" s="T246">v</ta>
            <ta e="T248" id="Seg_9495" s="T247">num</ta>
            <ta e="T249" id="Seg_9496" s="T248">adj</ta>
            <ta e="T250" id="Seg_9497" s="T249">n</ta>
            <ta e="T251" id="Seg_9498" s="T250">conj</ta>
            <ta e="T252" id="Seg_9499" s="T251">num</ta>
            <ta e="T253" id="Seg_9500" s="T252">conj</ta>
            <ta e="T254" id="Seg_9501" s="T253">num</ta>
            <ta e="T255" id="Seg_9502" s="T254">n</ta>
            <ta e="T256" id="Seg_9503" s="T255">n</ta>
            <ta e="T257" id="Seg_9504" s="T256">v</ta>
            <ta e="T258" id="Seg_9505" s="T257">n</ta>
            <ta e="T259" id="Seg_9506" s="T258">n</ta>
            <ta e="T260" id="Seg_9507" s="T259">ptcp</ta>
            <ta e="T261" id="Seg_9508" s="T260">n</ta>
            <ta e="T262" id="Seg_9509" s="T261">emphpro</ta>
            <ta e="T263" id="Seg_9510" s="T262">n</ta>
            <ta e="T264" id="Seg_9511" s="T263">pers</ta>
            <ta e="T265" id="Seg_9512" s="T264">v</ta>
            <ta e="T266" id="Seg_9513" s="T265">pers</ta>
            <ta e="T267" id="Seg_9514" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_9515" s="T267">v</ta>
            <ta e="T269" id="Seg_9516" s="T268">pers</ta>
            <ta e="T270" id="Seg_9517" s="T269">quant</ta>
            <ta e="T271" id="Seg_9518" s="T270">n</ta>
            <ta e="T272" id="Seg_9519" s="T271">conj</ta>
            <ta e="T273" id="Seg_9520" s="T272">adj</ta>
            <ta e="T274" id="Seg_9521" s="T273">v</ta>
            <ta e="T275" id="Seg_9522" s="T274">interrog</ta>
            <ta e="T276" id="Seg_9523" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_9524" s="T276">n</ta>
            <ta e="T278" id="Seg_9525" s="T277">v</ta>
            <ta e="T279" id="Seg_9526" s="T278">n</ta>
            <ta e="T280" id="Seg_9527" s="T279">v</ta>
            <ta e="T281" id="Seg_9528" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_9529" s="T281">v</ta>
            <ta e="T283" id="Seg_9530" s="T282">n</ta>
            <ta e="T284" id="Seg_9531" s="T283">n</ta>
            <ta e="T285" id="Seg_9532" s="T284">adj</ta>
            <ta e="T286" id="Seg_9533" s="T285">quant</ta>
            <ta e="T287" id="Seg_9534" s="T286">n</ta>
            <ta e="T288" id="Seg_9535" s="T287">conj</ta>
            <ta e="T289" id="Seg_9536" s="T288">adj</ta>
            <ta e="T290" id="Seg_9537" s="T289">v</ta>
            <ta e="T291" id="Seg_9538" s="T290">n</ta>
            <ta e="T292" id="Seg_9539" s="T291">n</ta>
            <ta e="T293" id="Seg_9540" s="T292">adv</ta>
            <ta e="T294" id="Seg_9541" s="T293">v</ta>
            <ta e="T295" id="Seg_9542" s="T294">conj</ta>
            <ta e="T296" id="Seg_9543" s="T295">v</ta>
            <ta e="T297" id="Seg_9544" s="T296">n</ta>
            <ta e="T298" id="Seg_9545" s="T297">n</ta>
            <ta e="T299" id="Seg_9546" s="T298">v</ta>
            <ta e="T300" id="Seg_9547" s="T299">n</ta>
            <ta e="T301" id="Seg_9548" s="T300">v</ta>
            <ta e="T302" id="Seg_9549" s="T301">adv</ta>
            <ta e="T303" id="Seg_9550" s="T302">interj</ta>
            <ta e="T304" id="Seg_9551" s="T303">pers</ta>
            <ta e="T305" id="Seg_9552" s="T304">adj</ta>
            <ta e="T306" id="Seg_9553" s="T305">v</ta>
            <ta e="T307" id="Seg_9554" s="T306">pers</ta>
            <ta e="T308" id="Seg_9555" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_9556" s="T308">v</ta>
            <ta e="T310" id="Seg_9557" s="T309">pers</ta>
            <ta e="T311" id="Seg_9558" s="T310">pp</ta>
            <ta e="T312" id="Seg_9559" s="T311">pers</ta>
            <ta e="T313" id="Seg_9560" s="T312">interrog</ta>
            <ta e="T314" id="Seg_9561" s="T313">v</ta>
            <ta e="T315" id="Seg_9562" s="T314">pers</ta>
            <ta e="T316" id="Seg_9563" s="T315">pers</ta>
            <ta e="T317" id="Seg_9564" s="T316">pers</ta>
            <ta e="T318" id="Seg_9565" s="T317">n</ta>
            <ta e="T319" id="Seg_9566" s="T318">v</ta>
            <ta e="T320" id="Seg_9567" s="T319">v</ta>
            <ta e="T321" id="Seg_9568" s="T320">v</ta>
            <ta e="T322" id="Seg_9569" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_9570" s="T322">interrog</ta>
            <ta e="T324" id="Seg_9571" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_9572" s="T324">adj</ta>
            <ta e="T326" id="Seg_9573" s="T325">v</ta>
            <ta e="T327" id="Seg_9574" s="T326">n</ta>
            <ta e="T328" id="Seg_9575" s="T327">adj</ta>
            <ta e="T329" id="Seg_9576" s="T328">n</ta>
            <ta e="T330" id="Seg_9577" s="T329">v</ta>
            <ta e="T331" id="Seg_9578" s="T330">adv</ta>
            <ta e="T332" id="Seg_9579" s="T331">n</ta>
            <ta e="T333" id="Seg_9580" s="T332">v</ta>
            <ta e="T334" id="Seg_9581" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_9582" s="T334">v</ta>
            <ta e="T336" id="Seg_9583" s="T335">interrog</ta>
            <ta e="T337" id="Seg_9584" s="T336">pers</ta>
            <ta e="T338" id="Seg_9585" s="T337">adv</ta>
            <ta e="T339" id="Seg_9586" s="T338">adv</ta>
            <ta e="T340" id="Seg_9587" s="T339">adv</ta>
            <ta e="T341" id="Seg_9588" s="T340">pers</ta>
            <ta e="T342" id="Seg_9589" s="T341">n</ta>
            <ta e="T343" id="Seg_9590" s="T342">v</ta>
            <ta e="T344" id="Seg_9591" s="T343">preverb</ta>
            <ta e="T345" id="Seg_9592" s="T344">v</ta>
            <ta e="T346" id="Seg_9593" s="T345">pers</ta>
            <ta e="T347" id="Seg_9594" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9595" s="T347">v</ta>
            <ta e="T349" id="Seg_9596" s="T348">adj</ta>
            <ta e="T350" id="Seg_9597" s="T349">n</ta>
            <ta e="T351" id="Seg_9598" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_9599" s="T351">v</ta>
            <ta e="T353" id="Seg_9600" s="T352">clit</ta>
            <ta e="T354" id="Seg_9601" s="T353">n</ta>
            <ta e="T355" id="Seg_9602" s="T354">quant</ta>
            <ta e="T356" id="Seg_9603" s="T355">pers</ta>
            <ta e="T357" id="Seg_9604" s="T356">v</ta>
            <ta e="T358" id="Seg_9605" s="T357">adj</ta>
            <ta e="T359" id="Seg_9606" s="T358">n</ta>
            <ta e="T360" id="Seg_9607" s="T359">adv</ta>
            <ta e="T361" id="Seg_9608" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_9609" s="T361">n</ta>
            <ta e="T363" id="Seg_9610" s="T362">v</ta>
            <ta e="T364" id="Seg_9611" s="T363">n</ta>
            <ta e="T365" id="Seg_9612" s="T364">n</ta>
            <ta e="T366" id="Seg_9613" s="T365">adj</ta>
            <ta e="T367" id="Seg_9614" s="T366">adj</ta>
            <ta e="T368" id="Seg_9615" s="T367">n</ta>
            <ta e="T369" id="Seg_9616" s="T368">conj</ta>
            <ta e="T370" id="Seg_9617" s="T369">adj</ta>
            <ta e="T371" id="Seg_9618" s="T370">v</ta>
            <ta e="T372" id="Seg_9619" s="T371">interrog</ta>
            <ta e="T373" id="Seg_9620" s="T372">v</ta>
            <ta e="T374" id="Seg_9621" s="T373">n</ta>
            <ta e="T375" id="Seg_9622" s="T374">adv</ta>
            <ta e="T376" id="Seg_9623" s="T375">n</ta>
            <ta e="T377" id="Seg_9624" s="T376">adv</ta>
            <ta e="T378" id="Seg_9625" s="T377">v</ta>
            <ta e="T379" id="Seg_9626" s="T378">nprop</ta>
            <ta e="T380" id="Seg_9627" s="T379">v</ta>
            <ta e="T381" id="Seg_9628" s="T380">adv</ta>
            <ta e="T382" id="Seg_9629" s="T381">pers</ta>
            <ta e="T383" id="Seg_9630" s="T382">v</ta>
            <ta e="T384" id="Seg_9631" s="T383">emphpro</ta>
            <ta e="T385" id="Seg_9632" s="T384">adj</ta>
            <ta e="T386" id="Seg_9633" s="T385">n</ta>
            <ta e="T387" id="Seg_9634" s="T386">dem</ta>
            <ta e="T388" id="Seg_9635" s="T387">n</ta>
            <ta e="T389" id="Seg_9636" s="T388">n</ta>
            <ta e="T390" id="Seg_9637" s="T389">n</ta>
            <ta e="T391" id="Seg_9638" s="T390">pp</ta>
            <ta e="T392" id="Seg_9639" s="T391">v</ta>
            <ta e="T393" id="Seg_9640" s="T392">ptcp</ta>
            <ta e="T394" id="Seg_9641" s="T393">adv</ta>
            <ta e="T395" id="Seg_9642" s="T394">conj</ta>
            <ta e="T396" id="Seg_9643" s="T395">ptcp</ta>
            <ta e="T397" id="Seg_9644" s="T396">adv</ta>
            <ta e="T398" id="Seg_9645" s="T397">n</ta>
            <ta e="T399" id="Seg_9646" s="T398">adv</ta>
            <ta e="T400" id="Seg_9647" s="T399">adj</ta>
            <ta e="T401" id="Seg_9648" s="T400">n</ta>
            <ta e="T402" id="Seg_9649" s="T401">v</ta>
            <ta e="T403" id="Seg_9650" s="T402">n</ta>
            <ta e="T404" id="Seg_9651" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_9652" s="T404">v</ta>
            <ta e="T406" id="Seg_9653" s="T405">v</ta>
            <ta e="T407" id="Seg_9654" s="T406">n</ta>
            <ta e="T408" id="Seg_9655" s="T407">n</ta>
            <ta e="T409" id="Seg_9656" s="T408">n</ta>
            <ta e="T410" id="Seg_9657" s="T409">nprop</ta>
            <ta e="T411" id="Seg_9658" s="T410">v</ta>
            <ta e="T412" id="Seg_9659" s="T411">n</ta>
            <ta e="T413" id="Seg_9660" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_9661" s="T413">quant</ta>
            <ta e="T415" id="Seg_9662" s="T414">adv</ta>
            <ta e="T416" id="Seg_9663" s="T415">dem</ta>
            <ta e="T417" id="Seg_9664" s="T416">ptcp</ta>
            <ta e="T418" id="Seg_9665" s="T417">n</ta>
            <ta e="T419" id="Seg_9666" s="T418">n</ta>
            <ta e="T420" id="Seg_9667" s="T419">adv</ta>
            <ta e="T421" id="Seg_9668" s="T420">v</ta>
            <ta e="T422" id="Seg_9669" s="T421">n</ta>
            <ta e="T423" id="Seg_9670" s="T422">v</ta>
            <ta e="T424" id="Seg_9671" s="T423">v</ta>
            <ta e="T425" id="Seg_9672" s="T424">n</ta>
            <ta e="T426" id="Seg_9673" s="T425">interrog</ta>
            <ta e="T427" id="Seg_9674" s="T426">v</ta>
            <ta e="T428" id="Seg_9675" s="T427">n</ta>
            <ta e="T429" id="Seg_9676" s="T428">v</ta>
            <ta e="T430" id="Seg_9677" s="T429">n</ta>
            <ta e="T431" id="Seg_9678" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_9679" s="T431">pers</ta>
            <ta e="T433" id="Seg_9680" s="T432">v</ta>
            <ta e="T434" id="Seg_9681" s="T433">interrog</ta>
            <ta e="T435" id="Seg_9682" s="T434">n</ta>
            <ta e="T436" id="Seg_9683" s="T435">v</ta>
            <ta e="T437" id="Seg_9684" s="T436">pers</ta>
            <ta e="T438" id="Seg_9685" s="T437">interrog</ta>
            <ta e="T439" id="Seg_9686" s="T438">v</ta>
            <ta e="T440" id="Seg_9687" s="T439">pro</ta>
            <ta e="T441" id="Seg_9688" s="T440">v</ta>
            <ta e="T442" id="Seg_9689" s="T441">pro</ta>
            <ta e="T443" id="Seg_9690" s="T442">n</ta>
            <ta e="T444" id="Seg_9691" s="T443">pers</ta>
            <ta e="T445" id="Seg_9692" s="T444">adv</ta>
            <ta e="T446" id="Seg_9693" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_9694" s="T446">v</ta>
            <ta e="T448" id="Seg_9695" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_9696" s="T448">v</ta>
            <ta e="T450" id="Seg_9697" s="T449">v</ta>
            <ta e="T451" id="Seg_9698" s="T450">adv</ta>
            <ta e="T452" id="Seg_9699" s="T451">adv</ta>
            <ta e="T453" id="Seg_9700" s="T452">v</ta>
            <ta e="T454" id="Seg_9701" s="T453">n</ta>
            <ta e="T455" id="Seg_9702" s="T454">pers</ta>
            <ta e="T456" id="Seg_9703" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_9704" s="T456">v</ta>
            <ta e="T458" id="Seg_9705" s="T457">adv</ta>
            <ta e="T459" id="Seg_9706" s="T458">emphpro</ta>
            <ta e="T460" id="Seg_9707" s="T459">adv</ta>
            <ta e="T461" id="Seg_9708" s="T460">v</ta>
            <ta e="T462" id="Seg_9709" s="T461">interrog</ta>
            <ta e="T463" id="Seg_9710" s="T462">n</ta>
            <ta e="T464" id="Seg_9711" s="T463">n</ta>
            <ta e="T465" id="Seg_9712" s="T464">v</ta>
            <ta e="T466" id="Seg_9713" s="T465">adj</ta>
            <ta e="T467" id="Seg_9714" s="T466">n</ta>
            <ta e="T468" id="Seg_9715" s="T467">interrog</ta>
            <ta e="T469" id="Seg_9716" s="T468">v</ta>
            <ta e="T470" id="Seg_9717" s="T469">adv</ta>
            <ta e="T471" id="Seg_9718" s="T470">adv</ta>
            <ta e="T472" id="Seg_9719" s="T471">v</ta>
            <ta e="T473" id="Seg_9720" s="T472">pers</ta>
            <ta e="T474" id="Seg_9721" s="T473">v</ta>
            <ta e="T475" id="Seg_9722" s="T474">pers</ta>
            <ta e="T476" id="Seg_9723" s="T475">v</ta>
            <ta e="T477" id="Seg_9724" s="T476">adv</ta>
            <ta e="T478" id="Seg_9725" s="T477">n</ta>
            <ta e="T479" id="Seg_9726" s="T478">n</ta>
            <ta e="T480" id="Seg_9727" s="T479">n</ta>
            <ta e="T481" id="Seg_9728" s="T480">n</ta>
            <ta e="T482" id="Seg_9729" s="T481">v</ta>
            <ta e="T483" id="Seg_9730" s="T482">adv</ta>
            <ta e="T484" id="Seg_9731" s="T483">pers</ta>
            <ta e="T485" id="Seg_9732" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_9733" s="T485">v</ta>
            <ta e="T487" id="Seg_9734" s="T486">n</ta>
            <ta e="T488" id="Seg_9735" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_9736" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_9737" s="T489">n</ta>
            <ta e="T491" id="Seg_9738" s="T490">v</ta>
            <ta e="T492" id="Seg_9739" s="T491">n</ta>
            <ta e="T493" id="Seg_9740" s="T492">v</ta>
            <ta e="T494" id="Seg_9741" s="T493">adv</ta>
            <ta e="T495" id="Seg_9742" s="T494">adj</ta>
            <ta e="T496" id="Seg_9743" s="T495">n</ta>
            <ta e="T497" id="Seg_9744" s="T496">n</ta>
            <ta e="T498" id="Seg_9745" s="T497">v</ta>
            <ta e="T499" id="Seg_9746" s="T498">v</ta>
            <ta e="T500" id="Seg_9747" s="T499">n</ta>
            <ta e="T501" id="Seg_9748" s="T500">n</ta>
            <ta e="T502" id="Seg_9749" s="T501">n</ta>
            <ta e="T503" id="Seg_9750" s="T502">n</ta>
            <ta e="T504" id="Seg_9751" s="T503">nprop</ta>
            <ta e="T505" id="Seg_9752" s="T504">n</ta>
            <ta e="T506" id="Seg_9753" s="T505">adv</ta>
            <ta e="T507" id="Seg_9754" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9755" s="T507">v</ta>
            <ta e="T509" id="Seg_9756" s="T508">conj</ta>
            <ta e="T510" id="Seg_9757" s="T509">adv</ta>
            <ta e="T511" id="Seg_9758" s="T510">v</ta>
            <ta e="T512" id="Seg_9759" s="T511">n</ta>
            <ta e="T513" id="Seg_9760" s="T512">n</ta>
            <ta e="T514" id="Seg_9761" s="T513">n</ta>
            <ta e="T515" id="Seg_9762" s="T514">adv</ta>
            <ta e="T516" id="Seg_9763" s="T515">ptcp</ta>
            <ta e="T517" id="Seg_9764" s="T516">n</ta>
            <ta e="T518" id="Seg_9765" s="T517">n</ta>
            <ta e="T519" id="Seg_9766" s="T518">conj</ta>
            <ta e="T520" id="Seg_9767" s="T519">n</ta>
            <ta e="T521" id="Seg_9768" s="T520">n</ta>
            <ta e="T522" id="Seg_9769" s="T521">n</ta>
            <ta e="T523" id="Seg_9770" s="T522">pp</ta>
            <ta e="T524" id="Seg_9771" s="T523">n</ta>
            <ta e="T525" id="Seg_9772" s="T524">adv</ta>
            <ta e="T526" id="Seg_9773" s="T525">adv</ta>
            <ta e="T527" id="Seg_9774" s="T526">v</ta>
            <ta e="T528" id="Seg_9775" s="T527">adv</ta>
            <ta e="T529" id="Seg_9776" s="T528">n</ta>
            <ta e="T530" id="Seg_9777" s="T529">pp</ta>
            <ta e="T531" id="Seg_9778" s="T530">v</ta>
            <ta e="T532" id="Seg_9779" s="T531">adv</ta>
            <ta e="T533" id="Seg_9780" s="T532">interj</ta>
            <ta e="T534" id="Seg_9781" s="T533">pers</ta>
            <ta e="T535" id="Seg_9782" s="T534">ptcp</ta>
            <ta e="T536" id="Seg_9783" s="T535">v</ta>
            <ta e="T537" id="Seg_9784" s="T536">v</ta>
            <ta e="T538" id="Seg_9785" s="T537">adv</ta>
            <ta e="T539" id="Seg_9786" s="T538">adv</ta>
            <ta e="T540" id="Seg_9787" s="T539">n</ta>
            <ta e="T541" id="Seg_9788" s="T540">pers</ta>
            <ta e="T542" id="Seg_9789" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_9790" s="T542">v</ta>
            <ta e="T544" id="Seg_9791" s="T543">adv</ta>
            <ta e="T545" id="Seg_9792" s="T544">adv</ta>
            <ta e="T546" id="Seg_9793" s="T545">v</ta>
            <ta e="T547" id="Seg_9794" s="T546">interj</ta>
            <ta e="T548" id="Seg_9795" s="T547">adv</ta>
            <ta e="T549" id="Seg_9796" s="T548">v</ta>
            <ta e="T550" id="Seg_9797" s="T549">conj</ta>
            <ta e="T551" id="Seg_9798" s="T550">n</ta>
            <ta e="T552" id="Seg_9799" s="T551">n</ta>
            <ta e="T553" id="Seg_9800" s="T552">adv</ta>
            <ta e="T554" id="Seg_9801" s="T553">n</ta>
            <ta e="T555" id="Seg_9802" s="T554">pp</ta>
            <ta e="T556" id="Seg_9803" s="T555">v</ta>
            <ta e="T557" id="Seg_9804" s="T556">adj</ta>
            <ta e="T558" id="Seg_9805" s="T557">n</ta>
            <ta e="T559" id="Seg_9806" s="T558">v</ta>
            <ta e="T560" id="Seg_9807" s="T559">n</ta>
            <ta e="T561" id="Seg_9808" s="T560">n</ta>
            <ta e="T562" id="Seg_9809" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_9810" s="T562">v</ta>
            <ta e="T564" id="Seg_9811" s="T563">n</ta>
            <ta e="T565" id="Seg_9812" s="T564">n</ta>
            <ta e="T566" id="Seg_9813" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_9814" s="T566">v</ta>
            <ta e="T568" id="Seg_9815" s="T567">v</ta>
            <ta e="T569" id="Seg_9816" s="T568">v</ta>
            <ta e="T570" id="Seg_9817" s="T569">v</ta>
            <ta e="T571" id="Seg_9818" s="T570">n</ta>
            <ta e="T572" id="Seg_9819" s="T571">n</ta>
            <ta e="T573" id="Seg_9820" s="T572">n</ta>
            <ta e="T574" id="Seg_9821" s="T573">adv</ta>
            <ta e="T575" id="Seg_9822" s="T574">adj</ta>
            <ta e="T576" id="Seg_9823" s="T575">n</ta>
            <ta e="T577" id="Seg_9824" s="T576">ptcp</ta>
            <ta e="T578" id="Seg_9825" s="T577">n</ta>
            <ta e="T579" id="Seg_9826" s="T578">n</ta>
            <ta e="T580" id="Seg_9827" s="T579">n</ta>
            <ta e="T581" id="Seg_9828" s="T580">v</ta>
            <ta e="T582" id="Seg_9829" s="T581">adv</ta>
            <ta e="T583" id="Seg_9830" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9831" s="T583">adj</ta>
            <ta e="T585" id="Seg_9832" s="T584">n</ta>
            <ta e="T586" id="Seg_9833" s="T585">v</ta>
            <ta e="T587" id="Seg_9834" s="T586">pers</ta>
            <ta e="T588" id="Seg_9835" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_9836" s="T588">v</ta>
            <ta e="T590" id="Seg_9837" s="T589">adv</ta>
            <ta e="T591" id="Seg_9838" s="T590">adv</ta>
            <ta e="T592" id="Seg_9839" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_9840" s="T592">interrog</ta>
            <ta e="T594" id="Seg_9841" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9842" s="T594">v</ta>
            <ta e="T596" id="Seg_9843" s="T595">n</ta>
            <ta e="T597" id="Seg_9844" s="T596">adv</ta>
            <ta e="T598" id="Seg_9845" s="T597">v</ta>
            <ta e="T599" id="Seg_9846" s="T598">quant</ta>
            <ta e="T600" id="Seg_9847" s="T599">adv</ta>
            <ta e="T601" id="Seg_9848" s="T600">v</ta>
            <ta e="T602" id="Seg_9849" s="T601">n</ta>
            <ta e="T603" id="Seg_9850" s="T602">n</ta>
            <ta e="T604" id="Seg_9851" s="T603">v</ta>
            <ta e="T605" id="Seg_9852" s="T604">n</ta>
            <ta e="T606" id="Seg_9853" s="T605">adj</ta>
            <ta e="T607" id="Seg_9854" s="T606">v</ta>
            <ta e="T608" id="Seg_9855" s="T607">n</ta>
            <ta e="T609" id="Seg_9856" s="T608">pp</ta>
            <ta e="T610" id="Seg_9857" s="T609">adv</ta>
            <ta e="T611" id="Seg_9858" s="T610">adv</ta>
            <ta e="T612" id="Seg_9859" s="T611">preverb</ta>
            <ta e="T613" id="Seg_9860" s="T612">v</ta>
            <ta e="T614" id="Seg_9861" s="T613">n</ta>
            <ta e="T615" id="Seg_9862" s="T614">n</ta>
            <ta e="T616" id="Seg_9863" s="T615">n</ta>
            <ta e="T617" id="Seg_9864" s="T616">n</ta>
            <ta e="T618" id="Seg_9865" s="T617">n</ta>
            <ta e="T619" id="Seg_9866" s="T618">v</ta>
            <ta e="T620" id="Seg_9867" s="T619">n</ta>
            <ta e="T621" id="Seg_9868" s="T620">num</ta>
            <ta e="T622" id="Seg_9869" s="T621">n</ta>
            <ta e="T623" id="Seg_9870" s="T622">ptcl</ta>
            <ta e="T624" id="Seg_9871" s="T623">v</ta>
            <ta e="T625" id="Seg_9872" s="T624">num</ta>
            <ta e="T626" id="Seg_9873" s="T625">quant</ta>
            <ta e="T627" id="Seg_9874" s="T626">adv</ta>
            <ta e="T628" id="Seg_9875" s="T627">adj</ta>
            <ta e="T629" id="Seg_9876" s="T628">n</ta>
            <ta e="T630" id="Seg_9877" s="T629">adv</ta>
            <ta e="T631" id="Seg_9878" s="T630">v</ta>
            <ta e="T632" id="Seg_9879" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_9880" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9881" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9882" s="T634">v</ta>
            <ta e="T636" id="Seg_9883" s="T635">interrog</ta>
            <ta e="T637" id="Seg_9884" s="T636">adj</ta>
            <ta e="T638" id="Seg_9885" s="T637">interrog</ta>
            <ta e="T639" id="Seg_9886" s="T638">quant</ta>
            <ta e="T640" id="Seg_9887" s="T639">n</ta>
            <ta e="T641" id="Seg_9888" s="T640">v</ta>
            <ta e="T642" id="Seg_9889" s="T641">interrog</ta>
            <ta e="T643" id="Seg_9890" s="T642">clit</ta>
            <ta e="T644" id="Seg_9891" s="T643">n</ta>
            <ta e="T645" id="Seg_9892" s="T644">v</ta>
            <ta e="T646" id="Seg_9893" s="T645">ptcl</ta>
            <ta e="T647" id="Seg_9894" s="T646">pers</ta>
            <ta e="T648" id="Seg_9895" s="T647">v</ta>
            <ta e="T649" id="Seg_9896" s="T648">v</ta>
            <ta e="T650" id="Seg_9897" s="T649">v</ta>
            <ta e="T651" id="Seg_9898" s="T650">v</ta>
            <ta e="T652" id="Seg_9899" s="T651">v</ta>
            <ta e="T653" id="Seg_9900" s="T652">pers</ta>
            <ta e="T654" id="Seg_9901" s="T653">adv</ta>
            <ta e="T655" id="Seg_9902" s="T654">adv</ta>
            <ta e="T656" id="Seg_9903" s="T655">v</ta>
            <ta e="T657" id="Seg_9904" s="T656">conj</ta>
            <ta e="T658" id="Seg_9905" s="T657">pers</ta>
            <ta e="T659" id="Seg_9906" s="T658">adj</ta>
            <ta e="T660" id="Seg_9907" s="T659">v</ta>
            <ta e="T661" id="Seg_9908" s="T660">n</ta>
            <ta e="T662" id="Seg_9909" s="T661">adv</ta>
            <ta e="T663" id="Seg_9910" s="T662">v</ta>
            <ta e="T664" id="Seg_9911" s="T663">conj</ta>
            <ta e="T665" id="Seg_9912" s="T664">pers</ta>
            <ta e="T666" id="Seg_9913" s="T665">adj</ta>
            <ta e="T667" id="Seg_9914" s="T666">n</ta>
            <ta e="T668" id="Seg_9915" s="T667">pers</ta>
            <ta e="T669" id="Seg_9916" s="T668">n</ta>
            <ta e="T670" id="Seg_9917" s="T669">v</ta>
            <ta e="T671" id="Seg_9918" s="T670">conj</ta>
            <ta e="T672" id="Seg_9919" s="T671">pers</ta>
            <ta e="T673" id="Seg_9920" s="T672">n</ta>
            <ta e="T674" id="Seg_9921" s="T673">v</ta>
            <ta e="T675" id="Seg_9922" s="T674">n</ta>
            <ta e="T676" id="Seg_9923" s="T675">conj</ta>
            <ta e="T677" id="Seg_9924" s="T676">pers</ta>
            <ta e="T678" id="Seg_9925" s="T677">n</ta>
            <ta e="T679" id="Seg_9926" s="T678">v</ta>
            <ta e="T680" id="Seg_9927" s="T679">pers</ta>
            <ta e="T681" id="Seg_9928" s="T680">n</ta>
            <ta e="T682" id="Seg_9929" s="T681">v</ta>
            <ta e="T683" id="Seg_9930" s="T682">n</ta>
            <ta e="T684" id="Seg_9931" s="T683">n</ta>
            <ta e="T685" id="Seg_9932" s="T684">preverb</ta>
            <ta e="T686" id="Seg_9933" s="T685">v</ta>
            <ta e="T687" id="Seg_9934" s="T686">v</ta>
            <ta e="T688" id="Seg_9935" s="T687">v</ta>
            <ta e="T689" id="Seg_9936" s="T688">pers</ta>
            <ta e="T690" id="Seg_9937" s="T689">n</ta>
            <ta e="T691" id="Seg_9938" s="T690">pp</ta>
            <ta e="T692" id="Seg_9939" s="T691">v</ta>
            <ta e="T693" id="Seg_9940" s="T692">n</ta>
            <ta e="T694" id="Seg_9941" s="T693">v</ta>
            <ta e="T695" id="Seg_9942" s="T694">v</ta>
            <ta e="T696" id="Seg_9943" s="T695">adj</ta>
            <ta e="T697" id="Seg_9944" s="T696">v</ta>
            <ta e="T698" id="Seg_9945" s="T697">n</ta>
            <ta e="T699" id="Seg_9946" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_9947" s="T699">v</ta>
            <ta e="T701" id="Seg_9948" s="T700">pers</ta>
            <ta e="T702" id="Seg_9949" s="T701">adv</ta>
            <ta e="T703" id="Seg_9950" s="T702">clit</ta>
            <ta e="T704" id="Seg_9951" s="T703">pers</ta>
            <ta e="T705" id="Seg_9952" s="T704">v</ta>
            <ta e="T706" id="Seg_9953" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_9954" s="T706">adv</ta>
            <ta e="T708" id="Seg_9955" s="T707">n</ta>
            <ta e="T709" id="Seg_9956" s="T708">v</ta>
            <ta e="T710" id="Seg_9957" s="T709">dem</ta>
            <ta e="T711" id="Seg_9958" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_9959" s="T711">n</ta>
            <ta e="T713" id="Seg_9960" s="T712">n</ta>
            <ta e="T714" id="Seg_9961" s="T713">n</ta>
            <ta e="T715" id="Seg_9962" s="T714">n</ta>
            <ta e="T716" id="Seg_9963" s="T715">v</ta>
            <ta e="T717" id="Seg_9964" s="T716">n</ta>
            <ta e="T718" id="Seg_9965" s="T717">n</ta>
            <ta e="T719" id="Seg_9966" s="T718">v</ta>
            <ta e="T720" id="Seg_9967" s="T719">adj</ta>
            <ta e="T721" id="Seg_9968" s="T720">n</ta>
            <ta e="T722" id="Seg_9969" s="T721">v</ta>
            <ta e="T723" id="Seg_9970" s="T722">n</ta>
            <ta e="T724" id="Seg_9971" s="T723">pp</ta>
            <ta e="T725" id="Seg_9972" s="T724">v</ta>
            <ta e="T726" id="Seg_9973" s="T725">adj</ta>
            <ta e="T727" id="Seg_9974" s="T726">n</ta>
            <ta e="T728" id="Seg_9975" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9976" s="T728">v</ta>
            <ta e="T730" id="Seg_9977" s="T729">n</ta>
            <ta e="T731" id="Seg_9978" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_9979" s="T731">adv</ta>
            <ta e="T733" id="Seg_9980" s="T732">adj</ta>
            <ta e="T734" id="Seg_9981" s="T733">n</ta>
            <ta e="T735" id="Seg_9982" s="T734">n</ta>
            <ta e="T736" id="Seg_9983" s="T735">v</ta>
            <ta e="T737" id="Seg_9984" s="T736">n</ta>
            <ta e="T738" id="Seg_9985" s="T737">pp</ta>
            <ta e="T739" id="Seg_9986" s="T738">v</ta>
            <ta e="T740" id="Seg_9987" s="T739">n</ta>
            <ta e="T741" id="Seg_9988" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_9989" s="T741">v</ta>
            <ta e="T743" id="Seg_9990" s="T742">v</ta>
            <ta e="T744" id="Seg_9991" s="T743">pers</ta>
            <ta e="T745" id="Seg_9992" s="T744">n</ta>
            <ta e="T746" id="Seg_9993" s="T745">adv</ta>
            <ta e="T747" id="Seg_9994" s="T746">adj</ta>
            <ta e="T748" id="Seg_9995" s="T747">adv</ta>
            <ta e="T749" id="Seg_9996" s="T748">conj</ta>
            <ta e="T750" id="Seg_9997" s="T749">emphpro</ta>
            <ta e="T751" id="Seg_9998" s="T750">ptcl</ta>
            <ta e="T752" id="Seg_9999" s="T751">adv</ta>
            <ta e="T753" id="Seg_10000" s="T752">v</ta>
            <ta e="T754" id="Seg_10001" s="T753">v</ta>
            <ta e="T755" id="Seg_10002" s="T754">pers</ta>
            <ta e="T756" id="Seg_10003" s="T755">ptcp</ta>
            <ta e="T757" id="Seg_10004" s="T756">adv</ta>
            <ta e="T759" id="Seg_10005" s="T758">adj</ta>
            <ta e="T760" id="Seg_10006" s="T759">n</ta>
            <ta e="T761" id="Seg_10007" s="T760">n</ta>
            <ta e="T762" id="Seg_10008" s="T761">n</ta>
            <ta e="T763" id="Seg_10009" s="T762">adj</ta>
            <ta e="T764" id="Seg_10010" s="T763">v</ta>
            <ta e="T765" id="Seg_10011" s="T764">ptcp</ta>
            <ta e="T766" id="Seg_10012" s="T765">n</ta>
            <ta e="T767" id="Seg_10013" s="T766">pers</ta>
            <ta e="T768" id="Seg_10014" s="T767">pers</ta>
            <ta e="T769" id="Seg_10015" s="T768">v</ta>
            <ta e="T770" id="Seg_10016" s="T769">v</ta>
            <ta e="T771" id="Seg_10017" s="T770">n</ta>
            <ta e="T772" id="Seg_10018" s="T771">pp</ta>
            <ta e="T773" id="Seg_10019" s="T772">n</ta>
            <ta e="T774" id="Seg_10020" s="T773">%%</ta>
            <ta e="T775" id="Seg_10021" s="T774">v</ta>
            <ta e="T776" id="Seg_10022" s="T775">n</ta>
            <ta e="T777" id="Seg_10023" s="T776">adj</ta>
            <ta e="T778" id="Seg_10024" s="T777">n</ta>
            <ta e="T779" id="Seg_10025" s="T778">quant</ta>
            <ta e="T780" id="Seg_10026" s="T779">pers</ta>
            <ta e="T781" id="Seg_10027" s="T780">emphpro</ta>
            <ta e="T782" id="Seg_10028" s="T781">pp</ta>
            <ta e="T783" id="Seg_10029" s="T782">v</ta>
            <ta e="T784" id="Seg_10030" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_10031" s="T784">n</ta>
            <ta e="T786" id="Seg_10032" s="T785">preverb</ta>
            <ta e="T787" id="Seg_10033" s="T786">v</ta>
            <ta e="T788" id="Seg_10034" s="T787">v</ta>
            <ta e="T789" id="Seg_10035" s="T788">ptcl</ta>
            <ta e="T790" id="Seg_10036" s="T789">interrog</ta>
            <ta e="T791" id="Seg_10037" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_10038" s="T791">adv</ta>
            <ta e="T793" id="Seg_10039" s="T792">n</ta>
            <ta e="T794" id="Seg_10040" s="T793">n</ta>
            <ta e="T795" id="Seg_10041" s="T794">adj</ta>
            <ta e="T796" id="Seg_10042" s="T795">pers</ta>
            <ta e="T798" id="Seg_10043" s="T797">n</ta>
            <ta e="T799" id="Seg_10044" s="T798">n</ta>
            <ta e="T800" id="Seg_10045" s="T799">emphpro</ta>
            <ta e="T801" id="Seg_10046" s="T800">n</ta>
            <ta e="T802" id="Seg_10047" s="T801">adj</ta>
            <ta e="T803" id="Seg_10048" s="T802">n</ta>
            <ta e="T804" id="Seg_10049" s="T803">v</ta>
            <ta e="T805" id="Seg_10050" s="T804">adj</ta>
            <ta e="T806" id="Seg_10051" s="T805">adv</ta>
            <ta e="T807" id="Seg_10052" s="T806">pers</ta>
            <ta e="T808" id="Seg_10053" s="T807">adj</ta>
            <ta e="T809" id="Seg_10054" s="T808">adj</ta>
            <ta e="T810" id="Seg_10055" s="T809">adj</ta>
            <ta e="T811" id="Seg_10056" s="T810">adj</ta>
            <ta e="T812" id="Seg_10057" s="T811">conj</ta>
            <ta e="T813" id="Seg_10058" s="T812">pers</ta>
            <ta e="T814" id="Seg_10059" s="T813">n</ta>
            <ta e="T815" id="Seg_10060" s="T814">adv</ta>
            <ta e="T816" id="Seg_10061" s="T815">n</ta>
            <ta e="T817" id="Seg_10062" s="T816">pers</ta>
            <ta e="T818" id="Seg_10063" s="T817">v</ta>
            <ta e="T820" id="Seg_10064" s="T819">adv</ta>
            <ta e="T821" id="Seg_10065" s="T820">n</ta>
            <ta e="T822" id="Seg_10066" s="T821">adj</ta>
            <ta e="T823" id="Seg_10067" s="T822">n</ta>
            <ta e="T824" id="Seg_10068" s="T823">n</ta>
            <ta e="T825" id="Seg_10069" s="T824">v</ta>
            <ta e="T826" id="Seg_10070" s="T825">adj</ta>
            <ta e="T827" id="Seg_10071" s="T826">n</ta>
            <ta e="T828" id="Seg_10072" s="T827">adv</ta>
            <ta e="T829" id="Seg_10073" s="T828">adj</ta>
            <ta e="T830" id="Seg_10074" s="T829">n</ta>
            <ta e="T831" id="Seg_10075" s="T830">n</ta>
            <ta e="T832" id="Seg_10076" s="T831">n</ta>
            <ta e="T833" id="Seg_10077" s="T832">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_10078" s="T0">RUS:cult</ta>
            <ta e="T8" id="Seg_10079" s="T7">RUS:cult</ta>
            <ta e="T67" id="Seg_10080" s="T66">RUS:cult</ta>
            <ta e="T77" id="Seg_10081" s="T76">RUS:mod</ta>
            <ta e="T78" id="Seg_10082" s="T77">RUS:cult</ta>
            <ta e="T88" id="Seg_10083" s="T87">RUS:cult</ta>
            <ta e="T104" id="Seg_10084" s="T103">RUS:cult</ta>
            <ta e="T107" id="Seg_10085" s="T106">RUS:cult</ta>
            <ta e="T118" id="Seg_10086" s="T117">RUS:cult</ta>
            <ta e="T147" id="Seg_10087" s="T146">RUS:cult</ta>
            <ta e="T149" id="Seg_10088" s="T148">RUS:cult</ta>
            <ta e="T168" id="Seg_10089" s="T167">RUS:cult</ta>
            <ta e="T183" id="Seg_10090" s="T182">RUS:cult</ta>
            <ta e="T186" id="Seg_10091" s="T185">RUS:cult</ta>
            <ta e="T211" id="Seg_10092" s="T210">RUS:cult</ta>
            <ta e="T213" id="Seg_10093" s="T212">RUS:cult</ta>
            <ta e="T236" id="Seg_10094" s="T235">RUS:cult</ta>
            <ta e="T238" id="Seg_10095" s="T237">RUS:cult</ta>
            <ta e="T241" id="Seg_10096" s="T240">RUS:cult</ta>
            <ta e="T258" id="Seg_10097" s="T257">RUS:cult</ta>
            <ta e="T261" id="Seg_10098" s="T260">RUS:cult</ta>
            <ta e="T277" id="Seg_10099" s="T276">RUS:cult</ta>
            <ta e="T283" id="Seg_10100" s="T282">RUS:cult</ta>
            <ta e="T291" id="Seg_10101" s="T290">RUS:cult</ta>
            <ta e="T298" id="Seg_10102" s="T297">RUS:cult</ta>
            <ta e="T306" id="Seg_10103" s="T305">RUS:cult</ta>
            <ta e="T362" id="Seg_10104" s="T361">RUS:cult</ta>
            <ta e="T364" id="Seg_10105" s="T363">RUS:cult</ta>
            <ta e="T376" id="Seg_10106" s="T375">RUS:cult</ta>
            <ta e="T379" id="Seg_10107" s="T378">RUS:cult</ta>
            <ta e="T388" id="Seg_10108" s="T387">RUS:cult</ta>
            <ta e="T408" id="Seg_10109" s="T407">RUS:cult</ta>
            <ta e="T410" id="Seg_10110" s="T409">RUS:cult</ta>
            <ta e="T418" id="Seg_10111" s="T417">RUS:cult</ta>
            <ta e="T502" id="Seg_10112" s="T501">RUS:cult</ta>
            <ta e="T504" id="Seg_10113" s="T503">RUS:cult</ta>
            <ta e="T519" id="Seg_10114" s="T518">RUS:gram</ta>
            <ta e="T540" id="Seg_10115" s="T539">RUS:cult</ta>
            <ta e="T550" id="Seg_10116" s="T549">RUS:gram</ta>
            <ta e="T551" id="Seg_10117" s="T550">RUS:cult</ta>
            <ta e="T554" id="Seg_10118" s="T553">RUS:cult</ta>
            <ta e="T571" id="Seg_10119" s="T570">RUS:cult</ta>
            <ta e="T572" id="Seg_10120" s="T571">RUS:cult</ta>
            <ta e="T573" id="Seg_10121" s="T572">RUS:cult</ta>
            <ta e="T575" id="Seg_10122" s="T574">RUS:cult</ta>
            <ta e="T602" id="Seg_10123" s="T601">RUS:cult</ta>
            <ta e="T683" id="Seg_10124" s="T682">RUS:cult</ta>
            <ta e="T688" id="Seg_10125" s="T687">RUS:cult</ta>
            <ta e="T712" id="Seg_10126" s="T711">RUS:cult</ta>
            <ta e="T720" id="Seg_10127" s="T719">RUS:cult</ta>
            <ta e="T793" id="Seg_10128" s="T792">RUS:cult</ta>
            <ta e="T802" id="Seg_10129" s="T801">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T134" id="Seg_10130" s="T130">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_10131" s="T0">Царь с женой поцеловался (/ [жене] написал), / в далекий путь оделся /.</ta>
            <ta e="T14" id="Seg_10132" s="T7">Возле окна его жена / села одна ждать его. /</ta>
            <ta e="T24" id="Seg_10133" s="T14">С утра до вечера / ждёт, ждёт, / всё время на поле(?) глядит. / </ta>
            <ta e="T35" id="Seg_10134" s="T24">Все глаза проглядела, / и они разболелись, [выглядывая] кого-нибудь / с утренней зари до ночи. /</ta>
            <ta e="T39" id="Seg_10135" s="T35">Никого не видно! /</ta>
            <ta e="T53" id="Seg_10136" s="T39">Только смотрит: / вьюга вьётся, / снег валит на поля(?), / земля вся становится белая. /</ta>
            <ta e="T59" id="Seg_10137" s="T53">Проходит девять месяцев. / </ta>
            <ta e="T65" id="Seg_10138" s="T59">Она не сводит глаз с поля. /</ta>
            <ta e="T74" id="Seg_10139" s="T65">Затем в Сочельник, ночью, / Бог ей даёт дочь. /</ta>
            <ta e="T88" id="Seg_10140" s="T74">Рано утром желанный гость, / днем и ночью [так] долго жданный, / издалека ушёл(?) / и пришёл домой царь-отец. /</ta>
            <ta e="T100" id="Seg_10141" s="T88">Она посмотрела на него, / с трудом пришла в себя, / не вынесла радости / и в полдень умерла. /</ta>
            <ta e="T104" id="Seg_10142" s="T100">Царь долго горевал (/курил). /</ta>
            <ta e="T106" id="Seg_10143" s="T104">Как быть?</ta>
            <ta e="T108" id="Seg_10144" s="T106">Он был грешником. /</ta>
            <ta e="T113" id="Seg_10145" s="T108">Год прошёл будто сон. / </ta>
            <ta e="T118" id="Seg_10146" s="T113">Царь взял себе другую жену. /</ta>
            <ta e="T134" id="Seg_10147" s="T118">Как сказать – девушка была / действительно будто царская жена [царица]: / с длинной шеей, бело[лицая] была, / и умом, и всем взяла. /</ta>
            <ta e="T142" id="Seg_10148" s="T134">Она и дальше всё хорошела, / но потом совсем другая [по характеру] стала. /</ta>
            <ta e="T148" id="Seg_10149" s="T142">Из дома она принесла / только зеркальце одно. /</ta>
            <ta e="T153" id="Seg_10150" s="T148">Зеркальце такое было: / оно умело разговаривать. /</ta>
            <ta e="T168" id="Seg_10151" s="T153">С ним одним, с ним рядом ей было / хорошо на душе, / [ему] она радовалась, / с ним она шутила, / красуясь, говорила: / "Свет мой, зеркальце! </ta>
            <ta e="T182" id="Seg_10152" s="T168">Скажи / вот, пролей свет [=скажи правду]: / я ли на свете всех / красивее, белее? /</ta>
            <ta e="T198" id="Seg_10153" s="T182">Зеркальце говорило: / "Ты, царица, не спорю, / ты одна всех красивее / и белее." /</ta>
            <ta e="T212" id="Seg_10154" s="T198">Царица хохочет, / плечами [пожимает?], / глазами подмигивает, / пальцами щёлкает, / руками вертит, / смотрясь в зеркальце. /</ta>
            <ta e="T232" id="Seg_10155" s="T212">Царевна-сирота / тихо, медленно росла, / между тем росла, / поднялась, выросла, / белолицая, чернобровая, / в душе полностью (?).</ta>
            <ta e="T238" id="Seg_10156" s="T232">Богатого человека [в женихи] сыскали, / королевича Елисея. /</ta>
            <ta e="T255" id="Seg_10157" s="T238">Пришёл человек [сват], царь дал слово, / будто из дома надлежит взять: / семь торговых городов / и сто сорок теремов. /</ta>
            <ta e="T274" id="Seg_10158" s="T255">К девушкам [на девичник] собираясь, / царица одевается / с зеркальцем своим, / [мельком] ему бросает: / "Я ль, скажи мне, всех / красивее и белее?" /</ta>
            <ta e="T278" id="Seg_10159" s="T274">Что же говорит зеркальце? /</ta>
            <ta e="T290" id="Seg_10160" s="T278">"Ты красива, я не спорю, / [но] царевна [лучше?] всех / красивее и белее. /</ta>
            <ta e="T302" id="Seg_10161" s="T290">Царица отпрыгнула / и замахала руками, / шлёпнула зеркальце, / затопала пятками./</ta>
            <ta e="T306" id="Seg_10162" s="T302">"Ах ты, такое[-сякое] стекло! /</ta>
            <ta e="T311" id="Seg_10163" s="T306">Ты [всё] врешь про меня. /</ta>
            <ta e="T315" id="Seg_10164" s="T311">Как ей со мной тягаться(?)?/</ta>
            <ta e="T319" id="Seg_10165" s="T315">Я её обман утихомирю. /</ta>
            <ta e="T322" id="Seg_10166" s="T319">Видите, [какая] выросла она! /</ta>
            <ta e="T333" id="Seg_10167" s="T322">Как [же] ей не быть белой: / мать беременная сидела, / только на снег глядела. /</ta>
            <ta e="T343" id="Seg_10168" s="T333">Ну-ка скажи: / как ей после этого быть красивее меня? /</ta>
            <ta e="T348" id="Seg_10169" s="T343">Скажи [честно?]: я ли не [красивее всех?]/</ta>
            <ta e="T355" id="Seg_10170" s="T348">Обойди наше [царство?], / хоть всю землю. /</ta>
            <ta e="T359" id="Seg_10171" s="T355">[Кроме] меня, нет / [больше таких же красавиц]. </ta>
            <ta e="T361" id="Seg_10172" s="T359">Не так ли?" /</ta>
            <ta e="T371" id="Seg_10173" s="T361">Зеркальце говорит: / "Царевна [лучше?], [?], красивее и белее." /</ta>
            <ta e="T373" id="Seg_10174" s="T371">Что поделаешь?</ta>
            <ta e="T378" id="Seg_10175" s="T373">[Съедаемая завистью?],/ она отбросила зеркальце. /</ta>
            <ta e="T402" id="Seg_10176" s="T378">Позвала Чернавку, / приказала ей идти, / (/сенной девушке своей): / "Царевну в лес / уведи, связав её, / живую, и оставь / там под сосной / на съедение волкам." / </ta>
            <ta e="T407" id="Seg_10177" s="T402">Будет ли чёрт спорить / с сердитой женщиной? /</ta>
            <ta e="T413" id="Seg_10178" s="T407">Вот с царевной / Чернавка пошла в лес. /</ta>
            <ta e="T421" id="Seg_10179" s="T413">Так далеко её увела, / царевну увела. /</ta>
            <ta e="T425" id="Seg_10180" s="T421">[Царевны] до смерти испугалась, / взмолилась: "Жизнь моя! /</ta>
            <ta e="T429" id="Seg_10181" s="T425">В чём, скажи, моя вина? /</ta>
            <ta e="T433" id="Seg_10182" s="T429">Девушка, не убивай меня! /</ta>
            <ta e="T441" id="Seg_10183" s="T433">Вижу в тебе человека, / что захочешь, то сделаю [для тебя]. /</ta>
            <ta e="T457" id="Seg_10184" s="T441">В душе её любя, она [Чернавка] / её не связала, не убила, / отпустила, сказав: / "Иди, Бог тебя хранит."</ta>
            <ta e="T461" id="Seg_10185" s="T457">Потом одна домой пришла. /</ta>
            <ta e="T462" id="Seg_10186" s="T461">"Ну что? / </ta>
            <ta e="T465" id="Seg_10187" s="T462">сказала ей царица, /</ta>
            <ta e="T469" id="Seg_10188" s="T465">Где красивая девушка?" /</ta>
            <ta e="T475" id="Seg_10189" s="T469">"Стоит одна в лесу, – / она сказала [ей?]. /</ta>
            <ta e="T479" id="Seg_10190" s="T475">Её локти коротко связаны.</ta>
            <ta e="T491" id="Seg_10191" s="T479">Зверю в когти попадётся, / мало почувствует,/ но для смерти хватит. /</ta>
            <ta e="T498" id="Seg_10192" s="T491">Пошёл слух: / царевна потерялась! /</ta>
            <ta e="T501" id="Seg_10193" s="T498">Царь горюет по дочери. /</ta>
            <ta e="T518" id="Seg_10194" s="T501">Королевич Елисей, / помолясь Богу, / оделся и поехал искать / прекрасную сердцем(?) /, чтобы потом взять её [в невесты?]. /</ta>
            <ta e="T531" id="Seg_10195" s="T518">А [невеста?], до утра / в лесу блуждая, / между тем всё шла, / [пока не] пришла к дому. /</ta>
            <ta e="T539" id="Seg_10196" s="T531">Навстречу ей [выбежала] с лаем собака, завизжала, / затихла, ласкаясь и радуясь. /</ta>
            <ta e="T543" id="Seg_10197" s="T539">Вот она вошла в ворота. / </ta>
            <ta e="T546" id="Seg_10198" s="T543">Снаружи тихо. /</ta>
            <ta e="T549" id="Seg_10199" s="T546">Собака, радуясь, бежит за ней. /</ta>
            <ta e="T559" id="Seg_10200" s="T549">А царевна пошла, / поднялась на крыльцо, / взяла в руки кольцо [замка]. /</ta>
            <ta e="T567" id="Seg_10201" s="T559">Двери тихонько открыла, / в дом тихо вошла. /</ta>
            <ta e="T579" id="Seg_10202" s="T567">Видит лавки [крытые] ковром, / под иконой дубовый стол, / печь с изразцовой лежанкой. /</ta>
            <ta e="T586" id="Seg_10203" s="T579">Девушка видит: здесь / наверное хорошие люди живут. /</ta>
            <ta e="T589" id="Seg_10204" s="T586">Они не будут ругаться. /</ta>
            <ta e="T595" id="Seg_10205" s="T589">Между тем никого не видно. /</ta>
            <ta e="T613" id="Seg_10206" s="T595">Дом [царевна] обошла, / всё хорошо [прибрала?], / зажгла свечу иконы, / натопила жарко печку, / забралась на полати / и тихонько легла. /</ta>
            <ta e="T617" id="Seg_10207" s="T613">Было около полудня. /</ta>
            <ta e="T627" id="Seg_10208" s="T617">Снаружи послышался шум: / вот заходят семь богатырей, / семь усачей. /</ta>
            <ta e="T641" id="Seg_10209" s="T627">Старший так сказал: / "Что же случилось, как всё хорошо, / как всё красиво. /</ta>
            <ta e="T649" id="Seg_10210" s="T641">Кто-то наш дом [прибрал?], / будто нас ждали. /</ta>
            <ta e="T650" id="Seg_10211" s="T649">Кто ты?</ta>
            <ta e="T656" id="Seg_10212" s="T650">Выйди покажись, / [хорошо?] нас встреть. /</ta>
            <ta e="T663" id="Seg_10213" s="T656">Если ты старый человек, / будешь нам дедушкой. /</ta>
            <ta e="T670" id="Seg_10214" s="T663">Если ты молодой человек, / будешь нам братом. /</ta>
            <ta e="T675" id="Seg_10215" s="T670">Если ты старушка – / [будешь нам] названной матерью. /</ta>
            <ta e="T682" id="Seg_10216" s="T675">Если ты девушка, – / будешь нам сестрой. /</ta>
            <ta e="T694" id="Seg_10217" s="T682">Царевна вниз спустилась, / поздоровалась с ними, / в пояс поклонилась (/трава растёт). /</ta>
            <ta e="T706" id="Seg_10218" s="T694">Раскраснелась, сказала: / "В ваш дом мол я вошла, / хотя меня не звали." /</ta>
            <ta e="T714" id="Seg_10219" s="T706">По её речи они поняли: / это была царевна. /</ta>
            <ta e="T725" id="Seg_10220" s="T714">Они её в уголок посадили, / хлебца дали, / рюмку полную налили, / мох вверху принесла [?] /</ta>
            <ta e="T742" id="Seg_10221" s="T725">Она вина не выпила, / хлебец разломила, / маленький кусочек в рот положила, / отдохнуть с дороги / на кровати попросилась. /</ta>
            <ta e="T758" id="Seg_10222" s="T742">Они увели девушку / наверх в светлую комнату / и (/там) её одну оставили, /засыпающую. / (…)</ta>
            <ta e="T767" id="Seg_10223" s="T758">К красному солнцу наконец / обратился молодец: / "Светлое наше солнце! /</ta>
            <ta e="T783" id="Seg_10224" s="T767">Ты поворачиваешь (/ходишь) / круглый год по небу, / сводишь зиму с теплой весной, / всех нас ты под собой видишь. /</ta>
            <ta e="T787" id="Seg_10225" s="T783">Ты наверное отбросишь мой вопрос [откажешь в ответе]? /</ta>
            <ta e="T797" id="Seg_10226" s="T787">Не видало ли ты где-нибудь на земле / молодой царевны? / (…)</ta>
            <ta e="T803" id="Seg_10227" s="T797">Месяц, месяц, мой друг, / позолоченный рожок! /</ta>
            <ta e="T811" id="Seg_10228" s="T803">Ты встаёшь глубокой ночью, / круглолицый, светлоокий. / </ta>
            <ta e="T819" id="Seg_10229" s="T811">И, любя твой [обычай?], / звезды на тебя смотрят. / (…) </ta>
            <ta e="T830" id="Seg_10230" s="T819">Там на краю тихой речки / есть высокая [гора?], / там глубокая нора. /</ta>
            <ta e="T834" id="Seg_10231" s="T830">Ночь, обрыв, земля. / (…)</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_10232" s="T0">The tsar and his wife kissed (/he wrote [to his wife], / he dressed up to go to a long journey /.</ta>
            <ta e="T14" id="Seg_10233" s="T7">His wife is at the window / she sat down to wait for him. /</ta>
            <ta e="T24" id="Seg_10234" s="T14">From morning to evening / she waits and waits, / all the time she is looking at the meadow(?). /</ta>
            <ta e="T35" id="Seg_10235" s="T24">She tired out her eyes by staring, / and they started to ache, [looking out] for somebody / from dawn to night. /</ta>
            <ta e="T39" id="Seg_10236" s="T35">There is nobody to be seen! /</ta>
            <ta e="T53" id="Seg_10237" s="T39">She looks: / a blizzard is coming, / snow is falling on the meadow(?), / the ground is becoming all white. /</ta>
            <ta e="T59" id="Seg_10238" s="T53">Nine months pass. /</ta>
            <ta e="T65" id="Seg_10239" s="T59">She doesn’t take her eyes off the meadow. /</ta>
            <ta e="T74" id="Seg_10240" s="T65">Then on Christmas Eve, at night, / God gives her a daughter. / </ta>
            <ta e="T88" id="Seg_10241" s="T74">Early in the morning, the welcome guest, / the one [so] long awaited day and night, / the tsar-father left(?) from far away / and came home.</ta>
            <ta e="T100" id="Seg_10242" s="T88">She looked at him, / she came to herself with difficulty, / [but] she couldn’t take the happiness / and at mid-day she died.</ta>
            <ta e="T104" id="Seg_10243" s="T100">The tsar mourned (/smoked) for a long time. /</ta>
            <ta e="T106" id="Seg_10244" s="T104">What to do?</ta>
            <ta e="T108" id="Seg_10245" s="T106">He was a sinner. /</ta>
            <ta e="T113" id="Seg_10246" s="T108">A year passed like a dream. /</ta>
            <ta e="T118" id="Seg_10247" s="T113">The tsar took another wife. /</ta>
            <ta e="T134" id="Seg_10248" s="T118">How to say – the young woman was / really like a tsar’s wife [tsarina]: / [she had] a long neck, a white [face], / and took [him] with her wit and everything. /</ta>
            <ta e="T142" id="Seg_10249" s="T134">Later she was all fine too, / but then she became entirely different [in her character]. /</ta>
            <ta e="T148" id="Seg_10250" s="T142">From home she brought / only a small mirror. /</ta>
            <ta e="T153" id="Seg_10251" s="T148">The mirror was like this: / it could talk. /</ta>
            <ta e="T168" id="Seg_10252" s="T153">Only with it, only with it nearby she felt / good in her soul, / she felt happy with it, / she joked with it, / flaunting she used to say: / "My light, my little mirror!</ta>
            <ta e="T182" id="Seg_10253" s="T168">Tell me / now, shed light [=tell the truth]: / am I in the whole world / the most beautiful, the whitest? / </ta>
            <ta e="T198" id="Seg_10254" s="T182">The little mirror said: / “You are [the most beautiful], tsarina, I’m not contesting it, / you are the most beautiful of all / and the whitest.” /</ta>
            <ta e="T212" id="Seg_10255" s="T198">The tsarina laughs, / [moves?] her shoulders, / winking with her eyes, / snapping her fingers, / turning her hands, / looking at herself in the mirror. /</ta>
            <ta e="T232" id="Seg_10256" s="T212">The orphan tsarevna / was growing up quietly, slowly, / growing up in the meantime, / she rose, grew up, / white faced, with black eyebrows, / fully (?) in her soul.</ta>
            <ta e="T238" id="Seg_10257" s="T232">They found a rich man [to marry her], / the prince Yelisey. /</ta>
            <ta e="T255" id="Seg_10258" s="T238">The man [matchmaker] came, the tsar gave his word, / that one should take from home / seven merchant towns / and one hundred and forty houses. /</ta>
            <ta e="T274" id="Seg_10259" s="T255">Preparing to go to the girls [girls’ meeting], / the tsarina was dressing / with her mirror, / [in passing] she asks: / “Tell me whether I’m of all / the most beautiful and the whitest?” /</ta>
            <ta e="T278" id="Seg_10260" s="T274">What does the mirror say? /</ta>
            <ta e="T290" id="Seg_10261" s="T278">“You are a beauty, I’m not contesting it, / [but] the tsarevna [is better?] / she is [still] more beautiful and white. /</ta>
            <ta e="T302" id="Seg_10262" s="T290">The tsarina jumped away / and started waving her hands, / slapped the mirror, / and started stomping her heels. /</ta>
            <ta e="T306" id="Seg_10263" s="T302">“You such [and such] glass! /</ta>
            <ta e="T311" id="Seg_10264" s="T306">You [only] tell lies about me. /</ta>
            <ta e="T315" id="Seg_10265" s="T311">How could she compete(?) with me? /</ta>
            <ta e="T319" id="Seg_10266" s="T315">I’ll calm down her deception. / </ta>
            <ta e="T322" id="Seg_10267" s="T319">Look, [how] she’s grown! /</ta>
            <ta e="T333" id="Seg_10268" s="T322">How could she not be white: / her mother sat pregnant / looking only at snow. /</ta>
            <ta e="T343" id="Seg_10269" s="T333">Well, tell me: / how could she be more beautiful than me after this? /</ta>
            <ta e="T348" id="Seg_10270" s="T343">Tell me [earnestly?]: am I not [more beautiful than all?]/</ta>
            <ta e="T355" id="Seg_10271" s="T348">Go around our [kingdom?], / or even the whole world. /</ta>
            <ta e="T359" id="Seg_10272" s="T355">[Besides] me, there is no / [other beauties like this].</ta>
            <ta e="T361" id="Seg_10273" s="T359">Isn’t that so?” /</ta>
            <ta e="T371" id="Seg_10274" s="T361">The mirror says: / “The tsarevna is [better?, [?], more beautiful and whiter.” /</ta>
            <ta e="T373" id="Seg_10275" s="T371">What to do?</ta>
            <ta e="T378" id="Seg_10276" s="T373">[All eaten up by jealousy?], / she threw the mirror aside. /</ta>
            <ta e="T402" id="Seg_10277" s="T378">She called [her maid] Chernavka, / ordered her to go, / (/this maid of hers): / “Take the tsarevna to the forest, / tied up, / alive, and leave her / there under the fir tree / so the wolves eat her up.” /</ta>
            <ta e="T407" id="Seg_10278" s="T402">Will [even] the devil argue / with an angry woman? /</ta>
            <ta e="T413" id="Seg_10279" s="T407">So, Chernavka went with the tsarevna to the forest. /</ta>
            <ta e="T421" id="Seg_10280" s="T413">She took her so far [into the forest], / she took the tsarevna. /</ta>
            <ta e="T425" id="Seg_10281" s="T421">[The tsarevna] got scared to death, / she begged: “O my life! /</ta>
            <ta e="T429" id="Seg_10282" s="T425">What is my fault, tell me? /</ta>
            <ta e="T433" id="Seg_10283" s="T429">Young woman, don’t kill me! /</ta>
            <ta e="T441" id="Seg_10284" s="T433">I see the person in you, / whatever you want, I’ll do it [for you]. /</ta>
            <ta e="T457" id="Seg_10285" s="T441">Loving her in her soul, she [Chernavka] / did not tie her up, didn’t kill her, / she let her go, saying: / “Go, God protects you.”</ta>
            <ta e="T461" id="Seg_10286" s="T457">Then she went home alone. /</ta>
            <ta e="T462" id="Seg_10287" s="T461">“So, what?” /</ta>
            <ta e="T465" id="Seg_10288" s="T462">said the tsarina to her, /</ta>
            <ta e="T469" id="Seg_10289" s="T465">Where is the beuatiful girl?” /</ta>
            <ta e="T475" id="Seg_10290" s="T469">“She is standing alone in the forest, - she said [to her?]. /</ta>
            <ta e="T479" id="Seg_10291" s="T475">Her elbows are bound tight.</ta>
            <ta e="T491" id="Seg_10292" s="T479">She will be caught in the claws of a beast, / she will feel little, / but it will be enough to die.</ta>
            <ta e="T498" id="Seg_10293" s="T491">The rumour spread: / the tsarevna has disappeared! /</ta>
            <ta e="T501" id="Seg_10294" s="T498">The tsar is grieving for his daughter./</ta>
            <ta e="T518" id="Seg_10295" s="T501">Prince Yelisey, / having prayed to God, / got dressed and went to look / for the one with a beautiful heart(?) /, so that later he could take her [as his bride?]./</ta>
            <ta e="T531" id="Seg_10296" s="T518">And [the bride?], having wandered until morning / in the forest, / in the meantime just kept walking, / [until] she came to a house. /</ta>
            <ta e="T539" id="Seg_10297" s="T531">A dog [ran out], barking, as she approached, started to yelp, / [then] quieted down, rubbing up against her, being happy./</ta>
            <ta e="T543" id="Seg_10298" s="T539">She went in through the gate. /</ta>
            <ta e="T546" id="Seg_10299" s="T543">It’s quiet outside. /</ta>
            <ta e="T549" id="Seg_10300" s="T546">The dog, happy, is running after her. /</ta>
            <ta e="T559" id="Seg_10301" s="T549">The tsarevna went, / went up to the door, / took the ring [of the lock] in her hands./</ta>
            <ta e="T567" id="Seg_10302" s="T559">She opened the doors quietly / and went into the house.</ta>
            <ta e="T579" id="Seg_10303" s="T567">She sees benches [covered with] rugs, / an oak table under an icon, a stove with a tiled place to lie on. /</ta>
            <ta e="T586" id="Seg_10304" s="T579">The young woman sees: here / there must be good people living. /</ta>
            <ta e="T589" id="Seg_10305" s="T586">They won’t scold [me]. /</ta>
            <ta e="T595" id="Seg_10306" s="T589">In the meantime nobody can be seen. /</ta>
            <ta e="T613" id="Seg_10307" s="T595">[The tsarevna] went through the house, / [cleaned up?] everywhere, / lit the candle by the icon, / lit the stove hot, / climbed up to the sleeping bench / and lay down quietly.</ta>
            <ta e="T617" id="Seg_10308" s="T613">It was close to noon. /</ta>
            <ta e="T627" id="Seg_10309" s="T617">Noise is heard from the outside: / seven warriors come in, / seven moustached men. </ta>
            <ta e="T641" id="Seg_10310" s="T627"> The oldest one said: / “What happened, everything is so nice, / everything is beautiful. /</ta>
            <ta e="T649" id="Seg_10311" s="T641">Somebody [cleaned up?] our house, / as if they were expecting us. /</ta>
            <ta e="T650" id="Seg_10312" s="T649">Who are you?</ta>
            <ta e="T656" id="Seg_10313" s="T650">Come out and show yourself, / meet us [nicely?]. /</ta>
            <ta e="T663" id="Seg_10314" s="T656">If you are an old man, / you’ll be our grandfather. /</ta>
            <ta e="T670" id="Seg_10315" s="T663">If you are a young man, / you’ll be our brother./</ta>
            <ta e="T675" id="Seg_10316" s="T670">If you are an old woman – / [you will be] our sworned mother. /</ta>
            <ta e="T682" id="Seg_10317" s="T675">If you are a young woman, – / you will be our sister. /</ta>
            <ta e="T694" id="Seg_10318" s="T682">The tsarevna came down, / greeted them, / bowed them down to the waist (/[as high as] grass grows). /</ta>
            <ta e="T706" id="Seg_10319" s="T694">She turned red in the face and said: / “I came into your house, / even though you didn’t invite me.”</ta>
            <ta e="T714" id="Seg_10320" s="T706">From the way she talked, they understood: / that she was the tsarevna. / </ta>
            <ta e="T725" id="Seg_10321" s="T714">They seated her in a corner, / gave her bread, / filled a [wine] glass for her, / she brought moss up [?] /</ta>
            <ta e="T742" id="Seg_10322" s="T725">She didn’t drink any wine, / broke the bread, / put a small piece in her mouth, / and she asked to let her have a rest on the bed after her journey. / /</ta>
            <ta e="T758" id="Seg_10323" s="T742">They led the young woman / upstairs to a well-lit room / and (/there) they left her alone, / as she was falling asleep. / (…)</ta>
            <ta e="T767" id="Seg_10324" s="T758">Finally the young man turned to the red sun: / / “Our bright sun!/</ta>
            <ta e="T783" id="Seg_10325" s="T767">You turn (/go) / around the sky the whole year, / you make the winter meet the warm spring, / and you see all of us underneath you. /</ta>
            <ta e="T787" id="Seg_10326" s="T783">You will perhaps throw my question aside [refuse to answer]? /</ta>
            <ta e="T797" id="Seg_10327" s="T787">Have you seen anywhere on earth / the young tsarevna? (…)</ta>
            <ta e="T803" id="Seg_10328" s="T797">Moon, moon, my friend, / gilded crescent moon! /</ta>
            <ta e="T811" id="Seg_10329" s="T803">You rise in the dark night, / round faced and bright eyed. /</ta>
            <ta e="T819" id="Seg_10330" s="T811">And, liking your [custom?], / the stars look at you. / (…)</ta>
            <ta e="T830" id="Seg_10331" s="T819">There at the edge of the quiet river / there is a tall [mountain?], / there is a deep den. /</ta>
            <ta e="T834" id="Seg_10332" s="T830">The night, the precipice, the ground. / (…)</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_10333" s="T0">Der Zar und seine Frau küssten sich (/ schrieb [der Frau]), / er zog sich an wie für eine weite Reise /.</ta>
            <ta e="T14" id="Seg_10334" s="T7">Am Fenster seine Frau / sie setzte sich alleine, um auf ihn zu warten. /</ta>
            <ta e="T24" id="Seg_10335" s="T14">Von morgens bis abends / sie wartet, sie wartet / die ganze Zeit schaut sie auf das Feld(?). /</ta>
            <ta e="T35" id="Seg_10336" s="T24">Ihre Augen schaute sie durch, / und sie fingen an zu schmerzen, als sie nach jemandem [suchten] / von der Morgenröte bis zur Nacht. /</ta>
            <ta e="T39" id="Seg_10337" s="T35">Niemand ist zu sehen! /</ta>
            <ta e="T53" id="Seg_10338" s="T39">Sie schaut nur: /Ein Schneesturm kommt auf, / Schnee fällt auf das Feld(?), / die Erde wird ganz weiß. /</ta>
            <ta e="T59" id="Seg_10339" s="T53">Neun Monate vergehen. /</ta>
            <ta e="T65" id="Seg_10340" s="T59">Sie wendete ihre Augen nicht vom Feld ab. /</ta>
            <ta e="T74" id="Seg_10341" s="T65">Dann am Heiligabend, nachts, / Gott gibt ihr eine Tochter. /</ta>
            <ta e="T88" id="Seg_10342" s="T74">Früh am morgen der erwünschte Gast, / Tag und Nacht [so] lange erwartet, / kam(?) von weit her / und kam nach Hause der Zar, der Vater. /</ta>
            <ta e="T100" id="Seg_10343" s="T88">Sie sah ihn an, / mit Mühe kam sie zu sich, / sie hielt die Freude nicht aus / und am Mittag starb sie. /</ta>
            <ta e="T104" id="Seg_10344" s="T100">Der Zar trauerte (/rauchte) lange. /</ta>
            <ta e="T106" id="Seg_10345" s="T104">Was tun?</ta>
            <ta e="T108" id="Seg_10346" s="T106">Er war ein Sündiger. /</ta>
            <ta e="T113" id="Seg_10347" s="T108">Ein verging wie im Traum. /</ta>
            <ta e="T118" id="Seg_10348" s="T113">Der Zar nahm eine andere Frau. /</ta>
            <ta e="T134" id="Seg_10349" s="T118">Wie zu sagen – das Mädchen war / wirklich wie eine Zarenfrau [Zarin]: / mit langem Hals, ein weißes [Gesicht], / sie nahm ihn mit ihrem Verstand und allem. /</ta>
            <ta e="T142" id="Seg_10350" s="T134">Später ging es ihr auch gut, / aber dann wurde sie ganz anders. /</ta>
            <ta e="T148" id="Seg_10351" s="T142">Von zuhause brachte sie / nur ein Spieglein. /</ta>
            <ta e="T153" id="Seg_10352" s="T148">Das Spieglein war so eins: / es konnte sprechen. /</ta>
            <ta e="T168" id="Seg_10353" s="T153">Nur mit ihm, nur mit ihm / ging es ihr gut, / sie erfreute sich [an ihm], / mit ihm scherzte sie, / vor Schönheit strahlend sagte sie: / "Mein Licht, Spieglein!</ta>
            <ta e="T182" id="Seg_10354" s="T168">Sage / nun, werfe Licht [=sag die Wahrheit]: / bin ich auf der ganzen Welt / die schönste, die weißeste? /</ta>
            <ta e="T198" id="Seg_10355" s="T182">Das Spieglein sagte: / "Du, Zarin, ich streite nicht, / du alleine bist die schönste / und die weißeste." /</ta>
            <ta e="T212" id="Seg_10356" s="T198">Die Zarin lacht, / sie [bewegt?] ihre Schulern, / sie zwinkert mit ihren Augen, / sie schnippt mit ihren Fingern, / sie dreht ihre Hände, / sich selber im Spieglein betrachtend.</ta>
            <ta e="T232" id="Seg_10357" s="T212">Die Zarenwaisin / wuchs leise, langsam auf, / sie wuchs inzwischen auf, / sie wurde groß, wuchs auf, / mit einem weißen Gesicht, mit schwarzen Augenbrauen, / ganz (?) in der Seele.</ta>
            <ta e="T238" id="Seg_10358" s="T232">Sie fanden einen reichen Mann [zur Heirat], / den Prinzen Jelisej. /</ta>
            <ta e="T255" id="Seg_10359" s="T238">Der Mensch [Brautwerber] kam, der Zar gab sein Wort, / dass man aus dem Hause nehmen soll: / sieben Kaufmannsstädte / und einhundertvierzig Turmhäuser. /</ta>
            <ta e="T274" id="Seg_10360" s="T255">Auf dem Weg zu den Mädchen [zum Junggesellinnenabschied], / zieht sich die Zarin an / mit ihrem Spieglein, / im Vorbeigehen fragt sie: / "Bin ich, sag mir, / die schönste und weißeste?" /</ta>
            <ta e="T278" id="Seg_10361" s="T274">Was sagt das Spieglein? /</ta>
            <ta e="T290" id="Seg_10362" s="T278">"Du bist schön, ich streite nicht, / [aber] die Zarentochter ist [besser?] / sie ist die scönste und weißeste. /</ta>
            <ta e="T302" id="Seg_10363" s="T290">Die Zaren sprang davon / und wedelte mit den Händen, / schlug das Spieglein, / und trampelte mit ihren Absätzen. /</ta>
            <ta e="T306" id="Seg_10364" s="T302">"Ach du, so ein [Stück] Glas! /</ta>
            <ta e="T311" id="Seg_10365" s="T306">Du lügst [nur] über mich. /</ta>
            <ta e="T315" id="Seg_10366" s="T311">Wie soll sie sich mit mir messen(?)? /</ta>
            <ta e="T319" id="Seg_10367" s="T315">Ich beschwichtige ihren Betrug. /</ta>
            <ta e="T322" id="Seg_10368" s="T319">Schaut, [wie] sie gewachsen ist! /</ta>
            <ta e="T333" id="Seg_10369" s="T322">Wie kann sie nicht weiß sein: / die schwangere Mutter saß, / und schaute nur auf den Schnee. /</ta>
            <ta e="T343" id="Seg_10370" s="T333">Nun, sag mir: / wie kann sie danach schöner als ich sein? /</ta>
            <ta e="T348" id="Seg_10371" s="T343">Sag mir [ehrlich?]: bin ich nicht [die schönste?] /</ta>
            <ta e="T355" id="Seg_10372" s="T348">Geh durch unser [Reich?], / oder durch die ganze Welt. /</ta>
            <ta e="T359" id="Seg_10373" s="T355">[Neben] mir gibt es keine / [anderen solchen Schönheiten].</ta>
            <ta e="T361" id="Seg_10374" s="T359">Ist das nicht so?" /</ta>
            <ta e="T371" id="Seg_10375" s="T361">Der Spiegel sagt: /"Die Zarentochter ist [besser?], [?], schöner und weißer." /</ta>
            <ta e="T373" id="Seg_10376" s="T371">Was tun?</ta>
            <ta e="T378" id="Seg_10377" s="T373">[Alle zerfressen von Eifersucht?], / sie warf den Spiegel zur Seite. /</ta>
            <ta e="T402" id="Seg_10378" s="T378">Sie rief Tschernawka, / befahl ihr zu gehen, / (/dieser ihrer Magd): / "Bring die Zarentochter in den Wald, / festgebunden, / lebending und lass sie / dort unter einer Fichte / den Wölfen zum Fraß." /</ta>
            <ta e="T407" id="Seg_10379" s="T402">Streitet der Teufel / mit einer wütenden Frau? /</ta>
            <ta e="T413" id="Seg_10380" s="T407">Nun mit der Zarentochter / ging Tschernawka in den Wald. /</ta>
            <ta e="T421" id="Seg_10381" s="T413">So weit brachte sie sie, / sie brachte die Zarentochter. /</ta>
            <ta e="T425" id="Seg_10382" s="T421">[Die Zarentochter] bekam Todesangst, / sie bat: "Mein Leben! /</ta>
            <ta e="T429" id="Seg_10383" s="T425">Sag, worin liegt meine Schuld? /</ta>
            <ta e="T433" id="Seg_10384" s="T429">Mädchen, töte mich nicht! /</ta>
            <ta e="T441" id="Seg_10385" s="T433">Ich sehe den Menschen in dir, / was immer du möchtes, tue ich [für dich]. /</ta>
            <ta e="T457" id="Seg_10386" s="T441">Sie im Inneren liebend, / band sie [Tschernawka] sie nicht fest, tötete sie nicht, / sie ließ sie und sagte: / "Geh, Gott beschützt dich."</ta>
            <ta e="T461" id="Seg_10387" s="T457">Dann ging sie alleine nach Hause. /</ta>
            <ta e="T462" id="Seg_10388" s="T461">"Nun was?" /</ta>
            <ta e="T465" id="Seg_10389" s="T462">sagte die Zarin zu ihr, /</ta>
            <ta e="T469" id="Seg_10390" s="T465">"wo ist das schöne Mädchen?" /</ta>
            <ta e="T475" id="Seg_10391" s="T469">"Sie steht alleine im Wald", / sagte sie [ihr?]. /</ta>
            <ta e="T479" id="Seg_10392" s="T475">Ihre Ellbogen sind fest angebunden.</ta>
            <ta e="T491" id="Seg_10393" s="T479">Sie wird einem wilden Tier in die Hände fallen, / sie wird wenig fühlen, / aber für den Tod reicht es. /</ta>
            <ta e="T498" id="Seg_10394" s="T491">Es ging das Gerücht umher: / die Zarentochter ist verschwunden! /</ta>
            <ta e="T501" id="Seg_10395" s="T498">Der Zar trauert um seine Tochter. /</ta>
            <ta e="T518" id="Seg_10396" s="T501">Prinz Jelisej, / zu Gott betend, / zog sich an und ging suchen / die Schöne mit Herzem(?) /, um sie dann zur [Frau?] zu nehmen. /</ta>
            <ta e="T531" id="Seg_10397" s="T518">Und [die Braut?], bis zum Morgen / im Wald umherirrend, / ging inzwischen, / [bis] sie an ein Haus kam. /</ta>
            <ta e="T539" id="Seg_10398" s="T531">Auf sie zu [lief] bellend ein Hund, fing an zu kläffen, / wurde ruhig, rieb sich an ihr und freute sich. /</ta>
            <ta e="T543" id="Seg_10399" s="T539">Sie ging durch das Tor hinein. /</ta>
            <ta e="T546" id="Seg_10400" s="T543">Es ist still draußen. /</ta>
            <ta e="T549" id="Seg_10401" s="T546">Der Hund läuft ihr fröhlich nach. /</ta>
            <ta e="T559" id="Seg_10402" s="T549">Die Zarentochter ging, / ging auf die Veranda, / nahm den Ring [des Schlosses] in die Hand. /</ta>
            <ta e="T567" id="Seg_10403" s="T559">Sie öffnete leise die Tür, / sie ging leise ins Haus. /</ta>
            <ta e="T579" id="Seg_10404" s="T567">Sie sieht [Bänke] bedeckt mit Teppich, / und einer Ikone einen Eichentisch, / einen Ofen mit einem gekachelten Platz zum Liegen. /</ta>
            <ta e="T586" id="Seg_10405" s="T579">Das Mädchen sieht: hier / leben wohl gute Leute. /</ta>
            <ta e="T589" id="Seg_10406" s="T586">Sie werden mich nicht beschmipfen. /</ta>
            <ta e="T595" id="Seg_10407" s="T589">In der Zwischenzeit ist niemand zu sehen. /</ta>
            <ta e="T613" id="Seg_10408" s="T595">[Die Zarentochter] ging durch das Haus, / [räumte?] alles auf, / zündete der Ikone eine Kerze an, / heizte den Ofen an, / kletterte auf die Schlafbank / und legte sich leise hin. /</ta>
            <ta e="T617" id="Seg_10409" s="T613">Es war fast Mittag. /</ta>
            <ta e="T627" id="Seg_10410" s="T617">Von draußen ist Lärm zu hören: /sieben Helden kommen hinein, / sieben Schnurbartträger. /</ta>
            <ta e="T641" id="Seg_10411" s="T627">Der älteste sagte: / "Was ist passiert, alles ist so gut, / alles ist so schön. /</ta>
            <ta e="T649" id="Seg_10412" s="T641">Irgendjemand hat unser Haus [aufgeräumt?], / als ob man uns erwartete. /</ta>
            <ta e="T650" id="Seg_10413" s="T649">Wer bist du?</ta>
            <ta e="T656" id="Seg_10414" s="T650">Komm heraus und zeig dich, / triff uns [nett?]. /</ta>
            <ta e="T663" id="Seg_10415" s="T656">Wenn du ein alter Mann bist, / wirst du unser Großvater. /</ta>
            <ta e="T670" id="Seg_10416" s="T663">Wenn du ein junger Mann bist, wirst du unser Bruder. /</ta>
            <ta e="T675" id="Seg_10417" s="T670">Wenn du eine alte Frau bist / [wirst du] unserer Adoptivmutter. /</ta>
            <ta e="T682" id="Seg_10418" s="T675">Wenn du ein Mädchen bist, / wirst du uns eine Schwester. /</ta>
            <ta e="T694" id="Seg_10419" s="T682">Die Zarentochter kam hinunter, / begrüßte sie, / verbeugte sich bis zur Taille (/wie das Gras wächst). /</ta>
            <ta e="T706" id="Seg_10420" s="T694">Sie wurde rot und sagte: / "Ich kam ich euer Haus, / obwohl ihr mich nicht eingeladen habt." /</ta>
            <ta e="T714" id="Seg_10421" s="T706">An ihrer Art zu sprechen verstanden sie: / das war die Zarentochter. /</ta>
            <ta e="T725" id="Seg_10422" s="T714">Sie setzten sie in eine Ecke, / gaben ein Brot, / gossen [ihr] ein Glas [Wein] ein, / sie brachte Moos [?] /</ta>
            <ta e="T742" id="Seg_10423" s="T725">Sie trank den Wein nicht, / sie brach das Brot, / sie legte sich ein kleines Stück in den Mund, / sie bat, sich auf dem Bett vom weg zu erholen. /</ta>
            <ta e="T758" id="Seg_10424" s="T742">Sie brachten das Mädchen / hoch in ein helles Zimmer / und (/dort) ließen sie sie alleine, / als sie eingeschlafen war. / (…)</ta>
            <ta e="T767" id="Seg_10425" s="T758">Schließlich zur roten Sonne / drehte sich der junge Mann: /"Unsere helle Sonne! /</ta>
            <ta e="T783" id="Seg_10426" s="T767">Du drehst dich (/gehst) / das ganze Jahr am Himmel, / du machst den Winter zum warmen Frühling, / uns alle siehst du unter dir. /</ta>
            <ta e="T787" id="Seg_10427" s="T783">Du wirst mein Frage wohl zur Seite werfen [nicht antworten]? /</ta>
            <ta e="T797" id="Seg_10428" s="T787">Sahst du nicht irgendwo auf der Erde / die jungen Zarentochter? (…)</ta>
            <ta e="T803" id="Seg_10429" s="T797">Mond, Mond, mein Freund, / vergoldetes Hörnchen! /</ta>
            <ta e="T811" id="Seg_10430" s="T803">Du gehst in der dunklen Nacht aus, / mit rundem Gesicht und großen Augen. /</ta>
            <ta e="T819" id="Seg_10431" s="T811">Und, dich liebend [Brauch?], / schauen dich die Sterne an. / (…)</ta>
            <ta e="T830" id="Seg_10432" s="T819">Dort am Ufer des ruhigen Flusses / ist ein hoher [Berg?], / dort ist eine tiefe Höhle. /</ta>
            <ta e="T834" id="Seg_10433" s="T830">Die Nacht, die Steilwand, die Erde. / (…)</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_10434" s="T0">царь с женой писали / в далекий путь собрался [оделся] /</ta>
            <ta e="T14" id="Seg_10435" s="T7">возле окна его жена / его ждать одна сидит /</ta>
            <ta e="T24" id="Seg_10436" s="T14">с утра [утром] до вечера / ждет ждет / все [время] на луну глядит / </ta>
            <ta e="T35" id="Seg_10437" s="T24">глаза глядя / заболели который / утренний заря /</ta>
            <ta e="T39" id="Seg_10438" s="T35">некоторых не видно /</ta>
            <ta e="T53" id="Seg_10439" s="T39">чуть [кое-как] он видит [смотрит] / завихрение [сильный ветер, который кружит] загибается / снег / земля вся белая стала /</ta>
            <ta e="T59" id="Seg_10440" s="T53">прошло девять месяцев / </ta>
            <ta e="T65" id="Seg_10441" s="T59">глаза прочь не берет [не хочет убирать (отвести)] /</ta>
            <ta e="T74" id="Seg_10442" s="T65">ночь / ему отдал дочь царю /</ta>
            <ta e="T88" id="Seg_10443" s="T74">рано утром надежный гость / днем и ночь жданный / издалека ушел / домой пришел царь-отец /</ta>
            <ta e="T100" id="Seg_10444" s="T88">посмотрел на нее [него] / тяжело вздохнул / стрелу не поломал [не мог?] /полдень [середина дня] потерялся /</ta>
            <ta e="T104" id="Seg_10445" s="T100">курил долго царь / как быть</ta>
            <ta e="T106" id="Seg_10446" s="T104">как быть</ta>
            <ta e="T108" id="Seg_10447" s="T106">был /</ta>
            <ta e="T113" id="Seg_10448" s="T108">год прошел возьми сон / </ta>
            <ta e="T118" id="Seg_10449" s="T113">другую жену взял царь /</ta>
            <ta e="T134" id="Seg_10450" s="T118">как сказать супруга стала / правда [как будто] как царская жена / высокая бело стало / умом [все ума] всех взял [собрал] /</ta>
            <ta e="T142" id="Seg_10451" s="T134">потом хорошел [хвастается] / совсем другой стал [становится] /</ta>
            <ta e="T148" id="Seg_10452" s="T142">из дома приносила / только зеркало одно /</ta>
            <ta e="T153" id="Seg_10453" s="T148">зеркальце такое было [так стало] / разговаривать умело /</ta>
            <ta e="T168" id="Seg_10454" s="T153">с ней один был [есть] / хорошо на душе радуется [радостно] / с ней весело шутя (шутить) / хорошо разговаривать / здравствуй зеркальце </ta>
            <ta e="T182" id="Seg_10455" s="T168">скажи / смотри вверх положи / я один на земле все / хорошо[ий] вид все бело? /</ta>
            <ta e="T198" id="Seg_10456" s="T182">сказало / ты царская жена не буду спорить / ты одна из всех прекрасна / на вид белая есть /</ta>
            <ta e="T212" id="Seg_10457" s="T198">царская жена засмеялась / плечами пожимает / глазами моргает / пальцами щелкает / руку [руками] вертит / в зеркальце смотря /</ta>
            <ta e="T232" id="Seg_10458" s="T212">царская дочь неродная дочь / тихо тихо росла / между ними росла / выросла / белый глаза без /</ta>
            <ta e="T238" id="Seg_10459" s="T232">давнишний человек нашелся / королевский сын /</ta>
            <ta e="T255" id="Seg_10460" s="T238">пришел человек царь слово дает / дома взять / семь сказал / царский сын /</ta>
            <ta e="T274" id="Seg_10461" s="T255">девушкам собраться / царская жена одевается / с зеркальцем одна / слово ей бросает / я что скажи мне все / на вид я белая (прекрасная) ли? /</ta>
            <ta e="T278" id="Seg_10462" s="T274">зачем зеркальце говорит /</ta>
            <ta e="T290" id="Seg_10463" s="T278">ты прекрасна я не спорю / дочь царя лучше всех / прекрасна ее всех есть /</ta>
            <ta e="T302" id="Seg_10464" s="T290">жена царя прочь ушла / махнула рукой / зеркальце бросила / пятками растоптала /</ta>
            <ta e="T306" id="Seg_10465" s="T302">ах ты такое стекло /</ta>
            <ta e="T311" id="Seg_10466" s="T306">ты все врешь на [про] меня /</ta>
            <ta e="T315" id="Seg_10467" s="T311">он как же смотреть со мной /</ta>
            <ta e="T319" id="Seg_10468" s="T315">я его обман на косточки помну /</ta>
            <ta e="T322" id="Seg_10469" s="T319">смотрите выросла я /</ta>
            <ta e="T333" id="Seg_10470" s="T322">почему я не прекрасна / мать в положении сидя / только в снег глядя /</ta>
            <ta e="T343" id="Seg_10471" s="T333">ну скажи как от меня / потом она на вид лучше стала /</ta>
            <ta e="T348" id="Seg_10472" s="T343">всю правду скажи [вверх скажи] я /</ta>
            <ta e="T355" id="Seg_10473" s="T348">всю землю /</ta>
            <ta e="T359" id="Seg_10474" s="T355">со мной нет/ нянька </ta>
            <ta e="T361" id="Seg_10475" s="T359">так найди /</ta>
            <ta e="T371" id="Seg_10476" s="T361">сказало / дочь царя совсем [лучше] на вид / прекрасная белая /</ta>
            <ta e="T373" id="Seg_10477" s="T371">что сделаешь</ta>
            <ta e="T378" id="Seg_10478" s="T373">тяжело дыша встала / прочь бросила</ta>
            <ta e="T402" id="Seg_10479" s="T378">так его заставила / сама девушка / царскую дочь в лес в глушь / увел замотать / живя / там (здесь) живет / кушать / </ta>
            <ta e="T407" id="Seg_10480" s="T402">медведь будет кушать / сердитая женщина</ta>
            <ta e="T413" id="Seg_10481" s="T407">царской дочкой / пойдет в лес /</ta>
            <ta e="T421" id="Seg_10482" s="T413">так далеко ушли / царская дочь отсюда прочь уйдет /</ta>
            <ta e="T425" id="Seg_10483" s="T421">до смерти испугалась / жить /</ta>
            <ta e="T429" id="Seg_10484" s="T425">где скажи есть /</ta>
            <ta e="T433" id="Seg_10485" s="T429">ты меня дочь не подведи /</ta>
            <ta e="T441" id="Seg_10486" s="T433">какого человека я найду / что любишь то и сделай /</ta>
            <ta e="T457" id="Seg_10487" s="T441">душой тебя любит / не обманет не / послал такое послание / иди небо держать</ta>
            <ta e="T461" id="Seg_10488" s="T457">потом одна домой пришла /</ta>
            <ta e="T462" id="Seg_10489" s="T461">что </ta>
            <ta e="T465" id="Seg_10490" s="T462">жена царя сказала /</ta>
            <ta e="T469" id="Seg_10491" s="T465">красивая девушка где /</ta>
            <ta e="T475" id="Seg_10492" s="T469">одна в лесу стоит / он[а] сказала ему [ей] /</ta>
            <ta e="T479" id="Seg_10493" s="T475">привязана /</ta>
            <ta e="T491" id="Seg_10494" s="T479">зверя в лапу попадет / мало / бродячий человек достаточно [хватит] будет</ta>
            <ta e="T498" id="Seg_10495" s="T491">разговор пошел теперь такой /царская дочь потерялась /</ta>
            <ta e="T501" id="Seg_10496" s="T498">за дочь /</ta>
            <ta e="T518" id="Seg_10497" s="T501">королевский сын / богу молясь [помолившись] прежде / одевшись сын пошел / пошел голова / потом ее возьмет /</ta>
            <ta e="T531" id="Seg_10498" s="T518">до утра / в глуши леса заблудилась / гуляет / над домом приехала /</ta>
            <ta e="T539" id="Seg_10499" s="T531">затаилась где-то / хвостом виляя [собака] радуется /</ta>
            <ta e="T543" id="Seg_10500" s="T539">в ворота он вошел / </ta>
            <ta e="T546" id="Seg_10501" s="T543">на улице тихо /</ta>
            <ta e="T549" id="Seg_10502" s="T546">радуясь /</ta>
            <ta e="T559" id="Seg_10503" s="T549">а царская дочь придя / на крыльцо поднялась / ключ? в руки взял /</ta>
            <ta e="T567" id="Seg_10504" s="T559">двери тихо открыла / в дом тихо вошла /</ta>
            <ta e="T579" id="Seg_10505" s="T567">спать легла [спала, спать будет] ковром на лавку / внизу скамейка [доска] / написанное /</ta>
            <ta e="T586" id="Seg_10506" s="T579">женщина смотрит здесь / наверное хорошие люди живут /</ta>
            <ta e="T589" id="Seg_10507" s="T586">они не будут ругаться /</ta>
            <ta e="T595" id="Seg_10508" s="T589">между ними ничего не видно /</ta>
            <ta e="T613" id="Seg_10509" s="T595">дома вокруг обошел / все хорошо убрала / иконы свечу зажгла / печку жарко натопила / на палату верх залезла / тихо легла /</ta>
            <ta e="T617" id="Seg_10510" s="T613">наступил полдень / </ta>
            <ta e="T627" id="Seg_10511" s="T617">шум слышен на улице / 7 мужчин зашли / 7 все они усатые (бородатые) /</ta>
            <ta e="T641" id="Seg_10512" s="T627">старшая дочь так была (сказала) / что за “люди” все хорошие / как все похожи /</ta>
            <ta e="T649" id="Seg_10513" s="T641">чей-то дом убрала / как будто нас ждали /</ta>
            <ta e="T650" id="Seg_10514" s="T649">кто ты</ta>
            <ta e="T656" id="Seg_10515" s="T650">выйди покажись / нас хорошо встреть /</ta>
            <ta e="T663" id="Seg_10516" s="T656">наверное ты большой человек / дедушкой все время будешь /</ta>
            <ta e="T670" id="Seg_10517" s="T663">наверное ты молодой человек / нам приходишься как брат [братом] /</ta>
            <ta e="T675" id="Seg_10518" s="T670">наверное ты бабушка / будем называть матерью /</ta>
            <ta e="T682" id="Seg_10519" s="T675">наверное ты женщина / нам сестрой будешь /</ta>
            <ta e="T694" id="Seg_10520" s="T682">царская дочь жила / поздоровалась с ними / земля /</ta>
            <ta e="T706" id="Seg_10521" s="T694">наступила осень [деревья (земля) пожелтела] / в дом вошел я / хотя его не звали /</ta>
            <ta e="T714" id="Seg_10522" s="T706">мимо улицы поедут / там с ними [вот это] царя дочь была /</ta>
            <ta e="T725" id="Seg_10523" s="T714">в угол посадили / кусочек хлеба дали / рюмку полную наполнили / мох вверху принесла /</ta>
            <ta e="T742" id="Seg_10524" s="T725">имя вода не взял / кусок хлеба отломил / маленький кусочек [лоскуток] в рот взял / с дороги отдохнуть / кровать просит /</ta>
            <ta e="T758" id="Seg_10525" s="T742">увезли ее дочь / вверх рассветает / одного там оставили его / сон взял один</ta>
            <ta e="T767" id="Seg_10526" s="T758">к красну солнцу наконец / обратился молодец / свет наш солнышко /</ta>
            <ta e="T783" id="Seg_10527" s="T767">ты ходишь / круглый год по небу / сводишь зиму с теплою весной / всех нас видишь под собой /</ta>
            <ta e="T787" id="Seg_10528" s="T783">аль откажешь мне в ответе /</ta>
            <ta e="T797" id="Seg_10529" s="T787">не видно ль где на свете / ты царевны молодой /</ta>
            <ta e="T803" id="Seg_10530" s="T797">месяц месяц мой дружок / позолоченный рожок /</ta>
            <ta e="T811" id="Seg_10531" s="T803">ты видишь во тьме глубокой / круглолицый, светлолицый / </ta>
            <ta e="T819" id="Seg_10532" s="T811">и обычай твой любя / звезды смотрят на тебя / </ta>
            <ta e="T830" id="Seg_10533" s="T819">через быстрая речка там за речкой тихоспущеной / есть высокая гора / в ней глубокая нора /</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T53" id="Seg_10534" s="T39">[OSV:] "mɛrkɨlʼ palʼčʼa" - "a whirlwind".</ta>
            <ta e="T134" id="Seg_10535" s="T118">[OSV:] "tašik" may come from "tɔːšan" - "neck". </ta>
            <ta e="T142" id="Seg_10536" s="T134">[OSV:] 1) "qɔːt-olä" - "too much"; 2) in the archivethere is a note: "somalʼčʼimpɨqo" - "to boast", but this meaning should be checked.</ta>
            <ta e="T198" id="Seg_10537" s="T182">[OSV:] "kes" could be a variant of "keššaŋ" - "in fact".</ta>
            <ta e="T290" id="Seg_10538" s="T278"> ‎‎[OSV:] "qotəl" seems to be the superlative form of "good", see also 4.16, 8.6.</ta>
            <ta e="T355" id="Seg_10539" s="T348">[OSV:] the verbal form "kolʼaptʼatɨ" has been edited into "kolʼaltatɨ".</ta>
            <ta e="T378" id="Seg_10540" s="T373">[OSV:] "puːtɨm amqo" seems to be an idiomatic expression.</ta>
            <ta e="T402" id="Seg_10541" s="T378">[OSV:] 1) "čʼumpɨ neː" - here means "a wolf"; 2) the participle forms "kurɨnʼɨmtɨj", "qəːčʼinʼɨmtɨj" have been edited into "kurɨnʼɨmpɨj", "qəːčʼinʼɨmpɨj".</ta>
            <ta e="T421" id="Seg_10542" s="T413">[OSV:] another possible interpretation: qən-tɔː-j (go.away-IPFV-3DU).</ta>
            <ta e="T457" id="Seg_10543" s="T441">[OSV:] Unusual usage of the possessive flexion in the personal pronoun: "təp-^0-tɨ" (he-NOM-3SG).</ta>
            <ta e="T475" id="Seg_10544" s="T469">[OSV:] 1) Untypical usage of possessive inflexion in the personal pronoun: təp-0-tɨ (she-NOM-3SG); 2) the form of Illative (təpɨntɨ) instead of Genitive (təpɨn) of the personal pronoun would be more correct.</ta>
            <ta e="T518" id="Seg_10545" s="T501">[OSV:] 1) the expression "itɨ-neːtɨ" seems to be idiomatic ("a young woman", "a bride" (?).</ta>
            <ta e="T531" id="Seg_10546" s="T518">[BrM:] Tentative analysis of 'kurɨtäntɨ'.</ta>
            <ta e="T539" id="Seg_10547" s="T531">[OSV:] here "läqa" means "a dog", another interpretation could be "lʼaqa" (friend), see also 7.5.</ta>
            <ta e="T567" id="Seg_10548" s="T559">[OSV:] "čʼontɨka" - "quietly, slowly".</ta>
            <ta e="T617" id="Seg_10549" s="T613">[OSV:] here "tətčʼe" is used in the meaning of "təttɨčʼak" - "closely".</ta>
            <ta e="T694" id="Seg_10550" s="T682">[OSV:] the verbal form "muqšaitɨla" has been edited into "mušqaıːtɨla". Tentative analysis.</ta>
            <ta e="T758" id="Seg_10551" s="T742">[OSV:] 1) "čʼeːlɨmpɨtɨlʼ" - "light" (adj), "ontɨqɨt" - "separately"; 2) after this sentence a part of the original text is omitted.</ta>
            <ta e="T767" id="Seg_10552" s="T758">[OSV:] the verbal form "koraltɨnsa" has been edited into "koraltɨpsa".</ta>
            <ta e="T797" id="Seg_10553" s="T787">[OSV:] after this sentence a part of the original text is omitted.</ta>
            <ta e="T819" id="Seg_10554" s="T811">[OSV:] after this sentence a part of the original text is omitted.</ta>
            <ta e="T830" id="Seg_10555" s="T819">[OSV:] here "čʼontɨ" is used in the meaning of "čʼontɨka" - "slowly".</ta>
            <ta e="T834" id="Seg_10556" s="T830">[OSV:] the text is not finished.</ta>
         </annotation>
         <annotation name="nto" tierref="nto" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="stl"
                          display-name="stl"
                          name="stl"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nto"
                          display-name="nto"
                          name="nto"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
